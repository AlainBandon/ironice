# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Include paths...
Debug_Include_Path=-I"./dependancies/irrlicht/include" -I"./dependancies/audiere/include" -I"./dependancies/vld/include" -I"./dependancies/ft235/include" 
Release_Include_Path=-I"./dependancies/irrlicht/include" -I"./dependancies/audiere/include" -I"./dependancies/ft235/include" 

# Library paths...
Debug_Library_Path=-L"./dependancies/irrlicht/lib/gccdebug" -L"./dependancies/audiere/gcclib" -L"./dependancies/vld/lib/gccWin32" -L"./dependancies/ft235/gcclib" 
Release_Library_Path=-L"./dependancies/irrlicht/lib/gccrelease" -L"./dependancies/audiere/gcclib" -L"./dependancies/ft235/gcclib" 

# Additional libraries...
Debug_Libraries=-Wl,--start-group -lopengl32 -laudiere -lvld -lfreetype  -Wl,--end-group
Release_Libraries=-Wl,--start-group -lopengl32 -laudiere -lfreetype  -Wl,--end-group

# Preprocessor definitions...
Debug_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D IRON_ICE_EDITOR 
Release_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D IRON_ICE_EDITOR 

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags=-O0 -g 
Release_Compiler_Flags=-O2 

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Release 

# Builds the Debug configuration...
.PHONY: Debug
Debug: create_folders ./gccDebug/src/CGUITTFont.o ./gccDebug/src/scene.o ./gccDebug/src/sprite.o ./gccDebug/src/renderview.o ./gccDebug/src/sceneview.o ./gccDebug/src/spriteview.o ./gccDebug/src/view.o ./gccDebug/src/audio.o ./gccDebug/src/engine.o ./gccDebug/src/main_editor.o ./gccDebug/src/utils.o 
	g++ ./gccDebug/src/CGUITTFont.o ./gccDebug/src/scene.o ./gccDebug/src/sprite.o ./gccDebug/src/renderview.o ./gccDebug/src/sceneview.o ./gccDebug/src/spriteview.o ./gccDebug/src/view.o ./gccDebug/src/audio.o ./gccDebug/src/engine.o ./gccDebug/src/main_editor.o ./gccDebug/src/utils.o  $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,./ -o ./gccDebug/IronIceEditor.exe

# Compiles file src/CGUITTFont.cpp for the Debug configuration...
-include ./gccDebug/src/CGUITTFont.d
./gccDebug/src/CGUITTFont.o: src/CGUITTFont.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/CGUITTFont.cpp $(Debug_Include_Path) -o ./gccDebug/src/CGUITTFont.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/CGUITTFont.cpp $(Debug_Include_Path) > ./gccDebug/src/CGUITTFont.d

# Compiles file src/scene.cpp for the Debug configuration...
-include ./gccDebug/src/scene.d
./gccDebug/src/scene.o: src/scene.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/scene.cpp $(Debug_Include_Path) -o ./gccDebug/src/scene.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/scene.cpp $(Debug_Include_Path) > ./gccDebug/src/scene.d

# Compiles file src/sprite.cpp for the Debug configuration...
-include ./gccDebug/src/sprite.d
./gccDebug/src/sprite.o: src/sprite.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/sprite.cpp $(Debug_Include_Path) -o ./gccDebug/src/sprite.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/sprite.cpp $(Debug_Include_Path) > ./gccDebug/src/sprite.d

# Compiles file src/renderview.cpp for the Debug configuration...
-include ./gccDebug/src/renderview.d
./gccDebug/src/renderview.o: src/renderview.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/renderview.cpp $(Debug_Include_Path) -o ./gccDebug/src/renderview.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/renderview.cpp $(Debug_Include_Path) > ./gccDebug/src/renderview.d

# Compiles file src/sceneview.cpp for the Debug configuration...
-include ./gccDebug/src/sceneview.d
./gccDebug/src/sceneview.o: src/sceneview.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/sceneview.cpp $(Debug_Include_Path) -o ./gccDebug/src/sceneview.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/sceneview.cpp $(Debug_Include_Path) > ./gccDebug/src/sceneview.d

# Compiles file src/spriteview.cpp for the Debug configuration...
-include ./gccDebug/src/spriteview.d
./gccDebug/src/spriteview.o: src/spriteview.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/spriteview.cpp $(Debug_Include_Path) -o ./gccDebug/src/spriteview.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/spriteview.cpp $(Debug_Include_Path) > ./gccDebug/src/spriteview.d

# Compiles file src/view.cpp for the Debug configuration...
-include ./gccDebug/src/view.d
./gccDebug/src/view.o: src/view.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/view.cpp $(Debug_Include_Path) -o ./gccDebug/src/view.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/view.cpp $(Debug_Include_Path) > ./gccDebug/src/view.d

# Compiles file src/audio.cpp for the Debug configuration...
-include ./gccDebug/src/audio.d
./gccDebug/src/audio.o: src/audio.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/audio.cpp $(Debug_Include_Path) -o ./gccDebug/src/audio.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/audio.cpp $(Debug_Include_Path) > ./gccDebug/src/audio.d

# Compiles file src/engine.cpp for the Debug configuration...
-include ./gccDebug/src/engine.d
./gccDebug/src/engine.o: src/engine.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/engine.cpp $(Debug_Include_Path) -o ./gccDebug/src/engine.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/engine.cpp $(Debug_Include_Path) > ./gccDebug/src/engine.d

# Compiles file src/main_editor.cpp for the Debug configuration...
-include ./gccDebug/src/main_editor.d
./gccDebug/src/main_editor.o: src/main_editor.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/main_editor.cpp $(Debug_Include_Path) -o ./gccDebug/src/main_editor.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/main_editor.cpp $(Debug_Include_Path) > ./gccDebug/src/main_editor.d

# Compiles file src/utils.cpp for the Debug configuration...
-include ./gccDebug/src/utils.d
./gccDebug/src/utils.o: src/utils.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/utils.cpp $(Debug_Include_Path) -o ./gccDebug/src/utils.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/utils.cpp $(Debug_Include_Path) > ./gccDebug/src/utils.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders ./gccRelease/src/CGUITTFont.o ./gccRelease/src/scene.o ./gccRelease/src/sprite.o ./gccRelease/src/renderview.o ./gccRelease/src/sceneview.o ./gccRelease/src/spriteview.o ./gccRelease/src/view.o ./gccRelease/src/audio.o ./gccRelease/src/engine.o ./gccRelease/src/main_editor.o ./gccRelease/src/utils.o 
	g++ ./gccRelease/src/CGUITTFont.o ./gccRelease/src/scene.o ./gccRelease/src/sprite.o ./gccRelease/src/renderview.o ./gccRelease/src/sceneview.o ./gccRelease/src/spriteview.o ./gccRelease/src/view.o ./gccRelease/src/audio.o ./gccRelease/src/engine.o ./gccRelease/src/main_editor.o ./gccRelease/src/utils.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,./ -o ./gccRelease/IronIceEditor.exe

# Compiles file src/CGUITTFont.cpp for the Release configuration...
-include ./gccRelease/src/CGUITTFont.d
./gccRelease/src/CGUITTFont.o: src/CGUITTFont.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/CGUITTFont.cpp $(Release_Include_Path) -o ./gccRelease/src/CGUITTFont.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/CGUITTFont.cpp $(Release_Include_Path) > ./gccRelease/src/CGUITTFont.d

# Compiles file src/scene.cpp for the Release configuration...
-include ./gccRelease/src/scene.d
./gccRelease/src/scene.o: src/scene.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/scene.cpp $(Release_Include_Path) -o ./gccRelease/src/scene.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/scene.cpp $(Release_Include_Path) > ./gccRelease/src/scene.d

# Compiles file src/sprite.cpp for the Release configuration...
-include ./gccRelease/src/sprite.d
./gccRelease/src/sprite.o: src/sprite.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/sprite.cpp $(Release_Include_Path) -o ./gccRelease/src/sprite.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/sprite.cpp $(Release_Include_Path) > ./gccRelease/src/sprite.d

# Compiles file src/renderview.cpp for the Release configuration...
-include ./gccRelease/src/renderview.d
./gccRelease/src/renderview.o: src/renderview.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/renderview.cpp $(Release_Include_Path) -o ./gccRelease/src/renderview.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/renderview.cpp $(Release_Include_Path) > ./gccRelease/src/renderview.d

# Compiles file src/sceneview.cpp for the Release configuration...
-include ./gccRelease/src/sceneview.d
./gccRelease/src/sceneview.o: src/sceneview.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/sceneview.cpp $(Release_Include_Path) -o ./gccRelease/src/sceneview.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/sceneview.cpp $(Release_Include_Path) > ./gccRelease/src/sceneview.d

# Compiles file src/spriteview.cpp for the Release configuration...
-include ./gccRelease/src/spriteview.d
./gccRelease/src/spriteview.o: src/spriteview.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/spriteview.cpp $(Release_Include_Path) -o ./gccRelease/src/spriteview.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/spriteview.cpp $(Release_Include_Path) > ./gccRelease/src/spriteview.d

# Compiles file src/view.cpp for the Release configuration...
-include ./gccRelease/src/view.d
./gccRelease/src/view.o: src/view.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/view.cpp $(Release_Include_Path) -o ./gccRelease/src/view.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/view.cpp $(Release_Include_Path) > ./gccRelease/src/view.d

# Compiles file src/audio.cpp for the Release configuration...
-include ./gccRelease/src/audio.d
./gccRelease/src/audio.o: src/audio.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/audio.cpp $(Release_Include_Path) -o ./gccRelease/src/audio.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/audio.cpp $(Release_Include_Path) > ./gccRelease/src/audio.d

# Compiles file src/engine.cpp for the Release configuration...
-include ./gccRelease/src/engine.d
./gccRelease/src/engine.o: src/engine.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/engine.cpp $(Release_Include_Path) -o ./gccRelease/src/engine.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/engine.cpp $(Release_Include_Path) > ./gccRelease/src/engine.d

# Compiles file src/main_editor.cpp for the Release configuration...
-include ./gccRelease/src/main_editor.d
./gccRelease/src/main_editor.o: src/main_editor.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/main_editor.cpp $(Release_Include_Path) -o ./gccRelease/src/main_editor.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/main_editor.cpp $(Release_Include_Path) > ./gccRelease/src/main_editor.d

# Compiles file src/utils.cpp for the Release configuration...
-include ./gccRelease/src/utils.d
./gccRelease/src/utils.o: src/utils.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/utils.cpp $(Release_Include_Path) -o ./gccRelease/src/utils.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/utils.cpp $(Release_Include_Path) > ./gccRelease/src/utils.d

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p ./gccDebug
	mkdir -p ./gccRelease

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f ./gccDebug/*.o
	rm -f ./gccDebug/*.d
	rm -f ./gccDebug/*.a
	rm -f ./gccDebug/*.so
	rm -f ./gccDebug/*.dll
	rm -f ./gccDebug/*.exe
	rm -f ./gccRelease/*.o
	rm -f ./gccRelease/*.d
	rm -f ./gccRelease/*.a
	rm -f ./gccRelease/*.so
	rm -f ./gccRelease/*.dll
	rm -f ./gccRelease/*.exe

