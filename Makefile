# Builds all the projects in the solution...
.PHONY: all_projects
all_projects: IronIceEditor IronIceEngine Irrlicht 

# Builds project 'IronIceEditor'...
.PHONY: IronIceEditor
IronIceEditor: 
	make --directory="." --file=IronIceEditor.makefile

# Builds project 'IronIceEngine'...
.PHONY: IronIceEngine
IronIceEngine: 
	make --directory="." --file=IronIceEngine.makefile

# Builds project 'Irrlicht'...
.PHONY: Irrlicht
Irrlicht: 
	make --directory="../../source/Irrlicht/" --file=Irrlicht.makefile

# Cleans all projects...
.PHONY: clean
clean:
	make --directory="." --file=IronIceEditor.makefile clean
	make --directory="." --file=IronIceEngine.makefile clean
	make --directory="../../source/Irrlicht/" --file=Irrlicht.makefile clean

