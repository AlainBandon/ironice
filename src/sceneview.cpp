﻿#include "sceneview.h"
#include "engine.h"
#include "audio.h"
#include "utils.h"

#include "md5.h"

// Define some values that we'll use to identify individual GUI controls.
#define GUI_ID_CONTEXT_MENU 101
#define GUI_ID_SAVE_SCENE_FILE_NAME_DLG 103
#define GUI_ID_CONTEXT_MENU_DIALOG_CHOICE 201
#define GUI_ID_CONTEXT_MENU_DIALOG_BG 202
#define GUI_ID_CONTEXT_MENU_DIALOG_BG_KEYS 203
#define GUI_ID_CONTEXT_MENU_SPRITE_LIST 204
#define GUI_ID_CONTEXT_MENU_SPRITE_KEYS 205
#define GUI_ID_CONTEXT_MENU_DIALOG_INPUTS 206
#define GUI_ID_CONTEXT_MENU_SOUNDS 207 // +208 +209
#define GUI_ID_CONTEXT_MENU_DIALOG_TEXTS 210 // +211 +212
#define GUI_ID_MESSAGE_BOX_DIALOG_DELETE 301
#define GUI_ID_MESSAGE_BOX_DIALOG_DELETE_ALL 302
#define GUI_ID_MESSAGE_BOX_SPRITE_DELETE 303
#define GUI_ID_MESSAGE_BOX_SPRITE_DELETE_ALL 304
#define GUI_ID_BUTTON_SPRITE_TAB 600
#define GUI_ID_BUTTON_BKG_TAB 601
#define GUI_ID_BUTTON_DLG_TAB 602
#define GUI_ID_POPUP_DLG 700


// SceneInterface
SceneInterface::SceneInterface() : 
	m_CurrentDialog(NULL),
	m_CurrentSprite(NULL),
	m_SceneNameForQuickSave(L""),
	m_Data(NULL),
	m_IsSetForVitaExport(false),
	m_IsSetForTextsExport(false),
	m_IsSetForTextsImport(false)
{
	m_Scene = new Scene();
	m_Data = new SceneEditorData();
}

SceneInterface::~SceneInterface()
{
	SafeDelete(m_Scene);
	SafeDelete(m_Data);
}

void SceneInterface::AddSpriteToBank(const SmartPointer<AnimatedSprite>& sprite)
{
	m_Scene->AddSpriteToBank(sprite);
	m_Data->AddSprite();
}

bool SceneInterface::RemoveBankSpriteAtIndex(u32 index)
{
	TArray< SmartPointer<AnimatedSprite> >& bank = m_Scene->GetSprites();
	if(index < bank.size())
	{
		SmartPointer<AnimatedSprite> sprite = bank[index];
		bank.erase(index);
		m_Data->RemoveSprite(index);
		m_Scene->CheckSceneSpritesIntegrity();
		return true;
	}
	return false;
}

bool SceneInterface::ClearBankSprites()
{
// 	SafeDelete(m_Scene);
// 	m_Scene = new Scene();
	m_Scene->ClearAllSprites();
	m_Data->ClearSprites();
	m_Scene->CheckSceneSpritesIntegrity();
	SetCurrentDialog(-1);
	SetCurrentSprite(-1);
	return true;
}


void SceneInterface::AddNewDialog(DialogNode* dialog)
{
	m_Scene->AddDialog(dialog);
	dialog->SetIsLoaded(true);
}

bool SceneInterface::RemoveDialogAtIndex(u32 index)
{
	return m_Scene->RemoveDialogAtIndex(index);
}

bool SceneInterface::ClearDialogs()
{
	SafeDelete(m_Scene);
	m_Scene = new Scene();
	SafeDelete(m_Data);
	m_Data = new SceneEditorData();
	return true;
}

bool SceneInterface::LoadDialog(const io::path &name)
{
	if(!m_Scene)
		return false;

	DialogNode* dlg = new DialogNode();
	bool ret = dlg->LoadFromFile((char*)name.c_str());
	if(!ret)
	{
		SafeDelete(dlg);
		return false;
	}
	m_Scene->AddDialog(dlg);
	m_Data->AddDialog(dlg);
	return true;
}

core::stringw SceneInterface::GetNextDialogName()
{
	if(!m_Scene)
		return L"Nouveau Dialogue";

	TArray<DialogNode*>& dialogs = m_Scene->GetDialogs();
	if(!dialogs.empty())
	{
		DialogNode* last = dialogs[dialogs.size()-1];
		if(last)
		{
			const core::stringw& dlgName = m_Data->m_DialogListNames[dialogs.size()-1];
			core::stringw name = dlgName;
			s32 idx = name.size();
			do 
			{
				name = name.subString(0, idx);
				s32 i = name.findLastChar(L"0123456789", 10);
				if(i >= 0)
					idx = i;
			} while (idx == name.size()-1);

			u32 valueSize = dlgName.size() - idx;
			if(valueSize > 0)
			{
				u32 value = wtoi(dlgName.subString(idx, valueSize).c_str(), 10);
				++value;
				core::stringw valStr;
				valStr += value;
				while(valStr.size() < valueSize)// keep the old char number if possible by adding zeroes 
				{
					core::stringw temp = valStr;
					valStr = L"";
					valStr += 0;
					valStr += temp;
				}
				name += valStr;
				return name;
			}									
		}
	}

	return L"Nouveau Dialogue";
}

bool SceneInterface::SaveDialog(const io::path &name)
{
	if(m_CurrentDialog)
	{
		return m_CurrentDialog->SaveToFile((char*)name.c_str());
	}
	return false;
}

bool SceneInterface::XMLExportDialog(const io::path &name)
{
	if(m_CurrentDialog)
	{
		return m_CurrentDialog->SaveToFileXML((char*)name.c_str());
	}
	return false;
}

bool SceneInterface::NewScene()
{
	SafeDelete(m_Scene);
	m_Scene = new Scene();
	SafeDelete(m_Data);
	m_Data = new SceneEditorData();

	// push everything in the cache
	m_Data->PushInCache(m_Scene);

	return true;
}

bool SceneInterface::LoadScene(const io::path &name, bool preloadAll/* = false*/)
{
	SafeDelete(m_Scene);
	m_Scene = new Scene();
	SafeDelete(m_Data);
	m_Data = new SceneEditorData();

	bool ret = m_Scene->LoadSceneFromFile((char*)name.c_str());
	if(!ret)
	{
		// restore the .scn.bak if any
		{
			io::path bakPath = name;
			bakPath += L".bak";
			CopyFile(bakPath.c_str(), name.c_str());
		}// and retry
		ret = m_Scene->LoadSceneFromFile((char*)name.c_str());
	}
	
	if(!ret)
	{
		SafeDelete(m_Scene);
		SafeDelete(m_Data);

		printf("Unable to load scene file %s\n", name.c_str());
		return false;
	}
	if(preloadAll)
	{
		/* TEMP TEMP TEMP*/
		for(u32 i = 0; i < m_Scene->GetDialogs().size(); ++i)
			m_Scene->LoadDialogFromIndex(i);
		for(u32 i = 0; i < m_Scene->GetSprites().size(); ++i)
			m_Scene->LoadSpriteFromIndex(i);
		/* TEMP TEMP TEMP*/
	}
	else
	{
		Engine* engine = Engine::GetInstance();

		// load the preview file if any
		io::path path = name.subString(0, name.size() - 3);
		path += "iie";
		bool forceRecreate = false;
		do
		{
			if(forceRecreate || !m_Data->LoadFromFile((char*)path.c_str()))
			{
				forceRecreate = false;
				printf("Preview gen : start\n");
				// preview loading failed : load everything to generate previews
				for(u32 i = 0; i < m_Scene->GetDialogs().size(); ++i)
				{
					DialogNode* dlg = m_Scene->GetDialogAtIndex(i);
					if(m_Scene->LoadDialog(dlg))
					{
						// add the dialog name
						m_Data->m_DialogListNames.push_back(dlg->GetDialogName());
						m_Data->m_DialogsMD5.push_back(dlg->GetMD5()._raw);

						// check for new sprite previews
						for(u32 i=0; i<m_Scene->GetSprites().size(); ++i)
						{
							SmartPointer<AnimatedSprite> sprite = m_Scene->GetSprites()[i];
							video::ITexture* tex = NULL;
							if(i >= m_Data->m_SpriteBankPreviews.size())
							{
								m_Data->AddSprite();
							}
							if(!m_Data->m_SpriteBankPreviews[i] && sprite.IsValid())
							{
								Texture* tex = sprite->RenderPreview();
								m_Data->m_SpriteBankPreviews[i] = ResizeTexture(tex, Dimension2(VIEW_ANIM_BANK_SIZE,VIEW_ANIM_BANK_SIZE));
								engine->GetDriver()->removeTexture(tex);
								m_Data->m_SpriteBankPreviews[i]->grab();// get unique ownership
								engine->GetDriver()->removeTexture(m_Data->m_SpriteBankPreviews[i]);

								m_Data->m_SpritesMD5[i] = sprite->GetMD5()._raw;
							}
						}
						dlg->UnLoad();
						m_Scene->FlushSoundsAlive();
						m_Scene->FlushSpritesBank();
					}
					else
					{
						printf("Preview gen : unable to load dialog file %d\n", i);
						m_Data->m_DialogListNames.push_back(L"");
						m_Data->m_DialogsMD5.push_back(MD5::MD5Raw());
					}
				}
				// check for sprites not used in any dialog
				for(u32 i=0; i<m_Scene->GetSprites().size(); ++i)
				{
					video::ITexture* tex = NULL;
					if(i >= m_Data->m_SpriteBankPreviews.size())
					{
						m_Data->AddSprite();
					}
					if(!m_Data->m_SpriteBankPreviews[i])
					{
						if(!m_Scene->LoadSpriteFromIndex(i))
						{
							printf("Preview gen : unable to load spr file %d\n", i);
						}
						SmartPointer<AnimatedSprite> sprite = m_Scene->GetSprites()[i];
						//assert(sprite.IsValid());
						if(sprite.IsValid())
						{
							Texture* tex = sprite->RenderPreview();
							m_Data->m_SpriteBankPreviews[i] = ResizeTexture(tex, Dimension2(VIEW_ANIM_BANK_SIZE,VIEW_ANIM_BANK_SIZE));
							engine->GetDriver()->removeTexture(tex);
							m_Data->m_SpriteBankPreviews[i]->grab();
							engine->GetDriver()->removeTexture(m_Data->m_SpriteBankPreviews[i]);

							m_Data->m_SpritesMD5[i] = sprite->GetMD5()._raw;
						}
						sprite = NULL;
						m_Scene->FlushSpritesBank();
					}
				}

				m_Data->SaveToFile((char*)path.c_str());
				printf("Preview gen : end\n");
			}
			else
			{
				if(m_Data->m_DialogsMD5.size() != m_Scene->GetDialogs().size() || m_Data->m_SpritesMD5.size() != m_Scene->GetSprites().size())
				{
					forceRecreate = true;
					SafeDelete(m_Data);
					m_Data = new SceneEditorData();
					printf("scene.iie not up to date... something strange happened during last save !\n");
				}
			}
		} while(forceRecreate);
	}
	assert(m_Data->m_DialogsMD5.size() == m_Scene->GetDialogs().size());
	assert(m_Data->m_SpritesMD5.size() == m_Scene->GetSprites().size());
	// push everything in the cache
	m_Data->PushInCache(m_Scene);

	m_CurrentDialog = NULL;
	m_CurrentSprite = NULL;
	
	return ret;
}

u32 SceneInterface::GetDialogVMemoryUsage(DialogNode* dlg, bool evaluateSprites/* = true*/)
{
	if(!m_Scene)
		return 0;

	u32 size = 0;
	if(dlg && dlg->GetIsLoaded())
	{
		Texture* tex = dlg->GetImageScene()->m_Image;
		if(tex)
		{
			size += tex->getSize().Width * tex->getSize().Height * 4;
		}
	}

	if(dlg && evaluateSprites)
	{
		for(u32 i = 0; i < m_Scene->GetSprites().size(); ++i)
		{
			SmartPointer<AnimatedSprite> spr = m_Scene->GetSpriteAtIndex(i);
			if(!spr.IsValid())
				continue;
			if(dlg->GetSprites().linear_search(spr) < 0)
				continue;
			for(u32 j = 0; j < spr->GetDataImages().size(); ++j)
			{
				Texture* tex = spr->GetDataImages()[j];
				if(tex)
				{
					size += tex->getSize().Width * tex->getSize().Height * 4;
				}
			}
		}
	}

	return size;
}

bool SceneInterface::ExportSceneForVita(const io::path &name)
{
	Engine* engine = Engine::GetInstance();

	// eventually save the current dialog if modified
	m_CurrentDialog = NULL;
	UpdateLoading();
	m_Scene->FlushSoundsAlive();
	m_Scene->FlushSpritesBank();

	// copy all files from the cache now up to date to the new dir
	bool ret = m_Scene->SaveSceneToFile((char*)name.c_str());

	io::path oldPath = m_Scene->GetScenePath();
	io::path newPath = name.subString(0, name.findLastChar("/")+1).c_str();
	m_Scene->SetScenePath(newPath);
	const bool logVMemoryUsage = true;
	if(logVMemoryUsage)
	{
		FILE* output = fopen((newPath + core::stringc("vmem.txt")).c_str(), "w");
		if(output)
		{
			fprintf(output, "VMemory usage : \n\n");
			fclose(output);
		}
	}

	for(u32 i = 0; i < m_Scene->GetDialogs().size(); ++i)
	{
		DialogNode* dlg = m_Scene->GetDialogAtIndex(i);

		if(m_Scene->LoadDialogFromIndex(i, true))
		{
			ret &= m_Scene->SaveDialogFromIndex(m_Scene->GetScenePath().c_str(), i);

			u32 dlgVMemoryUsage = GetDialogVMemoryUsage(dlg);
			if(logVMemoryUsage)
			{
				FILE* output = fopen((newPath + core::stringc("vmem.txt")).c_str(), "a");
				if(output)
				{
					u32 mem = dlgVMemoryUsage;
					u32 memMo = (mem/1024/1024);
					u32 memKo = (mem/1024 - memMo*1024);
					u32 memo = (mem - (mem/1024)*1024);
					fprintf(output, "Dialog %04d - \"%ls\" : \n%dMB %dKB %dB (%dB) \n\n", 
						i, dlg->GetDialogName().c_str(), memMo, memKo, memo, mem);
					fclose(output);
				}
			}

			dlg->UnLoad();
			m_Scene->FlushSoundsAlive();
			m_Scene->FlushSpritesBank();
		}
		else
		{
			printf("Vita export : unable to load dialog file %d\n", i);
			ret = false;
		}
	}

	m_Scene->SetScenePath(oldPath);

	return ret;
}

bool SceneInterface::ExportSceneDialogsTexts(const io::path &name)
{
	printf("Texts export : \n");
	Engine* engine = Engine::GetInstance();

	bool ret = true;

	io::path path = m_Scene->GetScenePath();
	m_Scene->SetScenePath(name.subString(0, name.findLastChar("/")+1).c_str());

	// create the output files
	FILE* outputs[Engine::LS_MAX];
	core::stringc outputNames[Engine::LS_MAX];
	const char* const names[Engine::LS_MAX] = {"texts_fr.txt", "texts_en.txt", "texts_de.txt", "texts_es.txt"};
	for(u32 i = 0; i < Engine::LS_MAX; ++i)
	{
		outputNames[i] = m_Scene->GetScenePath();
		outputNames[i] += names[i];
		outputs[i] = fopen(outputNames[i].c_str(), "w,ccs=UTF-8");
		if(outputs[i])
			fclose(outputs[i]);
	}
	

	for(u32 i = 0; i < m_Scene->GetDialogs().size(); ++i)
	{
		DialogNode* dlg = m_Scene->GetDialogAtIndex(i);

		if(dlg)
		{
			// just load the dialog and nothing else
			char fileName[1024];
			sprintf(fileName, "%sdlg/%04d.dlg", m_Scene->GetScenePath().c_str(), i);
			if(dlg->LoadFromFile(fileName))
			{
				for(u32 i = 0; i < Engine::LS_MAX; ++i)
				{
					outputs[i] = fopen(outputNames[i].c_str(), "a,ccs=UTF-8");

					// add content
					{
						core::stringw content;
						content += L"[dialog = ";
						content += dlg->GetDialogName();
						content += L"]\n";

						content += L"[header]\n";
						if(dlg->GetDialogHeader(i) != L"")
							content += dlg->GetDialogHeader(i);
						content += L"\n[/header]\n\n";

						content += L"[text]\n";
						if(dlg->GetDialogText(i) != L"")
							content += dlg->GetDialogText(i);
						content += L"\n[/text]\n\n";

						if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						{
							for(u32 j = 0; j < dlg->GetConditionalTexts(i).size(); ++j)
							{
								content += L"[condition]\n";
								if(dlg->GetConditionalTexts(i)[j] != L"")
									content += dlg->GetConditionalTexts(i)[j];
								content += L"\n[/condition]\n";
							}
						}

						content += L"[/dialog]\n\n\n\n";

						fwrite(content.c_str(), sizeof(wchar_t), content.size(), outputs[i]);
					}

					if(outputs[i])
						fclose(outputs[i]);
				}

				dlg->UnLoad();
			}
			else
			{
				printf("Texts export : unable to load dialog file %d\n", i);
				ret = false;
			}
		}
		else
		{
			printf("Texts export : unable to load dialog file %d\n", i);
			ret = false;
		}
	}

	m_Scene->SetScenePath(path);

	printf("Texts export done !\n");

	return ret;
}

struct DialogTextParsing
{
	core::stringw m_DialogName;// the name of the dialog (used in editor only)
	core::stringw m_DialogHeader[Engine::LS_MAX];// a dialog text header (in all languages) (used generally for the name of the character speaking)
	core::stringw m_DialogText[Engine::LS_MAX];// a dialog text content (in all languages)
	TArray<core::stringw> m_ConditionsText[Engine::LS_MAX];// must be the same size as the child nodes' array if multi valid mode

	bool operator==(const DialogTextParsing& other) const
	{
		return (m_DialogName == other.m_DialogName);
	}

	bool operator!=(const DialogTextParsing& other) const
	{
		return (m_DialogName != other.m_DialogName);
	}
};
bool SceneInterface::ImportSceneDialogsTexts(const io::path &name)
{
	printf("Texts import :\n");
	Engine* engine = Engine::GetInstance();

	bool ret = true;

	io::path path = m_Scene->GetScenePath();
	m_Scene->SetScenePath(name.subString(0, name.findLastChar("/")+1).c_str());

	TArray<DialogTextParsing> parsing;

	FILE* inputs[Engine::LS_MAX];
	core::stringc inputNames[Engine::LS_MAX];
	const char* const names[Engine::LS_MAX] = {"texts_fr.txt", "texts_en.txt", "texts_de.txt", "texts_es.txt"};
	for(u32 i = 0; i < Engine::LS_MAX; ++i)
	{
		inputNames[i] = m_Scene->GetScenePath();
		inputNames[i] += names[i];
		inputs[i] = fopen(inputNames[i].c_str(), "r,ccs=UTF-8");

		if(!inputs[i])
			continue;

		DialogTextParsing* currentParsing = NULL;
		wchar_t line[2048];
		u32 curCond = 0;
		u32 step = 0;
		bool isReadingContent = false;
		core::stringw str1stPart;
		core::stringw str2ndPart;
		core::stringw str = L"";
		while (fgetws(line, 2048, inputs[i]) != NULL) 
		{
			str += line;
			bool nextLine = false;
			while(!nextLine)
			{
				s32 start = str.findFirstChar(L"[");
				s32 end = str.findFirstChar(L"]");
				if(end >= 0)// found the end : we have a full entry start
				{
					str1stPart = str.subString(start, end-start, true);
					// balise end
					if(str1stPart.find(L"/dialog", 0) > 0)
					{
						curCond = 0;
						currentParsing = NULL;
					}
					else if(str1stPart.find(L"/header", 0) > 0)
					{
						s32 startContent = 0;
						s32 endContent = start-1;
						// trim one break at start if any
						if (str[startContent] == L'\r') // Mac or Windows breaks
						{
							++startContent;
							if (str[startContent] == L'\n') // Windows breaks
								++startContent;
						}
						else if (str[startContent] == L'\n') // Unix breaks
						{
							++startContent;
						}
						// trim one break at end if any
						if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
						{
							--endContent;
							if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
								--endContent;
						}
						else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
						{
							--endContent;
						}

						currentParsing->m_DialogHeader[i] = str.subString(startContent, endContent);

						isReadingContent = false;
					}
					else if(str1stPart.find(L"/text", 0) > 0)
					{
						s32 startContent = 0;
						s32 endContent = start-1;
						// trim one break at start if any
						if (str[startContent] == L'\r') // Mac or Windows breaks
						{
							++startContent;
							if (str[startContent] == L'\n') // Windows breaks
								++startContent;
						}
						else if (str[startContent] == L'\n') // Unix breaks
						{
							++startContent;
						}
						// trim one break at end if any
						if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
						{
							--endContent;
							if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
								--endContent;
						}
						else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
						{
							--endContent;
						}

						currentParsing->m_DialogText[i] = str.subString(startContent, endContent);
						
						isReadingContent = false;
					}
					else if(str1stPart.find(L"/condition", 0) > 0)
					{
						s32 startContent = 0;
						s32 endContent = start-1;
						// trim one break at start if any
						if (str[startContent] == L'\r') // Mac or Windows breaks
						{
							++startContent;
							if (str[startContent] == L'\n') // Windows breaks
								++startContent;
						}
						else if (str[startContent] == L'\n') // Unix breaks
						{
							++startContent;
						}
						// trim one break at end if any
						if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
						{
							--endContent;
							if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
								--endContent;
						}
						else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
						{
							--endContent;
						}

						if(curCond < currentParsing->m_ConditionsText[i].size())
							currentParsing->m_ConditionsText[i][curCond] = str.subString(startContent, endContent);
						else
							currentParsing->m_ConditionsText[i].push_back(str.subString(startContent, endContent));

						++curCond;
						isReadingContent = false;
					}
					// balise start
					else if(str1stPart.find(L"dialog", 0) > 0)
					{
						assert(!currentParsing);

						if(Get2ndPart(str, start, end, str2ndPart))
						{
							DialogTextParsing dummy;
							dummy.m_DialogName = str2ndPart;
							s32 id = parsing.linear_search(dummy);
							if(id >= 0)
							{
								currentParsing = &parsing[id];
							}
							else
							{
								parsing.push_back(dummy);
								currentParsing = &parsing[parsing.size()-1];
							}
						}
						else
						{
							assert(0);// we must have a name
						}
					}
					else if(str1stPart.find(L"header", 0) > 0)
					{
						assert(currentParsing);
						
						if(Get2ndPart(str, start, end, str2ndPart))
						{
							// useless content
						}
						isReadingContent = true;
					}
					else if(str1stPart.find(L"text", 0) > 0)
					{
						assert(currentParsing);

						if(Get2ndPart(str, start, end, str2ndPart))
						{
							// useless content
						}
						isReadingContent = true;
					}
					else if(str1stPart.find(L"condition", 0) > 0)
					{
						assert(currentParsing);

						if(Get2ndPart(str, start, end, str2ndPart))
						{
							// useless content
						}
						isReadingContent = true;
					}

					str = str.subString(end+1, str.size()-(end+1));// next iteration
				}
				else if(start >= 0 || isReadingContent) // not completed line : wait for adding next line to the previous result
				{
					nextLine = true;
				}
				else// not a content : discard
				{
					str = L"";
					nextLine = true;
				}
			}
		}
	}


	for(u32 i = 0; i < m_Scene->GetDialogs().size(); ++i)
	{
		DialogNode* dlg = m_Scene->GetDialogAtIndex(i);

		if(dlg)
		{
			DialogTextParsing dummy;
			dummy.m_DialogName = m_Data->m_DialogListNames[i];
			s32 id = parsing.linear_search(dummy);
			if(id >= 0)// something about this dialog has been parsed !
			{
				const DialogTextParsing& currentParsing = parsing[id];
				
				// just load the dialog and nothing else
				char fileName[1024];
				sprintf(fileName, "%sdlg/%04d.dlg", m_Scene->GetScenePath().c_str(), i);
				if(dlg->LoadFromFile(fileName))
				{
					for(u32 j = 0; j < Engine::LS_MAX; ++j)
					{
						dlg->SetDialogHeader(currentParsing.m_DialogHeader[j], j);
						dlg->SetDialogText(currentParsing.m_DialogText[j], j);
						if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						{
							for(u32 k = 0; k < currentParsing.m_ConditionsText[j].size(); ++k)
							{
								if(k < dlg->GetConditionalTexts(j).size())
								{
									dlg->GetConditionalTexts(j)[k] = currentParsing.m_ConditionsText[j][k];
								}
							}
						}
					}

					sprintf(fileName, "%sdlg/%04d.dlg", engine->GetGamePath("temp/").c_str(), i);
					dlg->SaveToFile(fileName);					
					// save it in /temp before unloading it !
					dlg->UnLoad();
				}
				else
				{
					printf("Texts import : unable to load dialog file %d\n", i);
					ret = false;
				}
			}
		}
	}

	m_Scene->SetScenePath(path);

	printf("Texts import done !\n");

	return ret;
}

void SceneInterface::LoadResourcePath(ResourcePathType type)
{
	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	if(!m_ResourcePath[type].empty())
		fs->changeWorkingDirectoryTo(m_ResourcePath[type]);
}

void SceneInterface::SaveResourcePath(ResourcePathType type, io::path path/* = ""*/)
{
	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	m_ResourcePath[type] = path.empty()? fs->getWorkingDirectory() : path;
}

bool SceneInterface::UpdateLoading()
{
	Engine* engine = Engine::GetInstance();

	Scene* scene = GetScene();
	DialogNode* dlg = GetCurrentDialog();
	//if(dlg)
	{
		u32 storeMD5DlgID = 0xffffffff;
		// unload all others
		for(u32 i = 0; i < scene->GetDialogs().size(); ++i)
		{
			DialogNode* curDlg = scene->GetDialogAtIndex(i);
			if(curDlg != dlg)
			{
				if(curDlg->GetIsLoaded())
				{
					MD5 md5 = curDlg->GetMD5();
					printf("unloading dialog %04d : md5 %s\n", i, md5.digestChars);
					if(md5._raw != m_Data->m_DialogsMD5[i])
					{
						printf("Dialog %04d has been modified : saving it before unloading\n", i);
						irr::io::IFileSystem *fs = engine->GetDevice()->getFileSystem();
						io::path path = fs->getWorkingDirectory();
						scene->SaveDialogFromIndex(engine->GetGamePath("temp/").c_str(), i);
						bool ret = fs->changeWorkingDirectoryTo(path);
					}

					// check sprites loaded
					{
						irr::io::IFileSystem *fs = engine->GetDevice()->getFileSystem();
						io::path path = fs->getWorkingDirectory();
						for(u32 j = 0; j < scene->GetSprites().size(); ++j)
						{
							const SmartPointer<AnimatedSprite>& spr = scene->GetSpriteAtIndex(j);
							if(!spr.IsValid())
								continue;

							MD5 md5 = spr->GetMD5();
							if(md5._raw != m_Data->m_SpritesMD5[j])
							{
								printf("Sprite %04d has been modified : saving it before unloading\n", j);
								scene->SaveSpriteFromIndex(engine->GetGamePath("temp/").c_str(), j);
							}
						}
						bool ret = fs->changeWorkingDirectoryTo(path);
					}

					curDlg->UnLoad();
				}
			}
			else if(dlg && !dlg->GetIsLoaded())
			{
				storeMD5DlgID = i;
			}
		}
		// loading check
		if(dlg && !dlg->GetIsLoaded())
		{
			TArray<bool> sprLoaded;
			// store the sprites still loaded (and potentially modified)
			for(u32 i = 0; i < m_Scene->GetSprites().size(); ++i)
			{
				sprLoaded.push_back(m_Scene->GetSpriteAtIndex(i).IsValid());
			}

			// try to load from the cache
			if(storeMD5DlgID != 0xffffffff)
			{
				dlg->SetID(storeMD5DlgID);// need a correct id for loading

				// try to load it from the cache
				io::path path = scene->GetScenePath();
				scene->SetScenePath(engine->GetGamePath("temp/").c_str());
				if(!scene->LoadDialog(dlg))
				{
					printf("Error : dialog %04d file not loadable...\n", storeMD5DlgID);
					// what to do... the file is corrupted but probably the link between this one and others are still correct... set it as loaded
					dlg->SetIsLoaded(true);
				}
				scene->SetScenePath(path);
			}
			// update the md5 of the dialog
			MD5 md5 = dlg->GetMD5();
			printf("loading dialog %04d : md5 %s\n", storeMD5DlgID, md5.digestChars);
			m_Data->m_DialogsMD5[storeMD5DlgID] = md5._raw;

			// Update the md5 of the sprites newly loaded
			for(u32 i = 0; i < m_Scene->GetSprites().size(); ++i)
			{
				if(sprLoaded[i])// skip spr already loaded before
					continue;
				const SmartPointer<AnimatedSprite>& spr = m_Scene->GetSpriteAtIndex(i);
				if(!spr.IsValid())
					continue;
				MD5 md5 = spr->GetMD5();
				m_Data->m_SpritesMD5[i] = md5._raw;// update the md5 (cause we can't have modified it for now)
			}
			sprLoaded.clear();

			return true;
		}
	}

	return false;
}

bool SceneInterface::SaveScene(const io::path &name)
{
	if(m_Scene)
	{ 
		if(m_IsSetForVitaExport)
		{
			bool ret = ExportSceneForVita(name);

			m_IsSetForVitaExport = false;
			return ret;
		}
		else
		{
			SetSceneNameForQuickSave(name);

			// eventually save the current dialog if modified
			DialogNode* last = m_CurrentDialog;
			m_CurrentDialog = NULL;
			UpdateLoading();

			// copy all files from the cache now up to date to dest dir
			bool ret = m_Scene->SaveSceneToFile((char*)name.c_str());

			io::path path = name.subString(0, name.size() - 3);
			path += "iie";
			m_Data->SaveToFile((char*)path.c_str());

			if(m_IsSetForTextsImport)
			{
				ImportSceneDialogsTexts(name);
				m_IsSetForTextsImport = false;
			}
			else if(m_IsSetForTextsExport)
			{
				ExportSceneDialogsTexts(name);
				m_IsSetForTextsExport = false;
			}

			m_CurrentDialog = last;
			UpdateLoading();

			return ret;
		}
	}
	return false;
}

static void ComputeSpriteBank(gui::IGUIListBox* ImgList, TArray<Texture*>& textures, const Dimension2& dim, core::stringc bankName)
{
	Engine* engine = Engine::GetInstance();
	gui::IGUIEnvironment* gui = engine->GetGui();
	video::IVideoDriver* driver = engine->GetDriver();
	// browse the archives for maps
	if ( ImgList )
	{
		ImgList->clear();

		gui::IGUISpriteBank *bank = gui->getSpriteBank(bankName);
		if ( !bank )
			bank = gui->addEmptySpriteBank(bankName);

		gui::SGUISprite sprite;
		gui::SGUISpriteFrame frame;
		Rectangle2 r;

		// remove textures from the engine because we want to delete them now !
		for(u32 i = 0; i < bank->getTextureCount(); ++i)
		{
			Texture* tex = bank->getTexture(i);
			driver->removeTexture(tex);
		}
		bank->clear();
		ImgList->setSpriteBank ( bank );

		u32 g = 0;
		core::stringw s;

		for ( u32 i=0; i< textures.size(); ++i)
		{
			Texture *tex = textures[i];
			if(tex->getSize() != dim)
				tex = ResizeTexture(textures[i], dim);

			bank->setTexture ( g, tex );

			r.LowerRightCorner.X = dim.Width;
			r.LowerRightCorner.Y = dim.Height;
			ImgList->setItemHeight ( r.LowerRightCorner.Y + 4 );
			frame.rectNumber = bank->getPositions().size();
			frame.textureNumber = g;

			bank->getPositions().push_back(r);

			sprite.Frames.set_used ( 0 );
			sprite.Frames.push_back(frame);
			sprite.frameTime = 0;
			bank->getSprites().push_back(sprite);

			ImgList->addItem ( s.c_str (), g );
			g += 1;
		}

		ImgList->setSelected ( -1 );
		gui::IGUIScrollBar * bar = (gui::IGUIScrollBar*)ImgList->getElementFromId( 0 );
		if ( bar )
			bar->setPos ( 0 );

	}
}



// SpriteBankListBox
AnimationBankListBox::AnimationBankListBox(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position): 
gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), 
m_Gui(env), 
m_ContextualMenu(NULL), 
m_OpenFileDlg(NULL), 
m_SaveFileDlg(NULL),
m_CurrentlyHovered(false), 
m_SceneInterface(NULL),
m_SpriteToEdit(NULL),
m_SpriteLoadType(SLT_NONE),
m_SceneEditorWindow(NULL),
m_DialogList(NULL)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	m_Title = m_Gui->addStaticText(titletext, Rectangle2(Position2(0, 0), Dimension2(this->getRelativePosition().getWidth(), VIEW_TITLES_HEIGHT)), true, true, this);
	m_ListBox = m_Gui->addListBox(Rectangle2(Position2(0, VIEW_TITLES_HEIGHT), Dimension2(this->getRelativePosition().getWidth(), this->getRelativePosition().getHeight()-VIEW_TITLES_HEIGHT)), this);
	m_ListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
}

AnimationBankListBox::~AnimationBankListBox()
{

}

void AnimationBankListBox::UpdateSpriteBank()
{
	if(m_SceneInterface->GetScene())
	{
		Engine* engine = Engine::GetInstance();
		TArray<Texture*> images;
		for(u32 i=0; i<m_SceneInterface->GetScene()->GetSprites().size(); ++i)
		{
			SmartPointer<AnimatedSprite> sprite = m_SceneInterface->GetScene()->GetSprites()[i];
			video::ITexture* tex = NULL;
			if(m_SceneInterface && m_SceneInterface->m_Data && i < m_SceneInterface->m_Data->m_SpriteBankPreviews.size())
				tex = m_SceneInterface->m_Data->m_SpriteBankPreviews[i];
			if(!tex && sprite.IsValid())
				tex = sprite->RenderPreview();
			images.push_back(tex);
		}
		ComputeSpriteBank(m_ListBox, images, Dimension2(VIEW_ANIM_BANK_SIZE,VIEW_ANIM_BANK_SIZE), "SpriteBank");
		// clean all
		for(u32 i=0; i<images.size(); ++i)
		{
			Texture* tex = images[i];
			if(tex)
				Engine::GetInstance()->GetDriver()->removeTexture(tex);
		}

		// update the interface sprite bank previews
		if(m_SceneInterface && m_SceneInterface->m_Data)
		{
			for(u32 i=0; i<m_SceneInterface->m_Data->m_SpriteBankPreviews.size(); ++i)
			{
				SAFE_UNLOAD(m_SceneInterface->m_Data->m_SpriteBankPreviews[i]);
			}
			m_SceneInterface->m_Data->m_SpriteBankPreviews.clear();
			gui::IGUIEnvironment* gui = engine->GetGui();
			gui::IGUISpriteBank *bank = gui->getSpriteBank("SpriteBank");
			for(u32 i=0; i<bank->getTextureCount(); ++i)
			{
				Texture* tex = bank->getTexture(i);
				tex->grab();
				m_SceneInterface->m_Data->m_SpriteBankPreviews.push_back(tex);
			}
		}
		images.clear();
	}
}

bool AnimationBankListBox::AddSpriteToBank(const io::path &name)
{
	if(name.find(L".spr") < 0)
		return false;

	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
	if(sprite->LoadSpriteFromFile((char*)name.c_str()))
	{
		m_SceneInterface->AddSpriteToBank(sprite);
		UpdateSpriteBank();
		return true;
	}
	return false;
}

bool AnimationBankListBox::ExportSpriteXML(const io::path &name)
{
	s32 index = m_ListBox->getSelected();
	if(index >= 0 && (u32)index < m_ListBox->getItemCount())
	{
		SmartPointer<AnimatedSprite> spr = m_SceneInterface->GetScene()->GetSprites()[index];
		if(!spr.IsValid())
		{
			io::path path = m_SceneInterface->GetScene()->GetScenePath();
			m_SceneInterface->GetScene()->SetScenePath(Engine::GetInstance()->GetGamePath("temp/").c_str());
			bool ret = m_SceneInterface->GetScene()->LoadSpriteFromIndex(index);
			assert(ret);
			m_SceneInterface->GetScene()->SetScenePath(path);
			spr = m_SceneInterface->GetScene()->GetSprites()[index];

			MD5 md5 = spr->GetMD5();
			m_SceneInterface->m_Data->m_SpritesMD5[index] = md5._raw;// update the md5 (cause we can't have modified it for now)
		}

		return spr->SaveSpriteToFileXML(name.c_str());
	}

	return false;
}

bool AnimationBankListBox::OnEvent(const SEvent& event)
{
// 	SEvent evt = event;
// 	printf("SpriteBankListBox\n");
	if (m_CurrentlyHovered && event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
		{
			bool isAnyImage = (m_ListBox->getItemCount() != 0);
			s32 index = m_ListBox->getSelected();
			Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
			gui::IGUIContextMenu* menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
			menu->addItem(L"Ajouter au dialogue (CTRL+A)", 666, isAnyImage && index >= 0);
			menu->addSeparator();
			menu->addItem(L"Nouveau (CTRL+N)", 666, true);
			menu->addItem(L"Importer (CTRL+I)", 666, true);
			menu->addItem(L"Créer depuis une image (CTRL+T)", 666, true);
			menu->addItem(L"Créer depuis un répertoire d'images (CTRL+B)", 666, true);
			menu->addItem(L"Supprimer (SUPPR)", 666, isAnyImage && index >= 0);
			menu->addItem(L"Supprimer tout (CTRL+SUPPR)", 666, isAnyImage);
			menu->addSeparator();
			menu->addItem(L"Editer (CTRL+E)", 666, isAnyImage && index >= 0);
			menu->addItem(L"Exporter en XML", 666, isAnyImage && index >= 0);

			Engine* engine = Engine::GetInstance();
			video::IVideoDriver* driver = engine->GetDriver();

			Dimension2 dim = driver->getScreenSize();
			r = menu->getRelativePosition();
			r.constrainTo(Rectangle2(Position2(0, 0), dim));
			menu->setRelativePosition(r);

			menu->setEventParent(this);
		}
// 		else if(event.MouseInput.Event == EMIE_LMOUSE_DOUBLE_CLICK)
// 		{
// 			LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
// 			if(layer)
// 			{
// 				s32 index = m_ListBox->getSelected();
// 				if(index >= 0 && (u32)index < m_SpriteInterface->GetSprite()->GetDataImages().size())
// 				{
// 					layer->SetImage(m_SpriteInterface->GetSprite()->GetDataImages()[index]);
// 					m_LayerMenu->OnLayersChanged();
// 					m_SceneEditorWindow->OnSpriteChange();
// 				}
// 			}
// 		}
	}
	else if(event.EventType == EET_KEY_INPUT_EVENT)// hotkeys 
	{
		bool isMessageBoxActive = getElementFromId(GUI_ID_MESSAGE_BOX_SPRITE_DELETE, true) ||
			getElementFromId(GUI_ID_MESSAGE_BOX_SPRITE_DELETE_ALL, true);
		if(!event.KeyInput.PressedDown && !isMessageBoxActive)// HACK : USE RELEASE EVENT BECAUSE LISTBOXES ABSORB KEY PRESS EVENTS WITH CTRL PRESSED BUT DOES NOTHING
		{
			if(event.KeyInput.Control)
			{
				if(event.KeyInput.Key == KEY_KEY_A)// add to dialog
				{
					s32 index = m_ListBox->getSelected();
					if(!m_SceneInterface->GetScene()->GetSpriteAtIndex(index).IsValid())
					{
						io::path path = m_SceneInterface->GetScene()->GetScenePath();
						m_SceneInterface->GetScene()->SetScenePath(Engine::GetInstance()->GetGamePath("temp/").c_str());
						bool ret = m_SceneInterface->GetScene()->LoadSpriteFromIndex(index);
						assert(ret);
						m_SceneInterface->GetScene()->SetScenePath(path);
					}
					if(m_SceneInterface->GetScene()->AddSpriteToDialog(m_SceneInterface->GetCurrentDialog(), index))
					{
						m_SceneEditorWindow->UpdateSpriteListBank();
						m_SceneEditorWindow->SetSceneTab(SceneEditorWindow::SCT_Sprites);
					}
				}
				else if(event.KeyInput.Key == KEY_KEY_N)// new sprite
				{
					SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
					m_SceneInterface->AddSpriteToBank(sprite);
					UpdateSpriteBank();
					u32 index = m_SceneInterface->GetScene()->GetSprites().size() - 1;
					printf("Sprite %04d has been created : saving it before unloading\n", index);
					m_SceneInterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);
					m_SpriteToEdit = sprite;
				}
				else if(event.KeyInput.Key == KEY_KEY_T)// new sprite from texture
				{
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sprites);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez une image à utiliser");
					m_SpriteLoadType = SLT_TEXTURE;
				}
				else if(event.KeyInput.Key == KEY_KEY_B)// new sprite from textures directory
				{
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sprites);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un répertoire à utiliser");
					m_SpriteLoadType = SLT_BATCH_TEXTURE;
				}
				else if(event.KeyInput.Key == KEY_KEY_I)// import
				{
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un sprite à importer");
					m_SpriteLoadType = SLT_SPRITE;
				}
				else if(event.KeyInput.Key == KEY_KEY_E)// edit
				{
					s32 index = m_ListBox->getSelected();
					if(index >= 0 && (u32)index < m_ListBox->getItemCount())
					{
						m_SpriteToEdit = m_SceneInterface->GetScene()->GetSprites()[index];
						if(!m_SpriteToEdit.IsValid())
						{
							io::path path = m_SceneInterface->GetScene()->GetScenePath();
							m_SceneInterface->GetScene()->SetScenePath(Engine::GetInstance()->GetGamePath("temp/").c_str());
							bool ret = m_SceneInterface->GetScene()->LoadSpriteFromIndex(index);
							assert(ret);
							m_SceneInterface->GetScene()->SetScenePath(path);
							m_SpriteToEdit = m_SceneInterface->GetScene()->GetSprites()[index];

							MD5 md5 = m_SpriteToEdit->GetMD5();
							m_SceneInterface->m_Data->m_SpritesMD5[index] = md5._raw;// update the md5 (cause we can't have modified it for now)
						}
					}
				}
				else if(event.KeyInput.Key == KEY_DELETE)// delete all
				{
					gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression des sprites", 
						L"Voulez-vous supprimer tous les sprites ?", 
						true, 
						gui::EMBF_YES|gui::EMBF_NO, 
						this, 
						GUI_ID_MESSAGE_BOX_SPRITE_DELETE_ALL);
					mes->setNotClipped(true);
					mes->setRelativePosition(core::position2di(-mes->getRelativePosition().getWidth()+m_ListBox->getRelativePosition().getWidth(),0));
					mes->setDraggable(false);
				}
			}
			else
			{
				if(event.KeyInput.Key == KEY_DELETE)// delete 
				{
					s32 index = m_ListBox->getSelected();
					if(index >= 0 && (u32)index < m_ListBox->getItemCount())
					{
#ifdef SNOWLIGHT_PROD_REQUEST
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							m_SceneInterface->RemoveBankSpriteAtIndex(index);
							m_ListBox->removeItem(index);
							UpdateSpriteBank();
							if((u32)index >= m_ListBox->getItemCount())
								index = m_ListBox->getItemCount()-1;
							m_ListBox->setSelected(index);
							m_SceneInterface->SetCurrentSprite(index);
						}
#else
						// TEMP : USE THIS UGLY MESSAGE BOX...
						core::stringw str = L"Voulez-vous supprimer ce sprite ?";
						gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression de sprite", 
							str.c_str(), 
							true, 
							gui::EMBF_YES|gui::EMBF_NO, 
							this, 
							GUI_ID_MESSAGE_BOX_SPRITE_DELETE);
						mes->setNotClipped(true);
						mes->setRelativePosition(core::position2di(-mes->getRelativePosition().getWidth()+m_ListBox->getRelativePosition().getWidth(),0));
						mes->setDraggable(false);
#endif
					}
				}
			}
		}
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}
		else if(event.GUIEvent.EventType == gui::EGET_MESSAGEBOX_YES)
		{
			if(event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_SPRITE_DELETE)
			{
				s32 index = m_ListBox->getSelected();
				if(index >= 0 && (u32)index < m_ListBox->getItemCount())
				{
					m_SceneInterface->RemoveBankSpriteAtIndex(index);
					m_ListBox->removeItem(index);
					UpdateSpriteBank();
					if((u32)index >= m_ListBox->getItemCount())
						index = m_ListBox->getItemCount()-1;
					m_ListBox->setSelected(index);
					m_SceneInterface->SetCurrentSprite(index);
				}

				m_Gui->setFocus(m_ListBox);
			}
			else if(event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_SPRITE_DELETE_ALL)
			{
				m_SceneInterface->ClearBankSprites();
				m_ListBox->clear();
				UpdateSpriteBank();
				m_SceneEditorWindow->OnSceneChange();
				m_DialogList->OnSceneChange();

				m_Gui->setFocus(m_ListBox);
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_MESSAGEBOX_NO || event.GUIEvent.EventType == gui::EGET_MESSAGEBOX_CANCEL)
		{
			if(event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_SPRITE_DELETE || 
				event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_SPRITE_DELETE_ALL)
			{
				m_Gui->setFocus(m_ListBox);
			}
		}

		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// ajouter au dialogue
					{
						s32 index = m_ListBox->getSelected();
						if(!m_SceneInterface->GetScene()->GetSpriteAtIndex(index).IsValid())
						{
							io::path path = m_SceneInterface->GetScene()->GetScenePath();
							m_SceneInterface->GetScene()->SetScenePath(Engine::GetInstance()->GetGamePath("temp/").c_str());
							bool ret = m_SceneInterface->GetScene()->LoadSpriteFromIndex(index);
							assert(ret);
							m_SceneInterface->GetScene()->SetScenePath(path);
						}
						if(m_SceneInterface->GetScene()->AddSpriteToDialog(m_SceneInterface->GetCurrentDialog(), index))
						{
							m_SceneEditorWindow->UpdateSpriteListBank();
							m_SceneEditorWindow->SetSceneTab(SceneEditorWindow::SCT_Sprites);
						}
					}
					break;
				case 2:// Nouveau
					{
						SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
						m_SceneInterface->AddSpriteToBank(sprite);
						UpdateSpriteBank();
						u32 index = m_SceneInterface->GetScene()->GetSprites().size() - 1;
						printf("Sprite %04d has been created : saving it before unloading\n", index);
						m_SceneInterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);
						m_SpriteToEdit = sprite;
					}
 					break;
				case 3:// Importer
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un sprite à importer");
					m_SpriteLoadType = SLT_SPRITE;
					break;
				case 4:// Créer depuis une image
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sprites);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez une image à utiliser");
					m_SpriteLoadType = SLT_TEXTURE;
					break;
				case 5:// Créer depuis un répertoire d'images
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sprites);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un répertoire à utiliser");
					m_SpriteLoadType = SLT_BATCH_TEXTURE;
					break;
				case 6:// supprimer
					{
#ifdef SNOWLIGHT_PROD_REQUEST
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							m_SceneInterface->RemoveBankSpriteAtIndex(index);
							m_ListBox->removeItem(index);
							UpdateSpriteBank();
							if((u32)index >= m_ListBox->getItemCount())
								index = m_ListBox->getItemCount()-1;
							m_ListBox->setSelected(index);
							m_SceneInterface->SetCurrentSprite(index);
						}
#else
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							// TEMP : USE THIS UGLY MESSAGE BOX...
							core::stringw str = L"Voulez-vous supprimer ce sprite ?";
							gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression de sprite", 
								str.c_str(), 
								true, 
								gui::EMBF_YES|gui::EMBF_NO, 
								this, 
								GUI_ID_MESSAGE_BOX_SPRITE_DELETE);
							mes->setNotClipped(true);
							mes->setRelativePosition(core::position2di(-mes->getRelativePosition().getWidth()+m_ListBox->getRelativePosition().getWidth(),0));
							mes->setDraggable(false);
						}
#endif
					}
					break;
				case 7:// supprimer tout
					{
						gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression des sprites", 
							L"Voulez-vous supprimer tous les sprites ?", 
							true, 
							gui::EMBF_YES|gui::EMBF_NO, 
							this, 
							GUI_ID_MESSAGE_BOX_SPRITE_DELETE_ALL);
						mes->setNotClipped(true);
						mes->setRelativePosition(core::position2di(-mes->getRelativePosition().getWidth()+m_ListBox->getRelativePosition().getWidth(),0));
						mes->setDraggable(false);
					}
					break;
				case 9:// editer
					{
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							m_SpriteToEdit = m_SceneInterface->GetScene()->GetSprites()[index];
							if(!m_SpriteToEdit.IsValid())
							{
								io::path path = m_SceneInterface->GetScene()->GetScenePath();
								m_SceneInterface->GetScene()->SetScenePath(Engine::GetInstance()->GetGamePath("temp/").c_str());
								bool ret = m_SceneInterface->GetScene()->LoadSpriteFromIndex(index);
								assert(ret);
								m_SceneInterface->GetScene()->SetScenePath(path);
								m_SpriteToEdit = m_SceneInterface->GetScene()->GetSprites()[index];

								MD5 md5 = m_SpriteToEdit->GetMD5();
								m_SceneInterface->m_Data->m_SpritesMD5[index] = md5._raw;// update the md5 (cause we can't have modified it for now)
							}
						}
					}
					break;
				case 10:// XML Export
					{
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							m_SaveFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un répertoire à utiliser");
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void AnimationBankListBox::OnSceneChange()
{
	m_ListBox->clear();
	UpdateSpriteBank();
	m_ListBox->setSelected(-1);
}


// DialogListBox
DialogListBox::DialogListBox(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position): 
gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), 
m_Gui(env), 
m_ContextualMenu(NULL), 
m_OpenFileDlg(NULL), 
m_CurrentlyHovered(false),
m_DialogFileType(DFT_NONE)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	m_Title = m_Gui->addStaticText(titletext, Rectangle2(Position2(0, 0), Dimension2(this->getRelativePosition().getWidth(), VIEW_TITLES_HEIGHT)), true, true, this);
	m_ListBox = m_Gui->addListBox(Rectangle2(Position2(0, VIEW_TITLES_HEIGHT), Dimension2(this->getRelativePosition().getWidth(), this->getRelativePosition().getHeight()-VIEW_TITLES_HEIGHT)), this);
	m_ListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
}

DialogListBox::~DialogListBox()
{

}

bool DialogListBox::AddDialog(const io::path &name)
{
	if(name.find(L".dlg") < 0)
		return false;

	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	bool ret = m_SceneInterface->LoadDialog(name);
	OnSceneChange();

	return ret;
}

bool DialogListBox::OnEvent(const SEvent& event)
{
	// 	SEvent evt = event;
	// 	printf("SpriteBankListBox\n");
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(m_CurrentlyHovered)
		{
			if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
			{
				s32 itemCount = m_ListBox->getItemCount();
				bool isAnyItem = (itemCount != 0);
				s32 index = m_ListBox->getSelected();
				Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
				gui::IGUIContextMenu* menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
				menu->addItem(L"Nouveau (CTRL+N)", 666, true);
				menu->addItem(L"Importer (CTRL+I)", 666, true);
				menu->addItem(L"Exporter (CTRL+E)", 666, isAnyItem && index >= 0);
				menu->addItem(L"Supprimer (SUPPR)", 666, isAnyItem && index >= 0);
				menu->addItem(L"Supprimer tout (CTRL+SUPPR)", 666, isAnyItem);
				menu->addItem(L"Dupliquer (CTRL+D)", 666, isAnyItem && index >= 0);
				menu->addSeparator();
				menu->addItem(L"Monter (CTRL+Q)", 666, (itemCount >= 2) && (index >= 1));
				menu->addItem(L"Descendre (CTRL+W)", 666, (itemCount >= 2) && (index >= 0)&& (index < itemCount-1));
				menu->addItem(L"Définir comme début (CTRL+B)", 666, isAnyItem && index >= 0);
				menu->addItem(L"Lier automatiquement (CTRL+L)", 666, isAnyItem);
				menu->addItem(L"Exporter en XML", 666, isAnyItem && index >= 0);

				Engine* engine = Engine::GetInstance();
				video::IVideoDriver* driver = engine->GetDriver();

				Dimension2 dim = driver->getScreenSize();
				r = menu->getRelativePosition();
				r.constrainTo(Rectangle2(Position2(0, 0), dim));
				menu->setRelativePosition(r);

				menu->setEventParent(this);
			}
			else if(event.MouseInput.Event == EMIE_LMOUSE_DOUBLE_CLICK && m_SceneInterface->GetCurrentDialog())
			{ 
				core::stringw* name = NULL;
				if(m_SceneInterface->GetCurrentDialog())
					name = m_SceneInterface->GetCurrentDialog()->GetDialogNamePtr();

				Engine* engine = Engine::GetInstance();
				video::IVideoDriver* driver = engine->GetDriver();

				Dimension2 dim = driver->getScreenSize();

				Rectangle2 rect(Position2(event.MouseInput.X, event.MouseInput.Y), Dimension2(256, 80));
				rect.constrainTo(m_Gui->getRootGUIElement()->getAbsoluteClippingRect());
				NameSelectWindow* wnd = new NameSelectWindow(m_Gui, m_Gui->getRootGUIElement(), -1, rect, m_ListBox, m_SceneInterface);
				wnd->drop();
			}
		}
	}
	else if(event.EventType == EET_KEY_INPUT_EVENT)// hotkeys 
	{
		bool isMessageBoxActive = getElementFromId(GUI_ID_MESSAGE_BOX_DIALOG_DELETE, true) ||
			getElementFromId(GUI_ID_MESSAGE_BOX_DIALOG_DELETE_ALL, true);
		if(!event.KeyInput.PressedDown && !isMessageBoxActive)// HACK : USE RELEASE EVENT BECAUSE LISTBOXES ABSORB KEY PRESS EVENTS WITH CTRL PRESSED BUT DOES NOTHING
		{
			if(event.KeyInput.Control)
			{
				if(event.KeyInput.Key == KEY_KEY_N)// new dialog
				{
					DialogNode* dlg = new DialogNode();
					dlg->SetDialogName(m_SceneInterface->GetNextDialogName());
					m_SceneInterface->AddNewDialog(dlg);
					m_SceneInterface->m_Data->AddDialog(dlg);
					m_ListBox->addItem(dlg->GetDialogName().c_str());
					s32 index = m_ListBox->getItemCount()-1;
					m_ListBox->setSelected(m_ListBox->getItemCount()-1);
					m_SceneInterface->SetCurrentDialog(index);
					m_SceneEditorWindow->OnDialogChange();
				}
				else if(event.KeyInput.Key == KEY_KEY_D)// duplicate
				{
					s32 index = m_ListBox->getSelected();
					if(index >= 0 && (u32)index < m_ListBox->getItemCount())
					{
						DialogNode* dlg = new DialogNode(m_SceneInterface->GetScene()->GetDialogs()[index]);
						dlg->SetDialogName(m_SceneInterface->GetNextDialogName());
						m_SceneInterface->AddNewDialog(dlg);
						m_SceneInterface->m_Data->AddDialog(dlg);
						m_ListBox->addItem(dlg->GetDialogName().c_str());
						s32 index = m_ListBox->getItemCount()-1;
						m_ListBox->setSelected(m_ListBox->getItemCount()-1);
						m_SceneInterface->SetCurrentDialog(index);
						m_SceneEditorWindow->OnDialogChange();
					}
				}
				else if(event.KeyInput.Key == KEY_KEY_Q)// up
				{
					s32 itemSelected = m_ListBox->getSelected();
					TArray<DialogNode*>& dialogs = m_SceneInterface->GetScene()->GetDialogs();
					if(itemSelected >= 1 && dialogs.size() >= 2 && (u32)itemSelected < dialogs.size())
					{
						DialogNode* temp = dialogs[itemSelected-1];
						dialogs[itemSelected-1] = dialogs[itemSelected];
						dialogs[itemSelected] = temp;
						dialogs[itemSelected-1]->SetID(itemSelected-1);
						dialogs[itemSelected]->SetID(itemSelected);

						m_ListBox->swapItems(itemSelected, itemSelected-1);
						m_SceneInterface->m_Data->SwapDialogNext(itemSelected-1);
						m_ListBox->setSelected(itemSelected-1);
					}
				}
				else if(event.KeyInput.Key == KEY_KEY_W)// down
				{
					s32 itemSelected = m_ListBox->getSelected();
					TArray<DialogNode*>& dialogs = m_SceneInterface->GetScene()->GetDialogs();
					if(itemSelected >= 0 && dialogs.size() >= 2 && (u32)itemSelected < dialogs.size()-1)
					{
						DialogNode* temp = dialogs[itemSelected+1];
						dialogs[itemSelected+1] = dialogs[itemSelected];
						dialogs[itemSelected] = temp;
						dialogs[itemSelected+1]->SetID(itemSelected+1);
						dialogs[itemSelected]->SetID(itemSelected);

						m_ListBox->swapItems(itemSelected, itemSelected+1);
						m_SceneInterface->m_Data->SwapDialogNext(itemSelected);
						m_ListBox->setSelected(itemSelected+1);
					}
				}
				else if(event.KeyInput.Key == KEY_KEY_I)// import
				{
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un dialogue à importer");
					m_DialogFileType = DFT_IMPORT;
				}
				else if(event.KeyInput.Key == KEY_KEY_E)// export
				{
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un dossier où exporter le dialogue");
					m_DialogFileType = DFT_EXPORT;
				}
				else if(event.KeyInput.Key == KEY_KEY_B)// initial dialog
				{
					m_SceneInterface->GetScene()->SetInitialDialog(m_ListBox->getSelected());
				}
				else if(event.KeyInput.Key == KEY_DELETE)// delete all
				{
					gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression des dialogues", 
						L"Voulez-vous supprimer tous les dialogues ?", 
						true, 
						gui::EMBF_YES|gui::EMBF_NO, 
						this, 
						GUI_ID_MESSAGE_BOX_DIALOG_DELETE_ALL);
					mes->setNotClipped(true);
					mes->setRelativePosition(core::position2di(0,0));
					mes->setDraggable(false);
				}
				else if(event.KeyInput.Key == KEY_KEY_L)// auto link
				{
					TArray<DialogNode*>& dialogs = m_SceneInterface->GetScene()->GetDialogs();
					for(s32 i = 0; i < (s32)dialogs.size()-1; ++i)
					{
						DialogNode* dlg = dialogs[i];
						switch(dlg->GetNodeType())
						{
						case DialogNode::DN_AUTO_VALID:
						case DialogNode::DN_MANUAL_VALID:
						case DialogNode::DN_MULTI_VALID:
							if(dlg->GetNbChildren() == 0)
								dlg->BranchToNode(dialogs[i+1]);
							break;
						case DialogNode::DN_QTE_VALID:
							if(dlg->GetNbChildren() == 0)
							{
								dlg->BranchToNode(dialogs[i+1]);
								dlg->BranchToNode(dialogs[0]);
							}
							break;
						default:
							break;
						}
					}
				}
			}
			else
			{
				if(event.KeyInput.Key == KEY_DELETE)// delete 
				{
					s32 index = m_ListBox->getSelected();
					if(index >= 0 && (u32)index < m_ListBox->getItemCount())
					{
#ifdef SNOWLIGHT_PROD_REQUEST
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							m_SceneInterface->RemoveDialogAtIndex(index);
							m_ListBox->removeItem(index);
							m_SceneInterface->m_Data->RemoveDialog(index);
							if((u32)index >= m_ListBox->getItemCount())
								index = m_ListBox->getItemCount()-1;
							m_ListBox->setSelected(index);
							m_SceneInterface->SetCurrentDialog(index);
							m_SceneEditorWindow->OnDialogChange();
						}
#else
						// TEMP : USE THIS UGLY MESSAGE BOX...
						core::stringw str = L"Voulez-vous supprimer le dialogue ";
						str += m_SceneInterface->GetScene()->GetDialogAtIndex(index)->GetDialogName();
						str += L" ?";
						gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression de dialogue", 
							str.c_str(), 
							true, 
							gui::EMBF_YES|gui::EMBF_NO, 
							this, 
							GUI_ID_MESSAGE_BOX_DIALOG_DELETE);
						mes->setNotClipped(true);
						mes->setRelativePosition(core::position2di(0,0));
						mes->setDraggable(false);
#endif
					}
				}
			}
		}
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}
		else if(event.GUIEvent.EventType == gui::EGET_LISTBOX_CHANGED || event.GUIEvent.EventType == gui::EGET_LISTBOX_SELECTED_AGAIN)
		{
			m_SceneInterface->SetCurrentDialog(m_ListBox->getSelected());
			//m_SceneEditorWindow->OnDialogChange();
		}
		else if(event.GUIEvent.EventType == gui::EGET_MESSAGEBOX_YES)
		{
			if(event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_DIALOG_DELETE)
			{
				s32 index = m_ListBox->getSelected();
				if(index >= 0 && (u32)index < m_ListBox->getItemCount())
				{
					m_SceneInterface->RemoveDialogAtIndex(index);
					m_ListBox->removeItem(index);
					m_SceneInterface->m_Data->RemoveDialog(index);
					if((u32)index >= m_ListBox->getItemCount())
						index = m_ListBox->getItemCount()-1;
					m_ListBox->setSelected(index);
					m_SceneInterface->SetCurrentDialog(index);
					m_SceneEditorWindow->OnDialogChange();
				}

				m_Gui->setFocus(m_ListBox);
			}
			else if(event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_DIALOG_DELETE_ALL)
			{
				m_SceneInterface->ClearDialogs();
				m_ListBox->clear();
				m_SceneInterface->m_Data->ClearDialogs();
				m_ListBox->setSelected(-1);
				m_SceneInterface->SetCurrentDialog(-1);
				m_SceneEditorWindow->OnDialogChange();

				m_Gui->setFocus(m_ListBox);
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_MESSAGEBOX_NO || event.GUIEvent.EventType == gui::EGET_MESSAGEBOX_CANCEL)
		{
			if(event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_DIALOG_DELETE || 
				event.GUIEvent.Caller->getID() == GUI_ID_MESSAGE_BOX_DIALOG_DELETE_ALL)
			{
				m_Gui->setFocus(m_ListBox);
			}
		}

		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// Nouveau
					{
						DialogNode* dlg = new DialogNode();
						dlg->SetDialogName(m_SceneInterface->GetNextDialogName());
						m_SceneInterface->AddNewDialog(dlg);
						m_SceneInterface->m_Data->AddDialog(dlg);
						m_ListBox->addItem(dlg->GetDialogName().c_str());
						s32 index = m_ListBox->getItemCount()-1;
						m_ListBox->setSelected(m_ListBox->getItemCount()-1);
						m_SceneInterface->SetCurrentDialog(index);
						m_SceneEditorWindow->OnDialogChange();
					}
					break;
				case 1:// Importer
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un dialogue à importer");
					m_DialogFileType = DFT_IMPORT;
					break;
				case 2:// Exporter
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un dossier où exporter le dialogue");
					m_DialogFileType = DFT_EXPORT;
					break;
				case 3:// supprimer
					{
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
#ifdef SNOWLIGHT_PROD_REQUEST
							s32 index = m_ListBox->getSelected();
							if(index >= 0 && (u32)index < m_ListBox->getItemCount())
							{
								m_SceneInterface->RemoveDialogAtIndex(index);
								m_ListBox->removeItem(index);
								m_SceneInterface->m_Data->RemoveDialog(index);
								if((u32)index >= m_ListBox->getItemCount())
									index = m_ListBox->getItemCount()-1;
								m_ListBox->setSelected(index);
								m_SceneInterface->SetCurrentDialog(index);
								m_SceneEditorWindow->OnDialogChange();
							}
#else
							// TEMP : USE THIS UGLY MESSAGE BOX...
							core::stringw str = L"Voulez-vous supprimer le dialogue ";
							str += m_SceneInterface->GetScene()->GetDialogAtIndex(index)->GetDialogName();
							str += L" ?";
							gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression de dialogue", 
								str.c_str(), 
								true, 
								gui::EMBF_YES|gui::EMBF_NO, 
								this, 
								GUI_ID_MESSAGE_BOX_DIALOG_DELETE);
							mes->setNotClipped(true);
							mes->setRelativePosition(core::position2di(0,0));
							mes->setDraggable(false);
#endif
						}
					}
					break;
				case 4:// supprimer tout
					{
						gui::IGUIWindow* mes = m_Gui->addMessageBox(L"Suppression des dialogues", 
							L"Voulez-vous supprimer tous les dialogues ?", 
							true, 
							gui::EMBF_YES|gui::EMBF_NO, 
							this, 
							GUI_ID_MESSAGE_BOX_DIALOG_DELETE_ALL);
						mes->setNotClipped(true);
						mes->setRelativePosition(core::position2di(0,0));
						mes->setDraggable(false);
					}
					break;
				case 5:// Dupliquer
					{
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							DialogNode* dlg = new DialogNode(m_SceneInterface->GetScene()->GetDialogs()[index]);
							dlg->SetDialogName(m_SceneInterface->GetNextDialogName());
							m_SceneInterface->AddNewDialog(dlg);
							m_SceneInterface->m_Data->AddDialog(dlg);
							m_ListBox->addItem(dlg->GetDialogName().c_str());
							s32 index = m_ListBox->getItemCount()-1;
							m_ListBox->setSelected(m_ListBox->getItemCount()-1);
							m_SceneInterface->SetCurrentDialog(index);
							m_SceneEditorWindow->OnDialogChange();
						}
					}
					break;
				case 7:// Monter
					{
						s32 itemSelected = m_ListBox->getSelected();
						TArray<DialogNode*>& dialogs = m_SceneInterface->GetScene()->GetDialogs();
						if(itemSelected >= 1 && dialogs.size() >= 2 && (u32)itemSelected < dialogs.size())
						{
							DialogNode* temp = dialogs[itemSelected-1];
							dialogs[itemSelected-1] = dialogs[itemSelected];
							dialogs[itemSelected] = temp;
							dialogs[itemSelected-1]->SetID(itemSelected-1);
							dialogs[itemSelected]->SetID(itemSelected);

							m_ListBox->swapItems(itemSelected, itemSelected-1);
							m_SceneInterface->m_Data->SwapDialogNext(itemSelected-1);
							m_ListBox->setSelected(itemSelected-1);
						}
					}
					break;
				case 8:// descendre
					{
						s32 itemSelected = m_ListBox->getSelected();
						TArray<DialogNode*>& dialogs = m_SceneInterface->GetScene()->GetDialogs();
						if(itemSelected >= 0 && dialogs.size() >= 2 && (u32)itemSelected < dialogs.size()-1)
						{
							DialogNode* temp = dialogs[itemSelected+1];
							dialogs[itemSelected+1] = dialogs[itemSelected];
							dialogs[itemSelected] = temp;
							dialogs[itemSelected+1]->SetID(itemSelected+1);
							dialogs[itemSelected]->SetID(itemSelected);

							m_ListBox->swapItems(itemSelected, itemSelected+1);
							m_SceneInterface->m_Data->SwapDialogNext(itemSelected);
							m_ListBox->setSelected(itemSelected+1);
						}
					}
					break;
				case 9:// définir comme début
					m_SceneInterface->GetScene()->SetInitialDialog(m_ListBox->getSelected());
					break;
				case 10:// lier automatiquement
					{
						TArray<DialogNode*>& dialogs = m_SceneInterface->GetScene()->GetDialogs();
						for(s32 i = 0; i < (s32)dialogs.size()-1; ++i)
						{
							DialogNode* dlg = dialogs[i];
							switch(dlg->GetNodeType())
							{
							case DialogNode::DN_AUTO_VALID:
							case DialogNode::DN_MANUAL_VALID:
							case DialogNode::DN_MULTI_VALID:
								if(dlg->GetNbChildren() == 0)
									dlg->BranchToNode(dialogs[i+1]);
								break;
							case DialogNode::DN_QTE_VALID:
								if(dlg->GetNbChildren() == 0)
								{
									dlg->BranchToNode(dialogs[i+1]);
									dlg->BranchToNode(dialogs[0]);
								}
								break;
							default:
								break;
							}
						}
					}
					break;
				case 11:// Exporter en xml
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un dossier où exporter le dialogue");
					m_DialogFileType = DFT_XML_EXPORT;
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void DialogListBox::OnSceneChange()
{
	if(m_SceneInterface->GetScene())
	{
		assert(m_SceneInterface->m_Data->m_DialogListNames.size() == m_SceneInterface->GetScene()->GetDialogs().size());
		m_ListBox->clear();
		for(u32 i = 0; i < m_SceneInterface->GetScene()->GetDialogs().size(); ++i)
		{
			m_ListBox->addItem(m_SceneInterface->m_Data->m_DialogListNames[i].c_str());
		}
		m_ListBox->setSelected(-1);

		m_SceneEditorWindow->OnDialogChange();
	}
}
 
// SceneEditorWindow
SceneEditorWindow::SceneEditorWindow(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position) 
	: gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), 
	m_CurrentTab(SCT_Dialog),
	m_Gui(env), 
	m_ContextualMenu(NULL), 
	m_LoadFileDlg(NULL),
	m_SaveFileDlg(NULL),
	m_OpenFileDlg(NULL),
	m_PopupDialog(NULL),
	m_CurrentlyHovered(false),
	m_LayerCurrentlyDisplaced(false),
	m_SceneInterface(NULL),
	m_FileLoadType(FLT_NONE),
	m_CurrentDlgTime(0.0f),
	m_IsPlaying(false),
	m_IsPlayingSoundPreview(false),
	m_CtrlIsPressed(false),
	m_ShiftIsPressed(false)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();
	m_ViewerBackground = m_Gui->addImage(Rectangle2(Position2(0, VIEW_SCENE_EDITOR_TOOLBARS_SIZE * 2), Dimension2(this->getRelativePosition().getWidth() - VIEW_SCENE_EDITOR_WINDOW_CONTROL - VIEW_SCROLL_BAR_THICKNESS, this->getRelativePosition().getHeight() - VIEW_SCENE_EDITOR_TOOLBARS_SIZE * 2 - VIEW_SCROLL_BAR_THICKNESS)), this);
	m_ViewerBackground->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	gui::IGUIStaticText* staticText = NULL;

	// Toolbar line 1
	staticText = m_Gui->addStaticText(L"Taille du rendu", Rectangle2(Position2(0, 0), Dimension2(128, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderWidthSpinBox = m_Gui->addSpinBox(L"Width", Rectangle2(Position2(128, 0), Dimension2(64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, this);
	m_RenderWidthSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderWidthSpinBox->setDecimalPlaces(0);
	m_RenderWidthSpinBox->setRange(0.0f, 1920.0f);
	m_RenderWidthSpinBox->setValue((f32)Engine::GetInstance()->m_RenderSize.Width);
	staticText = m_Gui->addStaticText(L" /", Rectangle2(Position2(128 + 64, 0), Dimension2(16, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderHeightSpinBox = m_Gui->addSpinBox(L"Height", Rectangle2(Position2(128 + 64 + 16, 0), Dimension2(64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, this);
	m_RenderHeightSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderHeightSpinBox->setDecimalPlaces(0);
	m_RenderHeightSpinBox->setRange(0.0f, 1080.0f);
	m_RenderHeightSpinBox->setValue((f32)Engine::GetInstance()->m_RenderSize.Height);
	staticText = m_Gui->addStaticText(L"Langue", Rectangle2(Position2(128 + 64 + 16 + 64, 0), Dimension2(128, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LanguageTypeComboBox = m_Gui->addComboBox(Rectangle2(Position2(128 + 64 + 16 + 64 + 64, 0), Dimension2(128, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), this);
	m_LanguageTypeComboBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	for(u32 i = 0; i < Engine::LS_MAX; ++i)
		m_LanguageTypeComboBox->addItem(engine->ms_LanguageSettingsString[i]);

	// Toolbar line 2
	u32 viewerWidth = this->getRelativePosition().getWidth() - VIEW_SCENE_EDITOR_WINDOW_CONTROL;
	staticText = m_Gui->addStaticText(L"Timeline", Rectangle2(Position2(0, VIEW_SCENE_EDITOR_TOOLBARS_SIZE), Dimension2(64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TimeControlScrollBar = m_Gui->addScrollBar(true, Rectangle2(Position2(64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE), Dimension2(viewerWidth - 64 - 64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), this);
	m_TimeControlScrollBar->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TimeControlScrollBar->setSmallStep(10);
	m_TimeControlScrollBar->setLargeStep(100);
	m_TimeText = m_Gui->addStaticText(L"0.0s", Rectangle2(Position2(viewerWidth-64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE), Dimension2(32, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	m_TimeText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_ButtonPlay = m_Gui->addButton(Rectangle2(Position2(viewerWidth-32, VIEW_SCENE_EDITOR_TOOLBARS_SIZE), Dimension2(32, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), this, -1, L"Jouer");
	m_ButtonPlay->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);

	Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());
	Dimension2 rendertargetDim(core::min_(Engine::GetInstance()->m_RenderSize.Width, dim.Width), core::min_(Engine::GetInstance()->m_RenderSize.Height, dim.Height));
	m_lastSize = rendertargetDim;

	if (driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		if(rendertargetDim.Width > 0 && rendertargetDim.Height > 0)
			m_RenderTarget = driver->addRenderTargetTexture(rendertargetDim, "RTT_SceneEditorWindow", video::ECF_A8R8G8B8);
		else 
			m_RenderTarget = NULL;
		//m_Viewer = m_Gui->addImage(m_RenderTarget, Position2(0, 0), false, m_ViewerBackground);
		m_Viewer = m_Gui->addImage(Rectangle2(Position2(0, 0), dim), m_ViewerBackground);
		m_Viewer->setImage(m_RenderTarget);
	}
	else
	{
		m_RenderTarget = NULL;
		m_Viewer = m_Gui->addImage(Rectangle2(Position2(0, 0), dim), m_ViewerBackground);
	}
	m_Viewer->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	Position2 basePos = m_ViewerBackground->getRelativePosition().UpperLeftCorner;
	m_ViewerWScrollBar = m_Gui->addScrollBar(true, Rectangle2(basePos + Position2(0, dim.Height), Dimension2(dim.Width, VIEW_SCROLL_BAR_THICKNESS)), this);
	m_ViewerWScrollBar->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT);
	m_ViewerWScrollBar->setSmallStep(32);
	m_ViewerWScrollBar->setLargeStep(256);
	m_ViewerHScrollBar = m_Gui->addScrollBar(false, Rectangle2(basePos + Position2(dim.Width, 0), Dimension2(VIEW_SCROLL_BAR_THICKNESS, dim.Height+VIEW_SCROLL_BAR_THICKNESS)), this);
	m_ViewerHScrollBar->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_ViewerHScrollBar->setSmallStep(32);
	m_ViewerHScrollBar->setLargeStep(256);

	m_ViewerWScrollBar->setMax((s32)(Engine::GetInstance()->m_RenderSize.Width) - dim.Width);
	m_ViewerHScrollBar->setMax((s32)(Engine::GetInstance()->m_RenderSize.Height) - dim.Height);

	m_ViewerPosition = Position2(0, 0);


	u32 posY = 0;

	// create tab control and tabs
	m_TabControl = m_Gui->addTabControl(Rectangle2(Position2(viewerWidth, posY), Dimension2(VIEW_SCENE_EDITOR_WINDOW_CONTROL, this->getRelativePosition().getHeight())), this);
	m_TabControl->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_TabControl->setTabHeight(16);
	m_TabControl->setTabExtraWidth(8);

	gui::IGUITab* t1 = m_TabControl->addTab(L"Dlg");
	gui::IGUITab* t2 = m_TabControl->addTab(L"Bkg");
	gui::IGUITab* t3 = m_TabControl->addTab(L"Spr");
	gui::IGUITab* t4 = m_TabControl->addTab(L"Tr");
	gui::IGUITab* t5 = m_TabControl->addTab(L"Snd");
	t1->setToolTipText(L"Dialogue");
	t2->setToolTipText(L"Fond");
	t3->setToolTipText(L"Sprites");
	t4->setToolTipText(L"Transitions");
	t5->setToolTipText(L"Audio");


	u32 width = t1->getRelativePosition().getWidth();

	// dialog tab
	posY = 0;
	staticText = m_Gui->addStaticText(L"Type", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogTypeComboBox = m_Gui->addComboBox(Rectangle2(Position2(width/2,posY), Dimension2(width/2, 16)), t1);
	m_DialogTypeComboBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogTypeComboBox->addItem(L"Auto");
	m_DialogTypeComboBox->addItem(L"Manuel");
	m_DialogTypeComboBox->addItem(L"Multiple");
	m_DialogTypeComboBox->addItem(L"QTE");
	posY += 16;

// 	staticText = m_Gui->addStaticText(L"Texte", Rectangle2(Position2(0,posY), Dimension2(width, 16)), true, true, t1);
// 	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
// 	posY += 16;
	staticText = m_Gui->addStaticText(L"Position", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogPosXSpinBox = m_Gui->addSpinBox(L"X", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogPosXSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogPosXSpinBox->setDecimalPlaces(3);
	m_DialogPosXSpinBox->setRange(0.0f, 1.0f);
	m_DialogPosXSpinBox->setStepSize(0.01f);
	m_DialogPosYSpinBox = m_Gui->addSpinBox(L"Y", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogPosYSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogPosYSpinBox->setDecimalPlaces(3);
	m_DialogPosYSpinBox->setRange(0.0f, 1.0f);
	m_DialogPosYSpinBox->setStepSize(0.01f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Dimensions", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogSizeXSpinBox = m_Gui->addSpinBox(L"X", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogSizeXSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogSizeXSpinBox->setDecimalPlaces(3);
	m_DialogSizeXSpinBox->setRange(0.0f, 1.0f);
	m_DialogSizeXSpinBox->setStepSize(0.01f);
	m_DialogSizeYSpinBox = m_Gui->addSpinBox(L"Y", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogSizeYSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogSizeYSpinBox->setDecimalPlaces(3);
	m_DialogSizeYSpinBox->setRange(0.0f, 1.0f);
	m_DialogSizeYSpinBox->setStepSize(0.01f);
	posY += 16;
	m_DialogHeaderBox = m_Gui->addEditBox(L"Texte", Rectangle2(Position2(0,posY), Dimension2(width, 32)), true, t1);
	m_DialogHeaderBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogHeaderBox->setAutoScroll(true);
	m_DialogHeaderBox->setMultiLine(true);
	m_DialogHeaderBox->setWordWrap(true);
	posY += 32;
	m_DialogTextBox = m_Gui->addEditBox(L"Texte", Rectangle2(Position2(0,posY), Dimension2(width, 128)), true, t1);
	m_DialogTextBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogTextBox->setAutoScroll(true);
	m_DialogTextBox->setMultiLine(true);
	m_DialogTextBox->setWordWrap(true);
	posY += 128;

	staticText = m_Gui->addStaticText(L"Vitesse", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogSpeedSpinBox = m_Gui->addSpinBox(L"Vitesse", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogSpeedSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogSpeedSpinBox->setDecimalPlaces(0);
	m_DialogSpeedSpinBox->setRange(0.0f, 1000.0f);
	m_DialogSpeedSpinBox->setStepSize(1.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Durée", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogDurationSpinBox = m_Gui->addSpinBox(L"Durée", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogDurationSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogDurationSpinBox->setDecimalPlaces(0);
	m_DialogDurationSpinBox->setRange(0.0f, 1000.0f);
	m_DialogDurationSpinBox->setStepSize(1.0f);
	posY += 16;
	m_DialogTextBGCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t1, -1, L"Utiliser fond");
	m_DialogTextBGCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogTextBGCheckBox->setToolTipText(L"Utiliser fond");
	posY += 16;

	staticText = m_Gui->addStaticText(L"Dialogues fils", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogChildListBox = m_Gui->addListBox(Rectangle2(Position2(0, posY), Dimension2(width, 48)), t1, -1, true);
	m_DialogChildListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 48;
	staticText = m_Gui->addStaticText(L"Choix", Rectangle2(Position2(0,posY), Dimension2(width, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogChoiceTextBox = m_Gui->addEditBox(L"Texte", Rectangle2(Position2(0,posY), Dimension2(width, 48)), true, t1);
	m_DialogChoiceTextBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogChoiceTextBox->setAutoScroll(true);
	m_DialogChoiceTextBox->setMultiLine(true);
	m_DialogChoiceTextBox->setWordWrap(true);
	posY += 48;
	staticText = m_Gui->addStaticText(L"Contrôles", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogInputsListBox = m_Gui->addListBox(Rectangle2(Position2(0, posY), Dimension2(width, 48)), t1, -1, true);
	m_DialogInputsListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 48;
	staticText = m_Gui->addStaticText(L"Durée", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogInputDurationSpinBox = m_Gui->addSpinBox(L"Durée", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogInputDurationSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogInputDurationSpinBox->setDecimalPlaces(3);
	m_DialogInputDurationSpinBox->setRange(-100.0f, 100.0f);
	m_DialogInputDurationSpinBox->setStepSize(0.1f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Nb d'appuis", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t1);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogInputNbPressSpinBox = m_Gui->addSpinBox(L"Nb d'appuis", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t1);
	m_DialogInputNbPressSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogInputNbPressSpinBox->setDecimalPlaces(0);
	m_DialogInputNbPressSpinBox->setRange(1.0f, 255.0f);
	m_DialogInputNbPressSpinBox->setStepSize(1.0f);
	posY += 16;
	for(u32 i = 0; i < (u32)InputData::PI_MAX; ++i)
	{
		m_DialogInputsCheckBox[i] = m_Gui->addCheckBox(false, Rectangle2(Position2((width/2)*(i%2), posY), Dimension2(width/2, 16)), t1, -1, InputData::GetInputName((InputData::EPadInput)i));
		m_DialogInputsCheckBox[i]->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
		m_DialogInputsCheckBox[i]->setToolTipText(InputData::GetInputName((InputData::EPadInput)i));
		if((i%2) == 1)
			posY += 16;
	}
	posY += 16;

	// background tab
	posY = 0;
	staticText = m_Gui->addStaticText(L"Image de fond", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogBackgroundListBox = m_Gui->addListBox(Rectangle2(Position2(0, posY), Dimension2(width, VIEW_BG_PREVIEW_SIZE+4)), t2, -1, true);
	m_DialogBackgroundListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += VIEW_BG_PREVIEW_SIZE+4 + 16;

	staticText = m_Gui->addStaticText(L"Clés", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_DialogBackgroundKeysListBox = m_Gui->addListBox(Rectangle2(Position2(0, posY), Dimension2(width, 64)), t2, -1, true);
	m_DialogBackgroundKeysListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 64;
	staticText = m_Gui->addStaticText(L"Temps", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyTimeSpinBox = m_Gui->addSpinBox(L"Temps", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyTimeSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyTimeSpinBox->setDecimalPlaces(3);
	m_DialogBGKeyTimeSpinBox->setRange(0.0f, 1000.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position X", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyPosXSpinBox = m_Gui->addSpinBox(L"Position X", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyPosXSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyPosXSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position Y", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyPosYSpinBox = m_Gui->addSpinBox(L"Position Y", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyPosYSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyPosYSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Zoom", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyScaleSpinBox = m_Gui->addSpinBox(L"Zoom", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyScaleSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyScaleSpinBox->setStepSize(0.1f);
	m_DialogBGKeyScaleSpinBox->setDecimalPlaces(5);
	m_DialogBGKeyScaleSpinBox->setRange(0.0f, 10.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Rotation", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyRotationSpinBox = m_Gui->addSpinBox(L"Rotation", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyRotationSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyRotationSpinBox->setDecimalPlaces(5);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Couleur", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorTextBox = m_Gui->addStaticText(L"FFFFFFFF", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, true, t2);
	m_DialogBGKeyColorTextBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorTextBox->setBackgroundColor(IColor(255,255,255,255));
	posY += 16;
	staticText = m_Gui->addStaticText(L"A", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorASpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyColorASpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorASpinBox->setStepSize(1.0f);
	m_DialogBGKeyColorASpinBox->setDecimalPlaces(0);
	m_DialogBGKeyColorASpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"R", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorRSpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyColorRSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorRSpinBox->setStepSize(1.0f);
	m_DialogBGKeyColorRSpinBox->setDecimalPlaces(0);
	m_DialogBGKeyColorRSpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"G", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorGSpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyColorGSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorGSpinBox->setStepSize(1.0f);
	m_DialogBGKeyColorGSpinBox->setDecimalPlaces(0);
	m_DialogBGKeyColorGSpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"B", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorBSpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogBGKeyColorBSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeyColorBSpinBox->setStepSize(1.0f);
	m_DialogBGKeyColorBSpinBox->setDecimalPlaces(0);
	m_DialogBGKeyColorBSpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	m_DialogBGKeysLoopCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t2, -1, L"Répéter");
	m_DialogBGKeysLoopCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogBGKeysLoopCheckBox->setToolTipText(L"Répéter");
	posY += 16+16;

	staticText = m_Gui->addStaticText(L"Tremblement de l'écran", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Amplitude", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogScreenShakeStrengthSpinBox = m_Gui->addSpinBox(L"Amplitude", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogScreenShakeStrengthSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogScreenShakeStrengthSpinBox->setDecimalPlaces(3);
	m_DialogScreenShakeStrengthSpinBox->setStepSize(0.1f);
	m_DialogScreenShakeStrengthSpinBox->setRange(0.0f, 10.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Fréquence", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t2);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogScreenShakeFrequencySpinBox = m_Gui->addSpinBox(L"Fréquence", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t2);
	m_DialogScreenShakeFrequencySpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_DialogScreenShakeFrequencySpinBox->setDecimalPlaces(3);
	m_DialogScreenShakeFrequencySpinBox->setStepSize(0.1f);
	m_DialogScreenShakeFrequencySpinBox->setRange(0.0f, 10.0f);
	posY += 16;

	// sprite tab
	posY = 0;
	staticText = m_Gui->addStaticText(L"Liste des sprites", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_SpriteListBox = m_Gui->addListBox(Rectangle2(Position2(0, posY), Dimension2(width, (u32)(VIEW_BG_PREVIEW_SIZE*1.5f))), t3, -1, true);
	m_SpriteListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += (u32)(VIEW_BG_PREVIEW_SIZE*1.5f) + 16;

	staticText = m_Gui->addStaticText(L"Clés", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_SpriteKeysListBox = m_Gui->addListBox(Rectangle2(Position2(0, posY), Dimension2(width, 64)), t3, -1, true);
	m_SpriteKeysListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 64;
	staticText = m_Gui->addStaticText(L"Temps", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyTimeSpinBox = m_Gui->addSpinBox(L"Temps", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyTimeSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyTimeSpinBox->setDecimalPlaces(3);
	m_SpriteKeyTimeSpinBox->setRange(0.0f, 1000.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position X", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyPosXSpinBox = m_Gui->addSpinBox(L"Position X", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyPosXSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyPosXSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position Y", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyPosYSpinBox = m_Gui->addSpinBox(L"Position Y", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyPosYSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyPosYSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Zoom", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyScaleSpinBox = m_Gui->addSpinBox(L"Zoom", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyScaleSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyScaleSpinBox->setStepSize(0.1f);
	m_SpriteKeyScaleSpinBox->setDecimalPlaces(5);
	m_SpriteKeyScaleSpinBox->setRange(0.0f, 10.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Rotation", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyRotationSpinBox = m_Gui->addSpinBox(L"Rotation", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyRotationSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyRotationSpinBox->setDecimalPlaces(5);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Couleur", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorTextBox = m_Gui->addStaticText(L"FFFFFFFF", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, true, t3);
	m_SpriteKeyColorTextBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorTextBox->setBackgroundColor(IColor(255,255,255,255));
	posY += 16;
	staticText = m_Gui->addStaticText(L"A", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorASpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyColorASpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorASpinBox->setStepSize(1.0f);
	m_SpriteKeyColorASpinBox->setDecimalPlaces(0);
	m_SpriteKeyColorASpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"R", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorRSpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyColorRSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorRSpinBox->setStepSize(1.0f);
	m_SpriteKeyColorRSpinBox->setDecimalPlaces(0);
	m_SpriteKeyColorRSpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"G", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorGSpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyColorGSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorGSpinBox->setStepSize(1.0f);
	m_SpriteKeyColorGSpinBox->setDecimalPlaces(0);
	m_SpriteKeyColorGSpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"B", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t3);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorBSpinBox = m_Gui->addSpinBox(L"255", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t3);
	m_SpriteKeyColorBSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeyColorBSpinBox->setStepSize(1.0f);
	m_SpriteKeyColorBSpinBox->setDecimalPlaces(0);
	m_SpriteKeyColorBSpinBox->setRange(0.0f, 255.0f);
	posY += 16;
	m_SpriteMirrorHCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t3, -1, L"Inversion horizontale");
	m_SpriteMirrorHCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteMirrorHCheckBox->setToolTipText(L"Invertion horizontale");
	posY += 16;
	m_SpriteMirrorVCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t3, -1, L"Inversion verticale");
	m_SpriteMirrorVCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteMirrorVCheckBox->setToolTipText(L"Invertion verticale");
	posY += 16;
	m_SpriteKeysLoopCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t3, -1, L"Répéter les clés");
	m_SpriteKeysLoopCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteKeysLoopCheckBox->setToolTipText(L"Répéter les clés");
	posY += 16;
	m_SpriteAnimLoopCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t3, -1, L"Répéter l'animation");
	m_SpriteAnimLoopCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteAnimLoopCheckBox->setToolTipText(L"Répéter l'animation");
	posY += 16;

	// transition tab
	posY = 0;
	staticText = m_Gui->addStaticText(L"Transition entrante", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t4);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Type", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t4);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionInTypeComboBox = m_Gui->addComboBox(Rectangle2(Position2(width/2,posY), Dimension2(width/2, 16)), t4);
	m_TransitionInTypeComboBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionInTypeComboBox->addItem(L"Aucune");
	m_TransitionInTypeComboBox->addItem(L"Fondu");
	posY += 16;
	staticText = m_Gui->addStaticText(L"Durée", Rectangle2(Position2(0,posY), Dimension2(width, 16)), true, true, t4);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionInDurationSpinBox = m_Gui->addSpinBox(L"Durée", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t4);
	m_TransitionInDurationSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionInDurationSpinBox->setDecimalPlaces(3);
	m_TransitionInDurationSpinBox->setRange(0.0f, 10.0f);
	m_TransitionInDurationSpinBox->setStepSize(0.1f);
	posY += 32;

	staticText = m_Gui->addStaticText(L"Transition sortante", Rectangle2(Position2(0, posY), Dimension2(width, 16)), true, true, t4);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Type", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t4);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionOutTypeComboBox = m_Gui->addComboBox(Rectangle2(Position2(width/2,posY), Dimension2(width/2, 16)), t4);
	m_TransitionOutTypeComboBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionOutTypeComboBox->addItem(L"Aucune");
	m_TransitionOutTypeComboBox->addItem(L"Fondu");
	posY += 16;
	staticText = m_Gui->addStaticText(L"Durée", Rectangle2(Position2(0,posY), Dimension2(width, 16)), true, true, t4);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionOutDurationSpinBox = m_Gui->addSpinBox(L"Durée", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t4);
	m_TransitionOutDurationSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_TransitionOutDurationSpinBox->setDecimalPlaces(3);
	m_TransitionOutDurationSpinBox->setRange(0.0f, 10.0f);
	m_TransitionOutDurationSpinBox->setStepSize(0.1f);
	posY += 32;

	// audio tab
	posY = 0;
	staticText = m_Gui->addStaticText(L"Son d'ambiance 1", Rectangle2(Position2(0, posY), Dimension2(width-32, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBGPlayButton1 = m_Gui->addButton(Rectangle2(Position2(width-32, posY), Dimension2(32, 16)), t5, -1, L"Jouer");
	m_AudioBGPlayButton1->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_AudioBGTextBox1 = m_Gui->addStaticText(L"", Rectangle2(Position2(0, posY), Dimension2(width-16, 16)), true, true, t5, -1, true);
	m_AudioBGTextBox1->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBGBrowseButton1 = m_Gui->addButton(Rectangle2(Position2(width-16, posY), Dimension2(16, 16)), t5, -1, L"...");
	m_AudioBGBrowseButton1->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Volume", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1VolumeSpinBox = m_Gui->addSpinBox(L"100", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioBG1VolumeSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1VolumeSpinBox->setStepSize(10.0f);
	m_AudioBG1VolumeSpinBox->setDecimalPlaces(0);
	m_AudioBG1VolumeSpinBox->setRange(0.0f, 100.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1PitchSpinBox = m_Gui->addSpinBox(L"0", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioBG1PitchSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1PitchSpinBox->setStepSize(10.0f);
	m_AudioBG1PitchSpinBox->setDecimalPlaces(0);
	m_AudioBG1PitchSpinBox->setRange(-100.0f, 100.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Balance", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1PanSpinBox = m_Gui->addSpinBox(L"0", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioBG1PanSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1PanSpinBox->setStepSize(10.0f);
	m_AudioBG1PanSpinBox->setDecimalPlaces(0);
	m_AudioBG1PanSpinBox->setRange(-100.0f, 100.0f);
	posY += 16;
	m_AudioBG1RepeatCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t5, -1, L"Répéter le son");
	m_AudioBG1RepeatCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1RepeatCheckBox->setToolTipText(L"Répéter le son");
	posY += 16;
	m_AudioBG1StopCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t5, -1, L"Stopper à la fin");
	m_AudioBG1StopCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG1StopCheckBox->setToolTipText(L"Stopper à la fin");
	posY += 32;

	staticText = m_Gui->addStaticText(L"Son d'ambiance 2", Rectangle2(Position2(0, posY), Dimension2(width-32, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBGPlayButton2 = m_Gui->addButton(Rectangle2(Position2(width-32, posY), Dimension2(32, 16)), t5, -1, L"Jouer");
	m_AudioBGPlayButton2->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_AudioBGTextBox2 = m_Gui->addStaticText(L"", Rectangle2(Position2(0, posY), Dimension2(width-16, 16)), true, true, t5, -1, true);
	m_AudioBGTextBox2->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBGBrowseButton2 = m_Gui->addButton(Rectangle2(Position2(width-16, posY), Dimension2(16, 16)), t5, -1, L"...");
	m_AudioBGBrowseButton2->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Volume", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2VolumeSpinBox = m_Gui->addSpinBox(L"100", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioBG2VolumeSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2VolumeSpinBox->setStepSize(10.0f);
	m_AudioBG2VolumeSpinBox->setDecimalPlaces(0);
	m_AudioBG2VolumeSpinBox->setRange(0.0f, 100.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2PitchSpinBox = m_Gui->addSpinBox(L"0", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioBG2PitchSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2PitchSpinBox->setStepSize(10.0f);
	m_AudioBG2PitchSpinBox->setDecimalPlaces(0);
	m_AudioBG2PitchSpinBox->setRange(-100.0f, 100.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Balance", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2PanSpinBox = m_Gui->addSpinBox(L"0", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioBG2PanSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2PanSpinBox->setStepSize(10.0f);
	m_AudioBG2PanSpinBox->setDecimalPlaces(0);
	m_AudioBG2PanSpinBox->setRange(-100.0f, 100.0f);
	posY += 16;
	m_AudioBG2RepeatCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t5, -1, L"Répéter le son");
	m_AudioBG2RepeatCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2RepeatCheckBox->setToolTipText(L"Répéter le son");
	posY += 16;
	m_AudioBG2StopCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t5, -1, L"Stopper à la fin");
	m_AudioBG2StopCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioBG2StopCheckBox->setToolTipText(L"Stopper à la fin");
	posY += 32;

	staticText = m_Gui->addStaticText(L"Voix", Rectangle2(Position2(0, posY), Dimension2(width-32, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoicePlayButton = m_Gui->addButton(Rectangle2(Position2(width-32, posY), Dimension2(32, 16)), t5, -1, L"Jouer");
	m_AudioVoicePlayButton->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	m_AudioVoiceTextBox = m_Gui->addStaticText(L"", Rectangle2(Position2(0, posY), Dimension2(width-16, 16)), true, true, t5, -1, true);
	m_AudioVoiceTextBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoiceBrowseButton = m_Gui->addButton(Rectangle2(Position2(width-16, posY), Dimension2(16, 16)), t5, -1, L"...");
	m_AudioVoiceBrowseButton->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Volume", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoiceVolumeSpinBox = m_Gui->addSpinBox(L"100", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioVoiceVolumeSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoiceVolumeSpinBox->setStepSize(10.0f);
	m_AudioVoiceVolumeSpinBox->setDecimalPlaces(0);
	m_AudioVoiceVolumeSpinBox->setRange(0.0f, 100.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoicePitchSpinBox = m_Gui->addSpinBox(L"0", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioVoicePitchSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoicePitchSpinBox->setStepSize(10.0f);
	m_AudioVoicePitchSpinBox->setDecimalPlaces(0);
	m_AudioVoicePitchSpinBox->setRange(-100.0f, 100.0f);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Balance", Rectangle2(Position2(0, posY), Dimension2(width/2, 16)), true, true, t5);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoicePanSpinBox = m_Gui->addSpinBox(L"0", Rectangle2(Position2(width/2, posY), Dimension2(width/2, 16)), true, t5);
	m_AudioVoicePanSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoicePanSpinBox->setStepSize(10.0f);
	m_AudioVoicePanSpinBox->setDecimalPlaces(0);
	m_AudioVoicePanSpinBox->setRange(-100.0f, 100.0f);
	posY += 16;
	m_AudioVoiceRepeatCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t5, -1, L"Répéter le son");
	m_AudioVoiceRepeatCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoiceRepeatCheckBox->setToolTipText(L"Répéter le son");
	posY += 16;
	m_AudioVoiceStopCheckBox = m_Gui->addCheckBox(false, Rectangle2(Position2(0, posY), Dimension2(width, 16)), t5, -1, L"Stopper à la fin");
	m_AudioVoiceStopCheckBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_AudioVoiceStopCheckBox->setToolTipText(L"Stopper à la fin");
	posY += 32;

	OnSceneChange();
}

SceneEditorWindow::~SceneEditorWindow()
{
	/*{
		FILE* output = fopen("test.bin", "wb");
		Image* img = TextureToImage(m_RenderTarget);
		PreProcessAlphaPixels((unsigned char*)(img->lock()), 256*256*4);
		RLECompressFile((unsigned char*)(img->lock()), 256*256*4, output);
		fclose(output);
		img->drop();
	}

	{
		FILE* input = fopen("test.bin", "rb");
		int inputlen = (int)_filelength(_fileno(input));
		unsigned char* fileguts = (unsigned char*)malloc(inputlen);
		fread(fileguts, inputlen, 1, input);
		fclose(input);
		Image* img = TextureToImage(m_RenderTarget);
		RLEDecompressFile(fileguts, inputlen, (unsigned char*)(img->lock()));
		Engine::GetInstance()->GetDriver()->writeImageToFile(img, "test.bmp");
		img->drop();
		free(fileguts);
	}*/
}

void SceneEditorWindow::OnSceneChange()
{
	OnDialogChange();
}

void SceneEditorWindow::OnDialogChange()
{
	m_TabControl->setEnabled(false);
	m_TabControl->setVisible(false);
	
	if(m_SceneInterface)
	{
		// unload all sounds
		if(m_SceneInterface->GetScene())
		{
			m_SceneInterface->GetScene()->FlushSoundsAlive();
		

			DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
			if(dlg)
			{
				m_TabControl->setEnabled(true);
				m_TabControl->setVisible(true);

				u32 languageIndex = Engine::GetInstance()->GetLanguage();
				m_DialogTypeComboBox->setSelected((s32)dlg->GetNodeType());
				m_DialogHeaderBox->setText(dlg->GetDialogHeader(languageIndex).c_str());
				m_DialogTextBox->setText(dlg->GetDialogText(languageIndex).c_str());
				m_DialogTextBGCheckBox->setChecked(dlg->GetUseTextBackGround());
				m_DialogSpeedSpinBox->setValue(dlg->GetDialogSpeed());
				m_DialogDurationSpinBox->setValue(dlg->GetDialogDuration());
				m_DialogPosXSpinBox->setValue(dlg->GetDialogPosition().X);
				m_DialogPosYSpinBox->setValue(dlg->GetDialogPosition().Y);
				m_DialogSizeXSpinBox->setValue(dlg->GetDialogSize().X);
				m_DialogSizeYSpinBox->setValue(dlg->GetDialogSize().Y);
				m_DialogChildListBox->clear();
				for(u32 i=0; i<dlg->GetChildNodes().size(); ++i)
				{
					s32 index = m_SceneInterface->GetScene()->GetDialogs().linear_search(dlg->GetChildNodes()[i]);
					assert(index >= 0);
					core::stringw str = m_SceneInterface->m_Data->m_DialogListNames[index];
					m_DialogChildListBox->addItem(str.c_str());
				}
				if(!dlg->GetChildNodes().empty() && dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
				{
					m_DialogChoiceTextBox->setEnabled(true);
					m_DialogChildListBox->setSelected(0);
					m_DialogChoiceTextBox->setText(dlg->GetConditionalTexts(languageIndex)[0].c_str());
				}
				else
				{
					m_DialogChildListBox->setSelected(-1);
					m_DialogChoiceTextBox->setText(L"");
					m_DialogChoiceTextBox->setEnabled(false);
				}

				m_DialogInputsListBox->clear();
				for(u32 i=0; i<dlg->GetInputs().size(); ++i)
					m_DialogInputsListBox->addItem(core::stringw(dlg->GetInputs()[i].m_Duration).c_str());
				m_DialogInputsListBox->setEnabled(dlg->GetNodeType() == DialogNode::DN_QTE_VALID);
				if(!dlg->GetInputs().empty() && dlg->GetNodeType() == DialogNode::DN_QTE_VALID)
				{
					m_DialogInputsListBox->setSelected(0);
					m_DialogInputDurationSpinBox->setEnabled(true);
					m_DialogInputNbPressSpinBox->setEnabled(true);
					m_DialogInputDurationSpinBox->setValue(dlg->GetInputs()[0].m_Duration);
					m_DialogInputNbPressSpinBox->setValue(dlg->GetInputs()[0].m_NbPress);
					for(u32 j = 0; j < (u32)InputData::PI_MAX; ++j)
					{
						m_DialogInputsCheckBox[j]->setEnabled(true);
						m_DialogInputsCheckBox[j]->setChecked(dlg->GetInputs()[0].IsPressed((InputData::EPadInput)j));
					}
				}
				else
				{
					m_DialogInputsListBox->setSelected(-1);
					m_DialogInputDurationSpinBox->setEnabled(false);
					m_DialogInputNbPressSpinBox->setEnabled(false);
					m_DialogInputDurationSpinBox->setValue(0.0f);
					m_DialogInputNbPressSpinBox->setValue(0.0f);
					for(u32 j = 0; j < (u32)InputData::PI_MAX; ++j)
					{
						m_DialogInputsCheckBox[j]->setEnabled(false);
						m_DialogInputsCheckBox[j]->setChecked(false);
					}
				}

				OnSound1Changed();
				OnSound2Changed();
				OnVoiceChanged();


				m_TransitionInTypeComboBox->setSelected((s32)dlg->GetTransitionIn().m_Type);
				m_TransitionInDurationSpinBox->setValue(dlg->GetTransitionIn().m_Duration);

				m_TransitionOutTypeComboBox->setSelected((s32)dlg->GetTransitionOut().m_Type);
				m_TransitionOutDurationSpinBox->setValue(dlg->GetTransitionOut().m_Duration);

				UpdateBgBank();

				UpdateSpriteListBank();
			}
		}
	}
}

void SceneEditorWindow::OnSound1Changed()
{
	DialogNode* currentDlg = m_SceneInterface->GetCurrentDialog();
	if(currentDlg)
	{
		const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
		if(sound.IsValid())
		{
			core::stringw str = sound->m_Path;
			m_AudioBGTextBox1->setText(str.c_str());
			m_AudioBGTextBox1->setToolTipText(str.c_str());
			m_AudioBG1VolumeSpinBox->setValue(sound->m_StreamData.m_Volume*100.0f);
			m_AudioBG1PitchSpinBox->setValue((sound->m_StreamData.m_Pitch<1.0f)? 200.0f*(sound->m_StreamData.m_Pitch-1.0f) : 100.0f*(sound->m_StreamData.m_Pitch-1.0f));
			m_AudioBG1PanSpinBox->setValue(100.0f*sound->m_StreamData.m_Pan);
			m_AudioBG1RepeatCheckBox->setChecked(sound->m_StreamData.m_Repeat);
			m_AudioBG1StopCheckBox->setChecked(sound->m_StreamData.m_StopAtDialogsEnd);

			return;
		}
	}

	m_AudioBGTextBox1->setText(L"");
	m_AudioBGTextBox1->setToolTipText(L"");
	m_AudioBG1VolumeSpinBox->setValue(100.0f);
	m_AudioBG1PitchSpinBox->setValue(0.0f);
	m_AudioBG1PanSpinBox->setValue(0.0f);
	m_AudioBG1RepeatCheckBox->setChecked(false);
	m_AudioBG1StopCheckBox->setChecked(false);
}

void SceneEditorWindow::OnSound2Changed()
{
	DialogNode* currentDlg = m_SceneInterface->GetCurrentDialog();
	if(currentDlg)
	{
		const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG2();
		if(sound.IsValid())
		{
			core::stringw str = sound->m_Path;
			m_AudioBGTextBox2->setText(str.c_str());
			m_AudioBGTextBox2->setToolTipText(str.c_str());
			m_AudioBG2VolumeSpinBox->setValue(sound->m_StreamData.m_Volume*100.0f);
			m_AudioBG2PitchSpinBox->setValue((sound->m_StreamData.m_Pitch<1.0f)? 200.0f*(sound->m_StreamData.m_Pitch-1.0f) : 100.0f*(sound->m_StreamData.m_Pitch-1.0f));
			m_AudioBG2PanSpinBox->setValue(100.0f*sound->m_StreamData.m_Pan);
			m_AudioBG2RepeatCheckBox->setChecked(sound->m_StreamData.m_Repeat);
			m_AudioBG2StopCheckBox->setChecked(sound->m_StreamData.m_StopAtDialogsEnd);

			return;
		}
	}

	m_AudioBGTextBox2->setText(L"");
	m_AudioBGTextBox2->setToolTipText(L"");
	m_AudioBG2VolumeSpinBox->setValue(100.0f);
	m_AudioBG2PitchSpinBox->setValue(0.0f);
	m_AudioBG2PanSpinBox->setValue(0.0f);
	m_AudioBG2RepeatCheckBox->setChecked(false);
	m_AudioBG2StopCheckBox->setChecked(false);
}

void SceneEditorWindow::OnVoiceChanged()
{
	DialogNode* currentDlg = m_SceneInterface->GetCurrentDialog();
	if(currentDlg)
	{
		const SmartPointer<SoundDataScene>& sound = currentDlg->GetVoice();
		if(sound.IsValid())
		{
			core::stringw str = sound->m_Path;
			m_AudioVoiceTextBox->setText(str.c_str());
			m_AudioVoiceTextBox->setToolTipText(str.c_str());
			m_AudioVoiceVolumeSpinBox->setValue(sound->m_StreamData.m_Volume*100.0f);
			m_AudioVoicePitchSpinBox->setValue((sound->m_StreamData.m_Pitch<1.0f)? 200.0f*(sound->m_StreamData.m_Pitch-1.0f) : 100.0f*(sound->m_StreamData.m_Pitch-1.0f));
			m_AudioVoicePanSpinBox->setValue(100.0f*sound->m_StreamData.m_Pan);
			m_AudioVoiceRepeatCheckBox->setChecked(sound->m_StreamData.m_Repeat);
			m_AudioVoiceStopCheckBox->setChecked(sound->m_StreamData.m_StopAtDialogsEnd);

			return;
		}
	}

	m_AudioVoiceTextBox->setText(L"");
	m_AudioVoiceTextBox->setToolTipText(L"");
	m_AudioVoiceVolumeSpinBox->setValue(100.0f);
	m_AudioVoicePitchSpinBox->setValue(0.0f);
	m_AudioVoicePanSpinBox->setValue(0.0f);
	m_AudioVoiceRepeatCheckBox->setChecked(false);
	m_AudioVoiceStopCheckBox->setChecked(false);
}

void SceneEditorWindow::OnSpriteSelectionChanged()
{
	DialogNode* currentDlg = m_SceneInterface->GetCurrentDialog();
	if(currentDlg)
	{
		s32 indexS = m_SpriteListBox->getSelected();
		SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
		if(data)
		{
			s32 index = m_SpriteKeysListBox->getSelected();
			TArray<KeyTime>& keys = data->m_Keys;
			if(index >= 0 && (u32)index < keys.size())
			{
				const KeyTime& key = keys[index];
				m_SpriteKeyTimeSpinBox->setValue(key.m_Time);
				m_SpriteKeyPosXSpinBox->setValue((float)key.m_Position.X);
				m_SpriteKeyPosYSpinBox->setValue((float)key.m_Position.Y);
				m_SpriteKeyScaleSpinBox->setValue((float)key.m_Scale);
				m_SpriteKeyRotationSpinBox->setValue((float)key.m_Rotation);
				m_SpriteKeyColorTextBox->setBackgroundColor(key.m_Color);
				m_SpriteKeyColorTextBox->setText(StringFormat(L"%08X", key.m_Color).c_str());
				IColor textColor = MathUtils::GetInverseColor(key.m_Color);
				textColor.setAlpha(200);
				m_SpriteKeyColorTextBox->setOverrideColor(textColor);
				m_SpriteKeyColorASpinBox->setValue((f32)key.m_Color.getAlpha());
				m_SpriteKeyColorRSpinBox->setValue((f32)key.m_Color.getRed());
				m_SpriteKeyColorGSpinBox->setValue((f32)key.m_Color.getGreen());
				m_SpriteKeyColorBSpinBox->setValue((f32)key.m_Color.getBlue());

				if(!m_IsPlaying)
				{
					m_CurrentDlgTime = key.m_Time + core::ROUNDING_ERROR_f32;
					float tMax = currentDlg->GetDialogTotalDuration(false, true);// max depends of the dialog length
					if(m_CurrentDlgTime > tMax)
						m_CurrentDlgTime = tMax;

					m_TimeControlScrollBar->setMax((s32)(tMax*100.0f));
					m_TimeControlScrollBar->setPos((s32)(m_CurrentDlgTime*100.0f));

					char str[32];
					sprintf(str, "%.2f", m_CurrentDlgTime);
					m_TimeText->setText(core::stringw(str).c_str());
				}
			}
			m_SpriteMirrorHCheckBox->setChecked(data->m_MirrorH);
			m_SpriteMirrorVCheckBox->setChecked(data->m_MirrorV);
			m_SpriteKeysLoopCheckBox->setChecked(data->m_Loop);
			m_SpriteAnimLoopCheckBox->setChecked(data->m_LoopAnim);
		}
	}
}

bool SceneEditorWindow::OnEvent(const SEvent& event)
{
	// 	SEvent evt = event;
	// 	printf("SpriteBankListBox\n");
	if(event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.Key == KEY_LSHIFT)
		{
			m_ShiftIsPressed = event.KeyInput.PressedDown;
		}
		else if(event.KeyInput.Key == KEY_LCONTROL)
		{
			m_CtrlIsPressed = event.KeyInput.PressedDown;
		}
		else if(event.KeyInput.Key == KEY_RETURN || event.KeyInput.Key == KEY_ESCAPE)
		{
			if(m_PopupDialog)
			{
				m_PopupDialog->remove();
				m_PopupDialog = NULL;
			}
		}
	}
	else if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_LEFT_UP)
		{
			m_LayerCurrentlyDisplaced = false;
		}
		else if(m_CurrentlyHovered)
		{
			if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
			{
				Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
				gui::IGUIContextMenu* menu = NULL;
				if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Dialog && 
					m_DialogChildListBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// dialog context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_CHOICE);
					menu->addItem(L"Ajouter", 666, dlg != NULL);
					menu->addItem(L"Supprimer", 666, (dlg != NULL) && m_DialogChildListBox->getSelected() >= 0);
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Dialog && 
					m_DialogInputsListBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// inputs context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					if( dlg && dlg->GetNodeType() == DialogNode::DN_QTE_VALID)
					{
						menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_INPUTS);
						menu->addItem(L"Ajouter", 666, dlg != NULL);
						menu->addItem(L"Supprimer", 666, (dlg != NULL) && m_DialogInputsListBox->getSelected() >= 0);
					}
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Dialog && 
					m_DialogHeaderBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// dialog context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_TEXTS);
					menu->addItem(L"Appliquer à toutes les langues", 666, dlg != NULL);
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Dialog && 
					m_DialogTextBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// dialog context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_TEXTS+1);
					menu->addItem(L"Appliquer à toutes les langues", 666, dlg != NULL);
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Dialog && 
					m_DialogChoiceTextBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// dialog context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_TEXTS+2);
					menu->addItem(L"Appliquer à toutes les langues", 666, dlg != NULL);
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Background && 
					m_DialogBackgroundListBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// background context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_BG);
					menu->addItem(L"Ajouter", 666, (dlg != NULL) && (m_DialogBackgroundListBox->getItemCount() == 0));
					menu->addItem(L"Supprimer", 666, (dlg != NULL) && (m_DialogBackgroundListBox->getSelected() >= 0));
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Background && 
					m_DialogBackgroundKeysListBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// background keys context mennu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_DIALOG_BG_KEYS);
					menu->addItem(L"Ajouter", 666, (dlg != NULL) && (dlg->GetImageScene()->m_Image));
					menu->addItem(L"Supprimer", 666, (dlg != NULL) && (dlg->GetImageScene()->m_Image) 
						&& (m_DialogBackgroundKeysListBox->getSelected() >= 0) && (m_DialogBackgroundKeysListBox->getItemCount() > 1));
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Sprites && 
					m_SpriteListBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// sprites context menu
					s32 itemCount = m_SpriteListBox->getItemCount();
					bool isAnyItem = (itemCount != 0);
					s32 index = m_SpriteListBox->getSelected();
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_SPRITE_LIST);
					//menu->addItem(L"Ajouter", 666, (dlg != NULL) && (m_SpriteListBox->getItemCount() == 0));
					menu->addItem(L"Supprimer", 666, (dlg != NULL) && isAnyItem && (index >= 0));
					menu->addItem(L"Monter", 666, (dlg != NULL) && isAnyItem && (index >= 1));
					menu->addItem(L"Descendre", 666, (dlg != NULL) && isAnyItem && (index >= 0)&& (index < itemCount-1));
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Sprites && 
					m_SpriteKeysListBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// sprites keys context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_SPRITE_KEYS);
					menu->addItem(L"Ajouter", 666, (dlg != NULL) && (m_SpriteListBox->getSelected() >= 0));
					menu->addItem(L"Supprimer", 666, (dlg != NULL) && (m_SpriteListBox->getSelected() >= 0) 
						&& (m_SpriteKeysListBox->getSelected() >= 0) && (m_SpriteKeysListBox->getItemCount() > 1));
				}
				else if(m_TabControl->isEnabled() && m_TabControl->getActiveTab() == SCT_Sounds && 
					(m_AudioBGTextBox1->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)) ||
					m_AudioBGTextBox2->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)) ||
					m_AudioVoiceTextBox->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y))) )
				{// background context menu
					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					u32 offset = m_AudioBGTextBox1->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y))? 0 :
						(m_AudioBGTextBox2->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y))? 1 : 2);
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU_SOUNDS+offset);
					menu->addItem(L"Supprimer", 666, (dlg != NULL));
				}
				else if(m_ViewerBackground->isPointInside(Position2(event.MouseInput.X, event.MouseInput.Y)))
				{// scene context menu
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
					menu->addItem(L"Nouveau", 666, true);
					menu->addItem(L"Sauvegarder", 666, true);
					menu->addItem(L"Exporter pour PSVita", 666, true);
					menu->addItem(L"Charger", 666, true);
					menu->addSeparator();
					menu->addItem(L"Exporter les textes", 666, true);
					menu->addItem(L"Importer les textes", 666, true);
				}

				if(menu)
				{
					Engine* engine = Engine::GetInstance();
					video::IVideoDriver* driver = engine->GetDriver();

					Dimension2 dim = driver->getScreenSize();
					r = menu->getRelativePosition();
					r.constrainTo(Rectangle2(Position2(0, 0), dim));
					menu->setRelativePosition(r);

					menu->setEventParent(this);
				}
			}
			else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
			{
				m_LayerCurrentlyDisplaced = true;
				m_LayerCurrentCursorPos = Position2(event.MouseInput.X, event.MouseInput.Y);
			}
			else if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
			{
				if(m_LayerCurrentlyDisplaced)
				{
					Position2 newPos = Position2(event.MouseInput.X, event.MouseInput.Y);

					DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
					KeyTime* key = NULL;
					if(dlg)
					{
						if(m_TabControl->getActiveTab() == (s32)SCT_Background)// we are editing the background : edit the current background key
						{
							s32 index = m_DialogBackgroundKeysListBox->getSelected();
							if(index >= 0 && (u32)index < (dlg->GetImageScene()->m_Keys).size())
								key = &(dlg->GetImageScene()->m_Keys[index]);
						}
						else if(m_TabControl->getActiveTab() == (s32)SCT_Sprites)// we are editing the sprites : edit the current sprite key
						{
							s32 index = m_SpriteListBox->getSelected();
							if(index >= 0 && (u32)index < dlg->GetSpritesData().size())
							{
								s32 indexK = m_SpriteKeysListBox->getSelected();
								TArray<KeyTime>& keys = dlg->GetSpritesData()[index].m_Keys;
								if(indexK >= 0 && (u32)indexK < keys.size())
								{
									key = &(keys[indexK]);
								}
							}
						}
					}
					if(key)
					{
						if(m_ShiftIsPressed)
						{
							float scale = key->m_Scale;
							scale -= (newPos.Y - m_LayerCurrentCursorPos.Y)*0.01f;
							key->m_Scale = core::max_(scale, 0.0f);
						}
						else if(m_CtrlIsPressed)
						{
							float rot = key->m_Rotation;
							rot += newPos.Y - m_LayerCurrentCursorPos.Y;
							key->m_Rotation = rot;
						}
						else
						{
							Position2 pos = key->m_Position;
							pos.X += (s32)((newPos.X - m_LayerCurrentCursorPos.X) / Engine::GetInstance()->m_InnerScale);
							pos.Y += (s32)((newPos.Y - m_LayerCurrentCursorPos.Y) / Engine::GetInstance()->m_InnerScale);
							key->m_Position = pos;
						}

						// Update the view
						if(m_TabControl->getActiveTab() == (s32)SCT_Background)
						{
							m_DialogBGKeyPosXSpinBox->setValue((f32)key->m_Position.X);
							m_DialogBGKeyPosYSpinBox->setValue((f32)key->m_Position.Y);
							m_DialogBGKeyScaleSpinBox->setValue((f32)key->m_Scale);
							m_DialogBGKeyRotationSpinBox->setValue((f32)key->m_Rotation);
							m_DialogBGKeyColorTextBox->setBackgroundColor(key->m_Color);
							m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", key->m_Color).c_str());
							IColor textColor = MathUtils::GetInverseColor(key->m_Color);
							textColor.setAlpha(200);
							m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
							m_DialogBGKeyColorASpinBox->setValue((f32)key->m_Color.getAlpha());
							m_DialogBGKeyColorRSpinBox->setValue((f32)key->m_Color.getRed());
							m_DialogBGKeyColorGSpinBox->setValue((f32)key->m_Color.getGreen());
							m_DialogBGKeyColorBSpinBox->setValue((f32)key->m_Color.getBlue());
						}
						else if(m_TabControl->getActiveTab() == (s32)SCT_Sprites)
						{
							m_SpriteKeyPosXSpinBox->setValue((f32)key->m_Position.X);
							m_SpriteKeyPosYSpinBox->setValue((f32)key->m_Position.Y);
							m_SpriteKeyScaleSpinBox->setValue((f32)key->m_Scale);
							m_SpriteKeyRotationSpinBox->setValue((f32)key->m_Rotation);
							m_SpriteKeyColorTextBox->setBackgroundColor(key->m_Color);
							m_SpriteKeyColorTextBox->setText(StringFormat(L"%08X", key->m_Color).c_str());
							IColor textColor = MathUtils::GetInverseColor(key->m_Color);
							textColor.setAlpha(200);
							m_SpriteKeyColorTextBox->setOverrideColor(textColor);
							m_SpriteKeyColorASpinBox->setValue((f32)key->m_Color.getAlpha());
							m_SpriteKeyColorRSpinBox->setValue((f32)key->m_Color.getRed());
							m_SpriteKeyColorGSpinBox->setValue((f32)key->m_Color.getGreen());
							m_SpriteKeyColorBSpinBox->setValue((f32)key->m_Color.getBlue());
						}
					}
					m_LayerCurrentCursorPos = newPos;
				}
			}
		}
	}
// 	else if(event)
// 	{
// 		m_ViewerBackground->getRelativePosition().getSize();
// 	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		DialogNode* currentDlg = m_SceneInterface->GetCurrentDialog();
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}
		else if(event.GUIEvent.EventType == gui::EGET_SCROLL_BAR_CHANGED)
		{
			if(event.GUIEvent.Caller == m_ViewerHScrollBar)
			{
				m_ViewerPosition.Y = m_ViewerHScrollBar->getPos();
			}
			else if(event.GUIEvent.Caller == m_ViewerWScrollBar)
			{
				m_ViewerPosition.X = m_ViewerWScrollBar->getPos();
			}
			else if(event.GUIEvent.Caller == m_TimeControlScrollBar)
			{
				if(currentDlg)
				{
					if(!m_IsPlaying)
					{
						m_CurrentDlgTime = (float)m_TimeControlScrollBar->getPos()*0.01f + core::ROUNDING_ERROR_f32;
						float tMax = currentDlg->GetDialogTotalDuration(false, true);// max depends of the dialog length
						if(m_CurrentDlgTime > tMax)
							m_CurrentDlgTime = tMax;

						m_TimeControlScrollBar->setMax((s32)(tMax*100.0f));
						m_TimeControlScrollBar->setPos((s32)(m_CurrentDlgTime*100.0f));

						char str[32];
						sprintf(str, "%.2f", m_CurrentDlgTime);
						m_TimeText->setText(core::stringw(str).c_str());
					}
				}
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_COMBO_BOX_CHANGED)
		{
			if(event.GUIEvent.Caller == m_LanguageTypeComboBox)
			{
				s32 type = m_LanguageTypeComboBox->getSelected();
				assert(type>=0 && type<(s32)Engine::LS_MAX);
				Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)type);
				OnDialogChange();
			}
			else if(currentDlg)
			{
				if(event.GUIEvent.Caller == m_DialogTypeComboBox)
				{
					s32 type = m_DialogTypeComboBox->getSelected();
					assert(type>=0 && type<(s32)DialogNode::DN_MAX);
					currentDlg->SetNodeType((DialogNode::DN_Type)type);

					// manage conditional texts activation or not
					s32 index = m_DialogChildListBox->getSelected();
					if(index >= 0 && (u32)index < currentDlg->GetConditionalTexts(Engine::GetInstance()->GetLanguage()).size() &&
						currentDlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
					{
						m_DialogChoiceTextBox->setEnabled(true);
						const core::stringw text = m_DialogChoiceTextBox->getText();
						m_DialogChoiceTextBox->setText(currentDlg->GetConditionalTexts(Engine::GetInstance()->GetLanguage())[index].c_str());
					}
					else
					{
						m_DialogChoiceTextBox->setEnabled(false);
						for(u32 i = 0; i < currentDlg->GetConditionalTexts(Engine::GetInstance()->GetLanguage()).size(); ++i)
							currentDlg->GetConditionalTexts(Engine::GetInstance()->GetLanguage())[i] = L"";
						m_DialogChoiceTextBox->setText(L"");
					}

					// manage inputs activation or not
					if(currentDlg->GetNodeType() == DialogNode::DN_QTE_VALID)
					{
						m_DialogInputsListBox->setEnabled(true);
						m_DialogInputsListBox->clear();
						if(currentDlg->GetInputs().empty())// empty ? create a default input
						{
							InputData input;
							input.SetPressed(InputData::PI_ACTION5, true);
							currentDlg->AddInput(input);
						}
						for(u32 i=0; i<currentDlg->GetInputs().size(); ++i)
							m_DialogInputsListBox->addItem(core::stringw(currentDlg->GetInputs()[i].m_Duration).c_str());
					}
					else
					{
						m_DialogInputsListBox->setEnabled(false);
						m_DialogInputsListBox->clear();
						currentDlg->GetInputs().clear();// we can't have inputs in non QTE type
					}

					m_DialogInputsListBox->setSelected(-1);
					m_DialogInputDurationSpinBox->setEnabled(false);
					m_DialogInputNbPressSpinBox->setEnabled(false);
					m_DialogInputDurationSpinBox->setValue(0.0f);
					m_DialogInputNbPressSpinBox->setValue(0.0f);
					for(u32 j = 0; j < (u32)InputData::PI_MAX; ++j)
					{
						m_DialogInputsCheckBox[j]->setEnabled(false);
						m_DialogInputsCheckBox[j]->setChecked(false);
					}
				}
				else if(event.GUIEvent.Caller == m_TransitionInTypeComboBox)
				{
					s32 type = m_TransitionInTypeComboBox->getSelected();
					assert(type>=0 && type<(s32)DialogNode::DN_TRANSITION_MAX);
					DialogNode::TransitionData trans;
					trans.m_Type = (DialogNode::DN_TransitionType)type;
					trans.m_Duration = currentDlg->GetTransitionIn().m_Duration;
					currentDlg->SetTransitionIn(trans);
				}
				else if(event.GUIEvent.Caller == m_TransitionOutTypeComboBox)
				{
					s32 type = m_TransitionOutTypeComboBox->getSelected();
					assert(type>=0 && type<(s32)DialogNode::DN_TRANSITION_MAX);
					DialogNode::TransitionData trans;
					trans.m_Type = (DialogNode::DN_TransitionType)type;
					trans.m_Duration = currentDlg->GetTransitionOut().m_Duration;
					currentDlg->SetTransitionOut(trans);
				}
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_SPINBOX_CHANGED)
		{
			bool renderChanged = false;
			if(event.GUIEvent.Caller == m_RenderWidthSpinBox)
			{
				u32 width = (u32)m_RenderWidthSpinBox->getValue();
				Engine::GetInstance()->m_RenderSize.Width = width;
				renderChanged = true;
			}
			else if(event.GUIEvent.Caller == m_RenderHeightSpinBox)
			{
				u32 height = (u32)m_RenderHeightSpinBox->getValue();
				Engine::GetInstance()->m_RenderSize.Height = height;
				renderChanged = true;
			}
			else if(currentDlg)
			{
				if(event.GUIEvent.Caller == m_DialogSpeedSpinBox)
				{
					f32 speed = core::clamp(m_DialogSpeedSpinBox->getValue(), 0.0f, 1000.0f);
					currentDlg->SetDialogSpeed(speed);
				}
				if(event.GUIEvent.Caller == m_DialogDurationSpinBox)
				{
					f32 duration = core::clamp(m_DialogDurationSpinBox->getValue(), 0.0f, 1000.0f);
					currentDlg->SetDialogDuration(duration);
				}
				else if(event.GUIEvent.Caller == m_DialogPosXSpinBox)
				{
					f32 val = core::clamp(m_DialogPosXSpinBox->getValue(), 0.0f, 1.0f);
					Vector2 vec = currentDlg->GetDialogPosition();
					vec.X = val;
					currentDlg->SetDialogPosition(vec);
				}
				else if(event.GUIEvent.Caller == m_DialogPosYSpinBox)
				{
					f32 val = core::clamp(m_DialogPosYSpinBox->getValue(), 0.0f, 1.0f);
					Vector2 vec = currentDlg->GetDialogPosition();
					vec.Y = val;
					currentDlg->SetDialogPosition(vec);
				}
				else if(event.GUIEvent.Caller == m_DialogSizeXSpinBox)
				{
					f32 val = core::clamp(m_DialogSizeXSpinBox->getValue(), 0.0f, 1.0f);
					Vector2 vec = currentDlg->GetDialogSize();
					vec.X = val;
					currentDlg->SetDialogSize(vec);
				}
				else if(event.GUIEvent.Caller == m_DialogSizeYSpinBox)
				{
					f32 val = core::clamp(m_DialogSizeYSpinBox->getValue(), 0.0f, 1.0f);
					Vector2 vec = currentDlg->GetDialogSize();
					vec.Y = val;
					currentDlg->SetDialogSize(vec);
				}
				else if(event.GUIEvent.Caller == m_DialogInputDurationSpinBox)
				{
					s32 index = m_DialogInputsListBox->getSelected();
					TArray<InputData>& inputs = currentDlg->GetInputs();
					if(index >= 0 && (u32)index < inputs.size())
					{
						f32 duration = core::clamp(m_DialogInputDurationSpinBox->getValue(), -100.0f, 100.0f);
						inputs[index].m_Duration = duration;

						m_DialogInputsListBox->setItem(index, core::stringw(inputs[index].m_Duration).c_str(), -1);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogInputNbPressSpinBox)
				{
					s32 index = m_DialogInputsListBox->getSelected();
					TArray<InputData>& inputs = currentDlg->GetInputs();
					if(index >= 0 && (u32)index < inputs.size())
					{
						u8 nbPress = core::clamp((u8)m_DialogInputNbPressSpinBox->getValue(), (u8)1, (u8)255);
						inputs[index].m_NbPress = nbPress;
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyTimeSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						f32 time = core::clamp(m_DialogBGKeyTimeSpinBox->getValue(), 0.0f, 1000.0f);
						keys[index].m_Time = time;
						KeyTime key = keys[index];
						keys.set_sorted(false);
						keys.sort();
						m_DialogBackgroundKeysListBox->clear();
						for(u32 i=0; i<keys.size(); ++i)
						{
							core::stringw str = L"";
							str += keys[i].m_Time;
							m_DialogBackgroundKeysListBox->addItem(str.c_str());
						}
						index = keys.binary_search(key);
						m_DialogBackgroundKeysListBox->setSelected(index);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyPosXSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						float X = (float)m_DialogBGKeyPosXSpinBox->getValue();
						keys[index].m_Position.X = (s32)X;
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyPosYSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						float Y = (float)m_DialogBGKeyPosYSpinBox->getValue();
						keys[index].m_Position.Y = (s32)Y;
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyScaleSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						float scale = (float)m_DialogBGKeyScaleSpinBox->getValue();
						keys[index].m_Scale = scale;
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyRotationSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						float rotation = (float)m_DialogBGKeyRotationSpinBox->getValue();
						keys[index].m_Rotation = rotation;
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyColorASpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						u32 alpha = (u32)m_DialogBGKeyColorASpinBox->getValue();
						keys[index].m_Color.setAlpha(alpha);

						m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
						m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
						IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
						textColor.setAlpha(200);
						m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyColorRSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						u32 red = (u32)m_DialogBGKeyColorRSpinBox->getValue();
						keys[index].m_Color.setRed(red);

						m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
						m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
						IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
						textColor.setAlpha(200);
						m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyColorGSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						u32 green = (u32)m_DialogBGKeyColorGSpinBox->getValue();
						keys[index].m_Color.setGreen(green);

						m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
						m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
						IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
						textColor.setAlpha(200);
						m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeyColorBSpinBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					TArray<KeyTime>& keys = currentDlg->GetImageScene()->m_Keys;
					if(index >= 0 && (u32)index < keys.size())
					{
						u32 blue = (u32)m_DialogBGKeyColorBSpinBox->getValue();
						keys[index].m_Color.setBlue(blue);

						m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
						m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
						IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
						textColor.setAlpha(200);
						m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogScreenShakeStrengthSpinBox)
				{
					f32 strengh = core::clamp(m_DialogScreenShakeStrengthSpinBox->getValue(), 0.0f, 10.0f);
					currentDlg->SetScreenShakeStrength(strengh);
				}
				else if(event.GUIEvent.Caller == m_DialogScreenShakeFrequencySpinBox)
				{
					f32 freq = core::clamp(m_DialogScreenShakeFrequencySpinBox->getValue(), 0.0f, 10.0f);
					currentDlg->SetScreenShakeFrequency(freq);
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyTimeSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							f32 time = core::clamp(m_SpriteKeyTimeSpinBox->getValue(), 0.0f, 1000.0f);
							keys[index].m_Time = time;
							KeyTime key = keys[index];
							keys.set_sorted(false);
							keys.sort();
							m_SpriteKeysListBox->clear();
							for(u32 i=0; i<keys.size(); ++i)
							{
								core::stringw str = L"";
								str += keys[i].m_Time;
								m_SpriteKeysListBox->addItem(str.c_str());
							}
							index = keys.binary_search(key);
							m_SpriteKeysListBox->setSelected(index);

							if(!m_IsPlaying)
							{
								m_CurrentDlgTime = key.m_Time + core::ROUNDING_ERROR_f32;
								float tMax = currentDlg->GetDialogTotalDuration(false, true);// max depends of the dialog length
								if(m_CurrentDlgTime > tMax)
									m_CurrentDlgTime = tMax;

								m_TimeControlScrollBar->setMax((s32)(tMax*100.0f));
								m_TimeControlScrollBar->setPos((s32)(m_CurrentDlgTime*100.0f));

								char str[32];
								sprintf(str, "%.2f", m_CurrentDlgTime);
								m_TimeText->setText(core::stringw(str).c_str());
							}
						}				
					}	
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyPosXSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							float X = (float)m_SpriteKeyPosXSpinBox->getValue();
							keys[index].m_Position.X = (s32)X;
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyPosYSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							float Y = (float)m_SpriteKeyPosYSpinBox->getValue();
							keys[index].m_Position.Y = (s32)Y;
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyScaleSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							float scale = (float)m_SpriteKeyScaleSpinBox->getValue();
							keys[index].m_Scale = scale;
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyRotationSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							float rotation = (float)m_SpriteKeyRotationSpinBox->getValue();
							keys[index].m_Rotation = rotation;
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyColorASpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							u32 alpha = (u32)m_SpriteKeyColorASpinBox->getValue();
							keys[index].m_Color.setAlpha(alpha);

							m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
							m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
							IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
							textColor.setAlpha(200);
							m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyColorRSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							u32 red = (u32)m_SpriteKeyColorRSpinBox->getValue();
							keys[index].m_Color.setRed(red);

							m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
							m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
							IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
							textColor.setAlpha(200);
							m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyColorGSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							u32 green = (u32)m_SpriteKeyColorGSpinBox->getValue();
							keys[index].m_Color.setGreen(green);

							m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
							m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
							IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
							textColor.setAlpha(200);
							m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeyColorBSpinBox)
				{
					s32 indexS = m_SpriteListBox->getSelected();
					SpriteDataScene* data = currentDlg->GetSpriteDataAtIndex(indexS);
					if(data)
					{
						s32 index = m_SpriteKeysListBox->getSelected();
						TArray<KeyTime>& keys = data->m_Keys;
						if(index >= 0 && (u32)index < keys.size())
						{
							u32 blue = (u32)m_SpriteKeyColorBSpinBox->getValue();
							keys[index].m_Color.setBlue(blue);

							m_DialogBGKeyColorTextBox->setBackgroundColor(keys[index].m_Color);
							m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", keys[index].m_Color).c_str());
							IColor textColor = MathUtils::GetInverseColor(keys[index].m_Color);
							textColor.setAlpha(200);
							m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
						}
					}
				}
				else if(event.GUIEvent.Caller == m_TransitionInDurationSpinBox)
				{
					f32 duration =  core::clamp(m_TransitionInDurationSpinBox->getValue(), 0.0f, 10.0f);
					DialogNode::TransitionData trans;
					trans.m_Type = currentDlg->GetTransitionIn().m_Type;
					trans.m_Duration = duration;
					currentDlg->SetTransitionIn(trans);
				}
				else if(event.GUIEvent.Caller == m_TransitionOutDurationSpinBox)
				{
					f32 duration =  core::clamp(m_TransitionOutDurationSpinBox->getValue(), 0.0f, 10.0f);
					DialogNode::TransitionData trans;
					trans.m_Type = currentDlg->GetTransitionOut().m_Type;
					trans.m_Duration = duration;
					currentDlg->SetTransitionOut(trans);
				}
				else if(event.GUIEvent.Caller == m_AudioBG1VolumeSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
					if(sound.IsValid())
					{
						f32 vol = core::clamp(0.01f*m_AudioBG1VolumeSpinBox->getValue(), 0.0f, 1.0f);
						sound->m_StreamData.m_Volume = vol;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG1PitchSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
					if(sound.IsValid())
					{
						f32 pitch = (m_AudioBG1PitchSpinBox->getValue() < 0.0f)? 1.0f+0.005f*m_AudioBG1PitchSpinBox->getValue() : 
							1.0f+0.01f*m_AudioBG1PitchSpinBox->getValue();
						pitch = core::clamp(pitch, 0.5f, 2.0f);
						sound->m_StreamData.m_Pitch = pitch;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG1PanSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
					if(sound.IsValid())
					{
						f32 pan = core::clamp(0.01f*m_AudioBG1PanSpinBox->getValue(), -1.0f, 1.0f);
						sound->m_StreamData.m_Pan = pan;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG2VolumeSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG2();
					if(sound.IsValid())
					{
						f32 vol = core::clamp(0.01f*m_AudioBG2VolumeSpinBox->getValue(), 0.0f, 1.0f);
						sound->m_StreamData.m_Volume = vol;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG2PitchSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG2();
					if(sound.IsValid())
					{
						f32 pitch = (m_AudioBG2PitchSpinBox->getValue() < 0.0f)? 1.0f+0.005f*m_AudioBG2PitchSpinBox->getValue() : 
							1.0f+0.01f*m_AudioBG2PitchSpinBox->getValue();
					pitch = core::clamp(pitch, 0.5f, 2.0f);
					sound->m_StreamData.m_Pitch = pitch;
					Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
						sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
						sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG2PanSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG2();
					if(sound.IsValid())
					{
						f32 pan = core::clamp(0.01f*m_AudioBG2PanSpinBox->getValue(), -1.0f, 1.0f);
						sound->m_StreamData.m_Pan = pan;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioVoiceVolumeSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetVoice();
					if(sound.IsValid())
					{
						f32 vol = core::clamp(0.01f*m_AudioVoiceVolumeSpinBox->getValue(), 0.0f, 1.0f);
						sound->m_StreamData.m_Volume = vol;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioVoicePitchSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetVoice();
					if(sound.IsValid())
					{
						f32 pitch = (m_AudioVoicePitchSpinBox->getValue() < 0.0f)? 1.0f+0.005f*m_AudioVoicePitchSpinBox->getValue() : 
							1.0f+0.01f*m_AudioVoicePitchSpinBox->getValue();
					pitch = core::clamp(pitch, 0.5f, 2.0f);
					sound->m_StreamData.m_Pitch = pitch;
					Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
						sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
						sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioVoicePanSpinBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetVoice();
					if(sound.IsValid())
					{
						f32 pan = core::clamp(0.01f*m_AudioVoicePanSpinBox->getValue(), -1.0f, 1.0f);
						sound->m_StreamData.m_Pan = pan;
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
			}

			if(renderChanged)
			{
				ProfileData::GetInstance()->m_ODS.m_RenderSize = Engine::GetInstance()->m_RenderSize;
				ProfileData::GetInstance()->WriteSave();

				Engine::GetInstance()->RecomputeSize();
				ChangeRenderTargetSize();
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_EDITBOX_CHANGED)
		{
			if(currentDlg)
			{
				if(event.GUIEvent.Caller == m_DialogHeaderBox)
				{
					u32 languageIndex = Engine::GetInstance()->GetLanguage();
					const core::stringw text = m_DialogHeaderBox->getText();
					currentDlg->SetDialogHeader(text, languageIndex);
				}
				else if(event.GUIEvent.Caller == m_DialogTextBox)
				{
					u32 languageIndex = Engine::GetInstance()->GetLanguage();
					const core::stringw text = m_DialogTextBox->getText();
					currentDlg->SetDialogText(text, languageIndex);
				}
				else if(event.GUIEvent.Caller == m_DialogChoiceTextBox)
				{
					u32 languageIndex = Engine::GetInstance()->GetLanguage();
					s32 index = m_DialogChildListBox->getSelected();
					if(index >= 0 && (u32)index < currentDlg->GetConditionalTexts(languageIndex).size() &&
						currentDlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
					{
						const core::stringw text = m_DialogChoiceTextBox->getText();
						currentDlg->GetConditionalTexts(languageIndex)[index] = text;
					}
				}
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_LISTBOX_CHANGED  || event.GUIEvent.EventType == gui::EGET_LISTBOX_SELECTED_AGAIN)
		{
			if(currentDlg)
			{
				if(event.GUIEvent.Caller == m_DialogChildListBox)
				{
					u32 languageIndex = Engine::GetInstance()->GetLanguage();
					s32 index = m_DialogChildListBox->getSelected();
					if(index >= 0 && (u32)index < currentDlg->GetConditionalTexts(languageIndex).size() &&
						currentDlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
					{
						m_DialogChoiceTextBox->setEnabled(true);
						const core::stringw text = m_DialogChoiceTextBox->getText();
						m_DialogChoiceTextBox->setText(currentDlg->GetConditionalTexts(languageIndex)[index].c_str());
					}
					else
					{
						m_DialogChoiceTextBox->setEnabled(false);
					}
				}
				else if(event.GUIEvent.Caller == m_DialogInputsListBox)
				{
					s32 index = m_DialogInputsListBox->getSelected();
					if(index >= 0 && (u32)index < currentDlg->GetInputs().size() &&
						currentDlg->GetNodeType() == DialogNode::DN_QTE_VALID)
					{
						m_DialogInputsListBox->setSelected(index);
						m_DialogInputDurationSpinBox->setEnabled(true);
						m_DialogInputNbPressSpinBox->setEnabled(true);
						m_DialogInputDurationSpinBox->setValue(currentDlg->GetInputs()[index].m_Duration);
						m_DialogInputNbPressSpinBox->setValue(currentDlg->GetInputs()[index].m_NbPress);
						for(u32 j = 0; j < (u32)InputData::PI_MAX; ++j)
						{
							m_DialogInputsCheckBox[j]->setEnabled(true);
							m_DialogInputsCheckBox[j]->setChecked(currentDlg->GetInputs()[index].IsPressed((InputData::EPadInput)j));
						}
					}
				}
				else if(event.GUIEvent.Caller == m_DialogBackgroundKeysListBox)
				{
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					if(index >= 0 && (u32)index < currentDlg->GetImageScene()->m_Keys.size())
					{
						const KeyTime& key = currentDlg->GetImageScene()->m_Keys[index];
						m_DialogBGKeyTimeSpinBox->setValue(key.m_Time);
						m_DialogBGKeyPosXSpinBox->setValue((float)key.m_Position.X);
						m_DialogBGKeyPosYSpinBox->setValue((float)key.m_Position.Y);
						m_DialogBGKeyScaleSpinBox->setValue((float)key.m_Scale);
						m_DialogBGKeyRotationSpinBox->setValue((float)key.m_Rotation);
						m_DialogBGKeyColorTextBox->setBackgroundColor(key.m_Color);
						m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", key.m_Color).c_str());
						IColor textColor = MathUtils::GetInverseColor(key.m_Color);
						textColor.setAlpha(200);
						m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
						m_DialogBGKeyColorASpinBox->setValue((f32)key.m_Color.getAlpha());
						m_DialogBGKeyColorRSpinBox->setValue((f32)key.m_Color.getRed());
						m_DialogBGKeyColorGSpinBox->setValue((f32)key.m_Color.getGreen());
						m_DialogBGKeyColorBSpinBox->setValue((f32)key.m_Color.getBlue());

						if(!m_IsPlaying)
						{
							m_CurrentDlgTime = key.m_Time + core::ROUNDING_ERROR_f32;
							float tMax = currentDlg->GetDialogTotalDuration(false, true);// max depends of the dialog length
							if(m_CurrentDlgTime > tMax)
								m_CurrentDlgTime = tMax;

							m_TimeControlScrollBar->setMax((s32)(tMax*100.0f));
							m_TimeControlScrollBar->setPos((s32)(m_CurrentDlgTime*100.0f));

							char str[32];
							sprintf(str, "%.2f", m_CurrentDlgTime);
							m_TimeText->setText(core::stringw(str).c_str());
						}
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteListBox)
				{
					s32 index = m_SpriteListBox->getSelected();
					if(currentDlg && index >= 0 && (u32)index < currentDlg->GetSpritesData().size())
					{
						TArray<KeyTime>& keys = currentDlg->GetSpritesData()[index].m_Keys;

						m_SpriteKeysListBox->clear();
						for(u32 i=0; i<keys.size(); ++i)
						{
							core::stringw str = L"";
							str += keys[i].m_Time;
							m_SpriteKeysListBox->addItem(str.c_str());
						}
					}
					m_SpriteKeysListBox->setSelected(0);
					OnSpriteSelectionChanged();
				}
				else if(event.GUIEvent.Caller == m_SpriteKeysListBox)
				{
					OnSpriteSelectionChanged();
				}
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED)
		{
			if(currentDlg)
			{
				if(event.GUIEvent.Caller == m_ButtonPlay)
				{
					m_IsPlaying = !m_IsPlaying;
					if(m_IsPlaying)
					{
						if(currentDlg->GetSoundBG1().IsValid())
							Engine::GetInstance()->GetAudioDevice()->SetupSound(currentDlg->GetSoundBG1()->m_Sound, 
								currentDlg->GetSoundBG1()->m_StreamData.m_Pan, 
								currentDlg->GetSoundBG1()->m_StreamData.m_Pitch, 
								currentDlg->GetSoundBG1()->m_StreamData.m_Volume, 
								currentDlg->GetSoundBG1()->m_StreamData.m_Repeat, 
								currentDlg->GetSoundBG1()->m_StreamData.m_StopAtDialogsEnd);
						if(currentDlg->GetSoundBG2().IsValid())
							Engine::GetInstance()->GetAudioDevice()->SetupSound(currentDlg->GetSoundBG2()->m_Sound, 
								currentDlg->GetSoundBG2()->m_StreamData.m_Pan, 
								currentDlg->GetSoundBG2()->m_StreamData.m_Pitch, 
								currentDlg->GetSoundBG2()->m_StreamData.m_Volume, 
								currentDlg->GetSoundBG2()->m_StreamData.m_Repeat, 
								currentDlg->GetSoundBG2()->m_StreamData.m_StopAtDialogsEnd);
						if(currentDlg->GetVoice().IsValid())
							Engine::GetInstance()->GetAudioDevice()->SetupSound(currentDlg->GetVoice()->m_Sound, 
								currentDlg->GetVoice()->m_StreamData.m_Pan, 
								currentDlg->GetVoice()->m_StreamData.m_Pitch, 
								currentDlg->GetVoice()->m_StreamData.m_Volume, 
								currentDlg->GetVoice()->m_StreamData.m_Repeat, 
								currentDlg->GetVoice()->m_StreamData.m_StopAtDialogsEnd);
						m_ButtonPlay->setText(L"Pause");
					}
					else
					{
						m_ButtonPlay->setText(L"Jouer");
					}

					if(m_IsPlayingSoundPreview)// stop sound previews
					{
						if(currentDlg->GetSoundBG1().IsValid())
							if(currentDlg->GetSoundBG1()->m_Sound.IsValid() && currentDlg->GetSoundBG1()->m_Sound->IsValid())
								currentDlg->GetSoundBG1()->m_Sound->Stop();
						if(currentDlg->GetSoundBG2().IsValid())
							if(currentDlg->GetSoundBG2()->m_Sound.IsValid() && currentDlg->GetSoundBG2()->m_Sound->IsValid())
								currentDlg->GetSoundBG2()->m_Sound->Stop();
						if(currentDlg->GetVoice().IsValid())
							if(currentDlg->GetVoice()->m_Sound.IsValid() && currentDlg->GetVoice()->m_Sound->IsValid())
								currentDlg->GetVoice()->m_Sound->Stop();

						m_AudioBGPlayButton1->setText(L"Jouer");
						m_AudioBGPlayButton2->setText(L"Jouer");
						m_AudioVoicePlayButton->setText(L"Jouer");
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBGBrowseButton1)
				{
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sounds);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un son à utiliser");
					m_FileLoadType = FLT_SOUND_BG1;
				}
				else if(event.GUIEvent.Caller == m_AudioBGBrowseButton2)
				{
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sounds);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un son à utiliser");
					m_FileLoadType = FLT_SOUND_BG2;
				}
				else if(event.GUIEvent.Caller == m_AudioVoiceBrowseButton)
				{
					m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Sounds);
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un son à utiliser");
					m_FileLoadType = FLT_SOUND_VOICE;
				}
				else if(event.GUIEvent.Caller == m_AudioBGPlayButton1 && !m_IsPlaying)
				{
					bool isValid = currentDlg->GetSoundBG1().IsValid() && 
						currentDlg->GetSoundBG1()->m_Sound.IsValid() && currentDlg->GetSoundBG1()->m_Sound->IsValid();
					if(isValid)
					{
						m_IsPlayingSoundPreview = isValid && !currentDlg->GetSoundBG1()->m_Sound->IsPlaying();
						m_AudioBGPlayButton1->setText(m_IsPlayingSoundPreview? L"Stop" : L"Jouer");
						m_AudioBGPlayButton2->setText(L"Jouer");
						m_AudioVoicePlayButton->setText(L"Jouer");
						if(m_IsPlayingSoundPreview)// stop others
						{
							if(currentDlg->GetSoundBG2().IsValid())
								if(currentDlg->GetSoundBG2()->m_Sound.IsValid() && currentDlg->GetSoundBG2()->m_Sound->IsValid())
									currentDlg->GetSoundBG2()->m_Sound->Stop();
							if(currentDlg->GetVoice().IsValid())
								if(currentDlg->GetVoice()->m_Sound.IsValid() && currentDlg->GetVoice()->m_Sound->IsValid())
									currentDlg->GetVoice()->m_Sound->Stop();
						}

						if(m_IsPlayingSoundPreview)
						{
							if(currentDlg->GetSoundBG1().IsValid())
								Engine::GetInstance()->GetAudioDevice()->SetupSound(currentDlg->GetSoundBG1()->m_Sound, 
									currentDlg->GetSoundBG1()->m_StreamData.m_Pan, 
									currentDlg->GetSoundBG1()->m_StreamData.m_Pitch, 
									currentDlg->GetSoundBG1()->m_StreamData.m_Volume, 
									currentDlg->GetSoundBG1()->m_StreamData.m_Repeat, 
									currentDlg->GetSoundBG1()->m_StreamData.m_StopAtDialogsEnd);
							
							currentDlg->GetSoundBG1()->m_Sound->Reset();
							currentDlg->GetSoundBG1()->m_Sound->Play();
						}
						else
							currentDlg->GetSoundBG1()->m_Sound->Stop();
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBGPlayButton2 && !m_IsPlaying)
				{
					bool isValid = currentDlg->GetSoundBG2().IsValid() && 
						currentDlg->GetSoundBG2()->m_Sound.IsValid() && currentDlg->GetSoundBG2()->m_Sound->IsValid();
					if(isValid)
					{
						m_IsPlayingSoundPreview = isValid && !currentDlg->GetSoundBG2()->m_Sound->IsPlaying();
						m_AudioBGPlayButton2->setText(m_IsPlayingSoundPreview? L"Stop" : L"Jouer");
						m_AudioBGPlayButton1->setText(L"Jouer");
						m_AudioVoicePlayButton->setText(L"Jouer");
						if(m_IsPlayingSoundPreview)// stop others
						{
							if(currentDlg->GetSoundBG1().IsValid())
								if(currentDlg->GetSoundBG1()->m_Sound.IsValid() && currentDlg->GetSoundBG1()->m_Sound->IsValid())
									currentDlg->GetSoundBG1()->m_Sound->Stop();
							if(currentDlg->GetVoice().IsValid())
								if(currentDlg->GetVoice()->m_Sound.IsValid() && currentDlg->GetVoice()->m_Sound->IsValid())
									currentDlg->GetVoice()->m_Sound->Stop();
						}
						if(m_IsPlayingSoundPreview)
						{
							if(currentDlg->GetSoundBG2().IsValid())
								Engine::GetInstance()->GetAudioDevice()->SetupSound(currentDlg->GetSoundBG2()->m_Sound, 
									currentDlg->GetSoundBG2()->m_StreamData.m_Pan, 
									currentDlg->GetSoundBG2()->m_StreamData.m_Pitch, 
									currentDlg->GetSoundBG2()->m_StreamData.m_Volume, 
									currentDlg->GetSoundBG2()->m_StreamData.m_Repeat, 
									currentDlg->GetSoundBG2()->m_StreamData.m_StopAtDialogsEnd);

							currentDlg->GetSoundBG2()->m_Sound->Reset();
							currentDlg->GetSoundBG2()->m_Sound->Play();
						}
						else
							currentDlg->GetSoundBG2()->m_Sound->Stop();
					}
				}
				else if(event.GUIEvent.Caller == m_AudioVoicePlayButton && !m_IsPlaying)
				{
					bool isValid = currentDlg->GetVoice().IsValid() && 
						currentDlg->GetVoice()->m_Sound.IsValid() && currentDlg->GetVoice()->m_Sound->IsValid();
					if(isValid)
					{
						m_IsPlayingSoundPreview = isValid && !currentDlg->GetVoice()->m_Sound->IsPlaying();
						m_AudioVoicePlayButton->setText(m_IsPlayingSoundPreview? L"Stop" : L"Jouer");
						m_AudioBGPlayButton1->setText(L"Jouer");
						m_AudioBGPlayButton2->setText(L"Jouer");
						if(m_IsPlayingSoundPreview)// stop others
						{
							if(currentDlg->GetSoundBG1().IsValid())
								if(currentDlg->GetSoundBG1()->m_Sound.IsValid() && currentDlg->GetSoundBG1()->m_Sound->IsValid())
									currentDlg->GetSoundBG1()->m_Sound->Stop();
							if(currentDlg->GetSoundBG2().IsValid())
								if(currentDlg->GetSoundBG2()->m_Sound.IsValid() && currentDlg->GetSoundBG2()->m_Sound->IsValid())
									currentDlg->GetSoundBG2()->m_Sound->Stop();
						}
						if(m_IsPlayingSoundPreview)
						{
							if(currentDlg->GetVoice().IsValid())
								Engine::GetInstance()->GetAudioDevice()->SetupSound(currentDlg->GetVoice()->m_Sound, 
									currentDlg->GetVoice()->m_StreamData.m_Pan, 
									currentDlg->GetVoice()->m_StreamData.m_Pitch, 
									currentDlg->GetVoice()->m_StreamData.m_Volume, 
									currentDlg->GetVoice()->m_StreamData.m_Repeat, 
									currentDlg->GetVoice()->m_StreamData.m_StopAtDialogsEnd);

							currentDlg->GetVoice()->m_Sound->Reset();
							currentDlg->GetVoice()->m_Sound->Play();
						}
						else
							currentDlg->GetVoice()->m_Sound->Stop();
					}
				}
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_CHECKBOX_CHANGED)
		{
			if(currentDlg)
			{
				if(event.GUIEvent.Caller == m_DialogTextBGCheckBox)
				{
					currentDlg->SetUseTextBackGround(m_DialogTextBGCheckBox->isChecked());
				}
				else if(event.GUIEvent.Caller == m_DialogBGKeysLoopCheckBox)
				{
					currentDlg->GetImageScene()->m_Loop = m_DialogBGKeysLoopCheckBox->isChecked();
				}
				else if(event.GUIEvent.Caller == m_SpriteMirrorHCheckBox)
				{
					s32 index = m_SpriteListBox->getSelected();
					if(currentDlg && index >= 0 && (u32)index < currentDlg->GetSpritesData().size())
					{
						currentDlg->GetSpritesData()[index].m_MirrorH = m_SpriteMirrorHCheckBox->isChecked();
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteMirrorVCheckBox)
				{
					s32 index = m_SpriteListBox->getSelected();
					if(currentDlg && index >= 0 && (u32)index < currentDlg->GetSpritesData().size())
					{
						currentDlg->GetSpritesData()[index].m_MirrorV = m_SpriteMirrorVCheckBox->isChecked();
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteKeysLoopCheckBox)
				{
					s32 index = m_SpriteListBox->getSelected();
					if(currentDlg && index >= 0 && (u32)index < currentDlg->GetSpritesData().size())
					{
						 currentDlg->GetSpritesData()[index].m_Loop = m_SpriteKeysLoopCheckBox->isChecked();
					}
				}
				else if(event.GUIEvent.Caller == m_SpriteAnimLoopCheckBox)
				{
					s32 index = m_SpriteListBox->getSelected();
					if(currentDlg && index >= 0 && (u32)index < currentDlg->GetSpritesData().size())
					{
						currentDlg->GetSpritesData()[index].m_LoopAnim = m_SpriteAnimLoopCheckBox->isChecked();
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG1RepeatCheckBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
					if(sound.IsValid())
					{
						sound->m_StreamData.m_Repeat = m_AudioBG1RepeatCheckBox->isChecked();
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG1StopCheckBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
					if(sound.IsValid())
					{
						sound->m_StreamData.m_StopAtDialogsEnd = m_AudioBG1StopCheckBox->isChecked();
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG2RepeatCheckBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG2();
					if(sound.IsValid())
					{
						sound->m_StreamData.m_Repeat = m_AudioBG2RepeatCheckBox->isChecked();
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioBG2StopCheckBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG2();
					if(sound.IsValid())
					{
						sound->m_StreamData.m_StopAtDialogsEnd = m_AudioBG2StopCheckBox->isChecked();
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioVoiceRepeatCheckBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetSoundBG1();
					if(sound.IsValid())
					{
						sound->m_StreamData.m_Repeat = m_AudioVoiceRepeatCheckBox->isChecked();
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else if(event.GUIEvent.Caller == m_AudioVoiceStopCheckBox)
				{
					const SmartPointer<SoundDataScene>& sound = currentDlg->GetVoice();
					if(sound.IsValid())
					{
						sound->m_StreamData.m_StopAtDialogsEnd = m_AudioVoiceStopCheckBox->isChecked();
						Engine::GetInstance()->GetAudioDevice()->SetupSound(sound->m_Sound, 
							sound->m_StreamData.m_Pan, sound->m_StreamData.m_Pitch, sound->m_StreamData.m_Volume, 
							sound->m_StreamData.m_Repeat, sound->m_StreamData.m_StopAtDialogsEnd);
					}
				}
				else
				{
					s32 index = m_DialogInputsListBox->getSelected();
					TArray<InputData>& inputs = currentDlg->GetInputs();
					if(index >= 0 && (u32)index < inputs.size())
					{
						for(u32 j = 0; j < (u32)InputData::PI_MAX; ++j)
						{
							if(event.GUIEvent.Caller == m_DialogInputsCheckBox[j])
							{
								currentDlg->GetInputs()[index].SetPressed((InputData::EPadInput)j, m_DialogInputsCheckBox[j]->isChecked());
								break;
							}
						}
					}
				}
			}
		}

		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// New
					m_SceneInterface->NewScene();
					m_SceneInterface->SetCurrentDialog(-1);
					OnSceneChange();
					m_AnimationBankList->OnSceneChange();
					m_DialogList->OnSceneChange();
					break;
				case 1:// Save
					m_SaveFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un Dossier où enregistrer");
					break;
				case 2:// PSVita Export
					m_SceneInterface->m_IsSetForVitaExport = true;
					m_SaveFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un Dossier où exporter");
					break;
				case 3:// Load
					m_LoadFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un fichier à charger");
					break;
				case 5:// texts exports
					if(m_SceneInterface->GetSceneNameForQuickSave() != L"")// if we already saved or load the scene
					{
						m_SceneInterface->m_IsSetForTextsExport = true;
						m_SceneInterface->SaveScene(m_SceneInterface->GetSceneNameForQuickSave());
						m_SceneInterface->m_IsSetForTextsExport = false;

						Rectangle2 rect = getRelativePosition();
						rect = Rectangle2(Position2(rect.getWidth()/2 - 128, rect.getHeight()/2 - 32), Dimension2(256, 64+32));
						m_PopupDialog = m_Gui->addWindow(rect, true, core::stringw(m_SceneInterface->GetSceneNameForQuickSave().c_str()).c_str(), this);
						gui::IGUIStaticText* text = m_Gui->addStaticText(L"Les textes ont bien été exportés", Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_PopupDialog);
						text->setToolTipText(core::stringw(m_SceneInterface->GetSceneNameForQuickSave().c_str()).c_str());
						gui::IGUIButton* btn = m_Gui->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_PopupDialog, GUI_ID_POPUP_DLG, L"OK");
					}
					else// if it is our fisrt save : call the save window
					{
						m_SceneInterface->m_IsSetForVitaExport = true;
						m_SaveFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un Dossier où enregistrer");
					}
					break;
				case 6:// texts imports
					if(m_SceneInterface->GetSceneNameForQuickSave() != L"")// if we already saved or load the scene
					{
						m_SceneInterface->m_IsSetForTextsImport = true;
						m_SceneInterface->SaveScene(m_SceneInterface->GetSceneNameForQuickSave());
						m_SceneInterface->m_IsSetForTextsImport = false;

						Rectangle2 rect = getRelativePosition();
						rect = Rectangle2(Position2(rect.getWidth()/2 - 128, rect.getHeight()/2 - 32), Dimension2(256, 64+32));
						m_PopupDialog = m_Gui->addWindow(rect, true, core::stringw(m_SceneInterface->GetSceneNameForQuickSave().c_str()).c_str(), this);
						gui::IGUIStaticText* text = m_Gui->addStaticText(L"Les textes ont bien été importés", Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_PopupDialog);
						text->setToolTipText(core::stringw(m_SceneInterface->GetSceneNameForQuickSave().c_str()).c_str());
						gui::IGUIButton* btn = m_Gui->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_PopupDialog, GUI_ID_POPUP_DLG, L"OK");
					}
					else// if it is our fisrt save : call the save window
					{
						m_SceneInterface->m_IsSetForTextsImport = true;
						m_SaveFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un Dossier où enregistrer");
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_CHOICE: // dialog choice context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// add
					{
						TArray<DialogNode*>& dlgs = m_SceneInterface->GetScene()->GetDialogs();
						TArray<const wchar_t*> list; 
						for(u32 i=0; i<dlgs.size(); ++i)
						{
// 							DialogNode* dlg = dlgs[i];
// 							assert(dlg);
// 							const wchar_t* str = dlg->GetDialogName().c_str();
							const core::stringw& str = m_SceneInterface->m_Data->m_DialogListNames[i];
							list.push_back(str.c_str());
						}
						ListSelectWindow* wnd = new ListSelectWindow(m_Gui, m_Gui->getRootGUIElement(), -1, Rectangle2(m_DialogChildListBox->getAbsoluteClippingRect().getCenter(), Dimension2(128, 128)), list, m_SceneInterface);
						wnd->drop();
					}
					break;
				case 1:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							s32 index = m_DialogChildListBox->getSelected();
							if(index >= 0)
							{
								dlg->RemoveChildAtIndex((u32)index);
								OnDialogChange();
							}
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_INPUTS: // inputs choice context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// add
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							InputData input;
							dlg->AddInput(input);
							m_DialogInputsListBox->addItem(core::stringw(input.m_Duration).c_str());
						}
					}
					break;
				case 1:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							s32 index = m_DialogInputsListBox->getSelected();
							if(index >= 0)
							{
								dlg->RemoveInputAtIndex((u32)index);
								m_DialogInputsListBox->removeItem((u32)index);

								m_DialogInputsListBox->setSelected(-1);
								m_DialogInputDurationSpinBox->setEnabled(false);
								m_DialogInputNbPressSpinBox->setEnabled(false);
								m_DialogInputDurationSpinBox->setValue(0.0f);
								m_DialogInputNbPressSpinBox->setValue(0.0f);
								for(u32 j = 0; j < (u32)InputData::PI_MAX; ++j)
								{
									m_DialogInputsCheckBox[j]->setEnabled(false);
									m_DialogInputsCheckBox[j]->setChecked(false);
								}
							}
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_TEXTS:
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// apply to all languages
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							// Apply current language content to all languages
							dlg->SetDialogHeader(dlg->GetDialogHeader(Engine::GetInstance()->GetLanguage()), Engine::LS_MAX);
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_TEXTS+1:
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// apply to all languages
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							// Apply current language content to all languages
							dlg->SetDialogText(dlg->GetDialogText(Engine::GetInstance()->GetLanguage()), Engine::LS_MAX);
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_TEXTS+2:
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// apply to all languages
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							// Apply current language content to all languages
							TArray<core::stringw>& textsRef = dlg->GetConditionalTexts(Engine::GetInstance()->GetLanguage());
							for(u32 k = 0; k < Engine::LS_MAX; ++k)
							{
								if(k == Engine::GetInstance()->GetLanguage())
									continue;
								TArray<core::stringw>& texts = dlg->GetConditionalTexts(k);
								s32 index = m_DialogChildListBox->getSelected();
								if(texts.size() == textsRef.size() && index >= 0 && (u32)index < texts.size())
									texts[index] = textsRef[index];
							}
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_BG: // BG context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// add
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg && !dlg->GetImageScene()->m_Image)// only 1 image for BG
						{
							m_SceneInterface->LoadResourcePath(SceneInterface::RPT_Backgrounds);
							m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez une image à utiliser");
							m_FileLoadType = FLT_TEXTURE;
						}
					}
					break;
				case 1:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							s32 index = m_DialogBackgroundListBox->getSelected();
							if(index >= 0)
							{
								dlg->SetImageScene(new ImageScene);
								UpdateBgBank();
							}
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_DIALOG_BG_KEYS: // BG keys context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// add
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg && dlg->GetImageScene()->m_Image)
						{
							TArray<KeyTime>& keys = dlg->GetImageScene()->m_Keys;
							KeyTime key = keys[keys.size()-1];
							keys.push_back(key);

							m_DialogBackgroundKeysListBox->clear();
							for(u32 i=0; i<keys.size(); ++i)
							{
								core::stringw str = L"";
								str += keys[i].m_Time;
								m_DialogBackgroundKeysListBox->addItem(str.c_str());
							}
							m_DialogBackgroundKeysListBox->setSelected(keys.size()-1);

							{
								const KeyTime& key = keys[keys.size()-1];
								m_DialogBGKeyTimeSpinBox->setValue(key.m_Time);
								m_DialogBGKeyPosXSpinBox->setValue((float)key.m_Position.X);
								m_DialogBGKeyPosYSpinBox->setValue((float)key.m_Position.Y);
								m_DialogBGKeyScaleSpinBox->setValue((float)key.m_Scale);
								m_DialogBGKeyRotationSpinBox->setValue((float)key.m_Rotation);
								m_DialogBGKeyColorTextBox->setBackgroundColor(key.m_Color);
								m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", key.m_Color).c_str());
								IColor textColor = MathUtils::GetInverseColor(key.m_Color);
								textColor.setAlpha(200);
								m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
								m_DialogBGKeyColorASpinBox->setValue((f32)key.m_Color.getAlpha());
								m_DialogBGKeyColorRSpinBox->setValue((f32)key.m_Color.getRed());
								m_DialogBGKeyColorGSpinBox->setValue((f32)key.m_Color.getGreen());
								m_DialogBGKeyColorBSpinBox->setValue((f32)key.m_Color.getBlue());
							}
						}
					}
					break;
				case 1:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							s32 index = m_DialogBackgroundKeysListBox->getSelected();
							if(index >= 0)
							{
								dlg->GetImageScene()->m_Keys.erase(index);
								m_DialogBackgroundKeysListBox->removeItem(index);
								m_DialogBackgroundKeysListBox->setSelected(-1);
							}
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_SPRITE_LIST: // Sprite context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							s32 index = m_SpriteListBox->getSelected();
							if(m_SceneInterface->GetScene()->RemoveSpriteFromDialog(dlg, index))
							{
								UpdateSpriteListBank();
							}
						}
					}
					break;
				case 1:// Monter
					{
						s32 itemSelected = m_SpriteListBox->getSelected();
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						TArray< SmartPointer<AnimatedSprite> >& sprites = dlg->GetSprites();
						TArray<SpriteDataScene>& spritesData = dlg->GetSpritesData();
						if(itemSelected >= 1 && sprites.size() >= 2 && (u32)itemSelected < sprites.size())
						{
							SmartPointer<AnimatedSprite> temp = sprites[itemSelected-1];
							sprites[itemSelected-1] = sprites[itemSelected];
							sprites[itemSelected] = temp;
							
							SpriteDataScene tempData = spritesData[itemSelected-1];
							spritesData[itemSelected-1] = spritesData[itemSelected];
							spritesData[itemSelected] = tempData;

							m_SpriteListBox->swapItems(itemSelected, itemSelected-1);
							m_SpriteListBox->setSelected(itemSelected-1);
						}
					}
					break;
				case 2:// descendre
					{
						s32 itemSelected = m_SpriteListBox->getSelected();
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						TArray< SmartPointer<AnimatedSprite> >& sprites = dlg->GetSprites();
						TArray<SpriteDataScene>& spritesData = dlg->GetSpritesData();
						if(itemSelected >= 0 && sprites.size() >= 2 && (u32)itemSelected < sprites.size()-1)
						{
							SmartPointer<AnimatedSprite> tempData = sprites[itemSelected+1];
							sprites[itemSelected+1] = sprites[itemSelected];
							sprites[itemSelected] = tempData;

							SpriteDataScene temp = spritesData[itemSelected+1];
							spritesData[itemSelected+1] = spritesData[itemSelected];
							spritesData[itemSelected] = temp;

							m_SpriteListBox->swapItems(itemSelected, itemSelected+1);
							m_SpriteListBox->setSelected(itemSelected+1);
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_SPRITE_KEYS: // Sprites keys context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// add
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						s32 index = m_SpriteListBox->getSelected();
						if(dlg && index >= 0 && (u32)index < dlg->GetSpritesData().size())
						{
							TArray<KeyTime>& keys = dlg->GetSpritesData()[index].m_Keys;
							KeyTime key = keys[keys.size()-1];
							keys.push_back(key);

							m_SpriteKeysListBox->clear();
							for(u32 i=0; i<keys.size(); ++i)
							{
								core::stringw str = L"";
								str += keys[i].m_Time;
								m_SpriteKeysListBox->addItem(str.c_str());
							}
							m_SpriteKeysListBox->setSelected(keys.size()-1);

							{
								const KeyTime& key = keys[keys.size()-1];
								m_SpriteKeyTimeSpinBox->setValue(key.m_Time);
								m_SpriteKeyPosXSpinBox->setValue((float)key.m_Position.X);
								m_SpriteKeyPosYSpinBox->setValue((float)key.m_Position.Y);
								m_SpriteKeyScaleSpinBox->setValue((float)key.m_Scale);
								m_SpriteKeyRotationSpinBox->setValue((float)key.m_Rotation);
								m_SpriteKeyColorTextBox->setBackgroundColor(key.m_Color);
								m_SpriteKeyColorTextBox->setText(StringFormat(L"%08X", key.m_Color).c_str());
								IColor textColor = MathUtils::GetInverseColor(key.m_Color);
								textColor.setAlpha(200);
								m_SpriteKeyColorTextBox->setOverrideColor(textColor);
								m_SpriteKeyColorASpinBox->setValue((f32)key.m_Color.getAlpha());
								m_SpriteKeyColorRSpinBox->setValue((f32)key.m_Color.getRed());
								m_SpriteKeyColorGSpinBox->setValue((f32)key.m_Color.getGreen());
								m_SpriteKeyColorBSpinBox->setValue((f32)key.m_Color.getBlue());

								if(!m_IsPlaying)
								{
									m_CurrentDlgTime = key.m_Time + core::ROUNDING_ERROR_f32;
									float tMax = dlg->GetDialogTotalDuration(false, true);// max depends of the dialog length
									if(m_CurrentDlgTime > tMax)
										m_CurrentDlgTime = tMax;

									m_TimeControlScrollBar->setMax((s32)(tMax*100.0f));
									m_TimeControlScrollBar->setPos((s32)(m_CurrentDlgTime*100.0f));

									char str[32];
									sprintf(str, "%.2f", m_CurrentDlgTime);
									m_TimeText->setText(core::stringw(str).c_str());
								}
							}
						}
					}
					break;
				case 1:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						s32 indexS = m_SpriteListBox->getSelected();
						if(dlg && indexS >= 0 && (u32)indexS < dlg->GetSpritesData().size())
						{
							s32 index = m_SpriteKeysListBox->getSelected();
							if(index >= 0)
							{
								dlg->GetSpritesData()[indexS].m_Keys.erase(index);
								m_SpriteKeysListBox->removeItem(index);
								m_SpriteKeysListBox->setSelected(-1);
							}
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_SOUNDS:// BG1
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							SetSoundBG1(L"");
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_SOUNDS + 1:// BG2
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							SetSoundBG2(L"");
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_CONTEXT_MENU_SOUNDS + 2:// VOICE
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// remove
					{
						DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
						if(dlg)
						{
							SetSoundVoice(L"");
						}
					}
					break;
				default:
					break;
				}
			}
			break;
		case GUI_ID_POPUP_DLG:
			{
				if (event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED)
				{
					if(m_PopupDialog)
					{
						m_PopupDialog->remove();
						m_PopupDialog = NULL;
					}
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void SceneEditorWindow::draw()
{
	gui::IGUIElement::draw();

// 	FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
// 	if(frame)
// 	{
// 		LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
// 		if(layer)
// 		{
// 			Position2 pos(m_Viewer->getAbsolutePosition().getCenter());
// 			pos.X -= frame->GetSize().Width>>1;
// 			pos.Y -= frame->GetSize().Height>>1;
// 			layer->Render(pos);
// 		}
// 	}
}

void SceneEditorWindow::UpdateBgBank()
{
	DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
	if(!dlg)
		return;

	TArray<Texture*> images;
	for(u32 i=0; i<1; ++i)
	{
		Texture* tex = dlg->GetImageScene()->m_Image;
		if(tex)
			images.push_back(tex);
	}
	ComputeSpriteBank(m_DialogBackgroundListBox, images, Dimension2(VIEW_BG_PREVIEW_SIZE,VIEW_BG_PREVIEW_SIZE), "BackgroundPreview");

	m_DialogBackgroundKeysListBox->clear();
	for(u32 i=0; i<dlg->GetImageScene()->m_Keys.size(); ++i)
	{
		core::stringw str = L"";
		str += dlg->GetImageScene()->m_Keys[i].m_Time;
		m_DialogBackgroundKeysListBox->addItem(str.c_str());
	}
	m_DialogBackgroundKeysListBox->setSelected(0);

	if(!dlg->GetImageScene()->m_Keys.empty())
	{
		const KeyTime& key = dlg->GetImageScene()->m_Keys[0];
		m_DialogBGKeyTimeSpinBox->setValue(key.m_Time);
		m_DialogBGKeyPosXSpinBox->setValue((float)key.m_Position.X);
		m_DialogBGKeyPosYSpinBox->setValue((float)key.m_Position.Y);
		m_DialogBGKeyScaleSpinBox->setValue((float)key.m_Scale);
		m_DialogBGKeyRotationSpinBox->setValue((float)key.m_Rotation);
		m_DialogBGKeyColorTextBox->setBackgroundColor(key.m_Color);
		m_DialogBGKeyColorTextBox->setText(StringFormat(L"%08X", key.m_Color).c_str());
		IColor textColor = MathUtils::GetInverseColor(key.m_Color);
		textColor.setAlpha(200);
		m_DialogBGKeyColorTextBox->setOverrideColor(textColor);
		m_DialogBGKeyColorASpinBox->setValue((f32)key.m_Color.getAlpha());
		m_DialogBGKeyColorRSpinBox->setValue((f32)key.m_Color.getRed());
		m_DialogBGKeyColorGSpinBox->setValue((f32)key.m_Color.getGreen());
		m_DialogBGKeyColorBSpinBox->setValue((f32)key.m_Color.getBlue());
	}
	m_DialogBGKeysLoopCheckBox->setChecked(dlg->GetImageScene()->m_Loop);

	m_DialogScreenShakeStrengthSpinBox->setValue(dlg->GetScreenShakeStrength());
	m_DialogScreenShakeFrequencySpinBox->setValue(dlg->GetScreenShakeFrequency());
}

void SceneEditorWindow::UpdateSpriteListBank(s32 select/* = -1*/)
{
	DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
	if(!dlg)
		return;

	TArray<Texture*> images;
	TArray< SmartPointer<AnimatedSprite> >& sprites = dlg->GetSprites();
	for(u32 i=0; i<sprites.size(); ++i)
	{
		Texture* tex = NULL;
		s32 index = m_SceneInterface->GetScene()->GetSprites().linear_search(sprites[i]);
		if(index >= 0)
			tex = m_SceneInterface->m_Data->m_SpriteBankPreviews[index];
		else
			tex = sprites[i]->RenderPreview();
		if(tex)
			images.push_back(tex);
	}
	ComputeSpriteBank(m_SpriteListBox, images, Dimension2(64,64), "SpriteListPreview");
	// clean all
	for(u32 i=0; i<images.size(); ++i)
	{
		Texture* tex = images[i];
		if(tex)
			Engine::GetInstance()->GetDriver()->removeTexture(tex);
	}
	images.clear();

	m_SpriteListBox->setSelected((select < 0)?m_SpriteListBox->getItemCount()-1:select);
	s32 index = m_SpriteListBox->getSelected();

	m_SpriteKeysListBox->clear();
	if(index >= 0)
	{
		SpriteDataScene& data = dlg->GetSpritesData()[(u32)index];
		for(u32 i=0; i<data.m_Keys.size(); ++i)
		{
			core::stringw str = L"";
			str += data.m_Keys[i].m_Time;
			m_SpriteKeysListBox->addItem(str.c_str());
		}
		m_SpriteKeysListBox->setSelected(0);
		OnSpriteSelectionChanged();
	}
}

void SceneEditorWindow::ChangeRenderTargetSize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());

	// 			if(m_RenderTarget)
	// 				driver->removeTexture(m_RenderTarget);
	Dimension2 rendertargetDim(core::min_(Engine::GetInstance()->m_RenderSize.Width, dim.Width), core::min_(Engine::GetInstance()->m_RenderSize.Height, dim.Height));
	if(rendertargetDim.Width > 0 && rendertargetDim.Height > 0)
		m_RenderTarget = driver->addRenderTargetTexture(rendertargetDim, "RTT_SceneEditorWindow", video::ECF_A8R8G8B8);
	else
		m_RenderTarget = NULL;
	m_Viewer->setImage(m_RenderTarget);

	s32 maxX = Engine::GetInstance()->m_RenderSize.Width - dim.Width;
	s32 maxY = Engine::GetInstance()->m_RenderSize.Height - dim.Height;
	m_ViewerWScrollBar->setMax(maxX);
	m_ViewerHScrollBar->setMax(maxY);
	m_ViewerWScrollBar->setMin(0);
	m_ViewerHScrollBar->setMin(0);

	m_ViewerPosition.X = core::max_(core::min_(maxX, m_ViewerPosition.X), 0);
	m_ViewerPosition.Y = core::max_(core::min_(maxY, m_ViewerPosition.Y), 0);
	m_ViewerWScrollBar->setPos(m_ViewerPosition.X);
	m_ViewerHScrollBar->setPos(m_ViewerPosition.Y);
}

void SceneEditorWindow::SetSoundBG1(const io::path& path)
{
	DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
	if(dlg)
	{
		SmartPointer<SoundDataScene> sd = (path.empty())? NULL : SmartPointer<SoundDataScene>(new SoundDataScene);//get existing sound or add a new one
		if(sd.IsValid())
		{
			sd->m_Path = path;
			if(!sd->m_Sound.IsValid())// load it if not already loaded
				sd->m_Sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile((char*)sd->m_Path.c_str());
			m_SceneInterface->GetScene()->GetSoundsAlive()[0] = sd->m_Sound;
		}
		else
			m_SceneInterface->GetScene()->GetSoundsAlive()[0] = NULL;
		dlg->SetSoundBG1(sd);
		OnSound1Changed();
	}
}

void SceneEditorWindow::SetSoundBG2(const io::path& path)
{
	DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
	if(dlg)
	{
		SmartPointer<SoundDataScene> sd = (path.empty())? NULL : SmartPointer<SoundDataScene>(new SoundDataScene);//get existing sound or add a new one
		if(sd.IsValid())
		{
			sd->m_Path = path;
			if(!sd->m_Sound.IsValid())// load it if not already loaded
				sd->m_Sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile((char*)sd->m_Path.c_str());
			m_SceneInterface->GetScene()->GetSoundsAlive()[1] = sd->m_Sound;
		}
		else
			m_SceneInterface->GetScene()->GetSoundsAlive()[1] = NULL;
		dlg->SetSoundBG2(sd);
		OnSound2Changed();
	}
}

void SceneEditorWindow::SetSoundVoice(const io::path& path)
{
	DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
	if(dlg)
	{
		SmartPointer<SoundDataScene> sd = (path.empty())? NULL : SmartPointer<SoundDataScene>(new SoundDataScene);//get existing sound or add a new one
		if(sd.IsValid())
		{
			sd->m_Path = path;
			if(!sd->m_Sound.IsValid())// load it if not already loaded
				sd->m_Sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile((char*)sd->m_Path.c_str());
			m_SceneInterface->GetScene()->GetSoundsAlive()[2] = sd->m_Sound;
		}
		else
			m_SceneInterface->GetScene()->GetSoundsAlive()[2] = NULL;
		dlg->SetVoice(sd);
		OnVoiceChanged();
	}
}

void SceneEditorWindow::UpdateRenderTarget(float dt)
{
	Engine* engine = Engine::GetInstance();
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	Dimension2 renderSize((u32)((f32)originalSize.Width*innerScale), (u32)((f32)originalSize.Height*innerScale));
	const Position2& innerOffset = engine->m_InnerOffset;

	u32 start = 0;
	u32 end = 0;
	f32 measure = 0.0f;

	if (m_RenderTarget)
	{
		video::IVideoDriver* driver = engine->GetDriver();
		// draw scene into render target

		// handle viewer size change
		Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());
		if(dim != m_lastSize)
		{
			ChangeRenderTargetSize();
			
			m_lastSize = dim;
		}

		// set render target texture
		driver->setRenderTarget(m_RenderTarget, true, true, COLOR_TRANSPARENT);
		
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		if(m_SceneInterface)
		{
			Scene* scene = m_SceneInterface->GetScene();
			DialogNode* dlg = m_SceneInterface->GetCurrentDialog();
			if(scene && dlg)
			{
				{
					LOG_PROFILE("Update dialog loading");
					if(m_SceneInterface->UpdateLoading())
					{
						OnDialogChange();
					}
				}

				const Dimension2& originalDim = originalSize;
				const Dimension2& renderDim = renderSize; 
				
				// real render zone
				{
					Position2 pos = Position2(0,0) - m_ViewerPosition;
					Dimension2 dim = renderDim;
					Rectangle2 destRect(pos + innerOffset, dim);
					const IColor color(255, 0, 0, 0);
					fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
				}

				bool restartSounds = core::iszero(m_CurrentDlgTime);

				if(m_IsPlaying)
				{
					m_CurrentDlgTime += dt;
					float tMax = dlg->GetDialogTotalDuration(false, true);// max depends of the dialog length
					if(m_CurrentDlgTime > tMax)// make it loop
					{
						m_CurrentDlgTime = 0;
					}

					m_TimeControlScrollBar->setMax((s32)(tMax*100.0f));
					m_TimeControlScrollBar->setPos((s32)(m_CurrentDlgTime*100.0f));
				}
				{
					char text[32];
					sprintf(text, "%.2f", m_CurrentDlgTime);
					m_TimeText->setText(core::stringw(text).c_str());
				}

				// This is a huge hack to sync the scene management with our external current time
				if(scene)
				{
					LOG_PROFILE("Scene sync");

					scene->ResetScene();
					scene->SetCurrentDialog(dlg);
					scene->Step(m_CurrentDlgTime, true);

					// force the inputs
					if(dlg && dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID)
					{
						if(m_CurrentDlgTime >= dlg->GetDialogTotalDuration(true, true))
						{
							scene->GetCurrentInputsPressed() = scene->GetCurrentInputs();
						}
					}
					else
					{
						scene->GetCurrentInputsPressed() = scene->GetCurrentInputs();
					}
				}

				// sound control
				{
					LOG_PROFILE("Sound update");

					if(dlg->GetSoundBG1().IsValid())
					{
						if(!dlg->GetSoundBG1()->m_Sound.IsValid())// load it if not already loaded
							dlg->GetSoundBG1()->m_Sound = engine->GetAudioDevice()->OpenSoundFile((char*)dlg->GetSoundBG1()->m_Path.c_str());

						if(dlg->GetSoundBG1()->m_Sound->IsValid())
						{
							if(m_IsPlaying)
							{
								dlg->GetSoundBG1()->m_Sound->Resume();
								if(restartSounds)
								{
									dlg->GetSoundBG1()->m_Sound->Reset();
									dlg->GetSoundBG1()->m_Sound->Stop();
								}
								if(!dlg->GetSoundBG1()->m_Sound->IsPlaying())
									dlg->GetSoundBG1()->m_Sound->Play();
							}
							else if(!m_IsPlayingSoundPreview)
							{
								if(restartSounds)
								{
									dlg->GetSoundBG1()->m_Sound->Reset();
									dlg->GetSoundBG1()->m_Sound->Stop();
								}
								dlg->GetSoundBG1()->m_Sound->Pause();
							}
						}
					}

					if(dlg->GetSoundBG2().IsValid())
					{
						if(!dlg->GetSoundBG2()->m_Sound.IsValid())// load it if not already loaded
							dlg->GetSoundBG2()->m_Sound = engine->GetAudioDevice()->OpenSoundFile((char*)dlg->GetSoundBG2()->m_Path.c_str());

						if(dlg->GetSoundBG2()->m_Sound->IsValid())
						{
							if(m_IsPlaying)
							{
								dlg->GetSoundBG2()->m_Sound->Resume();
								if(restartSounds)
								{
									dlg->GetSoundBG2()->m_Sound->Reset();
									dlg->GetSoundBG2()->m_Sound->Stop();
								}
								if(!dlg->GetSoundBG2()->m_Sound->IsPlaying())
									dlg->GetSoundBG2()->m_Sound->Play();
							}
							else if(!m_IsPlayingSoundPreview)
							{
								if(restartSounds)
								{
									dlg->GetSoundBG2()->m_Sound->Reset();
									dlg->GetSoundBG2()->m_Sound->Stop();
								}
								dlg->GetSoundBG2()->m_Sound->Pause();
							}
						}
					}

					if(dlg->GetVoice().IsValid())
					{
						if(!dlg->GetVoice()->m_Sound.IsValid())// load it if not already loaded
							dlg->GetVoice()->m_Sound = engine->GetAudioDevice()->OpenSoundFile((char*)dlg->GetVoice()->m_Path.c_str());

						if(dlg->GetVoice()->m_Sound->IsValid())
						{
							if(m_IsPlaying)
							{
								dlg->GetVoice()->m_Sound->Resume();
								if(restartSounds)
								{
									dlg->GetVoice()->m_Sound->Reset();
									dlg->GetVoice()->m_Sound->Stop();
								}
								if(!dlg->GetVoice()->m_Sound->IsPlaying())
									dlg->GetVoice()->m_Sound->Play();
							}
							else if(!m_IsPlayingSoundPreview)
							{
								if(restartSounds)
								{
									dlg->GetVoice()->m_Sound->Reset();
									dlg->GetVoice()->m_Sound->Stop();
								}
								dlg->GetVoice()->m_Sound->Pause();
							}
						}
					}
				}

				Position2 shakeOffset(P2Zero);
				{
					shakeOffset += GetShakeOffset(m_CurrentDlgTime, engine->m_InnerScale, dlg->GetScreenShakeStrength(), dlg->GetScreenShakeFrequency());
				}
				

				// BG
				Texture* tex = dlg->GetImageScene()->m_Image;
				if(tex)
				{
					LOG_PROFILE("BG rendering");

					//Position2 center = m_ViewerBackground->getRelativePosition().getCenter();
					s32 index = m_DialogBackgroundKeysListBox->getSelected();
					if(index<0 || (u32)index>=dlg->GetImageScene()->m_Keys.size())
						index = 0;
					assert(!dlg->GetImageScene()->m_Keys.empty());

					const KeyTime& key = MathUtils::GetInterpolatedKeyTime(dlg->GetImageScene()->m_Keys, m_CurrentDlgTime, dlg->GetImageScene()->m_Loop);
					//const KeyTime& key = dlg->GetImageKeys()[index];

					IColor tint = key.m_Color;
					Rectangle2 sourceRect(Position2(0,0), tex->getSize());
					Position2 pos = shakeOffset + Position2((s32)(0*innerScale), (s32)(0*innerScale)) 
						+ innerOffset - m_ViewerPosition;
					Dimension2 dim = originalDim;
					float scale = core::max_(key.m_Scale, Epsilon);
					Rectangle2 destRect(pos, Dimension2((s32)(dim.Width*innerScale), (s32)(dim.Height*innerScale)));

					Rectangle2 srcRect(-key.m_Position, Dimension2((s32)((f32)originalDim.Width/scale), (s32)((f32)originalDim.Height/scale)));

					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, 
						(f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
						core::matrix4().setRotationAxisRadians(key.m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
						core::matrix4().setScale(core::vector3df((f32)destRect.getWidth()/srcRect.getWidth(), (f32)destRect.getHeight()/srcRect.getHeight(), 0.0f));

					draw2DImage(driver, tex, srcRect, mat, true, tint);
				}

				// Sprites
				{
					LOG_PROFILE("Sprites rendering");
					//assert(dlg->GetSprites().size() == dlg->GetSpritesData().size());

					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)shakeOffset.X + innerOffset.X - m_ViewerPosition.X, (f32)shakeOffset.Y + innerOffset.Y - m_ViewerPosition.Y, 0.0f)) * 
						core::matrix4().setScale(core::vector3df(innerScale, innerScale, 0.0f));

					for(u32 i = 0; i < dlg->GetSprites().size(); ++i)
					{
						SmartPointer<AnimatedSprite> spr = dlg->GetSprites()[i];
						if(spr.IsValid())
						{
							const SpriteDataScene& data = dlg->GetSpritesData()[i];
							KeyTime keyTime = MathUtils::GetInterpolatedKeyTime(data.m_Keys, m_CurrentDlgTime, data.m_Loop);
							if(keyTime.m_Time >= 0.0f)
								spr->RenderFrame(spr->GetFrameIndexFromTime(m_CurrentDlgTime, data.m_LoopAnim), mat*keyTime.GetMatrixTransform(), keyTime.m_Color, data.m_MirrorH, data.m_MirrorV);
						}
					}
				}

				Position2 posInput = Position2((s32)(innerScale*originalSize.Width*0.5f), (s32)(innerScale*originalSize.Height*0.2f)) + innerOffset - m_ViewerPosition;

				// dialog text
				u32 languageIndex = (u32)engine->GetLanguage();
				if(dlg->GetDialogHeader(languageIndex).size() != 0 || dlg->GetDialogText(languageIndex).size() != 0 ||
					(!dlg->GetConditionalTexts(languageIndex).empty() && dlg->GetConditionalTexts(languageIndex)[0].size() != 0))
				{
					LOG_PROFILE("Dialog rendering");

					Position2 pos = Position2((s32)(originalSize.Width*dlg->GetDialogPosition().X*innerScale), (s32)(originalSize.Height*dlg->GetDialogPosition().Y*innerScale)) + innerOffset - m_ViewerPosition;
					Dimension2 dim((s32)(originalSize.Width*dlg->GetDialogSize().X), (s32)(originalSize.Height*dlg->GetDialogSize().Y));
					Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

					if(dlg->GetUseTextBackGround())
					{
						//driver->draw2DRectangleOutline(destRect, IColor(255, 255, 0, 0));
						Texture* tex = engine->GetTextBG();
						if(tex)
						{
							IColor tint = COLOR_WHITE;
							Rectangle2 srcRect(Position2(0,0), tex->getSize());
							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, (f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
								core::matrix4().setRotationAxisRadians(0.0f, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df((f32)destRect.getWidth()/srcRect.getWidth(), (f32)destRect.getHeight()/srcRect.getHeight(), 0.0f));
							draw2DImage(driver, tex, srcRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
						}
					}
					const s32 margin = (s32)(innerScale*20);
					destRect.LowerRightCorner.X -= margin;
					destRect.LowerRightCorner.Y -= margin;
					destRect.UpperLeftCorner.X += margin;
					destRect.UpperLeftCorner.Y += margin;

					if(dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID || dlg->GetNodeType() == DialogNode::DN_AUTO_VALID)
						posInput = destRect.LowerRightCorner;
					else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						posInput = destRect.UpperLeftCorner;//Position2(destRect.LowerRightCorner.X, destRect.UpperLeftCorner.Y);

					core::stringw str;
					TArray<CGUIFreetypeFont::ColorElement> colors;
					float ratio = core::abs_(cosf(0.75f*core::PI*m_CurrentDlgTime));
					IColor color = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), 
						MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COLOR_WHITE, 0.5f), ratio);
					Dimension2 strDim = (Dimension2)destRect.getSize();
					core::stringw header = dlg->GetDialogHeader(languageIndex);
					core::stringw text = header + dlg->GetDialogText(languageIndex);
					u32 textBaseSize = text.size();
					colors.push_back(CGUIFreetypeFont::ColorElement(COLOR_WHITE, textBaseSize));
					for(u32 i = 0; i < dlg->GetConditionalTexts(languageIndex).size(); ++i)
					{
						text += L"\n";
						text += dlg->GetConditionalTexts(languageIndex)[i];
						bool isCurrentChoice = (scene->GetCurrentDialogChoice() == i);
						colors.push_back(CGUIFreetypeFont::ColorElement(isCurrentChoice? color : COLOR_WHITE, text.size()-textBaseSize));
						textBaseSize = text.size();
					}
					FormatString(str, text, engine->GetDialogFont(), strDim);
					engine->GetDialogFont()->draw(str.subString(0, (dlg->GetDialogSpeed() == 0.0f)?text.size():header.size()+(u32)(dlg->GetDialogSpeed()*m_CurrentDlgTime)), 
						destRect, colors, 1.0f, false, false, COLOR_TRANSPARENT, &destRect);

					m_DialogChoiceAreas.clear();
					if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID && colors.size() > 1)
					{
						u32 curSize = colors[0].Size;
						for(u32 i = 1; i < colors.size(); ++i)
						{
							TArray<u32> outLinesWidth;
							Rectangle2 currentRect = engine->GetDialogFont()->getSubStringRect(text.c_str(), curSize+1, colors[i].Size-1, outLinesWidth);
							currentRect.LowerRightCorner += posInput;
							currentRect.UpperLeftCorner += posInput;
							m_DialogChoiceAreas.push_back(currentRect);

							curSize += colors[i].Size;
						}

						u32 currentChoice = scene->GetCurrentDialogChoice();
						posInput.X = m_DialogChoiceAreas[currentChoice].UpperLeftCorner.X + m_DialogChoiceAreas[currentChoice].getWidth();
						posInput.Y = m_DialogChoiceAreas[currentChoice].UpperLeftCorner.Y + (m_DialogChoiceAreas[currentChoice].getHeight()>>1);
					}
				}
				else
				{
					Position2 pos = Position2((s32)(originalSize.Width*dlg->GetDialogPosition().X*innerScale), (s32)(originalSize.Height*dlg->GetDialogPosition().Y*innerScale)) + innerOffset - m_ViewerPosition;
					Dimension2 dim((s32)(originalSize.Width*dlg->GetDialogSize().X), (s32)(originalSize.Height*dlg->GetDialogSize().Y));
					Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

					const s32 margin = (s32)(innerScale*20);
					destRect.LowerRightCorner.X -= margin;
					destRect.LowerRightCorner.Y -= margin;
					destRect.UpperLeftCorner.X += margin;
					destRect.UpperLeftCorner.Y += margin;

					if(dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID || dlg->GetNodeType() == DialogNode::DN_AUTO_VALID)
						posInput = destRect.LowerRightCorner;
					else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						posInput = destRect.UpperLeftCorner;//Position2(destRect.LowerRightCorner.X, destRect.UpperLeftCorner.Y);
				}

				// Buttons display
				if(scene)
				{
					LOG_PROFILE("Button rendering");

					InputData& inputs = scene->GetCurrentInputs();
					float currentInputsRemainingTime = scene->GetCurrentInputsRemainingTime();
					if(inputs.m_Inputs != 0)
					{
#ifdef _DEBUG
						// debug display
						{
							Position2 pos = Position2(0,0) + innerOffset - m_ViewerPosition;
							Dimension2 dim(originalDim.Width, originalDim.Height);
							Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

							//driver->draw2DRectangleOutline(destRect, IColor(255, 255, 0, 0));
							core::stringw text = L"Appuyez sur ";
							bool inputProcessed = false;
							TArray<CGUIFreetypeFont::ColorElement> colorElts;
							colorElts.push_back(CGUIFreetypeFont::ColorElement(COLOR_WHITE, text.size()));
							for(u32 i = 0; i < InputData::PI_MAX; ++i)
							{
								if(inputs.IsPressed((InputData::EPadInput)i))
								{
									IColor color = (scene->GetCurrentInputsPressed().IsPressed((InputData::EPadInput)i))?
										IColor(255, 0, 255, 0):IColor(255, 255, 0, 0);

									u32 start = text.size();
									if(inputProcessed)
										text += L" ET ";
									inputProcessed = true;
									text += InputData::GetInputName((InputData::EPadInput)i);
									u32 end = text.size();

									colorElts.push_back(CGUIFreetypeFont::ColorElement(color, end - start));
								}
							}
							engine->GetDialogFont()->draw(text, destRect, colorElts, 1.0f,  false, false);
						}
#endif // #ifdef _DEBUG

						// real display
						{
							u32 alpha = 255;
							IColor color = (dlg->GetNodeType() == DialogNode::DN_QTE_VALID && inputs.m_NbCurrentPress >= inputs.m_NbPress)? 
								IColor(alpha, 100, 255, 100) : IColor(alpha, 255, 255, 255);

							IColor tint(COLOR_WHITE);
							{
								float ratio = core::abs_(cosf(0.75f*core::PI*m_CurrentDlgTime));
								tint = MathUtils::GetInterpolatedColor(IColor(alpha, 9, 173, 231), IColor(alpha>>1, 255, 255, 255), 1.0f-ratio);
							}

							TArray<ProfileData::KeyMappingSerializable::IIKeyType> inputKeys;
							for(u32 i = 0; i < InputData::PI_MAX; ++i)
							{
								if(inputs.IsPressed((InputData::EPadInput)i))
								{
									inputKeys.push_back((ProfileData::KeyMappingSerializable::IIKeyType)i);
								}
							}
							Dimension2 largestSize(230,200);
							if(engine->GetKeyElement(Engine::KET_EMPTY_KEY2))
								largestSize = engine->GetKeyElement(Engine::KET_EMPTY_KEY2)->getSize();
							float renderScale = 0.35f * innerScale;
							float dx = renderScale*largestSize.Width;
							float dy = renderScale*largestSize.Height;
							for(u32 i = 0; i < inputKeys.size(); ++i)
							{
								ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;

								EKEY_CODE keycode = keymap.GetKeyCode1(inputKeys[i]);
								core::stringw strCode = GetStringFromKeyCode(keycode);

								if(strCode != L"")
								{
									Engine::KeyElementType type = (strCode.size() > 1)? Engine::KET_EMPTY_KEY2 : Engine::KET_EMPTY_KEY1;
									// special keys
									if(keycode == KEY_LBUTTON)
										type = Engine::KET_LMOUSE;
									else if(keycode == KEY_RBUTTON)
										type = Engine::KET_RMOUSE;
									else if(keycode == KEY_LEFT)
										type = Engine::KET_LEFT_KEY;
									else if(keycode == KEY_RIGHT)
										type = Engine::KET_RIGHT_KEY;
									else if(keycode == KEY_UP)
										type = Engine::KET_UP_KEY;
									else if(keycode == KEY_DOWN)
										type = Engine::KET_DOWN_KEY;

									const float fadeInDuration = core::min_(0.15f, 0.5f*inputs.m_Duration);
									const float fadeOutDuration = core::min_(0.3f, 0.5f*inputs.m_Duration);
									const float maxZoomFactor = 3.0f;
									const float minZoomFactor = 0.01f;
									float fadeEffector = 1.0f;
									float fadeEffector2 = 1.0f;
									if(dlg->GetNodeType() == DialogNode::DN_QTE_VALID && fadeInDuration > 0.0f && fadeOutDuration > 0.0f)
									{
										if(currentInputsRemainingTime >= inputs.m_Duration - fadeInDuration)
										{
											fadeEffector = inputs.m_Duration - currentInputsRemainingTime;// 0 to duration
											fadeEffector = 1.0f - core::min_(fadeEffector, fadeInDuration)/fadeInDuration;// 1.0f to 0.0f in fadeInDuration seconds
											fadeEffector *= (maxZoomFactor-1.0f);
											fadeEffector += 1.0f;// from maxZoomFactor to 1.0f in fadeInDuration seconds
										}
										else if(currentInputsRemainingTime <= fadeOutDuration)
										{
											fadeEffector = currentInputsRemainingTime;// duration to 0
											fadeEffector = 1.0f - core::min_(fadeEffector, fadeOutDuration)/fadeOutDuration;// 0.0f to 1.0f in fadeOutDuration seconds
											fadeEffector *= (minZoomFactor-1.0f);
											fadeEffector += 1.0f;// from 1.0f to minZoomFactor in fadeOutDuration seconds
										}
										else
										{
											if(inputs.m_NbPress > 1)
												fadeEffector2 = 1.0f - 0.6f * core::abs_(cosf(5.0f*core::PI*currentInputsRemainingTime));
											else
												fadeEffector2 = 1.0f - 0.1f * core::abs_(cosf(1.5f*core::PI*currentInputsRemainingTime));
										}
									}
									else
									{
										fadeEffector2 = 1.0f - 0.1f * core::abs_(cosf(1.5f*core::PI*m_CurrentDlgTime));
									}

									Position2 pos2(posInput);
									if(dlg->GetNodeType() == DialogNode::DN_QTE_VALID)// top centered
									{
										if(inputKeys.size() > 1)
											pos2.X += (s32)(((float)i - 0.5f * (inputKeys.size()-1)) * (dx + dy) * fadeEffector);
									}
									else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)// left aligned
									{
									}
									else// bottom right aligned
									{
										if(inputKeys.size() > 1)
											pos2.X -= (s32)((dx + dy) * (inputKeys.size() - i - 1));
										pos2.Y -= (s32)(dy*0.5f);
									}


									if(engine->GetKeyElement(type))
									{
										Dimension2 size = engine->GetKeyElement(type)->getSize();
										Rectangle2 sourceRect(Position2(0,0), size);

										if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)// left aligned
											pos2.X += (s32)(renderScale*size.Width*0.5f);
										else if(dlg->GetNodeType() != DialogNode::DN_QTE_VALID)// bottom right aligned
											pos2.X -= (s32)(renderScale*size.Width*0.5f);

										float renderScale = (float)dy/size.Height;
										if(type == Engine::KET_EMPTY_KEY2)
											renderScale *= 2.0f;
										else if(type == Engine::KET_RMOUSE || type == Engine::KET_LMOUSE)
											renderScale *= 1.0f;
										else
											renderScale *= 1.5f;

										
										renderScale *= fadeEffector*fadeEffector2;

										core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
											core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
											core::matrix4().setScale(core::vector3df(renderScale, renderScale, 0.0f));

										draw2DImage(driver, engine->GetKeyElement(type), sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);
									}

									if(type == Engine::KET_EMPTY_KEY2 || type == Engine::KET_EMPTY_KEY1)
									{
										//if(isSelected)
										{
											engine->GetDialogFont()->drawGauss(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), color, fadeEffector, true, true);
										}
										engine->GetDialogFont()->draw(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), color, fadeEffector, true, true);
									}
								}
							}
						}
					}
				}

				// draw transition effects
				{
					LOG_PROFILE("Transition rendering");

					float time;
					// in
					time = m_CurrentDlgTime;
					if(time < dlg->GetTransitionIn().m_Duration && dlg->GetTransitionIn().m_Duration > 0.0f)
					{
						switch(dlg->GetTransitionIn().m_Type)
						{
						case DialogNode::DN_TRANSITION_FADE:
							{
								const u8 a = 255-(u8)(255.0f*time/dlg->GetTransitionIn().m_Duration);
								const IColor color(a, 0, 0, 0);
								Position2 pos = innerOffset - m_ViewerPosition;
								Dimension2 dim((u32)(originalDim.Width*innerScale), (u32)(originalDim.Height*innerScale));
								Rectangle2 destRect(pos, dim);
								fill2DRect(driver, destRect, color, color, color, color, true/*USE_PREMULTIPLIED_ALPHA*/);
							}
							break;
						}
					}
					// out
					time = dlg->GetDialogTotalDuration(false, true) - m_CurrentDlgTime;
					if(time < dlg->GetTransitionOut().m_Duration && dlg->GetTransitionOut().m_Duration > 0.0f)
					{
						switch(dlg->GetTransitionOut().m_Type)
						{
						case DialogNode::DN_TRANSITION_FADE:
							{
								const IColor color(255-(u8)(255.0f*core::max_(0.0f, time)/dlg->GetTransitionOut().m_Duration), 0, 0, 0);
								Position2 pos = innerOffset - m_ViewerPosition;
								Dimension2 dim((u32)(originalDim.Width*innerScale), (u32)(originalDim.Height*innerScale));
								Rectangle2 destRect(pos, dim);
								fill2DRect(driver, destRect, color, color, color, color, true/*USE_PREMULTIPLIED_ALPHA*/);
							}
							break;
						}
					}
				}

				// real render zone : redraw the black bands 
				{
					IColor color(255, 0, 0, 0);
					// left
					{
						Position2 pos = Position2(0, 0) - m_ViewerPosition;
						Dimension2 dim(innerOffset.X, renderDim.Height);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}

					// right
					{
						Position2 pos = Position2((s32)(originalDim.Width*innerScale) + innerOffset.X, 0) - m_ViewerPosition;
						Dimension2 dim(innerOffset.X+1, renderDim.Height);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}

					// up
					{
						Position2 pos = Position2(0, 0) - m_ViewerPosition;
						Dimension2 dim(renderDim.Width, innerOffset.Y);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}

					// down
					{
						Position2 pos = Position2(0, (s32)(originalDim.Height*innerScale) + innerOffset.Y) - m_ViewerPosition;
						Dimension2 dim(renderDim.Width, innerOffset.Y+1);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}
				}

				// debug : render zone
// 				{
// 					const IColor grey(127, 0, 255, 0);
// 					{
// 						Position2 pos = m_InnerOffset - m_ViewerPosition;
// 						Dimension2 dim((s32)(originalDim.Width*m_InnerScale), (s32)(originalDim.Height*m_InnerScale));
// 						driver->draw2DLine(pos, pos + Position2(dim.Width-1, 0), grey);
// 						driver->draw2DLine(pos, pos + Position2(0, dim.Height-1), grey);
// 						driver->draw2DLine(pos + Position2(dim.Width-1, dim.Height-1), pos + Position2(dim.Width-1, 0), grey);
// 						driver->draw2DLine(pos + Position2(dim.Width-1, dim.Height-1), pos + Position2(0, dim.Height-1), grey);
// 					}
// 				}
			}

			if(scene)
			{
				LOG_PROFILE("Cleaning");

				scene->FlushSpritesBank();
			}
		}

		// set back old render target
		// The buffer might have been distorted, so clear it
		driver->setRenderTarget(0, true, true, IColor(0,200,200,200));
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

// name window
NameSelectWindow::NameSelectWindow(gui::IGUIEnvironment* environment, gui::IGUIElement* parent, s32 id, core::rect<s32> rectangle, gui::IGUIListBox* inListBox, SceneInterface* scnInterface) :
gui::IGUIElement(gui::EGUIET_WINDOW, environment, parent, id, Rectangle2(Position2(0, 0), environment->getVideoDriver()->getScreenSize())), m_Gui(environment), m_InListBox(inListBox), m_ScnInterface(scnInterface)
{
	const u32 btnWidth = 64;
	const u32 btnHeight = 16;

	m_Window = m_Gui->addWindow(rectangle, true, L"Entrez un nom à utiliser", this);

	assert(m_InListBox);
	assert(m_InListBox->getSelected() >= 0 && m_InListBox->getSelected() < (s32)m_InListBox->getItemCount());

	Rectangle2 rect1(Position2(16, 32), Dimension2(rectangle.getWidth() - 2*16, rectangle.getHeight() - 3*16 - btnHeight));
	m_EditBox = m_Gui->addEditBox(m_InListBox->getListItem(m_InListBox->getSelected()), rect1, true, m_Window);

	Rectangle2 rect2(Position2((rectangle.getWidth()-btnWidth)/2, rectangle.getHeight() - 8 - btnHeight), Dimension2(btnWidth, btnHeight));
	m_ValidButton = m_Gui->addButton(rect2, m_Window, -1, L"OK");
}

bool NameSelectWindow::OnEvent(const SEvent& event)
{
	// 	SEvent evt = event;
	// 	printf("TitleSelectWindow\n");
	if (event.EventType == EET_GUI_EVENT)
	{
		if((event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED && event.GUIEvent.Caller == m_ValidButton) || 
			(event.GUIEvent.EventType == gui::EGET_EDITBOX_ENTER && event.GUIEvent.Caller == m_EditBox))
		{
			m_InListBox->setItem(m_InListBox->getSelected(), m_EditBox->getText(), -1);
			if(m_ScnInterface && m_ScnInterface->GetCurrentDialog())
			{
				m_ScnInterface->GetCurrentDialog()->SetDialogName(m_EditBox->getText());
				u32 index = 0xffffffff;
				for(u32 i = 0; i < m_ScnInterface->GetScene()->GetDialogs().size(); ++i)
				{
					if(m_ScnInterface->GetScene()->GetDialogAtIndex(i) == m_ScnInterface->GetCurrentDialog())
					{
						index = i;
						break;
					}
				}
				assert(index != 0xffffffff);
				m_ScnInterface->m_Data->m_DialogListNames[index] = m_EditBox->getText();
				SceneEditorWindow* scnEditor = (SceneEditorWindow*)m_Gui->getRootGUIElement()->getElementFromId(GUI_ID_SCENE_EDITOR);
				if(scnEditor)
				{
					scnEditor->OnDialogChange();
				}
			}
			this->remove();
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_CLOSED && event.GUIEvent.Caller == m_Window)
		{
			this->remove();
		}
	}

	return false;
}


// list window
ListSelectWindow::ListSelectWindow(gui::IGUIEnvironment* environment, gui::IGUIElement* parent, s32 id, core::rect<s32> rectangle, const TArray<const wchar_t*>& list, SceneInterface* scnInterface) :
gui::IGUIElement(gui::EGUIET_WINDOW, environment, parent, id, Rectangle2(Position2(0, 0), environment->getVideoDriver()->getScreenSize())), m_Gui(environment), m_ScnInterface(scnInterface)
{
	const u32 btnWidth = 64;
	const u32 btnHeight = 16;

	m_Window = m_Gui->addWindow(rectangle, true, L"Entrez un nom à utiliser", this);

	Rectangle2 rect1(Position2(8, 32), Dimension2(rectangle.getWidth() - 2*8, rectangle.getHeight() - 3*16 - btnHeight));
	m_ListBox = m_Gui->addListBox(rect1, m_Window);
	for(u32 i=0; i<list.size(); ++i)
	{
		m_ListBox->addItem(list[i], -1);
	}

	Rectangle2 rect2(Position2((rectangle.getWidth()-btnWidth)/2, rectangle.getHeight() - 8 - btnHeight), Dimension2(btnWidth, btnHeight));
	m_ValidButton = m_Gui->addButton(rect2, m_Window, -1, L"OK");
}

bool ListSelectWindow::OnEvent(const SEvent& event)
{
	// 	SEvent evt = event;
	// 	printf("TitleSelectWindow\n");
	if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED && event.GUIEvent.Caller == m_ValidButton)
		{
			s32 index = m_ListBox->getSelected();
			if(index >= 0)
			{
				if(m_ScnInterface && m_ScnInterface->GetCurrentDialog())
				{
					DialogNode* child = m_ScnInterface->GetScene()->GetDialogAtIndex(index);
					assert(child);
					if(child)
						m_ScnInterface->GetCurrentDialog()->BranchToNode(child);

					SceneEditorWindow* scnEditor = (SceneEditorWindow*)m_Gui->getRootGUIElement()->getElementFromId(GUI_ID_SCENE_EDITOR);
					if(scnEditor)
					{
						scnEditor->OnDialogChange();
					}
				}
				this->remove();
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_CLOSED && event.GUIEvent.Caller == m_Window)
		{
			this->remove();
		}
	}

	return false;
}


#undef GUI_ID_CONTEXT_MENU
#undef GUI_ID_SAVE_SCENE_FILE_NAME_DLG
#undef GUI_ID_CONTEXT_MENU_DIALOG_CHOICE
#undef GUI_ID_CONTEXT_MENU_DIALOG_BG
#undef GUI_ID_CONTEXT_MENU_DIALOG_BG_KEYS
#undef GUI_ID_CONTEXT_MENU_SPRITE_LIST
#undef GUI_ID_CONTEXT_MENU_SPRITE_KEYS
#undef GUI_ID_CONTEXT_MENU_DIALOG_INPUTS
#undef GUI_ID_CONTEXT_MENU_DIALOG_TEXTS
#undef GUI_ID_BUTTON_SPRITE_TAB 
#undef GUI_ID_BUTTON_BKG_TAB 
#undef GUI_ID_BUTTON_DLG_TAB 
#undef GUI_ID_POPUP_DLG