#ifndef ENGINE_H
#define ENGINE_H

#include <time.h>

#include "utils.h"
#include "audio.h"

#include "CGUITTFont.h"

//#include "driverChoice.h"

//#define SNOW_LIGHT
//#define DEMO_VERSION

#ifdef IRON_ICE_EDITOR
	#define WINDOW_TITLE_STRING L"IronIce Editor 1.7"

	#define SNOWLIGHT_PROD_REQUEST // Used for editor specificities with snowlight production
#elif defined(IRON_ICE_ENGINE)
	#ifdef IRON_ICE_FINAL
		#define WINDOW_TITLE_STRING Engine::GetInstance()->m_CoreData.AppName.c_str()
	#else
		#define WINDOW_TITLE_STRING L"IronIce Engine"
	#endif
#else
	#define WINDOW_TITLE_STRING L""
#endif


class MyShaderCallBack : public video::IShaderConstantSetCallBack
{
public:

	virtual void OnSetConstants(video::IMaterialRendererServices* services, s32 userData);
};

class IDebugDisplayElement
{
public:
	enum EElementType {ET_3DLine, ET_MAX};

	EElementType m_Type;
	float m_Duration;

protected:
	IDebugDisplayElement(EElementType type, float duration) : m_Type(type), m_Duration(duration) {}
};

class DebugDisplay3DLine : public IDebugDisplayElement
{
public:
	DebugDisplay3DLine(const Vector3& start, const Vector3& end, float duration)
		: IDebugDisplayElement(ET_3DLine, duration), m_Start(start), m_End(end) {}

	Vector3 m_Start;
	Vector3 m_End;
};

class Engine
{
public:
	~Engine();

	enum LanguageSettings {LS_FRENCH, LS_ENGLISH, LS_GERMAN, LS_SPANISH, LS_MAX};// NEVER INSERT NEW LANGUAGES : ADD IT AT THE END
	static const wchar_t* const ms_LanguageSettingsString[LS_MAX];


	enum SourceFileTokenIDs // token mapping through source files
	{
		SFTID_LanguageMenuPanel		= 0, //+1
		SFTID_StartMenuPanel		= 1, //+1
		SFTID_MainMenuPanel			= 2, //+6
		SFTID_StorySubMenuPanel		= 8, //+10
		SFTID_StatsSubMenuPanel		= 18, //+4
		SFTID_OptionsSubMenuPanel	= 22, //+31
		SFTID_BonusSubMenuPanel		= 53, //+4
		SFTID_ExitSubMenuPanel		= 57, //+1
		SFTID_InGameMenuPanel		= 58, //+10
		SFTID_PopupPanel			= 68, //+4

		SFTID_MAX					= 72
	};
	static wchar_t* ms_LanguageStrings[LS_MAX][Engine::SFTID_MAX];
	static const wchar_t* const ms_EmptyString;
	static const wchar_t* const GetLanguageString(LanguageSettings lang, u32 tokenIdx)
	{
		return (tokenIdx < Engine::SFTID_MAX && ms_LanguageStrings[lang][tokenIdx])? ms_LanguageStrings[lang][tokenIdx] : ms_EmptyString;
	}

	static bool ms_FullScreen;
	static Dimension2 ms_WindowSize;

	static Engine* GetInstance();
	void DeleteInstance();

	void RequestResetDevice() {m_ResetDeviceRequest = true;}
	bool ShouldResetDevice() const {return m_ResetDeviceRequest;}
	void ResetDevice();
	void Exit() {if(m_Device) m_Device->closeDevice();}

	IrrlichtDevice* GetDevice() {return m_Device;}

	video::IVideoDriver* GetDriver() {return m_Driver;}
	scene::ISceneManager* GetSceneMgr() {return m_Smgr;}
	gui::IGUIEnvironment* GetGui() {return m_Gui;}

	IIAudioDevice* GetAudioDevice() {return m_AudioDevice;}

	void AddDebug3DLine(const Vector3& start, const Vector3& end, float duration = 0)
	{
		m_DebugDisplayElements.push_back(new DebugDisplay3DLine(start, end, duration));
	}

	s32 GetMaterialSolid() const {return m_MaterialSolid;}
	s32 GetMaterialTransparentAdd() const {return m_MaterialTransparentAdd;}
	s32 GetMaterialInstancing() const {return m_MaterialInstancing;}

	CGUIFreetypeFont* GetDialogFont() const {return m_DialogFont;}
	Texture* GetTextBG() const {return m_TextBG;}

	LanguageSettings GetLanguage() const {return m_Language;}
	void SetLanguage(LanguageSettings language) {m_Language = language;}

	const core::stringc& GetGamePath() const {return m_GamePath;}
	void SetGamePath(const core::stringc& path) {m_GamePath = path;}
	inline core::stringc GetGamePath(const char* const relativePath) const
	{
		return StringFormat("%s%s", m_GamePath.c_str(), relativePath).c_str();
	}


	void DebugDisplayRender(float dt);

	void Update(float dt);

	void RecomputeSize();

	Dimension2 GetNextResolution(const Dimension2& currentRes);
	Dimension2 GetPreviousResolution(const Dimension2& currentRes);
	void DumpResolutions();

	void SetGamma(u8 value)
	{
		f32 scaledValue = 0.01f*value * 2.0f + 0.5f;
		f32 red = scaledValue, green = scaledValue, blue = scaledValue, brightness = 0.0f, contrast = 0.0f;
		m_Device->setGammaRamp(red, green, blue, brightness, contrast);
	}

	void DrawLoadScreen();

	enum KeyElementType{ KET_EMPTY_KEY1, KET_EMPTY_KEY2, KET_LMOUSE, KET_RMOUSE, KET_LEFT_KEY, KET_RIGHT_KEY, KET_UP_KEY, KET_DOWN_KEY, KET_MAX };
	Texture* GetKeyElement(KeyElementType type) const {return m_KeyElements[type];}

	bool ReadLanguageStringsFromFile(const char* path);
#ifndef IRON_ICE_FINAL
	bool ReadLanguageStringsFromFileTxt(const char* path, Engine::LanguageSettings language = Engine::LS_FRENCH);
	bool WriteLanguageStringsToFile(const char* path);
#endif

	Dimension2 m_OriginalSize;
	Dimension2 m_RenderSize;

	float m_InnerScale;
	Position2 m_InnerOffset;

	struct CoreData
	{
		core::stringw AppName;
		bool LangsAllowed[Engine::LS_MAX];
		core::stringw LangsPath[Engine::LS_MAX];
		float LanguagePanelFontScale;
		float StartPanelFontScale;
		float MainMenuFontScale;
		float SubMenuFontScale;
		float PopupFontScale;
		float DialogFontScale;
		u8 Color[3];
		u8 HighLightColor[3];
		u8 GlowColor[3];

		// not serialized
		TArray<LanguageSettings> AvailableLangs;

		CoreData() : 
			AppName(""), 
			LanguagePanelFontScale(1.0f), 
			StartPanelFontScale(1.0f), 
			MainMenuFontScale(1.0f),
			SubMenuFontScale(1.0f),
			PopupFontScale(1.0f),
			DialogFontScale(1.0f)
		{
			const char* const names[Engine::LS_MAX] = {"texts_fr.txt", "texts_en.txt", "texts_de.txt", "texts_es.txt"};
			for(u32 i = 0; i < Engine::LS_MAX; ++i)
			{
				LangsAllowed[i] = false;
				LangsPath[i] = names[i];
			}

			//#FFFFFF
			Color[0] = 0xFF; 
			Color[1] = 0xFF; 
			Color[2] = 0xFF; 
			//#84D6F3
			HighLightColor[0] = 0x84; 
			HighLightColor[1] = 0xD6; 
			HighLightColor[2] = 0xF3; 
			//#09ADE7
			GlowColor[0] = 0x09; 
			GlowColor[1] = 0xAD; 
			GlowColor[2] = 0xE7; 
		}

		bool ReadCoreFromFile(const char* path);
#ifndef IRON_ICE_FINAL
		bool ReadCoreFromFileTxt(const char* path);
		bool WriteCoreToFile(const char* path);
#endif
	};
	CoreData m_CoreData;

protected:
	Engine();

	static Engine* ms_Instance;

	IrrlichtDevice* m_Device;

	video::IVideoDriver* m_Driver;
	scene::ISceneManager* m_Smgr;
	gui::IGUIEnvironment* m_Gui;

	s32 m_MaterialSolid;
	s32 m_MaterialTransparentAdd;
	s32 m_MaterialInstancing;

	CGUIFreetypeFont* m_DialogFont;
	Texture* m_TextBG;
	Texture* m_Loading;

	Texture* m_KeyElements[KET_MAX];

	LanguageSettings m_Language;

	TArray<IDebugDisplayElement*> m_DebugDisplayElements;

	IIAudioDevice* m_AudioDevice;

	core::stringc m_GamePath;

	bool m_ResetDeviceRequest;
};

class InputData
{
public:
	enum EPadInput
	{
		PI_LEFT,
		PI_RIGHT,
		PI_UP,
		PI_DOWN,
		PI_ACTION1,// vita = X
		PI_ACTION2,// vita = O
		PI_ACTION3,// vita = []
		PI_ACTION4,// vita = TRIANGLE
		PI_ACTION5,// vita = R
		PI_ACTION6,// vita = L
		PI_MAX // Number of valid inputs
	};// PI_MAX must be <= 32

	InputData() : m_Inputs(0), m_Duration(0.0f), m_NbPress(1), m_NbCurrentPress(0), m_CurrentState(false) {}
	InputData(EPadInput inputs, float duration, u8 nbPress = 1) :
		m_Inputs(inputs), m_Duration(duration), m_NbPress(nbPress), m_NbCurrentPress(0), m_CurrentState(false) {}
	~InputData() {}

	static const wchar_t* const GetInputName(EPadInput index) {return ((u32)index < (u32)PI_MAX)?ms_InputNames[(u32)index]:L"";}
	bool IsPressed(EPadInput index) const {return ((u32)index < (u32)PI_MAX)?((m_Inputs&(1<<((u32)index)))!=0):false;}
	void SetPressed(EPadInput index, bool state) {m_Inputs = (m_Inputs & ~(1<<((u32)index))) | (state<<((u32)index));}

	u32 m_Inputs;
	float m_Duration;
	u8 m_NbPress;


	u32 m_NbCurrentPress;
	bool m_CurrentState;// only used for QTE count

private:
	static const wchar_t* const ms_InputNames[PI_MAX];
};


struct AudioDataSerializable
{
	char ShortName[8];

	// 32 bits aligned
	u8 Volume;
	u8 Pitch;
	u8 Pan;
	bool Repeat:1;
	bool StopAtEnd:1;
	bool unk1:6;

	io::path GetName() const 
	{
		char temp[9] = {0};
		memcpy(temp, ShortName, 8);
		return io::path(temp);
	}

	AudioDataSerializable() : Volume(0), Pitch(0), Pan(0), Repeat(false), StopAtEnd(false) 
	{
		memset(ShortName, 0, 8);
	}
	AudioDataSerializable(io::path name, float volume, float pitch, float pan, bool repeat, bool stop)
	{
		memcpy(ShortName, name.c_str(), 8);
		Volume = (u8)(volume*100.0f);
		Pitch = (u8)(pitch*100.0f);
		Pan = (u8)((pan+1.0f)*100.0f);
		Repeat = repeat;
		StopAtEnd = stop;
	}

	float GetVolume() const {return (float)Volume*0.01f;}
	float GetPitch() const {return (float)Pitch*0.01f;}
	float GetPan() const {return (float)Pan*0.01f - 1.0f;}
	bool GetRepeat() const {return Repeat;}
	bool GetStopAtEnd() const {return StopAtEnd;}
};

struct SaveData
{
	SaveData(): Used(false) {}

	u32 ChapterId;
	u32 SceneId;
	u32 DialogId;// 0 = default initial dlg, otherwise do DialogId-1 to get the dialog ID

	time_t DateTime;

	AudioDataSerializable CurrentSounds[3];

	// 32 bits aligned
	bool Used:8;
	u8 unk1;
	u8 unk2;
	u8 unk3;

	core::stringw GetDateTime() const
	{
		struct tm locTime = *localtime(&DateTime);

		core::stringw str = StringFormat(L"%d/%02d/%02d - %02d:%02d", locTime.tm_year + 1900, locTime.tm_mon + 1, locTime.tm_mday, locTime.tm_hour, locTime.tm_min);
		return str;
	}

	core::stringw GetStringCompact() const
	{
		core::stringw str = StringFormat(L"Ch%02d Sc%02d :\r\n", ChapterId+1, SceneId+1);
		str += GetDateTime();
		return str;
	}

	void Save(u32 chapterID, u32 sceneID, u32 dialogID, AudioDataSerializable* sounds = NULL)
	{
		ChapterId = chapterID;
		SceneId = sceneID;
		DialogId = dialogID;
		time(&DateTime);
		char* empty = "";
		if(sounds)
		{
			for(u32 i = 0; i < 3; ++i)
				CurrentSounds[i] = sounds[i];
		}
		Used = true;
	}
};


#ifdef IRON_ICE_ENGINE
#define PROFILE_CHAPTERS_MAX 20
#define PROFILE_SAVE_SLOTS 12
#define PROFILE_AUTOSAVE_SLOT 0
#define PROFILE_QUICKSAVE_SLOT 1
class ProfileData
{
public:
	~ProfileData();

	static ProfileData* GetInstance();
	void DeleteInstance();

	SaveData& GetSave(u32 slotIndex) {assert(slotIndex < PROFILE_SAVE_SLOTS); return m_PDS.m_Saves[(slotIndex < PROFILE_SAVE_SLOTS)? slotIndex : 0];}
	bool WriteSave();
	bool ReadSave();

	bool IsLoaded() const {return m_IsLoaded;}
	void Initialize();

	struct ProfileDataSerializable
	{
		time_t m_DateTimeCreation;
		SaveData m_Saves[PROFILE_SAVE_SLOTS];

		u8 m_ProgressionCH;
		u8 m_ProgressionSC;
		float m_TimeCH[PROFILE_CHAPTERS_MAX];
		u32 m_nbQTESuccess;
		u32 m_nbQTEFail;
	};
	struct OptionDataSerializable
	{
		enum DriverType {DT_D3D, DT_OPENGL/*, DT_SOFTWARE*/, DT_MAX};
		Dimension2 m_WindowSize;
		bool m_FullScreen;
		bool m_VSync;
		DriverType m_DriverType;

		// Values from 0 to 100
		u8 m_GammaRamp;
		u8 m_VolumeGeneral;
		u8 m_VolumeVoice;
		u8 m_VolumeBG1;
		u8 m_VolumeBG2;

		OptionDataSerializable() : m_WindowSize(800,600)
			, m_FullScreen(false)
			, m_VSync(false)
			, m_DriverType(DT_OPENGL)
			, m_GammaRamp(50)
			, m_VolumeGeneral(100)
			, m_VolumeVoice(100)
			, m_VolumeBG1(100)
			, m_VolumeBG2(100)
		{
		}

		bool operator ==(const OptionDataSerializable& ods)
		{
			return m_WindowSize == ods.m_WindowSize &&
				m_FullScreen == ods.m_FullScreen &&
				m_VSync == ods.m_VSync &&
				m_DriverType == ods.m_DriverType &&
				m_GammaRamp == ods.m_GammaRamp &&
				m_VolumeGeneral == ods.m_VolumeGeneral &&
				m_VolumeVoice == ods.m_VolumeVoice &&
				m_VolumeBG1 == ods.m_VolumeBG1 &&
				m_VolumeBG2 == ods.m_VolumeBG2;
		}

		bool operator !=(const OptionDataSerializable& ods)
		{
			return m_WindowSize != ods.m_WindowSize ||
				m_FullScreen != ods.m_FullScreen ||
				m_VSync != ods.m_VSync ||
				m_DriverType != ods.m_DriverType ||
				m_GammaRamp != ods.m_GammaRamp ||
				m_VolumeGeneral != ods.m_VolumeGeneral ||
				m_VolumeVoice != ods.m_VolumeVoice ||
				m_VolumeBG1 != ods.m_VolumeBG1 ||
				m_VolumeBG2 != ods.m_VolumeBG2;
		}

		bool NeedsResetDevice(const OptionDataSerializable& ods)
		{
			return m_WindowSize != ods.m_WindowSize ||
				m_FullScreen != ods.m_FullScreen ||
				m_VSync != ods.m_VSync ||
				m_DriverType != ods.m_DriverType;
		}
	};
	struct KeyMappingSerializable
	{
		KeyMappingSerializable()
		{
			memset(m_KeyMapping1, 0, sizeof(EKEY_CODE)*II_KEY_MAX);
			memset(m_KeyMapping2, 0, sizeof(EKEY_CODE)*II_KEY_MAX);
		}
		~KeyMappingSerializable() {}

		enum IIKeyType
		{
			II_KEY_LEFT,
			II_KEY_RIGHT,
			II_KEY_UP,
			II_KEY_DOWN,
			II_KEY_QTE1,// + game validate
			II_KEY_QTE2,
			II_KEY_QTE3,
			II_KEY_QTE4,
			II_KEY_QTE5,
			II_KEY_QTE6,
			II_KEY_PAUSE,
			II_KEY_QUICK_MENU,
			II_KEY_MAX
		};

		EKEY_CODE m_KeyMapping1[II_KEY_MAX];// in game
		EKEY_CODE m_KeyMapping2[II_KEY_MAX];// menus

		bool MapKey1(IIKeyType IIKey, EKEY_CODE keyCode)
		{
			if((u32)keyCode != 0x0)
				for(u32 i = 0; i < II_KEY_MAX; ++i)
					if(i != IIKey && keyCode == m_KeyMapping1[i])// already used
						m_KeyMapping1[i] = (EKEY_CODE)0x0;// unmap it
//						return false;

			m_KeyMapping1[IIKey] = keyCode;
			return true;
		}
		bool MapKey2(IIKeyType IIKey, EKEY_CODE keyCode)
		{
			if((u32)keyCode != 0x0)
				for(u32 i = 0; i < II_KEY_MAX; ++i)
					if(i != IIKey && keyCode == m_KeyMapping2[i])// already used
						m_KeyMapping2[i] = (EKEY_CODE)0x0;// unmap it
// 						return false;

			m_KeyMapping2[IIKey] = keyCode;
			return true;
		}

		EKEY_CODE GetKeyCode1(IIKeyType IIKey)
		{
			return m_KeyMapping1[IIKey];
		}
		EKEY_CODE GetKeyCode2(IIKeyType IIKey)
		{
			return m_KeyMapping2[IIKey];
		}

		bool operator ==(const KeyMappingSerializable& kms)
		{
			return (memcmp(&kms, this, sizeof(KeyMappingSerializable)) == 0);
		}

		bool operator !=(const KeyMappingSerializable& kms)
		{
			return (memcmp(&kms, this, sizeof(KeyMappingSerializable)) != 0);
		}

	};
	ProfileDataSerializable m_PDS;
	OptionDataSerializable m_ODS;
	KeyMappingSerializable m_KMS;

protected:
	ProfileData();

	static ProfileData* ms_Instance;

	bool m_IsLoaded;
};
#elif defined(IRON_ICE_EDITOR)
class ProfileData
{
public:
	~ProfileData();

	static ProfileData* GetInstance();
	void DeleteInstance();

	bool WriteSave();
	bool ReadSave();

	bool IsLoaded() const {return m_IsLoaded;}
	void Initialize();

	struct OptionDataSerializable
	{
		enum DriverType {DT_D3D, DT_OPENGL, /*DT_SOFTWARE, */DT_MAX};
		Dimension2 m_WindowSize;
		Dimension2 m_RenderSize;
		bool m_FullScreen;
		bool m_VSync;
		DriverType m_DriverType;

		OptionDataSerializable() : m_WindowSize(1024,640)
			, m_RenderSize(800,600)
			, m_FullScreen(false)
			, m_VSync(true)
			, m_DriverType(DT_OPENGL)
		{
		}

		bool operator ==(const OptionDataSerializable& ods)
		{
			return m_WindowSize == ods.m_WindowSize &&
				m_RenderSize == ods.m_RenderSize &&
				m_FullScreen == ods.m_FullScreen &&
				m_VSync == ods.m_VSync &&
				m_DriverType == ods.m_DriverType;
		}

		bool operator !=(const OptionDataSerializable& ods)
		{
			return m_WindowSize != ods.m_WindowSize ||
				m_RenderSize != ods.m_RenderSize ||
				m_FullScreen != ods.m_FullScreen ||
				m_VSync != ods.m_VSync ||
				m_DriverType != ods.m_DriverType;
		}
	};

	struct KeyMappingSerializable
	{
		KeyMappingSerializable()
		{
			memset(m_KeyMapping1, 0, sizeof(EKEY_CODE)*II_KEY_MAX);
			memset(m_KeyMapping2, 0, sizeof(EKEY_CODE)*II_KEY_MAX);
		}
		~KeyMappingSerializable() {}

		enum IIKeyType
		{
			II_KEY_LEFT,
			II_KEY_RIGHT,
			II_KEY_UP,
			II_KEY_DOWN,
			II_KEY_QTE1,// + game validate
			II_KEY_QTE2,
			II_KEY_QTE3,
			II_KEY_QTE4,
			II_KEY_QTE5,
			II_KEY_QTE6,
			II_KEY_PAUSE,
			II_KEY_QUICK_MENU,
			II_KEY_MAX
		};

		EKEY_CODE m_KeyMapping1[II_KEY_MAX];// in game
		EKEY_CODE m_KeyMapping2[II_KEY_MAX];// menus

		bool MapKey1(IIKeyType IIKey, EKEY_CODE keyCode)
		{
			if((u32)keyCode != 0x0)
				for(u32 i = 0; i < II_KEY_MAX; ++i)
					if(i != IIKey && keyCode == m_KeyMapping1[i])// already used
						m_KeyMapping1[i] = (EKEY_CODE)0x0;// unmap it
			//						return false;

			m_KeyMapping1[IIKey] = keyCode;
			return true;
		}
		bool MapKey2(IIKeyType IIKey, EKEY_CODE keyCode)
		{
			if((u32)keyCode != 0x0)
				for(u32 i = 0; i < II_KEY_MAX; ++i)
					if(i != IIKey && keyCode == m_KeyMapping2[i])// already used
						m_KeyMapping2[i] = (EKEY_CODE)0x0;// unmap it
			// 						return false;

			m_KeyMapping2[IIKey] = keyCode;
			return true;
		}

		EKEY_CODE GetKeyCode1(IIKeyType IIKey)
		{
			return m_KeyMapping1[IIKey];
		}
		EKEY_CODE GetKeyCode2(IIKeyType IIKey)
		{
			return m_KeyMapping2[IIKey];
		}

		bool operator ==(const KeyMappingSerializable& kms)
		{
			return (memcmp(&kms, this, sizeof(KeyMappingSerializable)) == 0);
		}

		bool operator !=(const KeyMappingSerializable& kms)
		{
			return (memcmp(&kms, this, sizeof(KeyMappingSerializable)) != 0);
		}

	};

	OptionDataSerializable m_ODS;
	KeyMappingSerializable m_KMS;

protected:
	ProfileData();

	static ProfileData* ms_Instance;

	bool m_IsLoaded;

	io::path m_RootFullPath;
};
#endif

#endif //#ifdef ENGINE_H
