/** Example 014 Win32 Window

This example only runs under MS Windows and demonstrates that Irrlicht can
render inside a win32 window. MFC and .NET Windows.Forms windows are possible,
too.

In the begining, we create a windows window using the windows API. I'm not
going to explain this code, because it is windows specific. See the MSDN or a
windows book for details.
*/

#include <irrlicht.h>
#include <stdio.h>

#if defined(_IRR_WINDOWS_)
#include <windows.h>
#define sleep Sleep
#else
#include <unistd.h>
#endif

#include "utils.h"
#include "engine.h"
#include "view.h"

using namespace irr;

#pragma comment(lib, "irrlicht.lib")

//! Define _NGE_DISABLE_CONSOLE_ON_VC_RELEASE to enable console Only on Debug build with VC.
/*! This shows console only when the engine is compiled on Debug build. Release build
wont have the console shown. Comment this line out to disable this feature which means
the console will always be shown. */
#define _NGE_DISABLE_CONSOLE_ON_VC_RELEASE
#if defined(_NGE_DISABLE_CONSOLE_ON_VC_RELEASE)
#   if defined(_IRR_WINDOWS_)
#      if defined(_DEBUG)
#         pragma comment(linker, "/subsystem:console")
#      else
#         pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#      endif
#   endif
#endif

/*
   Now ask for the driver and create the Windows specific window.
*/
int main()
{
	USE_PREMULTIPLIED_ALPHA = false;

	Engine::ms_WindowSize = Dimension2(1024, 640);
	Engine::ms_FullScreen = false;
	Engine* engine = Engine::GetInstance();// will initialize the engine
	IrrlichtDevice* device = engine->GetDevice();

#ifdef _IRR_WINDOWS_
#ifndef _DEBUG
	FILE *out = fopen(engine->GetGamePath("out.txt").c_str(), "w");
	FILE *err = fopen(engine->GetGamePath("err.txt").c_str(), "w");

	*stdout = *out;
	*stderr = *err;
#endif
#endif

	video::IVideoDriver* driver = engine->GetDriver();
	scene::ISceneManager* smgr = engine->GetSceneMgr();
	gui::IGUIEnvironment* gui = engine->GetGui();

	Editor* editor = Editor::GetInstance();// will initialize the editor

	int lastFPS = -1;

	u32 then = device->getTimer()->getTime();

	while(device->run() && driver)
	{
		const u32 now = device->getTimer()->getTime();
		f32 frameDeltaTime = (f32)(now - then) / 1000.f; // Time in seconds
		if(frameDeltaTime > 0.1f)
			frameDeltaTime = 0.1f;

		then = now;

		if(frameDeltaTime <= 0.0f)
			continue;

		engine->Update(frameDeltaTime);

		Dimension2 curRenderSize = engine->GetDriver()->getCurrentRenderTargetSize();
		if(engine->ms_WindowSize != curRenderSize)
		{
			engine->ms_WindowSize = curRenderSize;
			ProfileData::GetInstance()->m_ODS.m_WindowSize = engine->ms_WindowSize;
			ProfileData::GetInstance()->WriteSave();

			engine->RecomputeSize();
		}

		if (device->isWindowActive())
		{
			driver->beginScene(true, true, video::SColor(0,200,200,200));

			editor->Update(frameDeltaTime);

			gui->drawAll();

			driver->endScene();

#ifdef PROFILE_FINE
			ProfilingLogger::TickProfiler();
			if( GetAsyncKeyState( VK_F10 ) &1 )
			{
				ProfilingLogger::StartProfiling(20);
			}
#endif

			// display frames per second in window title
			//printf("dt = %.03f\n", frameDeltaTime);
			int fps = driver->getFPS();
			if (lastFPS != fps)
			{
				core::stringw str = WINDOW_TITLE_STRING;
				str += " - FPS:";
				str += fps;

				device->setWindowCaption(str.c_str());
				lastFPS = fps;
			}
		}

#if defined(_IRR_WINDOWS_)
		sleep(10);
#endif
	}

	editor->DeleteInstance();
	engine->DeleteInstance();

	return 0;
}

/*
That's it, Irrlicht now runs in your own windows window.
**/
