#ifndef SPRITEVIEW_H
#define SPRITEVIEW_H

#include "utils.h"
#include "sprite.h"

//#include "driverChoice.h"

class SpriteInterface
{
public:
	SpriteInterface();
	~SpriteInterface();

	bool AddImageToBank(Texture* tex);
	bool RemoveBankImageAtIndex(u32 index, bool& isAnyLayerRemovedOut);
	bool ClearBankImages();

	bool AddNewFrame();
	bool InsertNewFrameAtIndex(u32 index, s32 indexToCopy = -1);
	bool RemoveFrameAtIndex(u32 index);
	bool ClearFrames();

	bool AddNewLayer();
	bool InsertNewLayerAtIndex(u32 index, s32 indexToCopy = -1);
	bool RemoveLayerAtIndex(u32 index);
	bool ClearLayers();
	bool SwapLayers(u32 index1, u32 index2);


	SmartPointer<AnimatedSprite>& GetSprite() {return m_Sprite;}

	FrameSprite* GetCurrentFrame() const {return m_CurrentFrame;}
	void SetCurrentFrame(s32 index) 
	{
		m_CurrentLayer = NULL; 
		if(index >= 0 && (u32)index < m_Sprite->GetFrames().size()) 
			m_CurrentFrame = m_Sprite->GetFrames()[index]; 
		else 
			m_CurrentFrame = NULL;
	}

	LayerSprite* GetCurrentLayer() const {return m_CurrentLayer;}
	void SetCurrentLayer(s32 index) 
	{
		if(!m_CurrentFrame)
			return;
		if(index >= 0 && (u32)index < m_CurrentFrame->GetLayers().size()) 
			m_CurrentLayer = m_CurrentFrame->GetLayers()[index]; 
		else 
			m_CurrentLayer = NULL;
	}

	bool LoadSprite(const io::path &name);
	bool SaveSprite(const io::path &name);

	void EditSprite(const SmartPointer<AnimatedSprite>& sprite);

protected:
	bool _CheckRemoveImage(Texture* tex);// check if the texture exists in the frames and remove layer containing this texture
	SmartPointer<AnimatedSprite> m_Sprite;

	FrameSprite* m_CurrentFrame;
	LayerSprite* m_CurrentLayer;
};

class EditorWindow : public gui::IGUIElement
{
public:
	EditorWindow(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~EditorWindow();

	void SetSpriteInterface(SpriteInterface* sprInterface) {m_SpriteInterface = sprInterface;}

	virtual void draw();

	void UpdateRenderTarget(float dt);

	bool GetDisplayDebugInfo() const {return m_DisplayDebugInfo;}
	void SetDisplayDebugInfo(bool state) {m_DisplayDebugInfo = state;}

	void OnSpriteChange();

	gui::IGUIImage* m_Viewer;
	gui::IGUIImage* m_ViewerBackground;
	gui::IGUIContextMenu* m_ContextualMenu;
	gui::IGUIFileOpenDialog* m_LoadFileDlg;
	gui::IGUIFileOpenDialog* m_SaveFileDlg;

	video::ITexture* m_RenderTarget;

	bool m_CurrentlyHovered;
	bool m_LayerCurrentlyDisplaced;
	Position2 m_LayerCurrentCursorPos;
	bool m_CtrlIsPressed;
	bool m_ShiftIsPressed;

protected:
	virtual bool OnEvent(const SEvent& event);

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	gui::IGUISpinBox* m_RenderZoomSpinBox;
	gui::IGUIStaticText* m_MouseCoord;

	gui::IGUISpinBox* m_SpriteWidthSpinBox;
	gui::IGUISpinBox* m_SpriteHeightSpinBox;
	gui::IGUISpinBox* m_SpriteFramerateSpinBox;

	gui::IGUISpinBox* m_FrameWidthSpinBox;
	gui::IGUISpinBox* m_FrameHeightSpinBox;

	gui::IGUISpinBox* m_LayerRotationSpinBox;
	gui::IGUISpinBox* m_LayerSrcPosXSpinBox;
	gui::IGUISpinBox* m_LayerSrcPosYSpinBox;
	gui::IGUISpinBox* m_LayerSrcWidthSpinBox;
	gui::IGUISpinBox* m_LayerSrcHeightSpinBox;
	gui::IGUISpinBox* m_LayerDestPosXSpinBox;
	gui::IGUISpinBox* m_LayerDestPosYSpinBox;
	gui::IGUISpinBox* m_LayerDestWidthSpinBox;
	gui::IGUISpinBox* m_LayerDestHeightSpinBox;

	SpriteInterface* m_SpriteInterface;

	bool m_DisplayOtherLayers;
	Dimension2 m_lastSize;
	bool m_DisplayDebugInfo;

	float m_RenderZoom;
};

class LayerMenu : public gui::IGUIElement
{
public:
	LayerMenu(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~LayerMenu();

	void SetSpriteInterface(SpriteInterface* sprInterface) {m_SpriteInterface = sprInterface;}
	void OnFrameChanged();
	void OnLayersChanged();

	void SetEditorWindow(EditorWindow* editorWindow) {m_EditorWindow = editorWindow;}

	void OnSpriteChange();

	gui::IGUIListBox* m_ListBox;
	gui::IGUIContextMenu* m_ContextualMenu;

	bool m_CurrentlyHovered;

protected:
	virtual bool OnEvent(const SEvent& event);

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	SpriteInterface* m_SpriteInterface;

	EditorWindow* m_EditorWindow;
};

class FrameScrollMenu : public gui::IGUIElement
{
public:
	FrameScrollMenu(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~FrameScrollMenu();

	void SetSpriteInterface(SpriteInterface* sprInterface) {m_SpriteInterface = sprInterface;}
	void SetLayerMenu(LayerMenu* layerMenu) {m_LayerMenu = layerMenu;}
	void SetEditorWindow(EditorWindow* editorWindow) {m_EditorWindow = editorWindow;}

	void GoNextFrame();

	void OnSpriteChange();

	gui::IGUIScrollBar* m_ScrollBar;
	gui::IGUIContextMenu* m_ContextualMenu;
	gui::IGUIButton* m_ButtonPlay;

	bool m_CurrentlyHovered;
	bool m_IsPlaying;

protected:
	virtual bool OnEvent(const SEvent& event);

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;
	gui::IGUIStaticText* m_Frames;

	SpriteInterface* m_SpriteInterface;
	LayerMenu* m_LayerMenu;
	EditorWindow* m_EditorWindow;
};

class SpriteBankListBox : public gui::IGUIElement
{
public:
	SpriteBankListBox(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~SpriteBankListBox();

	void SetSpriteInterface(SpriteInterface* sprInterface) {m_SpriteInterface = sprInterface;}

	bool AddTextureToBank(const io::path &name);

	void SetLayerMenu(LayerMenu* layerMenu) {m_LayerMenu = layerMenu;}
	void SetEditorWindow(EditorWindow* editorWindow) {m_EditorWindow = editorWindow;}
	void SetFrameScrollMenu(FrameScrollMenu* frameScrollMenu) {m_FrameScrollMenu = frameScrollMenu;}

	void OnSpriteChange();

	gui::IGUIListBox* m_ListBox;
	gui::IGUIContextMenu* m_ContextualMenu;
	gui::IGUIFileOpenDialog* m_OpenFileDlg;

	bool m_CurrentlyHovered;

protected:
	virtual bool OnEvent(const SEvent& event);

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	SpriteInterface* m_SpriteInterface;
	LayerMenu* m_LayerMenu;
	EditorWindow* m_EditorWindow;
	FrameScrollMenu* m_FrameScrollMenu;
};


#endif //#ifdef SPRITEVIEW_H
