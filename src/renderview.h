#ifndef RENDERVIEW_H
#define RENDERVIEW_H

#include "utils.h"
#include "sprite.h"
#include "scene.h"

#include "sceneview.h"

//#include "driverChoice.h"

class RenderWindow : public gui::IGUIElement
{
public:
	RenderWindow(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~RenderWindow();

	enum FileLoadType {FLT_NONE, FLT_TEXTURE, FLT_SOUND_BG, FLT_SOUND_VOICE};

	void ResetSceneInterface();

	virtual void draw();

	void LoadScene();

	void UpdateRenderTarget(float dt);

	void OnSceneChange();
	
	gui::IGUIImage* m_Viewer;
	gui::IGUIImage* m_ViewerBackground;
	gui::IGUIContextMenu* m_ContextualMenu;

	gui::IGUIFileOpenDialog* m_OpenFileDlg;

	video::ITexture* m_RenderTarget;

	bool m_CurrentlyHovered;
protected:
	virtual bool OnEvent(const SEvent& event);

	void ChangeRenderTargetSize();

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	gui::IGUIScrollBar* m_ViewerWScrollBar;
	gui::IGUIScrollBar* m_ViewerHScrollBar;

	gui::IGUISpinBox* m_RenderWidthSpinBox;
	gui::IGUISpinBox* m_RenderHeightSpinBox;
	gui::IGUIComboBox* m_LanguageTypeComboBox;

	Position2 m_ViewerPosition;

	SceneInterface* m_SceneInterface;

	Dimension2 m_lastSize;

	float m_CurrentDlgTime;
	bool m_IsPlaying;

	TArray<Rectangle2> m_DialogChoiceAreas;
};

#endif //RENDERVIEW_H
