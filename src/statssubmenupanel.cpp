﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

enum StringDesc {SD_PROGRESSION = Engine::SFTID_StatsSubMenuPanel, SD_PLAYTIME, SD_CHAPTER, SD_FAILS, SD_MAX};

// StatsSubMenuPanel
StatsSubMenuPanel::StatsSubMenuPanel() : IPanel()
, m_ActiveBG(NULL)
, m_CurrentSelection(0)
, m_CurrentSubMenuActive(SMAT_BOTTOM)
, m_PreviousSubMenuActive(SMAT_BOTTOM)
, m_CurrentSelectionValidatedFader(0.2f)
, m_PopupPanel(NULL)
{
	for(int i = 0; i < 2; ++i)
		m_Font[i] = NULL;

	for(int i = 0; i < 2; ++i)
		m_Arrows[i] = NULL;

	bool useRT = true;
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	if(useRT && driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		m_RenderTarget = driver->addRenderTargetTexture(Engine::GetInstance()->m_RenderSize, "RTT_Stats", video::ECF_A8R8G8B8);
	}
	else
	{
		m_RenderTarget = NULL;
	}
}

StatsSubMenuPanel::~StatsSubMenuPanel()
{
	UnloadPanel();
}

void StatsSubMenuPanel::LoadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	SAFE_LOAD_IMAGE(driver, m_ActiveBG, engine->GetGamePath("system/02_Active_BG.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_Arrows[0], engine->GetGamePath("system/02_Chapter_L_arrow.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_Arrows[1], engine->GetGamePath("system/02_Chapter_R_arrow.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[2] = {48, 60};
		for(int i = 0; i < 2; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA), size/6);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	if(!m_PopupPanel)
	{
		m_PopupPanel = new PopupPanel(m_RenderTarget || USE_PREMULTIPLIED_ALPHA);
		m_PopupPanel->LoadPanel();
	}

	OnResize();

	//StartFade(PANEL_FADETIME);

	IPanel::LoadPanel();
}

void StatsSubMenuPanel::UnloadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_ActiveBG);

	SAFE_UNLOAD(m_Arrows[0]);
	SAFE_UNLOAD(m_Arrows[1]);

	for(int i = 0; i < 2; ++i)
		SAFE_UNLOAD(m_Font[i]);

	if(m_PopupPanel)
		m_PopupPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void StatsSubMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	const u32 sizes[2] = {48, 60};
	if(m_Font[0])
	{
		for(int i = 0; i < 2; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			m_Font[i]->setSize(size, size/6);
		}
	}

	{
		Dimension2 size = (m_ActiveBG)? m_ActiveBG->getSize() : Dimension2(1161, 834);
		size.Width += 100;
		size.Height += 60;
		Position2 pos = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)size.Height*innerScale)) + innerOffset;
		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)size.Height*innerScale));
		if(SBT_MAX > 1)
		{
			m_SubMenusArea[SMAT_CENTER].Area = Rectangle2(pos-cornerOffset, pos);
			m_SubMenusArea[SMAT_CENTER].IsSelected = false;

			pos.X -= cornerOffset.X;
			m_SubMenusArea[SMAT_LEFT].Area = Rectangle2(innerOffset, pos);
			m_SubMenusArea[SMAT_LEFT].IsSelected = false;
		}
		else
		{
			m_SubMenusArea[SMAT_CENTER].Area = Rectangle2(innerOffset, pos);
			m_SubMenusArea[SMAT_CENTER].IsSelected = false;

			pos.X -= cornerOffset.X;
			m_SubMenusArea[SMAT_LEFT].Area = Rectangle2(P2Zero, P2Zero);
			m_SubMenusArea[SMAT_LEFT].IsSelected = false;
		}

		pos.X = innerOffset.X;
		cornerOffset = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)originalSize.Height*innerScale))+innerOffset;
		m_SubMenusArea[SMAT_BOTTOM].Area = Rectangle2(pos, cornerOffset);
		m_SubMenusArea[SMAT_BOTTOM].IsSelected = false;

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	if(m_RenderTarget && renderSize != m_RenderTarget->getSize())
		m_RenderTarget = driver->addRenderTargetTexture(renderSize, "RTT_Stats", video::ECF_A8R8G8B8);

	if(m_PopupPanel)
	{
		m_PopupPanel->OnResize();
	}
}

void StatsSubMenuPanel::Update(float dt)
{
	IPanel::Update(dt);

	m_CurrentSelectionValidatedFader.Tick(dt);

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Update(dt);
		}
	}
}

void StatsSubMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor off = IsFading()?
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0), 4.0f*m_Fader.GetRatio()-3.0f)
		: IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0);
	IColor on = COMPUTE_THEME_COLOR_FONT_GLOW(0);
	float ratio = m_CurrentSelectionValidatedFader.GetRealRatio();
	IColor onToOff = MathUtils::GetInterpolatedColor(off, on, ratio);
	IColor offToOn = MathUtils::GetInterpolatedColor(on, off, ratio);
	Rectangle2 rect(innerOffset, Dimension2((u32)(innerScale*originalSize.Width), (u32)(innerScale*originalSize.Height)));
	Rectangle2 rects[3] =
	{
		m_SubMenusArea[SMAT_LEFT].Area,
		m_SubMenusArea[SMAT_CENTER].Area,
		m_SubMenusArea[SMAT_BOTTOM].Area
	};
	if(m_CurrentSubMenuActive == SMAT_LEFT)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 1011
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_CENTER) // 0111 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1011
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, off, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_CENTER)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 0111
		{
			fill2DRect(driver, rects[0], off, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], off, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_BOTTOM)// 1111
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1010 -> 1111
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_POPUP)// 0000
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 0000
		{
			fill2DRect(driver, rects[0], off, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_BOTTOM) // 1111 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else
		{
			fill2DRect(driver, rects[0], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
			fill2DRect(driver, rects[1], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
		}
		fill2DRect(driver, rects[2], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
	}
	else
	{
		assert(false);
		fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
	}
}

void StatsSubMenuPanel::RenderDefault(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_ActiveBG)
	{
		ProfileData* profil = ProfileData::GetInstance();

		const u32 margin = 30;

		IColor color = isCenterActive? COMPUTE_THEME_COLOR(255) : COMPUTE_THEME_COLOR(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);

		const Dimension2& size = m_ActiveBG->getSize();
		float scale2 = (float)size.Width*scale*0.9f/originalSize.Width;
		Position2 pos((s32)((((f32)originalSize.Width-0.95f*size.Width-100)*scale + offset.X)*innerScale), (s32)((((f32)60+0.05f*size.Height)*scale + offset.Y)*innerScale));
		if(isCenterActive)
		{
			float scale3 = scale2*innerScale;
			Position2 pos3 = pos + Position2((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f)) + innerOffset;



			Position2 halfCorner((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f));
			m_ActiveValidationArea.Area = Rectangle2(pos3-halfCorner, pos3+halfCorner);
		}

		if(m_Font[1])
		{
			const ProfileData::ProfileDataSerializable& PDS = ProfileData::GetInstance()->m_PDS;
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width - margin)-100)*scale + offset.X)*innerScale)),
				(s32)((((f32)(60)*scale + offset.X)*innerScale)));
			pos += innerOffset;
			pos.Y += (u32)((float)margin * innerScale);

			Dimension2 dim = Dimension2((s32)((float)(size.Width - 2*margin) * innerScale), (s32)((float)(size.Height - 2*margin) * innerScale));

			core::stringw str = StringFormat(L"%ls : %d %%", Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_PROGRESSION),
				(u32)(100.0f*GameDescManager::GetGameDesc()->GetProgressionRatio(PDS.m_ProgressionCH, PDS.m_ProgressionSC)));
			m_Font[1]->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);
			pos.Y += m_Font[1]->getSize() + (u32)((float)10 * innerScale);


			u32 nbCH = core::min_(GameDescManager::GetGameDesc()->ChaptersDesc.size(), (u32)PROFILE_CHAPTERS_MAX);
			{
				float fPlaytime = 0.0f;
				for(u32 i = 0; i < nbCH; ++i)
					fPlaytime += PDS.m_TimeCH[i];
				u32 playtime = (u32)fPlaytime;
				u32 hh = playtime/3600;
				u32 mm = (playtime - hh*3600)/60;
				u32 ss = (playtime - hh*3600 - mm*60);
				if(playtime != 0)
					str = StringFormat(L"%ls : %d:%02d:%02d", Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_PLAYTIME), hh, mm, ss);
				else
					str = StringFormat(L"%ls : -", Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_PLAYTIME));
				m_Font[1]->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);
				pos.Y += m_Font[1]->getSize() + (u32)((float)10 * innerScale);
			}
			pos.X += (u32)((float)margin * innerScale);
			s32 baseY = pos.Y;
			u32 splitVal = (nbCH > 10)? (nbCH+1)/2 : nbCH;
			for(u32 i = 0; i < nbCH; ++i)
			{
				if(i == splitVal)
				{
					pos.Y = baseY;
					pos.X += (s32)dim.Width;
				}

				u32 playtime = (u32)PDS.m_TimeCH[i];
				u32 hh = playtime/3600;
				u32 mm = (playtime - hh*3600)/60;
				u32 ss = (playtime - hh*3600 - mm*60);
				if(playtime != 0)
					str = StringFormat(L"%ls %d : %d:%02d:%02d", Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_CHAPTER), i+1, hh, mm, ss);
				else
					str = StringFormat(L"%ls %d : -", Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_CHAPTER), i+1);
				m_Font[1]->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);
				pos.Y += m_Font[1]->getSize() + (u32)((float)10 * innerScale);
			}
			pos.X -= (u32)((float)margin * innerScale);

			str = StringFormat(L"%ls : %d", Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_FAILS), PDS.m_nbQTEFail);
			m_Font[1]->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);
			pos.Y += m_Font[1]->getSize() + (u32)((float)10 * innerScale);
		}
	}
}

void StatsSubMenuPanel::_Render(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	RenderFader(scale, offset);


	// center part
	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	// draw the active BG at the center
	if(m_ActiveBG)
	{
		IColor tint = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width-(size.Width>>1)-100)*innerScale),
			(s32)((f32)((size.Height>>1)+60)*innerScale));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X*scale+(f32)offset.X*innerScale + innerOffset.X, (f32)pos.Y*scale+(f32)offset.Y*innerScale + innerOffset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

		draw2DImage(driver, m_ActiveBG, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	}

	switch(m_CurrentSelection)
	{
	case SBT_DEFAULT:
		RenderDefault(scale, offset);
		break;
	default:
		assert(0);
		break;
	}

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Render();
		}
	}
}

void StatsSubMenuPanel::PreRender()
{
	// render here only if we have a render target : we render in the render target
	if(m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		// set render target texture
		IColor color = COLOR_TRANSPARENT;
		color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(m_RenderTarget, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		_Render();

		// set back old render target
		// The buffer might have been distorted, so clear it
		color = COLOR_TRANSPARENT;
		if(USE_PREMULTIPLIED_ALPHA)
			color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(0, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

void StatsSubMenuPanel::Render()
{
	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)),
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_Render(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();

		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)*Engine::GetInstance()->m_InnerScale), 0);

		IColor tint = COLOR_WHITE;

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((Engine::GetInstance()->m_RenderSize.Width>>1),
			(Engine::GetInstance()->m_RenderSize.Height>>1));

		if(m_CurrentSelectionValidatedFader.IsFading())
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// other to bottom fade
				ratio = 0.75f * ratio + m_CurrentSelectionValidatedFader.GetRealRatio()*0.25f;// 1 to 0.75
			else if(m_PreviousSubMenuActive == SMAT_BOTTOM)// bottom to other fade
				ratio = 0.75f * ratio + (1.0f - m_CurrentSelectionValidatedFader.GetRealRatio())*0.25f;// 0.75 to 1
		}
		else
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// bottom
				ratio = 0.75f*ratio;// 0.75
		}

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

bool StatsSubMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if(m_PopupPanel)
	{
		if(m_PopupPanel->OnEvent(event))
		{
			if(!m_PopupPanel->IsVisible())// this means that the popup just closed
			{
				ChangeSubMenuActive(m_PreviousSubMenuActive);
			}

			return true;
		}
	}

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Engine* engine = Engine::GetInstance();
			const Dimension2& renderSize = engine->m_RenderSize;
			const Dimension2& originalSize = engine->m_OriginalSize;
			float innerScale = engine->m_InnerScale;
			const Position2& innerOffset = engine->m_InnerOffset;

			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);

			// check which sub menu is active
			if(!m_CurrentSelectionValidatedFader.IsFading())
			{
				u32 oldSubMenuActive = m_CurrentSubMenuActive;
				for(int i = 0; i < SMAT_MAX; ++i)
				{
					m_SubMenusArea[i].IsSelected = false;
					if(m_SubMenusArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentSubMenuActive = i;
					}
				}
				m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
				if(oldSubMenuActive != m_CurrentSubMenuActive)
				{
					m_PreviousSubMenuActive = oldSubMenuActive;
					m_CurrentSelectionValidatedFader.StartFade();
				}
			}

			// center area
			m_ActiveValidationArea.IsSelected = m_ActiveValidationArea.Area.isPointInside(mousePos);
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{

		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		//if(m_SelectionFadeTimer == 0.0f)
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					// reset center area selection
					{
						m_ActiveValidationArea.IsSelected = false;
					}

					ChangeSubMenuActive((SBT_MAX > 1)? SMAT_LEFT : SMAT_BOTTOM);
				}
				else if(SBT_MAX > 1 && m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_BOTTOM);
				}
				return true;// MUST BYPASS THE MAINPANEL ESCAPE EVENT
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive((SBT_MAX > 1)? SMAT_LEFT : SMAT_CENTER);
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{

				}
				//m_TransitToChildPanelIndex = 0;
				//StartFade(PANEL_FADETIME);
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				if(SBT_MAX > 1 && m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					--m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
						m_CurrentSelection = 0;
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
				}
				else if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive((SBT_MAX > 1)? SMAT_LEFT : SMAT_BOTTOM);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{

				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				if(SBT_MAX > 1 && m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					++m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
					{
						m_CurrentSelection = SBT_MAX - 1;
						ChangeSubMenuActive(SMAT_BOTTOM);
					}
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{

				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{

				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{

				}
				else if(SBT_MAX > 1 && m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
			}
		}
	}

	return (m_CurrentSubMenuActive != SMAT_BOTTOM);// bypass the main panel events if we don't have the focus on the bottom element
}

#undef PANEL_FADETIME
