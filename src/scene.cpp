#include "scene.h"
#include "engine.h"
#include "utils.h"


#include <stdio.h>
//#include <malloc.h>

#define SCENE_FPS 60
const float SCENE_DT = 1.0f/(float)SCENE_FPS;

GameDesc* GameDescManager::ms_GameDesc = NULL;

u32 GameDesc::m_CurrentChapter = 0;
u32 GameDesc::m_CurrentScene = 0;
u32 GameDesc::m_CurrentDialog = 0;
AudioDataSerializable GameDesc::m_CurrentSounds[3] = {AudioDataSerializable(), AudioDataSerializable(), AudioDataSerializable()};
bool GameDesc::m_RequestSceneStart = false;


// SpriteEntity
SpriteEntity::SpriteEntity(const SmartPointer<AnimatedSprite>& animatedSprite, const SpriteDataScene& data):
m_AnimatedSprite(animatedSprite),
m_Data(data),
m_Position(P2Zero),
m_Scale(1.0, 1.0f),
m_Rotation(0.0f),
m_Tint(COLOR_WHITE),
m_CurrentFrame(0),
m_CurrentTime(0.0f)
{
	assert(m_AnimatedSprite.IsValid());
}

SpriteEntity::~SpriteEntity()
{

}

void SpriteEntity::Update(float dt)
{
	if(m_AnimatedSprite->GetFrames().empty())
		return;

	m_CurrentTime += dt;
	if(m_CurrentTime >= m_AnimatedSprite->GetFramerate())
	{
		++m_CurrentFrame;
		m_CurrentTime -= m_AnimatedSprite->GetFramerate();

		if(m_CurrentFrame >= m_AnimatedSprite->GetFrames().size())
			m_CurrentFrame = 0;
	}
}

void SpriteEntity::Render()
{
	if(m_AnimatedSprite->GetFrames().empty())
		return;
	if(m_CurrentFrame < m_AnimatedSprite->GetFrames().size())
	{
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_Position.X, (f32)m_Position.Y, 0.0f));
		m_AnimatedSprite->GetFrames()[m_CurrentFrame]->Render(mat, m_Tint);
	}
}

void SpriteEntity::Render(const core::matrix4& matrix)
{
	if(m_AnimatedSprite->GetFrames().empty())
		return;
	if(m_CurrentFrame < m_AnimatedSprite->GetFrames().size())
	{
		m_AnimatedSprite->GetFrames()[m_CurrentFrame]->Render(matrix, m_Tint);
	}
}

void SpriteEntity::RenderDebugInfo(bool recursive/* = false*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	driver->draw2DRectangleOutline(Rectangle2(m_Position, m_AnimatedSprite->GetSize()), IColor(255, 0, 255, 0));

	if(recursive)
	{
		if(m_CurrentFrame < m_AnimatedSprite->GetFrames().size())
		{
			core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_Position.X, (f32)m_Position.Y, 0.0f));
			m_AnimatedSprite->GetFrames()[m_CurrentFrame]->RenderDebugInfo(mat, false, false, recursive);
		}
	}
}


// DialogNode
DialogNode::DialogNode(u32 id/* = 0xffffffff*/) :
m_ID(id),
m_UseTextBackGround(true),
m_DialogSpeed(15.0f),
m_DialogDuration(0.0f),
m_DialogPosition(0.02f, 0.75f),
m_DialogSize(0.96f, 0.25f),
m_NodeType(DN_AUTO_VALID),
m_ImageScene(new ImageScene),
m_KeepPreviousSpritesAlive(false),
m_ScreenShakeStrength(0.0f),
m_ScreenShakeFrequency(1.0f),
m_IsLoaded(false),
m_LoadCount(0)
{
	for(u32 i = 0; i < Engine::LS_MAX; ++i)
	{
		m_DialogHeader[i] = L"";
		m_DialogText[i] = L"";
	}
	m_DialogHeader[Engine::LS_MAX] = L"Langue non supportee par le moteur !";
	m_DialogText[Engine::LS_MAX] = L"Langue non supportee par le moteur !";
}

DialogNode::DialogNode(DialogNode* dlg)
{
	*this = *dlg;
	m_ParentNodes.clear();
	TArray<DialogNode*> temp = m_ChildNodes;
	m_ChildNodes.clear();
	for(u32 i = 0; i < temp.size(); ++i)
		BranchToNode(temp[i]);
}

DialogNode::~DialogNode()
{
	if(m_IsLoaded)
		UnLoad();

	// remove children's link
	while(!m_ChildNodes.empty())
		RemoveChildAtIndex(0);

	// remove parents' link
	while(!m_ParentNodes.empty())
		RemoveParentAtIndex(0);
}

bool DialogNode::BranchToNode(DialogNode* childNode)
{
	if(!childNode)
		return false;

	m_ChildNodes.push_back(childNode);
	childNode->_AddParentNode(this);

	for(u32 i = 0; i < Engine::LS_MAX; ++i)
		if(m_ChildNodes.size() > m_ConditionsText[i].size())
			AddConditionalText(L"", i);

	return true;
}

bool DialogNode::RemoveChildAtIndex(u32 index)
{
	if(index >= m_ChildNodes.size())
		return false;

	DialogNode* child = m_ChildNodes[index];
	s32 parentIdx = child->_GetParentNodeIndex(this);
	if(parentIdx>=0)
	{
		child->_RemoveParentAtIndex((u32)parentIdx);
	}
	_RemoveChildAtIndex(index);

	RemoveConditionalTextAtIndex(index);

	return true;
}

bool DialogNode::RemoveParentAtIndex(u32 index)
{
	if(index >= m_ParentNodes.size())
		return false;

	DialogNode* parent = m_ParentNodes[index];
	s32 childIdx = parent->_GetChildNodeIndex(this);
	if(childIdx>=0)
	{
		parent->_RemoveChildAtIndex((u32)childIdx);
	}
	_RemoveParentAtIndex(index);

	parent->RemoveConditionalTextAtIndex(childIdx);

	return true;
}

void DialogNode::SetDialogHeader(const core::stringw& dialogHeader, u32 language)
{
	if(language < Engine::LS_MAX)
		m_DialogHeader[language] = dialogHeader;
	else if(language == Engine::LS_MAX)
	{
		for(u32 i = 0; i < Engine::LS_MAX; ++i)
			m_DialogHeader[i] = dialogHeader;
	}
}

void DialogNode::SetDialogText(const core::stringw& dialogText, u32 language)
{
	if(language < Engine::LS_MAX)
		m_DialogText[language] = dialogText;
	else if(language == Engine::LS_MAX)
	{
		for(u32 i = 0; i < Engine::LS_MAX; ++i)
			m_DialogText[i] = dialogText;
	}
}

void DialogNode::AddConditionalText(const core::stringw& conditionText, u32 language)
{
	if(language < Engine::LS_MAX)
		m_ConditionsText[language].push_back(conditionText);
	else if(language == Engine::LS_MAX)
	{
		for(u32 i = 0; i < Engine::LS_MAX; ++i)
			m_ConditionsText[i].push_back(conditionText);
	}
}

bool DialogNode::LoadFromFile(const char* name, bool useTextureSizeLimit/* = false*/)
{
	if(m_IsLoaded)
		return false;

	FILE* input = fopen(name, "rb");
	if(!input)
	{
		return false;
	}

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "DLGN", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	// current = 2.2

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// read background
	u32 nbBkgnd = 0;
	fread(&nbBkgnd, sizeof(u32), 1, input);
	if(nbBkgnd > 0)
	{
		Image* img = NULL;
		if(version[0] > 1 || (version[0] == 1 && version[1] >= 7))// from version 1.7
		{
			u32 size = 0;
			fread(&size, sizeof(u32), 1, input);// compressed size
			unsigned char* buffer = new unsigned char[size];
			fread(buffer, sizeof(unsigned char), size, input);// compressed data

			io::IReadFile* memFile  = Engine::GetInstance()->GetDevice()->getFileSystem()->createMemoryReadFile(buffer, size, "foo.jpg", false);
			if(memFile)
			{
				img = driver->createImageFromFile(memFile);

				memFile->drop();
			}
			else
			{
				assert(0);
			}

			delete[] buffer;
		}
		else
		{
			Dimension2 dim(0,0);
			fread(&(dim.Width), sizeof(u32), 1, input);// width
			fread(&(dim.Height), sizeof(u32), 1, input);// height
			u32 size = 0;
			fread(&size, sizeof(u32), 1, input);// non compressed size
			assert(size == 4*dim.Width*dim.Height);
			if(size != 4*dim.Width*dim.Height)
				return false;

			void* data = malloc(size);

			u32 compressedSize = 0;
			fread(&compressedSize, sizeof(u32), 1, input);// compressed size
			void* compressedData = malloc(compressedSize);
			fread(compressedData, sizeof(unsigned char), compressedSize, input);// compressed data
			int ret = RLEDecompressColors((unsigned char*)compressedData, compressedSize, (unsigned char*)data, size);
			free(compressedData);

			img = driver->createImageFromData(video::ECF_A8R8G8B8, dim, data);
			free(data);
		}

		float dx = (float)img->getDimension().Width/(float)TEXTURE_SIZE_LIMIT;
		float dy = (float)img->getDimension().Height/(float)TEXTURE_SIZE_LIMIT;
		float d = core::max_(dx, dy);

		if(useTextureSizeLimit && d > 1.0f)
		{
			Dimension2 dim((u32)((float)img->getDimension().Width/d), (u32)((float)img->getDimension().Height/d));
			Image* filter = driver->createImage(video::ECF_A8R8G8B8, dim);
			img->copyToScalingBoxFilter(filter, 0);
			img->drop();
			img = filter;
		}
// 		if(i==0)
// 			driver->writeImageToFile(img, "test.png");
		Texture* tex = ImageToTexture(img, L"texture");
		if(USE_PREMULTIPLIED_ALPHA)
			PremultiplyAlpha(tex);
		img->drop();
		//m_Image = tex;
		m_ImageScene->m_Image = tex;

		// keytimes
		u32 keySize = 0;
		fread(&keySize, sizeof(u32), 1, input);// nb keys
		for(u32 i = 0; i < keySize; ++i)
		{
			KeyTime key;
			if(version[0] >= 2)// from version 2.0
			{
				// read key
				fread(&key, sizeof(KeyTime), 1, input);
			}
			else
			{
				// read key
				fread(&key, sizeof(DeprecatedKeyTime), 1, input);
				key.m_Color = COLOR_WHITE;
			}
			if(useTextureSizeLimit && d > 1.0f)
			{
				key.m_Position.X = (u32)((float)key.m_Position.X/d);
				key.m_Position.Y = (u32)((float)key.m_Position.Y/d);
				key.m_Scale = key.m_Scale*d;
			}
			//m_ImageKeys.push_back(key);
			m_ImageScene->m_Keys.push_back(key);
		}

		if(version[0] > 1 || (version[0] == 1 && version[1] >= 3))// from version 1.3
		{
			fread(&m_ImageScene->m_Loop, sizeof(bool), 1, input);// loop ?
		}
	}

	fread(&m_NodeType, sizeof(DN_Type), 1, input);

	// read sprite indices
	u32 spritesSize = 0;
	fread(&spritesSize, sizeof(u32), 1, input);// size of sprites
	for(u32 i = 0; i < spritesSize; ++i)
	{
		SpriteDataScene data;
		fread(&data.m_Index, sizeof(u32), 1, input);// index of the sprite in the bank
		// keytimes
		u32 keySize = 0;
		fread(&keySize, sizeof(u32), 1, input);// nb keys
		for(u32 j = 0; j < keySize; ++j)
		{
			// write key
			KeyTime key;
			if(version[0] >= 2)// from version 2.0
			{
				// read key
				fread(&key, sizeof(KeyTime), 1, input);
			}
			else
			{
				// read key
				fread(&key, sizeof(DeprecatedKeyTime), 1, input);
				key.m_Color = COLOR_WHITE;
			}
			data.m_Keys.push_back(key);
		}
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 2))// from version 2.2
		{
			u8 tags = 0;
			fread(&tags, sizeof(tags), 1, input);
			data.m_MirrorH = ((tags & 1) != 0);
			data.m_MirrorV = (((tags>>1) & 1) != 0);
			data.m_Loop = (((tags>>2) & 1) != 0);
			data.m_LoopAnim = (((tags>>3) & 1) != 0);
		}
		else if(version[0] > 1 || (version[0] == 1 && version[1] >= 3))// from version 1.3
		{
			bool loop = false, loopAnim = false;
			fread(&loop, sizeof(bool), 1, input);// loop keys ?
			fread(&loopAnim, sizeof(bool), 1, input);// loop anim ?
			data.m_Loop = loop;
			data.m_LoopAnim = loopAnim;
		}
		m_SpritesData.push_back(data);
	}


	// read dialog name
	DeserializeStringW(m_DialogName, input);

	u32 nbLanguages = 1;
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 5))// from version 1.5 : Languages support
	{
		fread(&nbLanguages, sizeof(u32), 1, input);
	}
	// read header text
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 4))// from version 1.4
	{
		for(u32 i = 0; i < nbLanguages; ++i)
		{
			bool discard = false;
			if(i >= Engine::LS_MAX)// language present in the file not supported by this version of the engine : discard it
				discard = true;

			core::stringw temp = L"";
			DeserializeStringW(temp, input);
			if(!discard)
				m_DialogHeader[i] = temp;
		}
		for(u32 i = nbLanguages; i < Engine::LS_MAX; ++i)
		{
			m_DialogHeader[i] = L"";
		}
	}

	// read dialog text
	for(u32 i = 0; i < nbLanguages; ++i)
	{
		bool discard = false;
		if(i >= Engine::LS_MAX)// language present in the file not supported by this version of the engine : discard it
			discard = true;

		core::stringw temp = L"";
		DeserializeStringW(temp, input);
		if(!discard)
			m_DialogText[i] = temp;
	}
	for(u32 i = nbLanguages; i < Engine::LS_MAX; ++i)
	{
		m_DialogText[i] = L"";
	}

	if(version[0] > 1 || (version[0] == 1 && version[1] >= 4))// from version 1.4
	{
		fread(&m_UseTextBackGround, sizeof(bool), 1, input);
	}

	fread(&m_DialogSpeed, sizeof(float), 1, input);
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 6))// from version 1.6
	{
		fread(&m_DialogDuration, sizeof(float), 1, input);
	}

	if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))// from version 1.1
	{
		fread(&m_DialogPosition, sizeof(Vector2), 1, input);
		fread(&m_DialogSize, sizeof(Vector2), 1, input);
	}

	// read conditions text
	u32 conditionsSize = 0;
	fread(&conditionsSize, sizeof(u32), 1, input);// nb conditions
	u32 nbIter = (version[0] > 1 || (version[0] == 1 && version[1] >= 8))? nbLanguages : 1;// from version 1.8 : add the language support for condional texts
	for(u32 j = 0; j < nbIter; ++j)
	{
		for(u32 i = 0; i < conditionsSize; ++i)
		{
			// read text
			core::stringw temp = L"";
			DeserializeStringW(temp, input);
			if(m_ConditionsText[j].size() == conditionsSize)// already added : fill it
				m_ConditionsText[j][i] = temp;
			else
				m_ConditionsText[j].push_back(temp);
		}
	}
	for(u32 j = nbIter; j < Engine::LS_MAX; ++j)
	{
		for(u32 i = 0; i < conditionsSize; ++i)
		{
			if(m_ConditionsText[j].size() == conditionsSize)// already added : fill it
				m_ConditionsText[j][i] = L"";
			else
				m_ConditionsText[j].push_back(L"");
		}
	}

	// read inputs data
	u32 inputsSize = 0;
	fread(&inputsSize, sizeof(u32), 1, input);// nb inputs
	for(u32 i = 0; i < inputsSize; ++i)
	{
		float duration = 0.0f;
		fread(&duration, sizeof(float), 1, input);
		u32 inputs = 0;
		fread(&inputs, sizeof(u32), 1, input);
		u8 nbPress = 1;
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
			fread(&nbPress, sizeof(u8), 1, input);
		InputData data((InputData::EPadInput)inputs, duration, nbPress);
		m_Inputs.push_back(data);
	}

	// read bg sound file name
	{
		core::stringw temp = L"";
        DeserializeStringW(temp, input);
		if(temp.size() != 8 || temp[4] != L'.')
			temp = L"";
		SetSoundBG1(io::path(temp));
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
			fread(&m_SoundBG1->m_StreamData, sizeof(SoundDataScene::StreamData), 1, input);
	}

	if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))// from version 1.1
	{
		core::stringw temp = L"";
        DeserializeStringW(temp, input);
		if(temp.size() != 8 || temp[4] != L'.')
			temp = L"";
		SetSoundBG2(io::path(temp));
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
			fread(&m_SoundBG2->m_StreamData, sizeof(SoundDataScene::StreamData), 1, input);
	}

	// read m_Voice sound file name
	{
		core::stringw temp = L"";
        DeserializeStringW(temp, input);
		if(temp.size() != 8 || temp[4] != L'.')
			temp = L"";
		SetVoice(io::path(temp));
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
			fread(&m_Voice->m_StreamData, sizeof(SoundDataScene::StreamData), 1, input);
	}

	// read transitions
	fread(&m_TransitionIn, sizeof(TransitionData), 1, input);
	fread(&m_TransitionOut, sizeof(TransitionData), 1, input);

	// read screen shake data
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 4))// from version 1.4
	{
		fread(&m_ScreenShakeStrength, sizeof(float), 1, input);
		fread(&m_ScreenShakeFrequency, sizeof(float), 1, input);
	}

	fclose(input);

	++m_LoadCount;
	m_IsLoaded = true;
	return true;
}

bool DialogNode::UnLoad()
{
	if(!m_IsLoaded)
		return false;

// 	if(m_Image)
// 	{
// 		Engine::GetInstance()->GetDriver()->removeTexture(m_Image);
// 	}
// 	m_ImageKeys.clear();
	m_ImageScene = new ImageScene;

	m_SpritesData.clear();// clear sprite data
	m_Sprites.clear();// clear the sprite list

// 	for(u32 i = 0; i < Engine::LS_MAX; ++i)
// 		m_ConditionsText[i].clear();// clear conditions array

	m_Inputs.clear();

	if(m_SoundBG1.IsValid())
		m_SoundBG1->m_Sound = NULL;

	if(m_SoundBG2.IsValid())
		m_SoundBG2->m_Sound = NULL;

	if(m_Voice.IsValid())
		m_Voice->m_Sound = NULL;

	m_IsLoaded = false;
	return true;
}

#ifndef IRON_ICE_FINAL
bool DialogNode::SaveToFile(const char* name)
{
	if(!m_IsLoaded)
		return false;

	FILE* output = fopen(name, "wb");
	if(!output)
	{
		return false;
	}

	char header[5] = "DLGN";
	fwrite(header, sizeof(char), 4, output);// header

	// current version : 2.2
	unsigned char version[2] = {2, 2};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// write background
	u32 nbBkgnd = (m_ImageScene->m_Image == NULL)?0:1;
	fwrite(&nbBkgnd, sizeof(u32), 1, output);
	if(nbBkgnd > 0)
	{
		Image* img = TextureToImage(m_ImageScene->m_Image);
		u32 bpp = img->getBytesPerPixel();
		Dimension2 dim = img->getDimension();
		u32 size = dim.Width*dim.Height*bpp;
		assert(bpp == 4);

		if(version[0] > 1 || (version[0] == 1 && version[1] >= 7))// from version 1.7
		{
			unsigned char* buffer = new unsigned char[size];
			io::IWriteFile* memFile  = Engine::GetInstance()->GetDevice()->getFileSystem()->createMemoryWriteFile(buffer, size, "foo.jpg", false);
			if(memFile)
			{
				driver->writeImageToFile(img, memFile);
				u32 compressedSizeTotal = memFile->getPos();

				fwrite(&compressedSizeTotal, sizeof(u32), 1, output);// compressed size
				fwrite(buffer, sizeof(unsigned char), compressedSizeTotal, output);// data (compressed or not)

				memFile->drop();
			}
			else
			{
				assert(0);
			}

			delete[] buffer;
			img->drop();
		}
		else
		{
			void* data = img->lock();
			void* compressedData = malloc(size+4*sizeof(int));
			void* compressedDataOffset = compressedData;
			u32 compressedSizeTotal = 0;
			u32 compressedSize = 0;
			PreProcessAlphaPixels((unsigned char*)data, size);
			for(u32 i=0; i<4; ++i)// for each color
			{
				int ret = RLECompressColor((unsigned char*)(data)+i, size, (unsigned char*)(compressedDataOffset) + sizeof(int), &compressedSize);
				assert(ret != -1 && compressedSize <= dim.Width*dim.Height);
				assert(ret == 1 || compressedSize == dim.Width*dim.Height);
				*((int*)compressedDataOffset) = compressedSize;
				compressedSizeTotal += compressedSize + sizeof(int);
				compressedDataOffset = (void*)((unsigned char*)compressedData + compressedSizeTotal);
			}
			assert(compressedSizeTotal <= size+4*sizeof(int));
			img->unlock();
			img->drop();

			fwrite(&(dim.Width), sizeof(u32), 1, output);// width
			fwrite(&(dim.Height), sizeof(u32), 1, output);// height
			fwrite(&size, sizeof(u32), 1, output);// non compressed size
			fwrite(&compressedSizeTotal, sizeof(u32), 1, output);// compressed size

			fwrite(compressedData, sizeof(unsigned char), compressedSizeTotal, output);// data (compressed or not)
			free(compressedData);
		}

		// keytimes
		u32 keySize = m_ImageScene->m_Keys.size();
		fwrite(&keySize, sizeof(u32), 1, output);// nb keys
		for(u32 i = 0; i < keySize; ++i)
		{
			// write key
			KeyTime& key = m_ImageScene->m_Keys[i];
			if(version[0] >= 2)// from version 2.0
			{
				// write key
				fwrite(&key, sizeof(KeyTime), 1, output);
			}
			else
			{
				// write key
				fwrite(&key, sizeof(DeprecatedKeyTime), 1, output);
			}
		}
		if(version[0] > 1 || (version[0] == 1 && version[1] >= 3))// from version 1.3
		{
			fwrite(&m_ImageScene->m_Loop, sizeof(bool), 1, output);// loop ?
		}
	}

	fwrite(&m_NodeType, sizeof(DN_Type), 1, output);

	// write sprite indices
	u32 spritesSize = m_SpritesData.size();
	fwrite(&spritesSize, sizeof(u32), 1, output);// size of sprites
	for(u32 i = 0; i < spritesSize; ++i)
	{
		SpriteDataScene& data = m_SpritesData[i];
		fwrite(&data.m_Index, sizeof(u32), 1, output);// index of the sprite

		// keytimes
		u32 keySize = data.m_Keys.size();
		fwrite(&keySize, sizeof(u32), 1, output);// nb keys
		for(u32 j = 0; j < keySize; ++j)
		{
			// write key
			KeyTime& key = data.m_Keys[j];
			if(version[0] >= 2)// from version 2.0
			{
				// write key
				fwrite(&key, sizeof(KeyTime), 1, output);
			}
			else
			{
				// write key
				fwrite(&key, sizeof(DeprecatedKeyTime), 1, output);
			}
		}

		if(version[0] > 2 || (version[0] == 2 && version[1] >= 2))// from version 2.2
		{
			u8 tags = 0;
			tags |= ((u8)(data.m_MirrorH)? 1 : 0);
			tags |= ((u8)(data.m_MirrorV)? (1<<1) : 0);
			tags |= ((u8)(data.m_Loop)? (1<<2) : 0);
			tags |= ((u8)(data.m_LoopAnim)? (1<<3) : 0);
			fwrite(&tags, sizeof(u8), 1, output);
		}
		else if(version[0] > 1 || (version[0] == 1 && version[1] >= 3))// from version 1.3
		{
			bool loop = data.m_Loop, loopAnim = data.m_LoopAnim;
			fwrite(&loop, sizeof(bool), 1, output);// loop keys ?
			fwrite(&loopAnim, sizeof(bool), 1, output);// loop anim ?
		}
	}


	// write dialog name
	SerializeStringW(m_DialogName, output);

	u32 nbLanguages = Engine::LS_MAX;
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 5))// from version 1.5 : languages support
	{
		fwrite(&nbLanguages, sizeof(u32), 1, output);
	}

	// write header text
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 4))// from version 1.4
	{
		for(u32 i = 0; i < nbLanguages; ++i)
		{
			SerializeStringW(m_DialogHeader[i], output);
		}
	}

	// write dialog text
	for(u32 i = 0; i < nbLanguages; ++i)
	{
		SerializeStringW(m_DialogText[i], output);
	}

	if(version[0] > 1 || (version[0] == 1 && version[1] >= 4))// from version 1.4
	{
		fwrite(&m_UseTextBackGround, sizeof(bool), 1, output);
	}

	fwrite(&m_DialogSpeed, sizeof(float), 1, output);
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 6))// from version 1.6
	{
		fwrite(&m_DialogDuration, sizeof(float), 1, output);
	}

	if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))// from version 1.1
	{
		fwrite(&m_DialogPosition, sizeof(Vector2), 1, output);
		fwrite(&m_DialogSize, sizeof(Vector2), 1, output);
	}

	// write conditions text
	u32 conditionsSize = m_ConditionsText[0].size();
	fwrite(&conditionsSize, sizeof(u32), 1, output);// nb conditions
	u32 nbIter = (version[0] > 1 || (version[0] == 1 && version[1] >= 8))? nbLanguages : 1; // from version 1.8 : language support for conditional texts
	for(u32 j = 0; j < nbIter; ++j)
	{
		for(u32 i = 0; i < conditionsSize; ++i)
		{
			// write text
			SerializeStringW(m_ConditionsText[j][i], output);
		}
	}

	// write inputs data
	u32 inputsSize = m_Inputs.size();
	fwrite(&inputsSize, sizeof(u32), 1, output);// nb inputs
	for(u32 i = 0; i < inputsSize; ++i)
	{
		float duration = m_Inputs[i].m_Duration;
		fwrite(&duration, sizeof(float), 1, output);
		u32 input = m_Inputs[i].m_Inputs;
		fwrite(&input, sizeof(u32), 1, output);
		u8 nbPress = m_Inputs[i].m_NbPress;
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
			fwrite(&nbPress, sizeof(u8), 1, output);
	}

	// write bg sound 1 file name
	u32 size = m_SoundBG1.IsValid()?m_SoundBG1->m_Path.size():0;
	if(size != 8 || m_SoundBG1->m_Path[4] != L'.')// path fucked : no way we serialize this shit !
	{
		size = 0;
	}
	fwrite(&size, sizeof(u32), 1, output);
	if(size > 0)
	{
		core::stringw str = m_SoundBG1->m_Path;
		fwrite(str.c_str(), sizeof(wchar_t), size, output);// write the name
	}
	if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
		if(m_SoundBG1.IsValid())
			fwrite(&m_SoundBG1->m_StreamData, sizeof(SoundDataScene::StreamData), 1, output);

	if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))// from version 1.1
	{
		// write bg sound 2 file name
		size = m_SoundBG2.IsValid()?m_SoundBG2->m_Path.size():0;
		if(size != 8 || m_SoundBG2->m_Path[4] != L'.')// path fucked : no way we serialize this shit !
		{
			size = 0;
		}
		fwrite(&size, sizeof(u32), 1, output);
		if(size > 0)
		{
			core::stringw str = m_SoundBG2->m_Path;
			fwrite(str.c_str(), sizeof(wchar_t), size, output);// write the name
		}
		if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
			if(m_SoundBG2.IsValid())
				fwrite(&m_SoundBG2->m_StreamData, sizeof(SoundDataScene::StreamData), 1, output);
	}

	// write m_Voice sound file name
	size = m_Voice.IsValid()?m_Voice->m_Path.size():0;
	fwrite(&size, sizeof(u32), 1, output);
	if(size != 8 || m_Voice->m_Path[4] != L'.')// path fucked : no way we serialize this shit !
	{
		size = 0;
	}
	if(size > 0)
	{
		core::stringw str = m_Voice->m_Path;
		fwrite(str.c_str(), sizeof(wchar_t), size, output);// write the name
	}
	if(version[0] > 2 || (version[0] == 2 && version[1] >= 1))// from version 2.1
		if(m_Voice.IsValid())
			fwrite(&m_Voice->m_StreamData, sizeof(SoundDataScene::StreamData), 1, output);

	// write transitions
	fwrite(&m_TransitionIn, sizeof(TransitionData), 1, output);
	fwrite(&m_TransitionOut, sizeof(TransitionData), 1, output);

	// read screen shake data
	if(version[0] > 1 || (version[0] == 1 && version[1] >= 4))// from version 1.4
	{
		fwrite(&m_ScreenShakeStrength, sizeof(float), 1, output);
		fwrite(&m_ScreenShakeFrequency, sizeof(float), 1, output);
	}

	fclose(output);

	return true;
}

bool DialogNode::SaveToFileXML(const char* name)
{
	if(!m_IsLoaded)
		return false;

	const wchar_t* const languageSettingsString[Engine::LS_MAX] = {L"French", L"English", L"German", L"Spanish"};

	io::path fileName = name;

	io::path path = name;
	s32 index = path.findLastChar("/");
	assert(index != -1);
	path = path.subString(0, index+1);

	//create xml writer
	io::IFileSystem* fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	io::IXMLWriter* xwriter = fs->createXMLWriter( fileName );
	if (!xwriter)
		return false;

	// current version 2.2
	unsigned char version[2] = {2, 2};// major version, minor version

	// write the image files
	u32 nbBkgnd = (m_ImageScene->m_Image == NULL)?0:1;
	for(u32 i = 0; i < nbBkgnd; ++i)
	{
		char file[32];
		sprintf(file, "%04d.jpg", i);

		fileName = path;
		fileName += file;
		Image* img = TextureToImage(m_ImageScene->m_Image);
		Engine::GetInstance()->GetDriver()->writeImageToFile(img, fileName.c_str());		
		img->drop();
	}

	xwriter->writeXMLHeader();

	xwriter->writeElement(L"Dialog");
	xwriter->writeLineBreak();

	xwriter->writeElement(L"Version", true, L"Major", core::stringw((u32)version[0]).c_str(), L"Minor", core::stringw((u32)version[1]).c_str());
	xwriter->writeLineBreak();
	xwriter->writeLineBreak();

	xwriter->writeElement(L"DialogNode");
	xwriter->writeLineBreak();

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// write background
	// write layers
	xwriter->writeElement(L"BackGrounds");
	xwriter->writeLineBreak();
	if(nbBkgnd > 0)
	{
		xwriter->writeElement(L"BackGround");
		xwriter->writeLineBreak();

		char file[32];
		sprintf(file, "%04d.jpg", 0);
		xwriter->writeElement(L"Filename");
		xwriter->writeText(core::stringw(file).c_str());
		xwriter->writeClosingTag(L"Filename");
		xwriter->writeLineBreak();

		// keytimes
		xwriter->writeElement(L"KeyTimes");
		xwriter->writeLineBreak();
		u32 keySize = m_ImageScene->m_Keys.size();
		for(u32 i = 0; i < keySize; ++i)
		{
			xwriter->writeElement(L"KeyTime");
			xwriter->writeLineBreak();
			// write key
			KeyTime& key = m_ImageScene->m_Keys[i];
			xwriter->writeElement(L"Time");
			xwriter->writeText(core::stringw(key.m_Time).c_str());
			xwriter->writeClosingTag(L"Time");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"PositionX");
			xwriter->writeText(core::stringw(key.m_Position.X).c_str());
			xwriter->writeClosingTag(L"PositionX");
			xwriter->writeLineBreak();
			xwriter->writeElement(L"PositionY");
			xwriter->writeText(core::stringw(key.m_Position.Y).c_str());
			xwriter->writeClosingTag(L"PositionY");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"Scale");
			xwriter->writeText(core::stringw(key.m_Scale).c_str());
			xwriter->writeClosingTag(L"Scale");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"Rotation");
			xwriter->writeText(core::stringw(key.m_Rotation).c_str());
			xwriter->writeClosingTag(L"Rotation");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"Color");
			char color[32] = "";
			sprintf(color, "#%08x", key.m_Color.color);
			xwriter->writeText(core::stringw(color).c_str());
			xwriter->writeClosingTag(L"Color");
			xwriter->writeLineBreak();

			xwriter->writeClosingTag(L"KeyTime");
			xwriter->writeLineBreak();
		}
		xwriter->writeClosingTag(L"KeyTimes");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"Loop");
		xwriter->writeText(core::stringw(m_ImageScene->m_Loop).c_str());
		xwriter->writeClosingTag(L"Loop");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"BackGround");
		xwriter->writeLineBreak();
	}
	xwriter->writeClosingTag(L"BackGrounds");
	xwriter->writeLineBreak();


	xwriter->writeElement(L"NodeType");
	xwriter->writeText(core::stringw(m_ImageScene->m_Loop).c_str());
	xwriter->writeClosingTag(L"NodeType");
	xwriter->writeLineBreak();

	// write sprite indices
	xwriter->writeElement(L"SpriteInstances");
	xwriter->writeLineBreak();
	u32 spritesSize = m_SpritesData.size();
	for(u32 i = 0; i < spritesSize; ++i)
	{
		xwriter->writeElement(L"SpriteInstance");
		xwriter->writeLineBreak();

		SpriteDataScene& data = m_SpritesData[i];

		xwriter->writeElement(L"Index");
		xwriter->writeText(core::stringw(data.m_Index).c_str());
		xwriter->writeClosingTag(L"index");
		xwriter->writeLineBreak();
		char path[32];
		sprintf(path, "spr/%04d/sprite.xml", data.m_Index);
		xwriter->writeElement(L"Filename");
		xwriter->writeText(core::stringw(path).c_str());
		xwriter->writeClosingTag(L"Filename");
		xwriter->writeLineBreak();


		// keytimes
		xwriter->writeElement(L"KeyTimes");
		xwriter->writeLineBreak();
		u32 keySize = data.m_Keys.size();
		for(u32 j = 0; j < keySize; ++j)
		{
			xwriter->writeElement(L"KeyTime");
			xwriter->writeLineBreak();
			// write key
			KeyTime& key = data.m_Keys[j];
			xwriter->writeElement(L"Time");
			xwriter->writeText(core::stringw(key.m_Time).c_str());
			xwriter->writeClosingTag(L"Time");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"PositionX");
			xwriter->writeText(core::stringw(key.m_Position.X).c_str());
			xwriter->writeClosingTag(L"PositionX");
			xwriter->writeLineBreak();
			xwriter->writeElement(L"PositionY");
			xwriter->writeText(core::stringw(key.m_Position.Y).c_str());
			xwriter->writeClosingTag(L"PositionY");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"Scale");
			xwriter->writeText(core::stringw(key.m_Scale).c_str());
			xwriter->writeClosingTag(L"Scale");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"Rotation");
			xwriter->writeText(core::stringw(key.m_Rotation).c_str());
			xwriter->writeClosingTag(L"Rotation");
			xwriter->writeLineBreak();

			xwriter->writeElement(L"Color");
			char color[32] = "";
			sprintf(color, "#%08x", key.m_Color.color);
			xwriter->writeText(core::stringw(color).c_str());
			xwriter->writeClosingTag(L"Color");
			xwriter->writeLineBreak();

			xwriter->writeClosingTag(L"KeyTime");
			xwriter->writeLineBreak();
		}
		xwriter->writeClosingTag(L"KeyTimes");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"MirrorH");
		xwriter->writeText(core::stringw(data.m_MirrorH).c_str());
		xwriter->writeClosingTag(L"MirrorH");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"MirrorV");
		xwriter->writeText(core::stringw(data.m_MirrorV).c_str());
		xwriter->writeClosingTag(L"MirrorV");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"LoopKeys");
		xwriter->writeText(core::stringw(data.m_Loop).c_str());
		xwriter->writeClosingTag(L"LoopKeys");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"LoopAnim");
		xwriter->writeText(core::stringw(data.m_LoopAnim).c_str());
		xwriter->writeClosingTag(L"LoopAnim");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"SpriteInstance");
		xwriter->writeLineBreak();
	}
	xwriter->writeClosingTag(L"SpriteInstances");
	xwriter->writeLineBreak();


	// write dialog name
	xwriter->writeElement(L"DialogName");
	xwriter->writeText(m_DialogName.c_str());
	xwriter->writeClosingTag(L"DialogName");
	xwriter->writeLineBreak();

	u32 nbLanguages = Engine::LS_MAX;

	xwriter->writeElement(L"LocalizedData");
	xwriter->writeLineBreak();
	for(u32 i = 0; i < nbLanguages; ++i)
	{
		xwriter->writeElement(L"Language", false, L"Lang", core::stringw(languageSettingsString[i]).c_str());
		xwriter->writeLineBreak();
		
		xwriter->writeElement(L"DialogHeader");
		xwriter->writeText(m_DialogHeader[i].c_str());
		xwriter->writeClosingTag(L"DialogHeader");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"DialogText");
		xwriter->writeText(m_DialogText[i].c_str());
		xwriter->writeClosingTag(L"DialogText");
		xwriter->writeLineBreak();

		// choices
		xwriter->writeElement(L"Choices");
		xwriter->writeLineBreak();
		u32 conditionsSize = m_ConditionsText[i].size();
		for(u32 j = 0; j < conditionsSize; ++j)
		{
			xwriter->writeElement(L"Choice");
			xwriter->writeText(m_ConditionsText[i][j].c_str());
			xwriter->writeClosingTag(L"Choice");
			xwriter->writeLineBreak();
		}
		xwriter->writeClosingTag(L"Choices");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"Language");
		xwriter->writeLineBreak();
	}
	xwriter->writeClosingTag(L"LocalizedData");
	xwriter->writeLineBreak();


	xwriter->writeElement(L"UseTextBackGround");
	xwriter->writeText(core::stringw(m_UseTextBackGround).c_str());
	xwriter->writeClosingTag(L"UseTextBackGround");
	xwriter->writeLineBreak();
	

	xwriter->writeElement(L"DialogSpeed");
	xwriter->writeText(core::stringw(m_DialogSpeed).c_str());
	xwriter->writeClosingTag(L"DialogSpeed");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"DialogDuration");
	xwriter->writeText(core::stringw(m_DialogDuration).c_str());
	xwriter->writeClosingTag(L"DialogDuration");
	xwriter->writeLineBreak();

	xwriter->writeElement(L"DialogPositionX");
	xwriter->writeText(core::stringw(m_DialogPosition.X).c_str());
	xwriter->writeClosingTag(L"DialogPositionX");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"DialogPositionY");
	xwriter->writeText(core::stringw(m_DialogPosition.Y).c_str());
	xwriter->writeClosingTag(L"DialogPositionY");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"DialogSizeX");
	xwriter->writeText(core::stringw(m_DialogSize.X).c_str());
	xwriter->writeClosingTag(L"DialogSizeX");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"DialogSizeY");
	xwriter->writeText(core::stringw(m_DialogSize.Y).c_str());
	xwriter->writeClosingTag(L"DialogSizeY");
	xwriter->writeLineBreak();

	// write inputs data
	xwriter->writeElement(L"Inputs");
	xwriter->writeLineBreak();
	u32 inputsSize = m_Inputs.size();
	for(u32 i = 0; i < inputsSize; ++i)
	{
		xwriter->writeElement(L"Input");
		xwriter->writeLineBreak();

		float duration = m_Inputs[i].m_Duration;
		u32 input = m_Inputs[i].m_Inputs;
		u8 nbPress = m_Inputs[i].m_NbPress;

		xwriter->writeElement(L"Duration");
		xwriter->writeText(core::stringw(m_Inputs[i].m_Duration).c_str());
		xwriter->writeClosingTag(L"Duration");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"InputsBitsField");
		char val[32];
		sprintf(val, "%08x", m_Inputs[i].m_Inputs);
		xwriter->writeText(core::stringw(val).c_str());
		xwriter->writeClosingTag(L"InputsBitsField");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"NbPress");
		xwriter->writeText(core::stringw((u32)m_Inputs[i].m_NbPress).c_str());
		xwriter->writeClosingTag(L"NbPress");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"Input");
		xwriter->writeLineBreak();
	}
	xwriter->writeClosingTag(L"Inputs");
	xwriter->writeLineBreak();

	// write bg sound 1 file name
	u32 size = m_SoundBG1.IsValid()?m_SoundBG1->m_Path.size():0;
	if(size != 8 || m_SoundBG1->m_Path[4] != L'.')// path fucked : no way we serialize this shit !
	{
		size = 0;
	}
	if(size > 0)
	{
		xwriter->writeElement(L"SoundBG1");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"SoundDataScene");
		xwriter->writeLineBreak();

		core::stringw str = L"sounds/";
		str += m_SoundBG1->m_Path;
		xwriter->writeElement(L"FileName");
		xwriter->writeText(str.c_str());
		xwriter->writeClosingTag(L"FileName");
		xwriter->writeLineBreak();

		SoundDataScene::StreamData& streamData = m_SoundBG1->m_StreamData;
		xwriter->writeElement(L"StreamData");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"Volume");
		xwriter->writeText(core::stringw(streamData.m_Volume).c_str());
		xwriter->writeClosingTag(L"Volume");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Pitch");
		xwriter->writeText(core::stringw(streamData.m_Pitch).c_str());
		xwriter->writeClosingTag(L"Pitch");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Pan");
		xwriter->writeText(core::stringw(streamData.m_Pan).c_str());
		xwriter->writeClosingTag(L"Pan");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Repeat");
		xwriter->writeText(core::stringw(streamData.m_Repeat).c_str());
		xwriter->writeClosingTag(L"Repeat");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"StopAtDialogsEnd");
		xwriter->writeText(core::stringw(streamData.m_StopAtDialogsEnd).c_str());
		xwriter->writeClosingTag(L"StopAtDialogsEnd");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"StreamData");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"SoundDataScene");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"SoundBG1");
		xwriter->writeLineBreak();
	}

	// write bg sound 2 file name
	size = m_SoundBG2.IsValid()?m_SoundBG2->m_Path.size():0;
	if(size != 8 || m_SoundBG2->m_Path[4] != L'.')// path fucked : no way we serialize this shit !
	{
		size = 0;
	}
	if(size > 0)
	{
		xwriter->writeElement(L"SoundBG2");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"SoundDataScene");
		xwriter->writeLineBreak();

		core::stringw str = L"sounds/";
		str += m_SoundBG2->m_Path;
		xwriter->writeElement(L"FileName");
		xwriter->writeText(str.c_str());
		xwriter->writeClosingTag(L"FileName");
		xwriter->writeLineBreak();

		SoundDataScene::StreamData& streamData = m_SoundBG2->m_StreamData;
		xwriter->writeElement(L"StreamData");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"Volume");
		xwriter->writeText(core::stringw(streamData.m_Volume).c_str());
		xwriter->writeClosingTag(L"Volume");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Pitch");
		xwriter->writeText(core::stringw(streamData.m_Pitch).c_str());
		xwriter->writeClosingTag(L"Pitch");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Pan");
		xwriter->writeText(core::stringw(streamData.m_Pan).c_str());
		xwriter->writeClosingTag(L"Pan");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Repeat");
		xwriter->writeText(core::stringw(streamData.m_Repeat).c_str());
		xwriter->writeClosingTag(L"Repeat");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"StopAtDialogsEnd");
		xwriter->writeText(core::stringw(streamData.m_StopAtDialogsEnd).c_str());
		xwriter->writeClosingTag(L"StopAtDialogsEnd");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"StreamData");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"SoundDataScene");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"SoundBG2");
		xwriter->writeLineBreak();
	}

	// write voice file name
	size = m_Voice.IsValid()?m_Voice->m_Path.size():0;
	if(size != 8 || m_Voice->m_Path[4] != L'.')// path fucked : no way we serialize this shit !
	{
		size = 0;
	}
	if(size > 0)
	{
		xwriter->writeElement(L"Voice");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"SoundDataScene");
		xwriter->writeLineBreak();

		core::stringw str = L"sounds/";
		str += m_Voice->m_Path;
		xwriter->writeElement(L"FileName");
		xwriter->writeText(str.c_str());
		xwriter->writeClosingTag(L"FileName");
		xwriter->writeLineBreak();

		SoundDataScene::StreamData& streamData = m_Voice->m_StreamData;
		xwriter->writeElement(L"StreamData");
		xwriter->writeLineBreak();

		xwriter->writeElement(L"Volume");
		xwriter->writeText(core::stringw(streamData.m_Volume).c_str());
		xwriter->writeClosingTag(L"Volume");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Pitch");
		xwriter->writeText(core::stringw(streamData.m_Pitch).c_str());
		xwriter->writeClosingTag(L"Pitch");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Pan");
		xwriter->writeText(core::stringw(streamData.m_Pan).c_str());
		xwriter->writeClosingTag(L"Pan");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"Repeat");
		xwriter->writeText(core::stringw(streamData.m_Repeat).c_str());
		xwriter->writeClosingTag(L"Repeat");
		xwriter->writeLineBreak();
		xwriter->writeElement(L"StopAtDialogsEnd");
		xwriter->writeText(core::stringw(streamData.m_StopAtDialogsEnd).c_str());
		xwriter->writeClosingTag(L"StopAtDialogsEnd");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"StreamData");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"SoundDataScene");
		xwriter->writeLineBreak();

		xwriter->writeClosingTag(L"Voice");
		xwriter->writeLineBreak();
	}

	// write transitions
	xwriter->writeElement(L"TransitionIn");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"TransitionData");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"TransitionType");
	xwriter->writeText(core::stringw(m_TransitionIn.m_Type).c_str());
	xwriter->writeClosingTag(L"TransitionType");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"Duration");
	xwriter->writeText(core::stringw(m_TransitionIn.m_Duration).c_str());
	xwriter->writeClosingTag(L"Duration");
	xwriter->writeLineBreak();
	xwriter->writeClosingTag(L"TransitionData");
	xwriter->writeLineBreak();
	xwriter->writeClosingTag(L"TransitionIn");
	xwriter->writeLineBreak();

	xwriter->writeElement(L"TransitionOut");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"TransitionData");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"TransitionType");
	xwriter->writeText(core::stringw(m_TransitionOut.m_Type).c_str());
	xwriter->writeClosingTag(L"TransitionType");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"Duration");
	xwriter->writeText(core::stringw(m_TransitionOut.m_Duration).c_str());
	xwriter->writeClosingTag(L"Duration");
	xwriter->writeLineBreak();
	xwriter->writeClosingTag(L"TransitionData");
	xwriter->writeLineBreak();
	xwriter->writeClosingTag(L"TransitionOut");
	xwriter->writeLineBreak();

	// read screen shake data
	xwriter->writeElement(L"ScreenShakeStrength");
	xwriter->writeText(core::stringw(m_ScreenShakeStrength).c_str());
	xwriter->writeClosingTag(L"ScreenShakeStrength");
	xwriter->writeLineBreak();
	xwriter->writeElement(L"ScreenShakeFrequency");
	xwriter->writeText(core::stringw(m_ScreenShakeFrequency).c_str());
	xwriter->writeClosingTag(L"ScreenShakeFrequency");
	xwriter->writeLineBreak();


	xwriter->writeClosingTag(L"DialogNode");
	xwriter->writeLineBreak();

	xwriter->writeClosingTag(L"Dialog");
	xwriter->writeLineBreak();

	//delete xml writer
	xwriter->drop();

	return true;
}
#endif

#ifdef IRON_ICE_EDITOR
MD5 DialogNode::GetMD5()
{
	MD5 md5;
	md5.StartDigest();

	// write background
	u32 nbBkgnd = (m_ImageScene->m_Image == NULL)?0:1;
	md5.AddToDigest((BYTE*)&nbBkgnd, sizeof(u32));
	if(nbBkgnd > 0)
	{
		u32* color = (u32*)m_ImageScene->m_Image->lock();
		const irr::core::dimension2d<u32>& dim = m_ImageScene->m_Image->getSize();
		u32 size = dim.Height*dim.Width;
		md5.AddToDigest((BYTE*)color, size*sizeof(u32));

		// keytimes
		u32 keySize = m_ImageScene->m_Keys.size();
		md5.AddToDigest((BYTE*)&keySize, sizeof(u32));
		for(u32 i = 0; i < keySize; ++i)
		{
			// write key
			KeyTime& key = m_ImageScene->m_Keys[i];
			md5.AddToDigest((BYTE*)&key, sizeof(KeyTime));
		}
		md5.AddToDigest((BYTE*)&m_ImageScene->m_Loop, sizeof(bool));
	}

	md5.AddToDigest((BYTE*)&m_NodeType, sizeof(DN_Type));

	// write sprite indices
	u32 spritesSize = m_SpritesData.size();
	md5.AddToDigest((BYTE*)&spritesSize, sizeof(u32));
	for(u32 i = 0; i < spritesSize; ++i)
	{
		SpriteDataScene& data = m_SpritesData[i];
		md5.AddToDigest((BYTE*)&data.m_Index, sizeof(u32));

		// keytimes
		u32 keySize = data.m_Keys.size();
		md5.AddToDigest((BYTE*)&keySize, sizeof(u32));
		for(u32 j = 0; j < keySize; ++j)
		{
			// write key
			KeyTime& key = data.m_Keys[j];
			md5.AddToDigest((BYTE*)&key, sizeof(KeyTime));
		}

		bool mirrorH = data.m_MirrorH, mirrorV = data.m_MirrorV, loop = data.m_Loop, loopAnim = data.m_LoopAnim;
		md5.AddToDigest((BYTE*)&mirrorH, sizeof(bool));
		md5.AddToDigest((BYTE*)&mirrorV, sizeof(bool));
		md5.AddToDigest((BYTE*)&loop, sizeof(bool));
		md5.AddToDigest((BYTE*)&loopAnim, sizeof(bool));
	}


	// write dialog name
	md5.AddToDigest((BYTE*)m_DialogName.c_str(), m_DialogName.size()*sizeof(wchar_t));

	u32 nbLanguages = Engine::LS_MAX;

	// write header text
	for(u32 i = 0; i < nbLanguages; ++i)
	{
		md5.AddToDigest((BYTE*)m_DialogHeader[i].c_str(), m_DialogHeader[i].size()*sizeof(wchar_t));
	}

	// write dialog text
	for(u32 i = 0; i < nbLanguages; ++i)
	{
		md5.AddToDigest((BYTE*)m_DialogText[i].c_str(), m_DialogText[i].size()*sizeof(wchar_t));
	}

	md5.AddToDigest((BYTE*)&m_UseTextBackGround, sizeof(bool));

	md5.AddToDigest((BYTE*)&m_DialogSpeed, sizeof(float));
	md5.AddToDigest((BYTE*)&m_DialogDuration, sizeof(float));

	md5.AddToDigest((BYTE*)&m_DialogPosition, sizeof(Vector2));
	md5.AddToDigest((BYTE*)&m_DialogSize, sizeof(Vector2));

	// write conditions text
	u32 conditionsSize = m_ConditionsText[0].size();
	md5.AddToDigest((BYTE*)&conditionsSize, sizeof(u32));
	u32 nbIter = nbLanguages;
	for(u32 j = 0; j < nbIter; ++j)
	{
		for(u32 i = 0; i < conditionsSize; ++i)
		{
			md5.AddToDigest((BYTE*)m_ConditionsText[j][i].c_str(), m_ConditionsText[j][i].size()*sizeof(wchar_t));
		}
	}

	// write inputs data
	u32 inputsSize = m_Inputs.size();
	md5.AddToDigest((BYTE*)&inputsSize, sizeof(u32));
	for(u32 i = 0; i < inputsSize; ++i)
	{
		float duration = m_Inputs[i].m_Duration;
		md5.AddToDigest((BYTE*)&duration, sizeof(float));
		u32 input = m_Inputs[i].m_Inputs;
		md5.AddToDigest((BYTE*)&input, sizeof(u32));
		u8 nbPress = m_Inputs[i].m_NbPress;
		md5.AddToDigest((BYTE*)&nbPress, sizeof(u8));
	}

	if(m_SoundBG1.IsValid())
	{
		if(m_SoundBG1->IsAValidPath())
		{
			core::stringc temp = (m_SoundBG1->m_Path.subString((u32)core::max_(0, (s32)m_SoundBG1->m_Path.size()-8), 8));
			md5.AddToDigest((BYTE*)(temp.c_str()), 8*sizeof(char));
		}
		md5.AddToDigest((BYTE*)&m_SoundBG1->m_StreamData, sizeof(SoundDataScene::StreamData));
	}
	if(m_SoundBG2.IsValid())
	{
		if(m_SoundBG2->IsAValidPath())
		{
			core::stringc temp = (m_SoundBG2->m_Path.subString((u32)core::max_(0, (s32)m_SoundBG2->m_Path.size()-8), 8));
			md5.AddToDigest((BYTE*)(temp.c_str()), 8*sizeof(char));
		}
		md5.AddToDigest((BYTE*)&m_SoundBG2->m_StreamData, sizeof(SoundDataScene::StreamData));
	}
	if(m_Voice.IsValid())
	{
		if(m_Voice->IsAValidPath())
		{
			core::stringc temp = (m_Voice->m_Path.subString((u32)core::max_(0, (s32)m_Voice->m_Path.size()-8), 8));
			md5.AddToDigest((BYTE*)(temp.c_str()), 8*sizeof(char));
		}
		md5.AddToDigest((BYTE*)&m_Voice->m_StreamData, sizeof(SoundDataScene::StreamData));
	}

	// write transitions
	md5.AddToDigest((BYTE*)&m_TransitionIn, sizeof(TransitionData));
	md5.AddToDigest((BYTE*)&m_TransitionOut, sizeof(TransitionData));

	md5.AddToDigest((BYTE*)&m_ScreenShakeStrength, sizeof(float));
	md5.AddToDigest((BYTE*)&m_ScreenShakeFrequency, sizeof(float));

	md5.EndDigest();

	return md5;
}
#endif

float DialogNode::GetDialogTotalDuration(bool skipOutro /*= false*/, bool evaluateInputTime/* = false*/) const
{
	u32 languageIndex = (u32)Engine::GetInstance()->GetLanguage();
	u32 textSize = m_DialogText[languageIndex].size();
	for(u32 i = 0; i < m_ConditionsText[languageIndex].size(); ++i)
	{
		textSize += 1; //L"\n";
		textSize += m_ConditionsText[languageIndex][i].size();
	}

	// base time
	float duration = (m_DialogDuration == 0.0f)? ((m_DialogSpeed == 0.0f)? 0:(float)textSize/m_DialogSpeed) : m_DialogDuration;

	if(m_DialogDuration == 0.0f)// for dialogs driven by char writing speed, add an additionnal 0.5s
		duration += 0.5f;

	if(m_NodeType == DN_AUTO_VALID)
		duration += 0.5f;

	// + outro
	if(!skipOutro && m_TransitionOut.m_Type != DN_TRANSITION_NONE)
		duration += m_TransitionOut.m_Duration;

	if(evaluateInputTime)
	{
		if(m_NodeType == DN_MANUAL_VALID || m_NodeType == DN_MULTI_VALID)
		{
			// + extra time
			duration += DIALOG_EXTRA_DURATION;
		}
		else if(m_NodeType == DN_QTE_VALID)
		{
			for(u32 i = 0; i < m_Inputs.size(); ++i)
				duration += m_Inputs[i].m_Duration;
		}
	}

	return duration;
}


// Scene
Scene::Scene():
m_CurrentDialog(NULL),
m_InitialDialog(NULL),
m_NbSpritesBank(0),
m_NbDialogs(0),
m_CurrentTime(0.0f),
m_CurrentTimeInternal(0.0f),
m_Accumulator(0.0f),
m_CurrentChoice(0),
m_CurrentDlgValidated(false),
m_HasEnded(false),
m_Loop1stDialog(false),
m_NbSoundsBank(0)
{

}

Scene::~Scene()
{
	m_CurrentDialog = NULL;
	m_InitialDialog = NULL;

	for(u32 i=0; i<m_Dialogs.size(); ++i)
	{
		SafeDelete(m_Dialogs[i]);// delete content
	}
	m_Dialogs.clear();// clear array

	m_SpritesAlive.clear();
	m_Dialogs.clear();// clear array

	m_SoundsBank.clear();
	m_SoundsAlive.clear();
	m_SoundsAliveData.clear();

	//Engine::GetInstance()->GetAudioDevice()->CloseAllSounds();

	m_HasEnded = true;

	//Engine::GetInstance()->GetDriver()->removeAllTextures();
}

void Scene::ClearAllSprites()
{
	m_SpritesAlive.clear();
	m_SpritesBank.clear();

	// delete all ref to sprites (removed from bank previously)
	for(u32 i = 0; i < m_Dialogs.size(); ++i)
	{
		DialogNode* dlg = m_Dialogs[i];
		if(dlg)
			dlg->GetSprites().clear();// remove all obsolete pointers
	}
}

bool Scene::LoadSceneFromFile(const char* path)
{
	FILE* input = fopen(path, "rb");
	if(!input)
	{
		return false;
	}

	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	{
		io::path newPath(path);
		s32 endIdx = newPath.findLast('/');
		if(endIdx < 0)
			endIdx = newPath.findLast('\\');
		if(endIdx >= 0)
			newPath = newPath.subString(0, endIdx+1);
		else
			assert(false);
// 		if(!fs->changeWorkingDirectoryTo(newPath))
// 			assert(false);
		m_ScenePath = newPath;
	}
	//m_ScenePath = fs->getWorkingDirectory();

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "SCNN", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// meta data ?
	// name
	// desc
	// icon ?


	// read nb sprites bank
	m_NbSpritesBank = 0;
	fread(&m_NbSpritesBank, sizeof(u32), 1, input);
	// create the sprite bank (not loaded)
	for(u32 i = 0; i < m_NbSpritesBank; ++i)
		AddSpriteToBank(SmartPointer<AnimatedSprite>(NULL));

	// read nb dialogs
	m_NbDialogs = 0;
	fread(&m_NbDialogs, sizeof(u32), 1, input);
	// create the dialogs (not loaded)
	for(u32 i = 0; i < m_NbDialogs; ++i)
		m_Dialogs.push_back(new DialogNode(i));
	// link them in between
	for(u32 i = 0; i < m_NbDialogs; ++i)
	{
		u32 nbChildren = 0;
		fread(&nbChildren, sizeof(u32), 1, input);
		for(u32 j = 0; j < nbChildren; ++j)
		{
			u32 childIndex = 0;
			fread(&childIndex, sizeof(u32), 1, input);
			assert(childIndex < m_NbDialogs);
			m_Dialogs[i]->BranchToNode(m_Dialogs[childIndex]);
		}
	}

	// read nb sounds bank
	m_NbSoundsBank = 0;
	fread(&m_NbSoundsBank, sizeof(u32), 1, input);

	// read initial dialog index
	u32 initialDlgIndex = 0;
	fread(&initialDlgIndex, sizeof(u32), 1, input);
	assert(initialDlgIndex < m_NbDialogs);
	m_InitialDialog = m_Dialogs[initialDlgIndex];


	fclose(input);

	m_CurrentDialog = m_InitialDialog;
	m_CurrentChoice = 0;
	m_CurrentDlgValidated = false;
	m_HasEnded = false;

	CheckSceneIntegrity();

	return true;
}

#ifndef IRON_ICE_FINAL
bool Scene::Deprecated_SaveSceneToFile(const char* path)
{
	u32 nbSpritesBank = m_SpritesBank.size();
	u32 nbDialogs = m_Dialogs.size();
#ifndef IRON_ICE_EDITOR
	// sanity check : can't save dlg and sprites if not loaded. Editor only can do it with the cache folder
	for(u32 i = 0; i < nbDialogs; ++i)
	{
		if(!m_Dialogs[i]->GetIsLoaded())
		{
			return false;
		}
	}
	for(u32 i = 0; i < nbSpritesBank; ++i)
	{
		if(!m_SpritesBank[i].IsValid())
		{
			return false;
		}
	}
#endif

	// backup the old .scn
	FILE* input = fopen(path, "rb");
	if(input)// file already existing : backup it
	{
		io::path srcPath = path;
		io::path dstPath = path;
		dstPath += L".bak";
		CopyFile(srcPath.make_lower().c_str(), dstPath.make_lower().c_str());
		fclose(input);
	}

	FILE* output = fopen(path, "wb");
	if(!output)
	{
		return false;
	}

	CheckSceneIntegrity();

 	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
// 	m_ScenePath = fs->getWorkingDirectory();
	io::path dir = path;
	s32 index = dir.findLastChar("/");
	assert(index != -1);
	dir = dir.subString(0, index);
	bool ret = fs->changeWorkingDirectoryTo(dir);
	assert(ret);
	m_ScenePath = fs->getWorkingDirectory();

	char header[5] = "SCNN";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// meta data ?
	// name
	// desc
	// icon ?


	// write nb sprites bank
	fwrite(&nbSpritesBank, sizeof(u32), 1, output);

	// write nb dialogs
	fwrite(&nbDialogs, sizeof(u32), 1, output);
	// write their children's index
	for(u32 i = 0; i < nbDialogs; ++i)
	{
		u32 nbChildren = m_Dialogs[i]->GetNbChildren();
		fwrite(&nbChildren, sizeof(u32), 1, output);
		for(u32 j = 0; j < nbChildren; ++j)
		{
			s32 childIndex = m_Dialogs.linear_search(m_Dialogs[i]->GetChildAtIndex(j));
			assert(childIndex >= 0 && (u32)childIndex < nbDialogs);
			fwrite(&childIndex, sizeof(u32), 1, output);
		}
	}

	char fileName[1024];

	// save the sprites
	sprintf(fileName, "%s/spr", m_ScenePath.c_str());
	mkdir(fileName);
	for(u32 i = 0; i < nbSpritesBank; ++i)
	{
		bool isLoaded = m_SpritesBank[i].IsValid();
		if(!isLoaded)
			LoadSpriteFromIndex(i);
		sprintf(fileName, "%s/spr/%04d.spr", m_ScenePath.c_str(), i);
		m_SpritesBank[i]->SaveSpriteToFile(fileName);
		if(!isLoaded)
			m_SpritesBank[i] = NULL;
	}

	// write nb sounds bank
	u32 nbSoundsBank = m_SoundsBank.size();
	fwrite(&nbSoundsBank, sizeof(u32), 1, output);

	// save the sounds and substitute their original path to the new ones in the dialogs
	sprintf(fileName, "%s/sounds", m_ScenePath.c_str());
	mkdir(fileName);
	for(u32 i = 0; i < nbSoundsBank; ++i)
	{
		io::path sound = m_SoundsBank[i];
		if(sound != L"")
		{
			s32 index = sound.findLastChar(".");
			assert(index != -1);
			if(index < 0)
				continue;
			io::path extPath = sound.subString(index, sound.size()-index);
			const char* ext = (char*)(extPath.c_str());
			sprintf(fileName, "%s/sounds/%04d%s", m_ScenePath.c_str(), i, ext);
			io::path srcPath = sound;
			io::path dstPath = fileName;
			CopyFile(srcPath.make_lower().c_str(), dstPath.make_lower().c_str());

			sprintf(fileName, "%04d%s", i, ext);
			m_SoundsBank[i] = fileName;// prepare the filename for serialization under (relative path)
		}
	}

	// save the dialogs
	u32 initialDlgIndex = 0;
	sprintf(fileName, "%s/dlg", m_ScenePath.c_str());
	mkdir(fileName);
	for(u32 i = 0; i < nbDialogs; ++i)
	{
		sprintf(fileName, "%s/dlg/%04d.dlg", m_ScenePath.c_str(), i);
		m_Dialogs[i]->SaveToFile(fileName);

		if(m_InitialDialog == m_Dialogs[i])
			initialDlgIndex = i;
	}
	// write initial dialog index
	fwrite(&initialDlgIndex, sizeof(u32), 1, output);

	fclose(output);

	// And at the end restore the full path name for seamless save
	for(u32 i = 0; i < nbSoundsBank; ++i)
	{
		sprintf(fileName, "%s/sounds/%s", m_ScenePath.c_str(), m_SoundsBank[i].c_str());
		m_SoundsBank[i] = fileName;
	}

	return true;
}

bool Scene::SaveSceneToFile(const char* path)
{
	u32 nbSpritesBank = m_SpritesBank.size();
	u32 nbDialogs = m_Dialogs.size();

	// backup the old .scn
	FILE* input = fopen(path, "rb");
	if(input)// file already existing : backup it
	{
		io::path srcPath = path;
		io::path dstPath = path;
		dstPath += L".bak";
		CopyFile(srcPath.make_lower().c_str(), dstPath.make_lower().c_str());
		fclose(input);
	}

	FILE* output = fopen(path, "wb");
	if(!output)
	{
		return false;
	}

	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	// 	m_ScenePath = fs->getWorkingDirectory();
	io::path dir = path;
	s32 index = dir.findLastChar("/");
	assert(index != -1);
	dir = dir.subString(0, index);
	bool ret = fs->changeWorkingDirectoryTo(dir);
	assert(ret);
	m_ScenePath = fs->getWorkingDirectory();

	char header[5] = "SCNN";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// meta data ?
	// name
	// desc
	// icon ?


	// write nb sprites bank
	fwrite(&nbSpritesBank, sizeof(u32), 1, output);

	// write nb dialogs
	fwrite(&nbDialogs, sizeof(u32), 1, output);
	// write their children's index
	for(u32 i = 0; i < nbDialogs; ++i)
	{
		u32 nbChildren = m_Dialogs[i]->GetNbChildren();
		fwrite(&nbChildren, sizeof(u32), 1, output);
		for(u32 j = 0; j < nbChildren; ++j)
		{
			s32 childIndex = m_Dialogs.linear_search(m_Dialogs[i]->GetChildAtIndex(j));
			assert(childIndex >= 0 && (u32)childIndex < nbDialogs);
			fwrite(&childIndex, sizeof(u32), 1, output);
		}
	}

	char fileName[1024];

	// save the sprites
	sprintf(fileName, "%s/spr", m_ScenePath.c_str());
	mkdir(fileName);
	// remove everything
	{
		u32 i = 0;
		do
		{
			sprintf(fileName, "%s/spr/%04d.spr", m_ScenePath.c_str(), i);
			++i;
		} while(remove(fileName) == 0);
	}
	// 1st pass : save the sprites loaded (= potentially modified) in the cache
// 	for(u32 i = 0; i < nbSpritesBank; ++i)
// 	{
// 		bool isLoaded = m_SpritesBank[i].IsValid();
// 		if(!isLoaded)
// 			continue;
// 		char path[32];
// 		sprintf(path, "temp/spr/%04d.spr", i);
// 		if(m_SpritesBank[i]->SaveSpriteToFile((char*)Engine::GetInstance()->GetGamePath(path).c_str()))
// 			printf("Scene saving : save sprite file %s\n", path);
// 	}
	// 2nd pass : copy past the sprites from the cache
	for(u32 i = 0; i < nbSpritesBank; ++i)
	{
		bool isLoaded = m_SpritesBank[i].IsValid();
		sprintf(fileName, "%s/spr/%04d.spr", m_ScenePath.c_str(), i);
		char srcPath[32];
		sprintf(srcPath, "temp/spr/%04d.spr", i);
		if(CopyFile(Engine::GetInstance()->GetGamePath(srcPath).c_str(), fileName))// copy past it
			printf("Scene saving : copy sprite file %s to %s\n", srcPath, fileName);
		else 
			printf("Scene saving : failed to copy sprite file %s to %s\n", srcPath, fileName);
	}

	// save the dialogs
	u32 initialDlgIndex = 0;
	sprintf(fileName, "%s/dlg", m_ScenePath.c_str());
	mkdir(fileName);
	// remove everything
	{
		u32 i = 0;
		do
		{
			sprintf(fileName, "%s/dlg/%04d.dlg", m_ScenePath.c_str(), i);
			++i;
		} while(remove(fileName) == 0);
	}
	// 1st pass : save the dialogs loaded (= potentially modified) in the cache
// 	for(u32 i = 0; i < nbDialogs; ++i)
// 	{
// 		bool isLoaded = m_Dialogs[i]->GetIsLoaded();
// 		if(!isLoaded)
// 			continue;
// 		irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
// 		io::path path = fs->getWorkingDirectory();
// 		if(SaveDialogFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), i))
// 			printf("Scene saving : save dialog file %d\n", i);
// 		fs->changeWorkingDirectoryTo(path);
// 	}
	// 2nd pass : copy past the dialogs from the cache
	for(u32 i = 0; i < nbDialogs; ++i)
	{
		bool isLoaded = m_Dialogs[i]->GetIsLoaded();
		sprintf(fileName, "%s/dlg/%04d.dlg", m_ScenePath.c_str(), i);
		char srcPath[32];
		sprintf(srcPath, "temp/dlg/%04d.dlg", i);
		if(CopyFile(Engine::GetInstance()->GetGamePath(srcPath).c_str(), fileName))// copy past it
			printf("Scene saving : copy dialog file %s to %s\n", srcPath, fileName);
		else
			printf("Scene saving : failed to copy dialog file %s to %s\n", srcPath, fileName);

		if(m_InitialDialog == m_Dialogs[i])
			initialDlgIndex = i;
	}

	// write nb sounds bank
	u32 nbSoundsBank = m_SoundsBank.size();
	fwrite(&nbSoundsBank, sizeof(u32), 1, output);

	// save the sounds and substitute their original path with the new ones exported
	sprintf(fileName, "%s/sounds", m_ScenePath.c_str());
	mkdir(fileName);
	for(u32 i = 0; i < nbSoundsBank; ++i)
	{
		io::path sound = m_SoundsBank[i];
		if(sound != L"")
		{
			s32 index = sound.findLastChar(".");
			//assert(index != -1);
			if(index < 0)
			{
				printf("Scene saving : failed bad sound file name : %s\n", sound.c_str());
				continue;
			}
			io::path extPath = sound.subString(index, sound.size()-index);
			const char* ext = (char*)(extPath.c_str());
			sprintf(fileName, "%s/sounds/%04d%s", m_ScenePath.c_str(), i, ext);
			io::path srcPath = sound;
			io::path dstPath = fileName;
			if(CopyFile(srcPath.make_lower().c_str(), dstPath.make_lower().c_str()))
				printf("Scene saving : copy sound file %s to %s\n", srcPath.c_str(), fileName);
			else
				printf("Scene saving : failed to copy sound file %s to %s\n", srcPath.c_str(), fileName);
		}
	}

	// write initial dialog index
	fwrite(&initialDlgIndex, sizeof(u32), 1, output);

	fclose(output);

	return true;
}
#endif

bool Scene::InitDialog(DialogNode* dlg)
{
	if(dlg->GetIsLoaded())
	{
		if(dlg->GetImageScene()->m_Image)
			SetCurrentBackground(dlg->GetImageScene());

		// flush sprites alive ?
		m_SpritesAlive.clear();

		u32 index = 0;
		assert(dlg->GetSpritesData().size() == dlg->GetSprites().size());
		for(u32 i = 0; i < dlg->GetSprites().size(); ++i)
		{
			SmartPointer<AnimatedSprite> spr = dlg->GetSprites()[i];
			SmartPointer<SpriteEntity> sprEnt(new SpriteEntity(spr, dlg->GetSpritesData()[i]));
			m_SpritesAlive.push_back(sprEnt);
		}

		// always keep the sounds in the sound alive (empty or not)
		if(m_SoundsAlive.empty())// no bgms ? create an empty one
		{
			m_SoundsAliveData.clear();

			SmartPointer<AudioStream> bgm(NULL);
			for(u32 i = 0; i < 3; ++i)
			{
				m_SoundsAlive.push_back(bgm);
				m_SoundsAliveData.push_back(AudioDataSerializable());
			}
		}
		else// flush everything except our 3 1st (normally we should have only 3)
		{
			assert(m_SoundsAlive.size() == 3);
			SmartPointer<AudioStream> bgm1 = m_SoundsAlive[0];
			SmartPointer<AudioStream> bgm2 = m_SoundsAlive[1];
			SmartPointer<AudioStream> bgm3 = m_SoundsAlive[2];
			m_SoundsAlive.clear();
			m_SoundsAlive.push_back(bgm1);
			m_SoundsAlive.push_back(bgm2);
			m_SoundsAlive.push_back(bgm3);
		}
		if(dlg->GetSoundBG1().IsValid() && dlg->GetSoundBG1()->IsAValidPath())// Any bg 1? Replace it.
		{
			if(!dlg->GetSoundBG1()->m_Sound.IsValid())// load it if not already loaded
				dlg->GetSoundBG1()->m_Sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile((char*)dlg->GetSoundBG1()->m_Path.c_str());

			m_SoundsAlive[0] = dlg->GetSoundBG1()->m_Sound;// 0 reserved to bgm1 (always here empty or not)
			float vol = dlg->GetSoundBG1()->m_StreamData.m_Volume;
			float pitch = dlg->GetSoundBG1()->m_StreamData.m_Pitch;
			float pan = dlg->GetSoundBG1()->m_StreamData.m_Pan;
			bool repeat = dlg->GetSoundBG1()->m_StreamData.m_Repeat;
			bool stop = dlg->GetSoundBG1()->m_StreamData.m_StopAtDialogsEnd;
			Engine::GetInstance()->GetAudioDevice()->SetupSound(m_SoundsAlive[0], pan, pitch, vol, repeat, stop);
			if(m_SoundsAlive[0]->IsValid())
				m_SoundsAlive[0]->Reset();
			if(m_SoundsAlive[0]->IsPlaying())
			{
				m_SoundsAlive[0]->Stop();
				m_SoundsAlive[0]->Play();
			}
			m_SoundsAliveData[0] = AudioDataSerializable(dlg->GetSoundBG1()->m_Path.subString((u32)core::max_(0, (s32)dlg->GetSoundBG1()->m_Path.size()-8), 8), 
				vol, pitch, pan, repeat, stop);
		}
		else if(m_SoundsAlive[0].IsValid() && m_SoundsAlive[0]->GetStopAtEnd())
		{
			SmartPointer<AudioStream> bgm(NULL);
			m_SoundsAlive[0] = bgm;
			m_SoundsAliveData[0] = AudioDataSerializable();
		}
		// else keep the last one
		if(dlg->GetSoundBG2().IsValid() && dlg->GetSoundBG2()->IsAValidPath())// Any bg 2? Replace it.
		{
			if(!dlg->GetSoundBG2()->m_Sound.IsValid())// load it if not already loaded
				dlg->GetSoundBG2()->m_Sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile((char*)dlg->GetSoundBG2()->m_Path.c_str());
			m_SoundsAlive[1] = dlg->GetSoundBG2()->m_Sound;// 1 reserved to bgm2 (always here empty or not)
			float vol = dlg->GetSoundBG2()->m_StreamData.m_Volume;
			float pitch = dlg->GetSoundBG2()->m_StreamData.m_Pitch;
			float pan = dlg->GetSoundBG2()->m_StreamData.m_Pan;
			bool repeat = dlg->GetSoundBG2()->m_StreamData.m_Repeat;
			bool stop = dlg->GetSoundBG2()->m_StreamData.m_StopAtDialogsEnd;
			Engine::GetInstance()->GetAudioDevice()->SetupSound(m_SoundsAlive[1], pan, pitch, vol, repeat, stop);
			if(m_SoundsAlive[1]->IsValid())
				m_SoundsAlive[1]->Reset();
			if(m_SoundsAlive[1]->IsPlaying())
			{
				m_SoundsAlive[1]->Stop();
				m_SoundsAlive[1]->Play();
			}

			m_SoundsAliveData[1] = AudioDataSerializable(dlg->GetSoundBG2()->m_Path.subString((u32)core::max_(0, (s32)dlg->GetSoundBG2()->m_Path.size()-8), 8), 
				vol, pitch, pan, repeat, stop);
		}
		else if(m_SoundsAlive[1].IsValid() && m_SoundsAlive[1]->GetStopAtEnd())
		{
			SmartPointer<AudioStream> bgm(NULL);
			m_SoundsAlive[1] = bgm;
			m_SoundsAliveData[1] = AudioDataSerializable();
		}
		// else keep the last one
		if(dlg->GetVoice().IsValid() && dlg->GetVoice()->IsAValidPath())// Any voice? Add it.
		{
			if(!dlg->GetVoice()->m_Sound.IsValid())// load it if not already loaded
				dlg->GetVoice()->m_Sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile((char*)dlg->GetVoice()->m_Path.c_str());

			m_SoundsAlive[2] = dlg->GetVoice()->m_Sound;// 2 reserved to voice (always here empty or not)
			float vol = dlg->GetVoice()->m_StreamData.m_Volume;
			float pitch = dlg->GetVoice()->m_StreamData.m_Pitch;
			float pan = dlg->GetVoice()->m_StreamData.m_Pan;
			bool repeat = dlg->GetVoice()->m_StreamData.m_Repeat;
			bool stop = dlg->GetVoice()->m_StreamData.m_StopAtDialogsEnd;
			Engine::GetInstance()->GetAudioDevice()->SetupSound(m_SoundsAlive[2], pan, pitch, vol, repeat, stop);
			if(m_SoundsAlive[2]->IsValid())
				m_SoundsAlive[2]->Reset();
			if(m_SoundsAlive[2]->IsPlaying())
			{
				m_SoundsAlive[2]->Stop();
				m_SoundsAlive[2]->Play();
			}
			m_SoundsAliveData[2] = AudioDataSerializable(dlg->GetVoice()->m_Path.subString((u32)core::max_(0, (s32)dlg->GetVoice()->m_Path.size()-8), 8), 
				vol, pitch, pan, repeat, stop);
		}
		else if(m_SoundsAlive[2].IsValid() && m_SoundsAlive[2]->GetStopAtEnd())
		{
			SmartPointer<AudioStream> bgm(NULL);
			m_SoundsAlive[2] = bgm;
			m_SoundsAliveData[2] = AudioDataSerializable();
		}
		// else do nothing...

		return true;
	}
	return false;
}

bool Scene::LoadDialog(DialogNode* dlg)
{
	if(dlg && LoadDialogFromIndex(dlg->GetID()))
	{
		if(dlg->GetIsLoaded())
		{
			InitDialog(dlg);

			return true;
		}
	}
	return false;
}

#ifndef IRON_ICE_FINAL
bool Scene::SaveDialogFromIndex(const char* path, u32 index)
{
	u32 nbSpritesBank = m_SpritesBank.size();
	u32 nbDialogs = m_Dialogs.size();

	DialogNode* dlg = GetDialogAtIndex(index);
	if(!dlg || !dlg->GetIsLoaded())
		return false;

	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	io::path dir = path;
	s32 idx = dir.findLastChar("/");
	assert(idx != -1);
	dir = dir.subString(0, idx);
	mkdir(dir.c_str());
	bool ret = fs->changeWorkingDirectoryTo(dir);
	assert(ret);
	dir = fs->getWorkingDirectory();

	char fileName[1024];

	u32 nbSoundsBank = m_SoundsBank.size();

	// save the sounds and substitute their original path to the new ones in the dialogs
	sprintf(fileName, "%s/sounds", dir.c_str());
	mkdir(fileName);
	SmartPointer<SoundDataScene> sounds[3] = {dlg->GetSoundBG1(), dlg->GetSoundBG2(), dlg->GetVoice()};
	for(u32 i = 0; i < 3; ++i)
	{
		if(sounds[i].IsValid() && sounds[i]->IsAValidPath())
		{
			u32 bankId = AddSoundToBank(sounds[i]->m_Path, true);
			io::path uniquePath = m_SoundsBank[bankId];
			s32 j = uniquePath.findLastChar(".");
			//assert(j != -1);
			if(j != -1)
			{
				io::path extPath = uniquePath.subString(j, uniquePath.size()-j);
				const char* ext = (char*)(extPath.c_str());

				if(bankId == m_SoundsBank.size()-1)// new sound added : export it !
				{
					sprintf(fileName, "%s/sounds/%04d%s", dir.c_str(), bankId, ext);
					io::path srcPath = uniquePath;
					io::path dstPath = fileName;
					if(CopyFile(srcPath.make_lower().c_str(), dstPath.make_lower().c_str()))
						printf("Dialog saving : copy sound file %s to %s\n", srcPath.c_str(), fileName);
					else
						printf("Dialog saving : failed to copy sound file %s to %s\n", srcPath.c_str(), fileName);

					m_SoundsBank[bankId] = dstPath.c_str();// exported path
				}
				sprintf(fileName, "%04d%s", bankId, ext);
				sounds[i]->m_Path = fileName;// prepare the filename for serialization under (relative path)
			}
			else// no way we save a fucked sound !
			{
				sounds[i]->m_Path = "";
			}
		}
	}

	// save the dialogs
	sprintf(fileName, "%s/dlg", dir.c_str());
	mkdir(fileName);
	{
		sprintf(fileName, "%s/dlg/%04d.dlg", dir.c_str(), index);
		dlg->SaveToFile(fileName);
	}

	// And at the end restore the full path name for seamless save
	for(u32 i = 0; i < 3; ++i)
	{
		if(sounds[i].IsValid() && sounds[i]->IsAValidPath())
		{
			sprintf(fileName, "%s/sounds/%s", dir.c_str(), sounds[i]->m_Path.c_str());
			sounds[i]->m_Path = fileName;
		}
	}

	return true;
}

bool Scene::SaveSpriteFromIndex(const char* path, u32 index)
{
	SmartPointer<AnimatedSprite> spr = GetSpriteAtIndex(index);
	if(!spr.IsValid())
		return false;

	irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	io::path current = fs->getWorkingDirectory();
	io::path dir = path;
	s32 idx = dir.findLastChar("/");
	assert(idx != -1);
	dir = dir.subString(0, idx);
	mkdir(dir.c_str());
	bool ret = fs->changeWorkingDirectoryTo(dir);
	assert(ret);
	dir = fs->getWorkingDirectory();

	char fileName[1024];

	// save the sprites
	sprintf(fileName, "%s/spr", dir.c_str());
	mkdir(fileName);
	{
		sprintf(fileName, "%s/spr/%04d.spr", dir.c_str(), index);
		spr->SaveSpriteToFile(fileName);
	}

	fs->changeWorkingDirectoryTo(current);

	return true;
}
#endif

bool Scene::LoadDialogFromIndex(u32 index, bool useTextureSizeLimit/* = false*/)
{
	if(index < m_Dialogs.size())
	{
		char fileName[1024];
		sprintf(fileName, "%sdlg/%04d.dlg", m_ScenePath.c_str(), index);
		if(m_Dialogs[index]->LoadFromFile(fileName, useTextureSizeLimit))
		{
			if(m_Dialogs[index]->GetIsLoaded() && m_Dialogs[index]->GetSprites().empty())// dialog loaded but with no sprites loaded
			{
				TArray<SpriteDataScene>& data = m_Dialogs[index]->GetSpritesData();// load all sprite needed from the data
				for(u32 i = 0; i < data.size(); ++i)
				{
					u32 indexS = data[i].m_Index;// sprite index in the bank
					LoadSpriteFromIndex(indexS);// load in the bank if not loaded
					m_Dialogs[index]->_AddSprite(m_SpritesBank[indexS]);// add the sprite to the dialog
				}
			}
			// load correctly the sounds
			if(m_Dialogs[index]->GetSoundBG1().IsValid() && m_Dialogs[index]->GetSoundBG1()->IsAValidPath())
			{
				if(m_Dialogs[index]->GetSoundBG1()->m_Path.size() > 8)// something bad happened...
					m_Dialogs[index]->SetSoundBG1(L"");
				else
				{
					sprintf(fileName, "%ssounds/%s", m_ScenePath.c_str(), m_Dialogs[index]->GetSoundBG1()->m_Path.c_str());
					m_Dialogs[index]->GetSoundBG1()->m_Path = fileName;
				}
			}
			if(m_Dialogs[index]->GetSoundBG2().IsValid() && m_Dialogs[index]->GetSoundBG2()->IsAValidPath())
			{
				if(m_Dialogs[index]->GetSoundBG2()->m_Path.size() > 8)// something bad happened...
					m_Dialogs[index]->SetSoundBG2(L"");
				else
				{
					sprintf(fileName, "%ssounds/%s", m_ScenePath.c_str(), m_Dialogs[index]->GetSoundBG2()->m_Path.c_str());
					m_Dialogs[index]->GetSoundBG2()->m_Path = fileName;
				}
			}
			if(m_Dialogs[index]->GetVoice().IsValid() && m_Dialogs[index]->GetVoice()->IsAValidPath())
			{
				if(m_Dialogs[index]->GetVoice()->m_Path.size() > 8)// something bad happened...
					m_Dialogs[index]->SetVoice(L"");
				else
				{
					sprintf(fileName, "%ssounds/%s", m_ScenePath.c_str(), m_Dialogs[index]->GetVoice()->m_Path.c_str());
					m_Dialogs[index]->GetVoice()->m_Path = fileName;
				}
			}
			return true;
		}
		else// not loadable... something bad happened
		{
			// what to do... the file is corrupted but probably the link between this one and others are still correct... so keep it as loaded
			// m_Dialogs[index]->SetIsLoaded(true);
		}
	}
	return false;
}

bool Scene::LoadSpriteFromIndex(u32 index)
{
	if(index < m_SpritesBank.size() && !m_SpritesBank[index].IsValid())
	{
		m_SpritesBank[index] = SmartPointer<AnimatedSprite>(new AnimatedSprite());
		char fileName[256];
		sprintf(fileName, "%s/spr/%04d.spr", m_ScenePath.c_str(), index);
		bool ret = m_SpritesBank[index]->LoadSpriteFromFile(fileName);

		return ret;
	}
	return false;
}

u32 Scene::AddSoundToBank(const io::path& path, bool checkMD5/* = false*/)
{
#ifdef IRON_ICE_EDITOR
	MD5 md5Ref;
	if(checkMD5)
	{
		checkMD5 = md5Ref.DigestFile((char*)path.c_str());
		if(!checkMD5)
			printf( "Can't check %s's unicity.\n", path.c_str() ) ;
	}
#endif
	s32 index = -1;
	for (u32 i=0; i<m_SoundsBank.size(); ++i)
	{
		if (path == m_SoundsBank[i])// look for same content (same path)
		{
			index = (s32)i;
			break;
		}
#ifdef IRON_ICE_EDITOR
		else if(checkMD5)
		{
			MD5 md5;
			md5.DigestFile((char*)m_SoundsBank[i].c_str());
			if(md5._raw == md5Ref._raw)
			{
				index = (s32)i;
				break;
			}
		}
#endif
	}
	if(index >= 0)
		return index;
	m_SoundsBank.push_back(path);
	return m_SoundsBank.size()-1;
}

bool Scene::RemoveSound(const io::path& path)
{
	s32 index = m_SoundsBank.linear_search(path);
	if(index < 0)
		return false;

	m_SoundsBank.erase(index);
	return true;
}

bool Scene::AddSpriteToDialog(DialogNode* dlg, s32 bankIndex)
{
	if(!dlg || bankIndex < 0 || (u32)bankIndex >= m_SpritesBank.size())
		return false;

	SmartPointer<AnimatedSprite> spr = m_SpritesBank[bankIndex];
	dlg->_AddSprite(spr);
	SpriteDataScene data(bankIndex);
	data.m_Keys.push_back(KeyTime(Position2(Engine::GetInstance()->m_OriginalSize.Width>>1, Engine::GetInstance()->m_OriginalSize.Height>>1)));
	dlg->_AddSpriteData(data);

	return true;
}

bool Scene::CheckSceneSpritesIntegrity(bool removeUnusedData/* = false*/)
{
	bool errorFound = false;
	for(u32 i = 0; i < m_Dialogs.size(); ++i)
	{
		DialogNode* dlg = m_Dialogs[i];
		if(dlg && dlg->GetIsLoaded())
		{
			//assert(dlg->GetSprites().size() == dlg->GetSpritesData().size());// we don't manage cases where sprites have no data or opposite case
			for(u32 j = 0; j < dlg->GetSprites().size();)
			{
				SmartPointer<AnimatedSprite> spr = dlg->GetSprites()[j];
				s32 bankIndex = m_SpritesBank.linear_search(spr);
				if(bankIndex == -1)// sprite not existing in bank : remove it
				{
					dlg->GetSprites().erase(j);// will delete the sprite itself if not used anymore
					dlg->GetSpritesData().erase(j);// the corresponding data must be erased
					errorFound = true;
					continue;
				}
				SpriteDataScene& data = dlg->GetSpritesData()[j];
				if(bankIndex != data.m_Index)// index is not correct : replace it
				{
					data.m_Index = bankIndex;
					errorFound = true;
				}
				++j;
			}

			if(removeUnusedData)// remove unused spritedata
			{
				assert(dlg->GetSprites().size() <= dlg->GetSpritesData().size());// we shouldn't have more sprites than spritedata
				while(dlg->GetSprites().size() < dlg->GetSpritesData().size())
				{
					dlg->GetSpritesData().erase(dlg->GetSpritesData().size()-1);// erase last
					errorFound = true;
				}
			}
		}
	}

	return errorFound;
}

void Scene::CheckSceneIntegrity() // use it before you save the scene : to remove obsolete data
{
	bool errorsFound = CheckSceneSpritesIntegrity(true);// make sprites part clean
	for(u32 i = 0; i < m_Dialogs.size(); ++i)
	{
		DialogNode* dlg = m_Dialogs[i];
		if(dlg && dlg->GetIsLoaded())
		{
			// make sure we have same amount of children and conditional texts
			while(dlg->GetNbChildren() != dlg->GetConditionalTexts(0).size())
			{
				if(dlg->GetNbChildren() <= dlg->GetConditionalTexts(0).size())
					dlg->RemoveConditionalTextAtIndex(dlg->GetConditionalTexts(0).size()-1);
				else
					for(u32 j = 0; j < Engine::LS_MAX; ++j)
						dlg->AddConditionalText(L"{empty string}", j);
			}

			if(dlg->GetNodeType() != DialogNode::DN_MULTI_VALID)// non multi : we are not supposed to have conditional text : empty them all
			{
				for(u32 k = 0; k < Engine::LS_MAX; ++k)
					for(u32 j = 0; j < dlg->GetConditionalTexts(k).size(); ++j)
						dlg->GetConditionalTexts(k)[j] = L"";
			}

			if(dlg->GetNodeType() != DialogNode::DN_QTE_VALID)// non QTE : we are not supposed to have inputs : remove them all
			{
				dlg->GetInputs().clear();
			}
			else// QTE : we must have 2 children : 1st for win and 2nd for fail
			{
				// ending dialog has no child
// 				if(dlg->GetNbChildren() == 0)// no child at all : branch the win on ourself
// 					dlg->BranchToNode(dlg);

				if(dlg->GetNbChildren() == 1)// no 2nd child : branch the fail on ourself
					dlg->BranchToNode(dlg);

				// remove all others
				while(dlg->GetNbChildren() > 2)
					dlg->RemoveChildAtIndex(dlg->GetNbChildren()-1);

				assert(dlg->GetNbChildren() <= 2);

				if(dlg->GetInputs().empty())
				{
					InputData input(InputData::PI_ACTION1, 0.0f);
					dlg->AddInput(input);
				}
			}
		}
	}
}

void Scene::FlushSpritesBank()// unload sprites used nowhere
{
	for(u32 i = 0; i < m_SpritesBank.size(); ++i)
	{
		if(m_SpritesBank[i].IsValid() && m_SpritesBank[i].GetRefCount() == 1)
		{
			m_SpritesBank[i] = NULL;
		}
	}
}

void Scene::FlushSoundsAlive()// remove sounds used nowhere
{
	/*for(u32 i = 0; i < m_SoundsBank.size(); ++i)
		if(m_SoundsBank[i].IsValid() && m_SoundsBank[i].GetRefCount() == 1)
			m_SoundsBank.erase(i--);*/

	m_SoundsAlive.clear();
	m_SoundsAliveData.clear();
	SmartPointer<AudioStream> as(NULL);
	for(u32 i = 0; i < 3; ++i)
	{
		m_SoundsAlive.push_back(as);
		m_SoundsAliveData.push_back(AudioDataSerializable());
	}
}

void Scene::Step(float dt, bool skipDialogValidation/* = false*/)
{
	if(m_CurrentDialog)
	{
		bool hasEnded = HasCurrentDialogEnded(true);// ignore outro time
		bool useAccumulator = false;
		if(useAccumulator)
		{
			m_Accumulator += dt;
			m_Accumulator = core::min_(m_Accumulator, 0.1f);
			if(m_Accumulator >= SCENE_DT)
			{
				if(!hasEnded || m_CurrentDlgValidated)
					m_CurrentTimeInternal += SCENE_DT;
				m_CurrentTime += SCENE_DT;

				m_Accumulator -= SCENE_DT;
			}
		}
		else
		{
			if(!hasEnded || m_CurrentDlgValidated)
				m_CurrentTimeInternal += dt;
			m_CurrentTime += dt;
		}

		if(!m_CurrentDialog->GetIsLoaded())
			LoadDialog(m_CurrentDialog);

		StepInputs(skipDialogValidation);

		if(m_SoundsAlive[0].IsValid())
		{
			float vol = m_SoundsAlive[0]->GetVolume() *
#ifdef IRON_ICE_ENGINE
				0.01f*ProfileData::GetInstance()->m_ODS.m_VolumeBG1 * 0.01f*ProfileData::GetInstance()->m_ODS.m_VolumeGeneral;
#else
				1.0f;
#endif
			Engine::GetInstance()->GetAudioDevice()->SetSoundVolume(m_SoundsAlive[0], vol);
		}
		if(m_SoundsAlive[1].IsValid())
		{
			float vol = m_SoundsAlive[1]->GetVolume() *
#ifdef IRON_ICE_ENGINE
				0.01f*ProfileData::GetInstance()->m_ODS.m_VolumeBG2 * 0.01f*ProfileData::GetInstance()->m_ODS.m_VolumeGeneral;
#else
				1.0f;
#endif
			Engine::GetInstance()->GetAudioDevice()->SetSoundVolume(m_SoundsAlive[1], vol);
		}
		if(m_SoundsAlive[2].IsValid())
		{
			float vol = m_SoundsAlive[2]->GetVolume() *
#ifdef IRON_ICE_ENGINE
				0.01f*ProfileData::GetInstance()->m_ODS.m_VolumeVoice * 0.01f*ProfileData::GetInstance()->m_ODS.m_VolumeGeneral;
#else
				1.0f;
#endif
			Engine::GetInstance()->GetAudioDevice()->SetSoundVolume(m_SoundsAlive[2], vol);
		}
	}
}

void Scene::StepInputs(bool skipDialogValidation/* = false*/)
{
	bool hasEnded = HasCurrentDialogEnded(true);// ignore outro time

	m_CurrentInputs.m_Inputs = 0;// Current inputs to validate
	m_CurrentInputsRemainingTime = 0.0f;
	if(hasEnded)
	{
		static bool force1stChild = false;
		if(force1stChild || m_Loop1stDialog)
		{
			m_CurrentChoice = 0;
			m_CurrentDlgValidated = true;
		}
		else
		{
			if(!m_CurrentDlgValidated)
			{
				switch(m_CurrentDialog->GetNodeType())
				{
				case DialogNode::DN_AUTO_VALID:
					m_CurrentDlgValidated = true;
					break;
				case DialogNode::DN_MANUAL_VALID:
					m_CurrentInputs.SetPressed(InputData::PI_ACTION1, true);
					if(m_CurrentInputsPressed.IsPressed(InputData::PI_ACTION1))
					{
						m_CurrentDlgValidated = true;
						m_CurrentInputs.m_NbCurrentPress = 1;// for display
					}
					break;
				case DialogNode::DN_QTE_VALID:
					{
						float extraTime = m_CurrentTime-m_CurrentDialog->GetDialogTotalDuration(true);// begin date of the QTE
						float totalTime = 0.0f;
						u32 currentIndex = 0xffffffff;
						bool hasFailed = false;
						for(u32 i = 0; i < m_CurrentDialog->GetInputs().size(); ++i)
						{
							totalTime += m_CurrentDialog->GetInputs()[i].m_Duration;
							currentIndex = i;
							if(extraTime >= totalTime)// this input is elapsed : check it is validated
							{
								if(m_CurrentDialog->GetInputs()[i].m_NbCurrentPress < (u32)m_CurrentDialog->GetInputs()[i].m_NbPress)// input has not been pressed : we have fail !
								{
									m_CurrentChoice = 1;// 2nd choice = fail
									m_CurrentDlgValidated = true;
									m_CurrentInputs = m_CurrentDialog->GetInputs()[i];// for displaying the failed input
									m_CurrentInputsRemainingTime = totalTime - extraTime;
#ifdef IRON_ICE_ENGINE
									if(!m_Loop1stDialog && GameDesc::m_CurrentChapter < 5)
										++ProfileData::GetInstance()->m_PDS.m_nbQTEFail;
#endif
									break;
								}
							}
							else// next input time is situated after our time : we have the current input's index
								break;
						}

						if(m_CurrentChoice == 0)// still a win
						{
							if(extraTime >= totalTime)// we have reach the end of the inputs
							{
								m_CurrentDlgValidated = true;

#ifdef IRON_ICE_ENGINE
								if(!m_Loop1stDialog && GameDesc::m_CurrentChapter < 5)
									++ProfileData::GetInstance()->m_PDS.m_nbQTESuccess;
#endif
							}
							else if(currentIndex != 0xffffffff)
							{
								if(m_CurrentInputsPressed.m_Inputs == 0)// all released
								{
									m_CurrentInputsPressed.m_CurrentState = false;
								}
								else if(m_CurrentDialog->GetInputs()[currentIndex].m_Inputs == m_CurrentInputsPressed.m_Inputs &&
									!m_CurrentInputsPressed.m_CurrentState )// all just pressed correctly
								{
									++m_CurrentDialog->GetInputs()[currentIndex].m_NbCurrentPress;
									m_CurrentInputsPressed.m_CurrentState = true;
								}
								m_CurrentInputs = m_CurrentDialog->GetInputs()[currentIndex];
								m_CurrentInputsRemainingTime = totalTime - extraTime;
							}
						}
					}
					break;
				case DialogNode::DN_MULTI_VALID:
					if(m_CurrentInputsPressed.IsPressed(InputData::PI_DOWN))
					{
						++m_CurrentChoice;
						if(m_CurrentChoice >= m_CurrentDialog->GetNbChildren())
							m_CurrentChoice = 0;
						m_CurrentInputsPressed.SetPressed(InputData::PI_DOWN, false);
					}
					if(m_CurrentInputsPressed.IsPressed(InputData::PI_UP))
					{
						if(m_CurrentChoice == 0)
							m_CurrentChoice = m_CurrentDialog->GetNbChildren();
						--m_CurrentChoice;
						m_CurrentInputsPressed.SetPressed(InputData::PI_UP, false);
					}
					m_CurrentInputs.SetPressed(InputData::PI_ACTION1, true);
					if(m_CurrentInputsPressed.IsPressed(InputData::PI_ACTION1))
					{
						m_CurrentDlgValidated = true;
						m_CurrentInputs.m_NbCurrentPress = 1;
					}
					break;
				}
			}
		}

		if(!skipDialogValidation && m_CurrentDlgValidated && HasCurrentDialogEnded())// validated and outro finished
		{
			DialogNode* prevDlg = m_CurrentDialog;
			if(prevDlg)
			{
				prevDlg->ResetInputsCounter();
			}
			assert(m_CurrentChoice < m_CurrentDialog->GetNbChildren() || m_CurrentDialog->GetNbChildren() == 0);
			if(!m_Loop1stDialog)
				m_CurrentDialog = m_CurrentDialog->GetChildAtIndex(m_CurrentChoice);
			if(m_CurrentDialog)
			{
				m_CurrentDialog->ResetInputsCounter();
			}
			else// end of the scene
				m_HasEnded = true;
			m_CurrentChoice = 0;// default is win until we fail for QTE and default choice is always the 1st one for multi
			m_CurrentDlgValidated = false;
			if(prevDlg != m_CurrentDialog)
			{
				prevDlg->UnLoad();
				LoadDialog(m_CurrentDialog);
			}
			else
			{
				InitDialog(m_CurrentDialog);// reinit the dlg
			}
			m_CurrentTime = m_CurrentTimeInternal = 0.0f;
			//FlushSoundsAlive();
			FlushSpritesBank();
		}
	}
	else // not ended
	{
		//reset inputs
		m_CurrentInputsPressed = InputData();
	}
}

void Scene::RestoreSounds()
{
	if(m_SoundsAlive.empty())// no bgms ? create an empty one
	{
		m_SoundsAliveData.clear();

		SmartPointer<AudioStream> bgm(NULL);
		for(u32 i = 0; i < 3; ++i)
		{
			m_SoundsAlive.push_back(bgm);
			m_SoundsAliveData.push_back(AudioDataSerializable());
		}
	}

	for(u32 i = 0; i < 3; ++i)
	{
		if(GameDesc::m_CurrentSounds[i].GetName() != "")
		{
			char fileName[1024] = "";
			sprintf(fileName, "%ssounds/%s", m_ScenePath.c_str(), GameDesc::m_CurrentSounds[i].GetName().c_str());

			m_SoundsAlive[i] = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile(fileName);
			m_SoundsAliveData[i] = GameDesc::m_CurrentSounds[i];
			float vol = GameDesc::m_CurrentSounds[i].GetVolume();
			float pitch = GameDesc::m_CurrentSounds[i].GetPitch();
			float pan = GameDesc::m_CurrentSounds[i].GetPan();
			bool repeat = GameDesc::m_CurrentSounds[i].GetRepeat();
			bool stop = GameDesc::m_CurrentSounds[i].GetStopAtEnd();
			Engine::GetInstance()->GetAudioDevice()->SetupSound(m_SoundsAlive[i], pan, pitch, vol, repeat, stop);
			if(m_SoundsAlive[i]->IsValid())
				m_SoundsAlive[i]->Reset();
			if(m_SoundsAlive[i]->IsPlaying())
			{
				m_SoundsAlive[i]->Stop();
				m_SoundsAlive[i]->Play();
			}
		}
	}
}


// GameDesc
#ifndef IRON_ICE_FINAL
bool GameDesc::ReadLineTxt(wchar_t *line, NodeDescType& outType, u32& outId, core::stringw& outName)
{
	if((line[0] == L'C' || line[0] == L'c') && (line[1] == L'H' || line[1] == L'h'))
	{
		outType = NDT_CHAPTER;
	}
	else if((line[0] == L'S' || line[0] == L's') && (line[1] == L'C' || line[1] == L'c'))
	{
		outType = NDT_SCENE;
	}
	else
	{
		return false;
	}
	wchar_t wid[3] = {line[2], line[3], 0};
	outId = wtoi(wid, 10);
	line += 4;
	bool equalFound = false;
	while(isspace(*line) || *line == L'=')
	{
		if(*line == L'=')
			equalFound = true;
		++line;
	}
	if(!equalFound)
		return false;

	outName = line;

	return true;
}

bool GameDesc::ReadFromFileTxt(const char* path, Engine::LanguageSettings language/* = Engine::LS_FRENCH*/)
{
	if(language >= Engine::LS_MAX)
		return false;

	FILE* file = fopen(path, "r, ccs=UNICODE");
	if (!file)
		return false;

	//ChaptersDesc.clear();
	//m_NbScenes = 0;

	wchar_t line[1024];
	ChapterDesc* curChapter = NULL;
	NodeDesc* curNode = NULL;

	while (fgetws(line, 1024, file) != NULL)
	{
		NodeDescType type(NDT_UNDEFINED);
		u32 id(0xffffffff);
		core::stringw name;
		if(!ReadLineTxt(line, type, id, name))
		{
			if(curNode && !islinereturn(*line))
			{
				curNode->Name[language] += line;
			}
			continue;
		}
		if(type == NDT_CHAPTER)
		{
			for(u32 i = ChaptersDesc.size(); i < id; ++i)// fill the descs with empty descs (if needed) until the one we want
			{
				ChapterDesc desc(i);
				ChaptersDesc.push_back(desc);
			}
			ChapterDesc& desc = ChaptersDesc[id-1];
			desc.Name[language] = name;
			curChapter = &desc;
			curNode = curChapter;
		}
		else if(type == NDT_SCENE)
		{
			assert(curChapter);
			if(!curChapter)
				return false;
			for(u32 i = curChapter->ScenesDesc.size(); i < id; ++i)// fill the descs with empty descs (if needed) until the one we want
			{
				SceneDesc desc(i);
				curChapter->ScenesDesc.push_back(desc);
				++m_NbScenes;
			}
			SceneDesc& desc = curChapter->ScenesDesc[id-1];
			desc.Name[language] = name;
			curNode = &desc;
		}
		else
		{
			assert(false);
			return false;
		}
	}

	fclose(file);

	return true;
}
#endif

void GameDesc::LoadPreviews()
{
	Engine* engine = Engine::GetInstance();
	video::IVideoDriver* driver = engine->GetDriver();

	for(u32 i = 0; i < ChaptersDesc.size(); ++i)
	{
		ChapterDesc& chapter = ChaptersDesc[i];
		core::stringc path = GetChapterPath(chapter.ID, "preview.jpg");
		SAFE_LOAD_IMAGE(driver, chapter.PreviewTex, path, true);
		for(u32 j = 0; j < chapter.ScenesDesc.size(); ++j)
		{
			SceneDesc& scene = chapter.ScenesDesc[j];
			SAFE_LOAD_IMAGE(driver, scene.PreviewTex, GetScenePath(chapter.ID, scene.ID, "preview.jpg"), true);
		}
	}
}

void GameDesc::UnloadPreviews()
{
	for(u32 i = 0; i < ChaptersDesc.size(); ++i)
	{
		ChapterDesc& chapter = ChaptersDesc[i];
		SAFE_UNLOAD(chapter.PreviewTex);
		for(u32 j = 0; j < chapter.ScenesDesc.size(); ++j)
		{
			SceneDesc& scene = chapter.ScenesDesc[j];
			SAFE_UNLOAD(scene.PreviewTex);
		}
	}
}

bool GameDesc::GoToNextScene()
{
	bool noMoreScene = ChaptersDesc.size() == 0 || // no chapter at all
		(GameDesc::m_CurrentChapter >= ChaptersDesc.size()) || // chapter higher than the last one
		(	(GameDesc::m_CurrentChapter+1 == ChaptersDesc.size()) && // next scene after the last scene of the last chapter
		(GameDesc::m_CurrentScene+1 >= ChaptersDesc[ChaptersDesc.size()-1].ScenesDesc.size())	);
	if(noMoreScene)
		return false;

	GameDesc::m_CurrentScene = (GameDesc::m_CurrentScene+1 >= ChaptersDesc[GameDesc::m_CurrentChapter].ScenesDesc.size())? 0 : GameDesc::m_CurrentScene+1;
	if(GameDesc::m_CurrentScene == 0)
		++GameDesc::m_CurrentChapter;
	assert(GameDesc::m_CurrentChapter < ChaptersDesc.size());

	GameDesc::m_CurrentDialog = 0;// initial dialog
	for(u32 i = 0; i < 3; ++i)
		GameDesc::m_CurrentSounds[i] = AudioDataSerializable();

	return true;
}

float GameDesc::GetProgressionRatio(u32 chapterIdx, u32 sceneIdx)
{
	if(chapterIdx == 0xff && sceneIdx == 0xff)// game finished
		return 1.0f;
	if(!GetSceneFromId(chapterIdx, sceneIdx))
		return 0.0f;

	u32 scId = 0;
	for(u32 i = 0; i < chapterIdx; ++i)
	{
		ChapterDesc* ch = GetChapterFromId(i);
		if(ch)
			scId += ch->ScenesDesc.size();
	}
	scId += sceneIdx;

	return (float)scId/m_NbScenes;
}

bool GameDesc::ReadFromFile(const char* path)
{
	ChaptersDesc.clear();
	m_NbScenes = 0;

	FILE* input = fopen(path, "rb");
	if (!input)
		return false;

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "DESC", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	// read nb languages supported
	u32 nbLang = 0;
	fread(&nbLang, sizeof(u32), 1, input);

	// read nb chapters
	u32 nbChapters = 0;
	fread(&nbChapters, sizeof(u32), 1, input);
#if defined(SNOW_LIGHT) && defined(DEMO_VERSION)
	assert(nbChapters == 1);
#endif
	for(u32 i = 0; i < nbChapters; ++i)
	{
		ChapterDesc newChapter(i);
		ChaptersDesc.push_back(newChapter);
		ChapterDesc& chapter = ChaptersDesc.getLast();

		// read chapter names
		for(u32 k = 0; k < nbLang; ++k)
		{
			DeserializeStringW(chapter.Name[k], input);
		}

		u32 nbScenes = 0;
		fread(&nbScenes, sizeof(u32), 1, input);
#if defined(SNOW_LIGHT) && defined(DEMO_VERSION)
		assert(nbScenes == 4);
#endif
		for(u32 j = 0; j < nbScenes; ++j)
		{
			SceneDesc newScene(j);
			chapter.ScenesDesc.push_back(newScene);
			SceneDesc& scene = chapter.ScenesDesc.getLast();
			++m_NbScenes;

			// read scene names
			for(u32 k = 0; k < nbLang; ++k)
			{
				DeserializeStringW(scene.Name[k], input);
			}
		}
	}

	fclose(input);

	return true;
}

#ifndef IRON_ICE_FINAL
bool GameDesc::WriteToFile(const char* path)
{
	FILE* output = fopen(path, "wb");
	if (!output)
		return false;

	char header[5] = "DESC";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	// write nb languages supported
	u32 nbLang = Engine::LS_MAX;
	fwrite(&nbLang, sizeof(u32), 1, output);

	// write nb chapters
	u32 nbChapters = ChaptersDesc.size();
	fwrite(&nbChapters, sizeof(u32), 1, output);
	for(u32 i = 0; i < nbChapters; ++i)
	{
		ChapterDesc& chapter = ChaptersDesc[i];
		assert(chapter.ID == i);

		// write chapter names
		for(u32 k = 0; k < nbLang; ++k)
		{
			SerializeStringW(chapter.Name[k], output);
		}

		u32 nbScenes = chapter.ScenesDesc.size();
		fwrite(&nbScenes, sizeof(u32), 1, output);
		for(u32 j = 0; j < nbScenes; ++j)
		{
			SceneDesc& scene = chapter.ScenesDesc[j];
			assert(scene.ID == j);

			// write scene names
			for(u32 k = 0; k < nbLang; ++k)
			{
				SerializeStringW(scene.Name[k], output);
			}
		}
	}

	fclose(output);

	return true;
}
#endif


#ifdef IRON_ICE_EDITOR

SceneEditorData::SceneEditorData()
{

}

SceneEditorData::~SceneEditorData()
{
	printf("Clearing cache files...\n");

	ClearDialogs();
	ClearSprites();
	ClearSounds();
}

bool SceneEditorData::LoadFromFile(char* path)
{
	FILE* input = fopen(path, "rb");
	if(!input)
	{
		return false;
	}

	if(!m_SpriteBankPreviews.empty() || !m_DialogListNames.empty())
	{
		fclose(input);
		return false;
	}

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "IIED", 4) != 0)
	{
		fclose(input);
		return false;
	}

	// current version 1.0

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	u32 size = 0;
	fread(&size, sizeof(u32), 1, input);// nb conditions
	for(u32 i = 0; i < size; ++i)
	{
		// read text
		core::stringw temp = L"";
		DeserializeStringW(temp, input);
		m_DialogListNames.push_back(temp);
		m_DialogsMD5.push_back(MD5::MD5Raw());
	}

	// read raw images bank
	u32 bankSize = 0;
	fread(&bankSize, sizeof(u32), 1, input);// size of bank
	for(u32 i = 0; i < bankSize; ++i)
	{
		Image* img = NULL;
		{
			u32 size = 0;
			fread(&size, sizeof(u32), 1, input);// compressed size
			unsigned char* buffer = new unsigned char[size];
			fread(buffer, sizeof(unsigned char), size, input);// compressed data

			io::IReadFile* memFile  = Engine::GetInstance()->GetDevice()->getFileSystem()->createMemoryReadFile(buffer, size, "foo.png", false);
			if(memFile)
			{
				img = driver->createImageFromFile(memFile);

				memFile->drop();
			}
			else
			{
				assert(0);
			}

			delete[] buffer;
		}
		Texture* tex = ImageToTexture(img, L"texture");
		if(USE_PREMULTIPLIED_ALPHA)
			PremultiplyAlpha(tex);
		img->drop();
		tex->grab();
		driver->removeTexture(tex);
		m_SpriteBankPreviews.push_back(tex);
		m_SpritesMD5.push_back(MD5::MD5Raw());
	}

	fclose(input);

	return true;
}

bool SceneEditorData::SaveToFile(char* path)
{
	FILE* output = fopen(path, "wb");
	if(!output)
		return false;

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	char header[5] = "IIED";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version


	// write dialog names
	u32 size = m_DialogListNames.size();
	fwrite(&size, sizeof(u32), 1, output);// nb conditions
	for(u32 i = 0; i < size; ++i)
	{
		// write text
		SerializeStringW(m_DialogListNames[i], output);
	}

	// write raw images bank
	u32 bankSize = m_SpriteBankPreviews.size();
	fwrite(&bankSize, sizeof(u32), 1, output);// size of bank
	for(u32 i = 0; i < bankSize; ++i)
	{
		Image* img = TextureToImage(m_SpriteBankPreviews[i]);

		u32 bpp = img->getBytesPerPixel();
		Dimension2 dim = img->getDimension();
		u32 size = dim.Width*dim.Height*bpp;
		assert(bpp == 4);

		{
			unsigned char* buffer = new unsigned char[size];
			io::IWriteFile* memFile  = Engine::GetInstance()->GetDevice()->getFileSystem()->createMemoryWriteFile(buffer, size, "foo.png", false);
			if(memFile)
			{
				driver->writeImageToFile(img, memFile);
				u32 compressedSizeTotal = memFile->getPos();

				fwrite(&compressedSizeTotal, sizeof(u32), 1, output);// compressed size
				fwrite(buffer, sizeof(unsigned char), compressedSizeTotal, output);// data (compressed or not)

				memFile->drop();
			}
			else
			{
				assert(0);
			}

			delete[] buffer;
			img->drop();
		}
	}

	fclose(output);

	return true;
}

void SceneEditorData::RemoveDialog(u32 index)
{
	char path[32];
	sprintf(path, "temp/dlg/%04d.dlg", index);
	if(remove(Engine::GetInstance()->GetGamePath(path).c_str()) == 0)
		printf("Removed dialog file %s\n", path);

	for(u32 i = index ; i < m_DialogListNames.size()-1 ; ++i)
	{
		char pathNew[32];
		sprintf(pathNew, "temp/dlg/%04d.dlg", i);
		char pathOld[32];
		sprintf(pathOld, "temp/dlg/%04d.dlg", i+1);
		if(rename(Engine::GetInstance()->GetGamePath(pathOld).c_str(),
			Engine::GetInstance()->GetGamePath(pathNew).c_str()) == 0)
			printf("Renamed dialog file %s to %s\n", pathOld, pathNew);
	}

	m_DialogListNames.erase(index);
	m_DialogsMD5.erase(index);

}

void SceneEditorData::AddDialog(DialogNode* dlg)// new/import/duplicate
{
	m_DialogListNames.push_back(dlg->GetDialogName().c_str());
	m_DialogsMD5.push_back(MD5::MD5Raw());// empty md5 because we never saved it
}

void SceneEditorData::SwapDialogNext(u32 index)
{
	core::stringw tempStr = m_DialogListNames[index];
	MD5::MD5Raw tempMD5 = m_DialogsMD5[index];
	m_DialogListNames[index] = m_DialogListNames[index+1];
	m_DialogsMD5[index] = m_DialogsMD5[index+1];
	m_DialogListNames[index+1] = tempStr;
	m_DialogsMD5[index+1] = tempMD5;

	char pathTemp[32] = "temp/dlg/temp.dlg";
	char pathNew[32];
	sprintf(pathNew, "temp/dlg/%04d.dlg", index);
	char pathOld[32];
	sprintf(pathOld, "temp/dlg/%04d.dlg", index+1);
	rename(Engine::GetInstance()->GetGamePath(pathOld).c_str(),
		Engine::GetInstance()->GetGamePath(pathTemp).c_str());
	if(rename(Engine::GetInstance()->GetGamePath(pathNew).c_str(),
		Engine::GetInstance()->GetGamePath(pathOld).c_str()) == 0)
		printf("Renamed dialog file %s to %s\n", pathNew, pathOld);
	if(rename(Engine::GetInstance()->GetGamePath(pathTemp).c_str(),
		Engine::GetInstance()->GetGamePath(pathNew).c_str()) == 0)
		printf("Renamed dialog file %s to %s\n", pathOld, pathNew);
}

void SceneEditorData::ClearDialogs()
{
	for(u32 i = 0 ; i < m_DialogListNames.size() ; ++i)
	{
		char path[32];
		sprintf(path, "temp/dlg/%04d.dlg", i);
		if(remove(Engine::GetInstance()->GetGamePath(path).c_str()) == 0)
			printf("Removed dialog file %s\n", path);
	}

	m_DialogListNames.clear();
	m_DialogsMD5.clear();
}

void SceneEditorData::RemoveSprite(u32 index)
{
	SAFE_UNLOAD(m_SpriteBankPreviews[index]);
	m_SpriteBankPreviews.erase(index);
	m_SpritesMD5.erase(index);

	char path[32];
	sprintf(path, "temp/spr/%04d.spr", index);
	if(remove(Engine::GetInstance()->GetGamePath(path).c_str()) == 0)
		printf("Removed sprite file %s\n", path);

	for(s32 i = (s32)index+1 ; i < (s32)m_SpriteBankPreviews.size()-1 ; ++i)
	{
		char pathNew[32];
		sprintf(pathNew, "temp/spr/%04d.spr", i);
		char pathOld[32];
		sprintf(pathOld, "temp/spr/%04d.spr", i+1);
		if(rename(Engine::GetInstance()->GetGamePath(pathOld).c_str(),
			Engine::GetInstance()->GetGamePath(pathNew).c_str()) == 0)
			printf("Renamed sprite file %s to %s\n", pathOld, pathNew);
	}
}

void SceneEditorData::AddSprite()// new/import
{
	m_SpriteBankPreviews.push_back(NULL);// will be filled later
	m_SpritesMD5.push_back(MD5::MD5Raw());// empty md5 because we never saved it
}

void SceneEditorData::ClearSprites()
{
	for(u32 i = 0 ; i < m_SpriteBankPreviews.size() ; ++i)
	{
		char path[32];
		sprintf(path, "temp/spr/%04d.spr", i);
		if(remove(Engine::GetInstance()->GetGamePath(path).c_str()) == 0)
			printf("Removed sprite file %s\n", path);
		SAFE_UNLOAD(m_SpriteBankPreviews[i]);
	}

	m_SpriteBankPreviews.clear();
	m_SpritesMD5.clear();
}

void SceneEditorData::ClearSounds()
{
	Engine::GetInstance()->GetAudioDevice()->CloseAllSounds();

	char fileName[256];
	// remove everything
	u32 i = 0;
	int ret = -1;
	do
	{
		if(ret == 0)
			printf("Removed sound file %s from the cache\n", fileName);
		sprintf(fileName, "temp/sounds/%04d.mp3", i);
		++i;
	} while((ret = remove(Engine::GetInstance()->GetGamePath(fileName).c_str())) == 0);
	if(ret == 32)
		printf("ERROR : unable to remove sound file %s from the cache because the file is opened\n", fileName);
}

void SceneEditorData::PushInCache(Scene* scene)
{
	if(scene)
	{
		char fileName[1024];
		sprintf(fileName, "temp/spr");
		mkdir(Engine::GetInstance()->GetGamePath(fileName).c_str());
		// remove everything
		{
			u32 i = 0;
			int ret = -1;
			do
			{
				if(ret == 0)
					printf("Removed sprite file %s from the cache\n", fileName);
				sprintf(fileName, "temp/spr/%04d.spr", i);
				++i;
			} while((ret = remove(Engine::GetInstance()->GetGamePath(fileName).c_str())) == 0);
		}
		for(u32 i = 0; i < scene->GetSprites().size(); ++i)
		{
			sprintf(fileName, "%sspr/%04d.spr", scene->GetScenePath().c_str(), i);
			char path[32];
			sprintf(path, "temp/spr/%04d.spr", i);
			if(CopyFile(fileName, Engine::GetInstance()->GetGamePath(path).c_str()))
				printf("Copied sprite file %s to %s in the cache\n", fileName, Engine::GetInstance()->GetGamePath(path).c_str());
			else
			{
				printf("Unable to find sprite file %s : creating a new empty one...\n", fileName);
				irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
				io::path path = fs->getWorkingDirectory();
				scene->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), i);
				fs->changeWorkingDirectoryTo(path);
			}
		}
		sprintf(fileName, "temp/dlg");
		mkdir(Engine::GetInstance()->GetGamePath(fileName).c_str());
		// remove everything
		{
			u32 i = 0;
			int ret = -1;
			do
			{
				if(ret == 0)
					printf("Removed dialog file %s from the cache\n", fileName);
				sprintf(fileName, "temp/dlg/%04d.dlg", i);
				++i;
			} while((ret = remove(Engine::GetInstance()->GetGamePath(fileName).c_str())) == 0);
		}
		for(u32 i = 0; i < scene->GetDialogs().size(); ++i)
		{
			sprintf(fileName, "%sdlg/%04d.dlg", scene->GetScenePath().c_str(), i);
			char path[32];
			sprintf(path, "temp/dlg/%04d.dlg", i);
			if(CopyFile(fileName, Engine::GetInstance()->GetGamePath(path).c_str()))
				printf("Copied dialog file %s to %s in the cache\n", fileName, Engine::GetInstance()->GetGamePath(path).c_str());
			else
			{
				printf("Unable to find dialog file %s : creating a new empty one...\n", fileName);
				scene->GetDialogAtIndex(i)->SetIsLoaded(true);
				irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
				io::path path = fs->getWorkingDirectory();
				scene->SaveDialogFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), i);
				fs->changeWorkingDirectoryTo(path);
			}
		}

		sprintf(fileName, "temp/sounds");
		mkdir(Engine::GetInstance()->GetGamePath(fileName).c_str());
		// remove everything
		{
			u32 i = 0;
			int ret = -1;
			do
			{
				if(ret == 0)
					printf("Removed sound file %s from the cache\n", fileName);
				sprintf(fileName, "temp/sounds/%04d.mp3", i);
				++i;
			} while((ret = remove(Engine::GetInstance()->GetGamePath(fileName).c_str())) == 0);
			if(ret == 32)
				printf("ERROR : unable to remove sound file %s from the cache because the file is opened\n", fileName);
		}
		for(u32 i = 0; i < scene->GetNbSoundsBank(); ++i)
		{
			sprintf(fileName, "%ssounds/%04d.mp3", scene->GetScenePath().c_str(), i);
			char path[32];
			sprintf(path, "temp/sounds/%04d.mp3", i);
			if(CopyFile(fileName, Engine::GetInstance()->GetGamePath(path).c_str()))
			{
				scene->AddSoundToBank(Engine::GetInstance()->GetGamePath(path).c_str(), false);
				printf("Copied sound file %s to %s in the cache\n", fileName, Engine::GetInstance()->GetGamePath(path).c_str());
			}
			else
			{
				printf("Unable to find sound file %s : ignoring it...\n", fileName);
			}
		}
	}
}

#endif
