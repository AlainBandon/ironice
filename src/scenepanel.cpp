﻿#include "panel.h"
#include "engine.h"
#include "utils.h"



// ScenePanel
ScenePanel::ScenePanel() : IPanel()
, m_Font(NULL)
, m_Scene(NULL)
, m_CurrentDlgTime(0.0f)
, m_PlayState(PS_NONE)
, m_DisplayDialogs(true)
, m_DisplayFades(true)
, m_DisplayKeys(true)
, m_UpdateKeys(true)
, m_PlaySounds(true)
, m_Loop1stDialog(false)
, m_IsTheRealGameScene(false)
, m_UsePremultipliedAlpha(USE_PREMULTIPLIED_ALPHA)
{
	SetDimension();
	m_ScenePath = Engine::GetInstance()->GetGamePath("system/menu_scn/0000/scene.scn");
}


ScenePanel::~ScenePanel()
{
	UnloadPanel();
}

void ScenePanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.DialogFontScale;

	if(!m_Scene)
	{
		m_Scene = new Scene();
		bool ret = m_Scene->LoadSceneFromFile(m_ScenePath.c_str());
		assert(ret);

		//m_Scene->ForceLoadDialog();
		m_PlayState = PS_GAME;
	}

	if(!m_Font)
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/DialogFont.ttf"));

		const u32 sizes[] = {32};
		{
			u32 size = (u32)((f32)sizes[0] * innerScale * m_Scale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	OnResize();

	IPanel::LoadPanel();
}

void ScenePanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	if(m_Scene)
	{
		SafeDelete(m_Scene);

		m_PlayState = PS_NONE;
	}

	SAFE_UNLOAD(m_Font);

	IPanel::UnloadPanel();
}

void ScenePanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.DialogFontScale;

	if(m_Font)
	{
		const u32 sizes[] = {32};
		{
			u32 size = (u32)((f32)sizes[0] * innerScale * m_Scale * fontScale);
			m_Font->setSize(size);
		}
	}
}

void ScenePanel::OnDevicePreReset()
{
	if(m_Scene)
	{
		m_BackupCurrentDlgID = m_Scene->GetCurrentDialog()? m_Scene->GetCurrentDialog()->GetID() : 
			(m_Scene->GetInitialDialog()? m_Scene->GetInitialDialog()->GetID() : 0xffffffff);
		m_BackupCurrentDlgTime = m_Scene->GetCurrentDialogTime();
	}
}

void ScenePanel::OnDevicePostReset()
{
	if(m_Scene)
	{
		m_Scene->SetCurrentDialog(m_Scene->GetDialogAtIndex(m_BackupCurrentDlgID));
		m_Scene->Step(m_BackupCurrentDlgTime, true);
	}
}

void ScenePanel::Update(float dt) 
{
	IPanel::Update(dt);

	if(m_Scene)
	{
		m_Scene->Loop1stDialog(m_Loop1stDialog);

		if(m_IsTheRealGameScene)
		{
			// store the current dialog id for any save purpose
			GameDesc::m_CurrentDialog = m_Scene->GetCurrentDialog()? m_Scene->GetCurrentDialog()->GetID()+1 : 0;
			for(u32 i = 0; i < 3; ++i)
			{
				GameDesc::m_CurrentSounds[i] = m_Scene->GetSoundAliveNamesAtIndex(i);
			}
		}

		if(m_PlayState == PS_GAME && m_Scene->HasEnded())
			m_PlayState = PS_NONE;

		if(m_PlayState == PS_GAME)
		{
			m_Scene->Step(dt);

#ifdef IRON_ICE_ENGINE
			if(m_IsTheRealGameScene && GameDesc::m_CurrentChapter < core::min_(GameDescManager::GetGameDesc()->ChaptersDesc.size(), (u32)PROFILE_CHAPTERS_MAX))
				ProfileData::GetInstance()->m_PDS.m_TimeCH[GameDesc::m_CurrentChapter] += dt;
#endif
		}

		if(m_PlaySounds)
		{
			// sound control
			for(u32 i = 0; i < m_Scene->GetSoundsAlive().size(); ++i)
			{
				SmartPointer<AudioStream> sound = m_Scene->GetSoundsAlive()[i];

				if(sound.IsValid() && sound->IsValid())
				{
					if(m_PlayState == PS_GAME)
					{
						sound->Play();
					}
					else
					{
						sound->Stop();
					}
				}
			}
		}
		else
		{
			for(u32 i = 0; i < m_Scene->GetSoundsAlive().size(); ++i)
			{
				SmartPointer<AudioStream> sound = m_Scene->GetSoundsAlive()[i];

				if(sound.IsValid() && sound->IsValid())
				{
					sound->Stop();
				}
			}
		}
	}
}

void ScenePanel::Render()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale * m_Scale;
	Dimension2 renderSize((u32)((f32)originalSize.Width*innerScale), (u32)((f32)originalSize.Height*innerScale));
	const Position2& innerOffset = engine->m_InnerOffset;

	Rectangle2 clip(innerOffset + m_Position, renderSize);

	if(m_Scene)
	{
		m_CurrentDlgTime = m_Scene->GetCurrentDialogTime();

		DialogNode* dlg = m_Scene->GetCurrentDialog();
		
		if(dlg && dlg->GetIsLoaded())
		{
			IColor color = COLOR_BLACK;
			fill2DRect(driver, Rectangle2(innerOffset + m_Position, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);

			Position2 shakeOffset(P2Zero);
			{
				shakeOffset += GetShakeOffset(m_CurrentDlgTime, innerScale, dlg->GetScreenShakeStrength(), dlg->GetScreenShakeFrequency());
			}

			// BG
			Texture* tex = m_Scene->GetCurrentBackground().IsValid()?m_Scene->GetCurrentBackground()->m_Image:NULL;
			if(tex)
			{
				const KeyTime& key = MathUtils::GetInterpolatedKeyTime(dlg->GetImageScene()->m_Keys, m_CurrentDlgTime, dlg->GetImageScene()->m_Loop);

				IColor tint = key.m_Color;
				Rectangle2 sourceRect(Position2(0,0), tex->getSize());
				Position2 pos = shakeOffset + Position2((s32)(0*innerScale), (s32)(0*innerScale)) + innerOffset + m_Position;
				Dimension2 dim = originalSize;
				float scale = core::max_(key.m_Scale, Epsilon);
				Rectangle2 destRect(pos, Dimension2((s32)(dim.Width*innerScale), (s32)(dim.Height*innerScale)));

				Rectangle2 srcRect(-key.m_Position, Dimension2((s32)((f32)originalSize.Width/scale), (s32)((f32)originalSize.Height/scale)));

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, (f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
					core::matrix4().setRotationAxisRadians(key.m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)destRect.getWidth()/srcRect.getWidth(), (f32)destRect.getHeight()/srcRect.getHeight(), 0.0f));

				draw2DImage(driver, tex, srcRect, mat, true, tint, true, m_UsePremultipliedAlpha, &clip);
			}

			// Sprites
			{
				//assert(dlg->GetSprites().size() == dlg->GetSpritesData().size());

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)shakeOffset.X + innerOffset.X + m_Position.X, (f32)shakeOffset.Y + innerOffset.Y + m_Position.Y, 0.0f)) * 
					core::matrix4().setScale(core::vector3df(innerScale, innerScale, 0.0f));

				for(u32 i = 0; i < m_Scene->GetSpritesAlive().size(); ++i)
				{
					SmartPointer<SpriteEntity> sprEnt = m_Scene->GetSpritesAlive()[i];
					SmartPointer<AnimatedSprite> spr = sprEnt->GetAnimatedSprite();
					const SpriteDataScene& data = sprEnt->GetSpriteData();
					KeyTime keyTime = MathUtils::GetInterpolatedKeyTime(data.m_Keys, m_CurrentDlgTime, data.m_Loop);
					if(keyTime.m_Time >= 0.0f)
						spr->RenderFrame(spr->GetFrameIndexFromTime(m_CurrentDlgTime, data.m_LoopAnim), mat*keyTime.GetMatrixTransform(), keyTime.m_Color, false, false, &clip);
				}
			}

			Position2 posInput = Position2((s32)(innerScale*originalSize.Width*0.5f), (s32)(innerScale*originalSize.Height*0.2f)) + innerOffset + m_Position;

			// dialog text
			if(m_Font)
			{
				u32 languageIndex = (u32)Engine::GetInstance()->GetLanguage();
				if(dlg->GetDialogHeader(languageIndex).size() != 0 || dlg->GetDialogText(languageIndex).size() != 0 ||
					(!dlg->GetConditionalTexts(languageIndex).empty() && dlg->GetConditionalTexts(languageIndex)[0].size() != 0))
				{
					if(m_DisplayDialogs)
					{
						Position2 pos = Position2((s32)(innerScale*originalSize.Width*dlg->GetDialogPosition().X), (s32)(innerScale*originalSize.Height*dlg->GetDialogPosition().Y)) + innerOffset + m_Position;
						Dimension2 dim((s32)(originalSize.Width*dlg->GetDialogSize().X), (s32)(originalSize.Height*dlg->GetDialogSize().Y));
						Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

						if(dlg->GetUseTextBackGround())
						{
							//driver->draw2DRectangleOutline(destRect, IColor(255, 255, 0, 0));
							Texture* tex = Engine::GetInstance()->GetTextBG();
							if(tex)
							{
								IColor tint = COLOR_WHITE;
								Rectangle2 srcRect(Position2(0,0), tex->getSize());
								core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, (f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
									core::matrix4().setRotationAxisRadians(0.0f, core::vector3df(0.0f, 0.0f, 1.0f)) *
									core::matrix4().setScale(core::vector3df((f32)destRect.getWidth()/srcRect.getWidth(), (f32)destRect.getHeight()/srcRect.getHeight(), 0.0f));
								draw2DImage(driver, tex, srcRect, mat, true, tint, true, m_UsePremultipliedAlpha);
							}
						}
						const s32 margin = (s32)(m_Scale*innerScale*20);
						destRect.LowerRightCorner.X -= margin;
						destRect.LowerRightCorner.Y -= margin;
						destRect.UpperLeftCorner.X += margin;
						destRect.UpperLeftCorner.Y += margin;

						if(dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID || dlg->GetNodeType() == DialogNode::DN_AUTO_VALID)
							posInput = destRect.LowerRightCorner;
						else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
							posInput = destRect.UpperLeftCorner;//Position2(destRect.LowerRightCorner.X, destRect.UpperLeftCorner.Y);

						core::stringw str;
						TArray<CGUIFreetypeFont::ColorElement> colors;
						float ratio = core::abs_(cosf(0.75f*core::PI*m_CurrentDlgTime));
						IColor color = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), 
							MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(255), 0.5f), ratio);
						Dimension2 strDim = (Dimension2)destRect.getSize();
						core::stringw header = dlg->GetDialogHeader(languageIndex);
						core::stringw text = header + dlg->GetDialogText(languageIndex);
						u32 textBaseSize = text.size();
						colors.push_back(CGUIFreetypeFont::ColorElement(COMPUTE_THEME_COLOR(255), textBaseSize));
						for(u32 i = 0; i < dlg->GetConditionalTexts(languageIndex).size(); ++i)
						{
							text += L"\n";
							text += dlg->GetConditionalTexts(languageIndex)[i];
							bool isCurrentChoice = (m_Scene->GetCurrentDialogChoice() == i);
							colors.push_back(CGUIFreetypeFont::ColorElement(isCurrentChoice? color : COMPUTE_THEME_COLOR(255), text.size()-textBaseSize));
							textBaseSize = text.size();
						}
						FormatString(str, text, m_Font, strDim);
						m_Font->draw(str.subString(0, (dlg->GetDialogSpeed() == 0.0f)?text.size():header.size()+(u32)(dlg->GetDialogSpeed()*m_CurrentDlgTime)), 
							destRect, colors, 1.0f, false, false, COLOR_TRANSPARENT, &destRect);

						m_DialogChoiceAreas.clear();
						if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID && colors.size() > 1)
						{
							u32 curSize = colors[0].Size;
							for(u32 i = 1; i < colors.size(); ++i)
							{
								TArray<u32> outLinesWidth;
								Rectangle2 currentRect = m_Font->getSubStringRect(text.c_str(), curSize+1, colors[i].Size-1, outLinesWidth);
								currentRect.LowerRightCorner += posInput;
								currentRect.UpperLeftCorner += posInput;
								m_DialogChoiceAreas.push_back(currentRect);

								curSize += colors[i].Size;
							}

							u32 currentChoice = m_Scene->GetCurrentDialogChoice();
							posInput.X = m_DialogChoiceAreas[currentChoice].UpperLeftCorner.X + m_DialogChoiceAreas[currentChoice].getWidth();
							posInput.Y = m_DialogChoiceAreas[currentChoice].UpperLeftCorner.Y + (m_DialogChoiceAreas[currentChoice].getHeight()>>1);
						}
					}
				}
				else
				{
					Position2 pos = Position2((s32)(innerScale*originalSize.Width*dlg->GetDialogPosition().X), (s32)(innerScale*originalSize.Height*dlg->GetDialogPosition().Y)) + innerOffset + m_Position;
					Dimension2 dim((s32)(originalSize.Width*dlg->GetDialogSize().X), (s32)(originalSize.Height*dlg->GetDialogSize().Y));
					Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

					const s32 margin = (s32)(innerScale*20);
					destRect.LowerRightCorner.X -= margin;
					destRect.LowerRightCorner.Y -= margin;
					destRect.UpperLeftCorner.X += margin;
					destRect.UpperLeftCorner.Y += margin;

					if(dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID || dlg->GetNodeType() == DialogNode::DN_AUTO_VALID)
						posInput = destRect.LowerRightCorner;
					else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						posInput = destRect.UpperLeftCorner;//Position2(destRect.LowerRightCorner.X, destRect.UpperLeftCorner.Y);
				}
			}

			// Buttons display
			if(m_DisplayKeys)
			{
				InputData& inputs = m_Scene->GetCurrentInputs();
				float currentInputsRemainingTime = m_Scene->GetCurrentInputsRemainingTime();
				if(inputs.m_Inputs != 0)
				{
#ifdef _DEBUG
					// debug display
					{
						Position2 pos = Position2(0,0) + innerOffset + m_Position;
						Dimension2 dim(originalSize.Width, originalSize.Height);
						Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

						//driver->draw2DRectangleOutline(destRect, IColor(255, 255, 0, 0));
						core::stringw text = L"Appuyez sur ";
						bool inputProcessed = false;
						TArray<CGUIFreetypeFont::ColorElement> colorElts;
						colorElts.push_back(CGUIFreetypeFont::ColorElement(COLOR_WHITE, text.size()));
						for(u32 i = 0; i < InputData::PI_MAX; ++i)
						{
							if(inputs.IsPressed((InputData::EPadInput)i))
							{
								IColor color = (m_Scene->GetCurrentInputsPressed().IsPressed((InputData::EPadInput)i))?
									IColor(255, 0, 255, 0):IColor(255, 255, 0, 0);

								u32 start = text.size();
								if(inputProcessed)
									text += L" ET ";
								inputProcessed = true;
								text += InputData::GetInputName((InputData::EPadInput)i);
								u32 end = text.size();

								colorElts.push_back(CGUIFreetypeFont::ColorElement(color, end - start));
							}
						}
						m_Font->draw(text, destRect, colorElts, 1.0f,  false, false);
					}
#endif // #ifdef _DEBUG

					// real display
					{
						u32 alpha = 255;
						IColor color = (dlg->GetNodeType() == DialogNode::DN_QTE_VALID && inputs.m_NbCurrentPress >= inputs.m_NbPress)? 
							IColor(alpha, 100, 255, 100) : IColor(alpha, 255, 255, 255);

						IColor tint(COLOR_WHITE);
						{
							float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
							tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
						}

						TArray<ProfileData::KeyMappingSerializable::IIKeyType> inputKeys;
						for(u32 i = 0; i < InputData::PI_MAX; ++i)
						{
							if(inputs.IsPressed((InputData::EPadInput)i))
							{
								inputKeys.push_back((ProfileData::KeyMappingSerializable::IIKeyType)i);
							}
						}
						Dimension2 largestSize(230,200);
						if(engine->GetKeyElement(Engine::KET_EMPTY_KEY2))
							largestSize = engine->GetKeyElement(Engine::KET_EMPTY_KEY2)->getSize();
						float renderScale = 0.35f * innerScale;
						float dx = renderScale*largestSize.Width;
						float dy = renderScale*largestSize.Height;
						for(u32 i = 0; i < inputKeys.size(); ++i)
						{
							ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;

							EKEY_CODE keycode = keymap.GetKeyCode1(inputKeys[i]);
							core::stringw strCode = GetStringFromKeyCode(keycode);

							if(strCode != L"")
							{
								Engine::KeyElementType type = (strCode.size() > 1)? Engine::KET_EMPTY_KEY2 : Engine::KET_EMPTY_KEY1;
								// special keys
								if(keycode == KEY_LBUTTON)
									type = Engine::KET_LMOUSE;
								else if(keycode == KEY_RBUTTON)
									type = Engine::KET_RMOUSE;
								else if(keycode == KEY_LEFT)
									type = Engine::KET_LEFT_KEY;
								else if(keycode == KEY_RIGHT)
									type = Engine::KET_RIGHT_KEY;
								else if(keycode == KEY_UP)
									type = Engine::KET_UP_KEY;
								else if(keycode == KEY_DOWN)
									type = Engine::KET_DOWN_KEY;

								const float fadeInDuration = core::min_(0.15f, 0.5f*inputs.m_Duration);
								const float fadeOutDuration = core::min_(0.3f, 0.5f*inputs.m_Duration);
								const float maxZoomFactor = 3.0f;
								const float minZoomFactor = 0.01f;
								float fadeEffector = 1.0f;
								float fadeEffector2 = 1.0f;
								if(dlg->GetNodeType() == DialogNode::DN_QTE_VALID && fadeInDuration > 0.0f && fadeOutDuration > 0.0f)
								{
									if(currentInputsRemainingTime >= inputs.m_Duration - fadeInDuration)
									{
										fadeEffector = inputs.m_Duration - currentInputsRemainingTime;// 0 to duration
										fadeEffector = 1.0f - core::min_(fadeEffector, fadeInDuration)/fadeInDuration;// 1.0f to 0.0f in fadeInDuration seconds
										fadeEffector *= (maxZoomFactor-1.0f);
										fadeEffector += 1.0f;// from maxZoomFactor to 1.0f in fadeInDuration seconds
									}
									else if(currentInputsRemainingTime <= fadeOutDuration)
									{
										fadeEffector = currentInputsRemainingTime;// duration to 0
										fadeEffector = 1.0f - core::min_(fadeEffector, fadeOutDuration)/fadeOutDuration;// 0.0f to 1.0f in fadeOutDuration seconds
										fadeEffector *= (minZoomFactor-1.0f);
										fadeEffector += 1.0f;// from 1.0f to minZoomFactor in fadeOutDuration seconds
									}
									else
									{
										if(inputs.m_NbPress > 1)
											fadeEffector2 = 1.0f - 0.6f * core::abs_(cosf(5.0f*core::PI*currentInputsRemainingTime));
										else
											fadeEffector2 = 1.0f - 0.1f * core::abs_(cosf(1.5f*core::PI*currentInputsRemainingTime));
									}
								}
								else
								{
									fadeEffector2 = 1.0f - 0.1f * core::abs_(cosf(1.5f*core::PI*m_Time));
								}

								Position2 pos2(posInput);
								if(dlg->GetNodeType() == DialogNode::DN_QTE_VALID)// top centered
								{
									if(inputKeys.size() > 1)
										pos2.X += (s32)(((float)i - 0.5f * (inputKeys.size()-1)) * (dx + dy) * fadeEffector);
								}
								else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)// left aligned
								{
								}
								else// bottom right aligned
								{
									if(inputKeys.size() > 1)
										pos2.X -= (s32)((dx + dy) * (inputKeys.size() - i - 1));
									pos2.Y -= (s32)(dy*0.5f);
								}


								if(engine->GetKeyElement(type))
								{
									Dimension2 size = engine->GetKeyElement(type)->getSize();
									Rectangle2 sourceRect(Position2(0,0), size);

									if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)// left aligned
										pos2.X += (s32)(renderScale*size.Width*0.5f);
									else if(dlg->GetNodeType() != DialogNode::DN_QTE_VALID)// bottom right aligned
										pos2.X -= (s32)(renderScale*size.Width*0.5f);

									float renderScale2 = (float)dy/size.Height;
									if(type == Engine::KET_EMPTY_KEY2)
										renderScale2 *= 2.0f;
									else if(type == Engine::KET_RMOUSE || type == Engine::KET_LMOUSE)
										renderScale2 *= 1.0f;
									else
										renderScale2 *= 1.5f;


									renderScale2 *= fadeEffector*fadeEffector2;

									core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
										core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
										core::matrix4().setScale(core::vector3df(renderScale2, renderScale2, 0.0f));

									draw2DImage(driver, engine->GetKeyElement(type), sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);
								}

								if(type == Engine::KET_EMPTY_KEY2 || type == Engine::KET_EMPTY_KEY1)
								{
									//if(isSelected)
									{
										m_Font->drawGauss(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), tint, fadeEffector, true, true);
									}
									m_Font->draw(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), color, fadeEffector, true, true);
								}
							}
						}
					}
				}
			}

			// draw transition effects
			if(m_DisplayFades)
			{
				// in
				if(m_Scene->IsInIntro())
				{
					switch(dlg->GetTransitionIn().m_Type)
					{
					case DialogNode::DN_TRANSITION_FADE:
						{
							const u8 a = 255-(u8)(255.0f*m_Scene->GetIntroRatio());
							IColor color(a, 0, 0, 0);
							Position2 pos = innerOffset + m_Position;
							Dimension2 dim((u32)(originalSize.Width*innerScale), (u32)(originalSize.Height*innerScale));
							Rectangle2 destRect(pos, dim);
							fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
						}
						break;
					}
				}
				// out
				if(m_Scene->IsInOutro())
				{
					switch(dlg->GetTransitionOut().m_Type)
					{
					case DialogNode::DN_TRANSITION_FADE:
						{
							IColor color(255-(u8)(255.0f*m_Scene->GetOutroRatio()), 0, 0, 0);
							Position2 pos = innerOffset + m_Position;
							Dimension2 dim((u32)(originalSize.Width*innerScale), (u32)(originalSize.Height*innerScale));
							Rectangle2 destRect(pos, dim);
							fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
						}
						break;
					}
				}
			}
		}
	}
}

bool ScenePanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(!m_UpdateKeys)
		return false;

	EKEY_CODE key = (EKEY_CODE)0x0;
	bool isPressed = false;
	
	// convert mouse events into key events
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			key = KEY_LBUTTON;
			isPressed = true;
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_LEFT_UP)
		{
			key = KEY_LBUTTON;
			isPressed = false;
		}
		else if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
		{
			key = KEY_RBUTTON;
			isPressed = true;
		}
		else if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
		{
			key = KEY_RBUTTON;
			isPressed = false;
		}
		else if(event.MouseInput.Event == EMIE_MMOUSE_PRESSED_DOWN)
		{
			key = KEY_MBUTTON;
			isPressed = true;
		}
		else if(event.MouseInput.Event == EMIE_MMOUSE_LEFT_UP)
		{
			key = KEY_MBUTTON;
			isPressed = false;
		}
		else if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			// override scene dialog choice control with mouse controls
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			EKEY_CODE keyValid = keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE1);
			if(keyValid == KEY_LBUTTON || keyValid == KEY_RBUTTON || keyValid == KEY_MBUTTON)
			{
				for(u32 i = 0; i < m_DialogChoiceAreas.size(); ++i)
				{
					Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
					if(m_DialogChoiceAreas[i].isPointInside(mousePos))
					{
						m_Scene->SetCurrentDialogChoice(i);
					}
				}
			}
		}
	}

	if(key != 0x0 || event.EventType == EET_KEY_INPUT_EVENT)
	{
		ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
		if(key == 0x0)
		{
			isPressed = event.KeyInput.PressedDown;
			key = event.KeyInput.Key;
		}

		if(m_Scene && m_PlayState == PS_GAME)
		{
			if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_UP, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_DOWN, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_LEFT, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_RIGHT, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION1, isPressed);

				if(isPressed && m_Scene->GetCurrentDialogTime() > 0.1f) 
					SkipDialog();
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION2, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE3))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION3, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE4))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION4, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE5))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION5, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE6))
			{
				m_Scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION6, isPressed);
			}
		}

		if(GameDesc::m_CurrentChapter != 0xffffffff)
		{
			if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_PAUSE))
			{
				if(isPressed)
				{
					// 				if(m_PlayState == PS_MENU)
					// 					m_PlayState = PS_GAME;
					// 				else
					if(m_PlayState == PS_GAME)
						m_PlayState = PS_MENU;
				}
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QUICK_MENU))
			{
				if(isPressed)
				{
					/*if(m_PlayState == PS_QUICK_MENU)
					m_PlayState = PS_GAME;
					else */if(m_PlayState == PS_GAME)
						m_PlayState = PS_QUICK_MENU;
				}
			}
		}
	}

	return false;
}

void ScenePanel::SetCurrentDialog(u32 dialogID /*= 0xffffffff*/, bool restoreSounds/* = false*/) 
{
	if(m_Scene)
	{
		m_Scene->SetCurrentDialog((dialogID != 0xffffffff)? m_Scene->GetDialogAtIndex(dialogID) : m_Scene->GetInitialDialog());
		if(restoreSounds && dialogID != 0xffffffff)// not initial dialog : we need to restore first the sounds
		{
			m_Scene->RestoreSounds();
		}
	}
}

void ScenePanel::SkipDialog()
{
	if(m_Scene && m_Scene->GetCurrentDialog())
	{
		float duration = m_Scene->GetCurrentDialog()->GetDialogTotalDuration(true);
		float currentTime = m_Scene->GetCurrentDialogTime();
		if(m_Scene->GetCurrentDialog()->GetNodeType() == DialogNode::DN_AUTO_VALID)
		{
			if(currentTime < 1.0f)
				return;
			duration -= 0.5f;// auto dialogs have an extra 0.5s to leave extra time when skipping (because there is no confirmation to go to the next dialog)
		}
		float dt = duration-currentTime;
		if(dt > 0.0f)
			m_Scene->Step(dt);
	}
}
