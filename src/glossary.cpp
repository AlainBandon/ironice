#include "glossary.h"
#include "engine.h"
#include "utils.h"


#include <stdio.h>
//#include <malloc.h>
#ifdef _IRR_WINDOWS_
#include <direct.h>
#else
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <wchar.h>
#endif

// Glossary

TArray<GlossaryEntry*> Glossary::GetEntriesBySearch(Engine::LanguageSettings language, const core::stringw& search) const
{
	TArray<GlossaryEntry*> entries;
	TArray<core::stringw> searchKeys;
	core::stringw searchCpy = search;
	searchCpy.make_lower().split< TArray<core::stringw> >(searchKeys, L" ");
	for(u32 i = 0; i < m_Entries.size(); ++i)
	{
		GlossaryEntry* entry = m_Entries[i];
		if(searchKeys.empty())
		{
			entries.push_back(entry);
			continue;
		}
		core::stringw keywords =  entry->GetKeyWords(language);
		keywords.make_lower();
		core::stringw entryword =  entry->GetEntryWord(language);
		entryword.make_lower();
		bool reject = false;
		for(u32 j = 0; j < searchKeys.size(); ++j)
		{
			if(keywords.find(searchKeys[j].c_str()) < 0)// one of my typed words is not contained inside the keywords list
			{
				if(entryword.find(searchKeys[j].c_str()) < 0)// one of my typed words is not contained inside the entry name
				{
					reject = true;
					break;
				}
			}
		}

		if(!reject)
			entries.push_back(entry);// add this entry to the result
	}

	// now sort it :
	if(entries.size() > 1)
	{
		for(s32 i = 0; i < (s32)entries.size()-1; ++i)
		{
			for(s32 j = 0; j < (s32)entries.size()-1-i; ++j)
			{
				if(entries[j+1]->GetEntryWord(language) < entries[j]->GetEntryWord(language))// swap them
				{
					core::swap<GlossaryEntry*, GlossaryEntry*>(entries[j+1], entries[j]);
				}
			}
		}
	}

	return entries;
}

#ifndef IRON_ICE_FINAL
bool Glossary::ReadFromFileTxt(const char* path, Engine::LanguageSettings language/* = Engine::LS_FRENCH*/)
{
	if(language >= Engine::LS_MAX)
		return false;

	FILE* file = fopen(path, "r, ccs=UNICODE");
	if (!file)
		return false;

	wchar_t line[2048];
	

	enum TagType{TT_NONE, TT_ENTRY, TT_CONTENT};
	TagType currentTagType = TT_NONE;
	GlossaryEntry* currentEntry = NULL;
	u32 entryCounter = 0;
	u32 descCounter = 0;
	u32 step = 0;
	bool isReadingContent = false;
	core::stringw str1stPart;
	core::stringw str2ndPart;
	core::stringw str = L"";
	while (fgetws(line, 2048, file) != NULL) 
	{
		str += line;
		bool nextLine = false;
		while(!nextLine)
		{
			s32 start = str.findFirstChar(L"[");
			s32 end = str.findFirstChar(L"]");
			if(end >= 0)// found the end : we have a full entry start
			{
				str1stPart = str.subString(start, end-start, true);
				// balise end
				if(str1stPart.find(L"/vocabulary", 0) > 0)
				{
					// safe check
					if(currentEntry->GetKeyWords(language).empty())// if no keyword : set by default the entry word as a keyword
						currentEntry->SetKeyWords(language, currentEntry->GetEntryWord(language));
					currentEntry = NULL;
				}
				else if(str1stPart.find(L"/profile", 0) > 0)
				{
					// safe check
					if(currentEntry->GetKeyWords(language).empty())// if no keyword : set by default the entry word as a keyword
						currentEntry->SetKeyWords(language, currentEntry->GetEntryWord(language));
					currentEntry = NULL;
				}
				else if(str1stPart.find(L"/description", 0) > 0)
				{
					s32 startContent = 0;
					s32 endContent = start;
					// trim one break at start if any
					if (str[startContent] == L'\r') // Mac or Windows breaks
					{
						++startContent;
						if (str[startContent] == L'\n') // Windows breaks
							++startContent;
					}
					else if (str[startContent] == L'\n') // Unix breaks
					{
						++startContent;
					}
					// trim one break at end if any
					if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
					{
						--endContent;
						if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
							--endContent;
					}
					else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
					{
						--endContent;
					}

					if(currentEntry->GetContentList().size() > descCounter)// already have this desc
					{
						currentEntry->GetContentList()[descCounter].SetContent(language, str.subString(startContent, endContent));
					}
					else
					{
						GlossaryDescription desc;
						desc.SetContent(language, str.subString(startContent, endContent));
						currentEntry->AddContent(desc);
					}
					++descCounter;
					isReadingContent = false;
				}
				else if(str1stPart.find(L"/image", 0) > 0)
				{
					assert(currentEntry->GetEntryType() == GlossaryEntry::GET_PROFILE);

					s32 startContent = 0;
					s32 endContent = start;
					// trim one break at start if any
					if (str[startContent] == L'\r') // Mac or Windows breaks
					{
						++startContent;
						if (str[startContent] == L'\n') // Windows breaks
							++startContent;
					}
					else if (str[startContent] == L'\n') // Unix breaks
					{
						++startContent;
					}
					// trim one break at end if any
					if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
					{
						--endContent;
						if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
							--endContent;
					}
					else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
					{
						--endContent;
					}
					
					((GlossaryEntryProfile*)currentEntry)->SetImageDesc(language, str.subString(startContent, endContent));
					
					isReadingContent = false;
				}
				else if(str1stPart.find(L"/keywords", 0) > 0)
				{
					s32 startContent = 0;
					s32 endContent = start;
					// trim one break at start if any
					if (str[startContent] == L'\r') // Mac or Windows breaks
					{
						++startContent;
						if (str[startContent] == L'\n') // Windows breaks
							++startContent;
					}
					else if (str[startContent] == L'\n') // Unix breaks
					{
						++startContent;
					}
					// trim one break at end if any
					if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
					{
						--endContent;
						if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
							--endContent;
					}
					else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
					{
						--endContent;
					}

					currentEntry->SetKeyWords(language, str.subString(startContent, endContent));

					isReadingContent = false;
				}
				// balise start
				else if(str1stPart.find(L"vocabulary", 0) > 0)
				{
					assert(!currentEntry);
					if(m_Entries.size() > entryCounter)// already have this entry
					{
						currentEntry = m_Entries[entryCounter];
						assert(currentEntry->GetEntryType() == GlossaryEntry::GET_VOCABULARY);
					}
					else
					{
						currentEntry = new GlossaryEntryVocabulary(entryCounter);
						AddContent(currentEntry);// add the new vocabulary entry
					}
					++entryCounter;
					descCounter = 0;

					if(Get2ndPart(str, start, end, str2ndPart))
					{
						currentEntry->SetEntryWord(language, str2ndPart);
					}
					else
					{
						assert(0);// we must have a keyword
					}
				}
				else if(str1stPart.find(L"profile", 0) > 0)
				{
					assert(!currentEntry);
					if(m_Entries.size() > entryCounter)// already have this entry
					{
						currentEntry = m_Entries[entryCounter];
						assert(currentEntry->GetEntryType() == GlossaryEntry::GET_PROFILE);
					}
					else
					{
						currentEntry = new GlossaryEntryProfile(entryCounter);
						AddContent(currentEntry);// add the new profile entry
					}
					++entryCounter;
					descCounter = 0;

					if(Get2ndPart(str, start, end, str2ndPart))
					{
						currentEntry->SetEntryWord(language, str2ndPart);
					}
					else
					{
						assert(0);// we must have a keyword
					}
				}
				else if(str1stPart.find(L"description", 0) > 0)
				{
					assert(!isReadingContent);
					if(Get2ndPart(str, start, end, str2ndPart))// optionnal header
					{
						if(currentEntry->GetContentList().size() > descCounter)// already have this header
						{
							currentEntry->GetContentList()[descCounter].SetHeader(language, str2ndPart);
						}
						else
						{
							GlossaryDescription desc;
							desc.SetHeader(language, str2ndPart);
							currentEntry->AddContent(desc);
						}
					}
					isReadingContent = true;
				}
				else if(str1stPart.find(L"image", 0) > 0)
				{
					assert(!isReadingContent);
					assert(currentEntry->GetEntryType() == GlossaryEntry::GET_PROFILE);// only possible on profile
					if(Get2ndPart(str, start, end, str2ndPart))
					{
						if(!((GlossaryEntryProfile*)currentEntry)->GetImage())
						{
							Texture* image = NULL;
							core::stringc path = Engine::GetInstance()->GetGamePath("data/glossary/");
							path += str2ndPart;
							SAFE_LOAD_IMAGE(Engine::GetInstance()->GetDriver(), image, path, true);
							((GlossaryEntryProfile*)currentEntry)->SetImage(image);
						}
					}
					else
					{
						assert(0);// we must have a second part : image path
					}
					isReadingContent = true;
				}
				else if(str1stPart.find(L"keywords", 0) > 0)
				{
					assert(!isReadingContent);
					isReadingContent = true;
				}

				str = str.subString(end+1, str.size()-(end+1));// next iteration
			}
			else if(start >= 0 || isReadingContent) // not completed line : wait for adding next line to the previous result
			{
				nextLine = true;
			}
			else// not a content : discard
			{
				str = L"";
				nextLine = true;
			}
		}
	}
	
	fclose(file);

	return true;
}
#endif

bool Glossary::ReadFromFile(const char* path)
{
	for(u32 i = 0; i < m_Entries.size(); ++i) 
		SafeDelete(m_Entries[i]);
	m_Entries.clear();

	FILE* input = fopen(path, "rb");
	if (!input)
		return false;

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "GLOS", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	// read nb languages supported
	u32 nbLang = 0;
	fread(&nbLang, sizeof(u32), 1, input);

	// read nb entries
	u32 nbEntries = 0;
	fread(&nbEntries, sizeof(u32), 1, input);
	for(u32 i = 0; i < nbEntries; ++i)
	{
		// read entry type
		u8 type = 0;
		fread(&type, sizeof(u8), 1, input);

		GlossaryEntry* entry = NULL;
		if(type == GlossaryEntry::GET_VOCABULARY)
			entry = new GlossaryEntryVocabulary(i);
		else if(type == GlossaryEntry::GET_PROFILE)
			entry = new GlossaryEntryProfile(i);
		else
		{
			assert(0);
			return false;
		}
		AddContent(entry);

		// read entryword
		for(u32 k = 0; k < nbLang; ++k)
		{
			core::stringw str;
			DeserializeStringW(str, input);
			entry->SetEntryWord((Engine::LanguageSettings)k, str);
		}

		// read keywords
		for(u32 k = 0; k < nbLang; ++k)
		{
			core::stringw str;
			DeserializeStringW(str, input);
			entry->SetKeyWords((Engine::LanguageSettings)k, str);
		}

		u32 nbDescs = 0;
		fread(&nbDescs, sizeof(u32), 1, input);
		for(u32 j = 0; j < nbDescs; ++j)
		{
			GlossaryDescription desc;

			// read headers
			for(u32 k = 0; k < nbLang; ++k)
			{
				core::stringw str;
				DeserializeStringW(str, input);
				desc.SetHeader((Engine::LanguageSettings)k, str);
			}

			// read contents
			for(u32 k = 0; k < nbLang; ++k)
			{
				core::stringw str;
				DeserializeStringW(str, input);
				desc.SetContent((Engine::LanguageSettings)k, str);
			}

			entry->AddContent(desc);
		}

		if(type == GlossaryEntry::GET_PROFILE)
		{
			// read image
			u32 nbImage = 0;
			fread(&nbImage, sizeof(u32), 1, input);
			if(nbImage > 0)
			{
				Dimension2 dim(0,0);
				fread(&(dim.Width), sizeof(u32), 1, input);// width
				fread(&(dim.Height), sizeof(u32), 1, input);// height
				u32 size = 0;
				fread(&size, sizeof(u32), 1, input);// non compressed size
				assert(size == 4*dim.Width*dim.Height);
				if(size != 4*dim.Width*dim.Height)
					return false;

				void* data = malloc(size);

				u32 compressedSize = 0;
				fread(&compressedSize, sizeof(u32), 1, input);// compressed size
				void* compressedData = malloc(compressedSize);
				fread(compressedData, sizeof(unsigned char), compressedSize, input);// compressed data
				int ret = RLEDecompressColors((unsigned char*)compressedData, compressedSize, (unsigned char*)data, size);
				free(compressedData);

				Image* img = Engine::GetInstance()->GetDriver()->createImageFromData(video::ECF_A8R8G8B8, dim, data);
				free(data);
				
				Texture* tex = ImageToTexture(img, L"texture");
				if(tex)
				{
					tex->grab();
					Engine::GetInstance()->GetDriver()->removeTexture(tex);
					if(USE_PREMULTIPLIED_ALPHA)
						PremultiplyAlpha(tex);
				}
				img->drop();
				
				((GlossaryEntryProfile*)entry)->SetImage(tex);
			}

			// read image desc
			for(u32 k = 0; k < nbLang; ++k)
			{
				core::stringw str;
				DeserializeStringW(str, input);
				((GlossaryEntryProfile*)entry)->SetImageDesc((Engine::LanguageSettings)k, str);
			}
		}
	}

	fclose(input);

	return true;
}

#ifndef IRON_ICE_FINAL
bool Glossary::WriteToFile(const char* path)
{
	FILE* output = fopen(path, "wb");
	if (!output)
		return false;

	char header[5] = "GLOS";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	// write nb languages supported
	u32 nbLang = Engine::LS_MAX;
	fwrite(&nbLang, sizeof(u32), 1, output);

	// write nb entries
	u32 nbEntries = m_Entries.size();
	fwrite(&nbEntries, sizeof(u32), 1, output);
	for(u32 i = 0; i < nbEntries; ++i)
	{
		GlossaryEntry* entry = m_Entries[i];
		// write entry type
		u8 type = (u8)entry->GetEntryType();
		fwrite(&type, sizeof(u8), 1, output);

		// write entry word
		for(u32 k = 0; k < nbLang; ++k)
		{
			SerializeStringW(entry->GetEntryWord((Engine::LanguageSettings)k), output);
		}

		// write keywords
		for(u32 k = 0; k < nbLang; ++k)
		{
			SerializeStringW(entry->GetKeyWords((Engine::LanguageSettings)k), output);
		}

		u32 nbDescs = entry->GetContentList().size();
		fwrite(&nbDescs, sizeof(u32), 1, output);
		for(u32 j = 0; j < nbDescs; ++j)
		{
			const GlossaryDescription& desc = entry->GetContentList()[j];

			// write headers
			for(u32 k = 0; k < nbLang; ++k)
			{
				SerializeStringW(desc.GetHeader((Engine::LanguageSettings)k), output);
			}

			// write contents
			for(u32 k = 0; k < nbLang; ++k)
			{
				SerializeStringW(desc.GetContent((Engine::LanguageSettings)k), output);
			}
		}

		if(type == GlossaryEntry::GET_PROFILE)
		{
			u32 nbImage = (((GlossaryEntryProfile*)entry)->GetImage() == NULL)?0:1;
			fwrite(&nbImage, sizeof(u32), 1, output);
			if(nbImage > 0)
			{
				Image* img = TextureToImage(((GlossaryEntryProfile*)entry)->GetImage());
				u32 bpp = img->getBytesPerPixel();
				Dimension2 dim = img->getDimension();
				u32 size = dim.Width*dim.Height*bpp;
				assert(bpp == 4);
				void* data = img->lock();
				void* compressedData = malloc(size+4*sizeof(int));
				void* compressedDataOffset = compressedData;
				u32 compressedSizeTotal = 0;
				u32 compressedSize = 0;
				PreProcessAlphaPixels((unsigned char*)data, size);
				for(u32 i=0; i<4; ++i)// for each color
				{
					int ret = RLECompressColor((unsigned char*)(data)+i, size, (unsigned char*)(compressedDataOffset) + sizeof(int), &compressedSize);
					assert(ret != -1 && compressedSize <= dim.Width*dim.Height);
					assert(ret == 1 || compressedSize == dim.Width*dim.Height);
					*((int*)compressedDataOffset) = compressedSize;
					compressedSizeTotal += compressedSize + sizeof(int);
					compressedDataOffset = (void*)((unsigned char*)compressedData + compressedSizeTotal);
				}
				assert(compressedSizeTotal <= size+4*sizeof(int));
				img->unlock();
				img->drop();

				fwrite(&(dim.Width), sizeof(u32), 1, output);// width
				fwrite(&(dim.Height), sizeof(u32), 1, output);// height
				fwrite(&size, sizeof(u32), 1, output);// non compressed size
				fwrite(&compressedSizeTotal, sizeof(u32), 1, output);// compressed size

				fwrite(compressedData, sizeof(unsigned char), compressedSizeTotal, output);// data (compressed or not)
				free(compressedData);
			}

			// write image desc
			for(u32 k = 0; k < nbLang; ++k)
			{
				SerializeStringW(((GlossaryEntryProfile*)entry)->GetImageDesc((Engine::LanguageSettings)k), output);
			}
		}
	}

	fclose(output);

	return true;
}
#endif

