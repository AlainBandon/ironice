﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

enum StringDesc {SD_EXIT_POPUP = Engine::SFTID_ExitSubMenuPanel, SD_MAX};

// ExitSubMenuPanel
ExitSubMenuPanel::ExitSubMenuPanel() : IPanel()
, m_PopupPanel(NULL)
, m_ParentPanel(NULL)
{
	bool useRT = true;
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	if(useRT && driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		m_RenderTarget = driver->addRenderTargetTexture(Engine::GetInstance()->m_RenderSize, "RTT_Exit", video::ECF_A8R8G8B8);
	}
	else
	{
		m_RenderTarget = NULL;
	}
}

ExitSubMenuPanel::~ExitSubMenuPanel()
{
	UnloadPanel();
}

void ExitSubMenuPanel::LoadPanel() 
{
	if(!m_PopupPanel)
	{
		m_PopupPanel = new PopupPanel(m_RenderTarget || USE_PREMULTIPLIED_ALPHA);
		m_PopupPanel->LoadPanel();
	}

	OnResize();

	//StartFade(PANEL_FADETIME);

	IPanel::LoadPanel();
}

void ExitSubMenuPanel::UnloadPanel() 
{
	if(m_PopupPanel)
		m_PopupPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void ExitSubMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;


	if(m_RenderTarget && renderSize != m_RenderTarget->getSize())
		m_RenderTarget = driver->addRenderTargetTexture(renderSize, "RTT_Exit", video::ECF_A8R8G8B8);

	if(m_PopupPanel)
	{
		m_PopupPanel->OnResize();
	}
}

void ExitSubMenuPanel::Update(float dt) 
{
	IPanel::Update(dt);

	
	if(m_PopupPanel)
	{
		m_PopupPanel->Update(dt);

		// restart the popup on panel fade in if not already started
		if(!m_PopupPanel->IsVisible())
		{
			if(IsFading() && m_Fader.FadeType == Fader::FT_FADEIN)
			{
				m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_EXIT_POPUP));
			}
		}
	}
}

void ExitSubMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor off = IsFading()? 
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(200, 0, 0, 0), 4.0f*m_Fader.GetRatio()-3.0f) 
		: IColor(200, 0, 0, 0);
	IColor on = COLOR_TRANSPARENT;
	float ratio = m_Fader.GetRealRatio();
	IColor onToOff = MathUtils::GetInterpolatedColor(off, on, ratio);
	IColor offToOn = MathUtils::GetInterpolatedColor(on, off, ratio);
	Rectangle2 rect(innerOffset, Dimension2((u32)(innerScale*originalSize.Width), (u32)(innerScale*originalSize.Height)));
	
	fill2DRect(driver, rect, off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
}

void ExitSubMenuPanel::_Render(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	RenderFader(scale, offset);

	
	if(m_PopupPanel)
	{
		m_PopupPanel->Render();
	}

}

void ExitSubMenuPanel::PreRender()
{
	// render here only if we have a render target : we render in the render target
	if(m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		// set render target texture
		IColor color = COLOR_TRANSPARENT;
		color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(m_RenderTarget, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		_Render();

		// set back old render target
		// The buffer might have been distorted, so clear it
		color = COLOR_TRANSPARENT;
		if(USE_PREMULTIPLIED_ALPHA)
			color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(0, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

void ExitSubMenuPanel::Render()
{
	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)), 
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_Render(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)*Engine::GetInstance()->m_InnerScale), 0);

		IColor tint = COLOR_WHITE;

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((Engine::GetInstance()->m_RenderSize.Width>>1), 
			(Engine::GetInstance()->m_RenderSize.Height>>1));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

bool ExitSubMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if(m_PopupPanel)
	{
		if(m_PopupPanel->OnEvent(event))
		{
			if(!m_PopupPanel->IsVisible())// this means that the popup just closed
			{
				if(m_PopupPanel->GetResult() == PopupPanel::PRT_YES)
				{
					Engine::GetInstance()->Exit();
					return true;
				}
				else
				{
					m_ParentPanel->ForceDefaultSelection();
					return true;
				}
			}
		}
	}

// 	if (event.EventType == EET_MOUSE_INPUT_EVENT)
// 	{
// 		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
// 		{
// 			if(m_PopupPanel)
// 				m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_EXIT_POPUP));
// 		}
// 	}
// 	else if (event.EventType == EET_KEY_INPUT_EVENT)
// 	{
// 		//if(m_SelectionFadeTimer == 0.0f)
// 		if(event.KeyInput.PressedDown)
// 		{
// 			if(event.KeyInput.Key == KEY_RETURN || event.KeyInput.Key == KEY_UP)
// 			{
// 				if(m_PopupPanel)
// 					m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_EXIT_POPUP));
// 			}
// 		}
// 	}

	return (m_PopupPanel && m_PopupPanel->IsVisible());// bypass the main panel events when the popup is active
}

#undef PANEL_FADETIME
