#include "audio.h"


IIAudioDevice::IIAudioDevice() : m_IsPaused(false)
{
	m_AudioDevice = OpenDevice("autodetect");// default device
	if(!m_AudioDevice.get())
	{
        printf("No sound device available : using null device.");
        m_AudioDevice = OpenDevice("null");
    }
	assert(m_AudioDevice.get());
}

IIAudioDevice::~IIAudioDevice()
{
	for(u32 i = 0; i < m_Streams.size(); ++i)
	{
		m_Streams[i]->Stop();
	}

	m_Streams.clear();
}

SmartPointer<AudioStream> IIAudioDevice::OpenSoundFile(const char* filename, bool stream /*= true*/)
{
	SmartPointer<AudioStream> audio(new AudioStream());
	audio->m_StreamPtr = OpenSound(m_AudioDevice, filename, stream);

	m_Streams.push_back(audio);

	return audio;
}

bool IIAudioDevice::CloseSound(const SmartPointer<AudioStream>& sound)
{
	s32 index = m_Streams.linear_search(sound);
	if(index >= 0)
	{
		m_Streams.erase(index);
		return true;
	}
	return false;
}

bool IIAudioDevice::SetSoundVolume(const SmartPointer<AudioStream>& sound, float volume)
{
	s32 index = m_Streams.linear_search(sound);
	if(index >= 0)
	{
		SmartPointer<AudioStream> as = m_Streams[index];
		if(as.IsValid() && as->IsValid())
		{
			as->m_StreamPtr->setVolume(volume);

			return true;
		}
	}
	return false;
}

bool IIAudioDevice::SetupSound(const SmartPointer<AudioStream>& sound, 
							 float pan, float pitch, float volume, bool repeat/* = false*/, bool stopAtDialogsEnd/* = true*/)
{
	s32 index = m_Streams.linear_search(sound);
	if(index >= 0)
	{
		SmartPointer<AudioStream> as = m_Streams[index];
		if(as.IsValid() && as->IsValid())
		{
			as->SetPan(pan);
			as->SetPitch(pitch);
			as->SetVolume(volume);
			as->SetRepeat(repeat);
			as->SetStopAtEnd(stopAtDialogsEnd);

			return true;
		}
	}
	return false;
}

void IIAudioDevice::SetPaused(bool state)
{
	if(m_IsPaused != state)
	{
		if(state)
		{
			for(u32 i = 0; i < m_Streams.size(); ++i)
			{
				if(m_Streams[i]->IsValid())
					m_Streams[i]->Pause();
			}
		}
		else
		{
			for(u32 i = 0; i < m_Streams.size(); ++i)
			{
				if(m_Streams[i]->IsValid())
					m_Streams[i]->Resume();
			}
		}
	}
	m_IsPaused = state;
}

void IIAudioDevice::Update(float dt)
{
	static bool removeSoundsWhenUnreferencedElseWhere = true;
	if(m_AudioDevice.get())
		m_AudioDevice->update();

	for(u32 i = 0; i < m_Streams.size(); ++i)
	{
		if(removeSoundsWhenUnreferencedElseWhere && m_Streams[i].GetRefCount() <= 1)// remove unreferenced elsewhere streams
		{
			m_Streams.erase(i);
			--i;
			continue;
		}
// 		assert(m_Streams[i]->m_StreamPtr);
// 		if(m_Streams[i]->IsPlaying())
// 			m_Streams[i]->Play();
	}
}
