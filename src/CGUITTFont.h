#ifndef _GUI_FREETYPE_FONT_H
#define _GUI_FREETYPE_FONT_H

//! freetype support enabled with 1 and disabled with 0
#define COMPILE_WITH_FREETYPE 1

#define FONT_TEX_SIZE 512
#define DEFAULT_GAUSS_SIZE 10


#if COMPILE_WITH_FREETYPE

#include <ft2build.h>
#include <freetype/freetype.h>
#include <irrlicht.h>

class CGUITTFace : public irr::IReferenceCounted
{
public:
	CGUITTFace();
	virtual ~CGUITTFace();

	bool load(const irr::io::path& filename);

	FT_Face		face;               // handle to face

private:
	static int			countClassObjects;
	static FT_Library	library;    // handle to library
};

class CGUIFreetypeFont;

class CGUITTGlyph : public irr::IReferenceCounted
{
public:
	CGUITTGlyph();
	virtual ~CGUITTGlyph();

	bool cached;
	void cache(irr::u32 idx_, const CGUIFreetypeFont * freetypeFont, bool usePremultipliedAlpha);

	irr::u32 size;
	irr::s32 top;
	irr::s32 left;
	irr::u32 texw;
	irr::u32 texh;
	irr::u32 decalX;
	irr::u32 texIdx;
	irr::core::recti srcRect;
	irr::u32 texIdxGauss;
	irr::core::recti srcRectGauss;

private:
	
};

class CGUIFreetypeFont : public irr::gui::IGUIFont
{
	friend class CGUITTGlyph;

public:

	struct ColorElement
	{
		ColorElement(irr::video::SColor color, irr::u32 size) : Color(color), Size(size) {}
		~ColorElement() {}

		irr::video::SColor Color;
		irr::u32 Size;
	};

	//! constructor
	CGUIFreetypeFont(irr::video::IVideoDriver* Driver);

	//! destructor
	virtual ~CGUIFreetypeFont();

	//! loads a truetype font file
	bool attach(CGUITTFace *Face,irr::u32 size,bool usePremultipliedAlpha = false,irr::u32 gaussLevel = 0);

	//! set the size of the font
	bool setSize(irr::u32 size, irr::u32 gaussLevel = 0);

	//! set the size of the font
	bool setGaussLevel(irr::u32 gaussLevel);

	//! get the size of the font
	irr::u32 getSize() const {return Size;}

	//! draws an text and clips it to the specified rectangle if wanted
	virtual void draw(const irr::core::stringw& textstring, const irr::core::rect<irr::s32>& position, 
		irr::video::SColor color, bool hcenter=false, bool vcenter=false, const irr::core::rect<irr::s32>* clip=0);
	
	void draw(const irr::core::stringw& textstring, const irr::core::rect<irr::s32>& position, 
		irr::video::SColor color, float scale = 1.0f, bool hcenter=false, bool vcenter=false, 
		irr::video::SColor backgroundColor = irr::video::SColor(0,0,0,0), const irr::core::rect<irr::s32>* clip=0);

	void draw(const irr::core::stringw& textstring, const irr::core::rect<irr::s32>& position, 
		const irr::core::array<ColorElement>& colorElements, float scale = 1.0f, bool hcenter=false, bool vcenter=false, 
		irr::video::SColor backgroundColor = irr::video::SColor(0,0,0,0), const irr::core::rect<irr::s32>* clip=0);
	
	void drawGauss(const irr::core::stringw& textstring, const irr::core::rect<irr::s32>& position, 
		irr::video::SColor color, bool hcenter=false, bool vcenter=false, const irr::core::rect<irr::s32>* clip=0);
	
	void drawGauss(const irr::core::stringw& textstring, const irr::core::rect<irr::s32>& position, 
		irr::video::SColor color, float scale = 1.0f, bool hcenter=false, bool vcenter=false, 
		irr::video::SColor backgroundColor = irr::video::SColor(0,0,0,0), const irr::core::rect<irr::s32>* clip=0);

	//! returns the dimension of a text
	virtual irr::core::dimension2d<irr::u32> getDimension(const wchar_t* text) const;
	irr::core::dimension2d<irr::u32> getDimension(const wchar_t* text, irr::core::array<irr::u32>& outLinesWidth) const;
	irr::core::rect<irr::s32> getSubStringRect(const wchar_t* text, irr::u32 start, irr::u32 size, irr::core::array<irr::u32>& outLinesWidth) const;

	//! Calculates the index of the character in the text which is on a specific position.
	virtual irr::s32 getCharacterFromPos(const wchar_t* text, irr::s32 pixel_x) const;

	//! Not yet supported
	virtual void setKerningWidth (irr::s32 kerning) {}

	//! Not yet supported
	virtual void setKerningHeight (irr::s32 kerning) {}

	//! Not yet supported
	virtual irr::s32 getKerningWidth(const wchar_t* thisLetter=0, const wchar_t* previousLetter=0)	const { return 0; }

	//! Not yet supported
	virtual irr::s32 getKerningHeight()	const { return 0; }

	//! Not yet supported
	virtual void setInvisibleCharacters( const wchar_t *s ) {}

	const irr::core::array<irr::video::ITexture*>& GetTexturePool() const {return TexturePool;}
	const irr::core::array<irr::video::ITexture*>& GetTexturePoolGauss() const {return TexturePoolGauss;}

	irr::u32 getWidthFromCharacter(wchar_t c) const;


	bool AntiAlias;
	bool Transparency;

protected:
	void clearGlyphs();

private:
	irr::u32 getGlyphByChar(wchar_t c) const;
	irr::video::IVideoDriver* Driver;
	irr::core::array< CGUITTGlyph* > Glyphs;
	CGUITTFace * TrueTypeFace;
	mutable irr::core::dimension2d<irr::u32> LargestGlyph;

	irr::u32 cacheNewGlyph(const irr::u32* const bits, irr::u32 width, irr::u32 height, irr::core::recti& outSrcRect) const;
	irr::u32 cacheNewGlyphGauss(const irr::u32* const bits, irr::u32 width, irr::u32 height, irr::core::recti& outSrcRect) const;
	mutable irr::core::array<irr::video::ITexture*> TexturePool;
	mutable irr::core::vector2di CurrentTextureOffset;
	mutable irr::u32 CurrentTextureLineHeight;

	mutable irr::core::array<irr::video::ITexture*> TexturePoolGauss;
	mutable irr::core::vector2di CurrentTextureGaussOffset;
	mutable irr::u32 CurrentTextureGaussLineHeight;
	mutable irr::core::array< irr::core::array<float> > GaussMatrix;

	irr::u32 Size;

	bool UsePremultipliedAlpha;
	irr::u32 GaussLevel;

	void setGlyphTextureFlags() const;
	void restoreTextureFlags() const;

	static bool mTexFlag16;
	static bool mTexFlag32;
	static bool mTexFlagMip;
};

#endif // #if COMPILE_WITH_FREETYPE

#endif	// _GUI_FREETYPE_FONT_H

