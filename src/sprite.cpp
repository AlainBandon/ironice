#include "sprite.h"
#include "engine.h"
#include "utils.h"


#include <stdio.h>
//#include <malloc.h>

// LayerSprite
LayerSprite::LayerSprite() :
m_Image(NULL),
m_DestSize(D2Zero),
m_DestPosition(P2Zero),
m_SrcSize(D2Zero),
m_SrcPosition(P2Zero),
m_Rotation(0.0f)
{

}

LayerSprite::LayerSprite(LayerSprite* layer) :
m_Image(NULL),
m_DestSize(D2Zero),
m_DestPosition(P2Zero),
m_SrcSize(D2Zero),
m_SrcPosition(P2Zero),
m_Rotation(0.0f)
{
	if(layer)
	{
		m_Image = layer->GetImage();
		m_DestPosition = layer->GetDestPosition();
		m_DestSize = layer->GetDestSize();
		m_SrcPosition = layer->GetSrcPosition();
		m_SrcSize = layer->GetSrcSize();
		m_Rotation = layer->GetRotation();
	}
}

LayerSprite::~LayerSprite()
{
	m_Image = NULL;
}

void LayerSprite::SetImage(Texture* image) 
{
	if(image && m_Image != image)
	{
		m_DestPosition = P2Zero;
		m_DestSize = image->getSize();

		m_SrcPosition = P2Zero;
		m_SrcSize = image->getSize();

		m_Rotation = 0.0f;
	}
	m_Image = image;
}

// void LayerSprite::Render(const Position2& pos, float scale/* = 1.0f*/, const IColor& tint /*= WHITE*/)
// {
// 	if(!m_Image)
// 		return;
// 
// 	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
// 
// 	Rectangle2 sourceRect(m_SrcPosition, m_SrcSize);
// 	
// 	core::matrix4 matParent = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) * 
// 		core::matrix4().setScale(core::vector3df(scale, scale, 0.0f));
// 
// 	core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_DestPosition.X, (f32)m_DestPosition.Y, 0.0f)) *
// 		core::matrix4().setRotationAxisRadians(m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
// 		core::matrix4().setScale(core::vector3df((f32)m_DestSize.Width/m_SrcSize.Width, (f32)m_DestSize.Height/m_SrcSize.Height, 0.0f));
// 
// 	core::matrix4 finalMatrix(matParent*mat);
// 
// 	draw2DImage(driver, m_Image, sourceRect, finalMatrix, true, tint, true, USE_PREMULTIPLIED_ALPHA);
// }

void LayerSprite::Render(const core::matrix4& matrix, const IColor& tint /*= WHITE*/, bool mirrorH/* = false*/, bool mirrorV/* = false*/, core::rect<irr::s32>* clip/* = NULL*/)
{
	if(!m_Image)
		return;

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Rectangle2 sourceRect(m_SrcPosition, m_SrcSize);

	core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_DestPosition.X, (f32)m_DestPosition.Y, 0.0f)) *
		core::matrix4().setRotationAxisRadians(m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
		core::matrix4().setScale(core::vector3df((f32)m_DestSize.Width/m_SrcSize.Width, (f32)m_DestSize.Height/m_SrcSize.Height, 0.0f));

	if(mirrorH || mirrorV)
		mat = core::matrix4().setScale(core::vector3df(mirrorH? -1.0f : 1.0f, mirrorV? -1.0f : 1.0f, 0.0f)) * mat;
	
	core::matrix4 finalMatrix(matrix*mat);

	draw2DImage(driver, m_Image, sourceRect, finalMatrix, true, tint, true, USE_PREMULTIPLIED_ALPHA, clip);
}

// void LayerSprite::RenderDebugInfo(const Position2& pos, float scale/* = 1.0f*/)
// {
// 	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
// 	core::matrix4 matrix = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) * 
// 		core::matrix4().setScale(core::vector3df(scale, scale, 0.0f));
// 
// 	core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_DestPosition.X, (f32)m_DestPosition.Y, 0.0f)) *
// 		core::matrix4().setRotationAxisRadians(m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
// 		core::matrix4().setScale(core::vector3df((f32)m_DestSize.Width/m_SrcSize.Width, (f32)m_DestSize.Height/m_SrcSize.Height, 0.0f));
// 
// 	core::matrix4 finalMatrix(matrix*mat);
// 
// 	draw2DRect(driver, m_SrcSize, finalMatrix, IColor(255, 255, 0, 0));
// }

void LayerSprite::RenderDebugInfo(const core::matrix4& matrix, bool mirrorH/* = false*/, bool mirrorV/* = false*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_DestPosition.X, (f32)m_DestPosition.Y, 0.0f)) *
		core::matrix4().setRotationAxisRadians(m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
		core::matrix4().setScale(core::vector3df((f32)m_DestSize.Width/m_SrcSize.Width, (f32)m_DestSize.Height/m_SrcSize.Height, 0.0f));

	core::matrix4 finalMatrix(matrix*mat);

	draw2DRect(driver, m_SrcSize, finalMatrix, IColor(255, 255, 0, 0));
}


// FrameSprite
FrameSprite::FrameSprite():
m_Size(D2Zero)
{

}

FrameSprite::FrameSprite(FrameSprite* frame):
m_Size(D2Zero)
{
	if(frame)
	{
		m_Size = frame->GetSize();

		const TArray<LayerSprite*>& layers = frame->GetLayers();
		for(u32 i=0; i<layers.size(); ++i)
		{
			m_Layers.push_back(new LayerSprite(layers[i]));
		}
	}
}

FrameSprite::~FrameSprite()
{
	for(u32 i=0; i<m_Layers.size(); ++i)
	{
		SafeDelete(m_Layers[i]);// delete content
	}
	m_Layers.clear();// clear array
}

bool FrameSprite::AddLayer(LayerSprite* layer)// add a layer to the frame
{
	if(layer)
	{
		m_Layers.push_back(layer);
		return true;
	}
	return false;
}


// void FrameSprite::Render(const Position2& pos, float scale/* = 1.0f*/, const IColor& tint /*= WHITE*/)
// {
// 	if(m_Layers.empty())
// 		return;
// 
// 	for(u32 i = 0; i < m_Layers.size(); ++i)
// 	{
// 		LayerSprite* layer = m_Layers[i];
// 		if(layer)
// 		{
// 			layer->Render(pos, scale, tint);
// 		}
// 	}
// }

void FrameSprite::Render(const core::matrix4& matrix, const IColor& tint /*= WHITE*/, bool mirrorH/* = false*/, bool mirrorV/* = false*/, core::rect<irr::s32>* clip/* = NULL*/)
{
	if(m_Layers.empty())
		return;

	for(u32 i = 0; i < m_Layers.size(); ++i)
	{
		LayerSprite* layer = m_Layers[i];
		if(layer)
		{
			layer->Render(matrix, tint, mirrorH, mirrorV, clip);
		}
	}
}

// void FrameSprite::RenderDebugInfo(const Position2& pos, float scale /*= 1.0f*/, bool recursive/* = false*/)
// {
// 	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
// 	core::matrix4 matrix = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) * 
// 		core::matrix4().setScale(core::vector3df(scale, scale, 0.0f));
// 
// 	draw2DRect(driver, m_Size, matrix, IColor(255, 0, 0, 255));
// 
// 	if(recursive)
// 	{
// 		for(u32 i = 0; i < m_Layers.size(); ++i)
// 		{
// 			LayerSprite* layer = m_Layers[i];
// 			if(layer)
// 			{
// 				layer->RenderDebugInfo(pos, scale);
// 			}
// 		}
// 	}
// }

void FrameSprite::RenderDebugInfo(const core::matrix4& matrix, bool mirrorH/* = false*/, bool mirrorV/* = false*/, bool recursive/* = false*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	core::matrix4 finalMatrix(matrix);

	draw2DRect(driver, m_Size, finalMatrix, IColor(255, 0, 0, 255));

	if(recursive)
	{
		for(u32 i = 0; i < m_Layers.size(); ++i)
		{
			LayerSprite* layer = m_Layers[i];
			if(layer)
			{
				layer->RenderDebugInfo(matrix, mirrorH, mirrorV);
			}
		}
	}
}



// AnimatedSprite
AnimatedSprite::AnimatedSprite(): 
m_Size(D2Zero),
m_Framerate(1.0f/12.0f)
{
	m_Size = Dimension2(256,256);
}

AnimatedSprite::~AnimatedSprite()
{
	for(u32 i=0; i<m_Frames.size(); ++i)
	{
		SafeDelete(m_Frames[i]);// delete content
	}
	m_Frames.clear();// clear array

	for(u32 i=0; i<m_RawDataImages.size(); ++i)
	{
 		if(m_RawDataImages[i])
 			Engine::GetInstance()->GetDriver()->removeTexture(m_RawDataImages[i]);
	}
	m_RawDataImages.clear();// clear array
}

bool AnimatedSprite::LoadSpriteFromFile(char* path)// load sprite from a .spr file
{
	FILE* input = fopen(path, "rb");
	if(!input)
	{
		return false;
	}

	if(!m_RawDataImages.empty() || !m_Frames.empty())
	{
		fclose(input);
		return false;
	}

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "ASPR", 4) != 0)
	{
		fclose(input);
		return false;
	}

	// current version 1.2
	unsigned char latestVersion[2] = {1,2};// major version, minor version

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	// read raw images bank
	u32 bankSize = 0;
	fread(&bankSize, sizeof(u32), 1, input);// size of bank
	for(u32 i = 0; i < bankSize; ++i)
	{
		Image* img = NULL;
		if(version[0] > 1 || (version[0] == 1 && version[1] >= 2))// from version 1.2
		{
			u32 size = 0;
			fread(&size, sizeof(u32), 1, input);// compressed size
			unsigned char* buffer = new unsigned char[size];
			fread(buffer, sizeof(unsigned char), size, input);// compressed data

			io::IReadFile* memFile  = Engine::GetInstance()->GetDevice()->getFileSystem()->createMemoryReadFile(buffer, size, "foo.png", false);
			if(memFile)
			{
				img = driver->createImageFromFile(memFile);

				memFile->drop();
			}
			else
			{
				assert(0);
			}

			delete[] buffer;
		}
		else
		{
			Dimension2 dim(0,0);
			fread(&(dim.Width), sizeof(u32), 1, input);// width
			fread(&(dim.Height), sizeof(u32), 1, input);// height
			u32 size = 0;
			fread(&size, sizeof(u32), 1, input);// non compressed size
			assert(size == 4*dim.Width*dim.Height);
			if(size != 4*dim.Width*dim.Height)
				return false;

			void* data = malloc(size);

			u32 compressedSize = 0;
			fread(&compressedSize, sizeof(u32), 1, input);// compressed size
			void* compressedData = malloc(compressedSize);
			fread(compressedData, sizeof(unsigned char), compressedSize, input);// compressed data
			int ret = RLEDecompressColors((unsigned char*)compressedData, compressedSize, (unsigned char*)data, size);
			free(compressedData);

			img = driver->createImageFromData(video::ECF_A8R8G8B8, dim, data);
			free(data);
		}
		Texture* tex = ImageToTexture(img, L"texture");
		if(USE_PREMULTIPLIED_ALPHA)
			PremultiplyAlpha(tex);
		img->drop();
		m_RawDataImages.push_back(tex);
	}


	// read frames
	fread(&m_Framerate, sizeof(f32), 1, input);// framerate
	fread(&(m_Size.Width), sizeof(u32), 1, input);// width
	fread(&(m_Size.Height), sizeof(u32), 1, input);// height

	u32 nbFrames = 0;
	fread(&nbFrames, sizeof(u32), 1, input);// nb frames
	for(u32 i = 0; i < nbFrames; ++i)
	{
		FrameSprite* frame = new FrameSprite();
		if(frame)
		{
			Dimension2 frameSize(0,0);
			fread(&(frameSize.Width), sizeof(u32), 1, input);// frame width
			fread(&(frameSize.Height), sizeof(u32), 1, input);// frame height
			frame->SetSize(frameSize);

			// write layers
			u32 nbLayers = 0;
			fread(&nbLayers, sizeof(u32), 1, input);// nb layers
			for(u32 j = 0; j < nbLayers; ++j)
			{
				LayerSprite* layer = new LayerSprite();
				if(layer)
				{
					s32 index = -1;
					fread(&index, sizeof(s32), 1, input);// index
					if(index >= 0 && (u32)index < bankSize)
						layer->SetImage(m_RawDataImages[index]);

					if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))
					{
						float rotation = 0.0f;
						fread(&rotation, sizeof(float), 1, input);// rotation (version >= 1.1)
						layer->SetRotation(rotation);
					}

					Position2 srcPos(0,0);
					fread(&(srcPos.X), sizeof(u32), 1, input);// src X
					fread(&(srcPos.Y), sizeof(u32), 1, input);// src Y
					layer->SetSrcPosition(srcPos);
					Dimension2 srcSize(0,0);
					fread(&(srcSize.Width), sizeof(u32), 1, input);// src width
					fread(&(srcSize.Height), sizeof(u32), 1, input);// src height
					layer->SetSrcSize(srcSize);

					Position2 destPos(0,0);
					fread(&(destPos.X), sizeof(u32), 1, input);// dest X
					fread(&(destPos.Y), sizeof(u32), 1, input);// dest Y
					layer->SetDestPosition(destPos);
					Dimension2 destSize(0,0);
					fread(&(destSize.Width), sizeof(u32), 1, input);// dest width
					fread(&(destSize.Height), sizeof(u32), 1, input);// dest height
					layer->SetDestSize(destSize);

					frame->AddLayer(layer);
				}
			}

			AddFrame(frame);
		}
	}

	fclose(input);

#ifdef IRON_ICE_EDITOR
	if(Optimize())
	{
		printf("Sprite loading : optimisation succeeded : save sprite file %s\n", path);
		SaveSpriteToFile(path);
	}
	else if(version[0] < latestVersion[0] || (version[0] == latestVersion[0] && version[1] < latestVersion[1]))
	{
		printf("Sprite loading : sprite version deprecated : save sprite file %s\n", path);
		SaveSpriteToFile(path);
	}
#endif

	return true;
}

#ifndef IRON_ICE_FINAL
bool AnimatedSprite::SaveSpriteToFile(char* path)// save sprite to a .spr file
{
	FILE* output = fopen(path, "wb");
	if(!output)
		return false;

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	char header[5] = "ASPR";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 2};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	// write raw images bank
	u32 bankSize = m_RawDataImages.size();
	fwrite(&bankSize, sizeof(u32), 1, output);// size of bank
	for(u32 i = 0; i < bankSize; ++i)
	{
		Image* img = TextureToImage(m_RawDataImages[i]);

		u32 bpp = img->getBytesPerPixel();
		Dimension2 dim = img->getDimension();
		u32 size = dim.Width*dim.Height*bpp;
		assert(bpp == 4);

		if(version[0] > 1 || (version[0] == 1 && version[1] >= 2))// from version 1.2
		{
			unsigned char* buffer = new unsigned char[size];
			io::IWriteFile* memFile  = Engine::GetInstance()->GetDevice()->getFileSystem()->createMemoryWriteFile(buffer, size, "foo.png", false);
			if(memFile)
			{
				driver->writeImageToFile(img, memFile);
				u32 compressedSizeTotal = memFile->getPos();

				fwrite(&compressedSizeTotal, sizeof(u32), 1, output);// compressed size
				fwrite(buffer, sizeof(unsigned char), compressedSizeTotal, output);// data (compressed or not)

				memFile->drop();
			}
			else
			{
				assert(0);
			}

			delete[] buffer;
			img->drop();
		}
		else
		{
			void* data = img->lock();
			void* compressedData = malloc(size+4*sizeof(int));
			void* compressedDataOffset = compressedData;
			u32 compressedSizeTotal = 0;
			u32 compressedSize = 0;
			PreProcessAlphaPixels((unsigned char*)data, size);
			for(u32 i=0; i<4; ++i)// for each color
			{
				int ret = RLECompressColor((unsigned char*)(data)+i, size, (unsigned char*)(compressedDataOffset) + sizeof(int), &compressedSize);
				assert(ret != -1 && compressedSize <= dim.Width*dim.Height);
				assert(ret == 1 || compressedSize == dim.Width*dim.Height);
				*((int*)compressedDataOffset) = compressedSize;
				compressedSizeTotal += compressedSize + sizeof(int);
				compressedDataOffset = (void*)((unsigned char*)compressedData + compressedSizeTotal);
			}
			assert(compressedSizeTotal <= size+4*sizeof(int));
			img->unlock();
			img->drop();

			fwrite(&(dim.Width), sizeof(u32), 1, output);// width
			fwrite(&(dim.Height), sizeof(u32), 1, output);// height
			fwrite(&size, sizeof(u32), 1, output);// non compressed size
			fwrite(&compressedSizeTotal, sizeof(u32), 1, output);// compressed size

			fwrite(compressedData, sizeof(unsigned char), compressedSizeTotal, output);// data (compressed or not)
			free(compressedData);
		}
	}

	// write frames
	fwrite(&m_Framerate, sizeof(f32), 1, output);// framerate
	fwrite(&(m_Size.Width), sizeof(u32), 1, output);// width
	fwrite(&(m_Size.Height), sizeof(u32), 1, output);// height

	u32 nbFrames = m_Frames.size();
	fwrite(&nbFrames, sizeof(u32), 1, output);// nb frames
	for(u32 i = 0; i < nbFrames; ++i)
	{
		FrameSprite* frame = m_Frames[i];
		if(frame)
		{
			Dimension2 frameSize = frame->GetSize();
			fwrite(&(frameSize.Width), sizeof(u32), 1, output);// frame width
			fwrite(&(frameSize.Height), sizeof(u32), 1, output);// frame height

			// write layers
			u32 nbLayers = frame->GetLayers().size();
			fwrite(&nbLayers, sizeof(u32), 1, output);// nb layers
			for(u32 j = 0; j < nbLayers; ++j)
			{
				LayerSprite* layer = frame->GetLayers()[j];
				if(layer)
				{
					s32 index = -1;
					for(u32 k = 0; k < bankSize; ++k)
					{
						if(m_RawDataImages[k] == layer->GetImage())
						{
							index = k;
							break;
						}
					}
					fwrite(&index, sizeof(s32), 1, output);// index

					if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))
					{
						float rotation = layer->GetRotation();
						fwrite(&rotation, sizeof(float), 1, output);// rotation (version >= 1.1)
					}

					Position2 srcPos = layer->GetSrcPosition();
					fwrite(&(srcPos.X), sizeof(u32), 1, output);// src X
					fwrite(&(srcPos.Y), sizeof(u32), 1, output);// src Y
					Dimension2 srcSize = layer->GetSrcSize();
					fwrite(&(srcSize.Width), sizeof(u32), 1, output);// src width
					fwrite(&(srcSize.Height), sizeof(u32), 1, output);// src height
					
					Position2 destPos = layer->GetDestPosition();
					fwrite(&(destPos.X), sizeof(u32), 1, output);// dest X
					fwrite(&(destPos.Y), sizeof(u32), 1, output);// dest Y
					Dimension2 destSize = layer->GetDestSize();
					fwrite(&(destSize.Width), sizeof(u32), 1, output);// dest width
					fwrite(&(destSize.Height), sizeof(u32), 1, output);// dest height
				}
			}
		}
	}

	fclose(output);

	return true;
}

bool AnimatedSprite::SaveSpriteToFileXML(const char* path)// save sprite to a .xml file with png images
{
	io::path fileName = path;

	//create xml writer
	fileName += "sprite.xml";
	io::IFileSystem* fs = Engine::GetInstance()->GetDevice()->getFileSystem();
	io::IXMLWriter* xwriter = fs->createXMLWriter( fileName );
	if (!xwriter)
		return false;

	unsigned char version[2] = {1, 2};// major version, minor version

	// write raw images bank
	u32 bankSize = m_RawDataImages.size();
	for(u32 i = 0; i < bankSize; ++i)
	{
		char name[32];
		sprintf(name, "%04d.png", i);

		fileName = path;
		fileName += name;
		Image* img = TextureToImage(m_RawDataImages[i]);
		Engine::GetInstance()->GetDriver()->writeImageToFile(img, fileName.c_str());		
		img->drop();
	}

	xwriter->writeXMLHeader();

	xwriter->writeElement(L"Sprite");
	xwriter->writeLineBreak();

	xwriter->writeElement(L"Version", true, L"major", core::stringw((u32)version[0]).c_str(), L"minor", core::stringw((u32)version[1]).c_str());
	xwriter->writeLineBreak();
	xwriter->writeLineBreak();

	xwriter->writeElement(L"AnimatedSprite", true, 
		L"frameRate", core::stringw(m_Framerate).c_str(), 
		L"width", core::stringw(m_Size.Width).c_str(), 
		L"height", core::stringw(m_Size.Height).c_str());
	xwriter->writeLineBreak();


	// write frames
	u32 nbFrames = m_Frames.size();
	for(u32 i = 0; i < nbFrames; ++i)
	{
		FrameSprite* frame = m_Frames[i];
		if(frame)
		{
			Dimension2 frameSize = frame->GetSize();
			xwriter->writeElement(L"FrameSprite", true, 
				L"width", core::stringw(frameSize.Width).c_str(), 
				L"height", core::stringw(frameSize.Height).c_str());
			xwriter->writeLineBreak();

			// write layers
			u32 nbLayers = frame->GetLayers().size();
			for(u32 j = 0; j < nbLayers; ++j)
			{
				LayerSprite* layer = frame->GetLayers()[j];
				if(layer)
				{
					TArray<core::stringw> names, values;

					s32 index = -1;
					for(u32 k = 0; k < bankSize; ++k)
					{
						if(m_RawDataImages[k] == layer->GetImage())
						{
							index = k;
							break;
						}
					}
					names.push_back(L"index");
					values.push_back(core::stringw(index));
					char path[32];
					sprintf(path, "%04d.png", index);
					names.push_back(L"fileName");
					values.push_back(core::stringw(path));

					if(version[0] > 1 || (version[0] == 1 && version[1] >= 1))
					{
						float rotation = layer->GetRotation();
						names.push_back(L"rotation");
						values.push_back(core::stringw(rotation));
					}

					Position2 srcPos = layer->GetSrcPosition();
					names.push_back(L"sourceX");
					values.push_back(core::stringw(srcPos.X));
					names.push_back(L"sourceY");
					values.push_back(core::stringw(srcPos.Y));
					
					Dimension2 srcSize = layer->GetSrcSize();
					names.push_back(L"sourceWidth");
					values.push_back(core::stringw(srcSize.Width));
					names.push_back(L"sourceHeight");
					values.push_back(core::stringw(srcSize.Height));

					Position2 destPos = layer->GetDestPosition();
					names.push_back(L"destX");
					values.push_back(core::stringw(destPos.X));
					names.push_back(L"destY");
					values.push_back(core::stringw(destPos.Y));
					
					Dimension2 destSize = layer->GetDestSize();
					names.push_back(L"destWidth");
					values.push_back(core::stringw(destSize.Width));
					names.push_back(L"destHeight");
					values.push_back(core::stringw(destSize.Height));

					xwriter->writeElement(L"FrameSprite", true, names, values);
					xwriter->writeLineBreak();
				}
			}
		}
	}

	xwriter->writeClosingTag(L"Sprite");
	xwriter->writeLineBreak();

	//delete xml writer
	xwriter->drop();

	return true;
}
#endif

#ifdef IRON_ICE_EDITOR
MD5 AnimatedSprite::GetMD5()
{
	MD5 md5;
	md5.StartDigest();

	// write raw images bank
	u32 bankSize = m_RawDataImages.size();
	md5.AddToDigest((BYTE*)&bankSize, sizeof(u32));
	for(u32 i = 0; i < bankSize; ++i)
	{
		Texture* tex = m_RawDataImages[i];

		u32* color = (u32*)tex->lock();
		const irr::core::dimension2d<u32>& dim = tex->getSize();
		u32 size = dim.Height*dim.Width;
		md5.AddToDigest((BYTE*)color, size*sizeof(u32));
	}

	// write frames
	md5.AddToDigest((BYTE*)&m_Framerate, sizeof(f32));
	md5.AddToDigest((BYTE*)&m_Size.Width, sizeof(u32));
	md5.AddToDigest((BYTE*)&m_Size.Height, sizeof(u32));

	u32 nbFrames = m_Frames.size();
	md5.AddToDigest((BYTE*)&nbFrames, sizeof(u32));
	for(u32 i = 0; i < nbFrames; ++i)
	{
		FrameSprite* frame = m_Frames[i];
		if(frame)
		{
			Dimension2 frameSize = frame->GetSize();
			md5.AddToDigest((BYTE*)&frameSize.Width, sizeof(u32));
			md5.AddToDigest((BYTE*)&frameSize.Height, sizeof(u32));

			// write layers
			u32 nbLayers = frame->GetLayers().size();
			md5.AddToDigest((BYTE*)&nbLayers, sizeof(u32));
			for(u32 j = 0; j < nbLayers; ++j)
			{
				LayerSprite* layer = frame->GetLayers()[j];
				if(layer)
				{
					s32 index = -1;
					for(u32 k = 0; k < bankSize; ++k)
					{
						if(m_RawDataImages[k] == layer->GetImage())
						{
							index = k;
							break;
						}
					}
					md5.AddToDigest((BYTE*)&index, sizeof(s32));
					
					float rotation = layer->GetRotation();
					md5.AddToDigest((BYTE*)&rotation, sizeof(u32));

					Position2 srcPos = layer->GetSrcPosition();
					md5.AddToDigest((BYTE*)&srcPos.X, sizeof(u32));
					md5.AddToDigest((BYTE*)&srcPos.Y, sizeof(u32));
					Dimension2 srcSize = layer->GetSrcSize();
					md5.AddToDigest((BYTE*)&srcSize.Width, sizeof(u32));
					md5.AddToDigest((BYTE*)&srcSize.Height, sizeof(u32));

					Position2 destPos = layer->GetDestPosition();
					md5.AddToDigest((BYTE*)&destPos.X, sizeof(u32));
					md5.AddToDigest((BYTE*)&destPos.Y, sizeof(u32));
					Dimension2 destSize = layer->GetDestSize();
					md5.AddToDigest((BYTE*)&destSize.Width, sizeof(u32));
					md5.AddToDigest((BYTE*)&destSize.Height, sizeof(u32));
				}
			}
		}
	}

	md5.EndDigest();

	return md5;
}


bool AnimatedSprite::Optimize()
{
	printf("Sprite optimization : start\n");
	bool optimised = false;

	const u32 bankSize = m_RawDataImages.size();
	MD5::MD5Raw* md5Raws = new MD5::MD5Raw[bankSize];
	u32* redirectionTable = new u32[bankSize];
	
	// gather all md5s from the images in the bank
	for(u32 i = 0; i < bankSize; ++i)
	{
		MD5 md5;
		md5.StartDigest();
		Texture* tex = m_RawDataImages[i];

		u32* color = (u32*)tex->lock();
		const irr::core::dimension2d<u32>& dim = tex->getSize();
		u32 size = dim.Height*dim.Width;
		md5.AddToDigest((BYTE*)color, size*sizeof(u32));

		md5.EndDigest();
		md5Raws[i] = md5._raw;
		redirectionTable[i] = i;
	}

	// set the redirection when finding identical images
	for(u32 i = 1; i < bankSize; ++i)
	{
		for(u32 j = 0; j < i; ++j)
		{
			if(md5Raws[i] == md5Raws[j])
			{
				redirectionTable[i] = j;
				break;
			}
		}
	}

	// apply the redirections and remove unused textures
	for(u32 i = 0; i < bankSize; ++i)
	{
		if(redirectionTable[i] != i)// redirection to apply
		{
			printf("Sprite optimization : bank image %d identical to image %d\n", i, redirectionTable[i]);
			u32 nbFrames = m_Frames.size();
			for(u32 j = 0; j < nbFrames; ++j)
			{
				FrameSprite* frame = m_Frames[j];
				if(frame)
				{
					u32 nbLayers = frame->GetLayers().size();
					for(u32 k = 0; k < nbLayers; ++k)
					{
						LayerSprite* layer = frame->GetLayers()[k];
						if(layer && layer->GetImage() && layer->GetImage() == m_RawDataImages[i])// this layer is used the image to delete
						{
							// replace the image to delete with the image to keep (redirected)
							printf("Sprite optimization : bank image %d used in frame %d layer %d : redirecting it !\n", i, j, k);
							layer->SetImage(m_RawDataImages[redirectionTable[i]]);
						}
					}
				}
			}
			printf("Sprite optimization : deleting bank image %d...\n", i);
			Engine::GetInstance()->GetDriver()->removeTexture(m_RawDataImages[i]);
			m_RawDataImages[i] = NULL;
			optimised = true;
		}
	}

	// remove the deleted textures ref from the bank
	for(u32 i = 0; i < m_RawDataImages.size(); ++i)
	{
		if(!m_RawDataImages[i])
		{
			m_RawDataImages.erase(i);
			--i;
		}
	}
	SafeDeleteArray(md5Raws);

	printf("Sprite optimization : end\n");
	return optimised;
}
#endif

bool AnimatedSprite::AddImageToData(Texture* tex)// add an image to the image data pool
{
	if(tex)
	{
		m_RawDataImages.push_back(tex);
		tex->grab();
		return true;
	}
	return false;
}

bool AnimatedSprite::AddFrame(FrameSprite* frame)// add a new empty frame
{
	if(frame)
	{
		m_Frames.push_back(frame);
		return true;
	}
	return false;
}

void AnimatedSprite::RenderFrame(u32 frameIndex, const core::matrix4& matrix, const IColor& tint /*= WHITE*/, bool mirrorH/* = false*/, bool mirrorV/* = false*/, core::rect<irr::s32>* clip/* = NULL*/)
{
	if(m_Frames.empty())
		return;

	if(frameIndex < m_Frames.size())
	{
		m_Frames[frameIndex]->Render(matrix, tint, mirrorH, mirrorV, clip);
	}
}

void AnimatedSprite::RenderDebugInfo(u32 frameIndex, const core::matrix4& matrix, bool mirrorH/* = false*/, bool mirrorV/* = false*/, bool recursive/* = false*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	draw2DRect(driver, m_Size, matrix, IColor(255, 0, 255, 0));

	if(recursive)
	{
		if(frameIndex < m_Frames.size())
		{
			m_Frames[frameIndex]->RenderDebugInfo(matrix, mirrorH, mirrorV, recursive);
		}
	}
}

// void AnimatedSprite::RenderDebugInfo(u32 frameIndex, const Position2& pos, float scale/* = 1.0f*/, bool recursive/* = false*/)
// {
// 	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
// 	core::matrix4 matrix = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) * 
// 		core::matrix4().setScale(core::vector3df(scale, scale, 0.0f));
// 
// 	draw2DRect(driver, m_Size, matrix, IColor(255, 0, 255, 0));
// 
// 	if(recursive)
// 	{
// 		if(frameIndex < m_Frames.size())
// 		{
// 			m_Frames[frameIndex]->RenderDebugInfo(pos, scale, recursive);
// 		}
// 	}
// }

video::ITexture* AnimatedSprite::RenderPreview()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	u32 size = core::max_(m_Size.Width, m_Size.Height);
	Dimension2 dim(size, size);
	video::ITexture* renderTarget = driver->addRenderTargetTexture(dim, "RTT_AnimatedSprite", video::ECF_A8R8G8B8);
	if (renderTarget)
	{
		// draw scene into render target

		// set render target texture
		driver->setRenderTarget(renderTarget, true, true, COLOR_TRANSPARENT);

		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		
		Position2 pos(m_Size.Width>>1, m_Size.Height>>1);
		
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f));

		TArray<FrameSprite*>& frames = GetFrames();
		FrameSprite* frame = (GetFrames().size()>0)?GetFrames()[0]:NULL;
		if(frame)
		{
			frame->Render(mat);
		}

		// set back old render target
		// The buffer might have been distorted, so clear it
		driver->setRenderTarget(0, true, true, IColor(0,200,200,200));
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}

	return renderTarget;
}


