#ifndef AUDIO_H
#define AUDIO_H

#ifndef __APPLE__
#include <audiere.h>
#endif
#include "utils.h"


#ifdef __APPLE__
class RefCounted 
{
protected:
	~RefCounted() { }

public:
	void ref() {}
	void unref() {}
};


template<typename T>
class RefPtr 
{
public:
	RefPtr(T* ptr = 0) 
	{
		m_ptr = 0;
		*this = ptr;
	}

	RefPtr(const RefPtr<T>& ptr) 
	{
		m_ptr = 0;
		*this = ptr;
	}

	~RefPtr() 
	{
		if (m_ptr) 
		{
			m_ptr->unref();
			m_ptr = 0;
		}
	}

	RefPtr<T>& operator=(T* ptr) 
	{
		if (ptr != m_ptr) 
		{
			if (m_ptr) 
			{
				m_ptr->unref();
			}
			m_ptr = ptr;
			if (m_ptr) 
			{
				m_ptr->ref();
			}
		}
		return *this;
	}

	RefPtr<T>& operator=(const RefPtr<T>& ptr) 
	{
		*this = ptr.m_ptr;
		return *this;
	}

	T* operator->() const 
	{
		return m_ptr;
	}

	T& operator*() const 
	{
		return *m_ptr;
	}

	operator bool() const 
	{
		return (m_ptr != 0);
	}

	T* get() const 
	{
		return m_ptr;
	}

private:
	T* m_ptr;
};


template<typename T, typename U>
bool operator==(const RefPtr<T>& a, const RefPtr<U>& b) 
{
	return (a.get() == b.get());
}

template<typename T>
bool operator==(const RefPtr<T>& a, const T* b) 
{
	return (a.get() == b);
}

template<typename T>
bool operator==(const T* a, const RefPtr<T>& b) 
{
	return (a == b.get());
}


template<typename T, typename U>
bool operator!=(const RefPtr<T>& a, const RefPtr<U>& b) 
{
	return (a.get() != b.get());
}

template<typename T>
bool operator!=(const RefPtr<T>& a, const T* b) 
{
	return (a.get() != b);
}

template<typename T>
bool operator!=(const T* a, const RefPtr<T>& b) 
{
	return (a != b.get());
}

class OutputStream : public RefCounted 
{
protected:
	~OutputStream() {}

public:

	void play() {}

	void stop() {}

	bool isPlaying() {return false;}

	void reset() {}

	void setRepeat(bool repeat) {}

	bool getRepeat() {return false;}

	void setVolume(float volume) {}

	float getVolume() {return 0.0f;}

	void setPan(float pan) {}

	float getPan() {return 0.0f;}

	void setPitchShift(float shift) {}

	float getPitchShift() {return 0.0f;}

	bool isSeekable() {return true;}

	int getLength()  {return 0;}

	void setPosition(int position) {}

	int getPosition()  {return 0;}
};
typedef RefPtr<OutputStream> OutputStreamPtr;

enum SampleFormat 
{
	SF_U8,  ///< unsigned 8-bit integer [0,255]
	SF_S16, ///< signed 16-bit integer in host endianness [-32768,32767]
};

class SampleSource : public RefCounted 
{
protected:
	~SampleSource() { }

public:
	void getFormat(
		int& channel_count,
		int& sample_rate,
		SampleFormat& sample_format) {}

	int read(int frame_count, void* buffer) {return 0;}

	void reset() {}

	bool isSeekable() {return false;}

	int getLength() {return 0;}

	void setPosition(int position) {}

	int getPosition() {return 0;}

	bool getRepeat() {return false;}

	void setRepeat(bool repeat) {}

	int getTagCount() {return 0;}

	const char* getTagKey(int i) {return "";}

	const char* getTagValue(int i) {return "";}

	const char* getTagType(int i) {return "";}
};
typedef RefPtr<SampleSource> SampleSourcePtr;

class AudioDevice : public RefCounted 
{
protected:
	~AudioDevice() { }

public:

	void update() {}

	OutputStream* openStream(SampleSource* source) {return NULL;}

	OutputStream* openBuffer(
		void* samples,
		int frame_count,
		int channel_count,
		int sample_rate,
		SampleFormat sample_format) {return NULL;}

	const char* getName() {return "";}

	void registerCallback(void* callback) {}

	void unregisterCallback(void* callback) {}

	void clearCallbacks() {}
};
typedef RefPtr<AudioDevice> AudioDevicePtr;

inline AudioDevice* OpenDevice(
							   const char* name = 0,
							   const char* parameters = 0)
{
	return NULL;
}

enum FileFormat 
{
	FF_AUTODETECT,
	FF_WAV,
	FF_OGG,
	FF_FLAC,
	FF_MP3,
	FF_MOD,
	FF_AIFF,
	FF_SPEEX,
};

inline OutputStream* OpenSound(
							   const AudioDevicePtr& device,
							   const char* filename,
							   bool streaming = false,
							   FileFormat file_format = FF_AUTODETECT)
{
	return NULL;
}
#else
using namespace audiere;
#endif

class IIAudioDevice;

class AudioStream
{
	friend class IIAudioDevice;
public:
	AudioStream() : m_IsPlaying(false), m_StopAtEnd(false), m_Volume(1.0f) {}
	~AudioStream() 
	{
		if(m_StreamPtr)
		{
			m_StreamPtr->stop();
		}
	}

	bool IsValid() {return (m_StreamPtr.get() != NULL);}

	void Play() {if(!m_IsPlaying) {if(!m_StreamPtr->isPlaying()) m_StreamPtr->play(); m_IsPlaying = true;}}
	bool IsPlaying() const {return m_IsPlaying;/*m_StreamPtr->isPlaying();*/}
	void Stop() {if(m_IsPlaying) {if(m_StreamPtr->isPlaying()) m_StreamPtr->stop(); m_IsPlaying = false;}}

	void Pause() {if(m_IsPlaying) if(m_StreamPtr->isPlaying()) m_StreamPtr->stop();}
	void Resume() {if(m_IsPlaying) if(!m_StreamPtr->isPlaying()) m_StreamPtr->play();}

	void Reset() {if(m_StreamPtr->getPosition() != 0) m_StreamPtr->reset();}

	void SetPan(float pan) {pan = core::clamp(pan, -1.0f, 1.0f); m_StreamPtr->setPan(pan);}
	float GetPan() const {return m_StreamPtr->getPan();}
	void SetPitch(float pitch) {pitch = core::clamp(pitch, 0.5f, 2.0f); m_StreamPtr->setPitchShift(pitch);}
	float GetPitch() const {return m_StreamPtr->getPitchShift();}
	void SetVolume(float vol) {vol = core::clamp(vol, 0.0f, 1.0f); m_Volume = vol;}
	float GetVolume() const {return m_Volume;}

	void SetRepeat(bool repeat) {if(m_StreamPtr->getRepeat() != repeat) m_StreamPtr->setRepeat(repeat);}// CAUTION : the setReapeat reset the sound !
	bool GetRepeat() const {return m_StreamPtr->getRepeat();}

	void SetStopAtEnd(bool stop) {m_StopAtEnd = stop;}
	bool GetStopAtEnd() const {return m_StopAtEnd;}

	bool IsSeekable() const {return m_StreamPtr->isSeekable();}

protected:
	OutputStreamPtr m_StreamPtr;
	bool m_IsPlaying;

	bool m_StopAtEnd;
	float m_Volume;
};

class IIAudioDevice
{
public:
	IIAudioDevice();
	~IIAudioDevice();

	void Update(float dt);

	bool IsPaused() const {return m_IsPaused;}
	void SetPaused(bool state);

	SmartPointer<AudioStream> OpenSoundFile(const char* filename, bool stream = true);
	bool CloseSound(const SmartPointer<AudioStream>& sound);

	void CloseAllSounds() {m_Streams.clear();}
	void StopAllSounds() {for(u32 i = 0; i < m_Streams.size(); ++i) m_Streams[i]->Stop();}

	bool SetSoundVolume(const SmartPointer<AudioStream>& sound, float volume);
	bool SetupSound(const SmartPointer<AudioStream>& sound, 
		float pan, float pitch, float volume, bool repeat = false, bool stopAtDialogsEnd = true);
	
protected:
	AudioDevicePtr m_AudioDevice;

	TArray< SmartPointer<AudioStream> > m_Streams;

	bool m_IsPaused;
};

#endif //#ifdef AUDIO_H
