﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

#define LANG_ID Engine::SFTID_LanguageMenuPanel


// LanguagePanel
LanguagePanel::LanguagePanel() : IPanel(), m_BG(NULL), m_Font(NULL), m_CurrentLangSelection(0)
{
	for(int i = 0; i < Engine::LS_MAX; ++i)
		m_Languages[i] = NULL;
	m_LangSelec = NULL;
	for(int i = 0; i < 2; ++i)
		m_Arrows[i] = NULL;
}

LanguagePanel::~LanguagePanel()
{
	UnloadPanel();
}

void LanguagePanel::LoadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.LanguagePanelFontScale;

	SAFE_LOAD_IMAGE(driver, m_BG, engine->GetGamePath("system/Menu_BG.jpg"), USE_PREMULTIPLIED_ALPHA);

	if(!m_Font)
	{
		u32 size = (u32)(140.0f * innerScale * fontScale);

		CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));
		font->attach(face, size, USE_PREMULTIPLIED_ALPHA, size>>3);
		face->drop();// now we attached it we can drop the reference
		font->AntiAlias = true;
		font->Transparency = true;

		m_Font = font;
	}

	SAFE_LOAD_IMAGE(driver, m_Languages[Engine::LS_FRENCH], engine->GetGamePath("system/00_Lang_FR.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Languages[Engine::LS_ENGLISH], engine->GetGamePath("system/00_Lang_EN.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Languages[Engine::LS_GERMAN], engine->GetGamePath("system/00_Lang_DE.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Languages[Engine::LS_SPANISH], engine->GetGamePath("system/00_Lang_ES.png"), USE_PREMULTIPLIED_ALPHA);

	SAFE_LOAD_IMAGE(driver, m_LangSelec, engine->GetGamePath("system/00_Lang_0_selection.png"), USE_PREMULTIPLIED_ALPHA);

	SAFE_LOAD_IMAGE(driver, m_Arrows[0], engine->GetGamePath("system/00_L_Arrow.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Arrows[1], engine->GetGamePath("system/00_R_Arrow.png"), USE_PREMULTIPLIED_ALPHA);

	OnResize();

	StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN));

	IPanel::LoadPanel();
}

void LanguagePanel::UnloadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_BG);
	SAFE_UNLOAD(m_Font);

	SAFE_UNLOAD(m_Languages[Engine::LS_FRENCH]);
	SAFE_UNLOAD(m_Languages[Engine::LS_ENGLISH]);
	SAFE_UNLOAD(m_Languages[Engine::LS_GERMAN]);
	SAFE_UNLOAD(m_Languages[Engine::LS_SPANISH]);

	SAFE_UNLOAD(m_LangSelec);

	SAFE_UNLOAD(m_Arrows[0]);
	SAFE_UNLOAD(m_Arrows[1]);

	IPanel::UnloadPanel();
}

void LanguagePanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.LanguagePanelFontScale;

	if(m_Font)
	{
		u32 size = (u32)(140.0f * innerScale * fontScale);
		m_Font->setSize(size, size>>3);

		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;
		core::stringw str(Engine::ms_LanguageSettingsString[Engine::GetInstance()->m_CoreData.AvailableLangs[m_CurrentLangSelection]]);
		Dimension2 dim(m_Font->getDimension(str.c_str()));

		Position2 halfCorner((dim.Width>>1), (dim.Height>>1));
		m_LanguageSelectionArea[2].Area = Rectangle2(pos-halfCorner, pos+halfCorner);
	}

	if(m_LangSelec)
	{
		int nbLang = Engine::GetInstance()->m_CoreData.AvailableLangs.size();

		Dimension2 size = m_LangSelec->getSize();

		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height - (size.Height>>1) - 100)*innerScale)) + innerOffset;

		{
			u32 dx = (u32)(((f32)(size.Width>>1) + 40.0f)*innerScale);
			Position2 offset(-(nbLang-1)*dx, 0);
			for(int i = 0; i < nbLang; ++i, offset.X += (dx<<1))
			{
				Position2 cornerOffset((s32)((f32)size.Width*innerScale*0.5f), (s32)((f32)size.Height*innerScale*0.5f));
				m_LanguagesArea[i].Area = Rectangle2(pos + offset - cornerOffset, pos + offset + cornerOffset);
				m_LanguagesArea[i].IsSelected = false;
			}
		}
	}
}

void LanguagePanel::Update(float dt)
{
	IPanel::Update(dt);

}

void LanguagePanel::Render()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor color = COLOR_BLACK;
	fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);

	if(m_BG)
	{
		IColor tint = COLOR_WHITE;

		Dimension2 size = m_BG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_BG, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	if(m_LangSelec)
	{
		float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
		IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(50), ratio);

		int nbLang = Engine::GetInstance()->m_CoreData.AvailableLangs.size();

		Dimension2 size = m_LangSelec->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_LanguagesArea[m_CurrentLangSelection].Area.getCenter().X, (f32)m_LanguagesArea[m_CurrentLangSelection].Area.getCenter().Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_LangSelec, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);

		for(int i = 0; i < nbLang; ++i)
		{
			if(m_Languages[Engine::GetInstance()->m_CoreData.AvailableLangs[i]])
			{
				IColor tint = IColor(200, 255, 255, 255);

				Dimension2 size = m_Languages[i]->getSize();
				Rectangle2 sourceRect(Position2(0,0), size);

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_LanguagesArea[i].Area.getCenter().X, (f32)m_LanguagesArea[i].Area.getCenter().Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

				draw2DImage(driver, m_Languages[Engine::GetInstance()->m_CoreData.AvailableLangs[i]], sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
			}
		}
	}

	if(m_Font)
	{
		float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
		IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(255>>1), ratio);

		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		core::stringw str(Engine::GetLanguageString(Engine::GetInstance()->m_CoreData.AvailableLangs[m_CurrentLangSelection], LANG_ID));
		m_Font->drawGauss(str, Rectangle2(pos, Dimension2(0, 0)), tint, true, true);
		m_Font->draw(str, Rectangle2(pos, Dimension2(0, 0)), COMPUTE_THEME_COLOR(m_LanguageSelectionArea[2].IsSelected?255:180), true, true);

		if(m_Arrows[0] && m_Arrows[1])
		{
			float ratio = core::abs_(cosf(0.75f*core::PI*m_Time + core::PI*0.5f));
			IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(255), COMPUTE_THEME_COLOR_FONT_GLOW(50), ratio);

			Position2 offset((s32)((f32)60*innerScale), 0);

			//if(m_CurrentLangSelection > 0)
			{
				Dimension2 size = m_Arrows[0]->getSize();
				Rectangle2 sourceRect(Position2(0,0), size);
				Position2 pos2(pos.X - (s32)(innerScale*600) - offset.X, pos.Y);// text centered
				//Position2 pos(m_LanguagesArea[0].Area.UpperLeftCorner.X-offset.X, m_LanguagesArea[0].Area.getCenter().Y+offset.Y);// flag centered

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

				IColor color = (m_CurrentLangSelection > 0)? ( m_LanguageSelectionArea[0].IsSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : tint) : COMPUTE_THEME_COLOR(50);
				draw2DImage(driver,m_Arrows[0], sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);

				Position2 halfCorner((s32)(innerScale*size.Width*0.5f), (s32)(innerScale*size.Height*0.5f));
				m_LanguageSelectionArea[0].Area = Rectangle2(pos2-halfCorner, pos2+halfCorner);
			}

			//if(m_CurrentLangSelection < Engine::GetInstance()->m_CoreData.AvailableLangs.size()-1)
			{
				Dimension2 size = m_Arrows[1]->getSize();
				Rectangle2 sourceRect(Position2(0,0), size);
				Position2 pos2(pos.X + (s32)(innerScale*600) + offset.X, pos.Y);// text centered
				//Position2 pos(m_LanguagesArea[Engine::GetInstance()->m_CoreData.AvailableLangs.size()-1].Area.LowerRightCorner.X+offset.X, m_LanguagesArea[Engine::GetInstance()->m_CoreData.AvailableLangs.size()-1].Area.getCenter().Y+offset.Y); // flag centered

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

				IColor color = (m_CurrentLangSelection < Engine::GetInstance()->m_CoreData.AvailableLangs.size()-1)? ( m_LanguageSelectionArea[1].IsSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : tint) : COMPUTE_THEME_COLOR(50);
				draw2DImage(driver, m_Arrows[1], sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);

				Position2 halfCorner((s32)(innerScale*size.Width*0.5f), (s32)(innerScale*size.Height*0.5f));
				m_LanguageSelectionArea[1].Area = Rectangle2(pos2-halfCorner, pos2+halfCorner);
			}
		}
	}

// 	{
// 		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)256+innerOffset.X, (f32)256+innerOffset.Y, 0.0f)) *
// 			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
// 			core::matrix4().setScale(core::vector3df((f32)1, (f32)1, 0.0f));
// 		draw2DImage(driver, m_Font->GetTexturePoolGauss()[0], Rectangle2(0,0,512,512),  mat, true, WHITE, false, USE_PREMULTIPLIED_ALPHA);
// 	}

	if(IsFading())
	{
		IColor fadeColor = COLOR_BLACK;
		IColor transparent = COLOR_TRANSPARENT;
		//fade in
		float ratio = m_Fader.GetRatio();
		IColor color = MathUtils::GetInterpolatedColor(fadeColor, transparent, ratio);
		fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);
	}
}

bool LanguagePanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
			for(u32 i = 0; i < Engine::GetInstance()->m_CoreData.AvailableLangs.size(); ++i)
			{
				m_LanguagesArea[i].IsSelected = false;
				if(m_LanguagesArea[i].Area.isPointInside(mousePos))
				{
					m_CurrentLangSelection = i;
					m_LanguagesArea[m_CurrentLangSelection].IsSelected = true;
					break;
				}
			}

			int index = -1;
			for(int i = 0; i < 3; ++i)
			{
				m_LanguageSelectionArea[i].IsSelected = false;
				if(m_LanguageSelectionArea[i].Area.isPointInside(mousePos))
				{
					index = i;
				}
			}
			if(index >= 0)
				m_LanguageSelectionArea[index].IsSelected = true;
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_LanguageSelectionArea[0].IsSelected)
			{
				if(m_CurrentLangSelection > 0)
					--m_CurrentLangSelection;
			}
			else if(m_LanguageSelectionArea[1].IsSelected)
			{
				if(m_CurrentLangSelection < Engine::GetInstance()->m_CoreData.AvailableLangs.size()-1 )
					++m_CurrentLangSelection;
			}
			else
			{
				bool isValidClick = false;
				for(u32 i = 0; i < Engine::GetInstance()->m_CoreData.AvailableLangs.size(); ++i)
				{
					isValidClick |= m_LanguagesArea[i].IsSelected;
				}
				isValidClick |= m_LanguageSelectionArea[2].IsSelected;
				if(isValidClick)
				{
					Engine::GetInstance()->SetLanguage(Engine::GetInstance()->m_CoreData.AvailableLangs[m_CurrentLangSelection]);
					m_TransitToChildPanelIndex = 0;
					StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
				}
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				Engine::GetInstance()->SetLanguage(Engine::GetInstance()->m_CoreData.AvailableLangs[m_CurrentLangSelection]);
				m_TransitToChildPanelIndex = 0;
				StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentLangSelection > 0)
					--m_CurrentLangSelection;
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentLangSelection < Engine::GetInstance()->m_CoreData.AvailableLangs.size()-1 )
					++m_CurrentLangSelection;
			}
		}
	}

	return false;
}

#undef PANEL_FADETIME
