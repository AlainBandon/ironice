﻿#include "view.h"
#include "engine.h"
#include "audio.h"
#include "utils.h"


// Define some values that we'll use to identify individual GUI controls.
#define GUI_ID_SAVE_SPRITE_FILE_NAME_DLG 102
#define GUI_ID_SAVE_SCENE_FILE_NAME_DLG 103
#define GUI_ID_SAVE_DIALOG_FILE_NAME_DLG 104
#define GUI_ID_POPUP_DLG 105
#define GUI_ID_XML_EXPORT_DIALOG_FILE_NAME_DLG 106
#define GUI_ID_BUTTON_SPRITE_MODE 600
#define GUI_ID_BUTTON_SCENE_MODE 601
#define GUI_ID_BUTTON_RENDER_MODE 602

Editor* Editor::ms_Instance = NULL;



// MyEventReceiver
bool MyEventReceiver::OnEvent(const SEvent& event)
{
	gui::IGUIEnvironment* env = Context.device->getGUIEnvironment();

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
		{
			gui::IGUIElement* elt = NULL;
			if(Context.spritebanklist->m_CurrentlyHovered)
				elt = Context.spritebanklist;
			else if(Context.framescrollmenu->m_CurrentlyHovered)
				elt = Context.framescrollmenu;
			else if(Context.layermenu->m_CurrentlyHovered)
				elt = Context.layermenu;
			else if(Context.editorwindow->m_CurrentlyHovered)
				elt = Context.editorwindow;
			else if(Context.animbanklist->m_CurrentlyHovered)
				elt = Context.animbanklist;
			env->setFocus(elt);
		}
	}
	else if(event.EventType == EET_KEY_INPUT_EVENT)// hotkeys 
	{
		if(event.KeyInput.PressedDown)
		{
			if(event.KeyInput.Control)
			{
				if(event.KeyInput.Key == KEY_KEY_S)// quick save
				{
					if(Context.sceneinterface->GetSceneNameForQuickSave() != L"")// if we already saved or load the scene
					{
						if(!m_PopupDialog)
						{
							Context.sceneinterface->SaveScene(Context.sceneinterface->GetSceneNameForQuickSave());

							core::stringw txt = L"Le fichier a bien été enregistré";
							if(Context.sceneinterface->m_IsSetForTextsExport)
							{
								txt = L"Les textes ont bien été exportés";
							}
							else if(Context.sceneinterface->m_IsSetForTextsImport)
							{
								txt = L"Les textes ont bien été importés";
							}
							Rectangle2 rect = Context.editorwindow->getRelativePosition();
							rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
							m_PopupDialog = env->addWindow(rect, true, core::stringw(Context.sceneinterface->GetSceneNameForQuickSave().c_str()).c_str());
							gui::IGUIStaticText* text = env->addStaticText(txt.c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_PopupDialog);
							text->setToolTipText(core::stringw(Context.sceneinterface->GetSceneNameForQuickSave().c_str()).c_str());
							gui::IGUIButton* btn = env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_PopupDialog, GUI_ID_POPUP_DLG, L"OK");
						}
					}
					else// if it is our fisrt save : call the save window
					{
						if(!Context.sceneeditorwindow->m_SaveFileDlg)
							Context.sceneeditorwindow->m_SaveFileDlg = env->addFileOpenDialog(L"Choisissez un Dossier où enregistrer");
					}

					// ANTIBUG
					Context.sceneeditorwindow->m_CtrlIsPressed = false;
					Context.sceneeditorwindow->m_ShiftIsPressed = false;
				}
			}
			else
			{
				if(event.KeyInput.Key == KEY_RETURN || event.KeyInput.Key == KEY_ESCAPE)
				{
					if(m_PopupDialog)
					{
						m_PopupDialog->remove();
						m_PopupDialog = NULL;
					}
				}
			}
		}
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		if(event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED)
		{
			switch(id)
			{
			case GUI_ID_POPUP_DLG:
				{
					if(m_PopupDialog)
					{
						m_PopupDialog->remove();
						m_PopupDialog = NULL;
					}
				}
				break;
			case GUI_ID_SAVE_SPRITE_FILE_NAME_DLG:
				{
					if(m_SaveFileNameDialog)
					{
						m_SaveName += m_SaveFileNameEditBox->getText();
						Context.spriteinterface->SaveSprite(m_SaveName.c_str());
						m_SaveFileNameDialog->remove();
						m_SaveFileNameDialog = NULL;
					}
				}
				break;
			case GUI_ID_SAVE_SCENE_FILE_NAME_DLG:
				{
					if(m_SaveFileNameDialog)
					{
						m_SaveName += m_SaveFileNameEditBox->getText();
						Context.sceneinterface->SaveScene(m_SaveName.c_str());
						m_SaveFileNameDialog->remove();
						m_SaveFileNameDialog = NULL;
					}
				}
				break;
			case GUI_ID_SAVE_DIALOG_FILE_NAME_DLG:
				{
					if(m_SaveFileNameDialog)
					{
						m_SaveName += m_SaveFileNameEditBox->getText();
						Context.sceneinterface->SaveDialog(m_SaveName.c_str());
						m_SaveFileNameDialog->remove();
						m_SaveFileNameDialog = NULL;
					}
				}
				break;
			case GUI_ID_XML_EXPORT_DIALOG_FILE_NAME_DLG:
				{
					if(m_SaveFileNameDialog)
					{
						m_SaveName += m_SaveFileNameEditBox->getText();
						Context.sceneinterface->XMLExportDialog(m_SaveName.c_str());
						m_SaveFileNameDialog->remove();
						m_SaveFileNameDialog = NULL;
					}
				}
			case GUI_ID_BUTTON_SCENE_MODE:
				Editor::GetInstance()->ChangeMode(Editor::EM_SCENE);
				break;
			case GUI_ID_BUTTON_SPRITE_MODE:
				Editor::GetInstance()->ChangeMode(Editor::EM_SPRITE);
				break;
			case GUI_ID_BUTTON_RENDER_MODE:
				Editor::GetInstance()->ChangeMode(Editor::EM_RENDER);
				break;
			default: 
				break;
			}
		}

		switch(event.GUIEvent.EventType)
		{
		case gui::EGET_FILE_SELECTED:	
			if ( event.GUIEvent.Caller == Context.spritebanklist->m_OpenFileDlg )
			{
				Context.spritebanklist->AddTextureToBank(io::path(Context.spritebanklist->m_OpenFileDlg->getFileName()).c_str());
				Context.spritebanklist->m_OpenFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.editorwindow->m_LoadFileDlg)
			{
				Context.spriteinterface->LoadSprite(io::path(Context.editorwindow->m_LoadFileDlg->getFileName()).c_str());
				Context.editorwindow->OnSpriteChange();
				Context.spritebanklist->OnSpriteChange();
				Context.layermenu->OnSpriteChange();
				Context.framescrollmenu->OnSpriteChange();
				Context.editorwindow->m_LoadFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.editorwindow->m_SaveFileDlg)
			{
				//m_SaveName = io::path(Context.editorwindow->m_SaveFileDlg->getFileName());
				irr::io::IFileSystem *fs = Context.device->getFileSystem();
				irr::core::stringc current = fs->getWorkingDirectory();
				m_SaveName = io::path(current) + "/";
				Rectangle2 rect = Context.editorwindow->m_SaveFileDlg->getRelativePosition();
				rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
				m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
				gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
				text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
				m_SaveFileNameEditBox = env->addEditBox(L"sprite.spr", Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
				env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_SAVE_SPRITE_FILE_NAME_DLG, L"OK");
				Context.editorwindow->m_SaveFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.animbanklist->m_OpenFileDlg )
			{
				if(Context.animbanklist->m_SpriteLoadType == AnimationBankListBox::SLT_SPRITE)
					Context.animbanklist->AddSpriteToBank(io::path(Context.animbanklist->m_OpenFileDlg->getFileName()).c_str());
				else if(Context.animbanklist->m_SpriteLoadType == AnimationBankListBox::SLT_TEXTURE)
				{
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sprites);
					Texture* tex = Context.device->getVideoDriver()->getTexture(io::path(Context.animbanklist->m_OpenFileDlg->getFileName()));
					if(tex)
					{
						Dimension2 dim = tex->getSize();
						SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
						sprite->SetSize(dim);
						Context.sceneinterface->AddSpriteToBank(sprite);
						FrameSprite* frame = new FrameSprite();
						frame->SetSize(dim);
						sprite->AddFrame(frame);
						LayerSprite* layer = new LayerSprite();
						frame->AddLayer(layer);
						layer->SetImage(tex);
						sprite->AddImageToData(tex);
						Context.animbanklist->UpdateSpriteBank();
						u32 index = Context.sceneinterface->GetScene()->GetSprites().size() - 1;
						printf("Sprite %04d has been created : saving it before unloading\n", index);
						Context.sceneinterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);
					}
				}
				else if(Context.animbanklist->m_SpriteLoadType == AnimationBankListBox::SLT_BATCH_TEXTURE)
				{
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sprites);
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					//irr::core::stringc current = fs->getWorkingDirectory();
					//fs->changeWorkingDirectoryTo(Context.animbanklist->m_OpenFileDlg->getDirectoryName());
					irr::io::IFileList* folder = fs->createFileList();

					SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
					Context.sceneinterface->AddSpriteToBank(sprite);
					Dimension2 maxDim(0,0);
					for(u32 i = 0; i < folder->getFileCount(); ++i)
					{
						Texture* tex = Context.device->getVideoDriver()->getTexture(io::path(folder->getFileName(i)).c_str());
						if(tex)
						{
							FrameSprite* frame = new FrameSprite();
							Dimension2 dim = tex->getSize();
							if(dim.Width > maxDim.Width)
								maxDim.Width = dim.Width;
							if(dim.Height > maxDim.Height)
								maxDim.Height = dim.Height;

							frame->SetSize(dim);
							sprite->AddFrame(frame);
							LayerSprite* layer = new LayerSprite();
							frame->AddLayer(layer);
							layer->SetImage(tex);
							sprite->AddImageToData(tex);
						}
					}
					sprite->SetSize(maxDim);
					sprite->Optimize();
					Context.animbanklist->UpdateSpriteBank();
					u32 index = Context.sceneinterface->GetScene()->GetSprites().size() - 1;
					printf("Sprite %04d has been created : saving it before unloading\n", index);
					Context.sceneinterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);

					//fs->changeWorkingDirectoryTo(current);
				}
				Context.animbanklist->m_OpenFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.animbanklist->m_OpenFileDlg )
			{
				irr::io::IFileSystem *fs = Context.device->getFileSystem();
				irr::core::stringc current = fs->getWorkingDirectory();
				io::path saveName = io::path(current) + "/";
				Context.animbanklist->ExportSpriteXML(saveName.c_str());
				Context.animbanklist->m_SaveFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.sceneeditorwindow->m_LoadFileDlg)
			{
				Context.sceneinterface->SetSceneNameForQuickSave(io::path(Context.sceneeditorwindow->m_LoadFileDlg->getFileName()).c_str());
				Context.sceneinterface->LoadScene(io::path(Context.sceneeditorwindow->m_LoadFileDlg->getFileName()).c_str(), false);
				Context.sceneeditorwindow->OnSceneChange();
				Context.dialoglist->OnSceneChange();
				Context.animbanklist->OnSceneChange();
				Context.sceneeditorwindow->m_LoadFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.renderwindow->m_OpenFileDlg)
			{
				Context.renderwindow->LoadScene();
				Context.renderwindow->m_OpenFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.sceneeditorwindow->m_SaveFileDlg)
			{
				//m_SaveName = io::path(Context.sceneeditorwindow->m_SaveFileDlg->getFileName());
				irr::io::IFileSystem *fs = Context.device->getFileSystem();
				irr::core::stringc current = fs->getWorkingDirectory();
				m_SaveName = io::path(current) + "/";
				Rectangle2 rect = Context.sceneeditorwindow->m_SaveFileDlg->getRelativePosition();
				rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
				m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
				gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
				text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
				m_SaveFileNameEditBox = env->addEditBox(L"scene.scn", Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
				env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_SAVE_SCENE_FILE_NAME_DLG, L"OK");
				Context.sceneeditorwindow->m_SaveFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.sceneeditorwindow->m_OpenFileDlg )
			{
				if(Context.sceneeditorwindow->m_FileLoadType == SceneEditorWindow::FLT_TEXTURE)
				{
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Backgrounds);
					Texture* tex = Context.device->getVideoDriver()->getTexture(io::path(Context.sceneeditorwindow->m_OpenFileDlg->getFileName()));
					if(tex)
					{
 						KeyTime key(P2Zero);
						TArray<KeyTime> keys;
						keys.push_back(key);
						SmartPointer<ImageScene> imageScene(new ImageScene(tex, keys));
						Context.sceneinterface->GetCurrentDialog()->SetImageScene(imageScene);
						Context.sceneeditorwindow->UpdateBgBank();
					}
				}
				else if(Context.sceneeditorwindow->m_FileLoadType == SceneEditorWindow::FLT_SOUND_BG1)
				{
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sounds);
					io::path path = io::path(Context.sceneeditorwindow->m_OpenFileDlg->getFileName());
					const char* filename = (char*)(path.c_str());
					SmartPointer<AudioStream> sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile(filename);
					if(sound->IsValid())// sound is playable
					{
						//sound->Play();
						Context.sceneeditorwindow->SetSoundBG1(path);
					}
				}
				else if(Context.sceneeditorwindow->m_FileLoadType == SceneEditorWindow::FLT_SOUND_BG2)
				{
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sounds);
					io::path path = io::path(Context.sceneeditorwindow->m_OpenFileDlg->getFileName());
					const char* filename = (char*)(path.c_str());
					SmartPointer<AudioStream> sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile(filename);
					if(sound->IsValid())// sound is playable
					{
						//sound->Play();
						Context.sceneeditorwindow->SetSoundBG2(path);
					}
				}
				else if(Context.sceneeditorwindow->m_FileLoadType == SceneEditorWindow::FLT_SOUND_VOICE)
				{
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sounds);
					io::path path = io::path(Context.sceneeditorwindow->m_OpenFileDlg->getFileName());
					const char* filename = (char*)(path.c_str());
					SmartPointer<AudioStream> sound = Engine::GetInstance()->GetAudioDevice()->OpenSoundFile(filename);
					if(sound->IsValid())// sound is playable
					{
						//sound->Play();
						Context.sceneeditorwindow->SetSoundVoice(path);
					}
				}
				
				Context.sceneeditorwindow->m_OpenFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.dialoglist->m_OpenFileDlg )
			{
				if(Context.dialoglist->m_DialogFileType == DialogListBox::DFT_IMPORT)
				{
					Context.dialoglist->AddDialog(io::path(Context.dialoglist->m_OpenFileDlg->getFileName()).c_str());
					Context.dialoglist->m_OpenFileDlg = NULL;
				}
				else if(Context.dialoglist->m_DialogFileType == DialogListBox::DFT_EXPORT)
				{
					//m_SaveName = io::path(Context.editorwindow->m_SaveFileDlg->getFileName());
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					irr::core::stringc current = fs->getWorkingDirectory();
					m_SaveName = io::path(current) + "/";
					Rectangle2 rect = Context.dialoglist->m_OpenFileDlg->getRelativePosition();
					rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
					m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
					gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
					text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
					core::stringw filename = Context.sceneinterface->GetCurrentDialog()->GetDialogName();
					filename += L".dlg";
					m_SaveFileNameEditBox = env->addEditBox(filename.c_str(), Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
					env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_SAVE_DIALOG_FILE_NAME_DLG, L"OK");
					Context.dialoglist->m_OpenFileDlg = NULL;
				}
				else if(Context.dialoglist->m_DialogFileType == DialogListBox::DFT_XML_EXPORT)
				{
					//m_SaveName = io::path(Context.editorwindow->m_SaveFileDlg->getFileName());
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					irr::core::stringc current = fs->getWorkingDirectory();
					m_SaveName = io::path(current) + "/";
					Rectangle2 rect = Context.dialoglist->m_OpenFileDlg->getRelativePosition();
					rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
					m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
					gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
					text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
					core::stringw filename = Context.sceneinterface->GetCurrentDialog()->GetDialogName();
					filename += L".dlg";
					m_SaveFileNameEditBox = env->addEditBox(filename.c_str(), Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
					env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_XML_EXPORT_DIALOG_FILE_NAME_DLG, L"OK");
					Context.dialoglist->m_OpenFileDlg = NULL;
				}
			}
			break;
		case gui::EGET_DIRECTORY_SELECTED:
			/********************************* ANTI BUG *****************************************/
			if(wcslen(((gui::IGUIFileOpenDialog*)event.GUIEvent.Caller)->getFileName()) != 0)// we have selected a file, not a directory
			{
				break;// do nothing : we'll get soon the correct file selected event
			}
			/********************************* ANTI BUG *****************************************/
			else if ( event.GUIEvent.Caller == Context.spritebanklist->m_OpenFileDlg )
			{
				irr::io::IFileSystem *fs = Context.device->getFileSystem();
				irr::core::stringc current = fs->getWorkingDirectory();
				fs->changeWorkingDirectoryTo(Context.spritebanklist->m_OpenFileDlg->getDirectoryName());
				irr::io::IFileList* folder = fs->createFileList();

				for(u32 i = 0; i < folder->getFileCount(); ++i)
					Context.spritebanklist->AddTextureToBank(io::path(folder->getFileName(i)).c_str());

				fs->changeWorkingDirectoryTo(current);

				Context.spritebanklist->m_OpenFileDlg->remove();
				Context.spritebanklist->m_OpenFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.editorwindow->m_SaveFileDlg)
			{
				m_SaveName = Context.editorwindow->m_SaveFileDlg->getDirectoryName();
				Rectangle2 rect = Context.editorwindow->m_SaveFileDlg->getRelativePosition();
				rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
				m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
				gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
				text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
				m_SaveFileNameEditBox = env->addEditBox(L"sprite.spr", Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
				env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_SAVE_SPRITE_FILE_NAME_DLG, L"OK");
				Context.editorwindow->m_SaveFileDlg->remove();
				Context.editorwindow->m_SaveFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.animbanklist->m_OpenFileDlg )
			{
				if(Context.sceneeditorwindow->m_FileLoadType == AnimationBankListBox::SLT_SPRITE)
				{
					const wchar_t* str1 = Context.animbanklist->m_OpenFileDlg->getFileName();
					const io::path& str2 = Context.animbanklist->m_OpenFileDlg->getDirectoryName();
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					irr::core::stringc current = fs->getWorkingDirectory();
					fs->changeWorkingDirectoryTo(Context.animbanklist->m_OpenFileDlg->getDirectoryName());
					irr::io::IFileList* folder = fs->createFileList();

					for(u32 i = 0; i < folder->getFileCount(); ++i)
						Context.animbanklist->AddSpriteToBank(io::path(folder->getFileName(i)).c_str());

					u32 index = Context.sceneinterface->GetScene()->GetSprites().size() - 1;
					printf("Sprite %04d has been created : saving it before unloading\n", index);
					Context.sceneinterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);

					fs->changeWorkingDirectoryTo(current);

					Context.animbanklist->m_OpenFileDlg->remove();
					Context.animbanklist->m_OpenFileDlg = NULL;
				}
				else if(Context.animbanklist->m_SpriteLoadType == AnimationBankListBox::SLT_TEXTURE)
				{
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					irr::core::stringc current = fs->getWorkingDirectory();
					fs->changeWorkingDirectoryTo(Context.animbanklist->m_OpenFileDlg->getDirectoryName());
					irr::io::IFileList* folder = fs->createFileList();
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sprites);

					for(u32 i = 0; i < folder->getFileCount(); ++i)
					{
						Texture* tex = Context.device->getVideoDriver()->getTexture(io::path(folder->getFileName(i)).c_str());
						if(tex)
						{
							Dimension2 dim = tex->getSize();
							SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
							sprite->SetSize(dim);
							Context.sceneinterface->AddSpriteToBank(sprite);
							FrameSprite* frame = new FrameSprite();
							frame->SetSize(dim);
							sprite->AddFrame(frame);
							LayerSprite* layer = new LayerSprite();
							frame->AddLayer(layer);
							layer->SetImage(tex);
							sprite->AddImageToData(tex);
						}
					}
					Context.animbanklist->UpdateSpriteBank();
					u32 index = Context.sceneinterface->GetScene()->GetSprites().size() - 1;
					printf("Sprite %04d has been created : saving it before unloading\n", index);
					Context.sceneinterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);

					fs->changeWorkingDirectoryTo(current);
				}
				else if(Context.animbanklist->m_SpriteLoadType == AnimationBankListBox::SLT_BATCH_TEXTURE)
				{
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					irr::core::stringc current = fs->getWorkingDirectory();
					fs->changeWorkingDirectoryTo(Context.animbanklist->m_OpenFileDlg->getDirectoryName());
					irr::io::IFileList* folder = fs->createFileList();
					Context.sceneinterface->SaveResourcePath(SceneInterface::RPT_Sprites);

					SmartPointer<AnimatedSprite> sprite(new AnimatedSprite());
					Context.sceneinterface->AddSpriteToBank(sprite);
					Dimension2 maxDim(0,0);
					for(u32 i = 0; i < folder->getFileCount(); ++i)
					{
						Texture* tex = Context.device->getVideoDriver()->getTexture(io::path(folder->getFileName(i)).c_str());
						if(tex)
						{
							FrameSprite* frame = new FrameSprite();
							Dimension2 dim = tex->getSize();
							if(dim.Width > maxDim.Width)
								maxDim.Width = dim.Width;
							if(dim.Height > maxDim.Height)
								maxDim.Height = dim.Height;

							frame->SetSize(dim);
							sprite->AddFrame(frame);
							LayerSprite* layer = new LayerSprite();
							frame->AddLayer(layer);
							layer->SetImage(tex);
							sprite->AddImageToData(tex);
						}
					}
					sprite->SetSize(maxDim);
					sprite->Optimize();
					Context.animbanklist->UpdateSpriteBank();
					u32 index = Context.sceneinterface->GetScene()->GetSprites().size() - 1;
					printf("Sprite %04d has been created : saving it before unloading\n", index);
					Context.sceneinterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);

					fs->changeWorkingDirectoryTo(current);
				}

				Context.animbanklist->m_OpenFileDlg->remove();
				Context.animbanklist->m_OpenFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.animbanklist->m_SaveFileDlg )
			{
				io::path saveName = Context.animbanklist->m_SaveFileDlg->getDirectoryName();

				Context.animbanklist->ExportSpriteXML(saveName.c_str());
				
				Context.animbanklist->m_SaveFileDlg->remove();
				Context.animbanklist->m_SaveFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.dialoglist->m_OpenFileDlg )
			{
				if(Context.dialoglist->m_DialogFileType == DialogListBox::DFT_IMPORT)
				{
					irr::io::IFileSystem *fs = Context.device->getFileSystem();
					irr::core::stringc current = fs->getWorkingDirectory();
					fs->changeWorkingDirectoryTo(io::path(Context.dialoglist->m_OpenFileDlg->getFileName()).c_str());
					irr::io::IFileList* folder = fs->createFileList();

					for(u32 i = 0; i < folder->getFileCount(); ++i)
					{
						Context.dialoglist->AddDialog(io::path(folder->getFileName(i)).c_str());
					}

					fs->changeWorkingDirectoryTo(current);

					Context.dialoglist->m_OpenFileDlg->remove();
					Context.dialoglist->m_OpenFileDlg = NULL;
				}
				else if(Context.dialoglist->m_DialogFileType == DialogListBox::DFT_EXPORT)
				{
					m_SaveName = Context.dialoglist->m_OpenFileDlg->getDirectoryName();
					Rectangle2 rect = Context.dialoglist->m_OpenFileDlg->getRelativePosition();
					rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
					m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
					gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
					text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
					core::stringw filename = Context.sceneinterface->GetCurrentDialog()->GetDialogName();
					filename += L".dlg";
					m_SaveFileNameEditBox = env->addEditBox(filename.c_str(), Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
					env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_SAVE_DIALOG_FILE_NAME_DLG, L"OK");
					Context.dialoglist->m_OpenFileDlg->remove();
					Context.dialoglist->m_OpenFileDlg = NULL;
				}
				else if(Context.dialoglist->m_DialogFileType == DialogListBox::DFT_XML_EXPORT)
				{
					m_SaveName = Context.dialoglist->m_OpenFileDlg->getDirectoryName();
					Rectangle2 rect = Context.dialoglist->m_OpenFileDlg->getRelativePosition();
					rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
					m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
					gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
					text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
					core::stringw filename = Context.sceneinterface->GetCurrentDialog()->GetDialogName();
					filename += L".dlg";
					m_SaveFileNameEditBox = env->addEditBox(filename.c_str(), Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
					env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_XML_EXPORT_DIALOG_FILE_NAME_DLG, L"OK");
					Context.dialoglist->m_OpenFileDlg->remove();
					Context.dialoglist->m_OpenFileDlg = NULL;
				}
			}
			else if(event.GUIEvent.Caller == Context.sceneeditorwindow->m_SaveFileDlg)
			{
				m_SaveName = Context.sceneeditorwindow->m_SaveFileDlg->getDirectoryName();
				Rectangle2 rect = Context.sceneeditorwindow->m_SaveFileDlg->getRelativePosition();
				rect = Rectangle2(Position2(rect.getCenter().X - 128, rect.getCenter().Y - 32), Dimension2(256, 64+32));
				m_SaveFileNameDialog = env->addWindow(rect, true, L"Choisissez un nom de fichier à enregistrer");
				gui::IGUIStaticText* text = env->addStaticText(core::stringw(m_SaveName.c_str()).c_str(), Rectangle2(Position2(16, 16+8), Dimension2(256-16-16, 16)), false, true, m_SaveFileNameDialog);
				text->setToolTipText(core::stringw(m_SaveName.c_str()).c_str());
				m_SaveFileNameEditBox = env->addEditBox(L"scene.scn", Rectangle2(Position2(16,32+8), Dimension2(256-16-16, 16)), true, m_SaveFileNameDialog);
				env->addButton(Rectangle2(Position2(128-32, 64+32-16-8), Dimension2(64, 16)), m_SaveFileNameDialog, GUI_ID_SAVE_SCENE_FILE_NAME_DLG, L"OK");
				Context.sceneeditorwindow->m_SaveFileDlg->remove();
				Context.sceneeditorwindow->m_SaveFileDlg = NULL;
			}
			break;
		case gui::EGET_FILE_CHOOSE_DIALOG_CANCELLED:
			if ( event.GUIEvent.Caller == Context.spritebanklist->m_OpenFileDlg )
			{
				Context.spritebanklist->m_OpenFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.editorwindow->m_LoadFileDlg)
			{
				Context.editorwindow->m_LoadFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.editorwindow->m_SaveFileDlg)
			{
				Context.editorwindow->m_SaveFileDlg = NULL;
			}
			else if(event.GUIEvent.Caller == Context.renderwindow->m_OpenFileDlg)
			{
				Context.renderwindow->m_OpenFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.animbanklist->m_OpenFileDlg )
			{
				Context.animbanklist->m_OpenFileDlg = NULL;
			}
			else if ( event.GUIEvent.Caller == Context.dialoglist->m_OpenFileDlg )
			{
				Context.animbanklist->m_OpenFileDlg = NULL;
			}
			break;
		default:
			break;
		}
	}

	return false;
}




// Editor

Editor::Editor() : m_Accumulator(0.0f), m_EditorMode(EM_NONE)
{
	Engine* engine = Engine::GetInstance();
	m_Gui = engine->GetGui();

	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	/*
	To make the font a little bit nicer, we load an external font
	and set it as the new default font in the skin.
	To keep the standard font for tool tip text, we set it to
	the built-in font.
	*/

	//gui::IGUISkin* skin = m_Gui->getSkin();

	gui::IGUISkin *skin = m_Gui->createSkin(gui::EGST_WINDOWS_CLASSIC);

	gui::IGUIFont* font = m_Gui->getFont(engine->GetGamePath("system/system.bmp"));
	if (font)
		skin->setFont(font);
	
	skin->setFont(m_Gui->getBuiltInFont(), gui::EGDF_TOOLTIP);

	for (u32 i=0; i<gui::EGDC_COUNT ; ++i)
	{
		video::SColor col = skin->getColor((gui::EGUI_DEFAULT_COLOR)i);
		col.setAlpha(225);
		skin->setColor((gui::EGUI_DEFAULT_COLOR)i, col);
	}

	m_Gui->setSkin(skin);
	skin->drop();

	/*
	We add three buttons. The first one closes the engine. The second
	creates a window and the third opens a file open dialog. The third
	parameter is the id of the button, with which we can easily identify
	the button in the event receiver.
	*/	

// 	m_Gui->addButton(core::rect<s32>(10,240,110,240 + 32), 0, GUI_ID_QUIT_BUTTON,
// 			L"Quit", L"Exits Program");
// 	m_Gui->addButton(core::rect<s32>(10,280,110,280 + 32), 0, GUI_ID_NEW_WINDOW_BUTTON,
// 			L"New Window", L"Launches a new Window");
// 	m_Gui->addButton(core::rect<s32>(10,320,110,320 + 32), 0, GUI_ID_FILE_OPEN_BUTTON,
// 			L"File Open", L"Opens a file");

 	m_SpriteInterface = new SpriteInterface();
// 	m_SpriteInterface->GetSprite()->LoadSpriteFromFile("./media/test.spr");
	m_SceneInterface = new SceneInterface();

	gui::IGUIStaticText* text = NULL;
	Dimension2 dim = driver->getScreenSize();

	m_EditorWindow = new EditorWindow(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_EDITOR_WINDOW, L"", Rectangle2(Position2(VIEW_BORDER_LEFT+VIEW_LAYER_PREVIEW_SIZE+VIEW_SCROLL_BAR_THICKNESS+VIEW_SPACE_BETWEEN_ELEMENTS, VIEW_BORDER_TOP), Dimension2(dim.Width - VIEW_LAYER_PREVIEW_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_LEFT - VIEW_SPRITE_BANK_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_RIGHT - VIEW_SPACE_BETWEEN_ELEMENTS-VIEW_SPACE_BETWEEN_ELEMENTS, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM - VIEW_TIMELINE_HEIGHT - VIEW_SPACE_BETWEEN_ELEMENTS)));
	m_EditorWindow->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_EditorWindow->SetSpriteInterface(m_SpriteInterface);
	m_EditorWindow->OnSpriteChange();
	m_EditorWindow->drop();

	m_LayerMenu = new LayerMenu(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_LAYER_MENU, L"Layers", Rectangle2(Position2(VIEW_BORDER_LEFT, VIEW_BORDER_TOP), Dimension2(VIEW_LAYER_PREVIEW_SIZE+VIEW_SCROLL_BAR_THICKNESS, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM - VIEW_TIMELINE_HEIGHT - VIEW_SPACE_BETWEEN_ELEMENTS)));
	m_LayerMenu->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_LayerMenu->SetSpriteInterface(m_SpriteInterface);
	m_LayerMenu->SetEditorWindow(m_EditorWindow);
	m_LayerMenu->drop();

	m_FrameScrollMenu = new FrameScrollMenu(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_FRAME_SCROLL, L"Frames", Rectangle2(Position2(VIEW_BORDER_LEFT, dim.Height - VIEW_TIMELINE_HEIGHT-VIEW_BORDER_BOTTOM), Dimension2(dim.Width - VIEW_BORDER_LEFT - VIEW_SPRITE_BANK_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_RIGHT - VIEW_SPACE_BETWEEN_ELEMENTS, VIEW_TIMELINE_HEIGHT)));
	m_FrameScrollMenu->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT);
	m_FrameScrollMenu->SetSpriteInterface(m_SpriteInterface);
	m_FrameScrollMenu->SetLayerMenu(m_LayerMenu);
	m_FrameScrollMenu->SetEditorWindow(m_EditorWindow);
	m_FrameScrollMenu->drop();

	m_SpriteBankList = new SpriteBankListBox(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_SPRITE_BANK, L"Banque d'images", Rectangle2(Position2(dim.Width-VIEW_SPRITE_BANK_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_RIGHT, VIEW_BORDER_TOP), Dimension2(VIEW_SPRITE_BANK_SIZE+VIEW_SCROLL_BAR_THICKNESS, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM)));
	m_SpriteBankList->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_SpriteBankList->SetSpriteInterface(m_SpriteInterface);
	m_SpriteBankList->SetLayerMenu(m_LayerMenu);
	m_SpriteBankList->SetEditorWindow(m_EditorWindow);
	m_SpriteBankList->SetFrameScrollMenu(m_FrameScrollMenu);
	m_SpriteBankList->drop();

	
	m_SceneEditorWindow = new SceneEditorWindow(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_SCENE_EDITOR, L"", Rectangle2(Position2(VIEW_BORDER_LEFT+VIEW_DIALOG_LIST_SIZE+VIEW_SCROLL_BAR_THICKNESS+VIEW_SPACE_BETWEEN_ELEMENTS, VIEW_BORDER_TOP), Dimension2(dim.Width - VIEW_DIALOG_LIST_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_LEFT - VIEW_ANIM_BANK_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_RIGHT - VIEW_SPACE_BETWEEN_ELEMENTS-VIEW_SPACE_BETWEEN_ELEMENTS, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM)));
	m_SceneEditorWindow->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_SceneEditorWindow->SetSceneInterface(m_SceneInterface);
	m_SceneEditorWindow->OnSceneChange();
	m_SceneEditorWindow->drop();

	m_DialogList = new DialogListBox(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_DIALOG_LIST, L"Dialogues", Rectangle2(Position2(VIEW_BORDER_LEFT, VIEW_BORDER_TOP), Dimension2(VIEW_DIALOG_LIST_SIZE+VIEW_SCROLL_BAR_THICKNESS, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM)));
	m_DialogList->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_DialogList->SetSceneInterface(m_SceneInterface);
	m_DialogList->drop();

	m_AnimBankList = new AnimationBankListBox(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_ANIM_BANK, L"Banque de sprites", Rectangle2(Position2(dim.Width-VIEW_ANIM_BANK_SIZE-VIEW_SCROLL_BAR_THICKNESS-VIEW_BORDER_RIGHT, VIEW_BORDER_TOP), Dimension2(VIEW_ANIM_BANK_SIZE+VIEW_SCROLL_BAR_THICKNESS, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM)));
	m_AnimBankList->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_AnimBankList->SetSceneInterface(m_SceneInterface);
	m_AnimBankList->drop();


	m_SceneEditorWindow->SetDialogList(m_DialogList);
	m_SceneEditorWindow->SetAnimationBankList(m_AnimBankList);
	m_DialogList->SetSceneEditorWindow(m_SceneEditorWindow);
	m_DialogList->SetAnimationBankList(m_AnimBankList);
	m_AnimBankList->SetSceneEditorWindow(m_SceneEditorWindow);
	m_AnimBankList->SetDialogList(m_DialogList);


	m_RenderWindow = new RenderWindow(m_Gui, m_Gui->getRootGUIElement(), GUI_ID_RENDER, L"Aperçu", Rectangle2(Position2(VIEW_BORDER_LEFT, VIEW_BORDER_TOP), Dimension2(dim.Width - VIEW_BORDER_LEFT-VIEW_BORDER_RIGHT, dim.Height - VIEW_BORDER_TOP-VIEW_BORDER_BOTTOM)));
	m_RenderWindow->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_RenderWindow->drop();

//	m_Gui->addEditBox(L"Editable Text", core::rect<s32>(350, 80, 550, 100));

	m_ToolBar = m_Gui->addToolBar();
	m_ToolBar->addButton(GUI_ID_BUTTON_SCENE_MODE, L"Scene");
	m_ToolBar->addButton(GUI_ID_BUTTON_SPRITE_MODE, L"Sprite");
	m_ToolBar->addButton(GUI_ID_BUTTON_RENDER_MODE, L"Aperçu");
	
	// Store the appropriate data in a context structure.
	SAppContext context;
	context.device = device;
	context.counter = 0;
	context.spritebanklist = m_SpriteBankList;
	context.framescrollmenu = m_FrameScrollMenu;
	context.layermenu = m_LayerMenu;
	context.editorwindow = m_EditorWindow;
	context.animbanklist = m_AnimBankList;
	context.dialoglist = m_DialogList;
	context.sceneeditorwindow = m_SceneEditorWindow;
	context.renderwindow = m_RenderWindow;
	context.spriteinterface = m_SpriteInterface;
	context.sceneinterface = m_SceneInterface;

	// Then create the event receiver, giving it that context structure.
	m_EventReceiver = new MyEventReceiver(context);

	// And tell the device to use our custom event receiver.
	device->setEventReceiver(m_EventReceiver);


	ChangeMode(Editor::EM_SCENE);
}

Editor::~Editor()
{
	//m_SpriteInterface->GetSprite()->SaveSpriteToFile("test.spr");
	SafeDelete(m_EventReceiver);
	SafeDelete(m_SpriteInterface);
	SafeDelete(m_SceneInterface);
}

Editor* Editor::GetInstance()
{
	if(!ms_Instance)
		ms_Instance = new Editor();
	return ms_Instance;
}

void Editor::DeleteInstance()
{
	SafeDelete(ms_Instance);
}

void Editor::Update(float dt)
{
	if(m_EditorMode == EM_SPRITE)
	{
		m_EditorWindow->UpdateRenderTarget(dt);

		m_Accumulator += dt;
		float framerate = m_SpriteInterface->GetSprite()->GetFramerate();
		if(m_Accumulator > framerate)
		{
			m_Accumulator -= framerate;
			m_FrameScrollMenu->GoNextFrame();
		}
	}
	else if(m_EditorMode == EM_SCENE)
	{
		if(m_AnimBankList->m_SpriteToEdit.IsValid())// load the sprite to edit in the sprite editor
		{
			m_SpriteInterface->EditSprite(m_AnimBankList->m_SpriteToEdit);
			ChangeMode(EM_SPRITE);
			m_AnimBankList->m_SpriteToEdit = NULL;

			m_EditorWindow->OnSpriteChange();
			m_SpriteBankList->OnSpriteChange();
			m_LayerMenu->OnSpriteChange();
			m_FrameScrollMenu->OnSpriteChange();
		}
		else// normal update
		{
			m_SceneEditorWindow->UpdateRenderTarget(dt);
		}
	}
	else if(m_EditorMode == EM_RENDER)
	{
		m_RenderWindow->UpdateRenderTarget(dt);
	}
}

void Editor::ChangeMode(EditorMode mode)
{
	if(m_EditorMode == mode)
		return;

	switch(mode)
	{
	case EM_NONE:
		{
			m_EditorWindow->setEnabled(false);
			m_EditorWindow->setVisible(false);
			m_FrameScrollMenu->setEnabled(false);
			m_FrameScrollMenu->setVisible(false);
			m_LayerMenu->setEnabled(false);
			m_LayerMenu->setVisible(false);
			m_SpriteBankList->setEnabled(false);
			m_SpriteBankList->setVisible(false);

			m_AnimBankList->setEnabled(false);
			m_AnimBankList->setVisible(false);
			m_DialogList->setEnabled(false);
			m_DialogList->setVisible(false);
			m_SceneEditorWindow->setEnabled(false);
			m_SceneEditorWindow->setVisible(false);

			m_RenderWindow->setEnabled(false);
			m_RenderWindow->setVisible(false);
		}
		break;
	case EM_SCENE:
		{
			m_EditorWindow->setEnabled(false);
			m_EditorWindow->setVisible(false);
			m_FrameScrollMenu->setEnabled(false);
			m_FrameScrollMenu->setVisible(false);
			m_LayerMenu->setEnabled(false);
			m_LayerMenu->setVisible(false);
			m_SpriteBankList->setEnabled(false);
			m_SpriteBankList->setVisible(false);

			m_AnimBankList->setEnabled(true);
			m_AnimBankList->setVisible(true);
			
			m_DialogList->setEnabled(true);
			m_DialogList->setVisible(true);
			m_SceneEditorWindow->setEnabled(true);
			m_SceneEditorWindow->setVisible(true);

			m_RenderWindow->setEnabled(false);
			m_RenderWindow->setVisible(false);

			if(m_EditorMode == EM_SPRITE) // regenerate sprite preview texture in the scene editor
			{
				SmartPointer<AnimatedSprite>& spr = m_SpriteInterface->GetSprite();
				if(m_SceneInterface->GetScene())
				{
					s32 index = m_SceneInterface->GetScene()->GetSprites().linear_search(spr);
					if(index > 0)
					{
						const SmartPointer<AnimatedSprite>& spr = m_SceneInterface->GetScene()->GetSpriteAtIndex(index);
						MD5 md5 = spr->GetMD5();
						if(m_SceneInterface->m_Data->m_SpritesMD5[index] != md5._raw)							
						{
							SAFE_UNLOAD(m_SceneInterface->m_Data->m_SpriteBankPreviews[index]);
							m_SceneInterface->m_Data->m_SpritesMD5[index] = md5._raw;

							irr::io::IFileSystem *fs = Engine::GetInstance()->GetDevice()->getFileSystem();
							io::path path = fs->getWorkingDirectory();
							printf("Sprite %04d has been modified : saving it before returning to scene editor\n", index);
							m_SceneInterface->GetScene()->SaveSpriteFromIndex(Engine::GetInstance()->GetGamePath("temp/").c_str(), index);
							bool ret = fs->changeWorkingDirectoryTo(path);

							m_AnimBankList->UpdateSpriteBank();
						}
					}
				}
			}
			
		}
		break;
	case EM_SPRITE:
		{
			m_EditorWindow->setEnabled(true);
			m_EditorWindow->setVisible(true);
			m_FrameScrollMenu->setEnabled(true);
			m_FrameScrollMenu->setVisible(true);
			m_LayerMenu->setEnabled(true);
			m_LayerMenu->setVisible(true);
			m_SpriteBankList->setEnabled(true);
			m_SpriteBankList->setVisible(true);

			m_AnimBankList->setEnabled(false);
			m_AnimBankList->setVisible(false);
			m_DialogList->setEnabled(false);
			m_DialogList->setVisible(false);
			m_SceneEditorWindow->setEnabled(false);
			m_SceneEditorWindow->setVisible(false);

			m_RenderWindow->setEnabled(false);
			m_RenderWindow->setVisible(false);
		}
		break;
	case EM_RENDER:
		{
			// always reset the scene cause the dialogs use by the scene 
			// could have been modified but we don't have the corresponding scene data updated
			m_RenderWindow->ResetSceneInterface();

			m_EditorWindow->setEnabled(false);
			m_EditorWindow->setVisible(false);
			m_FrameScrollMenu->setEnabled(false);
			m_FrameScrollMenu->setVisible(false);
			m_LayerMenu->setEnabled(false);
			m_LayerMenu->setVisible(false);
			m_SpriteBankList->setEnabled(false);
			m_SpriteBankList->setVisible(false);

			m_AnimBankList->setEnabled(false);
			m_AnimBankList->setVisible(false);
			m_DialogList->setEnabled(false);
			m_DialogList->setVisible(false);
			m_SceneEditorWindow->setEnabled(false);
			m_SceneEditorWindow->setVisible(false);

			m_RenderWindow->setEnabled(true);
			m_RenderWindow->setVisible(true);
		}
		break;
	default:
		return;
	}

	m_EditorMode = mode;
}

#undef GUI_ID_SAVE_SPRITE_FILE_NAME_DLG
#undef GUI_ID_SAVE_SCENE_FILE_NAME_DLG
#undef GUI_ID_SAVE_DIALOG_FILE_NAME_DLG
#undef GUI_ID_POPUP_DLG
#undef GUI_ID_XML_EXPORT_DIALOG_FILE_NAME_DLG
#undef GUI_ID_BUTTON_SPRITE_MODE
#undef GUI_ID_BUTTON_SCENE_MODE
#undef GUI_ID_BUTTON_RENDER_MODE

