#ifndef UTILS_H
#define UTILS_H

#include <irrlicht.h>
#include <assert.h>

// #ifdef _DEBUG
// #include <vld.h>
// #endif
//

#ifdef _IRR_WINDOWS_
#include <windows.h>
#include <direct.h>
#define sprintf sprintf_s
#define wtoi(str, base) wcstol(str, NULL, base)
#define wtof(str) (float)wcstod(str, NULL)
#define mkdir _mkdir
#else
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <wchar.h>
#define mkdir(path) mkdir(path, 0777)
#define wtoi(str, base) wcstol(str, NULL, base)
#define wtof(str) (float)wcstod(str, NULL)
#endif

using namespace irr;

typedef core::vector2df Vector2;
typedef core::vector3df Vector3;
typedef core::quaternion Quaternion;
typedef core::matrix4 Matrix4;
typedef core::dimension2d<u32> Dimension2;
typedef core::rect<s32> Rectangle2;
typedef core::position2d<s32> Position2;
typedef video::ITexture Texture;
typedef video::IImage Image;
typedef video::SColor IColor;

// #define V3Zero MathUtils::GetV3Zero()
// #define M4Zero MathUtils::GetM4Zero()
// #define M3Zero MathUtils::GetM3Zero()

#define V3Zero MathUtils::ms_V3Zero
#define M4Zero MathUtils::ms_M4Zero

#define P2Zero MathUtils::ms_P2Zero
#define D2Zero MathUtils::ms_D2Zero
#define COLOR_WHITE MathUtils::ms_IColor_White
#define COLOR_BLACK MathUtils::ms_IColor_Black
#define COLOR_TRANSPARENT MathUtils::ms_IColor_Transparent

extern IColor ThemeColor;
extern IColor ThemeColorGlow;
extern IColor ThemeColorSelection; 
#define COMPUTE_THEME_COLOR(alpha) (ThemeColor.color|((alpha)<<24))
#define COMPUTE_THEME_COLOR_FONT_GLOW(alpha) (ThemeColorGlow.color|((alpha)<<24))// for gauss
#define COMPUTE_THEME_COLOR_FONT_SELECTION(alpha) (ThemeColorSelection.color|((alpha)<<24))// for selection

#define Epsilon 0.00001f

#define TArray core::array
// template <class T, typename TAlloc = irrAllocator<T> >
// using TArray = core::array<T, irrAllocator<T> >;

#define NB_ZOOM_FACTOR 19


#define SAFE_LOAD_IMAGE(driver, image, path, usePremultipliedAlpha) if(!image) \
{ \
	image = driver->getTexture(path); \
	if(image) \
	{ \
		image->grab(); \
		driver->removeTexture(image); \
		if(usePremultipliedAlpha) \
			PremultiplyAlpha(image); \
	} \
} \

#define SAFE_UNLOAD(res) if(res) \
{ \
	res->drop(); \
	res = NULL; \
} \

/*#define SerializeStringW(str, file) { \
	u32 size = str.size(); \
	fwrite(&size, sizeof(u32), 1, file); \
	if(size > 0) \
	{ \
		const wchar_t* buff = str.c_str(); \
		fwrite(buff, sizeof(wchar_t), size, file); \
	} \
} \

#define SerializeStringC(str, file) { \
	u32 size = str.size(); \
	fwrite(&size, sizeof(u32), 1, file); \
	if(size > 0) \
	{ \
		const char* buff = str.c_str(); \
		fwrite(buff, sizeof(char), size, file); \
	} \
} \

#define DeserializeStringW(str, file) { \
	u32 size = 0; \
	fread(&size, sizeof(u32), 1, file); \
	if(size > 0) \
	{ \
		wchar_t* buff = new wchar_t[size+1]; \
		buff[size] = 0; \
		fread(buff, sizeof(wchar_t), size, file); \
		(str) = buff; \
		SafeDeleteArray(buff); \
	} \
} \

#define DeserializeStringC(str, file) { \
	u32 size = 0; \
	fread(&size, sizeof(u32), 1, file); \
	if(size > 0) \
	{ \
		char* buff = new char[size+1]; \
		buff[size] = 0; \
		fread(buff, sizeof(char), size, file); \
		(str) = buff; \
		SafeDeleteArray(buff); \
	} \
} \
*/
//#define USE_PREMULTIPLIED_ALPHA 1
extern bool USE_PREMULTIPLIED_ALPHA;


template< class T > void SafeDelete( T*& pVal )
{
	delete pVal;
	pVal = NULL;
}

template< class T > void SafeDeleteArray( T*& pVal )
{
	delete[] pVal;
	pVal = NULL;
}

template< class T > T SquareOf( const T& val )
{
	return val*val;
}

template< class T > T CubeOf( const T& val )
{
	return val*val*val;
}

template< class T > T PowOf( const T& val, unsigned int exp )
{
	if(exp == 0)
		return 1;

	return val * PowOf(val, --exp);
}

void SerializeStringW(const core::stringw& str, FILE* file);
void SerializeStringC(const core::stringc& str, FILE* file);
void DeserializeStringW(core::stringw& str, FILE* file);
void DeserializeStringC(core::stringc& str, FILE* file);

class ReferenceCounter
{
private:
	int m_Count; // Reference count

public:
	void Grab()
	{
		// Increment the reference count
		++m_Count;
	}

	int Drop()
	{
		// Decrement the reference count and
		// return the reference count.
		return --m_Count;
	}

	int GetRefCount() const
	{
		return m_Count;
	}
};

template < typename T >
class SmartPointer
{
private:
	T*    m_Data;       // pointer
	ReferenceCounter* m_RefCounter; // Reference count

public:
	SmartPointer() : m_Data(0), m_RefCounter(0)
	{
		// Create a new reference
		m_RefCounter = new ReferenceCounter();
		// Increment the reference count
		m_RefCounter->Grab();
	}

	SmartPointer(T* pValue) : m_Data(pValue), m_RefCounter(0)
	{
		// Create a new reference
		m_RefCounter = new ReferenceCounter();
		// Increment the reference count
		m_RefCounter->Grab();
	}

	SmartPointer(const SmartPointer<T>& sp) : m_Data(sp.m_Data), m_RefCounter(sp.m_RefCounter)
	{
		// Copy constructor
		// Copy the data and reference pointer
		// and increment the reference count
		m_RefCounter->Grab();
	}

	~SmartPointer()
	{
		// Destructor
		// Decrement the reference count
		// if reference become zero delete the data
		if(m_RefCounter->Drop() == 0)
		{
			delete m_Data;
			delete m_RefCounter;
		}
	}

	int GetRefCount() const
	{
		return m_RefCounter?m_RefCounter->GetRefCount():0;
	}

	bool IsValid() const
	{
		return (m_Data && m_RefCounter && m_RefCounter->GetRefCount() > 0);
	}

	T& operator* ()
	{
		return *m_Data;
	}

	T& operator* () const
	{
		return *m_Data;
	}

	T* operator-> ()
	{
		return m_Data;
	}

	T* operator-> () const
	{
		return m_Data;
	}

	SmartPointer<T>& operator = (const SmartPointer<T>& sp)
	{
		// Assignment operator
		if (this != &sp) // Avoid self assignment
		{
			// Decrement the old reference count
			// if reference become zero delete the old data
			if(m_RefCounter->Drop() == 0)
			{
				delete m_Data;
				delete m_RefCounter;
			}

			// Copy the data and reference pointer
			// and increment the reference count
			m_Data = sp.m_Data;
			m_RefCounter = sp.m_RefCounter;
			m_RefCounter->Grab();
		}
		return *this;
	}

	bool operator == (const SmartPointer<T>& sp) const
	{
		return (m_Data == sp.m_Data);
	}

	bool operator != (const SmartPointer<T>& sp) const
	{
		return (m_Data != sp.m_Data);
	}
};

template < typename T >
class SharedPointer
{
private:
	T**    m_Data;       // pointer of pointer
	ReferenceCounter* m_RefDataCounter; // Reference count for data
	ReferenceCounter* m_RefCounter; // Reference count for pointer
	bool m_IsOwner;// can't become owner if non initialized with ownership

public:
	SharedPointer(T* pValue = NULL, bool isOwner = true) : m_Data(NULL), m_RefCounter(NULL), m_RefDataCounter(NULL), m_IsOwner(isOwner)
	{
		m_Data = new T*(pValue);// make our pointer of data pointer point on the value's pointer

		if(isOwner)
		{
			// Create a new reference for the value
			m_RefDataCounter = new ReferenceCounter();
			// Increment the reference count
			m_RefDataCounter->Grab();
		}

		// Create a new reference for our pointer of pointer
		m_RefCounter = new ReferenceCounter();
		// Increment the reference count
		m_RefCounter->Grab();
	}

	SharedPointer(const SharedPointer<T>& sp, bool isOwner = true) : m_Data(sp.m_Data), m_RefCounter(sp.m_RefCounter), m_RefDataCounter(NULL), m_IsOwner(isOwner)
	{
		// Copy constructor
		// Copy the data and reference pointer
		// and increment the reference count
		m_RefCounter->Grab();

		if(isOwner)
		{
			m_RefDataCounter = sp.m_RefDataCounter;
			m_RefDataCounter->Grab();
		}
	}

	~SharedPointer()
	{
		// Destructor
		// Decrement the reference count of the object if exists (else we are not the owner so we do nothing else for the pointer)
		// if reference become zero delete the data
		if(m_IsOwner && m_RefDataCounter && m_RefDataCounter->Drop() == 0)
		{
			assert(m_RefCounter->GetRefCount() > m_RefDataCounter->GetRefCount());// we can't have more references on the object than the refs on the pointer itself
			delete *m_Data;// delete the data
			delete m_RefDataCounter;
			*m_Data = NULL;// make it point on NULL for those who don't own it
			m_RefDataCounter = NULL;
		}

		// Decrement the reference count of the pointer
		// if reference become zero delete the pointer too
		if(m_RefCounter->Drop() == 0)
		{
			delete m_Data;// delete the pointer
			delete m_RefCounter;
			m_Data = NULL;// point NULL to be sure
			m_RefCounter = NULL;
		}
	}

	const bool GetIsOwner() const {return m_IsOwner;}
	void DropOwnership()
	{
		if(!m_IsOwner)
			return;

		if(m_RefDataCounter && m_RefDataCounter->Drop() == 0)// I was the last owner of this data and I want to give back ownership : delete all
		{
			delete *m_Data;// delete the data
			delete m_RefDataCounter;
			*m_Data = NULL;// make it point on NULL for those who don't own it
		}

		m_RefDataCounter = NULL;// in any case : drop the refdatacounter (deleted or not depending code above)

		m_IsOwner = false;
	}

	int GetRefCount() const
	{
		return m_RefCounter?m_RefCounter->GetRefCount():0;
	}
	int GetRefDataCount() const
	{
		return m_RefDataCounter?m_RefDataCounter->GetRefCount():0;
	}

	bool IsValid() const
	{
		return (*m_Data && m_RefCounter && m_RefCounter->GetRefCount() > 0);
	}

	T& operator* ()
	{
		return **m_Data;
	}

	T& operator* () const
	{
		return **m_Data;
	}

	T* operator-> ()
	{
		return *m_Data;
	}

	T* operator-> () const
	{
		return *m_Data;
	}

	SharedPointer<T>& operator = (const SharedPointer<T>& sp)
	{
		// Assignment operator
		if (this != &sp) // Avoid self assignment
		{
			// Decrement the reference count of the object if exists (else we are not the owner so we do nothing else for the pointer)
			// if reference become zero delete the data
			if(m_IsOwner && m_RefDataCounter && m_RefDataCounter->Drop() == 0)
			{
				assert(m_RefCounter->GetRefCount() > m_RefDataCounter->GetRefCount());// we can't have more references on the object than the refs on the pointer itself
				delete *m_Data;// delete the data
				delete m_RefDataCounter;
				*m_Data = NULL;// make it point on NULL for those who don't own it
				m_RefDataCounter = NULL;
			}
			assert(!m_RefDataCounter && !*m_Data);// I am not supposed to have any data here

			if(m_IsOwner)
			{
				delete m_Data;// delete the data pointer
				delete m_RefCounter;
				m_Data = NULL;
				m_RefCounter = NULL;
			}

			// Copy the data pointer and reference pointer
			// and increment the reference count
			m_Data = sp.m_Data;
			m_RefCounter = sp.m_RefCounter;
			m_RefCounter->Grab();
			if(m_IsOwner)
			{
				m_RefDataCounter = sp.m_RefDataCounter;
				m_RefDataCounter->Grab();
			}
		}
		return *this;
	}

	bool operator == (const SharedPointer<T>& sp) const
	{
		return (*m_Data == *(sp.m_Data));
	}

	bool operator != (const SharedPointer<T>& sp) const
	{
		return (*m_Data != *(sp.m_Data));
	}
};

struct KeyTime;

struct DeprecatedKeyTime
{
	Position2 m_Position;
	float m_Time;
	float m_Scale;
	float m_Rotation;

private:
	DeprecatedKeyTime() {}
	~DeprecatedKeyTime() {}
};

struct KeyTime
{
	KeyTime() {}
	KeyTime(const Position2& pos, float time = 0.0f, float scale = 1.0f, float rotation = 0.0f, IColor color = IColor(255, 255, 255, 255)):
		m_Position(pos), m_Time(time), m_Scale(scale), m_Rotation(rotation), m_Color(color) {}
	~KeyTime() {}

	bool operator >(const KeyTime& K) const {return (m_Time > K.m_Time);}
	bool operator <(const KeyTime& K) const
	{
		return (m_Time < K.m_Time);
	}
	bool operator ==(const KeyTime& K) const {return (m_Time == K.m_Time);}

	core::matrix4 GetMatrixTransform()
	{
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_Position.X, (f32)m_Position.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(m_Scale, m_Scale, 0.0f));
		return mat;
	}

	Position2 m_Position;
	float m_Time;
	float m_Scale;
	float m_Rotation;
	IColor m_Color;
};

class MathUtils
{
public:
	// 	static const Vector3& GetV3Zero() const {return ms_V3Zero;}
	// 	static const Matrix4& GetM4Zero() const {return ms_M4Zero;}
	// 	static const Matrix3& GetM3Zero() const {return ms_M3Zero;}

	static const Vector3& GetMatrixRight(const Matrix4& M)
	{
		return *((Vector3*)(&M.pointer()[0]));
	}

	static const Vector3& GetMatrixUp(const Matrix4& M)
	{
		return *((Vector3*)(&M.pointer()[4]));
	}

	static const Vector3& GetMatrixForward(const Matrix4& M)
	{
		return *((Vector3*)(&M.pointer()[8]));
	}

	static void GetMatrixAxis(const Matrix4& M, Vector3& outRight, Vector3& outUp, Vector3& outForward)
	{
		const f32* p = M.pointer();
		memcpy(&outRight, &p[0], 3*sizeof(f32));
		memcpy(&outUp, &p[4], 3*sizeof(f32));
		memcpy(&outForward, &p[8], 3*sizeof(f32));
	}

	static KeyTime GetInterpolatedKeyTime(const TArray<KeyTime>& keys, float time, bool loop = false);

	static IColor GetInterpolatedColor(IColor colorStart, IColor colorEnd, float ratio)
	{
		ratio = core::clamp(ratio, 0.0f, 1.0f);
		const float inv = 1.0f - ratio;

		return IColor((u32)core::round32(colorStart.getAlpha()*inv + colorEnd.getAlpha()*ratio),
			(u32)core::round32(colorStart.getRed()*inv + colorEnd.getRed()*ratio),
			(u32)core::round32(colorStart.getGreen()*inv + colorEnd.getGreen()*ratio),
			(u32)core::round32(colorStart.getBlue()*inv + colorEnd.getBlue()*ratio));
	}

	static IColor GetInverseColor(IColor color, bool inverseAlpha = false)
	{
		return IColor(inverseAlpha? (255 - color.getAlpha()) : color.getAlpha(),
			(255 - color.getRed()),
			(255 - color.getGreen()),
			(255 - color.getBlue()));
	}

	static float Gaussian(float x, float mu, float sigma)
	{
		return exp( -(((x-mu)/(sigma))*((x-mu)/(sigma)))/2.0f );
	}

	//private:
	static const Vector3 ms_V3Zero;
	static const Matrix4 ms_M4Zero;

	static const Position2 ms_P2Zero;
	static const Dimension2 ms_D2Zero;
	static const IColor ms_IColor_White;
	static const IColor ms_IColor_Black;
	static const IColor ms_IColor_Transparent;

	static const float ms_ZoomFactors[NB_ZOOM_FACTOR];

};

Image* TextureToImage(Texture* texture);
Texture* ImageToTexture(Image* image, core::stringc name);

int PreProcessAlphaPixels(unsigned char* data, unsigned int size);
int RLECompressColor(unsigned char* inData, unsigned int size, unsigned char* outData, unsigned int* outSize);// returns -1 if fail, 0 if not compressed and 1 if compressed
int RLEDecompressColors(unsigned char* inData, unsigned int size, unsigned char* outData, unsigned int outSize);// returns -1 if some errors occured

bool RLECompressFile(unsigned char* inData, unsigned int size, unsigned char* outData, unsigned int* outSize);

Texture* ResizeTexture(Texture* texture, const Dimension2& dim);

struct S2DImageData
{
	irr::core::rect<irr::s32> SourceRect;
	irr::core::matrix4 DestMatrix;
	irr::video::SColor VertexColor;
};

void drawBrightnessLayer(irr::video::IVideoDriver *driver, irr::core::recti position, float brightness);

void draw2DImage(irr::video::IVideoDriver *driver, irr::video::ITexture* texture , irr::core::rect<irr::s32> sourceRect, 
				 irr::core::position2d<irr::s32> position, irr::core::vector2df scale, bool useAlphaChannel, 
				 irr::video::SColor color, bool useFiltering = true, bool usePremultipliedAlpha = false, irr::core::rect<irr::s32>* clip = NULL);
void draw2DImage(irr::video::IVideoDriver *driver, irr::video::ITexture* texture , irr::core::rect<irr::s32> sourceRect, 
				 irr::core::position2d<irr::s32> position, irr::core::position2d<irr::s32> rotationPoint, irr::f32 rotation, 
				 irr::core::vector2df scale, bool useAlphaChannel, irr::video::SColor color, bool useFiltering = true, 
				 bool usePremultipliedAlpha = false, irr::core::rect<irr::s32>* clip = NULL);
void draw2DImage(irr::video::IVideoDriver *driver, irr::video::ITexture* texture, irr::core::rect<irr::s32> sourceRect, 
				 const core::matrix4& mat, bool useAlphaChannel, irr::video::SColor color, bool useFiltering = true, 
				 bool usePremultipliedAlpha = false, irr::core::rect<irr::s32>* clip = NULL);
void draw2DImageBatch(irr::video::IVideoDriver *driver, irr::video::ITexture* texture, const irr::core::array<S2DImageData>& imageData, 
					  bool useAlphaChannel, bool useFiltering = true, bool usePremultipliedAlpha = false);
void draw2DRect(video::IVideoDriver *driver, Dimension2 sourceDim, const core::matrix4& mat, video::SColor color = COLOR_WHITE);
void fill2DRect(irr::video::IVideoDriver *driver, const irr::core::rect<s32>& position, irr::video::SColor colorLeftUp, 
				irr::video::SColor colorRightUp, irr::video::SColor colorLeftDown, irr::video::SColor colorRightDown, 
				bool usePremultipliedAlpha = false);

u32 PremultiplyAlpha(u32 color);
void PremultiplyAlpha(irr::video::ITexture* texture);

irr::core::stringc StringFormat(const char * fmt,...);
irr::core::stringw StringFormat(const wchar_t * fmt,...);

void FormatString(core::stringw& strOut, const core::stringw& str, gui::IGUIFont* font, const Dimension2& dim, bool ignoreBottom = false);
void GetStringEndl(const core::stringw& str, core::array<u32>& endlOffsetsOut);
core::stringw SubStringLines(const core::stringw& str, u32 lineStart, u32 nbLines = 0, u32* startOut = NULL, u32* endOut = NULL, u32* nbTotalLines = NULL);

#ifdef CopyFile
#undef CopyFile
#endif
bool CopyFile(const char* srcName, const char* destName, bool overwrite = true);

Position2 GetShakeOffset(float time, float renderScale = 1.0f, float strengh = 1.0f, float frequency = 1.0f);

class Slider
{
public:
	Slider() : m_Value(0.0f) {if(!ms_IsInitialized) Initialize();}
	~Slider() {}

	void SetValue(float value) {m_Value = core::clamp(value, 0.0f, 1.0f);}
	float GetValue() const {return m_Value;}

	void Render(const Rectangle2& position, IColor color = COLOR_WHITE);

	static void Initialize();
	static void Cleanup();

protected:
	enum ElementType {ET_UNDER, ET_OVER, ET_SLIDER, ET_MAX};
	static bool ms_IsInitialized;
	static Texture* ms_Elements[ET_MAX];// under, over and slider

	float m_Value;

};

class ScrollBar
{
public:
	ScrollBar() : m_Size(0), m_Start(0), m_End(0) {if(!ms_IsInitialized) Initialize();}
	~ScrollBar() {}

	void SetSize(irr::u32 size)
	{
		m_Size = size;
		m_Start = irr::core::clamp(m_Start, 0U, size);
		m_End = irr::core::clamp(m_End, m_Start, m_Size);
	}
	irr::u32 GetSize() const {return m_Size;}
	void SetStart(irr::u32 start) {m_Start = irr::core::clamp(start, 0U, m_Size);}
	irr::u32 GetStart() const {return m_Start;}
	void SetEnd(irr::u32 end) {m_End = irr::core::clamp(end, 0U, m_Size);}
	irr::u32 GetEnd() const {return m_End;}

	irr::u32 SetDimensions(irr::u32 size, irr::u32 offset, irr::u32 displayedSize = 0)// returns the real offset start
	{
		m_Size = size;
		m_Start = irr::core::clamp(offset, 0U, (u32)core::max_(0, (s32)size-(s32)displayedSize));
		m_End = irr::core::clamp(m_Start + displayedSize, m_Start, core::min_(offset+displayedSize, m_Size));
		return m_Start;
	}

	irr::u32 GetDisplayedSize() const {return m_End - m_Start;}

	void Render(const Rectangle2& position, IColor color = COLOR_WHITE);

	static void Initialize();
	static void Cleanup();

protected:
	enum ElementType {ET_UNDER, ET_SLIDER, ET_MAX};
	static bool ms_IsInitialized;
	static Texture* ms_Elements[ET_MAX];// under and slider

	irr::u32 m_Size;
	irr::u32 m_Start;
	irr::u32 m_End;
};

wchar_t GetCharFromKeyCode(irr::EKEY_CODE code);
core::stringw GetStringFromKeyCode(EKEY_CODE code);

#if defined(_IRR_WINDOWS_) && defined(IRON_ICE_FINAL)
#define PROFILE_FINE
#endif
#ifdef PROFILE_FINE
#define LOG_PROFILE(s) ProfilingLogger log(s)
class ProfilingLogger
{
public:
	ProfilingLogger(char* s) : str(s)
	{
		if(FrameNumber == 0)
			return;

		BOOL s_use_qpc = QueryPerformanceFrequency( &liPerformanceFrequency);

		QueryPerformanceCounter( &liPerformanceCountStart);
	}

	~ProfilingLogger()
	{
		if(FrameNumber == 0)
			return;

		LARGE_INTEGER liPerformanceCountEnd;
		QueryPerformanceCounter( &liPerformanceCountEnd);
		float dTime = float(liPerformanceCountEnd.QuadPart - liPerformanceCountStart.QuadPart)/float(liPerformanceFrequency.QuadPart);

		printf("%s : %.03fms\n", str, dTime*1000.0f);
	}

	static void StartProfiling(s32 nbFrames = 1)
	{
		FrameNumber = nbFrames;
	}
	static void TickProfiler()
	{
		if(FrameNumber > 0)
			--FrameNumber;
	}

private:
	char* str;
	LARGE_INTEGER liPerformanceCountStart;
	LARGE_INTEGER liPerformanceFrequency;

	static s32 FrameNumber;
};
#else
#define LOG_PROFILE(s) 
#endif

bool islinereturn(wchar_t c);
bool isspace(wchar_t c);



// Glossary

bool Get2ndPart(const core::stringw& line, u32 start, u32 end, core::stringw& outPart2);

#define VIEW_BORDER_TOP 32
#define VIEW_BORDER_BOTTOM 16
#define VIEW_BORDER_RIGHT 16
#define VIEW_BORDER_LEFT 16
#define VIEW_TIMELINE_HEIGHT 16
#define VIEW_SCROLL_BAR_THICKNESS 16
#define VIEW_SPRITE_BANK_SIZE 128
#define VIEW_LAYER_PREVIEW_SIZE 64
#define VIEW_ANIM_BANK_SIZE 128
#define VIEW_BG_PREVIEW_SIZE 128
#define VIEW_DIALOG_LIST_SIZE 128
#define VIEW_TITLES_HEIGHT 16
#define VIEW_SPACE_BETWEEN_ELEMENTS 16
#define VIEW_EDITOR_WINDOW_CONTROL 128
#define VIEW_EDITOR_TOOLBARS_SIZE 16
#define VIEW_SCENE_EDITOR_WINDOW_CONTROL 128
#define VIEW_SCENE_EDITOR_TOOLBARS_SIZE 16

#define GUI_ID_EDITOR_WINDOW 1001
#define GUI_ID_LAYER_MENU 1002
#define GUI_ID_SPRITE_BANK 1003
#define GUI_ID_FRAME_SCROLL 1004

#define GUI_ID_ANIM_BANK 1005
#define GUI_ID_SCENE_EDITOR 1006
#define GUI_ID_DIALOG_LIST 1007

#define GUI_ID_RENDER 1008


#define DIALOG_EXTRA_DURATION 1.0f


#endif //#ifndef MATHUTILS_H
