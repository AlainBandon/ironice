﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

enum StringDesc {SD_DISPLAY = Engine::SFTID_OptionsSubMenuPanel, SD_AUDIO, SD_KEYS, SD_LANGUAGE, SD_DRIVER, SD_RESOLUTION, SD_FULLSCREEN, SD_VSYNC, SD_GAMMARAMP,
	SD_SAVE_POPUP, SD_NO, SD_YES, SD_D3D, SD_OPENGL, SD_SOFTWARE, SD_VOL_GENERAL, SD_VOL_BG1, SD_VOL_BG2, SD_VOL_VOICE,
	SD_KEY_LEFT, SD_KEY_RIGHT, SD_KEY_UP, SD_KEY_DOWN,
	SD_KEY_VALID, SD_KEY_CANCEL, SD_KEY_QTE1, SD_KEY_QTE2, SD_KEY_QTE3, SD_KEY_QTE4, SD_KEY_PAUSE, SD_KEY_QUICK_MENU, SD_MAX};


// OptionsSubMenuPanel
OptionsSubMenuPanel::OptionsSubMenuPanel() : IPanel()
, m_ActiveBG(NULL)
, m_CurrentSelection(0)
, m_CurrentLine(0)
, m_CurrentSubMenuActive(SMAT_BOTTOM)
, m_PreviousSubMenuActive(SMAT_BOTTOM)
, m_CurrentSelectionValidatedFader(0.2f)
, m_PopupPanel(NULL)
, m_IsProcessingKey(false)
{
	for(int i = 0; i < 2; ++i)
		m_Font[i] = NULL;

	bool useRT = true;
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	if(useRT && driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		m_RenderTarget = driver->addRenderTargetTexture(Engine::GetInstance()->m_RenderSize, "RTT_Options", video::ECF_A8R8G8B8);
	}
	else
	{
		m_RenderTarget = NULL;
	}
}

OptionsSubMenuPanel::~OptionsSubMenuPanel()
{
	UnloadPanel();
}

void OptionsSubMenuPanel::LoadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	SAFE_LOAD_IMAGE(driver, m_ActiveBG, engine->GetGamePath("system/02_Active_BG.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[2] = {48, 60};
		for(int i = 0; i < 2; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA), size/6);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	if(!m_PopupPanel)
	{
		m_PopupPanel = new PopupPanel(m_RenderTarget || USE_PREMULTIPLIED_ALPHA);
		m_PopupPanel->LoadPanel();
	}

	OnResize();

	//StartFade(PANEL_FADETIME);

	IPanel::LoadPanel();
}

void OptionsSubMenuPanel::UnloadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_ActiveBG);

	for(int i = 0; i < 2; ++i)
		SAFE_UNLOAD(m_Font[i]);

	if(m_PopupPanel)
		m_PopupPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void OptionsSubMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	const u32 sizes[2] = {48, 60};
	if(m_Font[0])
	{
		for(int i = 0; i < 2; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			m_Font[i]->setSize(size, size/6);
		}
	}

	{
		Dimension2 size = (m_ActiveBG)? m_ActiveBG->getSize() : Dimension2(1161, 834);
		size.Width += 100;
		size.Height += 60;
		Position2 pos = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)size.Height*innerScale)) + innerOffset;
		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)size.Height*innerScale));
		m_SubMenusArea[SMAT_CENTER].Area = Rectangle2(pos-cornerOffset, pos);
		m_SubMenusArea[SMAT_CENTER].IsSelected = false;

		pos.X -= cornerOffset.X;
		m_SubMenusArea[SMAT_LEFT].Area = Rectangle2(innerOffset, pos);
		m_SubMenusArea[SMAT_LEFT].IsSelected = false;

		pos.X = innerOffset.X;
		cornerOffset = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)originalSize.Height*innerScale))+innerOffset;
		m_SubMenusArea[SMAT_BOTTOM].Area = Rectangle2(pos, cornerOffset);
		m_SubMenusArea[SMAT_BOTTOM].IsSelected = false;

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	{

		Dimension2 size(originalSize.Width>>2, sizes[2]);
		Position2 pos = Position2((s32)((f32)100*innerScale), (s32)((f32)(originalSize.Height>>2)*innerScale)) + innerOffset;

		u32 dy = (u32)(((originalSize.Height>>1)/(f32)SBT_MAX)*innerScale);
		Position2 offset(0, 0);

		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)dy*innerScale));
		for(int i = 0; i < SBT_MAX; ++i, offset.Y += dy)
		{
			if(m_Font[1])
			{
				cornerOffset.X = m_Font[1]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), i+SD_DISPLAY)).Width;
			}
			m_ButtonsArea[i].Area = Rectangle2(pos + offset, pos + offset + cornerOffset);
			m_ButtonsArea[i].IsSelected = false;
		}

		m_ButtonsArea[m_CurrentSelection].IsSelected = true;
	}

	if(m_RenderTarget && renderSize != m_RenderTarget->getSize())
		m_RenderTarget = driver->addRenderTargetTexture(renderSize, "RTT_Options", video::ECF_A8R8G8B8);

	if(m_PopupPanel)
	{
		m_PopupPanel->OnResize();
	}
}

void OptionsSubMenuPanel::Update(float dt)
{
	IPanel::Update(dt);

	m_CurrentSelectionValidatedFader.Tick(dt);

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Update(dt);
		}
	}
}

void OptionsSubMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor off = IsFading()?
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0), 4.0f*m_Fader.GetRatio()-3.0f)
		: IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0);
	IColor on = COMPUTE_THEME_COLOR_FONT_GLOW(0);
	float ratio = m_CurrentSelectionValidatedFader.GetRealRatio();
	IColor onToOff = MathUtils::GetInterpolatedColor(off, on, ratio);
	IColor offToOn = MathUtils::GetInterpolatedColor(on, off, ratio);
	Rectangle2 rect(innerOffset, Dimension2((u32)(innerScale*originalSize.Width), (u32)(innerScale*originalSize.Height)));
	Rectangle2 rects[3] =
	{
		m_SubMenusArea[SMAT_LEFT].Area,
		m_SubMenusArea[SMAT_CENTER].Area,
		m_SubMenusArea[SMAT_BOTTOM].Area
	};
	if(m_CurrentSubMenuActive == SMAT_LEFT)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 1011
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_CENTER) // 0111 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1011
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, off, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_CENTER)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 0111
		{
			fill2DRect(driver, rects[0], off, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], off, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_BOTTOM)// 1111
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1010 -> 1111
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_POPUP)// 0000
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 0000
		{
			fill2DRect(driver, rects[0], off, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_BOTTOM) // 1111 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else
		{
			fill2DRect(driver, rects[0], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
			fill2DRect(driver, rects[1], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
		}
		fill2DRect(driver, rects[2], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
	}
	else
	{
		assert(false);
		fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
	}
}

void OptionsSubMenuPanel::RenderDisplay(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_ActiveBG)
	{
		ProfileData* profil = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = m_ActiveBG->getSize();

		if(m_Font[0] && m_Font[1])
		{
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width)-100)*scale + offset.X)*innerScale)),
				(s32)((((f32)(60)*scale + offset.Y)*innerScale)));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			ProfileData* profile = ProfileData::GetInstance();

			u32 dy = (u32)(scale * m_Font[1]->getSize()) + (u32)((float)30 * innerScale * scale);
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			core::stringw str1[4] =
			{
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_DRIVER)),
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_RESOLUTION)),
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_FULLSCREEN)),
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_VSYNC))
			};

			core::stringw str2[4] =
			{
				StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), SD_D3D+profile->m_ODS.m_DriverType)),
				StringFormat(L"%d x %d", profile->m_ODS.m_WindowSize.Width, profile->m_ODS.m_WindowSize.Height),
				StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), SD_NO+profile->m_ODS.m_FullScreen)),
				StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), SD_NO+profile->m_ODS.m_VSync))
			};

			for(u32 i = 0; i < 4; ++i)
			{
				bool isSelected = (m_CurrentLine==i);

				u32 alpha = (m_CurrentSubMenuActive == SMAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor color = COMPUTE_THEME_COLOR(alpha);

				float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);

				if(isSelected)
				{
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str1[i], Rectangle2(pos, D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[1]:m_Font[0])->draw(str1[i], Rectangle2(pos, D2Zero), color, scale, false, false);

				if(isSelected)
				{
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str2[i], Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[1]:m_Font[0])->draw(str2[i], Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), color, scale, false, false);

				m_LinesArea[i].Area = Rectangle2(pos, Dimension2(dx, dy));
				pos.Y += dy;
			}

			// the gamma ramp
			{
				bool isSelected = (m_CurrentLine==4);

				u32 alpha = (m_CurrentSubMenuActive == SMAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				core::stringw str = StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_GAMMARAMP));

				if(isSelected)
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					//float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str, Rectangle2(pos, D2Zero), tint, scale, false, false);
				}

				(isSelected?m_Font[1]:m_Font[0])->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);

				m_Slider.SetValue(0.01f*profile->m_ODS.m_GammaRamp);
				m_Slider.Render(Rectangle2(Position2(pos.X + (dx>>1), pos.Y + (dy>>2)), Dimension2(dx>>1, dy>>1)), colorTex);

				m_LinesArea[4].Area = Rectangle2(Position2(pos.X, pos.Y + (dy>>2)), Dimension2(dx, dy>>1));
				pos.Y += dy;
			}

			for(u32 i = 5; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}

void OptionsSubMenuPanel::RenderAudio(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_ActiveBG)
	{
		ProfileData* profile = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = m_ActiveBG->getSize();

		if(m_Font[0] && m_Font[1])
		{
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width)-100)*scale + offset.X)*innerScale)),
				(s32)((((f32)(60)*scale + offset.Y)*innerScale)));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			ProfileData* profil = ProfileData::GetInstance();

			u32 dy = (u32)(scale * m_Font[1]->getSize()) + (u32)((float)30 * innerScale * scale);
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			core::stringw str[4] =
			{
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_GENERAL)),
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_BG1)),
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_BG2)),
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_VOICE))
			};

			float values[4] =
			{
				0.01f*profile->m_ODS.m_VolumeGeneral,
				0.01f*profile->m_ODS.m_VolumeBG1,
				0.01f*profile->m_ODS.m_VolumeBG2,
				0.01f*profile->m_ODS.m_VolumeVoice
			};

			for(u32 i = 0; i < 4; ++i)
			{
				bool isSelected = (m_CurrentLine==i);

				u32 alpha = (m_CurrentSubMenuActive == SMAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				if(isSelected)
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					//float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str[i], Rectangle2(pos, D2Zero), tint, scale, false, false);
				}

				(isSelected?m_Font[1]:m_Font[0])->draw(str[i], Rectangle2(pos, D2Zero), color, scale, false, false);

				m_Slider.SetValue(values[i]);
				m_Slider.Render(Rectangle2(Position2(pos.X + (dx>>1), pos.Y + (dy>>2)), Dimension2(dx>>1, dy>>1)), colorTex);

				m_LinesArea[i].Area = Rectangle2(Position2(pos.X, pos.Y + (dy>>2)), Dimension2(dx, dy>>1));
				pos.Y += dy;
			}

			for(u32 i = 4; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}

void OptionsSubMenuPanel::RenderKeys(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_ActiveBG)
	{
		ProfileData* profile = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = m_ActiveBG->getSize();

		if(m_Font[0] && m_Font[1])
		{
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width)-100)*scale + offset.X)*innerScale)),
				(s32)((((f32)(60)*scale + offset.Y)*innerScale)));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			u32 dy = (u32)(scale * m_Font[1]->getSize())/* + (u32)((float)10 * innerScale * scale)*/;
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			ProfileData::KeyMappingSerializable& keymap = profile->m_KMS;
			for(u32 i = 0; i < 12; ++i)
			{
				bool isSelected = (m_CurrentLine==i);

				u32 alpha = (m_CurrentSubMenuActive == SMAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				IColor tint(COMPUTE_THEME_COLOR(255));
				if(isSelected)
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
				}

				core::stringw str = Engine::GetLanguageString(engine->GetLanguage(), SD_KEY_LEFT + i);
				if(isSelected)
				{
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str, Rectangle2(pos, D2Zero), tint, scale, false, false);
				}

				(isSelected?m_Font[1]:m_Font[0])->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);

				if(!m_IsProcessingKey || m_CurrentLine!=i)
				{
					EKEY_CODE keycode = keymap.GetKeyCode1((ProfileData::KeyMappingSerializable::IIKeyType)i);
					core::stringw strCode = GetStringFromKeyCode(keycode);

					if(strCode != L"")
					{
						Engine::KeyElementType type = (strCode.size() > 1)? Engine::KET_EMPTY_KEY2 : Engine::KET_EMPTY_KEY1;
						// special keys
						if(keycode == KEY_LBUTTON)
							type = Engine::KET_LMOUSE;
						else if(keycode == KEY_RBUTTON)
							type = Engine::KET_RMOUSE;
						else if(keycode == KEY_LEFT)
							type = Engine::KET_LEFT_KEY;
						else if(keycode == KEY_RIGHT)
							type = Engine::KET_RIGHT_KEY;
						else if(keycode == KEY_UP)
							type = Engine::KET_UP_KEY;
						else if(keycode == KEY_DOWN)
							type = Engine::KET_DOWN_KEY;

						Position2 pos2(pos.X + (dx>>2) + (dx>>1), pos.Y + (dy>>1));

						if(engine->GetKeyElement(type))
						{
							Dimension2 size = engine->GetKeyElement(type)->getSize();
							Rectangle2 sourceRect(Position2(0,0), size);

							float menuScale = (float)dy/size.Height;
							if(type == Engine::KET_EMPTY_KEY2)
								menuScale *= 2.0f;
							else if(type == Engine::KET_RMOUSE || type == Engine::KET_LMOUSE)
								menuScale *= 1.0f;
							else
								menuScale *= 1.5f;

							if(isSelected)
								menuScale *= 1.2f;

							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
								core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df(menuScale, menuScale, 0.0f));

							draw2DImage(driver, engine->GetKeyElement(type), sourceRect, mat, true, colorTex, true, true);
						}

						if(type == Engine::KET_EMPTY_KEY2 || type == Engine::KET_EMPTY_KEY1)
						{
							if(isSelected)
							{
								(isSelected?m_Font[1]:m_Font[0])->drawGauss(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), tint, scale, true, true);
							}
							(isSelected?m_Font[1]:m_Font[0])->draw(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), colorTex, scale, true, true);
						}
					}
				}

				m_LinesArea[i].Area = Rectangle2(pos, Dimension2(dx, dy));
				pos.Y += dy;
			}
		}
	}
}

void OptionsSubMenuPanel::RenderLanguage(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_ActiveBG)
	{
		ProfileData* profil = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = m_ActiveBG->getSize();

		if(m_Font[0] && m_Font[1])
		{
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width)-100)*scale + offset.X)*innerScale)),
				(s32)((((f32)(60)*scale + offset.Y)*innerScale)));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			ProfileData* profile = ProfileData::GetInstance();

			u32 dy = (u32)(scale * m_Font[1]->getSize()) + (u32)((float)10 * innerScale * scale);
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			core::stringw str1 = StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_LANGUAGE));
			core::stringw str2 = StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), Engine::SFTID_LanguageMenuPanel));

			{
				bool isSelected = (m_CurrentLine==0);

				u32 alpha = (m_CurrentSubMenuActive == SMAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor color = COMPUTE_THEME_COLOR(alpha);

				float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);

				if(isSelected)
				{
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str1, Rectangle2(pos, D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[1]:m_Font[0])->draw(str1, Rectangle2(pos, D2Zero), color, scale, false, false);

				if(isSelected)
				{
					(isSelected?m_Font[1]:m_Font[0])->drawGauss(str2, Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[1]:m_Font[0])->draw(str2, Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), color, scale, false, false);

				m_LinesArea[0].Area = Rectangle2(pos, Dimension2(dx, dy));
				pos.Y += dy;
			}

			for(u32 i = 1; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}

void OptionsSubMenuPanel::_Render(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	RenderFader(scale, offset);

	// draw the left submenu
	if(m_Font[0] && m_Font[1])
	{
		s32 x = (u32)((originalSize.Width/(f32)SBT_MAX)*innerScale);

		for(int i = 0; i < SBT_MAX; ++i)
		{
			core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), i+SD_DISPLAY));
			bool isSelected = m_ButtonsArea[i].IsSelected;
			u32 alpha = (m_CurrentSubMenuActive != SMAT_CENTER)? 255 : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
			IColor color = isSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : COMPUTE_THEME_COLOR((m_CurrentSubMenuActive != SMAT_CENTER)? alpha : 16);
			const Rectangle2& area = m_ButtonsArea[i].Area;
			Rectangle2 rect(Position2((s32)((float)(area.UpperLeftCorner.X-innerOffset.X)*scale+(float)offset.X*innerScale+innerOffset.X),
				(s32)((float)(area.UpperLeftCorner.Y-innerOffset.Y)*scale+(float)offset.Y*innerScale+innerOffset.Y)),
				Dimension2((u32)((float)area.getWidth()*scale), (u32)((float)area.getHeight()*scale)));
			//if(isSelected)
			{
				float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
				(isSelected?m_Font[1]:m_Font[0])->drawGauss(str, rect, tint, scale, false, false);
			}
			(isSelected?m_Font[1]:m_Font[0])->draw(str, rect, color, scale, false, false);
		}
	}

	// center part
	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	// draw the active BG at the center
	if(m_ActiveBG)
	{
		IColor tint = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width-(size.Width>>1)-100)*innerScale),
			(s32)((f32)((size.Height>>1)+60)*innerScale));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X*scale+(f32)offset.X*innerScale + innerOffset.X, (f32)pos.Y*scale+(f32)offset.Y*innerScale + innerOffset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

		draw2DImage(driver, m_ActiveBG, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	}

	switch(m_CurrentSelection)
	{
	case SBT_DISPLAY:
		RenderDisplay(scale, offset);
		break;
	case SBT_AUDIO:
		RenderAudio(scale, offset);
		break;
	case SBT_KEYS:
		RenderKeys(scale, offset);
		break;
	case SBT_LANGUAGE:
		RenderLanguage(scale, offset);
		break;
	default:
		assert(0);
		break;
	}

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Render();
		}
	}
}

void OptionsSubMenuPanel::PreRender()
{
	// render here only if we have a render target : we render in the render target
	if(m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		// set render target texture
		IColor color = COLOR_TRANSPARENT;
		color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(m_RenderTarget, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		_Render();

		// set back old render target
		// The buffer might have been distorted, so clear it
		color = COLOR_TRANSPARENT;
		if(USE_PREMULTIPLIED_ALPHA)
			color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(0, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

void OptionsSubMenuPanel::Render()
{
	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)),
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_Render(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();

		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)*Engine::GetInstance()->m_InnerScale), 0);

		IColor tint = COLOR_WHITE;

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((Engine::GetInstance()->m_RenderSize.Width>>1),
			(Engine::GetInstance()->m_RenderSize.Height>>1));

		if(m_CurrentSelectionValidatedFader.IsFading())
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// other to bottom fade
				ratio = 0.75f * ratio + m_CurrentSelectionValidatedFader.GetRealRatio()*0.25f;// 1 to 0.75
			else if(m_PreviousSubMenuActive == SMAT_BOTTOM)// bottom to other fade
				ratio = 0.75f * ratio + (1.0f - m_CurrentSelectionValidatedFader.GetRealRatio())*0.25f;// 0.75 to 1
		}
		else
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// bottom
				ratio = 0.75f*ratio;// 0.75
		}

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

bool OptionsSubMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	ProfileData* profile = ProfileData::GetInstance();

	if(m_PopupPanel)
	{
		if(m_PopupPanel->OnEvent(event))
		{
			if(!m_PopupPanel->IsVisible())// this means that the popup just closed
			{
				{
					if(m_PopupPanel->GetResult() == PopupPanel::PRT_YES)
					{
						// save the changes
						profile->WriteSave();

						// apply the changes for the ones that need to reset the device
						if(profile->m_ODS.NeedsResetDevice(m_ODS))
							Engine::GetInstance()->RequestResetDevice();
					}
					profile->ReadSave();
					//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
				}
				ChangeSubMenuActive(m_PreviousSubMenuActive);
			}

			return true;
		}
	}
	if(m_IsProcessingKey)
	{
		ProcessKey(event);
		return true;
	}
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Engine* engine = Engine::GetInstance();
			const Dimension2& renderSize = engine->m_RenderSize;
			const Dimension2& originalSize = engine->m_OriginalSize;
			float innerScale = engine->m_InnerScale;
			const Position2& innerOffset = engine->m_InnerOffset;

			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);

			// check which sub menu is active
			if(!m_CurrentSelectionValidatedFader.IsFading())
			{
				u32 oldSubMenuActive = m_CurrentSubMenuActive;
				for(int i = 0; i < SMAT_MAX; ++i)
				{
					m_SubMenusArea[i].IsSelected = false;
					if(m_SubMenusArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentSubMenuActive = i;
					}
				}
				m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
				if(oldSubMenuActive != m_CurrentSubMenuActive)
				{
					m_PreviousSubMenuActive = oldSubMenuActive;
					m_CurrentSelectionValidatedFader.StartFade();
				}
			}

			// Left menu
			u32 oldSelection = m_CurrentSelection;
			{
				for(int i = 0; i < SBT_MAX; ++i)
				{
					m_ButtonsArea[i].IsSelected = false;
					if(m_ButtonsArea[i].Area.isPointInside(mousePos))
					{
						if(m_CurrentSelection != i)
						{
							m_CurrentSelection = i;

							// reset center area selection
							for(u32 i = 0; i < 12; ++i)
								m_LinesArea[i].IsSelected = false;
							m_CurrentLine = 0;
						}
					}
				}
				m_ButtonsArea[m_CurrentSelection].IsSelected = true;
			}
			bool hasSelectionChanged = (oldSelection != m_CurrentSelection);

			// center part
			if( !((m_CurrentSelection == SBT_AUDIO || (m_CurrentSelection == SBT_DISPLAY && m_CurrentLine == 4)) &&
				event.MouseInput.isLeftPressed()) )// do not change the selection in the audio menu if left mouse is pressed
			{
				for(int i = 0; i < 12; ++i)
				{
					m_LinesArea[i].IsSelected = false;
					if(m_LinesArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentLine = i;
						m_LinesArea[m_CurrentLine].IsSelected = true;
					}
				}
			}

			if(m_CurrentSelection == SBT_AUDIO)
			{
				if(event.MouseInput.isLeftPressed())
				{
					float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
						(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
					if(value >= 0.0f && value <= 1.0f)
					{
						u8* valuesPtr[4] =
						{
							&profile->m_ODS.m_VolumeGeneral,
							&profile->m_ODS.m_VolumeBG1,
							&profile->m_ODS.m_VolumeBG2,
							&profile->m_ODS.m_VolumeVoice
						};
						*(valuesPtr[m_CurrentLine]) = (u8)(value*100);
					}
				}
			}
			else if(m_CurrentSelection == SBT_DISPLAY && m_CurrentLine == 4)
			{
				if(event.MouseInput.isLeftPressed())
				{
					float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
						(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
					if(value >= 0.0f && value <= 1.0f)
					{
						profile->m_ODS.m_GammaRamp = (u8)(value*100);
						//engine->SetGamma(profile->m_ODS.m_GammaRamp);
					}
				}
			}

		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)
			{
				m_ODS = profile->m_ODS;
				m_KMS = profile->m_KMS;
				profile->ReadSave();
				if(m_ODS != profile->m_ODS || m_KMS != profile->m_KMS)// if sth changed : ask for apply them
				{
					ChangeSubMenuActive(SMAT_POPUP);
					if(m_PopupPanel)
					{
						m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_SAVE_POPUP));
					}

					{
						ProfileData::OptionDataSerializable temp = profile->m_ODS;
						profile->m_ODS = m_ODS;// reapply the changes done for the display
						m_ODS = temp;// we store the old profile options
					}

					{
						ProfileData::KeyMappingSerializable temp = profile->m_KMS;
						profile->m_KMS = m_KMS;// reapply the changes done for the keys
						m_KMS = temp;// we store the old profile options
					}

					return true;
				}
			}
			else if(m_LinesArea[m_CurrentLine].IsSelected)
			{
				switch(m_CurrentSelection)
				{
				case SBT_DISPLAY:
					{
						switch(m_CurrentLine)
						{
						case 0:// driver
							{
								u32 value = (u32)profile->m_ODS.m_DriverType;
								++value %= ProfileData::OptionDataSerializable::DT_MAX;
								profile->m_ODS.m_DriverType = (ProfileData::OptionDataSerializable::DriverType)value;
							}
							break;
						case 1:// resolution
							profile->m_ODS.m_WindowSize = Engine::GetInstance()->GetNextResolution(profile->m_ODS.m_WindowSize);
							break;
						case 2:// fullscreen
							profile->m_ODS.m_FullScreen = !profile->m_ODS.m_FullScreen;
							break;
						case 3:// v-sync
							profile->m_ODS.m_VSync = !profile->m_ODS.m_VSync;
							break;
						case 4:// gamma ramp
							{
								Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
								float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
									(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
								if(value >= 0.0f && value <= 1.0f)
								{
									profile->m_ODS.m_GammaRamp = (u8)(value*100);
									//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
								}
							}
							break;
						default:
							assert(0);
							break;
						}
					}
					break;
				case SBT_AUDIO:
					{
						Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
						if(m_CurrentLine <= 4)
						{
							float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
								(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
							if(value >= 0.0f && value <= 1.0f)
							{
								u8* valuesPtr[4] =
								{
									&profile->m_ODS.m_VolumeGeneral,
									&profile->m_ODS.m_VolumeBG1,
									&profile->m_ODS.m_VolumeBG2,
									&profile->m_ODS.m_VolumeVoice
								};
								*(valuesPtr[m_CurrentLine]) = (u8)(value*100);
							}
						}
					}
					break;
				case SBT_KEYS:
					{
						m_IsProcessingKey = true;
					}
					break;
				case SBT_LANGUAGE:
					{
						s32 ls = (s32)Engine::GetInstance()->GetLanguage();
						++ls;
						if(ls >= Engine::LS_MAX)
							ls = 0;
						Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)ls);
						OnResize();// because of the left menu entries dimensions
					}
					break;
				default:
					assert(0);
					break;
				}
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		//if(m_SelectionFadeTimer == 0.0f)
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					// reset center area selection
					for(u32 i = 0; i < 12; ++i)
						m_LinesArea[i].IsSelected = false;

					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					// Check for change and ask for save in case of need
					m_ODS = profile->m_ODS;
					m_KMS = profile->m_KMS;
					profile->ReadSave();
					if(m_ODS != profile->m_ODS || m_KMS != profile->m_KMS)// if sth changed : ask for apply them
					{
						ChangeSubMenuActive(SMAT_POPUP);
						if(m_PopupPanel)
						{
							m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_SAVE_POPUP));
						}

						{
							ProfileData::OptionDataSerializable temp = profile->m_ODS;
							profile->m_ODS = m_ODS;// reapply the changes done for the display
							m_ODS = temp;// we store the old profile options
						}

						{
							ProfileData::KeyMappingSerializable temp = profile->m_KMS;
							profile->m_KMS = m_KMS;// reapply the changes done for the keys
							m_KMS = temp;// we store the old profile options
						}

						return true;
					}

					ChangeSubMenuActive(SMAT_BOTTOM);
				}
				return true;// MUST BYPASS THE MAINPANEL ESCAPE EVENT
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_KEYS)
					{
						m_IsProcessingKey = true;
					}
				}
				//m_TransitToChildPanelIndex = 0;
				//StartFade(PANEL_FADETIME);
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					--m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
						m_CurrentSelection = 0;
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
					// reset center area selection
					for(u32 i = 0; i < 12; ++i)
						m_LinesArea[i].IsSelected = false;
					m_CurrentLine = 0;
				}
				else if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					switch(m_CurrentSelection)
					{
					case SBT_DISPLAY:
					case SBT_AUDIO:
					case SBT_KEYS:
					case SBT_LANGUAGE:
						if(m_CurrentLine > 0)
							--m_CurrentLine;
						break;
					default:
						assert(0);
						break;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					++m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
					{
						m_CurrentSelection = SBT_MAX - 1;
						ChangeSubMenuActive(SMAT_BOTTOM);
					}
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
					// reset center area selection
					for(u32 i = 0; i < 12; ++i)
						m_LinesArea[i].IsSelected = false;
					m_CurrentLine = 0;
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					switch(m_CurrentSelection)
					{
					case SBT_DISPLAY:
						if(m_CurrentLine < 4)
							++m_CurrentLine;
						break;
					case SBT_AUDIO:
						if(m_CurrentLine < 3)
							++m_CurrentLine;
						break;
					case SBT_KEYS:
						if(m_CurrentLine < 11)
							++m_CurrentLine;
						break;
					case SBT_LANGUAGE:
						if(m_CurrentLine < 0)
							++m_CurrentLine;
						break;
					default:
						assert(0);
						break;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					switch(m_CurrentSelection)
					{
					case SBT_DISPLAY:
						{
							switch(m_CurrentLine)
							{
							case 0:// driver
								{
									u32 value = (u32)profile->m_ODS.m_DriverType + ProfileData::OptionDataSerializable::DT_MAX;
									--value %= ProfileData::OptionDataSerializable::DT_MAX;
									profile->m_ODS.m_DriverType = (ProfileData::OptionDataSerializable::DriverType)value;
								}
								break;
							case 1:// resolution
								profile->m_ODS.m_WindowSize = Engine::GetInstance()->GetPreviousResolution(profile->m_ODS.m_WindowSize);
								break;
							case 2:// fullscreen
								profile->m_ODS.m_FullScreen = !profile->m_ODS.m_FullScreen;
								break;
							case 3:// v-sync
								profile->m_ODS.m_VSync = !profile->m_ODS.m_VSync;
								break;
							case 4:// gamma ramp
								if(profile->m_ODS.m_GammaRamp > 0)
								{
									profile->m_ODS.m_GammaRamp -= 1;
									//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
								}
								break;
							default:
								assert(0);
								break;
							}
						}
						break;
					case SBT_AUDIO:
						{
							if(m_CurrentLine <= 4)
							{
								u8* valuesPtr[4] =
								{
									&profile->m_ODS.m_VolumeGeneral,
									&profile->m_ODS.m_VolumeBG1,
									&profile->m_ODS.m_VolumeBG2,
									&profile->m_ODS.m_VolumeVoice
								};
								if(*(valuesPtr[m_CurrentLine]) > 0)
									*(valuesPtr[m_CurrentLine]) -= 1;
							}
						}
						break;
					case SBT_KEYS:
						{

						}
						break;
					case SBT_LANGUAGE:
						{
							s32 ls = (s32)Engine::GetInstance()->GetLanguage();
							--ls;
							if(ls < 0)
								ls = Engine::LS_MAX - 1;
							Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)ls);
							OnResize();// because of the left menu entries dimensions
						}
						break;
					default:
						assert(0);
						break;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					switch(m_CurrentSelection)
					{
					case SBT_DISPLAY:
						{
							switch(m_CurrentLine)
							{
							case 0:// driver
								{
									u32 value = (u32)profile->m_ODS.m_DriverType;
									++value %= ProfileData::OptionDataSerializable::DT_MAX;
									profile->m_ODS.m_DriverType = (ProfileData::OptionDataSerializable::DriverType)value;
								}
								break;
							case 1:// resolution
								profile->m_ODS.m_WindowSize = Engine::GetInstance()->GetNextResolution(profile->m_ODS.m_WindowSize);
								break;
							case 2:// fullscreen
								profile->m_ODS.m_FullScreen = !profile->m_ODS.m_FullScreen;
								break;
							case 3:// v-sync
								profile->m_ODS.m_VSync = !profile->m_ODS.m_VSync;
								break;
							case 4:// gamma ramp
								if(profile->m_ODS.m_GammaRamp < 100)
								{
									profile->m_ODS.m_GammaRamp += 1;
									//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
								}
								break;
							default:
								assert(0);
								break;
							}
						}
						break;
					case SBT_AUDIO:
						{
							if(m_CurrentLine <= 4)
							{
								u8* valuesPtr[4] =
								{
									&profile->m_ODS.m_VolumeGeneral,
									&profile->m_ODS.m_VolumeBG1,
									&profile->m_ODS.m_VolumeBG2,
									&profile->m_ODS.m_VolumeVoice
								};
								if(*(valuesPtr[m_CurrentLine]) < 100)
									*(valuesPtr[m_CurrentLine]) += 1;
							}
						}
						break;
					case SBT_KEYS:
						{

						}
						break;
					case SBT_LANGUAGE:
						{
							s32 ls = (s32)Engine::GetInstance()->GetLanguage();
							++ls;
							if(ls >= Engine::LS_MAX)
								ls = 0;
							Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)ls);
							OnResize();// because of the left menu entries dimensions
						}
						break;
					default:
						assert(0);
						break;
					}
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
			}
		}
	}

	return (m_CurrentSubMenuActive != SMAT_BOTTOM);// bypass the main panel events if we don't have the focus on the bottom element
}

bool OptionsSubMenuPanel::ProcessKey(const SEvent& event)
{
	EKEY_CODE code = (EKEY_CODE)0x0;
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
			code = KEY_LBUTTON;
		else if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
			code = KEY_RBUTTON;
		else if(event.MouseInput.Event == EMIE_MMOUSE_PRESSED_DOWN)
			code = KEY_MBUTTON;
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			code = event.KeyInput.Key;
		}
	}

	core::stringw str = GetStringFromKeyCode(code);
	if(str != L"")
	{
		m_IsProcessingKey = !ProfileData::GetInstance()->m_KMS.MapKey1((ProfileData::KeyMappingSerializable::IIKeyType)m_CurrentLine, code);

		return true;
	}

	return false;
}


#undef PANEL_FADETIME
