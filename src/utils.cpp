#include "utils.h"
#include "engine.h"

#include <stdarg.h>

#ifdef _IRR_WINDOWS_
#include <windows.h>
#include <d3d9.h>
#endif
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif

const Vector3 MathUtils::ms_V3Zero = Vector3(0,0,0);
const Matrix4 MathUtils::ms_M4Zero = core::IdentityMatrix * 0;

const Position2 MathUtils::ms_P2Zero = Position2(0,0);
const Dimension2 MathUtils::ms_D2Zero = Dimension2(0,0);
const IColor MathUtils::ms_IColor_White = IColor(255,255,255,255);
const IColor MathUtils::ms_IColor_Black = IColor(255,0,0,0);
const IColor MathUtils::ms_IColor_Transparent = IColor(0,0,0,0);

IColor ThemeColor = IColor(0, 255, 255, 255);
IColor ThemeColorGlow = IColor(0, 9, 173, 231);
IColor ThemeColorSelection = IColor(0, 132, 214, 243); 

const float MathUtils::ms_ZoomFactors[NB_ZOOM_FACTOR] = {0.1f, 0.2f, 0.3f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.9f, 1.0f,
											2.0f, 3.0f, 4.0f, 5.0f, 6.0f, 7.0f, 8.0f, 9.0f, 10.0f};

bool USE_PREMULTIPLIED_ALPHA = true;

#ifdef PROFILE_FINE
s32 ProfilingLogger::FrameNumber = 0;
#endif

void SerializeStringW(const core::stringw& str, FILE* file)
{
	u32 size = str.size();
	fwrite(&size, sizeof(u32), 1, file);
	if(size > 0)
	{
		const wchar_t* buff = str.c_str();
		assert(sizeof(wchar_t) != 1);
		if(sizeof(wchar_t) == 2)
			fwrite(buff, sizeof(wchar_t), size, file);
		else
		{
			for(u32 i = 0; i < size; ++i)
			{
				assert((buff[i] & (0xff<<((sizeof(wchar_t)-2)<<3))) == 0); 
				u16 c = (u16)(buff[i] & 0xffff);
				fwrite(&c, sizeof(u16), 1, file);
			}
		}
	}
}

void SerializeStringC(const core::stringc& str, FILE* file)
{
	u32 size = str.size();
	fwrite(&size, sizeof(u32), 1, file);
	if(size > 0)
	{
		const char* buff = str.c_str();
		fwrite(buff, sizeof(char), size, file);
	}
}

void DeserializeStringW(core::stringw& str, FILE* file)
{
	u32 size = 0;
	fread(&size, sizeof(u32), 1, file);
	if(size > 0)
	{
		wchar_t* buff = new wchar_t[size+1];
		buff[size] = 0;
		assert(sizeof(wchar_t) != 1);
		if(sizeof(wchar_t) == 2)
            fread(buff, sizeof(wchar_t), size, file);
        else
        {
            for(u32 i = 0; i < size; ++i)
            {
                u16 c = 0;
                fread(&c, sizeof(u16), 1, file);
                buff[i] = (wchar_t)c;
            }
        }
		str = buff;
		SafeDeleteArray(buff);
	}
}

void DeserializeStringC(core::stringc& str, FILE* file)
{
	u32 size = 0;
	fread(&size, sizeof(u32), 1, file);
	if(size > 0)
	{
		char* buff = new char[size+1];
		buff[size] = 0;
		fread(buff, sizeof(char), size, file);
		str = buff;
		SafeDeleteArray(buff);
	}
}


KeyTime MathUtils::GetInterpolatedKeyTime(const TArray<KeyTime>& keys, float time, bool loop /*= false*/)
{
	KeyTime key;
	if(keys.size() == 0)
		return key;
	if(loop)
	{
		float maxTime = keys[keys.size()-1].m_Time;
		time = fmod(time, maxTime);
	}

	const KeyTime *pK1 = NULL, *pK2 = NULL;
	for(u32 i=0; i<keys.size(); ++i)
	{
		pK2 = &(keys[i]);
		if(pK2->m_Time > time)
			break;
		pK1 = pK2;
	}

	if(pK2)
	{
		if(pK1 == pK2)// after last key
		{
			return *pK2;// TEMP FOR PROD : shouldn't we return an invalid key to allow unpoping things ?
		}
		if(!pK1)// before first key : return an invalid key
		{
			key = *pK2;
			key.m_Time = -1.0f;// invalid key (but can be used anyway if needed)
			return key;
		}
		else
		{
			float timeScale = (time - pK1->m_Time)/(pK2->m_Time - pK1->m_Time);
			key.m_Time = time;
			key.m_Position = pK1->m_Position +
				Position2((s32)((float)(pK2->m_Position.X - pK1->m_Position.X)*timeScale),
				(s32)((float)(pK2->m_Position.Y - pK1->m_Position.Y)*timeScale));
			key.m_Rotation = pK1->m_Rotation + (pK2->m_Rotation - pK1->m_Rotation)*timeScale;
			key.m_Scale = pK1->m_Scale + (pK2->m_Scale - pK1->m_Scale)*timeScale;
			key.m_Color = GetInterpolatedColor(pK1->m_Color, pK2->m_Color, timeScale);
		}
	}

	return key;
}

Image* TextureToImage(Texture* texture)
{
	Engine* engine = Engine::GetInstance();
	video::IVideoDriver* driver = engine->GetDriver();
	video::IImage* image = driver->createImageFromData (
		texture->getColorFormat(),
		texture->getSize(),
		texture->lock(),
		false  //copy mem
		);

	texture->unlock();
	return image;
}

Texture* ImageToTexture(Image* image, core::stringc name)
{
	Engine* engine = Engine::GetInstance();
	video::IVideoDriver* driver = engine->GetDriver();
	video::ITexture* texture = driver->addTexture(name.c_str(),image);
	//texture->grab();
	return texture;
}

#include <stdio.h>
//#include <malloc.h>

const unsigned char DELIM = (char)0xFE;
const unsigned char MAX   = (char)0xFD;

int RLECompressColor(unsigned char* inData, unsigned int size, unsigned char* outData, unsigned int* outSize)
{
	if(!inData || size == 0 || !outData || !outSize || (size%4) != 0)
		return -1;

	int sizeP = (size>>2);
	// compress it
	unsigned char current_char;
	int  char_count = 0;
	(*outSize) = 0;

	bool exit = false;
	unsigned char* outDataStart = outData;
	unsigned char* outColorStart = outData;
	int currentSize = 0;

	for(unsigned int y = 0; y < size; y+=4)
	{
		if((y+8 < size) && (inData[y] == inData[y + 4]) && (inData[y] == inData[y + 8]))
		{
			current_char = inData[y];
			char_count = 0;
			bool hit_end = false;
			for(int q = 0; q < MAX; ++q)
			{
				if(((y + (q<<2)) < size) && (inData[y + (q<<2)] == current_char))
				{
					++char_count;
				}
				else
				{
					if(currentSize >= sizeP-3)
					{
						exit = true;
						break;
					}
					y += ((char_count - 1)<<2);
					*(outData++) = DELIM;
					*(outData++) = (unsigned char)char_count;
					*(outData++) = current_char;
					currentSize += 3;
					q = MAX;
					hit_end = true;
				}
			}
			if(exit)
			{
				break;
			}
			if(!hit_end)
			{
				if(currentSize >= sizeP-3)
				{
					exit = true;
					break;
				}
				y += ((MAX-1)<<2);
				*(outData++) = DELIM;
				*(outData++) = MAX;
				*(outData++) = current_char;
				currentSize += 3;
			}
		}
		else
		{
			if(inData[y] == DELIM)
			{
				if(currentSize >= sizeP-2)
				{
					exit = true;
					break;
				}
				*(outData++) = DELIM;
				*(outData++) = DELIM;
				currentSize += 2;
			}
			else
			{
				if(currentSize >= sizeP-1)
				{
					exit = true;
					break;
				}
				*(outData++) = inData[y];
				++currentSize;
			}
		}
	}
	(*outSize) = currentSize;

	if(exit)// compression useless : do not compress, just copy
	{
		for(unsigned int y = 0; y < size; y+=4)
		{
			*(outData++) = inData[y];
		}
		return 0;
	}

	return 1;
}

int RLEDecompressColors(unsigned char* inData, unsigned int size, unsigned char* outData, unsigned int outSize)
{
	if(!inData || size == 0 || !outData || (outSize%4) != 0)
		return -1;

	////////////////////////////////////////////////////////
	unsigned int sizeP = (outSize>>2);
	unsigned char* inDataStart = inData;
	unsigned char* outDataStart = outData;

	for(unsigned int i = 0; i < 4; ++i)
	{
		unsigned int sizeCompressed = *((unsigned int*)inDataStart);
		inDataStart += sizeof(int);
		outDataStart = outData + i;
		for(unsigned int y = 0; y < sizeCompressed; ++y)
		{
			if(y + 1 < sizeCompressed && inDataStart[y] == DELIM)
			{
				if(inDataStart[y + 1] == DELIM)
				{
					*outDataStart = DELIM;
					outDataStart += 4;
					y++;
				}
				else
				{
					assert(y + 2 < sizeCompressed);
					int buf_size = (int)inDataStart[y + 1];
					unsigned char c = inDataStart[y + 2];
					for(int q = 0; q < buf_size; ++q)
					{
						assert(((size_t)outDataStart-(size_t)outData) < outSize);// out of memory ??
						*outDataStart = c;
						outDataStart += 4;
					}
					y += 2;
				}
			}
			else
			{
				assert(!(y + 1 >= sizeCompressed && inDataStart[y] == DELIM));
				*outDataStart = inDataStart[y];
				outDataStart += 4;
			}
		}
		inDataStart += sizeCompressed;
	}

	return 1;
}

int PreProcessAlphaPixels(unsigned char* data, unsigned int size)
{
	// read in all bytes from InFile, and create OutFile
	if(!data || size == 0)
		return -1;

	////////////////////////////////////////////////////////

	for(unsigned int y = 0; y < size; y+=4, data+=4)
	{
		if(*(data+3) == 0x00/*<= 0x80*/)// check alpha channel
		{
			memset(data, 0x00, 4);
		}
	}

	return 0x0;
}

Texture* ResizeTexture(Texture* texture, const Dimension2& dim)
{
	if(!texture)
		return NULL;

	Texture* texOut = NULL;
	Engine* engine = Engine::GetInstance();
	video::IVideoDriver* driver = engine->GetDriver();
	Image* image = TextureToImage(texture);
	if (image)
	{
		Image* filter = driver->createImage(video::ECF_A8R8G8B8, dim);
		image->copyToScalingBoxFilter(filter, 0);
		image->drop();
		image = filter;
	}

	if (image)
	{
		texOut = driver->addTexture("temp", image);
		image->drop();
	}

	return texOut;
}

void drawBrightnessLayer(irr::video::IVideoDriver *driver, irr::core::recti position, float brightness)
{
	bool isPositive = (brightness >= 0.0f);
	u32 value = isPositive? (u32)(brightness*255) : (u32)((1.0f+brightness)*255);

	video::SColor color(value,value,value,value);
	//irr::video::SMaterial material;
	irr::video::SMaterial& material = driver->getMaterial2D();

	// Store and clear the world matrix
	irr::core::matrix4 oldWorldMat = driver->getTransform(irr::video::ETS_WORLD);
	// Store and clear the projection matrix
	irr::core::matrix4 oldProjMat = driver->getTransform(irr::video::ETS_PROJECTION);
	// Store and clear the view matrix
	irr::core::matrix4 oldViewMat = driver->getTransform(irr::video::ETS_VIEW);

	core::matrix4 m;
	// this fixes some problems with pixel exact rendering, but also breaks nice texturing
	driver->setTransform(irr::video::ETS_WORLD, m);

	// adjust the view such that pixel center aligns with texels
	// Otherwise, subpixel artifacts will occur
	if(driver->getDriverType() == video::EDT_DIRECT3D9)
		m.setTranslation(core::vector3df(-0.5f,-0.5f,0));
	driver->setTransform(irr::video::ETS_VIEW, m);

	const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
	m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0, 1.0);
	m.setTranslation(core::vector3df(-1,1,0));
	driver->setTransform(irr::video::ETS_PROJECTION, m);

	irr::core::vector3df corner[4];

	corner[0] = irr::core::vector3df((f32)position.UpperLeftCorner.X, (f32)position.UpperLeftCorner.Y, 0.0f);
	corner[1] = irr::core::vector3df((f32)position.LowerRightCorner.X, (f32)position.UpperLeftCorner.Y, 0.0f);
	corner[2] = irr::core::vector3df((f32)position.UpperLeftCorner.X, (f32)position.LowerRightCorner.Y, 0.0f);
	corner[3] = irr::core::vector3df((f32)position.LowerRightCorner.X, (f32)position.LowerRightCorner.Y, 0.0f);


	// Vertices for the image
	irr::u16 indices[6] = { 0, 1, 2, 3 ,2 ,1 };

	irr::video::S3DVertex vertice[4];
	for (int x = 0; x < 4; ++x)
	{

		vertice[x].Pos = irr::core::vector3df(corner[x].X, corner[x].Y, 0.0f);
		vertice[x].Color = color;
	}

	material.Lighting = false;
	material.ZWriteEnable = false;
	material.ZBuffer = false;
	material.BackfaceCulling = false;
	material.TextureLayer[0].Texture = NULL;

	material.MaterialTypeParam = irr::video::pack_textureBlendFunc(isPositive? irr::video::EBF_ONE : irr::video::EBF_DST_COLOR,
		isPositive? irr::video::EBF_ONE : irr::video::EBF_ZERO, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);

	material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;

	driver->setMaterial(material);
	//driver->draw2DVertexPrimitiveList(&vertices[0],4,&indices[0],2);
	driver->drawIndexedTriangleList(&vertice[0],4,&indices[0],2);

	// Restore projection and view matrices
	driver->setTransform(irr::video::ETS_WORLD,oldWorldMat);
	driver->setTransform(irr::video::ETS_PROJECTION,oldProjMat);
	driver->setTransform(irr::video::ETS_VIEW,oldViewMat);
}


void draw2DImage(irr::video::IVideoDriver *driver, irr::video::ITexture* texture, irr::core::rect<irr::s32> sourceRect, 
				 irr::core::position2d<irr::s32> position, irr::core::vector2df scale, bool useAlphaChannel, irr::video::SColor color, 
				 bool useFiltering/* = true*/, bool usePremultipliedAlpha/* = false*/, irr::core::rect<irr::s32>* clip/* = NULL*/)
{
	if(usePremultipliedAlpha)
	{
		color.color = PremultiplyAlpha(color.color);
	}

#ifdef _IRR_WINDOWS_
	IDirect3DDevice9* D3DDevice = NULL;
#endif
	if (clip)
	{
		if (clip->isValid())
		{
			if(driver->getDriverType() == video::EDT_OPENGL)
			{
				glEnable(GL_SCISSOR_TEST);
				const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
				glScissor(clip->UpperLeftCorner.X, renderTargetSize.Height-clip->LowerRightCorner.Y,
					clip->getWidth(), clip->getHeight());
			}
#ifdef _IRR_WINDOWS_
			else if(driver->getDriverType() == video::EDT_DIRECT3D9)
			{
				D3DDevice = driver->getExposedVideoData().D3D9.D3DDev9;

				if(D3DDevice)
				{
					D3DDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
					RECT scissor;
					scissor.left = clip->UpperLeftCorner.X;
					scissor.top = clip->UpperLeftCorner.Y;
					scissor.right = clip->LowerRightCorner.X;
					scissor.bottom = clip->LowerRightCorner.Y;
					D3DDevice->SetScissorRect(&scissor);
				}
			}
#endif
		}
	}

	//irr::video::SMaterial material;
	irr::video::SMaterial& material = driver->getMaterial2D();

	// Store and clear the world matrix
	irr::core::matrix4 oldWorldMat = driver->getTransform(irr::video::ETS_WORLD);
	// Store and clear the projection matrix
	irr::core::matrix4 oldProjMat = driver->getTransform(irr::video::ETS_PROJECTION);
	// Store and clear the view matrix
	irr::core::matrix4 oldViewMat = driver->getTransform(irr::video::ETS_VIEW);

	core::matrix4 m;
	// this fixes some problems with pixel exact rendering, but also breaks nice texturing
	driver->setTransform(irr::video::ETS_WORLD, m);

	// adjust the view such that pixel center aligns with texels
	// Otherwise, subpixel artifacts will occur
	if(driver->getDriverType() == video::EDT_DIRECT3D9)
		m.setTranslation(core::vector3df(-0.5f,-0.5f,0));
	driver->setTransform(irr::video::ETS_VIEW, m);

	const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
	m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0, 1.0);
	m.setTranslation(core::vector3df(-1,1,0));
	driver->setTransform(irr::video::ETS_PROJECTION, m);

	irr::core::vector3df corner[4];

	f32 halfWidth = (f32)(sourceRect.getWidth())*0.5f*scale.X;
	f32 halfHeight = (f32)(sourceRect.getHeight())*0.5f*scale.Y;
	corner[0] = irr::core::vector3df(0.0f - halfWidth, 0.0f - halfHeight, 0.0f);
	corner[1] = irr::core::vector3df(0.0f + halfWidth, 0.0f - halfHeight, 0.0f);
	corner[2] = irr::core::vector3df(0.0f - halfWidth, 0.0f + halfHeight, 0.0f);
	corner[3] = irr::core::vector3df(0.0f + halfWidth, 0.0f + halfHeight, 0.0f);

	// transform corners
	for (int x = 0; x < 4; x++)
	{
		corner[x] += irr::core::vector3df(halfWidth, halfHeight, 0.0f);
		corner[x] += irr::core::vector3df((float)position.X, (float)position.Y, 0.0f);
	}



	// Find the uv coordinates of the sourceRect
	irr::core::vector2df uvCorner[4];
	uvCorner[0] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
	uvCorner[1] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
	uvCorner[2] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.LowerRightCorner.Y);
	uvCorner[3] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.LowerRightCorner.Y);
	const irr::core::dimension2d<u32>& texSize = texture->getOriginalSize();
	for (int x = 0; x < 4; x++)
	{
		float uvX = uvCorner[x].X/(float)texSize.Width;
		float uvY = uvCorner[x].Y/(float)texSize.Height;
		uvCorner[x] = irr::core::vector2df(uvX,uvY);
	}

	// Vertices for the image
	irr::video::S3DVertex vertices[4];
	irr::u16 indices[6] = { 0, 1, 2, 3 ,2 ,1 };

	for (int x = 0; x < 4; x++)
	{
		vertices[x].Pos = irr::core::vector3df(corner[x].X, corner[x].Y, 0.0f);
		vertices[x].TCoords = uvCorner[x];
		vertices[x].Color = color;
	}

	material.Lighting = false;
	material.ZWriteEnable = false;
	material.ZBuffer = false;
	material.BackfaceCulling = false;
	material.TextureLayer[0].Texture = texture;
	// Fix dark border appearance when texture scaled down with bilinear or trilinear filter on
	if(uvCorner[0].X >= 0.0f && uvCorner[3].X <= 1.0f)
		material.TextureLayer[0].TextureWrapU = video::ETC_CLAMP;
	else
		material.TextureLayer[0].TextureWrapU = video::ETC_REPEAT;
	if(uvCorner[0].Y >= 0.0f && uvCorner[3].Y <= 1.0f)
		material.TextureLayer[0].TextureWrapV = video::ETC_CLAMP;
	else
		material.TextureLayer[0].TextureWrapV = video::ETC_REPEAT;

	if(usePremultipliedAlpha)
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_ONE, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);
	else
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_SRC_ALPHA, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);

	if (useAlphaChannel)
		material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;
	else
		material.MaterialType = irr::video::EMT_SOLID;

	material.TextureLayer[0].BilinearFilter = false;
	material.TextureLayer[0].TrilinearFilter = useFiltering && !(scale.X == 0.0f && scale.Y == 0.0f);

	driver->setMaterial(material);
	//driver->draw2DVertexPrimitiveList(&vertices[0],4,&indices[0],2);
	driver->drawIndexedTriangleList(&vertices[0],4,&indices[0],2);

	// Restore projection and view matrices
	driver->setTransform(irr::video::ETS_WORLD,oldWorldMat);
	driver->setTransform(irr::video::ETS_PROJECTION,oldProjMat);
	driver->setTransform(irr::video::ETS_VIEW,oldViewMat);

	if (clip)
	{
		if(driver->getDriverType() == video::EDT_OPENGL)
			glDisable(GL_SCISSOR_TEST);
	}
#ifdef _IRR_WINDOWS_
	else if(driver->getDriverType() == video::EDT_DIRECT3D9)
	{
		if(D3DDevice)
			D3DDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
	}
#endif
}


void draw2DImage(irr::video::IVideoDriver *driver, irr::video::ITexture* texture, irr::core::rect<irr::s32> sourceRect, 
				 irr::core::position2d<irr::s32> position, irr::core::position2d<irr::s32> rotationPoint, irr::f32 rotation, 
				 irr::core::vector2df scale, bool useAlphaChannel, irr::video::SColor color, bool useFiltering/* = true*/, 
				 bool usePremultipliedAlpha/* = false*/, irr::core::rect<irr::s32>* clip/* = NULL*/)
{
	//irr::video::SMaterial material;
	irr::video::SMaterial& material = driver->getMaterial2D();

	if(usePremultipliedAlpha)
	{
		color.color = PremultiplyAlpha(color.color);
	}

#ifdef _IRR_WINDOWS_
	IDirect3DDevice9* D3DDevice = NULL;
#endif
	if (clip)
	{
		if (clip->isValid())
		{
			if(driver->getDriverType() == video::EDT_OPENGL)
			{
				glEnable(GL_SCISSOR_TEST);
				const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
				glScissor(clip->UpperLeftCorner.X, renderTargetSize.Height-clip->LowerRightCorner.Y,
					clip->getWidth(), clip->getHeight());
			}
#ifdef _IRR_WINDOWS_
			else if(driver->getDriverType() == video::EDT_DIRECT3D9)
			{
				D3DDevice = driver->getExposedVideoData().D3D9.D3DDev9;

				if(D3DDevice)
				{
					D3DDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
					RECT scissor;
					scissor.left = clip->UpperLeftCorner.X;
					scissor.top = clip->UpperLeftCorner.Y;
					scissor.right = clip->LowerRightCorner.X;
					scissor.bottom = clip->LowerRightCorner.Y;
					D3DDevice->SetScissorRect(&scissor);
				}
			}
#endif
		}
	}

	// Store and clear the world matrix
	irr::core::matrix4 oldWorldMat = driver->getTransform(irr::video::ETS_WORLD);
	// Store and clear the projection matrix
	irr::core::matrix4 oldProjMat = driver->getTransform(irr::video::ETS_PROJECTION);
	// Store and clear the view matrix
	irr::core::matrix4 oldViewMat = driver->getTransform(irr::video::ETS_VIEW);

	core::matrix4 m;
	// this fixes some problems with pixel exact rendering, but also breaks nice texturing
	driver->setTransform(irr::video::ETS_WORLD, m);

	// adjust the view such that pixel center aligns with texels
	// Otherwise, subpixel artifacts will occur
	if(driver->getDriverType() == video::EDT_DIRECT3D9)
		m.setTranslation(core::vector3df(-0.5f,-0.5f,0));
	driver->setTransform(irr::video::ETS_VIEW, m);

	const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
	m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0, 1.0);
	m.setTranslation(core::vector3df(-1,1,0));
	driver->setTransform(irr::video::ETS_PROJECTION, m);

	// Find the positions of corners
	irr::core::vector2df corner[4];

	corner[0] = irr::core::vector2df((f32)position.X,(f32)position.Y);
	corner[1] = irr::core::vector2df((f32)position.X+(sourceRect.getWidth()-1)*scale.X,(f32)position.Y);
	corner[2] = irr::core::vector2df((f32)position.X,(f32)position.Y+(sourceRect.getHeight()-1)*scale.Y);
	corner[3] = irr::core::vector2df((f32)position.X+(sourceRect.getWidth()-1)*scale.X,(f32)position.Y+(sourceRect.getHeight()-1)*scale.Y);

	// Rotate corners
	if (rotation != 0.0f)
		for (int x = 0; x < 4; x++)
			corner[x].rotateBy(rotation,irr::core::vector2df((f32)rotationPoint.X, (f32)rotationPoint.Y));


	// Find the uv coordinates of the sourceRect
	irr::core::vector2df uvCorner[4];
	uvCorner[0] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
	uvCorner[1] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
	uvCorner[2] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.LowerRightCorner.Y);
	uvCorner[3] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.LowerRightCorner.Y);
	const irr::core::dimension2d<u32>& texSize = texture->getOriginalSize();
	for (int x = 0; x < 4; x++)
	{
		float uvX = uvCorner[x].X/(float)texSize.Width;
		float uvY = uvCorner[x].Y/(float)texSize.Height;
		uvCorner[x] = irr::core::vector2df(uvX,uvY);
	}

	// Vertices for the image
	irr::video::S3DVertex vertices[4];
	irr::u16 indices[6] = { 0, 1, 2, 3 ,2 ,1 };

	for (int x = 0; x < 4; x++)
	{
		vertices[x].Pos = irr::core::vector3df(corner[x].X, corner[x].Y, 0.0f);
		vertices[x].TCoords = uvCorner[x];
		vertices[x].Color = color;
	}

	material.Lighting = false;
	material.ZWriteEnable = false;
	material.ZBuffer = false;
	material.TextureLayer[0].Texture = texture;
	// Fix dark border appearance when texture scaled down with bilinear or trilinear filter on
	if(uvCorner[0].X >= 0.0f && uvCorner[3].X <= 1.0f)
		material.TextureLayer[0].TextureWrapU = video::ETC_CLAMP;
	else
		material.TextureLayer[0].TextureWrapU = video::ETC_REPEAT;
	if(uvCorner[0].Y >= 0.0f && uvCorner[3].Y <= 1.0f)
		material.TextureLayer[0].TextureWrapV = video::ETC_CLAMP;
	else
		material.TextureLayer[0].TextureWrapV = video::ETC_REPEAT;


	if(usePremultipliedAlpha)
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_ONE, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);
	else
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_SRC_ALPHA, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);

	if (useAlphaChannel)
		material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;
	else
		material.MaterialType = irr::video::EMT_SOLID;

	material.TextureLayer[0].BilinearFilter = false;
	if(scale.X != 1.0f || scale.Y != 1.0f || rotation != 0.0f)// use filter only if image have scale or rotation changed
		material.TextureLayer[0].TrilinearFilter = useFiltering;

	driver->setMaterial(material);
	//driver->draw2DVertexPrimitiveList(&vertices[0],4,&indices[0],2);
	driver->drawIndexedTriangleList(&vertices[0],4,&indices[0],2);

	// Restore projection and view matrices
	driver->setTransform(irr::video::ETS_WORLD,oldWorldMat);
	driver->setTransform(irr::video::ETS_PROJECTION,oldProjMat);
	driver->setTransform(irr::video::ETS_VIEW,oldViewMat);

	if (clip)
	{
		if(driver->getDriverType() == video::EDT_OPENGL)
			glDisable(GL_SCISSOR_TEST);
	}
#ifdef _IRR_WINDOWS_
	else if(driver->getDriverType() == video::EDT_DIRECT3D9)
	{
		if(D3DDevice)
			D3DDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
	}
#endif
}

void draw2DImage(irr::video::IVideoDriver *driver, irr::video::ITexture* texture, irr::core::rect<irr::s32> sourceRect, 
				 const core::matrix4& mat, bool useAlphaChannel, irr::video::SColor color, bool useFiltering/* = true*/, 
				 bool usePremultipliedAlpha/* = false*/, irr::core::rect<irr::s32>* clip/* = NULL*/)
{
	//irr::video::SMaterial material;
	irr::video::SMaterial& material = driver->getMaterial2D();

	if(usePremultipliedAlpha)
	{
		color.color = PremultiplyAlpha(color.color);
	}

#ifdef _IRR_WINDOWS_
	IDirect3DDevice9* D3DDevice = NULL;
#endif
	if (clip)
	{
		if (clip->isValid())
		{
			if(driver->getDriverType() == video::EDT_OPENGL)
			{
				glEnable(GL_SCISSOR_TEST);
				const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
				glScissor(clip->UpperLeftCorner.X, renderTargetSize.Height-clip->LowerRightCorner.Y,
					clip->getWidth(), clip->getHeight());
			}
#ifdef _IRR_WINDOWS_
			else if(driver->getDriverType() == video::EDT_DIRECT3D9)
			{
				D3DDevice = driver->getExposedVideoData().D3D9.D3DDev9;
				
				if(D3DDevice)
				{
					D3DDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, TRUE);
					RECT scissor;
					scissor.left = clip->UpperLeftCorner.X;
					scissor.top = clip->UpperLeftCorner.Y;
					scissor.right = clip->LowerRightCorner.X;
					scissor.bottom = clip->LowerRightCorner.Y;
					D3DDevice->SetScissorRect(&scissor);
				}
			}
#endif
		}
	}

	// Store and clear the world matrix
	irr::core::matrix4 oldWorldMat = driver->getTransform(irr::video::ETS_WORLD);
	// Store and clear the projection matrix
	irr::core::matrix4 oldProjMat = driver->getTransform(irr::video::ETS_PROJECTION);
	// Store and clear the view matrix
	irr::core::matrix4 oldViewMat = driver->getTransform(irr::video::ETS_VIEW);

	core::matrix4 m;
	// this fixes some problems with pixel exact rendering, but also breaks nice texturing
	driver->setTransform(irr::video::ETS_WORLD, mat);

	// adjust the view such that pixel center aligns with texels
	// Otherwise, subpixel artifacts will occur
	if(driver->getDriverType() == video::EDT_DIRECT3D9)
		m.setTranslation(core::vector3df(-0.5f,-0.5f,0));
	driver->setTransform(irr::video::ETS_VIEW, m);

	const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
	m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0, 1.0);
	m.setTranslation(core::vector3df(-1,1,0));
	driver->setTransform(irr::video::ETS_PROJECTION, m);

	// Find the positions of corners
	irr::core::vector3df corner[4];

	f32 halfWidth = (f32)(sourceRect.getWidth())*0.5f;
	f32 halfHeight = (f32)(sourceRect.getHeight())*0.5f;
	corner[0] = irr::core::vector3df(0.0f - halfWidth, 0.0f - halfHeight, 0.0f);
	corner[1] = irr::core::vector3df(0.0f + halfWidth, 0.0f - halfHeight, 0.0f);
	corner[2] = irr::core::vector3df(0.0f - halfWidth, 0.0f + halfHeight, 0.0f);
	corner[3] = irr::core::vector3df(0.0f + halfWidth, 0.0f + halfHeight, 0.0f);

	// Find the uv coordinates of the sourceRect
	irr::core::vector2df uvCorner[4];
	uvCorner[0] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
	uvCorner[1] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
	uvCorner[2] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.LowerRightCorner.Y);
	uvCorner[3] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.LowerRightCorner.Y);
	const irr::core::dimension2d<u32>& texSize = texture->getOriginalSize();
	for (int x = 0; x < 4; x++)
	{
		float uvX = uvCorner[x].X/(float)texSize.Width;
		float uvY = uvCorner[x].Y/(float)texSize.Height;
		uvCorner[x] = irr::core::vector2df(uvX,uvY);
	}

	// Vertices for the image
	irr::video::S3DVertex vertices[4];
	irr::u16 indices[6] = { 0, 1, 2, 3 ,2 ,1 };

	for (int x = 0; x < 4; x++)
	{
		vertices[x].Pos = corner[x];
		vertices[x].TCoords = uvCorner[x];
		vertices[x].Color = color;
	}

	//driver->enableMaterial2D(true);
	material.Lighting = false;
	material.ZWriteEnable = false;
	material.ZBuffer = false;
	material.BackfaceCulling = false;
	//material.AntiAliasing = true;
	material.TextureLayer[0].Texture = texture;
	// Fix dark border appearance when texture scaled down with bilinear or trilinear filter on
	if(uvCorner[0].X >= 0.0f && uvCorner[3].X <= 1.0f)
		material.TextureLayer[0].TextureWrapU = video::ETC_CLAMP;
	else
		material.TextureLayer[0].TextureWrapU = video::ETC_REPEAT;
	if(uvCorner[0].Y >= 0.0f && uvCorner[3].Y <= 1.0f)
		material.TextureLayer[0].TextureWrapV = video::ETC_CLAMP;
	else
		material.TextureLayer[0].TextureWrapV = video::ETC_REPEAT;

	if(usePremultipliedAlpha)
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_ONE, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);
	else
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_SRC_ALPHA, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);

	if (useAlphaChannel)
		material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;
	else
		material.MaterialType = irr::video::EMT_SOLID;

	material.TextureLayer[0].BilinearFilter = false;
	material.TextureLayer[0].TrilinearFilter = useFiltering;

	driver->setMaterial(material);
	//driver->draw2DVertexPrimitiveList(&vertices[0],4,&indices[0],2);
	driver->drawIndexedTriangleList(&vertices[0],4,&indices[0],2);

	//driver->enableMaterial2D(false);

	// Restore projection and view matrices
	driver->setTransform(irr::video::ETS_WORLD,oldWorldMat);
	driver->setTransform(irr::video::ETS_PROJECTION,oldProjMat);
	driver->setTransform(irr::video::ETS_VIEW,oldViewMat);

	if (clip)
	{
		if(driver->getDriverType() == video::EDT_OPENGL)
			glDisable(GL_SCISSOR_TEST);
	}
#ifdef _IRR_WINDOWS_
	else if(driver->getDriverType() == video::EDT_DIRECT3D9)
	{
		if(D3DDevice)
			D3DDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
	}
#endif
}

void draw2DImageBatch(irr::video::IVideoDriver *driver, irr::video::ITexture* texture, const irr::core::array<S2DImageData>& imageData, bool useAlphaChannel, bool useFiltering/* = true*/, bool usePremultipliedAlpha/* = false*/)
{
	if(imageData.empty())
		return;

	//irr::video::SMaterial material;
	irr::video::SMaterial& material = driver->getMaterial2D();

	// Store and clear the world matrix
	irr::core::matrix4 oldWorldMat = driver->getTransform(irr::video::ETS_WORLD);
	// Store and clear the projection matrix
	irr::core::matrix4 oldProjMat = driver->getTransform(irr::video::ETS_PROJECTION);
	// Store and clear the view matrix
	irr::core::matrix4 oldViewMat = driver->getTransform(irr::video::ETS_VIEW);

	core::matrix4 m;
	// this fixes some problems with pixel exact rendering, but also breaks nice texturing
	driver->setTransform(irr::video::ETS_WORLD, m);

	// adjust the view such that pixel center aligns with texels
	// Otherwise, subpixel artifacts will occur
	if(driver->getDriverType() == video::EDT_DIRECT3D9)
		m.setTranslation(core::vector3df(-0.5f,-0.5f,0));
	driver->setTransform(irr::video::ETS_VIEW, m);

	const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
	m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0, 1.0);
	m.setTranslation(core::vector3df(-1,1,0));
	driver->setTransform(irr::video::ETS_PROJECTION, m);

	irr::core::array<irr::video::S3DVertex> vertices;
	irr::core::array<irr::u16> indices;

	for(u32 i = 0; i < imageData.size(); ++i)
	{
		const S2DImageData& data = imageData[i];
		// Find the positions of corners
		irr::core::vector3df corner[4];

		const irr::core::rect<irr::s32>& sourceRect = data.SourceRect;
		f32 halfWidth = (f32)(sourceRect.getWidth())*0.5f;
		f32 halfHeight = (f32)(sourceRect.getHeight())*0.5f;
		corner[0] = irr::core::vector3df(0.0f - halfWidth, 0.0f - halfHeight, 0.0f);
		corner[1] = irr::core::vector3df(0.0f + halfWidth, 0.0f - halfHeight, 0.0f);
		corner[2] = irr::core::vector3df(0.0f - halfWidth, 0.0f + halfHeight, 0.0f);
		corner[3] = irr::core::vector3df(0.0f + halfWidth, 0.0f + halfHeight, 0.0f);
		for (int x = 0; x < 4; x++)
		{
			data.DestMatrix.transformVect(corner[x]);
		}

		// Find the uv coordinates of the sourceRect
		irr::core::vector2df uvCorner[4];
		uvCorner[0] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
		uvCorner[1] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.UpperLeftCorner.Y);
		uvCorner[2] = irr::core::vector2df((f32)sourceRect.UpperLeftCorner.X,(f32)sourceRect.LowerRightCorner.Y);
		uvCorner[3] = irr::core::vector2df((f32)sourceRect.LowerRightCorner.X,(f32)sourceRect.LowerRightCorner.Y);
		const irr::core::dimension2d<u32>& texSize = texture->getOriginalSize();
		for (int x = 0; x < 4; x++)
		{
			float uvX = uvCorner[x].X/(float)texSize.Width;
			float uvY = uvCorner[x].Y/(float)texSize.Height;
			uvCorner[x] = irr::core::vector2df(uvX,uvY);
		}

		// Vertices for the image
		irr::video::S3DVertex vertex;
		irr::u16 base = vertices.size();
		indices.push_back(base);
		indices.push_back(base+1);
		indices.push_back(base+2);
		indices.push_back(base+3);
		indices.push_back(base+2);
		indices.push_back(base+1);

		for (int x = 0; x < 4; x++)
		{
			vertex.Pos = corner[x];
			vertex.TCoords = uvCorner[x];
			vertex.Color.color = usePremultipliedAlpha? PremultiplyAlpha(data.VertexColor.color) : data.VertexColor.color;
			vertices.push_back(vertex);
		}
	}

	//driver->enableMaterial2D(true);
	material.Lighting = false;
	material.ZWriteEnable = false;
	material.ZBuffer = false;
	material.BackfaceCulling = false;
	//material.AntiAliasing = true;
	material.TextureLayer[0].Texture = texture;
	// Fix dark border appearance when texture scaled down with bilinear or trilinear filter on
	material.TextureLayer[0].TextureWrapU = video::ETC_CLAMP;
	material.TextureLayer[0].TextureWrapV = video::ETC_CLAMP;

	if(usePremultipliedAlpha)
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_ONE, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);
	else
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_SRC_ALPHA, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);

	if (useAlphaChannel)
		material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;
	else
		material.MaterialType = irr::video::EMT_SOLID;

	material.TextureLayer[0].BilinearFilter = false;
	material.TextureLayer[0].TrilinearFilter = useFiltering;

	driver->setMaterial(material);
	//driver->draw2DVertexPrimitiveList(&vertices[0],4,&indices[0],2);
	driver->drawIndexedTriangleList(&vertices[0],4*imageData.size(),&indices[0],2*imageData.size());

	//driver->enableMaterial2D(false);

	// Restore projection and view matrices
	driver->setTransform(irr::video::ETS_WORLD,oldWorldMat);
	driver->setTransform(irr::video::ETS_PROJECTION,oldProjMat);
	driver->setTransform(irr::video::ETS_VIEW,oldViewMat);
}

void draw2DRect(video::IVideoDriver *driver, Dimension2 sourceDim, const core::matrix4& mat, video::SColor color)
{
	//video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	//driver->draw2DRectangleOutline(Rectangle2(Position2(pos.X - (m_DestSize.Width>>1), pos.Y - (m_DestSize.Height>>1)) + m_DestPosition, m_DestSize), color);

	// Find the positions of corners
	irr::core::vector3df corner[4];

	f32 halfWidth = (f32)(sourceDim.Width-1)*0.5f;
	f32 halfHeight = (f32)(sourceDim.Height-1)*0.5f;
	corner[0] = irr::core::vector3df(0.0f - halfWidth, 0.0f - halfHeight, 0.0f);
	corner[1] = irr::core::vector3df(0.0f + halfWidth, 0.0f - halfHeight, 0.0f);
	corner[2] = irr::core::vector3df(0.0f - halfWidth, 0.0f + halfHeight, 0.0f);
	corner[3] = irr::core::vector3df(0.0f + halfWidth, 0.0f + halfHeight, 0.0f);

	// transform corners
	for (int x = 0; x < 4; x++)
	{
		mat.transformVect(corner[x]);
		//corner[x] += irr::core::vector3df(halfWidth, halfHeight, 0.0f);
	}

	Position2 A((s32)corner[0].X, (s32)corner[0].Y);
	Position2 B((s32)corner[1].X, (s32)corner[1].Y);
	Position2 C((s32)corner[3].X, (s32)corner[3].Y);
	Position2 D((s32)corner[2].X, (s32)corner[2].Y);
	driver->draw2DLine(A, B, color);
	driver->draw2DLine(B, C, color);
	driver->draw2DLine(C, D, color);
	driver->draw2DLine(D, A, color);
}

void fill2DRect(irr::video::IVideoDriver *driver, const irr::core::rect<s32>& position, irr::video::SColor colorLeftUp, irr::video::SColor colorRightUp, irr::video::SColor colorLeftDown, irr::video::SColor colorRightDown, bool usePremultipliedAlpha/* = false*/)
{
	//irr::video::SMaterial material;
	irr::video::SMaterial& material = driver->getMaterial2D();

	if(usePremultipliedAlpha)
	{
		colorLeftUp.color = PremultiplyAlpha(colorLeftUp.color);
		colorRightUp.color = PremultiplyAlpha(colorRightUp.color);
		colorLeftDown.color = PremultiplyAlpha(colorLeftDown.color);
		colorRightDown.color = PremultiplyAlpha(colorRightDown.color);
	}

	video::SColor colors[4] = {colorLeftUp, colorRightUp, colorLeftDown, colorRightDown};

	// Store and clear the world matrix
	irr::core::matrix4 oldWorldMat = driver->getTransform(irr::video::ETS_WORLD);
	// Store and clear the projection matrix
	irr::core::matrix4 oldProjMat = driver->getTransform(irr::video::ETS_PROJECTION);
	// Store and clear the view matrix
	irr::core::matrix4 oldViewMat = driver->getTransform(irr::video::ETS_VIEW);

	core::matrix4 m;
	// this fixes some problems with pixel exact rendering, but also breaks nice texturing
	driver->setTransform(irr::video::ETS_WORLD, m);

	// adjust the view such that pixel center aligns with texels
	// Otherwise, subpixel artifacts will occur
	if(driver->getDriverType() == video::EDT_DIRECT3D9)
		m.setTranslation(core::vector3df(-0.5f,-0.5f,0));
	driver->setTransform(irr::video::ETS_VIEW, m);

	const core::dimension2d<u32>& renderTargetSize = driver->getCurrentRenderTargetSize();
	m.buildProjectionMatrixOrthoLH(f32(renderTargetSize.Width), f32(-(s32)(renderTargetSize.Height)), -1.0, 1.0);
	m.setTranslation(core::vector3df(-1,1,0));
	driver->setTransform(irr::video::ETS_PROJECTION, m);


	// Find the positions of corners
	irr::core::vector3df corner[4];

	f32 halfWidth = (f32)(position.getWidth())*0.5f;
	f32 halfHeight = (f32)(position.getHeight())*0.5f;
	corner[0] = irr::core::vector3df(0.0f - halfWidth, 0.0f - halfHeight, 0.0f);
	corner[1] = irr::core::vector3df(0.0f + halfWidth, 0.0f - halfHeight, 0.0f);
	corner[2] = irr::core::vector3df(0.0f - halfWidth, 0.0f + halfHeight, 0.0f);
	corner[3] = irr::core::vector3df(0.0f + halfWidth, 0.0f + halfHeight, 0.0f);

	// transform corners
	for (int x = 0; x < 4; x++)
	{
		corner[x] += irr::core::vector3df(halfWidth, halfHeight, 0.0f);
		corner[x] += irr::core::vector3df((float)position.UpperLeftCorner.X, (float)position.UpperLeftCorner.Y, 0.0f);
	}

	// Vertices for the image
	irr::u16 indices[6] = { 0, 1, 2, 3 ,2 ,1 };

	irr::video::S3DVertex vertice[4];
	for (int x = 0; x < 4; ++x)
	{

		vertice[x].Pos = irr::core::vector3df(corner[x].X, corner[x].Y, 0.0f);
		vertice[x].Color = colors[x];
	}

	material.Lighting = false;
	material.ZWriteEnable = false;
	material.ZBuffer = false;
	material.BackfaceCulling = false;
	material.TextureLayer[0].Texture = NULL;


	if(usePremultipliedAlpha)
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_ONE, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);
	else
		material.MaterialTypeParam = irr::video::pack_textureBlendFunc(irr::video::EBF_SRC_ALPHA, irr::video::EBF_ONE_MINUS_SRC_ALPHA, irr::video::EMFN_MODULATE_1X, irr::video::EAS_TEXTURE | irr::video::EAS_VERTEX_COLOR);

	material.MaterialType = irr::video::EMT_ONETEXTURE_BLEND;

	driver->setMaterial(material);
	//driver->draw2DVertexPrimitiveList(&vertices[0],4,&indices[0],2);
	driver->drawIndexedTriangleList(&vertice[0],4,&indices[0],2);

	// Restore projection and view matrices
	driver->setTransform(irr::video::ETS_WORLD,oldWorldMat);
	driver->setTransform(irr::video::ETS_PROJECTION,oldProjMat);
	driver->setTransform(irr::video::ETS_VIEW,oldViewMat);
}

u32 PremultiplyAlpha(u32 color)
{
	u32 a = (color>>24)/* & 0xff*/ + 1;
	u32 r = (a*((color>>16) & 0xff))>>8;
	u32 g = (a*((color>>8) & 0xff))>>8;
	u32 b = (a*(color & 0xff))>>8;
	return (--a<<24) | (r<<16) | (g<<8) | b;
}

void PremultiplyAlpha(irr::video::ITexture* texture)
{
	if(!texture || texture->getColorFormat() != irr::video::ECF_A8R8G8B8)
		return;

	u32* color = (u32*)texture->lock();
	const irr::core::dimension2d<u32>& dim = texture->getSize();
	u32 size = dim.Height*dim.Width;
	do
	{
		*color = PremultiplyAlpha(*color);
		++color;
	}while (--size);

	texture->unlock();
}

irr::core::stringc StringFormat(const char * fmt,...)
{
	va_list va;
	va_start(va,fmt);
	char buffer[300];
	int ret = vsnprintf(buffer,300,fmt,va);
	assert(ret >= 0);
	va_end(va);
	return buffer;
}

irr::core::stringw StringFormat(const wchar_t * fmt,...)
{
	va_list va;
	va_start(va,fmt);
	wchar_t buffer[300];
	int ret = vswprintf(buffer,300,fmt,va);
	assert(ret >= 0);
	va_end(va);
	return buffer;
}

void FormatString(core::stringw& strOut, const core::stringw& str, gui::IGUIFont* font, const Dimension2& dim, bool ignoreBottom/* = false*/)
{
	if(!font)
		return;

	u32 size = str.size();
	if(size == 0)
		return;

	strOut = str;

	u32 cursor = 0;
	u32 lastBackspace = (u32)(-1);
	u32 lastSpace = (u32)(-1);
	u32 currentHeight = 0;
	while(cursor < size)
	{
		const wchar_t& c = strOut[cursor];

		if(lastSpace != lastBackspace && lastSpace != (u32)(-1))
		{
			Dimension2 lineSize = font->getDimension(strOut.subString(lastBackspace+1, cursor - lastBackspace).c_str());
			if(lineSize.Width >= dim.Width && (!ignoreBottom || currentHeight < dim.Height))
			{
				strOut[lastSpace] = L'\n';// replace the last space ok with a backspace
				lastBackspace = lastSpace;
				currentHeight += font->getDimension(L" ").Height;
				lastSpace = (u32)(-1);// Only one break per space
			}
		}

		bool lineBreak = false;
		if (c == L'\r') // Mac or Windows breaks
		{
			lineBreak = true;
			if (strOut[cursor+1] == L'\n') // Windows breaks
			{
				++cursor;
			}
		}
		else if (c == L'\n') // Unix breaks
		{
			lineBreak = true;
		}
		if (lineBreak)
		{
			lastBackspace = cursor;
			currentHeight += font->getDimension(L" ").Height;
		}
		else if(isspace(c))
		{
			lastSpace = cursor;
		}
		++cursor;
	}
}

void GetStringEndl(const core::stringw& str, core::array<u32>& endlOffsetsOut)
{
	u32 size = str.size();
	if(size == 0)
		return;

	u32 cursor = 0;
	while(cursor < size)
	{
		const wchar_t& c = str[cursor];

		bool lineBreak = false;
		if (c == L'\r') // Mac or Windows breaks
		{
			lineBreak = true;
			if (str[cursor+1] == L'\n') // Windows breaks
			{
				++cursor;
			}
		}
		else if (c == L'\n') // Unix breaks
		{
			lineBreak = true;
		}
		if (lineBreak)
		{
			endlOffsetsOut.push_back(cursor);
		}

		++cursor;
	}
}

core::stringw SubStringLines(const core::stringw& str, u32 lineStart, u32 nbLines/* = 0*/, u32* startOut/* = NULL*/, u32* endOut/* = NULL*/, u32* nbTotalLines/* = NULL*/)
{
	core::array<u32> endlOffsets;
	GetStringEndl(str, endlOffsets);
	if(nbTotalLines)
		*nbTotalLines = endlOffsets.size();
	if(lineStart >= endlOffsets.size())
		return L"";
	u32 lineEnd = lineStart + nbLines;

	u32 start = (lineStart == 0 || endlOffsets.empty())? 0 : endlOffsets[lineStart-1]+1;
	u32 end = (nbLines == 0 || lineEnd > endlOffsets.size())? str.size() : endlOffsets[lineEnd-1];
	if(startOut)
		*startOut = start;
	if(endOut)
		*endOut = end;
	return str.subString(start, end-start);
}

bool CopyFile(const char* srcName, const char* destName, bool overwrite/* = true*/)
{
	char buf[BUFSIZ];
	size_t size;

	int ret = strcmp(srcName, destName);
	if(ret == 0)
		return false;

	FILE* source = fopen(srcName, "rb");
	if(!source)
		return false;

	if(!overwrite)
		if(fopen(destName, "rb"))// existing file
			return false;

	FILE* dest = fopen(destName, "wb");

	if(source && dest)
	{
		while (size = fread(buf, 1, BUFSIZ, source))
		{
			fwrite(buf, 1, size, dest);
		}
	}

	if(source)
		fclose(source);
	if(dest)
		fclose(dest);

	return true;
}

Position2 GetShakeOffset(float time, float renderScale/* = 1.0f*/, float strengh/* = 1.0f*/, float frequency/* = 1.0f*/)
{
	Position2 shakeOffset(P2Zero);

	float vib = cosf(frequency*3.0f*2.0f*core::PI*time);
	shakeOffset += Position2((s32)(renderScale*strengh*24*vib), (s32)(renderScale*strengh*24*vib));
	float vib2 = sinf(frequency*5.0f*2.0f*core::PI*time);
	shakeOffset += Position2((s32)(renderScale*strengh*-10*vib2), (s32)(renderScale*strengh*10*vib2));
	float vib3 = sinf(frequency*0.6f*2.0f*core::PI*time) + sinf(frequency*0.25f*2.0f*core::PI*time);
	float vib4 = cosf(frequency*7.0f*2.0f*core::PI*time);
	if(abs(vib3) > 1.2f)
		shakeOffset += Position2((s32)(renderScale*strengh*24*vib4), 0);

	return shakeOffset;
}

// Slider
bool Slider::ms_IsInitialized = false;
Texture* Slider::ms_Elements[Slider::ET_MAX] = {NULL, NULL, NULL};

void Slider::Initialize()
{
	if(!ms_IsInitialized)
	{
		Engine* engine = Engine::GetInstance();
		video::IVideoDriver* driver = engine->GetDriver();

		SAFE_LOAD_IMAGE(driver, ms_Elements[ET_UNDER], engine->GetGamePath("system/02_Dipstick_under.png"), true);
		SAFE_LOAD_IMAGE(driver, ms_Elements[ET_OVER], engine->GetGamePath("system/02_Dipstick_blue.png"), true);
		SAFE_LOAD_IMAGE(driver, ms_Elements[ET_SLIDER], engine->GetGamePath("system/02_Dipstick_cursor.png"), true);

		ms_IsInitialized = true;
	}
}

void Slider::Cleanup()
{
	if(ms_IsInitialized)
	{
		SAFE_UNLOAD(ms_Elements[ET_UNDER]);
		SAFE_UNLOAD(ms_Elements[ET_OVER]);
		SAFE_UNLOAD(ms_Elements[ET_SLIDER]);

		ms_IsInitialized = false;
	}
}

void Slider::Render(const Rectangle2& position, IColor color/* = WHITE*/)
{
	if(!ms_IsInitialized)
	{
		Initialize();
	}

	Engine* engine = Engine::GetInstance();
	video::IVideoDriver* driver = engine->GetDriver();

	float realScale = 1.0f;
	// draw under element
	if(ms_Elements[ET_UNDER])
	{
		Dimension2 size = ms_Elements[ET_UNDER]->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);

		float scaleX = (float)position.getWidth()/size.Width;
		float scaleY = (float)position.getHeight()/size.Height;
		realScale = scaleY;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)position.UpperLeftCorner.X + scaleX*0.5f*size.Width,
				(f32)position.getCenter().Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

		draw2DImage(driver, ms_Elements[ET_UNDER], sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);
	}

	// draw over element
	if(ms_Elements[ET_OVER])
	{
		Dimension2 size = ms_Elements[ET_OVER]->getSize();
		size.Width = (u32)(m_Value * size.Width);
		Rectangle2 sourceRect(Position2(0,0), size);

		float scaleX = m_Value * position.getWidth()/size.Width;
		float scaleY = (float)position.getHeight()/size.Height;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)position.UpperLeftCorner.X + scaleX*0.5f*size.Width,
			(f32)position.getCenter().Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

		draw2DImage(driver, ms_Elements[ET_OVER], sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);
	}

	// draw slider element
	if(ms_Elements[ET_SLIDER])
	{
		Dimension2 size = ms_Elements[ET_SLIDER]->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);

		float scale = realScale;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)position.UpperLeftCorner.X + m_Value*position.getWidth(),
			(f32)position.getCenter().Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)scale, (f32)scale, 0.0f));

		draw2DImage(driver, ms_Elements[ET_SLIDER], sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);
	}
}

// Scrollbar
bool ScrollBar::ms_IsInitialized = false;
Texture* ScrollBar::ms_Elements[ScrollBar::ET_MAX] = {NULL, NULL};

void ScrollBar::Initialize()
{
	if(!ms_IsInitialized)
	{
		Engine* engine = Engine::GetInstance();
		video::IVideoDriver* driver = engine->GetDriver();

		SAFE_LOAD_IMAGE(driver, ms_Elements[ET_UNDER], engine->GetGamePath("system/scrollbar_under.png"), true);
		SAFE_LOAD_IMAGE(driver, ms_Elements[ET_SLIDER], engine->GetGamePath("system/scrollbar_slider.png"), true);

		ms_IsInitialized = true;
	}
}

void ScrollBar::Cleanup()
{
	if(ms_IsInitialized)
	{
		SAFE_UNLOAD(ms_Elements[ET_UNDER]);
		SAFE_UNLOAD(ms_Elements[ET_SLIDER]);

		ms_IsInitialized = false;
	}
}

void ScrollBar::Render(const Rectangle2& position, IColor color/* = WHITE*/)
{
	if(!ms_IsInitialized)
	{
		Initialize();
	}

	Engine* engine = Engine::GetInstance();
	video::IVideoDriver* driver = engine->GetDriver();

	const s32 margin = 8;// 8 + 16(scalable) + 8 = 32pxl height

	bool rotate = (position.getWidth() > position.getHeight());
	float angle = rotate? -90.0f : 0.0f;

	float realScale = 1.0f;
	// draw under element
	if(ms_Elements[ET_UNDER])
	{

		TArray<S2DImageData> data;

		// borders
		{
			Dimension2 size(ms_Elements[ET_UNDER]->getSize().Width, margin);
			float scaleX = (rotate? (float)position.getHeight():(float)position.getWidth())/size.Width;
			float scaleY = core::min_(1.0f, (rotate? (float)position.getWidth() : (float)position.getHeight())/(size.Height<<1));
			{
				Rectangle2 sourceRect(Position2(0,0), size);

				float px = (f32)position.UpperLeftCorner.X;
				if(rotate)
					px += scaleY*0.5f*size.Height;
				else
					px += scaleX*0.5f*size.Width;
				float py = (f32)position.UpperLeftCorner.Y;
				if(rotate)
					py += scaleX*0.5f*size.Width;
				else
					py += scaleY*0.5f*size.Height;

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(px, py, 0.0f)) *
					core::matrix4().setRotationAxisRadians(angle*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				S2DImageData id;
				id.DestMatrix = mat;
				id.SourceRect = sourceRect;
				id.VertexColor = color;
				data.push_back(id);
			}

			{
				Rectangle2 sourceRect(Position2(0,ms_Elements[ET_UNDER]->getSize().Height - margin), size);

				float px = (f32)position.LowerRightCorner.X;
				if(rotate)
					px -= scaleY*0.5f*size.Height;
				else
					px -= scaleX*0.5f*size.Width;
				float py = (f32)position.LowerRightCorner.Y;
				if(rotate)
					py -= scaleX*0.5f*size.Width;
				else
					py -= scaleY*0.5f*size.Height;

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(px, py, 0.0f)) *
					core::matrix4().setRotationAxisRadians(angle*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				S2DImageData id;
				id.DestMatrix = mat;
				id.SourceRect = sourceRect;
				id.VertexColor = color;
				data.push_back(id);
			}
		}

		// center
		{
			Dimension2 size = ms_Elements[ET_UNDER]->getSize() - Dimension2(0, margin<<1);
			Rectangle2 sourceRect(Position2(0,margin), size);

			float scaleX = (rotate? (float)position.getHeight():(float)position.getWidth())/size.Width;
			float scaleY = (rotate? core::max_(0.0f, (float)position.getWidth() - (margin<<1)) : core::max_(0.0f, (float)position.getHeight() - (margin<<1)))/size.Height;

			if(scaleY > 0.0f)
			{
				float px = (f32)position.UpperLeftCorner.X;
				if(rotate)
					px += scaleY*0.5f*size.Height + margin;
				else
					px += scaleX*0.5f*size.Width;
				float py = (f32)position.UpperLeftCorner.Y;
				if(rotate)
					py += scaleX*0.5f*size.Width;
				else
					py += scaleY*0.5f*size.Height + margin;

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(px, py, 0.0f)) *
					core::matrix4().setRotationAxisRadians(angle*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				S2DImageData id;
				id.DestMatrix = mat;
				id.SourceRect = sourceRect;
				id.VertexColor = color;
				data.push_back(id);
			}
		}

		draw2DImageBatch(driver, ms_Elements[ET_UNDER], data, true, true, USE_PREMULTIPLIED_ALPHA);
	}

	// draw slider element
	if(ms_Elements[ET_SLIDER])
	{
		TArray<S2DImageData> data;

		// borders
		{
 			Dimension2 size(ms_Elements[ET_SLIDER]->getSize().Width, margin);
			float scaleX = (!rotate)? (float)position.getWidth()/size.Width : (float)position.getHeight()/size.Width;
			float scaleY = core::min_(1.0f, (!rotate)? ((float)GetDisplayedSize()/GetSize()) * position.getHeight()/(size.Height<<1) :
				((float)GetDisplayedSize()/GetSize()) * position.getWidth()/(size.Height<<1));
			{
				Rectangle2 sourceRect(Position2(0,0), size);

				float px = (f32)position.UpperLeftCorner.X;
				if(rotate)
					px += scaleY*0.5f*size.Height + ((float)GetStart()/GetSize())*position.getWidth();
				else
					px += scaleX*0.5f*size.Width;
				float py = (f32)position.UpperLeftCorner.Y;
				if(rotate)
					py += scaleX*0.5f*size.Width;
				else
					py += scaleY*0.5f*size.Height + ((float)GetStart()/GetSize())*position.getHeight();

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(px, py, 0.0f)) *
					core::matrix4().setRotationAxisRadians(angle*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				S2DImageData id;
				id.DestMatrix = mat;
				id.SourceRect = sourceRect;
				id.VertexColor = color;
				data.push_back(id);
			}

			{
				Rectangle2 sourceRect(Position2(0,ms_Elements[ET_SLIDER]->getSize().Height - margin), size);

				float px = (f32)position.UpperLeftCorner.X;
				if(rotate)
					px += -scaleY*0.5f*size.Height + ((float)GetEnd()/GetSize())*position.getWidth();
				else
					px += -scaleX*0.5f*size.Width + position.getWidth();
				float py = (f32)position.UpperLeftCorner.Y;
				if(rotate)
					py += -scaleX*0.5f*size.Width + position.getHeight();
				else
					py += -scaleY*0.5f*size.Height + ((float)GetEnd()/GetSize())*position.getHeight();

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(px, py, 0.0f)) *
					core::matrix4().setRotationAxisRadians(angle*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				S2DImageData id;
				id.DestMatrix = mat;
				id.SourceRect = sourceRect;
				id.VertexColor = color;
				data.push_back(id);
			}
		}

		// center
		{
			Dimension2 size = ms_Elements[ET_SLIDER]->getSize() - Dimension2(0, margin<<1);
			Rectangle2 sourceRect(Position2(0,margin), size);

			float scaleX = (!rotate)? (float)position.getWidth()/size.Width : (float)position.getHeight()/size.Width;
			float scaleY = core::max_(0.0f, (!rotate)? ((((float)GetDisplayedSize()/GetSize()) * position.getHeight())-(margin<<1))/size.Height :
				((((float)GetDisplayedSize()/GetSize()) * position.getWidth())-(margin<<1))/size.Height);

			if(scaleY > 0.0f)
			{
				float px = (f32)position.UpperLeftCorner.X;
				if(rotate)
					px += scaleY*0.5f*size.Height + ((float)GetStart()/GetSize())*position.getWidth() + margin;
				else
					px += scaleX*0.5f*size.Width;
				float py = (f32)position.UpperLeftCorner.Y;
				if(rotate)
					py += scaleX*0.5f*size.Width;
				else
					py += scaleY*0.5f*size.Height + ((float)GetStart()/GetSize())*position.getHeight() + margin;

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(px, py, 0.0f)) *
					core::matrix4().setRotationAxisRadians(angle*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				S2DImageData id;
				id.DestMatrix = mat;
				id.SourceRect = sourceRect;
				id.VertexColor = color;
				data.push_back(id);
			}
		}

		draw2DImageBatch(driver, ms_Elements[ET_SLIDER], data, true, true, USE_PREMULTIPLIED_ALPHA);
	}
}

#if defined(_IRR_WINDOWS_)
// Get the codepage from the locale language id
// Based on the table from http://www.science.co.il/Language/Locale-Codes.asp?s=decimal
static unsigned int LocaleIdToCodepage(unsigned int lcid)
{
	switch ( lcid )
	{
	case 1098:  // Telugu
	case 1095:  // Gujarati
	case 1094:  // Punjabi
	case 1103:  // Sanskrit
	case 1111:  // Konkani
	case 1114:  // Syriac
	case 1099:  // Kannada
	case 1102:  // Marathi
	case 1125:  // Divehi
	case 1067:  // Armenian
	case 1081:  // Hindi
	case 1079:  // Georgian
	case 1097:  // Tamil
		return 0;
	case 1054:  // Thai
		return 874;
	case 1041:  // Japanese
		return 932;
	case 2052:  // Chinese (PRC)
	case 4100:  // Chinese (Singapore)
		return 936;
	case 1042:  // Korean
		return 949;
	case 5124:  // Chinese (Macau S.A.R.)
	case 3076:  // Chinese (Hong Kong S.A.R.)
	case 1028:  // Chinese (Taiwan)
		return 950;
	case 1048:  // Romanian
	case 1060:  // Slovenian
	case 1038:  // Hungarian
	case 1051:  // Slovak
	case 1045:  // Polish
	case 1052:  // Albanian
	case 2074:  // Serbian (Latin)
	case 1050:  // Croatian
	case 1029:  // Czech
		return 1250;
	case 1104:  // Mongolian (Cyrillic)
	case 1071:  // FYRO Macedonian
	case 2115:  // Uzbek (Cyrillic)
	case 1058:  // Ukrainian
	case 2092:  // Azeri (Cyrillic)
	case 1092:  // Tatar
	case 1087:  // Kazakh
	case 1059:  // Belarusian
	case 1088:  // Kyrgyz (Cyrillic)
	case 1026:  // Bulgarian
	case 3098:  // Serbian (Cyrillic)
	case 1049:  // Russian
		return 1251;
	case 8201:  // English (Jamaica)
	case 3084:  // French (Canada)
	case 1036:  // French (France)
	case 5132:  // French (Luxembourg)
	case 5129:  // English (New Zealand)
	case 6153:  // English (Ireland)
	case 1043:  // Dutch (Netherlands)
	case 9225:  // English (Caribbean)
	case 4108:  // French (Switzerland)
	case 4105:  // English (Canada)
	case 1110:  // Galician
	case 10249:  // English (Belize)
	case 3079:  // German (Austria)
	case 6156:  // French (Monaco)
	case 12297:  // English (Zimbabwe)
	case 1069:  // Basque
	case 2067:  // Dutch (Belgium)
	case 2060:  // French (Belgium)
	case 1035:  // Finnish
	case 1080:  // Faroese
	case 1031:  // German (Germany)
	case 3081:  // English (Australia)
	case 1033:  // English (United States)
	case 2057:  // English (United Kingdom)
	case 1027:  // Catalan
	case 11273:  // English (Trinidad)
	case 7177:  // English (South Africa)
	case 1030:  // Danish
	case 13321:  // English (Philippines)
	case 15370:  // Spanish (Paraguay)
	case 9226:  // Spanish (Colombia)
	case 5130:  // Spanish (Costa Rica)
	case 7178:  // Spanish (Dominican Republic)
	case 12298:  // Spanish (Ecuador)
	case 17418:  // Spanish (El Salvador)
	case 4106:  // Spanish (Guatemala)
	case 18442:  // Spanish (Honduras)
	case 3082:  // Spanish (International Sort)
	case 13322:  // Spanish (Chile)
	case 19466:  // Spanish (Nicaragua)
	case 2058:  // Spanish (Mexico)
	case 10250:  // Spanish (Peru)
	case 20490:  // Spanish (Puerto Rico)
	case 1034:  // Spanish (Traditional Sort)
	case 14346:  // Spanish (Uruguay)
	case 8202:  // Spanish (Venezuela)
	case 1089:  // Swahili
	case 1053:  // Swedish
	case 2077:  // Swedish (Finland)
	case 5127:  // German (Liechtenstein)
	case 1078:  // Afrikaans
	case 6154:  // Spanish (Panama)
	case 4103:  // German (Luxembourg)
	case 16394:  // Spanish (Bolivia)
	case 2055:  // German (Switzerland)
	case 1039:  // Icelandic
	case 1057:  // Indonesian
	case 1040:  // Italian (Italy)
	case 2064:  // Italian (Switzerland)
	case 2068:  // Norwegian (Nynorsk)
	case 11274:  // Spanish (Argentina)
	case 1046:  // Portuguese (Brazil)
	case 1044:  // Norwegian (Bokmal)
	case 1086:  // Malay (Malaysia)
	case 2110:  // Malay (Brunei Darussalam)
	case 2070:  // Portuguese (Portugal)
		return 1252;
	case 1032:  // Greek
		return 1253;
	case 1091:  // Uzbek (Latin)
	case 1068:  // Azeri (Latin)
	case 1055:  // Turkish
		return 1254;
	case 1037:  // Hebrew
		return 1255;
	case 5121:  // Arabic (Algeria)
	case 15361:  // Arabic (Bahrain)
	case 9217:  // Arabic (Yemen)
	case 3073:  // Arabic (Egypt)
	case 2049:  // Arabic (Iraq)
	case 11265:  // Arabic (Jordan)
	case 13313:  // Arabic (Kuwait)
	case 12289:  // Arabic (Lebanon)
	case 4097:  // Arabic (Libya)
	case 6145:  // Arabic (Morocco)
	case 8193:  // Arabic (Oman)
	case 16385:  // Arabic (Qatar)
	case 1025:  // Arabic (Saudi Arabia)
	case 10241:  // Arabic (Syria)
	case 14337:  // Arabic (U.A.E.)
	case 1065:  // Farsi
	case 1056:  // Urdu
	case 7169:  // Arabic (Tunisia)
		return 1256;
	case 1061:  // Estonian
	case 1062:  // Latvian
	case 1063:  // Lithuanian
		return 1257;
	case 1066:  // Vietnamese
		return 1258;
	}
	return 65001;   // utf-8
}

#include <windows.h>
wchar_t GetCharFromKeyCode(irr::EKEY_CODE code)
{
	wchar_t ret = 0;

// 	BYTE state[256];
// 	HKL layout=GetKeyboardLayout(0);
//
// 	if(!GetKeyboardState(state))
// 		return 0;

	ret=(wchar_t)MapVirtualKey/*Ex*/(code, MAPVK_VK_TO_CHAR/*, layout*/);

	return ret;
}
// wchar_t GetCharFromKeyCode(irr::EKEY_CODE code)
// {
// 	wchar_t ret = 0;
//
// 	int scan;
// 	BYTE state[256];
// 	HKL layout=GetKeyboardLayout(0);
//
// 	if(!GetKeyboardState(state))
// 		return 0;
//
// 	scan=MapVirtualKeyEx(code, 0, layout);
// 	WORD keyChars[2];
// 	int conversionResult = ToAsciiEx((UINT)code, scan, state, keyChars, 0, layout);
// 	if (conversionResult == 1)
// 	{
// 		HKL KEYBOARD_INPUT_HKL = GetKeyboardLayout(0);
// 		unsigned int KEYBOARD_INPUT_CODEPAGE = LocaleIdToCodepage( LOWORD(KEYBOARD_INPUT_HKL) );
//
// 		WORD unicodeChar;
// 		MultiByteToWideChar(
// 			KEYBOARD_INPUT_CODEPAGE,
// 			MB_PRECOMPOSED, // default
// 			(LPCSTR)keyChars,
// 			sizeof(keyChars),
// 			(WCHAR*)&unicodeChar,
// 			1 );
// 		ret = unicodeChar;
// 	}
//
// 	return ret;
// }
#elif defined(_IRR_LINUX_PLATFORM_)
#include <X11/XKBlib.h>
#include <X11/keysym.h>
struct LocalKeyMap
{
	LocalKeyMap() {}
	LocalKeyMap(s32 x11, s32 win32)
		: X11Key(x11), Win32Key(win32)
	{
	}

	s32 X11Key;
	s32 Win32Key;

	bool operator<(const LocalKeyMap& o) const
	{
		return Win32Key<o.Win32Key;
	}
};

wchar_t GetCharFromKeyCode(irr::EKEY_CODE code)
{
	static core::array<LocalKeyMap> KeyMap;
	if(KeyMap.empty())
	{
		KeyMap.reallocate(84);
		KeyMap.push_back(LocalKeyMap(XK_BackSpace, KEY_BACK));
		KeyMap.push_back(LocalKeyMap(XK_Tab, KEY_TAB));
		KeyMap.push_back(LocalKeyMap(XK_ISO_Left_Tab, KEY_TAB));
		KeyMap.push_back(LocalKeyMap(XK_Linefeed, 0)); // ???
		KeyMap.push_back(LocalKeyMap(XK_Clear, KEY_CLEAR));
		KeyMap.push_back(LocalKeyMap(XK_Return, KEY_RETURN));
		KeyMap.push_back(LocalKeyMap(XK_Pause, KEY_PAUSE));
		KeyMap.push_back(LocalKeyMap(XK_Scroll_Lock, KEY_SCROLL));
		KeyMap.push_back(LocalKeyMap(XK_Sys_Req, 0)); // ???
		KeyMap.push_back(LocalKeyMap(XK_Escape, KEY_ESCAPE));
		KeyMap.push_back(LocalKeyMap(XK_Insert, KEY_INSERT));
		KeyMap.push_back(LocalKeyMap(XK_Delete, KEY_DELETE));
		KeyMap.push_back(LocalKeyMap(XK_Home, KEY_HOME));
		KeyMap.push_back(LocalKeyMap(XK_Left, KEY_LEFT));
		KeyMap.push_back(LocalKeyMap(XK_Up, KEY_UP));
		KeyMap.push_back(LocalKeyMap(XK_Right, KEY_RIGHT));
		KeyMap.push_back(LocalKeyMap(XK_Down, KEY_DOWN));
		KeyMap.push_back(LocalKeyMap(XK_Prior, KEY_PRIOR));
		KeyMap.push_back(LocalKeyMap(XK_Page_Up, KEY_PRIOR));
		KeyMap.push_back(LocalKeyMap(XK_Next, KEY_NEXT));
		KeyMap.push_back(LocalKeyMap(XK_Page_Down, KEY_NEXT));
		KeyMap.push_back(LocalKeyMap(XK_End, KEY_END));
		KeyMap.push_back(LocalKeyMap(XK_Begin, KEY_HOME));
		KeyMap.push_back(LocalKeyMap(XK_Num_Lock, KEY_NUMLOCK));
		KeyMap.push_back(LocalKeyMap(XK_KP_Space, KEY_SPACE));
		KeyMap.push_back(LocalKeyMap(XK_KP_Tab, KEY_TAB));
		KeyMap.push_back(LocalKeyMap(XK_KP_Enter, KEY_RETURN));
		KeyMap.push_back(LocalKeyMap(XK_KP_F1, KEY_F1));
		KeyMap.push_back(LocalKeyMap(XK_KP_F2, KEY_F2));
		KeyMap.push_back(LocalKeyMap(XK_KP_F3, KEY_F3));
		KeyMap.push_back(LocalKeyMap(XK_KP_F4, KEY_F4));
		KeyMap.push_back(LocalKeyMap(XK_KP_Home, KEY_HOME));
		KeyMap.push_back(LocalKeyMap(XK_KP_Left, KEY_LEFT));
		KeyMap.push_back(LocalKeyMap(XK_KP_Up, KEY_UP));
		KeyMap.push_back(LocalKeyMap(XK_KP_Right, KEY_RIGHT));
		KeyMap.push_back(LocalKeyMap(XK_KP_Down, KEY_DOWN));
		KeyMap.push_back(LocalKeyMap(XK_Print, KEY_PRINT));
		KeyMap.push_back(LocalKeyMap(XK_KP_Prior, KEY_PRIOR));
		KeyMap.push_back(LocalKeyMap(XK_KP_Page_Up, KEY_PRIOR));
		KeyMap.push_back(LocalKeyMap(XK_KP_Next, KEY_NEXT));
		KeyMap.push_back(LocalKeyMap(XK_KP_Page_Down, KEY_NEXT));
		KeyMap.push_back(LocalKeyMap(XK_KP_End, KEY_END));
		KeyMap.push_back(LocalKeyMap(XK_KP_Begin, KEY_HOME));
		KeyMap.push_back(LocalKeyMap(XK_KP_Insert, KEY_INSERT));
		KeyMap.push_back(LocalKeyMap(XK_KP_Delete, KEY_DELETE));
		KeyMap.push_back(LocalKeyMap(XK_KP_Equal, 0)); // ???
		KeyMap.push_back(LocalKeyMap(XK_KP_Multiply, KEY_MULTIPLY));
		KeyMap.push_back(LocalKeyMap(XK_KP_Add, KEY_ADD));
		KeyMap.push_back(LocalKeyMap(XK_KP_Separator, KEY_SEPARATOR));
		KeyMap.push_back(LocalKeyMap(XK_KP_Subtract, KEY_SUBTRACT));
		KeyMap.push_back(LocalKeyMap(XK_KP_Decimal, KEY_DECIMAL));
		KeyMap.push_back(LocalKeyMap(XK_KP_Divide, KEY_DIVIDE));
		KeyMap.push_back(LocalKeyMap(XK_KP_0, KEY_KEY_0));
		KeyMap.push_back(LocalKeyMap(XK_KP_1, KEY_KEY_1));
		KeyMap.push_back(LocalKeyMap(XK_KP_2, KEY_KEY_2));
		KeyMap.push_back(LocalKeyMap(XK_KP_3, KEY_KEY_3));
		KeyMap.push_back(LocalKeyMap(XK_KP_4, KEY_KEY_4));
		KeyMap.push_back(LocalKeyMap(XK_KP_5, KEY_KEY_5));
		KeyMap.push_back(LocalKeyMap(XK_KP_6, KEY_KEY_6));
		KeyMap.push_back(LocalKeyMap(XK_KP_7, KEY_KEY_7));
		KeyMap.push_back(LocalKeyMap(XK_KP_8, KEY_KEY_8));
		KeyMap.push_back(LocalKeyMap(XK_KP_9, KEY_KEY_9));
		KeyMap.push_back(LocalKeyMap(XK_F1, KEY_F1));
		KeyMap.push_back(LocalKeyMap(XK_F2, KEY_F2));
		KeyMap.push_back(LocalKeyMap(XK_F3, KEY_F3));
		KeyMap.push_back(LocalKeyMap(XK_F4, KEY_F4));
		KeyMap.push_back(LocalKeyMap(XK_F5, KEY_F5));
		KeyMap.push_back(LocalKeyMap(XK_F6, KEY_F6));
		KeyMap.push_back(LocalKeyMap(XK_F7, KEY_F7));
		KeyMap.push_back(LocalKeyMap(XK_F8, KEY_F8));
		KeyMap.push_back(LocalKeyMap(XK_F9, KEY_F9));
		KeyMap.push_back(LocalKeyMap(XK_F10, KEY_F10));
		KeyMap.push_back(LocalKeyMap(XK_F11, KEY_F11));
		KeyMap.push_back(LocalKeyMap(XK_F12, KEY_F12));
		KeyMap.push_back(LocalKeyMap(XK_Shift_L, KEY_LSHIFT));
		KeyMap.push_back(LocalKeyMap(XK_Shift_R, KEY_RSHIFT));
		KeyMap.push_back(LocalKeyMap(XK_Control_L, KEY_LCONTROL));
		KeyMap.push_back(LocalKeyMap(XK_Control_R, KEY_RCONTROL));
		KeyMap.push_back(LocalKeyMap(XK_Caps_Lock, KEY_CAPITAL));
		KeyMap.push_back(LocalKeyMap(XK_Shift_Lock, KEY_CAPITAL));
		KeyMap.push_back(LocalKeyMap(XK_Meta_L, KEY_LWIN));
		KeyMap.push_back(LocalKeyMap(XK_Meta_R, KEY_RWIN));
		KeyMap.push_back(LocalKeyMap(XK_Alt_L, KEY_LMENU));
		KeyMap.push_back(LocalKeyMap(XK_Alt_R, KEY_RMENU));
		KeyMap.push_back(LocalKeyMap(XK_ISO_Level3_Shift, KEY_RMENU));
		KeyMap.push_back(LocalKeyMap(XK_Menu, KEY_MENU));
		KeyMap.push_back(LocalKeyMap(XK_space, KEY_SPACE));
		KeyMap.push_back(LocalKeyMap(XK_exclam, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_quotedbl, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_section, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_numbersign, KEY_OEM_2));
		KeyMap.push_back(LocalKeyMap(XK_dollar, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_percent, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_ampersand, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_apostrophe, KEY_OEM_7));
		KeyMap.push_back(LocalKeyMap(XK_parenleft, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_parenright, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_asterisk, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_plus, KEY_PLUS)); //?
		KeyMap.push_back(LocalKeyMap(XK_comma, KEY_COMMA)); //?
		KeyMap.push_back(LocalKeyMap(XK_minus, KEY_MINUS)); //?
		KeyMap.push_back(LocalKeyMap(XK_period, KEY_PERIOD)); //?
		KeyMap.push_back(LocalKeyMap(XK_slash, KEY_OEM_2)); //?
		KeyMap.push_back(LocalKeyMap(XK_0, KEY_KEY_0));
		KeyMap.push_back(LocalKeyMap(XK_1, KEY_KEY_1));
		KeyMap.push_back(LocalKeyMap(XK_2, KEY_KEY_2));
		KeyMap.push_back(LocalKeyMap(XK_3, KEY_KEY_3));
		KeyMap.push_back(LocalKeyMap(XK_4, KEY_KEY_4));
		KeyMap.push_back(LocalKeyMap(XK_5, KEY_KEY_5));
		KeyMap.push_back(LocalKeyMap(XK_6, KEY_KEY_6));
		KeyMap.push_back(LocalKeyMap(XK_7, KEY_KEY_7));
		KeyMap.push_back(LocalKeyMap(XK_8, KEY_KEY_8));
		KeyMap.push_back(LocalKeyMap(XK_9, KEY_KEY_9));
		KeyMap.push_back(LocalKeyMap(XK_colon, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_semicolon, KEY_OEM_1));
		KeyMap.push_back(LocalKeyMap(XK_less, KEY_OEM_102));
		KeyMap.push_back(LocalKeyMap(XK_equal, KEY_PLUS));
		KeyMap.push_back(LocalKeyMap(XK_greater, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_question, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_at, KEY_KEY_2)); //?
		KeyMap.push_back(LocalKeyMap(XK_mu, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_EuroSign, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_A, KEY_KEY_A));
		KeyMap.push_back(LocalKeyMap(XK_B, KEY_KEY_B));
		KeyMap.push_back(LocalKeyMap(XK_C, KEY_KEY_C));
		KeyMap.push_back(LocalKeyMap(XK_D, KEY_KEY_D));
		KeyMap.push_back(LocalKeyMap(XK_E, KEY_KEY_E));
		KeyMap.push_back(LocalKeyMap(XK_F, KEY_KEY_F));
		KeyMap.push_back(LocalKeyMap(XK_G, KEY_KEY_G));
		KeyMap.push_back(LocalKeyMap(XK_H, KEY_KEY_H));
		KeyMap.push_back(LocalKeyMap(XK_I, KEY_KEY_I));
		KeyMap.push_back(LocalKeyMap(XK_J, KEY_KEY_J));
		KeyMap.push_back(LocalKeyMap(XK_K, KEY_KEY_K));
		KeyMap.push_back(LocalKeyMap(XK_L, KEY_KEY_L));
		KeyMap.push_back(LocalKeyMap(XK_M, KEY_KEY_M));
		KeyMap.push_back(LocalKeyMap(XK_N, KEY_KEY_N));
		KeyMap.push_back(LocalKeyMap(XK_O, KEY_KEY_O));
		KeyMap.push_back(LocalKeyMap(XK_P, KEY_KEY_P));
		KeyMap.push_back(LocalKeyMap(XK_Q, KEY_KEY_Q));
		KeyMap.push_back(LocalKeyMap(XK_R, KEY_KEY_R));
		KeyMap.push_back(LocalKeyMap(XK_S, KEY_KEY_S));
		KeyMap.push_back(LocalKeyMap(XK_T, KEY_KEY_T));
		KeyMap.push_back(LocalKeyMap(XK_U, KEY_KEY_U));
		KeyMap.push_back(LocalKeyMap(XK_V, KEY_KEY_V));
		KeyMap.push_back(LocalKeyMap(XK_W, KEY_KEY_W));
		KeyMap.push_back(LocalKeyMap(XK_X, KEY_KEY_X));
		KeyMap.push_back(LocalKeyMap(XK_Y, KEY_KEY_Y));
		KeyMap.push_back(LocalKeyMap(XK_Z, KEY_KEY_Z));
		KeyMap.push_back(LocalKeyMap(XK_bracketleft, KEY_OEM_4));
		KeyMap.push_back(LocalKeyMap(XK_backslash, KEY_OEM_5));
		KeyMap.push_back(LocalKeyMap(XK_bracketright, KEY_OEM_6));
		KeyMap.push_back(LocalKeyMap(XK_asciicircum, KEY_OEM_5));
		KeyMap.push_back(LocalKeyMap(XK_degree, 0)); //?
		KeyMap.push_back(LocalKeyMap(XK_underscore, KEY_MINUS)); //?
		KeyMap.push_back(LocalKeyMap(XK_grave, KEY_OEM_3));
		KeyMap.push_back(LocalKeyMap(XK_acute, KEY_OEM_6));
		KeyMap.push_back(LocalKeyMap(XK_a, KEY_KEY_A));
		KeyMap.push_back(LocalKeyMap(XK_b, KEY_KEY_B));
		KeyMap.push_back(LocalKeyMap(XK_c, KEY_KEY_C));
		KeyMap.push_back(LocalKeyMap(XK_d, KEY_KEY_D));
		KeyMap.push_back(LocalKeyMap(XK_e, KEY_KEY_E));
		KeyMap.push_back(LocalKeyMap(XK_f, KEY_KEY_F));
		KeyMap.push_back(LocalKeyMap(XK_g, KEY_KEY_G));
		KeyMap.push_back(LocalKeyMap(XK_h, KEY_KEY_H));
		KeyMap.push_back(LocalKeyMap(XK_i, KEY_KEY_I));
		KeyMap.push_back(LocalKeyMap(XK_j, KEY_KEY_J));
		KeyMap.push_back(LocalKeyMap(XK_k, KEY_KEY_K));
		KeyMap.push_back(LocalKeyMap(XK_l, KEY_KEY_L));
		KeyMap.push_back(LocalKeyMap(XK_m, KEY_KEY_M));
		KeyMap.push_back(LocalKeyMap(XK_n, KEY_KEY_N));
		KeyMap.push_back(LocalKeyMap(XK_o, KEY_KEY_O));
		KeyMap.push_back(LocalKeyMap(XK_p, KEY_KEY_P));
		KeyMap.push_back(LocalKeyMap(XK_q, KEY_KEY_Q));
		KeyMap.push_back(LocalKeyMap(XK_r, KEY_KEY_R));
		KeyMap.push_back(LocalKeyMap(XK_s, KEY_KEY_S));
		KeyMap.push_back(LocalKeyMap(XK_t, KEY_KEY_T));
		KeyMap.push_back(LocalKeyMap(XK_u, KEY_KEY_U));
		KeyMap.push_back(LocalKeyMap(XK_v, KEY_KEY_V));
		KeyMap.push_back(LocalKeyMap(XK_w, KEY_KEY_W));
		KeyMap.push_back(LocalKeyMap(XK_x, KEY_KEY_X));
		KeyMap.push_back(LocalKeyMap(XK_y, KEY_KEY_Y));
		KeyMap.push_back(LocalKeyMap(XK_z, KEY_KEY_Z));
		KeyMap.push_back(LocalKeyMap(XK_ssharp, KEY_OEM_4));
		KeyMap.push_back(LocalKeyMap(XK_adiaeresis, KEY_OEM_7));
		KeyMap.push_back(LocalKeyMap(XK_odiaeresis, KEY_OEM_3));
		KeyMap.push_back(LocalKeyMap(XK_udiaeresis, KEY_OEM_1));
		KeyMap.push_back(LocalKeyMap(XK_Super_L, KEY_LWIN));
		KeyMap.push_back(LocalKeyMap(XK_Super_R, KEY_RWIN));

		KeyMap.sort();
	}

	wchar_t ret = 0;

	LocalKeyMap mp(0, (s32)code);
	s32 X11Key = 0;
	char* buf;

	const s32 idx = KeyMap.binary_search(mp);
	if (idx != -1)
	{
		X11Key = KeyMap[idx].X11Key;
	}
	else
	{
		X11Key = 0;
	}
	if(X11Key != 0)
		buf = XKeysymToString(X11Key);
	ret = (wchar_t)buf[0];

	return ret;
}
#endif

static const wchar_t* const s_KeyStringsFromKeyCode[KEY_KEY_CODES_COUNT] =
{
	L"",
	L"LMOUSE", //           = 0x01,  // Left mouse button
	L"RMOUSE", //           = 0x02,  // Right mouse button
	L"CANCEL", //           = 0x03,  // Control-break processing
	L"MMOUSE", //           = 0x04,  // Middle mouse button (three-button mouse)
	L"XBUTTON1", //         = 0x05,  // Windows 2000/XP: X1 mouse button
	L"XBUTTON2", //         = 0x06,  // Windows 2000/XP: X2 mouse button
	L"",
	L"BACK", //             = 0x08,  // BACKSPACE key
	L"TAB", //              = 0x09,  // TAB key
	L"", L"",
	L"CLEAR", //            = 0x0C,  // CLEAR key
	L"ENTER", //            = 0x0D,  // ENTER key
	L"", L"",
	L"SHIFT", //            = 0x10,  // SHIFT key
	L"CTRL", //          = 0x11,  // CTRL key
	L"ALT", //              = 0x12,  // ALT key
	L"PAUSE", //            = 0x13,  // PAUSE key
	L"CAPS", //             = 0x14,  // CAPS LOCK key
	L"KANA", //             = 0x15,  // IME Kana mode
	L"",
	L"JUNJA", //            = 0x17,  // IME Junja mode
	L"FINAL", //            = 0x18,  // IME final mode
	L"HANJA", //            = 0x19,  // IME Hanja mode
	L"",
	L"ESC", //           = 0x1B,  // ESC key
	L"CONVERT", //          = 0x1C,  // IME convert
	L"NONCONVERT", //       = 0x1D,  // IME nonconvert
	L"ACCEPT", //           = 0x1E,  // IME accept
	L"MODECHANGE", //       = 0x1F,  // IME mode change request
	L"SPACE", //            = 0x20,  // SPACEBAR
	L"PRIOR", //            = 0x21,  // PAGE UP key
	L"NEXT", //             = 0x22,  // PAGE DOWN key
	L"END", //              = 0x23,  // END key
	L"HOME", //             = 0x24,  // HOME key
	L"LEFT", //             = 0x25,  // LEFT ARROW key
	L"UP", //               = 0x26,  // UP ARROW key
	L"RIGHT", //            = 0x27,  // RIGHT ARROW key
	L"DOWN", //             = 0x28,  // DOWN ARROW key
	L"SELECT", //           = 0x29,  // SELECT key
	L"PRINT", //            = 0x2A,  // PRINT key
	L"EXECUT", //           = 0x2B,  // EXECUTE key
	L"SNAPSHOT", //         = 0x2C,  // PRINT SCREEN key
	L"INS", //           = 0x2D,  // INS key
	L"DEL", //           = 0x2E,  // DEL key
	L"HELP", //             = 0x2F,  // HELP key
	L"KEY_0", //            = 0x30,  // 0 key
	L"KEY_1", //            = 0x31,  // 1 key
	L"KEY_2", //            = 0x32,  // 2 key
	L"KEY_3", //            = 0x33,  // 3 key
	L"KEY_4", //            = 0x34,  // 4 key
	L"KEY_5", //            = 0x35,  // 5 key
	L"KEY_6", //            = 0x36,  // 6 key
	L"KEY_7", //            = 0x37,  // 7 key
	L"KEY_8", //            = 0x38,  // 8 key
	L"KEY_9", //            = 0x39,  // 9 key
	L"", L"", L"", L"", L"", L"", L"",
	L"KEY_A", //            = 0x41,  // A key
	L"KEY_B", //            = 0x42,  // B key
	L"KEY_C", //            = 0x43,  // C key
	L"KEY_D", //            = 0x44,  // D key
	L"KEY_E", //            = 0x45,  // E key
	L"KEY_F", //            = 0x46,  // F key
	L"KEY_G", //            = 0x47,  // G key
	L"KEY_H", //            = 0x48,  // H key
	L"KEY_I", //            = 0x49,  // I key
	L"KEY_J", //            = 0x4A,  // J key
	L"KEY_K", //            = 0x4B,  // K key
	L"KEY_L", //            = 0x4C,  // L key
	L"KEY_M", //            = 0x4D,  // M key
	L"KEY_N", //            = 0x4E,  // N key
	L"KEY_O", //            = 0x4F,  // O key
	L"KEY_P", //            = 0x50,  // P key
	L"KEY_Q", //            = 0x51,  // Q key
	L"KEY_R", //            = 0x52,  // R key
	L"KEY_S", //            = 0x53,  // S key
	L"KEY_T", //            = 0x54,  // T key
	L"KEY_U", //            = 0x55,  // U key
	L"KEY_V", //            = 0x56,  // V key
	L"KEY_W", //            = 0x57,  // W key
	L"KEY_X", //            = 0x58,  // X key
	L"KEY_Y", //            = 0x59,  // Y key
	L"KEY_Z", //            = 0x5A,  // Z key
	L"LWIN", //             = 0x5B,  // Left Windows key (Microsoft� Natural� keyboard)
	L"RWIN", //             = 0x5C,  // Right Windows key (Natural keyboard)
	L"APPS", //             = 0x5D,  // Applications key (Natural keyboard)
	L"",
	L"SLEEP", //            = 0x5F,  // Computer Sleep key
	L"NUMPAD0", //          = 0x60,  // Numeric keypad 0 key
	L"NUMPAD1", //          = 0x61,  // Numeric keypad 1 key
	L"NUMPAD2", //          = 0x62,  // Numeric keypad 2 key
	L"NUMPAD3", //          = 0x63,  // Numeric keypad 3 key
	L"NUMPAD4", //          = 0x64,  // Numeric keypad 4 key
	L"NUMPAD5", //          = 0x65,  // Numeric keypad 5 key
	L"NUMPAD6", //          = 0x66,  // Numeric keypad 6 key
	L"NUMPAD7", //          = 0x67,  // Numeric keypad 7 key
	L"NUMPAD8", //          = 0x68,  // Numeric keypad 8 key
	L"NUMPAD9", //          = 0x69,  // Numeric keypad 9 key
	L"MULTIPLY", //         = 0x6A,  // Multiply key
	L"ADD", //              = 0x6B,  // Add key
	L"SEPARATOR", //        = 0x6C,  // Separator key
	L"SUBTRACT", //         = 0x6D,  // Subtract key
	L"DECIMAL", //          = 0x6E,  // Decimal key
	L"DIVIDE", //           = 0x6F,  // Divide key
	L"F1", //               = 0x70,  // F1 key
	L"F2", //               = 0x71,  // F2 key
	L"F3", //               = 0x72,  // F3 key
	L"F4", //               = 0x73,  // F4 key
	L"F5", //               = 0x74,  // F5 key
	L"F6", //               = 0x75,  // F6 key
	L"F7", //               = 0x76,  // F7 key
	L"F8", //               = 0x77,  // F8 key
	L"F9", //               = 0x78,  // F9 key
	L"F10", //              = 0x79,  // F10 key
	L"F11", //              = 0x7A,  // F11 key
	L"F12", //              = 0x7B,  // F12 key
	L"F13", //              = 0x7C,  // F13 key
	L"F14", //              = 0x7D,  // F14 key
	L"F15", //              = 0x7E,  // F15 key
	L"F16", //              = 0x7F,  // F16 key
	L"F17", //              = 0x80,  // F17 key
	L"F18", //              = 0x81,  // F18 key
	L"F19", //              = 0x82,  // F19 key
	L"F20", //              = 0x83,  // F20 key
	L"F21", //              = 0x84,  // F21 key
	L"F22", //              = 0x85,  // F22 key
	L"F23", //              = 0x86,  // F23 key
	L"F24", //              = 0x87,  // F24 key
	L"", L"", L"", L"", L"", L"", L"", L"",
	L"NUMLOCK", //          = 0x90,  // NUM LOCK key
	L"SCROLL", //           = 0x91,  // SCROLL LOCK key
	L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"",
	L"LSHIFT", //           = 0xA0,  // Left SHIFT key
	L"RSHIFT", //           = 0xA1,  // Right SHIFT key
	L"LCTRL", //         = 0xA2,  // Left CONTROL key
	L"RCTRL", //         = 0xA3,  // Right CONTROL key
	L"LALT", //             = 0xA4,  // Left MENU key
	L"RALT", //             = 0xA5,  // Right MENU key
	L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"",
	L"OEM_1", //            = 0xBA,  // for US    ";:"
	L"PLUS", //             = 0xBB,  // Plus Key   "+"
	L"COMMA", //            = 0xBC,  // Comma Key  ","
	L"MINUS", //            = 0xBD,  // Minus Key  "-"
	L"PERIOD", //           = 0xBE,  // Period Key "."
	L"OEM_2", //            = 0xBF,  // for US    "/?"
	L"OEM_3", //            = 0xC0,  // for US    "`~"
	L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"",
	L"OEM_4", //            = 0xDB,  // for US    "[{"
	L"OEM_5", //            = 0xDC,  // for US    "\|"
	L"OEM_6", //            = 0xDD,  // for US    "]}"
	L"OEM_7", //            = 0xDE,  // for US    "'""
	L"OEM_8", //            = 0xDF,  // None
	L"",
	L"OEM_AX", //           = 0xE1,  // for Japan "AX"
	L"OEM_102", //          = 0xE2,  // "<>" or "\|"
	L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"", L"",
	L"ATTN", //             = 0xF6,  // Attn key
	L"CRSEL", //            = 0xF7,  // CrSel key
	L"EXSEL", //            = 0xF8,  // ExSel key
	L"EREOF", //            = 0xF9,  // Erase EOF key
	L"PLAY", //             = 0xFA,  // Play key
	L"ZOOM", //             = 0xFB,  // Zoom key
	L"",
	L"PA1", //              = 0xFD,  // PA1 key
	L"OEM_CLEAR" //         = 0xFE,   // Clear key
};

core::stringw GetStringFromKeyCode(EKEY_CODE code)
{
	core::stringw ret = L"";

	switch(code)
	{
	case KEY_ESCAPE:
	case KEY_RETURN:
	case KEY_TAB:
	case KEY_DELETE:
	case KEY_BACK:
	case KEY_SPACE:
	case KEY_LBUTTON:
	case KEY_RBUTTON:
		//case KEY_MBUTTON:
	case KEY_MENU:
	case KEY_LMENU:
	case KEY_RMENU:
	case KEY_SHIFT:
	case KEY_LSHIFT:
	case KEY_RSHIFT:
	case KEY_CONTROL:
	case KEY_LCONTROL:
	case KEY_RCONTROL:
	case KEY_LEFT:
	case KEY_RIGHT:
	case KEY_UP:
	case KEY_DOWN:
		ret = s_KeyStringsFromKeyCode[code];
		break;
	default:
		ret += GetCharFromKeyCode(code);
		ret = ret.make_upper();
		break;
	}

	return ret;
}

bool islinereturn(wchar_t c) { return c == L'\n' || c == L'\r'; }
bool isspace(wchar_t c) { return c == L' ' || c == L'\f' || c == L'\t' || c == L'\v'; }



// Glossary

bool Get2ndPart(const core::stringw& line, u32 start, u32 end, core::stringw& outPart2)
{
	core::stringw content = line.subString(start, end-start);
	s32 postEqualStart = content.findFirstChar(L"=");
	if(postEqualStart >= 0)
	{
		while(isspace(content[++postEqualStart]));
		outPart2 = content.subString(postEqualStart, content.size() - postEqualStart);
		return true;
	}
	return false; // empty keyword
}


