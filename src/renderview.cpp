﻿#include "renderview.h"
#include "engine.h"
#include "audio.h"
#include "utils.h"

// Define some values that we'll use to identify individual GUI controls.
#define GUI_ID_CONTEXT_MENU 101

 
// RenderWindow
RenderWindow::RenderWindow(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position) 
	: gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), 
	m_Gui(env), 
	m_ContextualMenu(NULL), 
	m_CurrentlyHovered(false),
	m_SceneInterface(NULL),
	m_CurrentDlgTime(0.0f),
	m_IsPlaying(false)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();
	m_ViewerBackground = m_Gui->addImage(Rectangle2(Position2(0, VIEW_SCENE_EDITOR_TOOLBARS_SIZE), Dimension2(this->getRelativePosition().getWidth() - VIEW_SCROLL_BAR_THICKNESS, this->getRelativePosition().getHeight() - VIEW_SCENE_EDITOR_TOOLBARS_SIZE - VIEW_SCROLL_BAR_THICKNESS)), this);
	m_ViewerBackground->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	gui::IGUIStaticText* staticText = NULL;

	// Toolbar line 1
	staticText = m_Gui->addStaticText(L"Taille du rendu", Rectangle2(Position2(0, 0), Dimension2(128, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderWidthSpinBox = m_Gui->addSpinBox(L"Width", Rectangle2(Position2(128, 0), Dimension2(64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, this);
	m_RenderWidthSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderWidthSpinBox->setDecimalPlaces(0);
	m_RenderWidthSpinBox->setRange(0.0f, 1920.0f);
	m_RenderWidthSpinBox->setValue((f32)Engine::GetInstance()->m_RenderSize.Width);
	staticText = m_Gui->addStaticText(L" /", Rectangle2(Position2(128 + 64, 0), Dimension2(16, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderHeightSpinBox = m_Gui->addSpinBox(L"Height", Rectangle2(Position2(128 + 64 + 16, 0), Dimension2(64, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, this);
	m_RenderHeightSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderHeightSpinBox->setDecimalPlaces(0);
	m_RenderHeightSpinBox->setRange(0.0f, 1080.0f);
	m_RenderHeightSpinBox->setValue((f32)Engine::GetInstance()->m_RenderSize.Height);
	staticText = m_Gui->addStaticText(L"Langue", Rectangle2(Position2(128 + 64 + 16 + 64, 0), Dimension2(128, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LanguageTypeComboBox = m_Gui->addComboBox(Rectangle2(Position2(128 + 64 + 16 + 64 + 64, 0), Dimension2(128, VIEW_SCENE_EDITOR_TOOLBARS_SIZE)), this);
	m_LanguageTypeComboBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	for(u32 i = 0; i < Engine::LS_MAX; ++i)
		m_LanguageTypeComboBox->addItem(engine->ms_LanguageSettingsString[i]);

	Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());
	Dimension2 rendertargetDim(core::min_(Engine::GetInstance()->m_RenderSize.Width, dim.Width), core::min_(Engine::GetInstance()->m_RenderSize.Height, dim.Height));
	m_lastSize = rendertargetDim;

	if (driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		if(rendertargetDim.Width > 0 && rendertargetDim.Height > 0)
			m_RenderTarget = driver->addRenderTargetTexture(rendertargetDim, "RTT_RenderWindow", video::ECF_A8R8G8B8);
		else
			m_RenderTarget = NULL;
		//m_Viewer = m_Gui->addImage(m_RenderTarget, Position2(0, 0), false, m_ViewerBackground);
		m_Viewer = m_Gui->addImage(Rectangle2(Position2(0, 0), dim), m_ViewerBackground);
		m_Viewer->setImage(m_RenderTarget);
	}
	else
	{
		m_RenderTarget = NULL;
		m_Viewer = m_Gui->addImage(Rectangle2(Position2(0, 0), dim), m_ViewerBackground);
	}
	m_Viewer->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	Position2 basePos = m_ViewerBackground->getRelativePosition().UpperLeftCorner;
	m_ViewerWScrollBar = m_Gui->addScrollBar(true, Rectangle2(basePos + Position2(0, dim.Height), Dimension2(dim.Width, VIEW_SCROLL_BAR_THICKNESS)), this);
	m_ViewerWScrollBar->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT);
	m_ViewerWScrollBar->setSmallStep(32);
	m_ViewerWScrollBar->setLargeStep(256);
	m_ViewerHScrollBar = m_Gui->addScrollBar(false, Rectangle2(basePos + Position2(dim.Width, 0), Dimension2(VIEW_SCROLL_BAR_THICKNESS, dim.Height+VIEW_SCROLL_BAR_THICKNESS)), this);
	m_ViewerHScrollBar->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_ViewerHScrollBar->setSmallStep(32);
	m_ViewerHScrollBar->setLargeStep(256);

	m_ViewerWScrollBar->setMax((s32)(Engine::GetInstance()->m_RenderSize.Width) - dim.Width);
	m_ViewerHScrollBar->setMax((s32)(Engine::GetInstance()->m_RenderSize.Height) - dim.Height);

	m_ViewerPosition = Position2(0, 0);

	ResetSceneInterface();

	OnSceneChange();
}

RenderWindow::~RenderWindow()
{
	SafeDelete(m_SceneInterface);
}

void RenderWindow::ResetSceneInterface()
{
	SafeDelete(m_SceneInterface);

	m_SceneInterface = new SceneInterface();

	m_IsPlaying = false;
}

void RenderWindow::LoadScene()
{
	if(!m_OpenFileDlg || !m_SceneInterface)
		return;

	ResetSceneInterface();

	m_SceneInterface->LoadScene(io::path(m_OpenFileDlg->getFileName()).c_str());

	m_IsPlaying = true;

	Scene* scene = m_SceneInterface->GetScene();
}

void RenderWindow::OnSceneChange()
{

}

bool RenderWindow::OnEvent(const SEvent& event)
{
	// 	SEvent evt = event;
	// 	printf("SpriteBankListBox\n");
	EKEY_CODE key = (EKEY_CODE)0x0;
	bool isPressed = false;

	// convert mouse events into key events
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(m_CurrentlyHovered && !m_IsPlaying)
		{
			if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
			{
				Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
				gui::IGUIContextMenu* menu = NULL;
				{// scene context menu
					menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
					menu->addItem(L"Charger", 666, true);
				}

				if(menu)
				{
					Engine* engine = Engine::GetInstance();
					video::IVideoDriver* driver = engine->GetDriver();

					Dimension2 dim = driver->getScreenSize();
					r = menu->getRelativePosition();
					r.constrainTo(Rectangle2(Position2(0, 0), dim));
					menu->setRelativePosition(r);

					menu->setEventParent(this);
				}
			}
		}
		else if(m_IsPlaying)
		{
			if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
			{
				key = KEY_LBUTTON;
				isPressed = true;
			}
			else if(event.MouseInput.Event == EMIE_LMOUSE_LEFT_UP)
			{
				key = KEY_LBUTTON;
				isPressed = false;
			}
			else if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
			{
				key = KEY_RBUTTON;
				isPressed = true;
			}
			else if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
			{
				key = KEY_RBUTTON;
				isPressed = false;
			}
			else if(event.MouseInput.Event == EMIE_MMOUSE_PRESSED_DOWN)
			{
				key = KEY_MBUTTON;
				isPressed = true;
			}
			else if(event.MouseInput.Event == EMIE_MMOUSE_LEFT_UP)
			{
				key = KEY_MBUTTON;
				isPressed = false;
			}
			else if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
			{
				// override scene dialog choice control with mouse controls
				ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
				EKEY_CODE keyValid = keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE1);
				if(keyValid == KEY_LBUTTON || keyValid == KEY_RBUTTON || keyValid == KEY_MBUTTON)
				{
					for(u32 i = 0; i < m_DialogChoiceAreas.size(); ++i)
					{
						Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
						mousePos -= m_Viewer->getAbsolutePosition().UpperLeftCorner;
						if(m_DialogChoiceAreas[i].isPointInside(mousePos))
						{
							m_SceneInterface->GetScene()->SetCurrentDialogChoice(i);
						}
					}
				}
			}
		}
	}

	if(key != 0x0 || event.EventType == EET_KEY_INPUT_EVENT)
	{
		ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
		if(key == 0x0)
		{
			isPressed = event.KeyInput.PressedDown;
			key = event.KeyInput.Key;
		}

		Scene* scene = m_SceneInterface? m_SceneInterface->GetScene() : NULL;
		if(scene && m_IsPlaying)
		{
			if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_UP, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_DOWN, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_LEFT, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_RIGHT, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION1, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION2, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE3))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION3, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE4))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION4, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE5))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION5, isPressed);
			}
			else if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_QTE6))
			{
				scene->GetCurrentInputsPressed().SetPressed(InputData::PI_ACTION6, isPressed);
			}
		}

		if(key == keymap.GetKeyCode1(ProfileData::KeyMappingSerializable::II_KEY_PAUSE))
		{
			if(isPressed)
				m_IsPlaying = !m_IsPlaying;
		}
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		DialogNode* currentDlg = m_SceneInterface->GetCurrentDialog();
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}
		else if(event.GUIEvent.EventType == gui::EGET_SCROLL_BAR_CHANGED)
		{
			if(event.GUIEvent.Caller == m_ViewerHScrollBar)
			{
				m_ViewerPosition.Y = m_ViewerHScrollBar->getPos();
			}
			else if(event.GUIEvent.Caller == m_ViewerWScrollBar)
			{
				m_ViewerPosition.X = m_ViewerWScrollBar->getPos();
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_SPINBOX_CHANGED)
		{
			bool renderChanged = false;
			if(event.GUIEvent.Caller == m_RenderWidthSpinBox)
			{
				u32 width = (u32)m_RenderWidthSpinBox->getValue();
				Engine::GetInstance()->m_RenderSize.Width = width;
				renderChanged = true;
			}
			else if(event.GUIEvent.Caller == m_RenderHeightSpinBox)
			{
				u32 height = (u32)m_RenderHeightSpinBox->getValue();
				Engine::GetInstance()->m_RenderSize.Height = height;
				renderChanged = true;
			}

			if(renderChanged)
			{
				ProfileData::GetInstance()->m_ODS.m_RenderSize = Engine::GetInstance()->m_RenderSize;
				ProfileData::GetInstance()->WriteSave();

				Engine::GetInstance()->RecomputeSize();
				ChangeRenderTargetSize();
			}
		}
		else if(event.GUIEvent.EventType == gui::EGET_COMBO_BOX_CHANGED)
		{
			if(event.GUIEvent.Caller == m_LanguageTypeComboBox)
			{
				s32 type = m_LanguageTypeComboBox->getSelected();
				assert(type>=0 && type<(s32)Engine::LS_MAX);
				Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)type);
			}
		}
		
		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// Load
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un fichier à charger");
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void RenderWindow::draw()
{
	gui::IGUIElement::draw();
}

void RenderWindow::ChangeRenderTargetSize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());

	// 			if(m_RenderTarget)
	// 				driver->removeTexture(m_RenderTarget);
	Dimension2 rendertargetDim(core::min_(Engine::GetInstance()->m_RenderSize.Width, dim.Width), core::min_(Engine::GetInstance()->m_RenderSize.Height, dim.Height));
	if(rendertargetDim.Width > 0 && rendertargetDim.Height > 0)
		m_RenderTarget = driver->addRenderTargetTexture(rendertargetDim, "RTT_RenderWindow", video::ECF_A8R8G8B8);
	else
		m_RenderTarget = NULL;
	m_Viewer->setImage(m_RenderTarget);

	s32 maxX = Engine::GetInstance()->m_RenderSize.Width - dim.Width;
	s32 maxY = Engine::GetInstance()->m_RenderSize.Height - dim.Height;
	m_ViewerWScrollBar->setMax(maxX);
	m_ViewerHScrollBar->setMax(maxY);
	m_ViewerWScrollBar->setMin(0);
	m_ViewerHScrollBar->setMin(0);

	m_ViewerPosition.X = core::max_(core::min_(maxX, m_ViewerPosition.X), 0);
	m_ViewerPosition.Y = core::max_(core::min_(maxY, m_ViewerPosition.Y), 0);
	m_ViewerWScrollBar->setPos(m_ViewerPosition.X);
	m_ViewerHScrollBar->setPos(m_ViewerPosition.Y);
}

void RenderWindow::UpdateRenderTarget(float dt)
{
	Engine* engine = Engine::GetInstance();
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	Dimension2 renderSize((u32)((f32)originalSize.Width*innerScale), (u32)((f32)originalSize.Height*innerScale));
	const Position2& innerOffset = engine->m_InnerOffset;

	if (m_RenderTarget)
	{
		video::IVideoDriver* driver = engine->GetDriver();
		// draw scene into render target

		// handle viewer size change
		Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());
		if(dim != m_lastSize)
		{
			ChangeRenderTargetSize();
			
			m_lastSize = dim;
		}

		// set render target texture
		driver->setRenderTarget(m_RenderTarget, true, true, COLOR_TRANSPARENT);
		
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		if(m_SceneInterface)
		{
			Scene* scene = m_SceneInterface->GetScene();

			if(m_IsPlaying && scene->HasEnded())
				m_IsPlaying = false;

			if(m_IsPlaying)
			{
				if(m_Gui->getHovered() == m_Viewer)
					m_Gui->setFocus(this);

				scene->Step(dt);
			}

			m_CurrentDlgTime = scene->GetCurrentDialogTime();

			DialogNode* dlg = scene->GetCurrentDialog();
			
			if(dlg && dlg->GetIsLoaded())
			{
				const Dimension2& originalDim = originalSize;
				const Dimension2& renderDim = renderSize; 
				
				// real render zone
				{
					Position2 pos = Position2(0,0) - m_ViewerPosition;
					Dimension2 dim = renderDim;
					Rectangle2 destRect(pos + innerOffset, dim);
					IColor color(255, 0, 0, 0);
					fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
				}

//				{
// 					char text[32];
// 					sprintf(text, "%.2f", m_CurrentDlgTime);
// 					m_TimeText->setText(core::stringw(text).c_str());
//				}

				// sound control
				for(u32 i = 0; i < scene->GetSoundsAlive().size(); ++i)
				{
					SmartPointer<AudioStream> sound = scene->GetSoundsAlive()[i];

					if(sound.IsValid() && sound->IsValid())
					{
						if(m_IsPlaying)
						{
							sound->Play();
						}
						else
						{
							sound->Stop();
						}
					}
				}

				Position2 shakeOffset(P2Zero);
				{
					shakeOffset += GetShakeOffset(m_CurrentDlgTime, innerScale, dlg->GetScreenShakeStrength(), dlg->GetScreenShakeFrequency());
				}

				// BG
				Texture* tex = scene->GetCurrentBackground().IsValid()?scene->GetCurrentBackground()->m_Image:NULL;
				if(tex)
				{
					/*const KeyTime& key = MathUtils::GetInterpolatedKeyTime(dlg->GetImageScene()->m_Keys, m_CurrentDlgTime);

					IColor tint = WHITE;
					Rectangle2 sourceRect(Position2(0,0), tex->getSize());
					Position2 pos = Position2((s32)(key.m_Position.X*innerScale), (s32)(key.m_Position.Y*innerScale)) + innerOffset - m_ViewerPosition;
					Dimension2 dim = tex->getSize();
					Rectangle2 destRect(pos, Dimension2((s32)(dim.Width*innerScale), (s32)(dim.Height*innerScale)));

					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, (f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
						core::matrix4().setRotationAxisRadians(key.m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
						core::matrix4().setScale(core::vector3df(key.m_Scale*(f32)destRect.getWidth()/sourceRect.getWidth(), key.m_Scale*(f32)destRect.getHeight()/sourceRect.getHeight(), 0.0f));

					draw2DImage(driver, tex, sourceRect, mat, true, tint);*/

					const KeyTime& key = MathUtils::GetInterpolatedKeyTime(dlg->GetImageScene()->m_Keys, m_CurrentDlgTime, dlg->GetImageScene()->m_Loop);

					IColor tint = key.m_Color;
					Rectangle2 sourceRect(Position2(0,0), tex->getSize());
					Position2 pos = shakeOffset + Position2((s32)(0*innerScale), (s32)(0*innerScale)) + innerOffset - m_ViewerPosition;
					Dimension2 dim = originalDim;
					float scale = core::max_(key.m_Scale, Epsilon);
					Rectangle2 destRect(pos, Dimension2((s32)(dim.Width*innerScale), (s32)(dim.Height*innerScale)));

					Rectangle2 srcRect(-key.m_Position, Dimension2((s32)((f32)originalDim.Width/scale), (s32)((f32)originalDim.Height/scale)));

					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, (f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
						core::matrix4().setRotationAxisRadians(key.m_Rotation*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
						core::matrix4().setScale(core::vector3df((f32)destRect.getWidth()/srcRect.getWidth(), (f32)destRect.getHeight()/srcRect.getHeight(), 0.0f));

					draw2DImage(driver, tex, srcRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
				}

				// Sprites
				{
					//assert(dlg->GetSprites().size() == dlg->GetSpritesData().size());

					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)shakeOffset.X + innerOffset.X - m_ViewerPosition.X, (f32)shakeOffset.Y + innerOffset.Y - m_ViewerPosition.Y, 0.0f)) * 
						core::matrix4().setScale(core::vector3df(innerScale, innerScale, 0.0f));

					for(u32 i = 0; i < scene->GetSpritesAlive().size(); ++i)
					{
						SmartPointer<SpriteEntity> sprEnt = scene->GetSpritesAlive()[i];
						SmartPointer<AnimatedSprite> spr = sprEnt->GetAnimatedSprite();
						const SpriteDataScene& data = sprEnt->GetSpriteData();
						KeyTime keyTime = MathUtils::GetInterpolatedKeyTime(data.m_Keys, m_CurrentDlgTime, data.m_Loop);
						if(keyTime.m_Time >= 0.0f)
							spr->RenderFrame(spr->GetFrameIndexFromTime(m_CurrentDlgTime, data.m_LoopAnim), mat*keyTime.GetMatrixTransform(), keyTime.m_Color);
					}
				}

				Position2 posInput = Position2((s32)(innerScale*originalSize.Width*0.5f), (s32)(innerScale*originalSize.Height*0.2f)) + innerOffset - m_ViewerPosition;

				// dialog text
				u32 languageIndex = (u32)engine->GetLanguage();
				if(dlg->GetDialogHeader(languageIndex).size() != 0 || dlg->GetDialogText(languageIndex).size() != 0 ||
					(!dlg->GetConditionalTexts(languageIndex).empty() && dlg->GetConditionalTexts(languageIndex)[0].size() != 0))
				{
					Position2 pos = Position2((s32)(originalSize.Width*dlg->GetDialogPosition().X*innerScale), (s32)(originalSize.Height*dlg->GetDialogPosition().Y*innerScale)) + innerOffset - m_ViewerPosition;
					Dimension2 dim((s32)(originalSize.Width*dlg->GetDialogSize().X), (s32)(originalSize.Height*dlg->GetDialogSize().Y));
					Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

					if(dlg->GetUseTextBackGround())
					{
						//driver->draw2DRectangleOutline(destRect, IColor(255, 255, 0, 0));
						Texture* tex = engine->GetTextBG();
						if(tex)
						{
							IColor tint = COLOR_WHITE;
							Rectangle2 srcRect(Position2(0,0), tex->getSize());
							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+(f32)((u32)(dim.Width*innerScale))*0.5f, (f32)pos.Y+(f32)((u32)(dim.Height*innerScale))*0.5f, 0.0f)) *
								core::matrix4().setRotationAxisRadians(0.0f, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df((f32)destRect.getWidth()/srcRect.getWidth(), (f32)destRect.getHeight()/srcRect.getHeight(), 0.0f));
							draw2DImage(driver, tex, srcRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
						}
					}
					const s32 margin = (s32)(innerScale*20);
					destRect.LowerRightCorner.X -= margin;
					destRect.LowerRightCorner.Y -= margin;
					destRect.UpperLeftCorner.X += margin;
					destRect.UpperLeftCorner.Y += margin;

					if(dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID || dlg->GetNodeType() == DialogNode::DN_AUTO_VALID)
						posInput = destRect.LowerRightCorner;
					else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						posInput = destRect.UpperLeftCorner;//Position2(destRect.LowerRightCorner.X, destRect.UpperLeftCorner.Y);

					core::stringw str;
					TArray<CGUIFreetypeFont::ColorElement> colors;
					float ratio = core::abs_(cosf(0.75f*core::PI*m_CurrentDlgTime));
					IColor color = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), 
						MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COLOR_WHITE, 0.5f), ratio);
					Dimension2 strDim = (Dimension2)destRect.getSize();
					core::stringw header = dlg->GetDialogHeader(languageIndex);
					core::stringw text = header + dlg->GetDialogText(languageIndex);
					u32 textBaseSize = text.size();
					colors.push_back(CGUIFreetypeFont::ColorElement(COLOR_WHITE, textBaseSize));
					for(u32 i = 0; i < dlg->GetConditionalTexts(languageIndex).size(); ++i)
					{
						text += L"\n";
						text += dlg->GetConditionalTexts(languageIndex)[i];
						bool isCurrentChoice = (scene->GetCurrentDialogChoice() == i);
						colors.push_back(CGUIFreetypeFont::ColorElement(isCurrentChoice? color : COLOR_WHITE, text.size()-textBaseSize));
						textBaseSize = text.size();
					}
					FormatString(str, text, engine->GetDialogFont(), strDim);
					engine->GetDialogFont()->draw(str.subString(0, (dlg->GetDialogSpeed() == 0.0f)?text.size():header.size()+(u32)(dlg->GetDialogSpeed()*m_CurrentDlgTime)), 
						destRect, colors, 1.0f, false, false, COLOR_TRANSPARENT, &destRect);

					m_DialogChoiceAreas.clear();
					if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID && colors.size() > 1)
					{
						u32 curSize = colors[0].Size;
						for(u32 i = 1; i < colors.size(); ++i)
						{
							TArray<u32> outLinesWidth;
							Rectangle2 currentRect = engine->GetDialogFont()->getSubStringRect(text.c_str(), curSize+1, colors[i].Size-1, outLinesWidth);
							currentRect.LowerRightCorner += posInput;
							currentRect.UpperLeftCorner += posInput;
							m_DialogChoiceAreas.push_back(currentRect);

							curSize += colors[i].Size;
						}

						u32 currentChoice = scene->GetCurrentDialogChoice();
						posInput.X = m_DialogChoiceAreas[currentChoice].UpperLeftCorner.X + m_DialogChoiceAreas[currentChoice].getWidth();
						posInput.Y = m_DialogChoiceAreas[currentChoice].UpperLeftCorner.Y + (m_DialogChoiceAreas[currentChoice].getHeight()>>1);
					}
				}
				else
				{
					Position2 pos = Position2((s32)(originalSize.Width*dlg->GetDialogPosition().X*innerScale), (s32)(originalSize.Height*dlg->GetDialogPosition().Y*innerScale)) + innerOffset - m_ViewerPosition;
					Dimension2 dim((s32)(originalSize.Width*dlg->GetDialogSize().X), (s32)(originalSize.Height*dlg->GetDialogSize().Y));
					Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

					const s32 margin = (s32)(innerScale*20);
					destRect.LowerRightCorner.X -= margin;
					destRect.LowerRightCorner.Y -= margin;
					destRect.UpperLeftCorner.X += margin;
					destRect.UpperLeftCorner.Y += margin;

					if(dlg->GetNodeType() == DialogNode::DN_MANUAL_VALID || dlg->GetNodeType() == DialogNode::DN_AUTO_VALID)
						posInput = destRect.LowerRightCorner;
					else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)
						posInput = destRect.UpperLeftCorner;//Position2(destRect.LowerRightCorner.X, destRect.UpperLeftCorner.Y);
				}

				// Buttons display
				{
					InputData& inputs = scene->GetCurrentInputs();
					float currentInputsRemainingTime = scene->GetCurrentInputsRemainingTime();
					if(inputs.m_Inputs != 0)
					{
#ifdef _DEBUG
						// debug display
						{
							Position2 pos = Position2(0,0) + innerOffset - m_ViewerPosition;
							Dimension2 dim(originalDim.Width, originalDim.Height);
							Rectangle2 destRect(pos, Dimension2((u32)(dim.Width*innerScale), (u32)(dim.Height*innerScale)));

							//driver->draw2DRectangleOutline(destRect, IColor(255, 255, 0, 0));
							core::stringw text = L"Appuyez sur ";
							bool inputProcessed = false;
							TArray<CGUIFreetypeFont::ColorElement> colorElts;
							colorElts.push_back(CGUIFreetypeFont::ColorElement(COLOR_WHITE, text.size()));
							for(u32 i = 0; i < InputData::PI_MAX; ++i)
							{
								if(inputs.IsPressed((InputData::EPadInput)i))
								{
									IColor color = (scene->GetCurrentInputsPressed().IsPressed((InputData::EPadInput)i))?
										IColor(255, 0, 255, 0):IColor(255, 255, 0, 0);
									
									u32 start = text.size();
									if(inputProcessed)
										text += L" ET ";
									inputProcessed = true;
									text += InputData::GetInputName((InputData::EPadInput)i);
									u32 end = text.size();

									colorElts.push_back(CGUIFreetypeFont::ColorElement(color, end - start));
								}
							}
							engine->GetDialogFont()->draw(text, destRect, colorElts, 1.0f,  false, false);
						}
#endif // #ifdef _DEBUG

						// real display
						{
							u32 alpha = 255;
							IColor color = (dlg->GetNodeType() == DialogNode::DN_QTE_VALID && inputs.m_NbCurrentPress >= inputs.m_NbPress)? 
								IColor(alpha, 100, 255, 100) : IColor(alpha, 255, 255, 255);

							IColor tint(COLOR_WHITE);
							{
								float ratio = core::abs_(cosf(0.75f*core::PI*m_CurrentDlgTime));
								tint = MathUtils::GetInterpolatedColor(IColor(alpha, 9, 173, 231), IColor(alpha>>1, 255, 255, 255), 1.0f-ratio);
							}

							TArray<ProfileData::KeyMappingSerializable::IIKeyType> inputKeys;
							for(u32 i = 0; i < InputData::PI_MAX; ++i)
							{
								if(inputs.IsPressed((InputData::EPadInput)i))
								{
									inputKeys.push_back((ProfileData::KeyMappingSerializable::IIKeyType)i);
								}
							}
							Dimension2 largestSize(230,200);
							if(engine->GetKeyElement(Engine::KET_EMPTY_KEY2))
								largestSize = engine->GetKeyElement(Engine::KET_EMPTY_KEY2)->getSize();
							float renderScale = 0.35f * innerScale;
							float dx = renderScale*largestSize.Width;
							float dy = renderScale*largestSize.Height;
							for(u32 i = 0; i < inputKeys.size(); ++i)
							{
								ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;

								EKEY_CODE keycode = keymap.GetKeyCode1(inputKeys[i]);
								core::stringw strCode = GetStringFromKeyCode(keycode);

								if(strCode != L"")
								{
									Engine::KeyElementType type = (strCode.size() > 1)? Engine::KET_EMPTY_KEY2 : Engine::KET_EMPTY_KEY1;
									// special keys
									if(keycode == KEY_LBUTTON)
										type = Engine::KET_LMOUSE;
									else if(keycode == KEY_RBUTTON)
										type = Engine::KET_RMOUSE;
									else if(keycode == KEY_LEFT)
										type = Engine::KET_LEFT_KEY;
									else if(keycode == KEY_RIGHT)
										type = Engine::KET_RIGHT_KEY;
									else if(keycode == KEY_UP)
										type = Engine::KET_UP_KEY;
									else if(keycode == KEY_DOWN)
										type = Engine::KET_DOWN_KEY;

									const float fadeInDuration = core::min_(0.15f, 0.5f*inputs.m_Duration);
									const float fadeOutDuration = core::min_(0.3f, 0.5f*inputs.m_Duration);
									const float maxZoomFactor = 3.0f;
									const float minZoomFactor = 0.01f;
									float fadeEffector = 1.0f;
									float fadeEffector2 = 1.0f;
									if(dlg->GetNodeType() == DialogNode::DN_QTE_VALID && fadeInDuration > 0.0f && fadeOutDuration > 0.0f)
									{
										if(currentInputsRemainingTime >= inputs.m_Duration - fadeInDuration)
										{
											fadeEffector = inputs.m_Duration - currentInputsRemainingTime;// 0 to duration
											fadeEffector = 1.0f - core::min_(fadeEffector, fadeInDuration)/fadeInDuration;// 1.0f to 0.0f in fadeInDuration seconds
											fadeEffector *= (maxZoomFactor-1.0f);
											fadeEffector += 1.0f;// from maxZoomFactor to 1.0f in fadeInDuration seconds
										}
										else if(currentInputsRemainingTime <= fadeOutDuration)
										{
											fadeEffector = currentInputsRemainingTime;// duration to 0
											fadeEffector = 1.0f - core::min_(fadeEffector, fadeOutDuration)/fadeOutDuration;// 0.0f to 1.0f in fadeOutDuration seconds
											fadeEffector *= (minZoomFactor-1.0f);
											fadeEffector += 1.0f;// from 1.0f to minZoomFactor in fadeOutDuration seconds
										}
										else
										{
											if(inputs.m_NbPress > 1)
												fadeEffector2 = 1.0f - 0.6f * core::abs_(cosf(5.0f*core::PI*currentInputsRemainingTime));
											else
												fadeEffector2 = 1.0f - 0.1f * core::abs_(cosf(1.5f*core::PI*currentInputsRemainingTime));
										}
									}
									else
									{
										fadeEffector2 = 1.0f - 0.1f * core::abs_(cosf(1.5f*core::PI*m_CurrentDlgTime));
									}

									Position2 pos2(posInput);
									if(dlg->GetNodeType() == DialogNode::DN_QTE_VALID)// top centered
									{
										if(inputKeys.size() > 1)
											pos2.X += (s32)(((float)i - 0.5f * (inputKeys.size()-1)) * (dx + dy) * fadeEffector);
									}
									else if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)// left aligned
									{
									}
									else// bottom right aligned
									{
										if(inputKeys.size() > 1)
											pos2.X -= (s32)((dx + dy) * (inputKeys.size() - i - 1));
										pos2.Y -= (s32)(dy*0.5f);
									}


									if(engine->GetKeyElement(type))
									{
										Dimension2 size = engine->GetKeyElement(type)->getSize();
										Rectangle2 sourceRect(Position2(0,0), size);

										if(dlg->GetNodeType() == DialogNode::DN_MULTI_VALID)// left aligned
											pos2.X += (s32)(renderScale*size.Width*0.5f);
										else if(dlg->GetNodeType() != DialogNode::DN_QTE_VALID)// bottom right aligned
											pos2.X -= (s32)(renderScale*size.Width*0.5f);

										float renderScale = (float)dy/size.Height;
										if(type == Engine::KET_EMPTY_KEY2)
											renderScale *= 2.0f;
										else if(type == Engine::KET_RMOUSE || type == Engine::KET_LMOUSE)
											renderScale *= 1.0f;
										else
											renderScale *= 1.5f;


										renderScale *= fadeEffector*fadeEffector2;

										core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
											core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
											core::matrix4().setScale(core::vector3df(renderScale, renderScale, 0.0f));

										draw2DImage(driver, engine->GetKeyElement(type), sourceRect, mat, true, color, true, USE_PREMULTIPLIED_ALPHA);
									}

									if(type == Engine::KET_EMPTY_KEY2 || type == Engine::KET_EMPTY_KEY1)
									{
										//if(isSelected)
										{
											engine->GetDialogFont()->drawGauss(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), color, fadeEffector, true, true);
										}
										engine->GetDialogFont()->draw(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), color, fadeEffector, true, true);
									}
								}
							}
						}
					}
				}

				// draw transition effects
				{
					// in
					if(scene->IsInIntro())
					{
						switch(dlg->GetTransitionIn().m_Type)
						{
						case DialogNode::DN_TRANSITION_FADE:
							{
								const u8 a = 255-(u8)(255.0f*scene->GetIntroRatio());
								IColor color(a, 0, 0, 0);
								Position2 pos = innerOffset - m_ViewerPosition;
								Dimension2 dim((u32)(originalDim.Width*innerScale), (u32)(originalDim.Height*innerScale));
								Rectangle2 destRect(pos, dim);
								fill2DRect(driver, destRect, color, color, color, color, true/*USE_PREMULTIPLIED_ALPHA*/);
							}
							break;
						}
					}
					// out
					if(scene->IsInOutro())
					{
						switch(dlg->GetTransitionOut().m_Type)
						{
						case DialogNode::DN_TRANSITION_FADE:
							{
								IColor color(255-(u8)(255.0f*scene->GetOutroRatio()), 0, 0, 0);
								Position2 pos = innerOffset - m_ViewerPosition;
								Dimension2 dim((u32)(originalDim.Width*innerScale), (u32)(originalDim.Height*innerScale));
								Rectangle2 destRect(pos, dim);
								fill2DRect(driver, destRect, color, color, color, color, true/*USE_PREMULTIPLIED_ALPHA*/);
							}
							break;
						}
					}
				}

				// real render zone : redraw the black bands 
				{
					IColor color(255, 0, 0, 0);
					// left
					{
						Position2 pos = Position2(0, 0) - m_ViewerPosition;
						Dimension2 dim(innerOffset.X, renderDim.Height);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}

					// right
					{
						Position2 pos = Position2((s32)(originalDim.Width*innerScale) + innerOffset.X, 0) - m_ViewerPosition;
						Dimension2 dim(innerOffset.X+1, renderDim.Height);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}

					// up
					{
						Position2 pos = Position2(0, 0) - m_ViewerPosition;
						Dimension2 dim(renderDim.Width, innerOffset.Y);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}

					// down
					{
						Position2 pos = Position2(0, (s32)(originalDim.Height*innerScale) + innerOffset.Y) - m_ViewerPosition;
						Dimension2 dim(renderDim.Width, innerOffset.Y+1);
						Rectangle2 destRect(pos, dim);
						fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
					}
				}

				// debug : render zone
// 				{
// 					const IColor grey(127, 0, 255, 0);
// 					{
// 						Position2 pos = innerOffset - m_ViewerPosition;
// 						Dimension2 dim((s32)(originalDim.Width*innerScale), (s32)(originalDim.Height*innerScale));
// 						driver->draw2DLine(pos, pos + Position2(dim.Width-1, 0), grey);
// 						driver->draw2DLine(pos, pos + Position2(0, dim.Height-1), grey);
// 						driver->draw2DLine(pos + Position2(dim.Width-1, dim.Height-1), pos + Position2(dim.Width-1, 0), grey);
// 						driver->draw2DLine(pos + Position2(dim.Width-1, dim.Height-1), pos + Position2(0, dim.Height-1), grey);
// 					}
// 				}
			}
		}

		// set back old render target
		// The buffer might have been distorted, so clear it
		driver->setRenderTarget(0, true, true, IColor(0,200,200,200));
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

#undef GUI_ID_CONTEXT_MENU