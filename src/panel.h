﻿#ifndef PANEL_H
#define PANEL_H

#include "utils.h"
#include "engine.h"
#include "scene.h"

#include "glossary.h"

#include "CGUITTFont.h"

#define PANEL_MENU_FADER_ALPHA 200
#define PANEL_MENU_NOT_ACTIVE_TRANSPARENCY 180
#define PANEL_THEME_COLOR 0xff09ade7

class IPanel
{
public:

	struct Fader
	{
		enum FaderType {FT_FADEIN, FT_FADEOUT};
		enum FaderSide {FS_FADE_RIGHT, FS_FADE_LEFT};

		Fader(): Duration(0.0f), FadeTimer(0.0f), FadeType(FT_FADEIN), FadeSide(FS_FADE_RIGHT) {}
		Fader(float fadeTime, FaderType fadeType = FT_FADEIN, FaderSide fadeSide = FS_FADE_RIGHT): Duration(fadeTime), FadeTimer(0.0f), FadeType(fadeType), FadeSide(fadeSide) {}

		void StartFade()
		{
			FadeTimer = Duration;
		}
		bool IsFading() const {return (FadeTimer > 0.0f);}
		void Tick(float dt) {if(IsFading())	FadeTimer = core::max_(FadeTimer - dt, 0.0f);}
		float GetRealRatio() const {return (Duration != 0.0f)? FadeTimer/Duration : 0.0f;}
		float GetRatio() const {return (FadeType == Fader::FT_FADEIN)? 1.0f - GetRealRatio() : GetRealRatio();}

		float Duration;
		float FadeTimer;
		FaderType FadeType;
		FaderSide FadeSide;
	};

	virtual ~IPanel() {m_Children.clear();}

	void AddChild(IPanel* child) {if(child) m_Children.push_back(child);}
	IPanel* GetChildAtIndex(u32 index) {return (index < m_Children.size())? m_Children[index] : NULL;}

	virtual void Update(float dt) 
	{
		m_Time += dt;
		m_WasFading = m_Fader.IsFading();
		m_Fader.Tick(dt);
	}
	virtual void PreRender() {}
	virtual void Render() {}
	virtual void PostRender() {}
	virtual bool OnEvent(const SEvent& event) {return false;}

	virtual void ResetPanel() {m_Time = 0.0f; m_TransitToChildPanelIndex = -1;}

	virtual bool ShouldForceReload() const {return false;}// tell if the panel should be reloaded when transitioning from itself to itself again

	virtual void LoadPanel() {m_IsLoaded = true;}
	virtual void UnloadPanel() {m_IsLoaded = false;}

	virtual void OnResize() {}

	virtual void OnDevicePreReset() {}
	virtual void OnDevicePostReset() {}

	void StartFade(const Fader& fader) {m_Fader = fader; m_Fader.StartFade();}
	bool IsFading() const {return (m_WasFading || m_Fader.IsFading());}

	bool IsLoaded() const {return m_IsLoaded;}

	s32 TransitToChildPanelIndex() const {return m_TransitToChildPanelIndex;}

protected:
	struct ButtonArea
	{
		Rectangle2 Area;
		bool IsSelected;

		ButtonArea() : Area(0,0,0,0), IsSelected(false) {}
	};

	IPanel() : m_Time(0.0f), m_TransitToChildPanelIndex(-1), m_IsLoaded(false), m_WasFading(false) {}
	s32 m_TransitToChildPanelIndex;// < 0 = no transit
	bool m_IsLoaded;
	bool m_WasFading;// allows to display 100% faded frame

	float m_Time;

	Fader m_Fader;

	TArray<IPanel*> m_Children;
};

class ScenePanel : public IPanel
{
public:
	ScenePanel();
	virtual ~ScenePanel();

	virtual void Update(float dt);
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	virtual void OnDevicePreReset();
	virtual void OnDevicePostReset();

	core::stringc GetScenePath() const {return m_ScenePath;}
	void SetScenePath(const core::stringc& path) {m_ScenePath = path;}// must do a LoadPanel() after this call to change the scene

	void SetCurrentDialog(u32 dialogID = 0xffffffff, bool restoreSounds = false);

	void ForceLoadScene() {if(m_Scene) m_Scene->ForceLoadDialog();}

	void SetDimension(const Position2& position = P2Zero, float scale = 1.0f) 
	{
		bool resized = (m_Scale != scale);
		m_Position = position;
		m_Scale = scale;
		if(resized)
			OnResize();
	}

	void UsePremultipliedAlpha(bool state) {m_UsePremultipliedAlpha = state;}

	bool IsSceneFinished() const {return (!m_Scene || m_Scene->GetCurrentDialog() == NULL);}

	enum PlayState {PS_NONE, PS_GAME, PS_QUICK_MENU, PS_MENU, PS_MAX};
	PlayState GetPlayState() const {return m_PlayState;}
	void SetPlayState(PlayState state) {m_PlayState = state;}

	void SkipDialog();

	Scene* GetScene() const {return m_Scene;}

	bool m_DisplayDialogs:1;
	bool m_DisplayFades:1;
	bool m_DisplayKeys:1;
	bool m_UpdateKeys:1;
	bool m_PlaySounds:1;
	bool m_Loop1stDialog:1;
	bool m_IsTheRealGameScene:1;
protected:
	CGUIFreetypeFont* m_Font;

	Scene* m_Scene;

	float m_CurrentDlgTime;
	PlayState m_PlayState;

	float m_Scale;
	Position2 m_Position;

	core::stringc m_ScenePath;

	bool m_UsePremultipliedAlpha;

	u32 m_BackupCurrentDlgID;
	float m_BackupCurrentDlgTime;

	TArray<Rectangle2> m_DialogChoiceAreas;
};

class LogoPanel : public IPanel
{
public:
	LogoPanel();
	virtual ~LogoPanel();

	virtual void Update(float dt);
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();


protected:
	Texture* m_Logo[3];
	Texture* m_BG;

	u32 m_Phase;
};

class PopupPanel : public IPanel
{
public:
	enum PopupType {PT_OK, PT_OK_CANCEL, PT_YES_NO, PT_MAX};
	enum PopupResultType {PRT_NONE, PRT_YES, PRT_NO, PRT_OK, PRT_CANCEL, PRT_MAX};

	PopupPanel(bool usePremultipliedAlpha = USE_PREMULTIPLIED_ALPHA);
	virtual ~PopupPanel();

	virtual void Update(float dt);
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);
	virtual void OnResize();

	virtual void LoadPanel();
	virtual void UnloadPanel();

	PopupType GetType() const {return m_Type;}
	void SetType(PopupType type) {m_Type = type;}

	bool IsVisible() const {return m_IsVisible;}
	void SetVisible(bool state) {m_IsVisible = state;}

	PopupResultType GetResult() const {return m_Result;}
	void Display(PopupType type, const wchar_t* const message);

protected:
	Texture* m_Window;

	CGUIFreetypeFont *m_Font[3];

	PopupType m_Type;
	core::stringw m_Message;

	bool m_IsVisible;
	PopupResultType m_Result;

	ButtonArea m_Buttons[2];

	u32 m_CurrentSelection;

	bool m_UsePremultipliedAlpha;
};

class LanguagePanel : public IPanel
{
public:
	LanguagePanel();
	virtual ~LanguagePanel();

	virtual void Update(float dt);
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

protected:
	Texture* m_BG;
	CGUIFreetypeFont* m_Font;

	Texture* m_Languages[Engine::LS_MAX];
	Texture* m_LangSelec;
	Texture* m_Arrows[2];

	ButtonArea m_LanguageSelectionArea[3];
	ButtonArea m_LanguagesArea[Engine::LS_MAX];
	u32 m_CurrentLangSelection;

};

class MainMenuPanel : public IPanel
{
public:
	enum MainButtonType 
	{
		MBT_ESCAPE, 
		MBT_STORY, 
		MBT_STAT, 
		MBT_OPTIONS, 
		MBT_BONUS, 
		MBT_EXIT, 
		MBT_MAX
	};

	MainMenuPanel();
	virtual ~MainMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	void ForceDefaultSelection();

protected:
	Texture* m_BG;
	CGUIFreetypeFont* m_Font[3];

	Texture* m_MainLine;
	Texture* m_EscapeButton;
	
	ButtonArea m_ButtonsArea[MBT_MAX];

	u32 m_CurrentSelection;
	u32 m_PreviousSelection;
	float m_SelectionFadeTimer;
	s32 m_ScrollingSide;

	IPanel* m_SubPanels[MBT_EXIT - MBT_STORY + 1];// selectable with m_CurrentSelection
	ScenePanel* m_ScenePanel;

	GameDesc* m_GameDesc;
};

class StartMenuPanel : public IPanel
{
public:
	StartMenuPanel();
	virtual ~StartMenuPanel();

	virtual void Update(float dt);
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

protected:
	Texture* m_BG;
	CGUIFreetypeFont* m_Font;

	Texture* m_Title;
};

class StorySubMenuPanel : public IPanel
{
public:
	enum SubButtonType 
	{
		SBT_CONTINUE, 
		SBT_NEWGAME, 
		SBT_LOAD, 
		SBT_DELETESAVE, 
		SBT_CHAPTER, 
		SBT_MAX
	};

	StorySubMenuPanel();
	virtual ~StorySubMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	enum SubMenuAreaType {SMAT_LEFT, SMAT_BOTTOM, SMAT_CENTER, SMAT_POPUP, SMAT_MAX};
	SubMenuAreaType GetCurrentSubMenuActive() const {return (SubMenuAreaType)m_CurrentSubMenuActive;}

protected:
	void RenderFader(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderNewGame(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderSave(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderChapter(float scale = 1.0f, const Position2& offset = P2Zero);
	void _Render(float scale = 1.0f, const Position2& offset = P2Zero);

	CGUIFreetypeFont* m_Font[4];

	Texture* m_ActiveBG;

	Texture* m_Arrows[2];

	Texture* m_Selec;

	Texture* m_SaveSlotTex;
	Texture* m_SaveSlotDelete;
	Texture* m_SaveSlotDeleteA;

	ButtonArea m_SubMenusArea[SMAT_MAX];
	ButtonArea m_ButtonsArea[SBT_MAX];
	ButtonArea m_ChapterSlotArea[5];
	ButtonArea m_SceneSelectionArea[2];

	ButtonArea m_ActiveValidationArea;

	u32 m_CurrentSubMenuActive;
	u32 m_PreviousSubMenuActive;

	void ChangeSubMenuActive(u32 newSMAT)
	{
		for(int i = 0; i < SMAT_MAX; ++i)
			m_SubMenusArea[i].IsSelected = false;

		m_PreviousSubMenuActive = m_CurrentSubMenuActive;
		m_CurrentSubMenuActive = newSMAT;
		m_CurrentSelectionValidatedFader.StartFade();

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	// left menu
	u32 m_CurrentSelection;
	bool m_CurrentSelectionValidated;
	Fader m_CurrentSelectionValidatedFader;

	// save menus
	u32 m_CurrentSaveSlotSelected;

	// chapter menu
	u32 m_CurrentChapterSelected;
	s32 m_CurrentChapterSlotSelected;
	u32 m_CurrentSceneSelected;
	Fader m_CurrentChapterSelectionFader;

	video::ITexture* m_RenderTarget;

	ScenePanel* m_ScenePreviewNew;
	ScenePanel* m_ScenePreviewContinue;
	ScenePanel* m_ScenePreviewSave[PROFILE_SAVE_SLOTS];

	PopupPanel* m_PopupPanel;
};

class StatsSubMenuPanel : public IPanel
{
public:
	enum SubButtonType 
	{
		SBT_DEFAULT,
		SBT_MAX
	};

	StatsSubMenuPanel();
	virtual ~StatsSubMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	enum SubMenuAreaType {SMAT_LEFT, SMAT_BOTTOM, SMAT_CENTER, SMAT_POPUP, SMAT_MAX};
	SubMenuAreaType GetCurrentSubMenuActive() const {return (SubMenuAreaType)m_CurrentSubMenuActive;}

protected:
	void RenderFader(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderDefault(float scale = 1.0f, const Position2& offset = P2Zero);
	void _Render(float scale = 1.0f, const Position2& offset = P2Zero);

	CGUIFreetypeFont* m_Font[2];

	Texture* m_ActiveBG;

	Texture* m_Arrows[2];

	ButtonArea m_SubMenusArea[SMAT_MAX];
	ButtonArea m_ButtonsArea[SBT_MAX];

	ButtonArea m_ActiveValidationArea;

	u32 m_CurrentSubMenuActive;
	u32 m_PreviousSubMenuActive;

	void ChangeSubMenuActive(u32 newSMAT)
	{
		for(int i = 0; i < SMAT_MAX; ++i)
			m_SubMenusArea[i].IsSelected = false;

		m_PreviousSubMenuActive = m_CurrentSubMenuActive;
		m_CurrentSubMenuActive = newSMAT;
		m_CurrentSelectionValidatedFader.StartFade();

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	// left menu
	u32 m_CurrentSelection;
	bool m_CurrentSelectionValidated;
	Fader m_CurrentSelectionValidatedFader;

	video::ITexture* m_RenderTarget;

	PopupPanel* m_PopupPanel;
};

class OptionsSubMenuPanel : public IPanel
{
public:
	enum SubButtonType 
	{
		SBT_DISPLAY, 
		SBT_AUDIO, 
		SBT_KEYS, 
		SBT_LANGUAGE, 
		SBT_MAX
	};

	OptionsSubMenuPanel();
	virtual ~OptionsSubMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	enum SubMenuAreaType {SMAT_LEFT, SMAT_BOTTOM, SMAT_CENTER, SMAT_POPUP, SMAT_MAX};
	SubMenuAreaType GetCurrentSubMenuActive() const {return (SubMenuAreaType)m_CurrentSubMenuActive;}

protected:
	void RenderFader(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderDisplay(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderAudio(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderLanguage(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderKeys(float scale = 1.0f, const Position2& offset = P2Zero);
	void _Render(float scale = 1.0f, const Position2& offset = P2Zero);

	CGUIFreetypeFont* m_Font[2];

	Texture* m_ActiveBG;

	ButtonArea m_SubMenusArea[SMAT_MAX];
	ButtonArea m_ButtonsArea[SBT_MAX];
	ButtonArea m_LinesArea[12];

	u32 m_CurrentLine;

	u32 m_CurrentSubMenuActive;
	u32 m_PreviousSubMenuActive;

	void ChangeSubMenuActive(u32 newSMAT)
	{
		for(int i = 0; i < SMAT_MAX; ++i)
			m_SubMenusArea[i].IsSelected = false;

		m_PreviousSubMenuActive = m_CurrentSubMenuActive;
		m_CurrentSubMenuActive = newSMAT;
		m_CurrentSelectionValidatedFader.StartFade();

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	bool m_IsProcessingKey;
	bool ProcessKey(const SEvent& event);

	// left menu
	u32 m_CurrentSelection;
	bool m_CurrentSelectionValidated;
	Fader m_CurrentSelectionValidatedFader;

	video::ITexture* m_RenderTarget;

	PopupPanel* m_PopupPanel;
	ProfileData::OptionDataSerializable m_ODS;
	ProfileData::KeyMappingSerializable m_KMS;

	Slider m_Slider;
};

class BonusSubMenuPanel : public IPanel
{
public:
	enum SubButtonType 
	{
		SBT_GLOSSARY, 
		SBT_GALLERY, 
		SBT_DRAGONANDWEED, 
		SBT_CREDITS, 
		SBT_MAX
	};

	BonusSubMenuPanel();
	virtual ~BonusSubMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	enum SubMenuAreaType {SMAT_LEFT, SMAT_BOTTOM, SMAT_CENTER, SMAT_POPUP, SMAT_MAX};
	SubMenuAreaType GetCurrentSubMenuActive() const {return (SubMenuAreaType)m_CurrentSubMenuActive;}

protected:
	void RenderFader(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderSceneBased(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderGlossary(float scale = 1.0f, const Position2& offset = P2Zero);
	void _Render(float scale = 1.0f, const Position2& offset = P2Zero);

	bool ProcessSearch(const SEvent& event);

	CGUIFreetypeFont* m_Font[3];

	Texture* m_ActiveBG;

	Texture* m_SearchBar;

	Texture* m_Arrows[2];

	Texture* m_Selec;

	ButtonArea m_SubMenusArea[SMAT_MAX];
	ButtonArea m_ButtonsArea[SBT_MAX];

	ButtonArea m_ActiveValidationArea;

	u32 m_CurrentSubMenuActive;
	u32 m_PreviousSubMenuActive;

	void ChangeSubMenuActive(u32 newSMAT)
	{
		for(int i = 0; i < SMAT_MAX; ++i)
			m_SubMenusArea[i].IsSelected = false;

		m_PreviousSubMenuActive = m_CurrentSubMenuActive;
		m_CurrentSubMenuActive = newSMAT;
		m_CurrentSelectionValidatedFader.StartFade();

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	u32 m_CurrentGlossaryEntry;
	u32 m_CurrentGlossaryEntryVisibilityStart;
	u32 m_CurrentGlossaryContentVisibilityStart;
	bool m_CurrentGlossaryEntryActive;
	bool m_GlossarySearchActive;
	ButtonArea m_LineArea[13];// 8 glossary entries + glossary content + searchbox + entry list zone + 2 scrollbars 

	u32 m_CurrentGlossaryContentLineNb;
	core::stringw m_CurrentGlossarySearch;
	core::stringw m_CurrentCachedGlossaryContent;
	TArray<GlossaryEntry*> m_CurrentEntries;

	// left menu
	u32 m_CurrentSelection;
	bool m_CurrentSelectionValidated;
	Fader m_CurrentSelectionValidatedFader;

	video::ITexture* m_RenderTarget;

	PopupPanel* m_PopupPanel;

	Glossary m_Glossary;

	Texture* m_PreviewGallery;
	Texture* m_PreviewDandW;
	Texture* m_PreviewCredits;

	ScrollBar m_ScrollBar;
};

class ExitSubMenuPanel : public IPanel
{
public:
	ExitSubMenuPanel();
	virtual ~ExitSubMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	void SetParentPanel(MainMenuPanel* parent) {m_ParentPanel = parent;}

protected:
	void RenderFader(float scale = 1.0f, const Position2& offset = P2Zero);
	void _Render(float scale = 1.0f, const Position2& offset = P2Zero);

	video::ITexture* m_RenderTarget;

	PopupPanel* m_PopupPanel;

	MainMenuPanel* m_ParentPanel;
};

class InGameMenuPanel : public IPanel
{
public:
	enum MainButtonType 
	{
		MBT_RESUME, 
		MBT_SAVE, 
		MBT_LOAD, 
		MBT_OPTIONS, 
		MBT_EXIT, 
		MBT_MAX
	};

	enum SubButtonType 
	{
		SBT_DISPLAY, 
		SBT_AUDIO, 
		SBT_KEYS, 
		SBT_LANGUAGE, 
		SBT_MAX
	};

	InGameMenuPanel();
	virtual ~InGameMenuPanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual void OnResize();

	virtual void OnDevicePreReset();
	virtual void OnDevicePostReset();

	enum MenuAreaType {MAT_BOTTOM, MAT_RIGHT, MAT_CENTER, MAT_POPUP, MAT_MAX};
	MenuAreaType GetCurrentMenuActive() const {return (MenuAreaType)m_CurrentMenuActive;}

	bool IsRequestingQuit() const {return m_RequestQuit;}
	void RequestQuit(bool state) {m_RequestQuit = state;}

	bool IsRequestingSkip() const {return m_RequestSkip;}
	void RequestSkip(bool state) {m_RequestSkip = state;}

	bool IsQuickMenu() const {return m_UseQuickMenu;}
	void UseQuickMenu(bool state) {m_UseQuickMenu = state;}

protected:
	void UpdateMenu(float dt);
	void RenderMenu();
	bool OnEventMenu(const SEvent& event);
	void _RenderMenu(float scale = 1.0f, const Position2& offset = P2Zero);

	void UpdateQuickMenu(float dt);
	void RenderQuickMenu();
	bool OnEventQuickMenu(const SEvent& event);
	void _RenderQuickMenu(float scale = 1.0f, const Position2& offset = P2Zero);

	void RenderFader(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderDisplay(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderAudio(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderLanguage(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderKeys(float scale = 1.0f, const Position2& offset = P2Zero);
	void RenderSave(float scale = 1.0f, const Position2& offset = P2Zero);

	CGUIFreetypeFont* m_Font[4];

	Texture* m_MenuTex;
	Texture* m_QuickMenuTex;

	Texture* m_Selec;

	Texture* m_SaveSlotTex;

	ButtonArea m_SubMenusArea[MAT_MAX];
	ButtonArea m_ButtonsArea[SBT_MAX];
	ButtonArea m_ButtonsArea2[MBT_MAX];
	ButtonArea m_LinesArea[12];

	ButtonArea m_ActiveValidationArea;

	u32 m_CurrentLine;

	// save menu
	u32 m_CurrentSaveSlotSelected;

	u32 m_CurrentMenuActive;
	u32 m_PreviousMenuActive;

	void ChangeMenuActive(u32 newMAT)
	{
		for(int i = 0; i < MAT_MAX; ++i)
			m_SubMenusArea[i].IsSelected = false;

		m_PreviousMenuActive = m_CurrentMenuActive;
		m_CurrentMenuActive = newMAT;
		m_MATChangeFader.StartFade();

		m_SubMenusArea[m_CurrentMenuActive].IsSelected = true;
	}

	bool m_IsProcessingKey;
	bool ProcessKey(const SEvent& event);

	// sub menu
	u32 m_CurrentMBSelection;
	bool m_CurrentMBSelectionValidated;

	// sub menu
	u32 m_CurrentSBSelection;
	bool m_CurrentSBSelectionValidated;

	bool m_RequestQuit;
	bool m_RequestSkip;

	Fader m_MATChangeFader;
	Fader m_MenuSelectionFader;

	video::ITexture* m_RenderTarget;

	PopupPanel* m_PopupPanel;
	ProfileData::OptionDataSerializable m_ODS;
	ProfileData::KeyMappingSerializable m_KMS;

	Slider m_Slider;

	bool m_UseQuickMenu;
};

class GamePanel : public IPanel
{
public:
	GamePanel();
	virtual ~GamePanel();

	virtual void Update(float dt);
	virtual void PreRender();
	virtual void Render();
	virtual bool OnEvent(const SEvent& event);

	virtual void LoadPanel();
	virtual void UnloadPanel();

	virtual bool ShouldForceReload() const {return true;}

	virtual void OnResize();

	virtual void OnDevicePreReset();
	virtual void OnDevicePostReset();

protected:
	ScenePanel* m_ScenePanel;
	InGameMenuPanel* m_MenuPanel;
};



#endif 
