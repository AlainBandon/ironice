﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

// GamePanel
GamePanel::GamePanel() : IPanel()
{
	m_ScenePanel = new ScenePanel();
	m_MenuPanel = new InGameMenuPanel();
}

GamePanel::~GamePanel()
{
	UnloadPanel();

	SafeDelete(m_ScenePanel);
	SafeDelete(m_MenuPanel);
}

void GamePanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	engine->DrawLoadScreen();

	if(!m_ScenePanel->IsLoaded())
	{
		m_ScenePanel->SetScenePath(GameDesc::GetScenePath(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene));
		m_ScenePanel->m_IsTheRealGameScene = true;
		m_ScenePanel->LoadPanel();
		if(GameDesc::m_CurrentDialog > 0)
			m_ScenePanel->SetCurrentDialog(GameDesc::m_CurrentDialog - 1, true);
	}
	if(!m_MenuPanel->IsLoaded())
	{
		m_MenuPanel->LoadPanel();
	}

	OnResize();

	StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN));

	IPanel::LoadPanel();
}

void GamePanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	if(m_ScenePanel)
		m_ScenePanel->UnloadPanel();
	if(m_MenuPanel)
		m_MenuPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void GamePanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	if(m_ScenePanel)
		m_ScenePanel->OnResize();
	if(m_MenuPanel)
		m_MenuPanel->OnResize();
}

void GamePanel::OnDevicePreReset() 
{
	if(m_ScenePanel) 
		m_ScenePanel->OnDevicePreReset();
	if(m_MenuPanel) 
		m_MenuPanel->OnDevicePreReset();
}
void GamePanel::OnDevicePostReset() 
{
	if(m_ScenePanel) 
		m_ScenePanel->OnDevicePostReset();
	if(m_MenuPanel) 
		m_MenuPanel->OnDevicePostReset();
}


void GamePanel::Update(float dt) 
{
	IPanel::Update(dt);

	if(GameDesc::m_RequestSceneStart)// A scene load has been requested by the user while in game : exit the current scene and reload the new one
	{
		GameDesc::m_RequestSceneStart = false;
		m_TransitToChildPanelIndex = 0;
	}

	if(m_ScenePanel)
	{
		if(m_MenuPanel && m_MenuPanel->IsRequestingSkip())
		{
			m_ScenePanel->SkipDialog();
			m_MenuPanel->RequestSkip(false);
		}

		m_ScenePanel->Update(dt);
		if(m_ScenePanel->IsSceneFinished())// transit to the next one
		{
			if(GameDescManager::GetGameDesc()->GoToNextScene())// go to next one if any
				m_TransitToChildPanelIndex = 0;
			else// otherwise = game finished : return to the main menu
				m_TransitToChildPanelIndex = 1;

			// save the progression
			if(m_TransitToChildPanelIndex == 0)// game not finished yet
			{
				float progression = GameDescManager::GetGameDesc()->GetProgressionRatio(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene);
				if(progression > GameDescManager::GetGameDesc()->GetProgressionRatio(ProfileData::GetInstance()->m_PDS.m_ProgressionCH, ProfileData::GetInstance()->m_PDS.m_ProgressionSC))
				{
					ProfileData::GetInstance()->m_PDS.m_ProgressionCH = GameDesc::m_CurrentChapter;
					ProfileData::GetInstance()->m_PDS.m_ProgressionSC = GameDesc::m_CurrentScene;
				}
			}
			else// game finished !
			{
				ProfileData::GetInstance()->m_PDS.m_ProgressionCH = 0xff;
				ProfileData::GetInstance()->m_PDS.m_ProgressionSC = 0xff;
			}

			if(GameDesc::m_CurrentChapter != 0xffffffff)
			{
				// autosave the next scene or this one if no more scene
				ProfileData::GetInstance()->m_PDS.m_Saves[0].Save(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene, GameDesc::m_CurrentDialog, GameDesc::m_CurrentSounds);
				ProfileData::GetInstance()->WriteSave();
			}
		}
	}
	if(GameDesc::m_CurrentChapter != 0xffffffff)
	{
		if(m_MenuPanel && m_ScenePanel && m_ScenePanel->GetPlayState() != ScenePanel::PS_GAME)
		{
			m_MenuPanel->UseQuickMenu((m_ScenePanel->GetPlayState() == ScenePanel::PS_QUICK_MENU));
			m_MenuPanel->Update(dt);
		}
	}
}

void GamePanel::PreRender()
{
	if(m_ScenePanel)
		m_ScenePanel->PreRender();
	if(GameDesc::m_CurrentChapter != 0xffffffff)
	{
		if(m_MenuPanel && m_ScenePanel && m_ScenePanel->GetPlayState() != ScenePanel::PS_GAME)
			m_MenuPanel->PreRender();
	}

}

void GamePanel::Render()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor color = COLOR_BLACK;
	fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);

	if(m_ScenePanel)
		m_ScenePanel->Render();
	if(GameDesc::m_CurrentChapter != 0xffffffff)
	{
		if(m_MenuPanel && m_ScenePanel && m_ScenePanel->GetPlayState() != ScenePanel::PS_GAME)
			m_MenuPanel->Render();
	}

	if(IsFading())
	{
		IColor fadeColor = COLOR_BLACK;
		IColor transparent = COLOR_TRANSPARENT;
		//fade in
		float ratio = m_Fader.GetRatio();
		IColor color = MathUtils::GetInterpolatedColor(fadeColor, transparent, ratio);
		fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);
	}
}

bool GamePanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if(GameDesc::m_CurrentChapter != 0xffffffff)
	{
		if(m_MenuPanel && m_ScenePanel && m_ScenePanel->GetPlayState() != ScenePanel::PS_GAME)
		{
			bool ret = m_MenuPanel->OnEvent(event);
			if(m_MenuPanel->IsRequestingQuit())
			{
				m_TransitToChildPanelIndex = 1;// quit return to main menu
				m_MenuPanel->RequestQuit(false);// reset the value
				//Engine::GetInstance()->GetAudioDevice()->StopAllSounds();

				// autosave where we are
				if(GameDesc::m_CurrentChapter != 0xffffffff)
				{
					ProfileData::GetInstance()->m_PDS.m_Saves[0].Save(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene, GameDesc::m_CurrentDialog, GameDesc::m_CurrentSounds);
					ProfileData::GetInstance()->WriteSave();
				}
				return true;
			}

			if(ret)
				return true;
			else
			{
				m_ScenePanel->SetPlayState(ScenePanel::PS_GAME);
				return true;
			}
		}
	}
	else
	{
		if (event.EventType == EET_KEY_INPUT_EVENT)
		{
			//if(m_SelectionFadeTimer == 0.0f)
			if(event.KeyInput.PressedDown)
			{
				ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
				if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
				{
					m_TransitToChildPanelIndex = 1;// quit return to main menu
					m_MenuPanel->RequestQuit(false);// reset the value

					return true;
				}
			}
		}
	}

	if(m_ScenePanel)
	{
		if(m_ScenePanel->OnEvent(event))
			return true;
	}

	return false;
}

#undef PANEL_FADETIME
