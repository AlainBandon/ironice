#ifndef GLOSSARY_H
#define GLOSSARY_H

#include "utils.h"
#include "engine.h"

class GlossaryDescription
{
public:
	GlossaryDescription() {for(u32 i = 0; i < Engine::LS_MAX; ++i) m_Header[i] = m_Content[i] = L"";}
	~GlossaryDescription() {}

	const core::stringw& GetHeader(Engine::LanguageSettings language) const {return m_Header[language];}
	void SetHeader(Engine::LanguageSettings language, const core::stringw& header) {m_Header[language] = header;}

	const core::stringw& GetContent(Engine::LanguageSettings language) const {return m_Content[language];}
	void SetContent(Engine::LanguageSettings language, const core::stringw& content) {m_Content[language] = content;}

protected:
	core::stringw m_Header[Engine::LS_MAX];
	core::stringw m_Content[Engine::LS_MAX];
};

class GlossaryEntry
{
public:
	enum GlossaryEntryType{GET_UNDEFINED, GET_VOCABULARY, GET_PROFILE, GET_MAX};

	virtual ~GlossaryEntry() {}

	void SetEntryWord(Engine::LanguageSettings language, const core::stringw& word) {m_EntryWord[language] = word;}
	const core::stringw& GetEntryWord(Engine::LanguageSettings language) const {return m_EntryWord[language];}

	void SetKeyWords(Engine::LanguageSettings language, const core::stringw& word) {m_KeyWords[language] = word;}
	const core::stringw& GetKeyWords(Engine::LanguageSettings language) {return m_KeyWords[language];}

	GlossaryEntryType GetEntryType() const {return m_Type;}

	TArray<GlossaryDescription>& GetContentList() {return m_ContentList;}
	void AddContent(const GlossaryDescription& content) {m_ContentList.push_back(content);}

protected:
	GlossaryEntry(u32 id, GlossaryEntryType type) : m_ID(id), m_Type(type) {}

	u32 m_ID;
	GlossaryEntryType m_Type;

	core::stringw m_EntryWord[Engine::LS_MAX];
	core::stringw m_KeyWords[Engine::LS_MAX];


	TArray<GlossaryDescription> m_ContentList;
};

class GlossaryEntryVocabulary : public GlossaryEntry
{
public:
	GlossaryEntryVocabulary(u32 id) : GlossaryEntry(id, GET_VOCABULARY) {}
	virtual ~GlossaryEntryVocabulary() {}

protected:
};

class GlossaryEntryProfile : public GlossaryEntry
{
public:
	GlossaryEntryProfile(u32 id) : GlossaryEntry(id, GET_PROFILE), m_Image(NULL) {}
	virtual ~GlossaryEntryProfile() {SAFE_UNLOAD(m_Image);}


	void SetImage(Texture* image) {m_Image = image;}
	Texture* GetImage() const {return m_Image;}

	void SetImageDesc(Engine::LanguageSettings language, const core::stringw& desc) {m_ImageDesc[language] = desc;}
	const core::stringw& GetImageDesc(Engine::LanguageSettings language) const {return m_ImageDesc[language];}

protected:
	Texture* m_Image;
	core::stringw m_ImageDesc[Engine::LS_MAX];
};


class Glossary
{
public:
	Glossary() {}
	~Glossary() { for(u32 i = 0; i < m_Entries.size(); ++i) SafeDelete(m_Entries[i]); m_Entries.clear();}


	TArray<GlossaryEntry*>& GetEntries() {return m_Entries;}
	void AddContent(GlossaryEntry* const entry) {m_Entries.push_back(entry);}

	TArray<GlossaryEntry*> GetEntriesBySearch(Engine::LanguageSettings language, const core::stringw& search) const;

	bool ReadFromFile(const char* path);
#ifndef IRON_ICE_FINAL
	bool ReadFromFileTxt(const char* path, Engine::LanguageSettings language = Engine::LS_FRENCH);
	bool WriteToFile(const char* path);
#endif

private:
	TArray<GlossaryEntry*> m_Entries;
};

#endif 
