﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

#define GLOSSARY_NB_ENTRIES_DISPLAYED 8
#define GLOSSARY_ENTRY_MAX_LETTERS_DISPLAYED 12
#define GLOSSARY_ENTRY_DESC_LINES_DISPLAYED 11
#define GLOSSARY_SEARCH_BOX_MAXLETTERS_DISPLAYED 39

#define COMPUTE_GLOSSARY_HEADER_COLOR(alpha) COMPUTE_THEME_COLOR_FONT_SELECTION(alpha)

enum StringDesc {SD_GLOSSARY = Engine::SFTID_BonusSubMenuPanel, SD_GALLERY, SD_DRAGONANDWEED, SD_CREDITS, SD_MAX};

// BonusSubMenuPanel
BonusSubMenuPanel::BonusSubMenuPanel() : IPanel()
, m_ActiveBG(NULL)
, m_SearchBar(NULL)
, m_CurrentSelection(0)
, m_CurrentGlossaryEntryVisibilityStart(0)
, m_CurrentGlossaryEntry(0)
, m_CurrentGlossaryContentVisibilityStart(0)
, m_CurrentGlossaryEntryActive(false)
, m_GlossarySearchActive(false)
, m_CurrentGlossaryContentLineNb(0)
, m_CurrentSubMenuActive(SMAT_BOTTOM)
, m_PreviousSubMenuActive(SMAT_BOTTOM)
, m_CurrentSelectionValidatedFader(0.2f)
, m_PopupPanel(NULL)
, m_CurrentGlossarySearch(L"")
, m_CurrentCachedGlossaryContent(L"")
, m_Selec(NULL)
, m_PreviewGallery(NULL)
, m_PreviewDandW(NULL)
, m_PreviewCredits(NULL)
{
	for(int i = 0; i < 3; ++i)
		m_Font[i] = NULL;

	for(int i = 0; i < 2; ++i)
		m_Arrows[i] = NULL;

	bool useRT = true;
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	if(useRT && driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		m_RenderTarget = driver->addRenderTargetTexture(Engine::GetInstance()->m_RenderSize, "RTT_Bonus", video::ECF_A8R8G8B8);
	}
	else
	{
		m_RenderTarget = NULL;
	}

	Engine* engine = Engine::GetInstance();
	if(!m_Glossary.ReadFromFile(engine->GetGamePath("data/glossary/glossary.gls").c_str()))
	{
#ifndef IRON_ICE_FINAL
		if(!m_Glossary.ReadFromFileTxt(engine->GetGamePath("data/glossary/glossary_fr.txt").c_str(), Engine::LS_FRENCH))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/glossary/glossary_fr.txt.");
		if(!m_Glossary.ReadFromFileTxt(engine->GetGamePath("data/glossary/glossary_en.txt").c_str(), Engine::LS_ENGLISH))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/glossary/glossary_en.txt.");
		if(!m_Glossary.ReadFromFileTxt(engine->GetGamePath("data/glossary/glossary_de.txt").c_str(), Engine::LS_GERMAN))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/glossary/glossary_de.txt.");
		if(!m_Glossary.ReadFromFileTxt(engine->GetGamePath("data/glossary/glossary_es.txt").c_str(), Engine::LS_SPANISH))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/glossary/glossary_es.txt.");
		if(!m_Glossary.WriteToFile(engine->GetGamePath("data/glossary/glossary.gls").c_str()))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible d'écrire le fichier data/glossary/glossary.gls.");
#else
		engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/glossary/glossary.gls.");
		assert(0);
#endif
	}
}

BonusSubMenuPanel::~BonusSubMenuPanel()
{
	UnloadPanel();
}

void BonusSubMenuPanel::LoadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	SAFE_LOAD_IMAGE(driver, m_ActiveBG, engine->GetGamePath("system/02_Active_BG.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_SearchBar, engine->GetGamePath("system/02_SearchBox.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_Arrows[0], engine->GetGamePath("system/02_Chapter_L_arrow.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_Arrows[1], engine->GetGamePath("system/02_Chapter_R_arrow.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_Selec, engine->GetGamePath("system/00_selection.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_PreviewGallery, engine->GetGamePath("system/menu_scn/0001/preview.jpg"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_PreviewDandW, engine->GetGamePath("system/menu_scn/0002/preview.jpg"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_PreviewCredits, engine->GetGamePath("system/menu_scn/0003/preview.jpg"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[3] = {42, 48, 60};
		for(int i = 0; i < 3; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA), size/6);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	if(!m_PopupPanel)
	{
		m_PopupPanel = new PopupPanel(m_RenderTarget || USE_PREMULTIPLIED_ALPHA);
		m_PopupPanel->LoadPanel();
	}

	m_CurrentEntries = m_Glossary.GetEntriesBySearch(Engine::GetInstance()->GetLanguage(), m_CurrentGlossarySearch);

	OnResize();

	//StartFade(PANEL_FADETIME);

	IPanel::LoadPanel();
}

void BonusSubMenuPanel::UnloadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_ActiveBG);

	SAFE_UNLOAD(m_SearchBar);

	SAFE_UNLOAD(m_Arrows[0]);
	SAFE_UNLOAD(m_Arrows[1]);

	SAFE_UNLOAD(m_Selec);

	SAFE_UNLOAD(m_PreviewGallery);
	SAFE_UNLOAD(m_PreviewDandW);
	SAFE_UNLOAD(m_PreviewCredits);


	for(int i = 0; i < 3; ++i)
		SAFE_UNLOAD(m_Font[i]);

	if(m_PopupPanel)
		m_PopupPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void BonusSubMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	const u32 sizes[3] = {42, 48, 60};
	if(m_Font[0])
	{
		for(int i = 0; i < 3; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			m_Font[i]->setSize(size, size/6);
		}
	}

	{
		Dimension2 size = (m_ActiveBG)? m_ActiveBG->getSize() : Dimension2(1161, 834);
		size.Width += 100;
		size.Height += 60;
		Position2 pos = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)size.Height*innerScale)) + innerOffset;
		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)size.Height*innerScale));
		m_SubMenusArea[SMAT_CENTER].Area = Rectangle2(pos-cornerOffset, pos);
		m_SubMenusArea[SMAT_CENTER].IsSelected = false;

		pos.X -= cornerOffset.X;
		m_SubMenusArea[SMAT_LEFT].Area = Rectangle2(innerOffset, pos);
		m_SubMenusArea[SMAT_LEFT].IsSelected = false;

		pos.X = innerOffset.X;
		cornerOffset = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)originalSize.Height*innerScale))+innerOffset;
		m_SubMenusArea[SMAT_BOTTOM].Area = Rectangle2(pos, cornerOffset);
		m_SubMenusArea[SMAT_BOTTOM].IsSelected = false;

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	{

		Dimension2 size(originalSize.Width>>2, sizes[2]);
		Position2 pos = Position2((s32)((f32)100*innerScale), (s32)((f32)(originalSize.Height>>2)*innerScale)) + innerOffset;

		u32 dy = (u32)(((originalSize.Height>>1)/(f32)SBT_MAX)*innerScale);
		Position2 offset(0, 0);

		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)dy*innerScale));
		for(int i = 0; i < SBT_MAX; ++i, offset.Y += dy)
		{
			if(m_Font[2])
			{
				cornerOffset.X = m_Font[2]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_GLOSSARY + i)).Width;
			}
			m_ButtonsArea[i].Area = Rectangle2(pos + offset, pos + offset + cornerOffset);
			m_ButtonsArea[i].IsSelected = false;
		}

		m_ButtonsArea[m_CurrentSelection].IsSelected = true;
	}

	if(m_RenderTarget && renderSize != m_RenderTarget->getSize())
		m_RenderTarget = driver->addRenderTargetTexture(renderSize, "RTT_Bonus", video::ECF_A8R8G8B8);

	if(m_PopupPanel)
	{
		m_PopupPanel->OnResize();
	}
}

void BonusSubMenuPanel::Update(float dt)
{
	IPanel::Update(dt);

	m_CurrentSelectionValidatedFader.Tick(dt);

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Update(dt);
		}
	}
}

void BonusSubMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor off = IsFading()?
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0), 4.0f*m_Fader.GetRatio()-3.0f)
		: IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0);
	IColor on = COMPUTE_THEME_COLOR_FONT_GLOW(0);
	float ratio = m_CurrentSelectionValidatedFader.GetRealRatio();
	IColor onToOff = MathUtils::GetInterpolatedColor(off, on, ratio);
	IColor offToOn = MathUtils::GetInterpolatedColor(on, off, ratio);
	Rectangle2 rect(innerOffset, Dimension2((u32)(innerScale*originalSize.Width), (u32)(innerScale*originalSize.Height)));
	Rectangle2 rects[3] =
	{
		m_SubMenusArea[SMAT_LEFT].Area,
		m_SubMenusArea[SMAT_CENTER].Area,
		m_SubMenusArea[SMAT_BOTTOM].Area
	};
	if(m_CurrentSubMenuActive == SMAT_LEFT)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 1011
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_CENTER) // 0111 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1011
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, off, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_CENTER)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 0111
		{
			fill2DRect(driver, rects[0], off, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], off, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_BOTTOM)// 1111
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1010 -> 1111
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_POPUP)// 0000
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 0000
		{
			fill2DRect(driver, rects[0], off, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_BOTTOM) // 1111 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else
		{
			fill2DRect(driver, rects[0], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
			fill2DRect(driver, rects[1], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
		}
		fill2DRect(driver, rects[2], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
	}
	else
	{
		assert(false);
		fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
	}
}


void BonusSubMenuPanel::RenderSceneBased(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	Texture* tex = NULL;
	switch(m_CurrentSelection)
	{
	case SBT_GALLERY:
		tex = m_PreviewGallery;
		break;
	case SBT_DRAGONANDWEED:
		tex = m_PreviewDandW;
		break;
	case SBT_CREDITS:
		tex = m_PreviewCredits;
		break;
	default:
		break;
	}
	if(tex && m_ActiveBG)
	{
		ProfileData* profil = ProfileData::GetInstance();
		SaveData& save = profil->GetSave(PROFILE_AUTOSAVE_SLOT);

		const u32 margin = 30;

		IColor color = COMPUTE_THEME_COLOR(isCenterActive? 255:PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);
		IColor colorTex = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		float scale2 = (float)size.Width*scale*0.9f/originalSize.Width;
		Position2 pos((s32)((((f32)originalSize.Width-0.95f*size.Width-100)*scale + offset.X)*innerScale), (s32)((((f32)60+0.05f*size.Height)*scale + offset.Y)*innerScale));
		if(isCenterActive)
		{
			float scale3 = scale2*innerScale;
			Position2 pos3 = pos + Position2((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f)) + innerOffset;

			if(m_Selec && isCenterActive)
			{
				float ratio = core::abs_(cosf(0.5f*core::PI*m_Time));
				IColor tint = m_ActiveValidationArea.IsSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(0), ratio);
				Rectangle2 sourceRect(Position2(0,0), m_Selec->getSize());
				float scaleX = (float)(s32)(scale3*originalSize.Width)/(m_Selec->getSize().Width - (18<<1));
				float scaleY = (float)(s32)(scale3*originalSize.Height)/(m_Selec->getSize().Height - (18<<1));

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos3.X,	(f32)pos3.Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				draw2DImage(driver, m_Selec, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
			}

			Position2 halfCorner((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f));
			m_ActiveValidationArea.Area = Rectangle2(pos3-halfCorner, pos3+halfCorner);
		}
		//else
		{
			Rectangle2 sourceRect(Position2(0,0), tex->getSize());
			scale2 = (float)size.Width*scale*innerScale*0.9f/tex->getSize().Width;
			pos += Position2((s32)(scale2*tex->getSize().Width*0.5f), (s32)(scale2*tex->getSize().Height*0.5f)) + innerOffset;

			core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X,	(f32)pos.Y, 0.0f)) *
				core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
				core::matrix4().setScale(core::vector3df((f32)scale2, (f32)scale2, 0.0f));

			draw2DImage(driver, tex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
		}

		// draw the description of the preview
		core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_GLOSSARY + m_CurrentSelection));
		if(m_Font[2])
		{
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width>>1)-100)*scale + offset.X)*innerScale)/* - (longestStrSize>>1)*/),
				(s32)((((f32)((size.Height)+60)*scale + offset.Y)*innerScale)));
			pos += innerOffset;

			pos.Y -= (u32)((float)margin*2 * innerScale * scale);

			pos.Y -= (u32)(scale * m_Font[2]->getSize()) + (u32)((float)20 * innerScale * scale);
			m_Font[2]->draw(str, Rectangle2(pos, D2Zero), color, scale, true, false);
		}
	}
}

void BonusSubMenuPanel::RenderGlossary(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_ActiveBG)
	{
		ProfileData* profil = ProfileData::GetInstance();

		const u32 margin = 30;
		const u32 realMargin = (u32)(scale*innerScale*margin);

		IColor color = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		float scale2 = (float)size.Width*scale*0.9f/originalSize.Width;
		Position2 pos((s32)((((f32)originalSize.Width-0.95f*size.Width-100)*scale + offset.X)*innerScale), (s32)((((f32)60+0.05f*size.Height)*scale + offset.Y)*innerScale));
		if(isCenterActive)
		{
			float scale3 = scale2*innerScale;
			Position2 pos3 = pos + Position2((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f)) + innerOffset;



			Position2 halfCorner((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f));
			m_ActiveValidationArea.Area = Rectangle2(pos3-halfCorner, pos3+halfCorner);
		}
		else
		{

		}

		// default scrollbar zones : in case we do not display them
		m_LineArea[11].Area = Rectangle2(0, 0, 0, 0);
		m_LineArea[12].Area = Rectangle2(0, 0, 0, 0);
		if(m_Font[0] && m_Font[2])
		{
			Position2 pos = Position2((s32)(((((f32)originalSize.Width-size.Width-100)*scale + offset.X)*innerScale)),
				(s32)((((f32)(60)*scale + offset.X)*innerScale)));
			pos += innerOffset;

			u32 h = (u32)(scale * innerScale * (size.Height - (margin<<1)));
			u32 w = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			pos.X += realMargin;
			pos.Y += realMargin;

			Position2 glossaryStart(pos);

			u32 dy = h/GLOSSARY_NB_ENTRIES_DISPLAYED;
			u32 dx = w/4;

			m_LineArea[10].Area = Rectangle2(pos, Dimension2(dx, h));
			if(m_LineArea[10].IsSelected || !m_CurrentGlossaryEntryActive)
			{
				IColor bgColor(64, 0, 0, 0);
				fill2DRect(driver, m_LineArea[10].Area, bgColor, bgColor, bgColor, bgColor, true);
			}

			pos.Y += dy>>1;
			pos.X += dx>>1;

			IColor color = IColor(isCenterActive? 200:PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);
			//if(m_CurrentEntries.size() > GLOSSARY_NB_ENTRIES_DISPLAYED)
			{
				m_LineArea[11].Area = Rectangle2(Position2(pos.X + (dx>>1), pos.Y - (dy>>1)),
					Dimension2((u32)((float)16*innerScale*scale), h));
				m_ScrollBar.SetDimensions(m_CurrentEntries.size(), m_CurrentGlossaryEntryVisibilityStart, GLOSSARY_NB_ENTRIES_DISPLAYED);
				m_ScrollBar.Render(m_LineArea[11].Area, color);
			}

			for(u32 i = m_CurrentGlossaryEntryVisibilityStart; i < m_CurrentGlossaryEntryVisibilityStart+GLOSSARY_NB_ENTRIES_DISPLAYED; ++i)
			{
				u32 alpha = isCenterActive? ((i==m_CurrentGlossaryEntry || m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].IsSelected)? 255:180) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				if(i < m_CurrentEntries.size())
				{
					m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].Area = Rectangle2(Position2(pos.X - (dx>>1), pos.Y - (dy>>1)), Dimension2(dx, dy));

					if(i==m_CurrentGlossaryEntry)
					{
						const bool displayFullWord = false;
// 						const core::stringw& str = displayFullWord? m_CurrentEntries[i]->GetEntryWord(engine->GetLanguage()) :
// 							m_CurrentEntries[i]->GetEntryWord(engine->GetLanguage()).subString(0, GLOSSARY_ENTRY_MAX_LETTERS_DISPLAYED);
						const core::stringw& str = m_CurrentEntries[i]->GetEntryWord(engine->GetLanguage());
						m_Font[2]->drawGauss(str, m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].Area, tint, scale, true, true);
						m_Font[2]->draw(str, m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].Area, color, scale, true, true,
							displayFullWord? IColor(isCenterActive?128:PANEL_MENU_NOT_ACTIVE_TRANSPARENCY, 0, 0, 0) : COLOR_TRANSPARENT);
					}
					else
					{
//						const core::stringw& str = m_CurrentEntries[i]->GetEntryWord(engine->GetLanguage()).subString(0, GLOSSARY_ENTRY_MAX_LETTERS_DISPLAYED);
						const core::stringw& str = m_CurrentEntries[i]->GetEntryWord(engine->GetLanguage());
						if(m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].IsSelected)
							m_Font[0]->drawGauss(str, m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].Area, tint, scale, true, true);
						m_Font[0]->draw(str, m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].Area, color, scale, true, true);
					}
				}
				else
				{
					m_LineArea[i-m_CurrentGlossaryEntryVisibilityStart].Area = Rectangle2(0, 0, 0, 0);
				}

				pos.Y += dy;
			}

			pos = glossaryStart;
			pos.X += dx + realMargin;

			u32 lineHeight = (u32)(scale * m_Font[0]->getDimension(L" ").Height);

			{
				u32 ddy = (u32)(scale * m_Font[2]->getSize()) + (u32)((float)50 * innerScale * scale);
				m_LineArea[8].Area = Rectangle2(Position2(pos.X - (realMargin>>1), pos.Y),
					Dimension2(dx*3, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED*lineHeight + ddy));

				if(m_LineArea[8].IsSelected || m_CurrentGlossaryEntryActive)
				{
					IColor bgColor(64, 0, 0, 0);
					fill2DRect(driver, m_LineArea[8].Area, bgColor, bgColor, bgColor, bgColor, true);
				}
			}

			// draw entry content
			if(!m_CurrentEntries.empty())
			{
				u32 alpha = isCenterActive? (m_LineArea[8].IsSelected? 255 : 200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor color = COMPUTE_THEME_COLOR(alpha);
				IColor colorTex = IColor(alpha,255,255,255);

				GlossaryEntry* entry = m_CurrentEntries[m_CurrentGlossaryEntry];

				if(isCenterActive && (m_LineArea[8].IsSelected || m_CurrentGlossaryEntryActive))
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
					m_Font[2]->drawGauss(entry->GetEntryWord(engine->GetLanguage()), Rectangle2(Position2(pos.X + ((dx*3-(realMargin<<1))>>1), pos.Y), D2Zero), tint, scale, true, false);
				}
				m_Font[2]->draw(entry->GetEntryWord(engine->GetLanguage()),
					Rectangle2(Position2(pos.X + ((dx*3-(realMargin<<1))>>1), pos.Y), D2Zero), color, scale, true, false);

				pos.Y += (u32)(scale * m_Font[2]->getSize()) + (u32)((float)50 * innerScale * scale);

				// if a profile entry : draw the image first if any
				Texture* image = NULL;
				u32 profileImageHeight = 0;
				if(entry->GetEntryType() == GlossaryEntry::GET_PROFILE)
				{
					image = ((GlossaryEntryProfile*)entry)->GetImage();
					if(image)
					{
						Dimension2 size = image->getSize();
						float scale3 = (float)dx/size.Width;

						u32 realImgHeight = lineHeight*m_CurrentGlossaryContentVisibilityStart;
						profileImageHeight = (u32)(scale3*size.Height);
						if(realImgHeight < profileImageHeight)
						{
							Rectangle2 sourceRect(0, (s32)((float)realImgHeight/scale3), (s32)size.Width, (s32)size.Height);
							Position2 pos3(pos.X - (realMargin<<1) + 2*dx + (dx>>1), pos.Y + (s32)(scale3*0.5f*sourceRect.getHeight()));

							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos3.X), (f32)(pos3.Y), 0.0f)) *
								core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df(scale3, scale3, 0.0f));

							draw2DImage(driver, image, sourceRect, mat, true, colorTex, true, true);

							core::stringw desc = ((GlossaryEntryProfile*)entry)->GetImageDesc(engine->GetLanguage());
							if(desc != L"")
							{
								FormatString(desc, desc, m_Font[0], Dimension2(dx, profileImageHeight));
								TArray<u32> endlOffsets;
								GetStringEndl(desc, endlOffsets);
								profileImageHeight += (endlOffsets.size()+1)*lineHeight;
								pos3.Y += (s32)(scale3*0.5f*sourceRect.getHeight());
								if(realImgHeight < profileImageHeight)
									m_Font[0]->draw(desc, Rectangle2(pos3, D2Zero), color, scale, true, false);
							}
						}
					}
				}

				core::stringw str;
				TArray<GlossaryDescription>& list = entry->GetContentList();
				TArray<CGUIFreetypeFont::ColorElement> colorElts;
				IColor colorHeader = COMPUTE_GLOSSARY_HEADER_COLOR(alpha);
				for(u32 j = 0; j < list.size(); ++j)
				{
					const GlossaryDescription& desc = list[j];
					{
						const core::stringw& temp = desc.GetHeader(engine->GetLanguage());
						str += temp;
						colorElts.push_back(CGUIFreetypeFont::ColorElement(colorHeader, temp.size()));
					}
					{
						const core::stringw& temp = desc.GetContent(engine->GetLanguage());
						str += temp;
						colorElts.push_back(CGUIFreetypeFont::ColorElement(color, temp.size()));
					}
					//str += L"\n";
				}
				if(m_CurrentCachedGlossaryContent == L"")// cache obsolete : rebuild it
				{
					m_CurrentCachedGlossaryContent = str;
					if(entry->GetEntryType() == GlossaryEntry::GET_PROFILE && image)
						FormatString(m_CurrentCachedGlossaryContent, m_CurrentCachedGlossaryContent, m_Font[0], Dimension2(dx * 2 - (realMargin<<1), profileImageHeight), true);
					FormatString(m_CurrentCachedGlossaryContent, m_CurrentCachedGlossaryContent, m_Font[0], Dimension2(dx * 3 - (realMargin<<1), h));
				}
				u32 start = 0, end = 0, nbTotalLines = 0;
				core::stringw finalStr = SubStringLines(m_CurrentCachedGlossaryContent, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED, &start, &end, &nbTotalLines);
				m_CurrentGlossaryContentLineNb = nbTotalLines;
				// must recompute the color elements
				u32 cursor = 0;
				for(u32 j = 0; j < colorElts.size(); ++j)
				{
					if(cursor <= start)
					{
						cursor += colorElts[j].Size;
						if(cursor <= start)
							colorElts[j].Size = 0;
						else
						{
							colorElts[j].Size = cursor - start;
							break;
						}
					}
					else
						break;
				}
				m_Font[0]->draw(finalStr, Rectangle2(pos, D2Zero), colorElts, scale, false, false);

				if(m_CurrentGlossaryContentLineNb > GLOSSARY_ENTRY_DESC_LINES_DISPLAYED)
				{
					m_LineArea[12].Area = Rectangle2(Position2(pos.X + dx*3 - realMargin, pos.Y),
						Dimension2((u32)((float)16*innerScale*scale), GLOSSARY_ENTRY_DESC_LINES_DISPLAYED * lineHeight));
					m_ScrollBar.SetDimensions(m_CurrentGlossaryContentLineNb, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED);
					m_ScrollBar.Render(m_LineArea[12].Area, colorTex);
				}
			}

			// draw the search box
			pos.Y = (s32)((((f32)(60 + size.Height)*scale + offset.X)*innerScale)) + innerOffset.Y - realMargin;
			if(m_SearchBar)
			{
				u32 alpha = (isCenterActive && (m_LineArea[9].IsSelected || m_GlossarySearchActive))? 200:100;
				IColor colorTex = IColor(alpha,255,255,255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				Dimension2 size = m_SearchBar->getSize();
				float scale3 = (float)(dx*3 - (realMargin))/size.Width;

				Rectangle2 sourceRect(P2Zero, size);

				u32 ddx = (s32)(scale3*0.5f*sourceRect.getWidth());
				u32 ddy = (s32)(scale3*0.5f*sourceRect.getHeight());
				pos.X += ddx;
				pos.Y -= ddy;
				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X , (f32)pos.Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df(scale3, scale3, 0.0f));

				draw2DImage(driver, m_SearchBar, sourceRect, mat, true, colorTex, true, true);

				m_LineArea[9].Area = Rectangle2(pos.X - ddx, pos.Y - ddy, pos.X + ddx, pos.Y + ddy);

				Rectangle2 textArea(m_LineArea[9].Area.UpperLeftCorner, m_LineArea[9].Area.LowerRightCorner + Position2(-(s32)(ddy*3), 0));
				//u32 start = (u32)core::max_((s32)m_CurrentGlossarySearch.size() - GLOSSARY_SEARCH_BOX_MAXLETTERS_DISPLAYED, 0);
				u32 start = m_CurrentGlossarySearch.size();
				u32 textWidth = 0;
				while(start > 0)
				{
					--start;
					textWidth += m_Font[0]->getWidthFromCharacter(m_CurrentGlossarySearch[start]);
					if(textWidth > (u32)textArea.getWidth())
					{
						++start;
						break;
					}
				}

				m_Font[0]->draw(m_CurrentGlossarySearch.subString(start, m_CurrentGlossarySearch.size()-start), textArea, color, scale, true, true);

			}
		}
	}
}

void BonusSubMenuPanel::_Render(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	RenderFader(scale, offset);

	// draw the left submenu
	if(m_Font[2] && m_Font[3])
	{
		s32 x = (u32)((originalSize.Width/(f32)SBT_MAX)*innerScale);

		for(int i = 0; i < SBT_MAX; ++i)
		{
			core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_GLOSSARY + i));
			bool isSelected = m_ButtonsArea[i].IsSelected;
			u32 alpha = (m_CurrentSubMenuActive != SMAT_CENTER)? 255 : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
			IColor color = isSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : COMPUTE_THEME_COLOR((m_CurrentSubMenuActive != SMAT_CENTER)? alpha : 16);
			const Rectangle2& area = m_ButtonsArea[i].Area;
			Rectangle2 rect(Position2((s32)((float)(area.UpperLeftCorner.X-innerOffset.X)*scale+(float)offset.X*innerScale+innerOffset.X),
				(s32)((float)(area.UpperLeftCorner.Y-innerOffset.Y)*scale+(float)offset.Y*innerScale+innerOffset.Y)),
				Dimension2((u32)((float)area.getWidth()*scale), (u32)((float)area.getHeight()*scale)));
			//if(isSelected)
			{
				float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
				(isSelected?m_Font[2]:m_Font[1])->drawGauss(str, rect, tint, scale, false, false);
			}
			(isSelected?m_Font[2]:m_Font[1])->draw(str, rect, color, scale, false, false);
		}
	}

	// center part
	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	// draw the active BG at the center
	if(m_ActiveBG)
	{
		IColor tint = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width-(size.Width>>1)-100)*innerScale),
			(s32)((f32)((size.Height>>1)+60)*innerScale));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X*scale+(f32)offset.X*innerScale + innerOffset.X, (f32)pos.Y*scale+(f32)offset.Y*innerScale + innerOffset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

		draw2DImage(driver, m_ActiveBG, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	}

	switch(m_CurrentSelection)
	{
	case SBT_GLOSSARY:
		RenderGlossary(scale, offset);
		break;
	case SBT_GALLERY:
	case SBT_DRAGONANDWEED:
	case SBT_CREDITS:
		RenderSceneBased(scale, offset);
		break;
	default:
		assert(0);
		break;
	}

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Render();
		}
	}
}

void BonusSubMenuPanel::PreRender()
{
	// render here only if we have a render target : we render in the render target
	if(m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		// set render target texture
		IColor color = COLOR_TRANSPARENT;
		color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(m_RenderTarget, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		_Render();

		// set back old render target
		// The buffer might have been distorted, so clear it
		color = COLOR_TRANSPARENT;
		if(USE_PREMULTIPLIED_ALPHA)
			color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(0, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

void BonusSubMenuPanel::Render()
{
	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)),
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_Render(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();

		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)*Engine::GetInstance()->m_InnerScale), 0);

		IColor tint = COLOR_WHITE;

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((Engine::GetInstance()->m_RenderSize.Width>>1),
			(Engine::GetInstance()->m_RenderSize.Height>>1));

		if(m_CurrentSelectionValidatedFader.IsFading())
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// other to bottom fade
				ratio = 0.75f * ratio + m_CurrentSelectionValidatedFader.GetRealRatio()*0.25f;// 1 to 0.75
			else if(m_PreviousSubMenuActive == SMAT_BOTTOM)// bottom to other fade
				ratio = 0.75f * ratio + (1.0f - m_CurrentSelectionValidatedFader.GetRealRatio())*0.25f;// 0.75 to 1
		}
		else
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// bottom
				ratio = 0.75f*ratio;// 0.75
		}

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

bool BonusSubMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if(m_PopupPanel)
	{
		if(m_PopupPanel->OnEvent(event))
		{
			if(!m_PopupPanel->IsVisible())// this means that the popup just closed
			{
				{
					if(m_PopupPanel->GetResult() == PopupPanel::PRT_YES)
					{
						Engine::GetInstance()->RequestResetDevice();
					}
				}
				ChangeSubMenuActive(m_PreviousSubMenuActive);
			}

			return true;
		}
	}

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Engine* engine = Engine::GetInstance();
			const Dimension2& renderSize = engine->m_RenderSize;
			const Dimension2& originalSize = engine->m_OriginalSize;
			float innerScale = engine->m_InnerScale;
			const Position2& innerOffset = engine->m_InnerOffset;

			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);

			// check which sub menu is active
			if(!m_CurrentSelectionValidatedFader.IsFading())
			{
				u32 oldSubMenuActive = m_CurrentSubMenuActive;
				for(int i = 0; i < SMAT_MAX; ++i)
				{
					m_SubMenusArea[i].IsSelected = false;
					if(m_SubMenusArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentSubMenuActive = i;
					}
				}
				m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
				if(oldSubMenuActive != m_CurrentSubMenuActive)
				{
					m_PreviousSubMenuActive = oldSubMenuActive;
					m_CurrentSelectionValidatedFader.StartFade();
				}
			}

			// Left menu
			u32 oldSelection = m_CurrentSelection;
			{
				for(int i = 0; i < SBT_MAX; ++i)
				{
					m_ButtonsArea[i].IsSelected = false;
					if(m_ButtonsArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentSelection = i;
					}
				}
				m_ButtonsArea[m_CurrentSelection].IsSelected = true;
			}
			bool hasSelectionChanged = (oldSelection != m_CurrentSelection);

			// center area
			m_ActiveValidationArea.IsSelected = m_ActiveValidationArea.Area.isPointInside(mousePos);

			if(m_CurrentSelection == SBT_GLOSSARY && event.MouseInput.isLeftPressed())
			{
				if(m_LineArea[11].IsSelected)
				{
					float value = (float)(mousePos.Y - m_LineArea[11].Area.UpperLeftCorner.Y)/
						(float)(m_LineArea[11].Area.getHeight());
					if(value >= 0.0f && value <= 1.0f)
					{
						// make the visibility start to have center of the visibility centered on the value
						m_CurrentGlossaryEntryVisibilityStart = (u32)core::max_(0, (s32)(value*m_CurrentEntries.size())-(GLOSSARY_NB_ENTRIES_DISPLAYED>>1));
						m_CurrentGlossaryEntryVisibilityStart =
							m_ScrollBar.SetDimensions(m_CurrentEntries.size(), m_CurrentGlossaryEntryVisibilityStart, GLOSSARY_NB_ENTRIES_DISPLAYED);
					}
				}
				else if(m_LineArea[12].IsSelected)
				{
					float value = (float)(mousePos.Y - m_LineArea[12].Area.UpperLeftCorner.Y)/
						(float)(m_LineArea[12].Area.getHeight());
					if(value >= 0.0f && value <= 1.0f)
					{
						// make the visibility start to have center of the visibility centered on the value
						m_CurrentGlossaryContentVisibilityStart = (u32)core::max_(0, (s32)(value*m_CurrentGlossaryContentLineNb)-(GLOSSARY_ENTRY_DESC_LINES_DISPLAYED>>1));
						m_CurrentGlossaryContentVisibilityStart =
							m_ScrollBar.SetDimensions(m_CurrentGlossaryContentLineNb, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED);
					}
				}
			}
			else if(m_CurrentSelection == SBT_GLOSSARY)
			{
				for(int i = 0; i < 13; ++i)
				{
					m_LineArea[i].IsSelected = m_LineArea[i].Area.isPointInside(mousePos);
				}

				if(m_CurrentGlossaryEntryActive)
					m_CurrentGlossaryEntryActive = !m_LineArea[10].IsSelected;
				else
					m_CurrentGlossaryEntryActive = m_LineArea[8].IsSelected;
			}
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_CurrentSelection == SBT_GLOSSARY)
			{
				for(int i = 0; i < 8; ++i)
				{
					if(m_LineArea[i].IsSelected)
					{
						m_CurrentGlossaryEntry = m_CurrentGlossaryEntryVisibilityStart + i;
						m_CurrentGlossaryContentVisibilityStart = 0;
						m_CurrentCachedGlossaryContent = L"";
					}
				}

				m_GlossarySearchActive = m_LineArea[9].IsSelected;

				if(m_LineArea[11].IsSelected)
				{
					Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
					float value = (float)(mousePos.Y - m_LineArea[11].Area.UpperLeftCorner.Y)/
						(float)(m_LineArea[11].Area.getHeight());
					if(value >= 0.0f && value <= 1.0f)
					{
						// make the visibility start to have center of the visibility centered on the value
						m_CurrentGlossaryEntryVisibilityStart = (u32)core::max_(0, (s32)(value*m_CurrentEntries.size())-(GLOSSARY_NB_ENTRIES_DISPLAYED>>1));
						m_CurrentGlossaryEntryVisibilityStart =
							m_ScrollBar.SetDimensions(m_CurrentEntries.size(), m_CurrentGlossaryEntryVisibilityStart, GLOSSARY_NB_ENTRIES_DISPLAYED);
					}
				}
				else if(m_LineArea[12].IsSelected)
				{
					Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
					float value = (float)(mousePos.Y - m_LineArea[12].Area.UpperLeftCorner.Y)/
						(float)(m_LineArea[12].Area.getHeight());
					if(value >= 0.0f && value <= 1.0f)
					{
						// make the visibility start to have center of the visibility centered on the value
						m_CurrentGlossaryContentVisibilityStart = (u32)core::max_(0, (s32)(value*m_CurrentGlossaryContentLineNb)-(GLOSSARY_ENTRY_DESC_LINES_DISPLAYED>>1));
						m_CurrentGlossaryContentVisibilityStart =
							m_ScrollBar.SetDimensions(m_CurrentGlossaryContentLineNb, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED);
					}
				}
			}
			else
			{
				if(m_ActiveValidationArea.IsSelected)
				{
					// PLAY THE SCENE
					GameDesc::m_CurrentChapter = 0xffffffff;
					GameDesc::m_CurrentScene = m_CurrentSelection;
					GameDesc::m_CurrentDialog = 0;
					for(u32 i = 0; i < 3; ++i)
						GameDesc::m_CurrentSounds[i] = AudioDataSerializable();
					GameDesc::m_RequestSceneStart = true;
				}
			}
		}
		else if(event.MouseInput.Event == EMIE_MOUSE_WHEEL)
		{
			if(m_CurrentSubMenuActive == SMAT_CENTER)
			{
				if(m_CurrentSelection == SBT_GLOSSARY)
				{
					if(m_LineArea[10].IsSelected)
					{
						if(event.MouseInput.Wheel < 0.0f)
							++m_CurrentGlossaryEntryVisibilityStart;
						else
							if(m_CurrentGlossaryEntryVisibilityStart > 0)
								--m_CurrentGlossaryEntryVisibilityStart;
						m_CurrentGlossaryEntryVisibilityStart =
							m_ScrollBar.SetDimensions(m_CurrentEntries.size(), m_CurrentGlossaryEntryVisibilityStart, GLOSSARY_NB_ENTRIES_DISPLAYED);
					}
					else if(m_LineArea[8].IsSelected)
					{
						if(event.MouseInput.Wheel < 0.0f)
							++m_CurrentGlossaryContentVisibilityStart;
						else
							if(m_CurrentGlossaryContentVisibilityStart > 0)
								--m_CurrentGlossaryContentVisibilityStart;
						m_CurrentGlossaryContentVisibilityStart =
							m_ScrollBar.SetDimensions(m_CurrentGlossaryContentLineNb, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED);
					}
				}
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(m_GlossarySearchActive)
		{
			GlossaryEntry* lastEntry = (m_CurrentGlossaryEntry < m_CurrentEntries.size())? m_CurrentEntries[m_CurrentGlossaryEntry] : NULL;
			ProcessSearch(event);
			{
				m_CurrentEntries = m_Glossary.GetEntriesBySearch(Engine::GetInstance()->GetLanguage(), m_CurrentGlossarySearch);
				s32 currentEntryIdx = m_CurrentEntries.linear_search(lastEntry);
				if(currentEntryIdx < 0)
				{
					m_CurrentGlossaryEntry = 0;
					m_CurrentGlossaryEntryVisibilityStart = 0;
					m_CurrentGlossaryContentVisibilityStart = 0;
					m_CurrentCachedGlossaryContent = L"";
				}
				else
					m_CurrentGlossaryEntry = (u32)currentEntryIdx;
			}

			{
				// Special content
				core::stringw str(L"Waves of Decadence");
				if(m_CurrentGlossarySearch == str)
				{
					m_CurrentGlossarySearch = L"";

					// PLAY THE SCENE
					GameDesc::m_CurrentChapter = 0xffffffff;
					GameDesc::m_CurrentScene = 4;
					GameDesc::m_CurrentDialog = 0;
					for(u32 i = 0; i < 3; ++i)
						GameDesc::m_CurrentSounds[i] = AudioDataSerializable();
					GameDesc::m_RequestSceneStart = true;
				}
			}
		}
		//if(m_SelectionFadeTimer == 0.0f)
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_GLOSSARY && m_GlossarySearchActive)
					{
						m_GlossarySearchActive = false;
						m_LineArea[9].IsSelected = m_GlossarySearchActive;
					}
					else
					{
						// reset center area selection
						{
							m_ActiveValidationArea.IsSelected = false;
							for(u32 i = 0; i < 13; ++i)
								m_LineArea[i].IsSelected = false;
						}

						ChangeSubMenuActive(SMAT_LEFT);
					}
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_BOTTOM);
				}
				return true;// MUST BYPASS THE MAINPANEL ESCAPE EVENT
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_GLOSSARY)
					{
						m_GlossarySearchActive = !m_GlossarySearchActive;
						m_LineArea[9].IsSelected = m_GlossarySearchActive;
					}
					else
					{
						// PLAY THE SCENE
						GameDesc::m_CurrentChapter = 0xffffffff;
						GameDesc::m_CurrentScene = m_CurrentSelection;
						GameDesc::m_CurrentDialog = 0;
						for(u32 i = 0; i < 3; ++i)
							GameDesc::m_CurrentSounds[i] = AudioDataSerializable();
						GameDesc::m_RequestSceneStart = true;
					}
				}
				//m_TransitToChildPanelIndex = 0;
				//StartFade(PANEL_FADETIME);
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					--m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
						m_CurrentSelection = 0;
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
				}
				else if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(!m_GlossarySearchActive)
					{
						if(!m_CurrentGlossaryEntryActive)
						{
							if(m_CurrentGlossaryEntry > 0)
							{
								--m_CurrentGlossaryEntry;
								m_CurrentGlossaryContentVisibilityStart = 0;
								m_CurrentCachedGlossaryContent = L"";
								if((s32)m_CurrentGlossaryEntryVisibilityStart <= (s32)m_CurrentGlossaryEntry - GLOSSARY_NB_ENTRIES_DISPLAYED ||
									m_CurrentGlossaryEntryVisibilityStart > m_CurrentGlossaryEntry)// auto scrolling
								{
									m_CurrentGlossaryEntryVisibilityStart = m_CurrentGlossaryEntry;
									m_CurrentGlossaryEntryVisibilityStart =
										m_ScrollBar.SetDimensions(m_CurrentEntries.size(), m_CurrentGlossaryEntryVisibilityStart, GLOSSARY_NB_ENTRIES_DISPLAYED);
								}
							}
						}
						else
						{
							if(m_CurrentGlossaryContentVisibilityStart > 0)
							{
								--m_CurrentGlossaryContentVisibilityStart;
								m_CurrentGlossaryContentVisibilityStart =
									m_ScrollBar.SetDimensions(m_CurrentGlossaryContentLineNb, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED);
							}
						}
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					++m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
					{
						m_CurrentSelection = SBT_MAX - 1;
						ChangeSubMenuActive(SMAT_BOTTOM);
					}
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(!m_GlossarySearchActive)
					{
						if(!m_CurrentGlossaryEntryActive)
						{
							if(m_CurrentGlossaryEntry < m_CurrentEntries.size()-1)
							{
								++m_CurrentGlossaryEntry;
								m_CurrentGlossaryContentVisibilityStart = 0;
								m_CurrentCachedGlossaryContent = L"";
								if((s32)m_CurrentGlossaryEntryVisibilityStart <= (s32)m_CurrentGlossaryEntry - GLOSSARY_NB_ENTRIES_DISPLAYED ||
									m_CurrentGlossaryEntryVisibilityStart > m_CurrentGlossaryEntry)// auto scrolling
								{
									m_CurrentGlossaryEntryVisibilityStart =
										(u32)core::max_(0, (s32)m_CurrentGlossaryEntry - (GLOSSARY_NB_ENTRIES_DISPLAYED-1));
									m_CurrentGlossaryEntryVisibilityStart =
										m_ScrollBar.SetDimensions(m_CurrentEntries.size(), m_CurrentGlossaryEntryVisibilityStart, GLOSSARY_NB_ENTRIES_DISPLAYED);
								}
							}
						}
						else
						{
							++m_CurrentGlossaryContentVisibilityStart;
							m_CurrentGlossaryContentVisibilityStart =
								m_ScrollBar.SetDimensions(m_CurrentGlossaryContentLineNb, m_CurrentGlossaryContentVisibilityStart, GLOSSARY_ENTRY_DESC_LINES_DISPLAYED);
						}
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(!m_GlossarySearchActive)
					{
						if(m_CurrentGlossaryEntryActive)
							m_CurrentGlossaryEntryActive = false;
						m_LineArea[10].IsSelected = true;
						m_LineArea[8].IsSelected = false;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(!m_GlossarySearchActive)
					{
						if(!m_CurrentGlossaryEntryActive)
							m_CurrentGlossaryEntryActive = true;
						m_LineArea[10].IsSelected = false;
						m_LineArea[8].IsSelected = true;
					}
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
			}
		}
	}

	return (m_CurrentSubMenuActive != SMAT_BOTTOM);// bypass the main panel events if we don't have the focus on the bottom element
}

bool BonusSubMenuPanel::ProcessSearch(const SEvent& event)
{
	if(event.KeyInput.PressedDown)
	{
		switch(event.KeyInput.Key)
		{
			// TODO : Cursor control processing
		case KEY_END:
			break;
		case KEY_HOME:
			break;
		case KEY_RETURN:
			break;
		case KEY_LEFT:
			break;
		case KEY_RIGHT:
			break;
		case KEY_UP:
			break;
		case KEY_DOWN:
			break;
		case KEY_BACK:
			if(!m_CurrentGlossarySearch.empty())
				m_CurrentGlossarySearch = m_CurrentGlossarySearch.subString(0, m_CurrentGlossarySearch.size()-1);
			break;
		case KEY_DELETE:
			m_CurrentGlossarySearch = L"";
			break;
			// unused keys
		case KEY_ESCAPE:
		case KEY_TAB:
		case KEY_SHIFT:
		case KEY_F1:
		case KEY_F2:
		case KEY_F3:
		case KEY_F4:
		case KEY_F5:
		case KEY_F6:
		case KEY_F7:
		case KEY_F8:
		case KEY_F9:
		case KEY_F10:
		case KEY_F11:
		case KEY_F12:
		case KEY_F13:
		case KEY_F14:
		case KEY_F15:
		case KEY_F16:
		case KEY_F17:
		case KEY_F18:
		case KEY_F19:
		case KEY_F20:
		case KEY_F21:
		case KEY_F22:
		case KEY_F23:
		case KEY_F24:
			// ignore these keys
			return false;
		default:
			// process input
			{
				const wchar_t c = event.KeyInput.Char;
				if (c != 0)
				{
					m_CurrentGlossarySearch += c;
				}
			}
			return true;
		}
	}

	return false;
}

#undef PANEL_FADETIME
