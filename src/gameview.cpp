#include "gameview.h"
#include "engine.h"
#include "audio.h"
#include "utils.h"

// Define some values that we'll use to identify individual GUI controls.
// #define GUI_ID_SAVE_SPRITE_FILE_NAME_DLG 102
// #define GUI_ID_SAVE_SCENE_FILE_NAME_DLG 103
// #define GUI_ID_SAVE_DIALOG_FILE_NAME_DLG 104
// #define GUI_ID_BUTTON_SPRITE_MODE 600
// #define GUI_ID_BUTTON_SCENE_MODE 601
// #define GUI_ID_BUTTON_RENDER_MODE 602

Game* Game::ms_Instance = NULL;



// MyEventReceiver
bool MyEventReceiver::OnEvent(const SEvent& event)
{
	gui::IGUIEnvironment* env = Context.device->getGUIEnvironment();

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			// do not send mouse move events if mouse is disabled
			if(Context.game->IsMouseDisabled())
			{
				return false;
			}

			//printf("%d:%d\n", event.MouseInput.X, event.MouseInput.Y);
		}
		else if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
		{

		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		// any key pressed : disable mouse for {1}s
		Context.game->DisableMouse(0.2f);
#ifdef _DEBUG
		if(event.KeyInput.Key == KEY_KEY_P && event.KeyInput.PressedDown)
		{
// 			ProfileData::GetInstance()->m_ODS.m_WindowSize = Engine::GetInstance()->GetNextResolution(ProfileData::GetInstance()->m_ODS.m_WindowSize);
// 			ProfileData::GetInstance()->m_ODS.m_FullScreen = !ProfileData::GetInstance()->m_ODS.m_FullScreen;
// 			ProfileData::GetInstance()->m_ODS.m_VSync = !ProfileData::GetInstance()->m_ODS.m_VSync;
			Engine::GetInstance()->RequestResetDevice();
		}
#endif
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		s32 id = event.GUIEvent.Caller->getID();
		if(event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED)
		{

		}

		switch(event.GUIEvent.EventType)
		{
		case gui::EGET_FILE_SELECTED:
			break;
		case gui::EGET_DIRECTORY_SELECTED:
			/********************************* ANTI BUG *****************************************/
			if(wcslen(((gui::IGUIFileOpenDialog*)event.GUIEvent.Caller)->getFileName()) != 0)// we have selected a file, not a directory
			{
				break;// do nothing : we'll get soon the correct file selected event
			}
			/********************************* ANTI BUG *****************************************/
			else
			{

			}
			break;
		case gui::EGET_FILE_CHOOSE_DIALOG_CANCELLED:
			break;
		default:
			break;
		}
	}

	if(Context.game->GetCurrentPanel())
		Context.game->GetCurrentPanel()->OnEvent(event);

	return false;
}




// Game

Game::Game() : m_Accumulator(0.0f), m_TimerDisableMouse(0.0f), m_CurrentPanel(NULL), m_LastDisableMousePosition(P2Zero), m_EventReceiver(NULL)
{
	InitializeEventReceiver();

	Engine* engine = Engine::GetInstance();
	GameDesc* desc = GameDescManager::GetGameDesc();// generate here the gamedesc the first time we load the main panel (no need before)
	if(!desc->ReadFromFile(engine->GetGamePath("data/scenedesc.scd").c_str()))
	{
#ifndef IRON_ICE_FINAL
		bool txtFound = false;
		if(!desc->ReadFromFileTxt(engine->GetGamePath("data/scenedesc_fr.txt").c_str(), Engine::LS_FRENCH))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/scenedesc_fr.txt.");
		if(!desc->ReadFromFileTxt(engine->GetGamePath("data/scenedesc_en.txt").c_str(), Engine::LS_ENGLISH))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/scenedesc_en.txt.");
		if(!desc->ReadFromFileTxt(engine->GetGamePath("data/scenedesc_de.txt").c_str(), Engine::LS_GERMAN))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/scenedesc_de.txt.");
		if(!desc->ReadFromFileTxt(engine->GetGamePath("data/scenedesc_es.txt").c_str(), Engine::LS_SPANISH))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/scenedesc_es.txt.");
		if(!desc->WriteToFile(engine->GetGamePath("data/scenedesc.scd").c_str()))
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible d'ecrire le fichier data/scenedesc.scd.");

		if(!desc->ReadFromFile(engine->GetGamePath("data/scenedesc.scd").c_str()))
		{
			engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/scenedesc.scd.");
			assert(0);
		}
#else
		engine->GetGui()->addMessageBox(L"Erreur", L"Impossible de lire le fichier data/scenedesc.scd.");
		assert(0);
#endif
	}
	if(desc)
	{
		desc->LoadPreviews();
	}

	BuildPanels();
}

void Game::InitializeEventReceiver()
{
	if(m_EventReceiver)
		SafeDelete(m_EventReceiver);

	Engine* engine = Engine::GetInstance();

	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	// Store the appropriate data in a context structure.
	SAppContext context;
	context.device = device;
	context.counter = 0;
	context.game = this;

	// Then create the event receiver, giving it that context structure.
	m_EventReceiver = new MyEventReceiver(context);

	// And tell the device to use our custom event receiver.
	device->setEventReceiver(m_EventReceiver);
}

void Game::BuildPanels()
{
	enum PanelType {PT_LOGO, PT_LANGUAGE, PT_STARTMENU, PT_MAINMENU, PT_GAME};
	IPanel* panel = NULL;
	panel = new LogoPanel();
	m_Panels.insert(panel, PT_LOGO);
	panel = new LanguagePanel();
	m_Panels.insert(panel, PT_LANGUAGE);
	panel = new StartMenuPanel();
	m_Panels.insert(panel, PT_STARTMENU);
	panel = new MainMenuPanel();
	m_Panels.insert(panel, PT_MAINMENU);
	panel = new GamePanel();
	m_Panels.insert(panel, PT_GAME);

	m_Panels[PT_LOGO]->AddChild(m_Panels[PT_LANGUAGE]);
	m_Panels[PT_LANGUAGE]->AddChild(m_Panels[PT_STARTMENU]);
	m_Panels[PT_STARTMENU]->AddChild(m_Panels[PT_MAINMENU]);
	m_Panels[PT_MAINMENU]->AddChild(m_Panels[PT_GAME]);
	//m_Panels[PT_MAINMENU]->AddChild(m_Panels[PT_STARTMENU]);
	m_Panels[PT_GAME]->AddChild(m_Panels[PT_GAME]);
	m_Panels[PT_GAME]->AddChild(m_Panels[PT_MAINMENU]);

	m_CurrentPanel = m_Panels[0];
}

Game::~Game()
{
	SafeDelete(m_EventReceiver);
	m_CurrentPanel = NULL;
	for(u32 i = 0; i < m_Panels.size(); ++i)
		SafeDelete(m_Panels[i]);
	m_Panels.clear();

	// delete the gameDesc
	GameDescManager::DeleteGameDesc();

	// delete the profile
	ProfileData::GetInstance()->DeleteInstance();
}

Game* Game::GetInstance()
{
	if(!ms_Instance)
		ms_Instance = new Game();
	return ms_Instance;
}

void Game::DeleteInstance()
{
	SafeDelete(ms_Instance);
}

void Game::OnDevicePreReset()
{
	if(m_CurrentPanel && m_CurrentPanel->IsLoaded())
	{
		m_CurrentPanel->OnDevicePreReset();
		m_CurrentPanel->UnloadPanel();
	}
	GameDesc* desc = GameDescManager::GetGameDesc();
	desc->UnloadPreviews();
}

void Game::OnDevicePostReset()
{
	InitializeEventReceiver();

	if(m_CurrentPanel && !m_CurrentPanel->IsLoaded())
	{
		m_CurrentPanel->LoadPanel();
		m_CurrentPanel->OnDevicePostReset();
	}
	GameDesc* desc = GameDescManager::GetGameDesc();
	desc->LoadPreviews();
}

void Game::Update(float dt)
{
	m_Accumulator += dt;

	// Mouse activation/deactivation code
	{
		gui::ICursorControl* cursor = Engine::GetInstance()->GetDevice()->getCursorControl();
		const Position2 currentCursorPos(cursor->getPosition());

		if(IsMouseTemporarilyDisabled() || !IsMouseDisabled())
		{
			m_LastDisableMousePosition = currentCursorPos;
		}

		// Moved but not enough since last time disabled : don't enable it
		const s32 distPxlMin = 20;
		if(IsMouseDisabled())
			EnableMouse((m_LastDisableMousePosition - currentCursorPos).getLengthSQ() > distPxlMin*distPxlMin);
		cursor->setVisible(!IsMouseDisabled());

		if(m_TimerDisableMouse > 0.0f)
		{
			m_TimerDisableMouse -= dt;
			if(m_TimerDisableMouse < 0.0f)
				m_TimerDisableMouse = 0.0f;
		}
	}

	if(m_CurrentPanel)
	{
		if(!m_CurrentPanel->IsLoaded())
			m_CurrentPanel->LoadPanel();

		m_CurrentPanel->Update(dt);
		s32 childIndex = m_CurrentPanel->TransitToChildPanelIndex();
		if(childIndex >= 0 && !m_CurrentPanel->IsFading())
		{
			IPanel* oldPanel = m_CurrentPanel;
			m_CurrentPanel = m_CurrentPanel->GetChildAtIndex(childIndex);

			if(m_CurrentPanel->ShouldForceReload())
			{
				oldPanel->UnloadPanel();
				m_CurrentPanel->LoadPanel();
				m_CurrentPanel->ResetPanel();
			}
			else
			{
				if(m_CurrentPanel)
				{
					if(!m_CurrentPanel->IsLoaded())
						m_CurrentPanel->LoadPanel();
					m_CurrentPanel->ResetPanel();
				}
				if(oldPanel && oldPanel != m_CurrentPanel)
					oldPanel->UnloadPanel();
			}
		}
	}
	//printf("dt = %f\n", dt);
}

void Game::Render()
{
	if(m_CurrentPanel)
	{
		m_CurrentPanel->PreRender();
		m_CurrentPanel->Render();
		m_CurrentPanel->PostRender();
	}

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	// rescale from [0;100] to [-0.5;0.5]
	float brightness = 0.01f * ProfileData::GetInstance()->m_ODS.m_GammaRamp - 0.5f;
	drawBrightnessLayer(driver, Rectangle2(P2Zero, renderSize), brightness);


	// real render zone : redraw the black bands
	{
		const IColor color(255, 0, 0, 0);
		// left
		{
			Position2 pos = Position2(0, 0);
			Dimension2 dim(innerOffset.X, renderSize.Height);
			Rectangle2 destRect(pos, dim);
			fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
		}

		// right
		{
			Position2 pos = Position2((s32)(originalSize.Width*innerScale) + innerOffset.X, 0);
			Dimension2 dim(renderSize.Width-pos.X, renderSize.Height);
			Rectangle2 destRect(pos, dim);
			fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
		}

		// up
		{
			Position2 pos = Position2(0, 0);
			Dimension2 dim(renderSize.Width, innerOffset.Y);
			Rectangle2 destRect(pos, dim);
			fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
		}

		// down
		{
			Position2 pos = Position2(0, (s32)(originalSize.Height*innerScale) + innerOffset.Y);
			Dimension2 dim(renderSize.Width, renderSize.Height-pos.Y);
			Rectangle2 destRect(pos, dim);
			fill2DRect(driver, destRect, color, color, color, color, USE_PREMULTIPLIED_ALPHA);
		}
	}
	//printf("mouse disable() = %d\ndisable timer = %f\n", IsMouseDisabled(), m_TimerDisableMouse);
}





// #undef GUI_ID_SAVE_SPRITE_FILE_NAME_DLG
// #undef GUI_ID_SAVE_SCENE_FILE_NAME_DLG
// #undef GUI_ID_SAVE_DIALOG_FILE_NAME_DLG
// #undef GUI_ID_BUTTON_SPRITE_MODE
// #undef GUI_ID_BUTTON_SCENE_MODE
// #undef GUI_ID_BUTTON_RENDER_MODE

