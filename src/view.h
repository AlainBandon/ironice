#ifndef VIEW_H
#define VIEW_H

#include "utils.h"
#include "sprite.h"

#include "spriteview.h"
#include "sceneview.h"
#include "renderview.h"

//#include "driverChoice.h"


// Declare a structure to hold some context for the event receiver so that it
// has it available inside its OnEvent() method.
struct SAppContext
{
	IrrlichtDevice *device;
	s32				counter;

	SpriteBankListBox*	spritebanklist;
	FrameScrollMenu* framescrollmenu;
	LayerMenu* layermenu;
	EditorWindow* editorwindow;

	AnimationBankListBox* animbanklist;
	DialogListBox* dialoglist;
	SceneEditorWindow* sceneeditorwindow;

	RenderWindow* renderwindow;

	gui::IGUIFileOpenDialog* openfiledlg;

	SpriteInterface* spriteinterface;
	SceneInterface* sceneinterface;
};


/*
The Event Receiver is not only capable of getting keyboard and
mouse input events, but also events of the graphical user interface
(gui). There are events for almost everything: Button click,
Listbox selection change, events that say that a element was hovered
and so on. To be able to react to some of these events, we create
an event receiver.
We only react to gui events, and if it's such an event, we get the
id of the caller (the gui element which caused the event) and get
the pointer to the gui environment.
*/
class MyEventReceiver : public IEventReceiver
{
public:
	MyEventReceiver(const SAppContext& context) : 
	  Context(context), m_SaveFileNameDialog(NULL), m_SaveFileNameEditBox(NULL), m_PopupDialog(NULL) { }

	virtual bool OnEvent(const SEvent& event);

private:
	SAppContext Context;

	gui::IGUIWindow* m_SaveFileNameDialog;
	gui::IGUIEditBox* m_SaveFileNameEditBox;
	gui::IGUIWindow* m_PopupDialog;
	io::path m_SaveName;
};

class Editor
{
public:
	~Editor();

	enum EditorMode{EM_NONE, EM_SPRITE, EM_SCENE, EM_RENDER};

	static Editor* GetInstance();
	void DeleteInstance();

	void Update(float dt);

	void ChangeMode(EditorMode mode);	

protected:
	Editor();

	static Editor* ms_Instance;

	gui::IGUIEnvironment* m_Gui;

	MyEventReceiver* m_EventReceiver;

	//sprite view
	SpriteBankListBox*	m_SpriteBankList;
	FrameScrollMenu* m_FrameScrollMenu;
	LayerMenu* m_LayerMenu;
	EditorWindow* m_EditorWindow;

	//scene view
	AnimationBankListBox* m_AnimBankList;
	DialogListBox* m_DialogList;
	SceneEditorWindow* m_SceneEditorWindow;

	// render view
	RenderWindow* m_RenderWindow;

	gui::IGUIToolBar* m_ToolBar;

	SpriteInterface* m_SpriteInterface;
	SceneInterface* m_SceneInterface;

	float m_Accumulator;

	EditorMode m_EditorMode;
};

#endif //#ifdef VIEW_H
