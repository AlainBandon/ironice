﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f
#define SELECTION_FADETIME 0.5f

enum StringDesc {SD_BACK = Engine::SFTID_MainMenuPanel, SD_STORY, SD_STATS, SD_OPTIONS, SD_BONUS, SD_EXIT, SD_MAX};

// MainMenuPanel
MainMenuPanel::MainMenuPanel() : IPanel()
, m_BG(NULL)
, m_MainLine(NULL)
, m_EscapeButton(NULL)
, m_CurrentSelection(0)
, m_PreviousSelection(0)
, m_SelectionFadeTimer(0.0f)
, m_ScrollingSide(0)
, m_GameDesc(NULL)
{
	for(int i = 0; i < 3; ++i)
		m_Font[i] = NULL;

	for(int i = 0; i < MBT_EXIT - MBT_STORY + 1; ++i)
		m_SubPanels[i] = NULL;
	m_ScenePanel = NULL;
}

MainMenuPanel::~MainMenuPanel()
{
	UnloadPanel();

	GameDescManager::DeleteGameDesc();// no more needed after this : delete it safely
}

void MainMenuPanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.MainMenuFontScale;

	engine->DrawLoadScreen();

	if(!m_GameDesc)
	{
		m_GameDesc = GameDescManager::GetGameDesc();
	}

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[3] = {48, 60, 90};
		for(int i = 0; i < 3; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, USE_PREMULTIPLIED_ALPHA, (size/6));
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	SAFE_LOAD_IMAGE(driver, m_MainLine, engine->GetGamePath("system/02_Main-line_under.png"), USE_PREMULTIPLIED_ALPHA);

	//SAFE_LOAD_IMAGE(driver, m_EscapeButton, engine->GetGamePath("system/02_Echap_under.png"), USE_PREMULTIPLIED_ALPHA);

	if(!m_SubPanels[0])
	{
		StorySubMenuPanel* panel0 = new StorySubMenuPanel();
		panel0->LoadPanel();
		m_SubPanels[0] = panel0;
		StatsSubMenuPanel* panel1 = new StatsSubMenuPanel();
		panel1->LoadPanel();
		m_SubPanels[1] = panel1;
		OptionsSubMenuPanel* panel2 = new OptionsSubMenuPanel();
		panel2->LoadPanel();
		m_SubPanels[2] = panel2;
		BonusSubMenuPanel* panel3 = new BonusSubMenuPanel();
		panel3->LoadPanel();
		m_SubPanels[3] = panel3;
		ExitSubMenuPanel* panel4 = new ExitSubMenuPanel();
		panel4->SetParentPanel(this);
		panel4->LoadPanel();
		m_SubPanels[4] = panel4;
	}

	if(!m_ScenePanel)
	{
		m_ScenePanel = new ScenePanel();
		m_ScenePanel->SetScenePath(engine->GetGamePath("system/menu_scn/0000/scene.scn"));
		m_ScenePanel->LoadPanel();
		m_ScenePanel->SetCurrentDialog();
 		m_ScenePanel->m_DisplayDialogs = false;
 		m_ScenePanel->m_DisplayFades = false;
 		m_ScenePanel->m_DisplayKeys = false;
 		m_ScenePanel->m_UpdateKeys = false;
		m_ScenePanel->ForceLoadScene();
	}
	if(!m_ScenePanel)
	{
		SAFE_LOAD_IMAGE(driver, m_BG, engine->GetGamePath("system/Menu_BG.jpg"), USE_PREMULTIPLIED_ALPHA);
	}

	// Load the profile if not already loaded
	ProfileData* profile = ProfileData::GetInstance();
	// check for illegal saves
	{
		for(u32 i = 0; i < PROFILE_SAVE_SLOTS; ++i)
		{
			SaveData& save = profile->GetSave(i);
			if(save.Used)
			{
				if(save.ChapterId >= m_GameDesc->ChaptersDesc.size())// illegal chapter
				{
					save.Used = false;// remove it
					continue;
				}
				if(save.SceneId >= m_GameDesc->ChaptersDesc[save.ChapterId].ScenesDesc.size())// illegal scene
				{
					save.Used = false;// remove it
					continue;
				}
			}
		}
	}

	GameDesc::m_CurrentChapter = 0xffffffff;

	OnResize();

	StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN));

	IPanel::LoadPanel();
}

void MainMenuPanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_BG);
	
	for(int i = 0; i < 3; ++i)
		SAFE_UNLOAD(m_Font[i]);

	SAFE_UNLOAD(m_MainLine);

	SafeDelete(m_ScenePanel);

// 	SAFE_UNLOAD(m_EscapeButton);

	for(int i = 0; i < MBT_EXIT - MBT_STORY + 1; ++i)
	{
		SafeDelete(m_SubPanels[i]);
	}
	m_GameDesc = NULL;

	IPanel::UnloadPanel();
}

void MainMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.MainMenuFontScale;

	if(m_Font[0])
	{
		const u32 sizes[3] = {48, 60, 90};
		for(int i = 0; i < 3; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			m_Font[i]->setSize(size, size/6);
		}
	}

	{

// 		if(m_EscapeButton)
// 		{
// 			Dimension2 size = m_EscapeButton->getSize();
// 
// 			Position2 pos = Position2((s32)((f32)((size.Width>>1)+100)*innerScale), (s32)((f32)((size.Height>>1)+60)*innerScale)) + innerOffset;
// 
// 			Position2 cornerOffset((s32)((f32)size.Width*innerScale*0.5f), (s32)((f32)size.Height*innerScale*0.5f));
// 			m_ButtonsArea[MBT_ESCAPE].Area = Rectangle2(pos - cornerOffset, pos + cornerOffset);
// 		}
		
		if(m_MainLine)
		{
			Dimension2 size = m_MainLine->getSize();
			Position2 pos = Position2(0, (s32)((f32)(originalSize.Height - (size.Height>>1) - 50)*innerScale)) + innerOffset;

			u32 dx = (u32)((originalSize.Width/(f32)(MBT_EXIT - MBT_STORY + 1))*innerScale);
			Position2 offset(dx>>1, 0);
			Position2 cornerOffset((s32)((f32)dx*0.5f), (s32)((f32)size.Height*innerScale*0.5f));
			for(int i = MBT_STORY; i <= MBT_EXIT; ++i, offset.X += dx)
			{
				m_ButtonsArea[i].Area = Rectangle2(pos + offset - cornerOffset, pos + offset + cornerOffset);
				m_ButtonsArea[i].IsSelected = false;
			}
		}
	}

	if(m_SubPanels[m_PreviousSelection] && m_SubPanels[m_PreviousSelection]->IsFading())
	{
		m_SubPanels[m_PreviousSelection]->OnResize();
	}

	if(m_SubPanels[m_CurrentSelection])
	{
		m_SubPanels[m_CurrentSelection]->OnResize();
	}
}

void MainMenuPanel::Update(float dt) 
{
	IPanel::Update(dt);

	if(m_ScenePanel)
		m_ScenePanel->Update(dt);

	if(m_SubPanels[m_PreviousSelection] && m_SubPanels[m_PreviousSelection]->IsFading())
		m_SubPanels[m_PreviousSelection]->Update(dt);

	if(m_SubPanels[m_CurrentSelection])
		m_SubPanels[m_CurrentSelection]->Update(dt);

	if(m_SelectionFadeTimer > 0.0f)
	{
		m_SelectionFadeTimer -= dt;
		if(m_SelectionFadeTimer < 0.0f)
			m_SelectionFadeTimer = 0.0f;
	}

	
	if(GameDesc::m_RequestSceneStart)
	{
		GameDesc::m_RequestSceneStart = false;
		m_TransitToChildPanelIndex = 0;
		StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
	}
}

void MainMenuPanel::PreRender()
{
	if(m_ScenePanel)
		m_ScenePanel->PreRender();

	if(m_SubPanels[m_PreviousSelection] && m_SubPanels[m_PreviousSelection]->IsFading())
	{
		m_SubPanels[m_PreviousSelection]->PreRender();
	}

	if(m_SubPanels[m_CurrentSelection])
	{
		m_SubPanels[m_CurrentSelection]->PreRender();
	}
}

void MainMenuPanel::Render()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor color = COLOR_BLACK;
	fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);

	if(m_ScenePanel)
		m_ScenePanel->Render();
	else if(m_BG)
	{
		IColor tint = COLOR_WHITE;

		Dimension2 size = m_BG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_BG, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	bool isActive = true;
	switch(MBT_STORY + m_CurrentSelection)
	{
	case MBT_STORY:
		isActive = (((StorySubMenuPanel*)m_SubPanels[m_CurrentSelection])->GetCurrentSubMenuActive() == StorySubMenuPanel::SMAT_BOTTOM);
		break;
	case MBT_STAT:
		isActive = (((StatsSubMenuPanel*)m_SubPanels[m_CurrentSelection])->GetCurrentSubMenuActive() == StorySubMenuPanel::SMAT_BOTTOM);
		break;
	case MBT_OPTIONS:
		isActive = (((OptionsSubMenuPanel*)m_SubPanels[m_CurrentSelection])->GetCurrentSubMenuActive() == StorySubMenuPanel::SMAT_BOTTOM);
		break;
	case MBT_BONUS	:
		isActive = (((BonusSubMenuPanel*)m_SubPanels[m_CurrentSelection])->GetCurrentSubMenuActive() == StorySubMenuPanel::SMAT_BOTTOM);
		break;
	case MBT_EXIT:
		break;
	default:
		assert(0);
		break;
	}

	// render main panel
// 	if(m_EscapeButton)
// 	{
// 		float ratio = core::abs_(cosf(0.5f*core::PI*m_Time));
// 		IColor tint = MathUtils::GetInterpolatedColor(WHITE, IColor(m_ButtonsArea[MBT_ESCAPE].IsSelected? 255 : 200, 255, 255, 255), ratio);
// 		
// 		Dimension2 size = m_EscapeButton->getSize();
// 		Rectangle2 sourceRect(Position2(0,0), size);
// 		
// 		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_ButtonsArea[MBT_ESCAPE].Area.getCenter().X, (f32)m_ButtonsArea[MBT_ESCAPE].Area.getCenter().Y, 0.0f)) *
// 			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
// 			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));
// 
// 		draw2DImage(driver, m_EscapeButton, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
// 	}

	if(m_MainLine)
	{
		float ratio = core::abs_(cosf(0.5f*core::PI*m_Time));
		IColor tint = MathUtils::GetInterpolatedColor(COLOR_WHITE, IColor(200, 255, 255, 255), ratio);

		Dimension2 size = m_MainLine->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)m_ButtonsArea[MBT_STORY].Area.getCenter().X, (f32)m_ButtonsArea[MBT_STORY].Area.getCenter().Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_MainLine, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

// 	if(m_Font[0])
// 	{
// 		core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_BACK));
// 		m_Font[0]->draw(str, m_ButtonsArea[MBT_ESCAPE].Area, COMPUTE_THEME_COLOR(m_ButtonsArea[MBT_ESCAPE].IsSelected? 255 : 200), true, true);
// 	}

	if(m_Font[1] && m_Font[2])
	{
		s32 dx = (u32)((originalSize.Width/(f32)(MBT_EXIT - MBT_STORY + 1))*innerScale);

		s32 xRef = m_ButtonsArea[MBT_STORY].Area.getCenter().X;
		s32 xRefFarthest = m_ButtonsArea[MBT_EXIT].Area.getCenter().X + dx;
		for(int i = MBT_STORY; i <= MBT_EXIT; ++i)
		{
// 			if(!isActive && i != MBT_STORY + m_CurrentSelection)// only display current selection when not active
// 				continue;

#ifdef DEMO_VERSION
			IColor notAvailableColor = IColor(200, 100, 100, 100);
#endif
			
			Position2 pos(m_ButtonsArea[i].Area.getCenter());
			Position2 cornerOffset(m_ButtonsArea[i].Area.getWidth()>>1, m_ButtonsArea[i].Area.getHeight()>>1);

			u32 currentSlot = MBT_STORY + ((i-MBT_STORY+(MBT_EXIT-MBT_STORY+1)-m_CurrentSelection)%(MBT_EXIT-MBT_STORY+1));
			bool isSelected = (m_SelectionFadeTimer == 0.0f) && m_ButtonsArea[currentSlot].IsSelected;

			pos.X -= m_CurrentSelection * dx - m_ScrollingSide * (u32)((m_SelectionFadeTimer/(SELECTION_FADETIME/**core::abs_(m_ScrollingSide)*/))*(dx));
			if(pos.X >= (MBT_EXIT - MBT_STORY + 1) * dx + (dx>>1) + innerOffset.X)
				pos.X -= (MBT_EXIT - MBT_STORY + 1) * dx;
			else if(pos.X <= -(dx>>1) + innerOffset.X)
				pos.X += (MBT_EXIT - MBT_STORY + 1) * dx;

			Rectangle2 rect(pos - cornerOffset, pos + cornerOffset);
			core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), i+SD_BACK));
			float xRatio = (1.0f - (f32)core::abs_(pos.X - xRef)/(f32)(xRefFarthest - xRef));
			xRatio *= xRatio;
			u32 a = isSelected? 255 : (u32)(xRatio*200)+55;
			float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*currentSlot)-3.0f, 0.0f);
			IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(200>>2), COMPUTE_THEME_COLOR_FONT_GLOW(200), ratio);
			IColor color(COMPUTE_THEME_COLOR(a));
#ifdef DEMO_VERSION
			if(i == MBT_STAT || i == MBT_BONUS)
				tint = color = notAvailableColor;
#endif
			if( m_SelectionFadeTimer == 0.0f && i == MBT_STORY + m_CurrentSelection)
			{
				//m_Font[2]->drawGauss(str, rect, tint, true, true, &clippingRect);
				m_Font[2]->draw(str, rect, color, true, true);
			}
			else
			{
				m_Font[1]->drawGauss(str, rect, tint, true, true);
				if(isActive)
					m_Font[1]->draw(str, rect, color, true, true);

				// FIXME : HACK to fix the asymmetry caused by the modulo on m_CurrentSelection (when returning to 0) : to hide this we always draw the reflection righ and left
				if(m_SelectionFadeTimer > 0.0f /*&& i == ((MBT_STORY+m_CurrentSelection+(MBT_EXIT - MBT_STORY))%(MBT_EXIT - MBT_STORY + 1))*/)// i = previous element of the current one
				{
					bool redraw = false;
					if(pos.X < (dx>>1))// already drawn on the left : draw on the right too
					{
						pos.X += (MBT_EXIT - MBT_STORY + 1) * dx;
						redraw = true;
					}
					else if(pos.X > (MBT_EXIT - MBT_STORY) * dx + (dx>>1))// already drawn on the right : draw on the left too
					{
						pos.X -= (MBT_EXIT - MBT_STORY + 1) * dx;
						redraw = true;
					}

					if(redraw)
					{
						xRatio = (1.0f - (f32)core::abs_(pos.X - xRef)/(f32)(xRefFarthest - xRef));
						xRatio *= xRatio;
						a = isSelected? 255 : (u32)(xRatio*230)+25;

						Rectangle2 rect(pos - cornerOffset, pos + cornerOffset);
						IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(a), COMPUTE_THEME_COLOR(a>>2), ratio);
						IColor color(a, 255, 255, 255);
#ifdef DEMO_VERSION
						if(i == MBT_STAT || i == MBT_BONUS)
							tint = color = notAvailableColor;
#endif
						m_Font[1]->drawGauss(str, rect, tint, true, true);
						if(isActive)
							m_Font[1]->draw(str, rect, color, true, true);
					}
				}
			}
		}
	}

	// render subpanels
	if(m_SubPanels[m_PreviousSelection] && m_SubPanels[m_PreviousSelection]->IsFading())
	{
		m_SubPanels[m_PreviousSelection]->Render();
	}

	if(m_SubPanels[m_CurrentSelection])
	{
		m_SubPanels[m_CurrentSelection]->Render();
	}

	if(IsFading())
	{
		IColor fadeColor = COLOR_BLACK;
		IColor transparent = COLOR_TRANSPARENT;
		//fade in
		float ratio = m_Fader.GetRatio();
		IColor color = MathUtils::GetInterpolatedColor(fadeColor, transparent, ratio);
		fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);
	}
}

bool MainMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	// the sub panel has the priority on our panel
	if(m_SubPanels[m_CurrentSelection])
	{
		if(m_SubPanels[m_CurrentSelection]->OnEvent(event))
		{
			for(int i = 0; i < MBT_MAX; ++i)
				m_ButtonsArea[i].IsSelected = false;

			return true;
		}
	}

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
			for(int i = 0; i < MBT_MAX; ++i)
			{
				m_ButtonsArea[i].IsSelected = m_ButtonsArea[i].Area.isPointInside(mousePos);
			}
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_SelectionFadeTimer == 0.0f)
			{
				for(int i = 0; i < MBT_MAX; ++i)
				{
					if(m_ButtonsArea[i].IsSelected)
					{
// 						if(i == MBT_ESCAPE)
// 						{
// 							m_TransitToChildPanelIndex = 0;
// 							m_ButtonsArea[MBT_ESCAPE].IsSelected = true;// force the selection on the button
// 							StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
// 						}
// 						else
						{
#ifdef DEMO_VERSION
							{
								int curSel = m_CurrentSelection;
								u32 inc = i-MBT_STORY;
								curSel += inc;
								if(curSel+1 > MBT_EXIT - MBT_STORY)
									curSel -= (MBT_EXIT - MBT_STORY + 1);

								if(curSel+1 == MBT_STAT || curSel+1 == MBT_BONUS)
									continue;
							}
#endif
							if(i > MBT_STORY)
							{
								m_PreviousSelection = m_CurrentSelection;
								if(m_SubPanels[m_PreviousSelection])
								{
									m_SubPanels[m_CurrentSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT, Fader::FS_FADE_LEFT));
								}

								u32 inc = i-MBT_STORY;
								m_ScrollingSide = inc;
								m_SelectionFadeTimer = SELECTION_FADETIME/**inc*/;
								m_CurrentSelection += inc;
								if(m_CurrentSelection > MBT_EXIT - MBT_STORY)
									m_CurrentSelection -= (MBT_EXIT - MBT_STORY + 1);

								if(m_SubPanels[m_CurrentSelection])
								{
									m_SubPanels[m_CurrentSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN, Fader::FS_FADE_RIGHT));
									m_SubPanels[m_CurrentSelection]->OnResize();
								}
							}
						}
						//break;
					}
				}
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(m_SelectionFadeTimer == 0.0f)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1) && event.KeyInput.PressedDown)
			{
				//m_TransitToChildPanelIndex = 0;
				//StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2) && event.KeyInput.PressedDown)
			{
// 				m_TransitToChildPanelIndex = 0;
// 				m_ButtonsArea[MBT_ESCAPE].IsSelected = true;// force the selection on the button
// 				StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				m_PreviousSelection = m_CurrentSelection;
				if(m_SubPanels[m_PreviousSelection])
				{
					m_SubPanels[m_PreviousSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT, Fader::FS_FADE_RIGHT));
				}

				m_ScrollingSide = -1;
				m_SelectionFadeTimer = SELECTION_FADETIME;
				--m_CurrentSelection;
				if(m_CurrentSelection > MBT_EXIT - MBT_STORY)
					m_CurrentSelection = MBT_EXIT - MBT_STORY;

#ifdef DEMO_VERSION
				while(m_CurrentSelection+1 == MBT_STAT || m_CurrentSelection+1 == MBT_BONUS)
				{
					--m_ScrollingSide;
					--m_CurrentSelection;
					if(m_CurrentSelection > MBT_EXIT - MBT_STORY)
						m_CurrentSelection = MBT_EXIT - MBT_STORY;
				}
#endif

				if(m_SubPanels[m_CurrentSelection])
				{
					m_SubPanels[m_CurrentSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN, Fader::FS_FADE_LEFT));
					m_SubPanels[m_CurrentSelection]->OnResize();
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				m_PreviousSelection = m_CurrentSelection;
				if(m_SubPanels[m_PreviousSelection])
				{
					m_SubPanels[m_PreviousSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT, Fader::FS_FADE_LEFT));
				}

				m_ScrollingSide = 1;
				m_SelectionFadeTimer = SELECTION_FADETIME;
				++m_CurrentSelection;
				if(m_CurrentSelection > MBT_EXIT - MBT_STORY)
					m_CurrentSelection = 0;

#ifdef DEMO_VERSION
				while(m_CurrentSelection+1 == MBT_STAT || m_CurrentSelection+1 == MBT_BONUS)
				{
					++m_ScrollingSide;
					++m_CurrentSelection;
					if(m_CurrentSelection > MBT_EXIT - MBT_STORY)
						m_CurrentSelection = 0;
				}
#endif

				if(m_SubPanels[m_CurrentSelection])
				{
					m_SubPanels[m_CurrentSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN, Fader::FS_FADE_RIGHT));
					m_SubPanels[m_CurrentSelection]->OnResize();
				}
			}
		}
	}

	if(m_ScenePanel)
	{
		if(m_ScenePanel->OnEvent(event))
			return true;
	}

	return false;
}

void MainMenuPanel::ForceDefaultSelection()
{
	m_PreviousSelection = m_CurrentSelection;
	if(m_SubPanels[m_PreviousSelection])
	{
		m_SubPanels[m_PreviousSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT, Fader::FS_FADE_LEFT));
	}

	u32 inc = (MBT_EXIT - MBT_STORY + 1) - m_CurrentSelection;
	m_ScrollingSide = inc;
	m_SelectionFadeTimer = SELECTION_FADETIME/**inc*/;
	m_CurrentSelection = 0;

	if(m_SubPanels[m_CurrentSelection])
	{
		m_SubPanels[m_CurrentSelection]->StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN, Fader::FS_FADE_RIGHT));
		m_SubPanels[m_CurrentSelection]->OnResize();
	}
}

#undef PANEL_FADETIME
#undef SELECTION_FADETIME
