﻿#include "engine.h"
#include "utils.h"

#include <stdio.h>  /* defines FILENAME_MAX */
#ifdef _IRR_WINDOWS_
#include <direct.h>
#define GetCurrentDir _getcwd
#else
#include <unistd.h>
#define GetCurrentDir getcwd
#endif


bool Engine::ms_FullScreen = false;
Dimension2 Engine::ms_WindowSize(800, 600);
Engine* Engine::ms_Instance = NULL;
ProfileData* ProfileData::ms_Instance = NULL;

const wchar_t* const Engine::ms_EmptyString = L"{Missing String}";
const wchar_t* const Engine::ms_LanguageSettingsString[LS_MAX] = {L"F R A N Ç A I S", L"E N G L I S H", L"D E U T S C H", L"E S P A Ñ O L"};

const wchar_t* const InputData::ms_InputNames[PI_MAX] = {L"Gauche", L"Droite", L"Haut", L"Bas", L"Action1", L"Action2", L"Action3", L"Action4", L"Action5", L"Action6"};

wchar_t* Engine::ms_LanguageStrings[LS_MAX][Engine::SFTID_MAX] = {NULL};

OutputStreamPtr stream;


// Engine
Engine::Engine() :
m_OriginalSize(1920, 1080),
m_RenderSize(ms_WindowSize),
m_Device(NULL),
m_Driver(NULL),
m_Gui(NULL),
m_Smgr(NULL),
m_TextBG(NULL),
m_Loading(NULL),
m_ResetDeviceRequest(false),
m_AudioDevice(NULL),
m_DialogFont(NULL)
{
	ProfileData* profil = ProfileData::GetInstance();// load the save

#ifdef IRON_ICE_ENGINE
	m_GamePath = "./game/";
#elif defined(IRON_ICE_EDITOR)
	m_GamePath = "./editor/";
#else
	do not compile
#endif

	core::LOCALE_DECIMAL_POINTS = irr::core::stringc(".,");

	m_AudioDevice = new IIAudioDevice();

	m_Language = LS_FRENCH;

	for(int i = 0; i < KET_MAX; ++i)
		m_KeyElements[i] = NULL;


	ResetDevice();


	for(u32 k = 0; k < LS_MAX; ++k)
		for(u32 i = 0; i < Engine::SFTID_MAX; ++i)
			ms_LanguageStrings[k][i] = NULL;

#ifdef IRON_ICE_ENGINE
	if(!m_CoreData.ReadCoreFromFile(GetGamePath("game.core").c_str()))
	{
#ifndef IRON_ICE_FINAL
		if(!m_CoreData.ReadCoreFromFileTxt(GetGamePath("game.txt").c_str()))
			m_Gui->addMessageBox(L"Erreur", L"Impossible de lire le fichier game.txt.");
		if(!m_CoreData.WriteCoreToFile(GetGamePath("game.core").c_str()))
			m_Gui->addMessageBox(L"Erreur", L"Impossible d'écrire le fichier game.core.");
#else
		m_Gui->addMessageBox(L"Erreur", L"Impossible de lire le fichier game.core.");
		assert(0);
#endif
	}

	ThemeColor = IColor(0, m_CoreData.Color[0], m_CoreData.Color[1], m_CoreData.Color[2]);
	ThemeColorSelection = IColor(0, m_CoreData.HighLightColor[0], m_CoreData.HighLightColor[1], m_CoreData.HighLightColor[2]);
	ThemeColorGlow = IColor(0, m_CoreData.GlowColor[0], m_CoreData.GlowColor[1], m_CoreData.GlowColor[2]);

	if(!m_CoreData.LangsAllowed[m_Language])// default language not allowed : change it
	{
		LanguageSettings oldLang = m_Language;
		for(u32 i = 0; i < Engine::LS_MAX; ++i)
		{
			if(m_CoreData.LangsAllowed[i])
			{
				SetLanguage((LanguageSettings)i);
				break;
			}
		}
		if(oldLang == m_Language)
		{
			m_Gui->addMessageBox(L"Erreur", L"Aucune langue n'est définie.");
			assert(0);
		}
	}
#endif

#ifdef IRON_ICE_ENGINE
	if(!ReadLanguageStringsFromFile(GetGamePath("system/lang/strings.lst").c_str()))
	{
#ifndef IRON_ICE_FINAL
		if(!ReadLanguageStringsFromFileTxt((GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_FRENCH])).c_str(), Engine::LS_FRENCH))
			m_Gui->addMessageBox(L"Erreur", (core::stringw("Impossible de lire le fichier ") + (GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_FRENCH]))).c_str());
		if(!ReadLanguageStringsFromFileTxt((GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_ENGLISH])).c_str(), Engine::LS_ENGLISH))
			m_Gui->addMessageBox(L"Erreur", (core::stringw("Impossible de lire le fichier ") + (GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_ENGLISH]))).c_str());
		if(!ReadLanguageStringsFromFileTxt((GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_GERMAN])).c_str(), Engine::LS_GERMAN))
			m_Gui->addMessageBox(L"Erreur", (core::stringw("Impossible de lire le fichier ") + (GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_GERMAN]))).c_str());
		if(!ReadLanguageStringsFromFileTxt((GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_SPANISH])).c_str(), Engine::LS_SPANISH))
			m_Gui->addMessageBox(L"Erreur", (core::stringw("Impossible de lire le fichier ") + (GetGamePath("system/lang/")+io::path(m_CoreData.LangsPath[Engine::LS_SPANISH]))).c_str());
		if(!WriteLanguageStringsToFile(GetGamePath("system/lang/strings.lst").c_str()))
			m_Gui->addMessageBox(L"Erreur", L"Impossible d'écrire le fichier system/lang/strings.lst.");
#else
		m_Gui->addMessageBox(L"Erreur", L"Impossible de lire le fichier system/lang/strings.lst.");
		assert(0);
#endif
	}
#endif

#ifdef IRON_ICE_EDITOR
	assert(m_Device);
	irr::io::IFileSystem *fs = m_Device->getFileSystem();
	irr::core::stringc current = fs->getWorkingDirectory();
	m_GamePath = current;
	m_GamePath += "/editor/";

	// create the cache dir 
	{
		char fileName[1024];
		sprintf(fileName, "%stemp", m_GamePath.c_str());
		mkdir(fileName);
	}
#endif
}

Engine::~Engine()
{
	SAFE_UNLOAD(m_TextBG);
	SAFE_UNLOAD(m_Loading);
	SAFE_UNLOAD(m_DialogFont);

	for(int i = 0; i < KET_MAX; ++i)
		SAFE_UNLOAD(m_KeyElements[i]);

	//m_Device->setGammaRamp(1.0f, 1.0f, 1.0f, 0.0f, 0.0f);

	m_Device->drop();

	SafeDelete(m_AudioDevice);

	for(u32 k = 0; k < LS_MAX; ++k)
		for(u32 i = 0; i < Engine::SFTID_MAX; ++i)
			SafeDeleteArray(ms_LanguageStrings[k][i]);

	Slider::Cleanup();
	ScrollBar::Cleanup();
}

#if defined(_IRR_WINDOWS_)
#include "windows.h"
#include "resource.h"
#endif

void Engine::ResetDevice()
{
	if(m_Device)
	{
		SAFE_UNLOAD(m_TextBG);
		SAFE_UNLOAD(m_Loading);

		SAFE_UNLOAD(m_DialogFont);

		for(int i = 0; i < KET_MAX; ++i)
			SAFE_UNLOAD(m_KeyElements[i]);

		Slider::Cleanup();
		ScrollBar::Cleanup();

		m_Device->closeDevice();
		m_Device->run();
		m_Device->drop();
	}

	// ask user for driver
	//video::E_DRIVER_TYPE driverType = video::EDT_DIRECT3D9;
	video::E_DRIVER_TYPE driverType = video::EDT_OPENGL;

#ifdef IRON_ICE_ENGINE
	// 1 try to load the profile
	// 2 if profile doesn't exist : create an autosave at ch0 sc0 at slot 0
	ProfileData* profile = ProfileData::GetInstance();
	if(!profile->IsLoaded())
	{
		// create a NULL device to detect screen resolution
		IrrlichtDevice *nulldevice = createDevice(video::EDT_NULL);
		core::dimension2d<u32> deskres = nulldevice->getVideoModeList()->getDesktopResolution();
		nulldevice -> drop();

		profile->m_ODS.m_DriverType = ProfileData::OptionDataSerializable::DT_OPENGL;
		profile->m_ODS.m_WindowSize = deskres;
#ifdef _DEBUG
		profile->m_ODS.m_FullScreen = false;
		profile->m_ODS.m_VSync = false;
#else
        profile->m_ODS.m_FullScreen = true;
		profile->m_ODS.m_VSync = true;
#endif

		profile->Initialize();// this creates the default profile
	}
	ms_WindowSize = profile->m_ODS.m_WindowSize;
	ms_FullScreen = profile->m_ODS.m_FullScreen;

	m_RenderSize = ms_WindowSize;
	RecomputeSize();

	switch(profile->m_ODS.m_DriverType)
	{
	case ProfileData::OptionDataSerializable::DT_D3D:
		driverType = video::EDT_DIRECT3D9;
		break;
	case ProfileData::OptionDataSerializable::DT_OPENGL:
		driverType = video::EDT_OPENGL;
		break;
	/*case ProfileData::OptionDataSerializable::DT_SOFTWARE:
		driverType = video::EDT_SOFTWARE;
		break;*/
	}

	bool useVsync = profile->m_ODS.m_VSync;
	m_Device = createDevice(driverType, ms_WindowSize, 32, ms_FullScreen, false, useVsync);

#if defined(_IRR_WINDOWS_)
	// WINDOWS ONLY
	{
		HWND hwndMyWindow = FindWindowEx( NULL, NULL, "CIrrDeviceWin32", NULL );
		HINSTANCE hInstance = (HINSTANCE)GetWindowLong(hwndMyWindow, GWL_HINSTANCE);

		HICON hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
		// Set big icon (ALT+TAB)
		SendMessage(hwndMyWindow,WM_SETICON,ICON_BIG,(LPARAM)hIcon);
		// Set little icon (titlebar)
		SendMessage(hwndMyWindow,WM_SETICON,ICON_SMALL,(LPARAM)hIcon);
#if(_WIN32_WINNT >= 0x0501)
		SendMessage(hwndMyWindow,WM_SETICON,ICON_SMALL2,(LPARAM)hIcon);
#endif
	}
#endif // #if defined(_IRR_WINDOWS_)
#elif defined(IRON_ICE_EDITOR)
	// 1 try to load the profile
	// 2 if profile doesn't exist : create it
	ProfileData* profile = ProfileData::GetInstance();
	if(!profile->IsLoaded())
	{
		profile->Initialize();// this creates the default profile
	}
	ms_WindowSize = profile->m_ODS.m_WindowSize;
	ms_FullScreen = profile->m_ODS.m_FullScreen;
	m_RenderSize = profile->m_ODS.m_RenderSize;

	RecomputeSize();

	switch(profile->m_ODS.m_DriverType)
	{
	case ProfileData::OptionDataSerializable::DT_D3D:
		driverType = video::EDT_DIRECT3D9;
		break;
	case ProfileData::OptionDataSerializable::DT_OPENGL:
		driverType = video::EDT_OPENGL;
		break;
	/*case ProfileData::OptionDataSerializable::DT_SOFTWARE:
		driverType = video::EDT_SOFTWARE;
		break;*/
	}

	bool useVsync = profile->m_ODS.m_VSync;
	m_Device = createDevice(driverType, ms_WindowSize, 32, ms_FullScreen, false, useVsync);

// #if defined(_IRR_WINDOWS_)
// 	// WINDOWS ONLY
// 	{
// 		HWND hwndMyWindow = FindWindowEx( NULL, NULL, "CIrrDeviceWin32", NULL );
// 		HINSTANCE hInstance = (HINSTANCE)GetWindowLong(hwndMyWindow, GWL_HINSTANCE);
//
// 		HICON hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
// 		// Set big icon (ALT+TAB)
// 		SendMessage(hwndMyWindow,WM_SETICON,ICON_BIG,(LPARAM)hIcon);
// 		// Set little icon (titlebar)
// 		SendMessage(hwndMyWindow,WM_SETICON,ICON_SMALL,(LPARAM)hIcon);
// #if(_WIN32_WINNT >= 0x0501)
// 		SendMessage(hwndMyWindow,WM_SETICON,ICON_SMALL2,(LPARAM)hIcon);
// #endif
// 	}
// #endif // #if defined(_IRR_WINDOWS_)
#endif

	if (m_Device == 0)
		return; // could not create selected driver.

	/* The creation was successful, now we set the event receiver and
		store pointers to the driver and to the gui environment. */

	m_Device->setWindowCaption(L"");
	m_Device->setResizable(true);

	m_Driver = m_Device->getVideoDriver();
	m_Smgr = m_Device->getSceneManager();
	m_Gui = m_Device->getGUIEnvironment();

#ifdef IRON_ICE_EDITOR // only used by editor
	if(!m_DialogFont)
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(GetGamePath("system/DialogFont.ttf"));

		const u32 size = 32;
		{
			CGUIFreetypeFont *font = new CGUIFreetypeFont(m_Driver);
			font->attach(face, size);
			font->AntiAlias = true;
			font->Transparency = true;

			m_DialogFont = font;
		}
		face->drop();// now we attached it we can drop the reference
	}
#endif// #ifdef IRON_ICE_EDITOR

	SAFE_LOAD_IMAGE(m_Driver, m_TextBG, GetGamePath("system/text_bg.png"), USE_PREMULTIPLIED_ALPHA);

#ifdef IRON_ICE_ENGINE
	SAFE_LOAD_IMAGE(m_Driver, m_Loading, GetGamePath("system/loading.png"), USE_PREMULTIPLIED_ALPHA);
#endif

	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_EMPTY_KEY1], GetGamePath("system/button/pc/PC_Key_Empty1.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_EMPTY_KEY2], GetGamePath("system/button/pc/PC_Key_Empty2.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_LMOUSE], GetGamePath("system/button/pc/PC_Mouse_Left.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_RMOUSE], GetGamePath("system/button/pc/PC_Mouse_Right.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_LEFT_KEY], GetGamePath("system/button/pc/PC_Key_Left.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_RIGHT_KEY], GetGamePath("system/button/pc/PC_Key_Right.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_UP_KEY], GetGamePath("system/button/pc/PC_Key_Up.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(m_Driver, m_KeyElements[KET_DOWN_KEY], GetGamePath("system/button/pc/PC_Key_Down.png"), USE_PREMULTIPLIED_ALPHA);


	//SetGamma(profile->m_ODS.m_GammaRamp);

	RecomputeSize();

	m_ResetDeviceRequest = false;

	if(!m_Device/* && profile->m_ODS.m_DriverType != ProfileData::OptionDataSerializable::DT_SOFTWARE*/)
	{
		printf("Failed to Reset the driver with current config\n");
		/*profile->m_ODS.m_DriverType = ProfileData::OptionDataSerializable::DT_SOFTWARE;
		ResetDevice();*/
	}
}

Engine* Engine::GetInstance()
{
	if(!ms_Instance)
		ms_Instance = new Engine();
	return ms_Instance;
}

void Engine::DeleteInstance()
{
	SafeDelete(ms_Instance);
}

void Engine::Update(float dt)
{
// 	static float t = 0;
// 	t += dt;
// 	static SmartPointer<AudioStream> sound1;
// 	if(!sound1.IsValid() && t > 0.0f)
// 	{
// 		sound1 = m_AudioDevice->OpenSound("test.mp3", true);
// 		sound1->SetVolume(0.5f);
// 		sound1->Play();
// 	}
// 	static SmartPointer<AudioStream> sound2;
// 	if(!sound2.IsValid() && t > 5.0f )
// 	{
// 		sound1->Stop();
//  		sound2 = m_AudioDevice->OpenSound("test2.mp3", true);
// 		sound2->SetVolume(0.5f);
//  		sound2->Play();
// 	}
//
// 	if(t > 7.0f && !sound1->IsPlaying())
// 	{
// 		//sound2->Stop();
// 		sound2 = sound1;
// 		sound1->Play();
// 	}

	bool updateSounds = m_Device->isWindowActive();// pause all sounds when window is not active
	m_AudioDevice->SetPaused(!updateSounds);
	if(updateSounds)
		m_AudioDevice->Update(dt);
}

void Engine::RecomputeSize()
{
	m_InnerScale = core::min_((float)m_RenderSize.Width/m_OriginalSize.Width,
		(float)m_RenderSize.Height/m_OriginalSize.Height) + core::ROUNDING_ERROR_f32;// round epsilon to prevent both black bands H and V

	float dx = (((float)m_RenderSize.Width - (float)((u32)(m_OriginalSize.Width*m_InnerScale)))*0.5f);
	float dy = (((float)m_RenderSize.Height - (float)((u32)(m_OriginalSize.Height*m_InnerScale)))*0.5f);
	m_InnerOffset = Position2((s32)dx, (s32)dy);

	if(m_DialogFont)
	{
		const u32 sizes[] = {32};
		{
			u32 size = core::max_(1U, (u32)((f32)sizes[0] * m_InnerScale));
			m_DialogFont->setSize(size);
		}
	}
}

void Engine::DumpResolutions()
{
	FILE* output = fopen(GetGamePath("res.txt").c_str(), "w");
	if (!output)
		return;

	video::IVideoModeList* list = m_Device? m_Device->getVideoModeList() : NULL;
	if(list)
	{
		for(s32 i = 0; i < list->getVideoModeCount(); ++i)
		{
			s32 bpp = list->getVideoModeDepth(i);			
			Dimension2 dim = list->getVideoModeResolution(i);

			fprintf(output, "%d * %d : %d bpp\n", dim.Width, dim.Height, bpp);
		}
	}

	fclose(output);
}

Dimension2 Engine::GetNextResolution(const Dimension2& currentRes)
{
	Dimension2 firstDim(0,0);
	video::IVideoModeList* list = m_Device? m_Device->getVideoModeList() : NULL;
	if(list)
	{
		for(s32 i = 0; i < list->getVideoModeCount(); ++i)
		{
			s32 bpp = list->getVideoModeDepth(i);
			/*if(bpp != 32)
				continue;*/

			Dimension2 dim = list->getVideoModeResolution(i);
			if(firstDim == D2Zero)
				firstDim = dim;

			if(dim.Width > currentRes.Width)
				return dim;
			else if(dim.Width == currentRes.Width && dim.Height > currentRes.Height)
				return dim;
		}
	}
	return firstDim;
}

Dimension2 Engine::GetPreviousResolution(const Dimension2& currentRes)
{
	Dimension2 lastDim(0,0);
	video::IVideoModeList* list = m_Device? m_Device->getVideoModeList() : NULL;
	if(list)
	{
		for(s32 i = list->getVideoModeCount()-1; i > 0; --i)
		{
			s32 bpp = list->getVideoModeDepth(i);
			if(bpp != 32)
				continue;

			Dimension2 dim = list->getVideoModeResolution(i);
			if(lastDim == D2Zero)
				lastDim = dim;

			if(dim.Width < currentRes.Width)
				return dim;
			else if(dim.Width == currentRes.Width && dim.Height < currentRes.Height)
				return dim;
		}
	}
	return lastDim;
}

void Engine::DrawLoadScreen()
{
	if(m_Driver && m_Loading)
	{
		m_Driver->beginScene(true, true, video::SColor(0,0,0,0));

		IColor tint = COLOR_WHITE;
		Dimension2 size = m_Loading->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(m_OriginalSize.Width>>1)*m_InnerScale), (s32)((f32)(m_OriginalSize.Height>>1)*m_InnerScale)) + m_InnerOffset;

		//innerScale = 1.0f;
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)m_InnerScale, (f32)m_InnerScale, 0.0f));

		draw2DImage(m_Driver, m_Loading, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);

		m_Driver->endScene();
	}
}

bool Engine::ReadLanguageStringsFromFile(const char* path)
{
	for(u32 k = 0; k < LS_MAX; ++k) 
		for(u32 i = 0; i < Engine::SFTID_MAX; ++i) 
			SafeDeleteArray(ms_LanguageStrings[k][i]);
	
	FILE* input = fopen(path, "rb");
	if (!input)
		return false;

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "LSTE", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	// read nb languages supported
	u32 nbLang = 0;
	fread(&nbLang, sizeof(u32), 1, input);

	// read nb entries
	u32 nbEntries = 0;
	fread(&nbEntries, sizeof(u32), 1, input);
	for(u32 k = 0; k < LS_MAX; ++k) 
	{		
		// read strings
		for(u32 i = 0; i < nbEntries; ++i)
		{
			core::stringw str;
			DeserializeStringW(str, input);
			if(str != L"")
			{
				ms_LanguageStrings[k][i] = new wchar_t[str.size()+1]();
				wcsncpy(ms_LanguageStrings[k][i], str.c_str(), str.size());
				ms_LanguageStrings[k][i][str.size()] = 0;
			}
		}
	}

	fclose(input);

	return true;
}

#ifndef IRON_ICE_FINAL
bool Engine::ReadLanguageStringsFromFileTxt(const char* path, Engine::LanguageSettings language/* = Engine::LS_FRENCH*/)
{
	if(language >= Engine::LS_MAX)
		return false;

	FILE* file = fopen(path, "r, ccs=UNICODE");
	if(!file)
		return false;

	wchar_t line[2048];


	u32 idx = 0;
	bool isReadingContent = false;
	core::stringw str1stPart;
	core::stringw str2ndPart;
	core::stringw str = L"";
	while(fgetws(line, 2048, file) != NULL) 
	{
		if(idx >= Engine::SFTID_MAX)
		{
			printf("Error : too many tokens in the file!\n");
			break;
		}
		str += line;
		bool nextLine = false;
		while(!nextLine)
		{
			if(idx >= Engine::SFTID_MAX)
			{
				printf("Error : too many tokens in the file!\n");
				break;
			}
			s32 start = str.findFirstChar(L"[");
			s32 end = str.findFirstChar(L"]");
			if(end >= 0)// found the end : we have a full entry start
			{
				str1stPart = str.subString(start, end-start, true);
				// balise end
				if(str1stPart.find(L"/token", 0) > 0)
				{
					s32 startContent = 0;
					s32 endContent = start;
					// trim one break at start if any
					if (str[startContent] == L'\r') // Mac or Windows breaks
					{
						++startContent;
						if (str[startContent] == L'\n') // Windows breaks
							++startContent;
					}
					else if (str[startContent] == L'\n') // Unix breaks
					{
						++startContent;
					}
					// trim one break at end if any
					if (endContent > 0 && str[endContent-1] == L'\n') // Unix or Windows breaks
					{
						--endContent;
						if (endContent > 0 && str[endContent-1] == L'\r') // Windows breaks
							--endContent;
					}
					else if (endContent > 0 && str[endContent-1] == L'\r') // Mac breaks
					{
						--endContent;
					}

					SafeDeleteArray(ms_LanguageStrings[language][idx]);
					core::stringw content = str.subString(startContent, endContent);
					ms_LanguageStrings[language][idx] = new wchar_t[content.size()+1]();
					wcsncpy(ms_LanguageStrings[language][idx], content.c_str(), content.size());
					ms_LanguageStrings[language][idx][content.size()] = 0;

					++idx;
					isReadingContent = false;
				}
				else if(str1stPart.find(L"token", 0) > 0)
				{
					assert(!isReadingContent);
					if(Get2ndPart(str, start, end, str2ndPart))// optionnal header
					{
						
					}
					isReadingContent = true;
				}
				
				str = str.subString(end+1, str.size()-(end+1));// next iteration
			}
			else if(start >= 0 || isReadingContent) // not completed line : wait for adding next line to the previous result
			{
				nextLine = true;
			}
			else// not a content : discard
			{
				str = L"";
				nextLine = true;
			}
		}
	}

	fclose(file);

	return true;
}

bool Engine::WriteLanguageStringsToFile(const char* path)
{
	FILE* output = fopen(path, "wb");
	if (!output)
		return false;

	char header[5] = "LSTE";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	// write nb languages supported
	u32 nbLang = Engine::LS_MAX;
	fwrite(&nbLang, sizeof(u32), 1, output);

	// write nb entries
	u32 nbEntries = Engine::SFTID_MAX;
	fwrite(&nbEntries, sizeof(u32), 1, output);
	for(u32 k = 0; k < nbLang; ++k)
	{
		// write strings
		for(u32 i = 0; i < nbEntries; ++i)
		{
			SerializeStringW(ms_LanguageStrings[k][i], output);
		}
	}

	fclose(output);

	return true;
}
#endif

bool Engine::CoreData::ReadCoreFromFile(const char* path)
{
	FILE* input = fopen(path, "rb");
	if (!input)
		return false;

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "CORE", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	// read nb languages supported
	u32 nbLang = 0;
	fread(&nbLang, sizeof(u32), 1, input);

	DeserializeStringW(AppName, input);

	AvailableLangs.clear();
	u32 size = 0;
	fread(&size, sizeof(u32), 1, input);
	for(u32 i = 0; i < size; ++i)
	{
		u8 val = 0;
		fread(&val, sizeof(u8), 1, input);
		AvailableLangs.push_back((LanguageSettings)val);
		LangsAllowed[val] = true;
	}

	for(u32 i = 0; i < nbLang; ++i)
		DeserializeStringW(LangsPath[i], input);

	fread(&LanguagePanelFontScale, sizeof(float), 1, input);
	fread(&StartPanelFontScale, sizeof(float), 1, input);
	fread(&MainMenuFontScale, sizeof(float), 1, input);
	fread(&SubMenuFontScale, sizeof(float), 1, input);
	fread(&PopupFontScale, sizeof(float), 1, input);
	fread(&DialogFontScale, sizeof(float), 1, input);


	fread(Color, sizeof(u8), 3, input);
	fread(HighLightColor, sizeof(u8), 3, input);
	fread(GlowColor, sizeof(u8), 3, input);

	fclose(input);

	return true;
}

#ifndef IRON_ICE_FINAL
static bool ReadCoreLineTxt(const core::stringw& line, core::stringw& token, core::stringw& val)
{
	s32 idx = line.findFirstChar(L"=");
	if(idx < 0)
		return false;

	token = line.subString(0, idx).trim();
	val = line.subString(idx+1, line.size()-idx-1).trim();
	return true;
}

bool Engine::CoreData::ReadCoreFromFileTxt(const char* path)
{
	FILE* file = fopen(path, "r, ccs=UNICODE");
	if (!file)
		return false;

	AvailableLangs.clear();

	wchar_t line[1024];
	core::stringw token;
	core::stringw val;
	while (fgetws(line, 1024, file) != NULL)
	{
		if(!ReadCoreLineTxt(line, token, val))
		{
			continue;
		}

		if(token.find("AppName") != std::string::npos)
		{
			AppName = val;
		}
		else if(token.find("LangFrench") != std::string::npos)
		{
			LangsPath[Engine::LS_FRENCH] = val;
			LangsAllowed[Engine::LS_FRENCH] = !val.empty();
			if(!val.empty())
				AvailableLangs.push_back(Engine::LS_FRENCH);
		}
		else if(token.find("LangEnglish") != std::string::npos)
		{
			LangsPath[Engine::LS_ENGLISH] = val;
			LangsAllowed[Engine::LS_ENGLISH] = !val.empty();
			if(!val.empty())
				AvailableLangs.push_back(Engine::LS_ENGLISH);
		}
		else if(token.find("LangGerman") != std::string::npos)
		{
			LangsPath[Engine::LS_GERMAN] = val;
			LangsAllowed[Engine::LS_GERMAN] = !val.empty();
			if(!val.empty())
				AvailableLangs.push_back(Engine::LS_GERMAN);
		}
		else if(token.find("LangSpanish") != std::string::npos)
		{
			LangsPath[Engine::LS_SPANISH] = val;
			LangsAllowed[Engine::LS_SPANISH] = !val.empty();
			if(!val.empty())
				AvailableLangs.push_back(Engine::LS_SPANISH);
		}
		else if(token.find("LanguagePanelFontScale") != std::string::npos)
		{
			LanguagePanelFontScale = wtof(val.c_str());
		}
		else if(token.find("StartPanelFontScale") != std::string::npos)
		{
			StartPanelFontScale = wtof(val.c_str());
		}
		else if(token.find("MainMenuFontScale") != std::string::npos)
		{
			MainMenuFontScale = wtof(val.c_str());
		}
		else if(token.find("SubMenuFontScale") != std::string::npos)
		{
			SubMenuFontScale = wtof(val.c_str());
		}
		else if(token.find("PopupFontScale") != std::string::npos)
		{
			PopupFontScale = wtof(val.c_str());
		}
		else if(token.find("DialogFontScale") != std::string::npos)
		{
			DialogFontScale = wtof(val.c_str());
		}
		else if(token.find("FontColor") != std::string::npos)
		{
			if(val[0] == L'#' && val.size() == 7)
			{
				wchar_t color[3];
				color[2] = 0;

				memcpy(color, &val[1], sizeof(wchar_t)*2);
				Color[0] = (u8)wtoi(color, 16);
				memcpy(color, &val[3], sizeof(wchar_t)*2);
				Color[1] = (u8)wtoi(color, 16);
				memcpy(color, &val[5], sizeof(wchar_t)*2);
				Color[2] = (u8)wtoi(color, 16);
			}
		}
		else if(token.find("FontHighlightColor") != std::string::npos)
		{
			if(val[0] == L'#' && val.size() == 7)
			{
				wchar_t color[3];
				color[2] = 0;

				memcpy(color, &val[1], sizeof(wchar_t)*2);
				HighLightColor[0] = (u8)wtoi(color, 16);
				memcpy(color, &val[3], sizeof(wchar_t)*2);
				HighLightColor[1] = (u8)wtoi(color, 16);
				memcpy(color, &val[5], sizeof(wchar_t)*2);
				HighLightColor[2] = (u8)wtoi(color, 16);
			}
		}
		else if(token.find("FontGlowColor") != std::string::npos)
		{
			if(val[0] == L'#' && val.size() == 7)
			{
				wchar_t color[3];
				color[2] = 0;

				memcpy(color, &val[1], sizeof(wchar_t)*2);
				GlowColor[0] = (u8)wtoi(color, 16);
				memcpy(color, &val[3], sizeof(wchar_t)*2);
				GlowColor[1] = (u8)wtoi(color, 16);
				memcpy(color, &val[5], sizeof(wchar_t)*2);
				GlowColor[2] = (u8)wtoi(color, 16);
			}
		}
	}

	fclose(file);

	return true;
}

bool Engine::CoreData::WriteCoreToFile(const char* path)
{
	FILE* output = fopen(path, "wb");
	if (!output)
		return false;

	char header[5] = "CORE";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version


	// write nb languages supported
	u32 nbLang = Engine::LS_MAX;
	fwrite(&nbLang, sizeof(u32), 1, output);

	SerializeStringW(AppName, output);

	u32 size = AvailableLangs.size();
	fwrite(&size, sizeof(u32), 1, output);
	for(u32 i = 0; i < size; ++i)
	{
		u8 val = (u8)AvailableLangs[i];
		fwrite(&val, sizeof(u8), 1, output);
	}

	for(u32 i = 0; i < Engine::LS_MAX; ++i)
		SerializeStringW(LangsPath[i], output);

	fwrite(&LanguagePanelFontScale, sizeof(float), 1, output);
	fwrite(&StartPanelFontScale, sizeof(float), 1, output);
	fwrite(&MainMenuFontScale, sizeof(float), 1, output);
	fwrite(&SubMenuFontScale, sizeof(float), 1, output);
	fwrite(&PopupFontScale, sizeof(float), 1, output);
	fwrite(&DialogFontScale, sizeof(float), 1, output);

	fwrite(Color, sizeof(u8), 3, output);
	fwrite(HighLightColor, sizeof(u8), 3, output);
	fwrite(GlowColor, sizeof(u8), 3, output);

	fclose(output);

	return true;
}
#endif


#ifdef IRON_ICE_ENGINE
// ProfileData
ProfileData::ProfileData() : m_IsLoaded(false)
{
	m_IsLoaded = ReadSave();
}

ProfileData::~ProfileData()
{
	m_IsLoaded = false;
}

void ProfileData::Initialize()
{
	// create a default profile

	// ProfileDataSerializable
	{
		// create an autosave at ch0 sc0 (initial dialog) at slot 0

		time(&m_PDS.m_DateTimeCreation);
		m_PDS.m_Saves[PROFILE_AUTOSAVE_SLOT].Save(0, 0, 0);

#ifdef _DEBUG
		m_PDS.m_ProgressionCH = 0xff;
		m_PDS.m_ProgressionSC = 0xff;
#else
		m_PDS.m_ProgressionCH = 0;
		m_PDS.m_ProgressionSC = 0;
#endif
		for(u32 i = 0; i < PROFILE_CHAPTERS_MAX; ++i)
			m_PDS.m_TimeCH[i] = 0.0f;
		m_PDS.m_nbQTESuccess = 0;
		m_PDS.m_nbQTEFail = 0;
	}

	// KeyMappingSerializable
	{
		// create a default mapping
		EKEY_CODE UNDEFINED_KEY = (EKEY_CODE)0x0;
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_LEFT,			KEY_KEY_Q);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_RIGHT,			KEY_KEY_D);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_UP,			KEY_KEY_Z);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_DOWN,			KEY_KEY_S);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE1,			KEY_LBUTTON);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE2,			KEY_SPACE);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE3,			KEY_KEY_R);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE4,			KEY_KEY_F);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE5,			KEY_KEY_A);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE6,			KEY_KEY_E);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_PAUSE,			KEY_ESCAPE);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QUICK_MENU,	KEY_RETURN);

		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_LEFT,			KEY_LEFT);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_RIGHT,			KEY_RIGHT);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_UP,			KEY_UP);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_DOWN,			KEY_DOWN);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE1,			KEY_RETURN);// Validation
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE2,			KEY_ESCAPE);// Cancel
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE3,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE4,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE5,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE6,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_PAUSE,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QUICK_MENU,	UNDEFINED_KEY);
	}
	WriteSave();

	m_IsLoaded = true;
}

ProfileData* ProfileData::GetInstance()
{
	if(!ms_Instance)
		ms_Instance = new ProfileData();
	return ms_Instance;
}

void ProfileData::DeleteInstance()
{
	SafeDelete(ms_Instance);
}

#define ENCODE_KEY1 0xABAD1DEA
#define ENCODE_GEN_KEY2(major, minor, address1, address2) ((((u8)major)<<24)+(((u8)minor)<<16)+(u16)(*((u32*)address1)+0xDEADBEAF - *((u32*)address2)))
#define ENCODE_U32(data, size, key) \
do \
{ \
	u32 d = (size>>2); \
	u32* pt = (u32*)&data; \
	u32 k2 = (u32)key; \
	do \
	{ \
		 *pt += ENCODE_KEY1 - k2; \
		 ++pt; \
	}while(--d); \
}while(0)

#define DECODE_U32(data, size, key) \
do \
{ \
	u32 d = (size>>2); \
	u32* pt = (u32*)&data; \
	u32 k2 = (u32)key; \
	do \
	{ \
		*pt -= ENCODE_KEY1 - k2; \
		++pt; \
	}while(--d); \
}while(0)

bool ProfileData::WriteSave()
{
	FILE* output = fopen("./game/profile/profile.svg", "wb");
	if(!output)
	{
		return false;
	}

	char header[5] = "SVGP";
	fwrite(header, sizeof(char), 4, output);// header

	//current version 1.1
	unsigned char version[2] = {1, 1};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	u32 key = 0;

	// write profile
	{
		ProfileDataSerializable encodedData = m_PDS;
		key = ENCODE_GEN_KEY2(version[0], version[1], &encodedData.m_DateTimeCreation, &encodedData.m_Saves[0].DateTime);
		ENCODE_U32(encodedData, sizeof(ProfileDataSerializable), key);
		fwrite(&encodedData, sizeof(ProfileDataSerializable), 1, output);
	}
	key += ENCODE_KEY1;
	{
		OptionDataSerializable encodedData = m_ODS;
		ENCODE_U32(encodedData, sizeof(OptionDataSerializable), key);
		fwrite(&encodedData, sizeof(OptionDataSerializable), 1, output);
	}
	key += ENCODE_KEY1;
	{
		KeyMappingSerializable encodedData = m_KMS;
		ENCODE_U32(encodedData, sizeof(KeyMappingSerializable), key);
		fwrite(&encodedData, sizeof(KeyMappingSerializable), 1, output);
	}

	fclose(output);

	return true;
}

bool ProfileData::ReadSave()
{
	FILE* input = fopen("./game/profile/profile.svg", "rb");
	if(!input)
	{
		return false;
	}

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "SVGP", 4) != 0)
	{
		fclose(input);
		return false;
	}

	//current version 1.1
	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	u32 key = 0;

	// read profile
	{
		ProfileDataSerializable decodedData;
		fread(&decodedData, sizeof(ProfileDataSerializable), 1, input);
		key = ENCODE_GEN_KEY2(version[0], version[1], &decodedData.m_DateTimeCreation, &decodedData.m_Saves[0].DateTime);
		DECODE_U32(decodedData, sizeof(ProfileDataSerializable), key);
		m_PDS = decodedData;
	}
	key += ENCODE_KEY1;
	{
		OptionDataSerializable decodedData;
		fread(&decodedData, sizeof(OptionDataSerializable), 1, input);
		DECODE_U32(decodedData, sizeof(OptionDataSerializable), key);
		m_ODS = decodedData;
	}
	key += ENCODE_KEY1;
	{
		KeyMappingSerializable decodedData;
		fread(&decodedData, sizeof(KeyMappingSerializable), 1, input);
		DECODE_U32(decodedData, sizeof(KeyMappingSerializable), key);
		m_KMS = decodedData;
	}

	fclose(input);

	return true;
}
#elif defined(IRON_ICE_EDITOR)
// ProfileData
ProfileData::ProfileData() : m_IsLoaded(false)
{
	char cCurrentPath[FILENAME_MAX];
	GetCurrentDir(cCurrentPath, sizeof(cCurrentPath));
	m_RootFullPath = cCurrentPath;

	m_IsLoaded = ReadSave();
}

ProfileData::~ProfileData()
{
	m_IsLoaded = false;
}

void ProfileData::Initialize()
{
	// create a default profile

	// KeyMappingSerializable
	{
		// create a default mapping
		EKEY_CODE UNDEFINED_KEY = (EKEY_CODE)0x0;
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_LEFT,			KEY_KEY_Q);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_RIGHT,			KEY_KEY_D);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_UP,			KEY_KEY_Z);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_DOWN,			KEY_KEY_S);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE1,			KEY_LBUTTON);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE2,			KEY_SPACE);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE3,			KEY_KEY_R);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE4,			KEY_KEY_F);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE5,			KEY_KEY_A);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QTE6,			KEY_KEY_E);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_PAUSE,			KEY_ESCAPE);
		m_KMS.MapKey1(KeyMappingSerializable::II_KEY_QUICK_MENU,	KEY_RETURN);

		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_LEFT,			KEY_LEFT);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_RIGHT,			KEY_RIGHT);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_UP,			KEY_UP);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_DOWN,			KEY_DOWN);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE1,			KEY_RETURN);// Validation
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE2,			KEY_ESCAPE);// Cancel
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE3,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE4,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE5,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QTE6,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_PAUSE,			UNDEFINED_KEY);
		m_KMS.MapKey2(KeyMappingSerializable::II_KEY_QUICK_MENU,	UNDEFINED_KEY);
	}

	WriteSave();

	m_IsLoaded = true;
}

ProfileData* ProfileData::GetInstance()
{
	if(!ms_Instance)
		ms_Instance = new ProfileData();
	return ms_Instance;
}

void ProfileData::DeleteInstance()
{
	SafeDelete(ms_Instance);
}

bool ProfileData::WriteSave()
{
	io::path path = m_RootFullPath;
	path += "/editor/profile/profile.sve";
	FILE* output = fopen(path.c_str(), "wb");
	if(!output)
	{
		return false;
	}

	char header[5] = "SVEP";
	fwrite(header, sizeof(char), 4, output);// header

	unsigned char version[2] = {1, 0};// major version, minor version
	fwrite(version, sizeof(unsigned char), 2, output);// version

	fwrite(&m_ODS, sizeof(OptionDataSerializable), 1, output);
	fwrite(&m_KMS, sizeof(KeyMappingSerializable), 1, output);

	fclose(output);

	return true;
}

bool ProfileData::ReadSave()
{
	io::path path = m_RootFullPath;
	path += "/editor/profile/profile.sve";
	FILE* input = fopen(path.c_str(), "rb");
	if(!input)
	{
		return false;
	}

	char header[4];
	fread(header, sizeof(char), 4, input);// header
	if(strncmp(header, "SVEP", 4) != 0)
	{
		fclose(input);
		return false;
	}

	unsigned char version[2];// major version, minor version
	fread(version, sizeof(unsigned char), 2, input);// version

	fread(&m_ODS, sizeof(OptionDataSerializable), 1, input);
	fread(&m_KMS, sizeof(KeyMappingSerializable), 1, input);

	fclose(input);

	return true;
}
#endif

