﻿#include "panel.h"
#include "engine.h"
#include "utils.h"

#define PANEL_FADETIME 0.5f

enum StringDesc {SD_START = Engine::SFTID_StartMenuPanel, SD_MAX};

// StartMenuPanel
StartMenuPanel::StartMenuPanel() : IPanel(), m_BG(NULL), m_Font(NULL), m_Title(NULL)
{
	
}

StartMenuPanel::~StartMenuPanel()
{
	UnloadPanel();
}

void StartMenuPanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.StartPanelFontScale;

	SAFE_LOAD_IMAGE(driver, m_BG, engine->GetGamePath("system/Menu_BG.jpg"), USE_PREMULTIPLIED_ALPHA);

	if(!m_Font)
	{
		u32 size = (u32)(80.0f * innerScale * fontScale);

		CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));
		font->attach(face, size, USE_PREMULTIPLIED_ALPHA, (size>>2));
		face->drop();// now we attached it we can drop the reference
		font->AntiAlias = true;
		font->Transparency = true;

		m_Font = font;
	}

	SAFE_LOAD_IMAGE(driver, m_Title, engine->GetGamePath("system/01_SL_logo.png"), USE_PREMULTIPLIED_ALPHA);

	OnResize();

	StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEIN));

	IPanel::LoadPanel();
}

void StartMenuPanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_BG);

	SAFE_UNLOAD(m_Font);

	SAFE_UNLOAD(m_Title);

	IPanel::UnloadPanel();
}

void StartMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.StartPanelFontScale;

	if(m_Font)
	{
		u32 size = (u32)(80.0f * innerScale * fontScale);
		m_Font->setSize(size, size>>2);
	}
}

void StartMenuPanel::Update(float dt) 
{
	IPanel::Update(dt);

}

void StartMenuPanel::Render()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor color = COLOR_BLACK;
	fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);

	if(m_BG)
	{
		IColor tint = COLOR_WHITE;
		
		Dimension2 size = m_BG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_BG, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	if(m_Title)
	{
		float ratio = core::abs_(cosf(0.15f*core::PI*m_Time));
		IColor tint = MathUtils::GetInterpolatedColor(COLOR_WHITE, IColor(200, 255, 255, 255), ratio);
		
		Dimension2 size = m_Title->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);

		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(0 + (size.Height>>1) + 60)*innerScale)) + innerOffset;
		
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_Title, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	if(m_Font)
	{
		float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
		IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(50), ratio);
		
		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height - (m_Font->getSize()>>1) - 100)*innerScale)) + innerOffset;

		core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_START));
		m_Font->drawGauss(str, Rectangle2(pos, Dimension2(0, 0)), tint, true, true);
		m_Font->draw(str, Rectangle2(pos, Dimension2(0, 0)), COMPUTE_THEME_COLOR(200), true, true);
	}

	if(IsFading())
	{
		IColor fadeColor = COLOR_BLACK;
		IColor transparent = COLOR_TRANSPARENT;
		//fade in
		float ratio = m_Fader.GetRatio();
		IColor color = MathUtils::GetInterpolatedColor(fadeColor, transparent, ratio);
		fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);
	}
}

bool StartMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			m_TransitToChildPanelIndex = 0;
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		// ANY KEY
		{
			StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			m_TransitToChildPanelIndex = 0;
		}
	}

	return false;
}

#undef PANEL_FADETIME
