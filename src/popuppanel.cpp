﻿#include "panel.h"
#include "engine.h"
#include "utils.h"



#define PANEL_FADETIME 0.5f

enum StringDesc {SD_BTN_YES = Engine::SFTID_PopupPanel, SD_BTN_NO, SD_BTN_OK, SD_BTN_CANCEL, SD_MAX};

// LogoPanel
PopupPanel::PopupPanel(bool usePremultipliedAlpha/* = USE_PREMULTIPLIED_ALPHA*/) : IPanel()
, m_Window(NULL)
, m_Type(PT_OK)
, m_IsVisible(false)
, m_Result(PRT_NONE)
, m_UsePremultipliedAlpha(usePremultipliedAlpha)
, m_CurrentSelection(0)
{
	for(int i = 0; i < 3; ++i)
		m_Font[i] = NULL;
}

PopupPanel::~PopupPanel()
{
	UnloadPanel();
}

void PopupPanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.PopupFontScale;

	SAFE_LOAD_IMAGE(driver, m_Window, engine->GetGamePath("system/popup.png"), m_UsePremultipliedAlpha);

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[3] = {40, 48, 60};
		for(int i = 0; i < 3; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, m_UsePremultipliedAlpha, size/6);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	OnResize();

	IPanel::LoadPanel();
}

void PopupPanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_Window);

	for(int i = 0; i < 3; ++i)
		SAFE_UNLOAD(m_Font[i]);

	IPanel::UnloadPanel();
}

void PopupPanel::Display(PopupType type, const wchar_t* const message = NULL) 
{
	m_Type = type; 
	m_Message = message;
	m_Result = PRT_NONE;
	m_Time = 0;
	m_IsVisible = true;
	m_CurrentSelection = (m_Type==PT_OK)? 0 : 1;// default is no/cancel
	OnResize();
}

void PopupPanel::Update(float dt) 
{
	if(!m_IsVisible)
		return;

	IPanel::Update(dt);
}


void PopupPanel::OnResize()
{
	if(!m_IsVisible)
		return;

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.PopupFontScale;
	
	const u32 sizes[3] = {40, 48, 60};
	if(m_Font[0])
	{
		for(int i = 0; i < 3; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			m_Font[i]->setSize(size, size/6);
		}
	}

	if(m_Window)
	{
		const Dimension2& size = m_Window->getSize();
		Position2 pos = Position2((s32)(0.5f*originalSize.Width*innerScale) + innerOffset.X, 
			(s32)(0.5f*originalSize.Height*innerScale) + innerOffset.Y);

		if(m_Font[2])
		{
			Position2 upOffset(0, (s32)(innerScale*size.Height*0.45f));
			Position2 pos2(pos - upOffset);

			pos2 = pos + upOffset;
			pos2.Y -= m_Font[2]->getSize() + (u32)((float)20 * innerScale);
			switch(m_Type)
			{
			case PT_OK:
				{
					const Dimension2& dim1 = m_Font[2]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_OK));
					Position2 cornerOffset1(dim1.Width>>1, dim1.Height>>1);
					m_Buttons[0].Area = Rectangle2(pos2-cornerOffset1, pos2+cornerOffset1);
					m_Buttons[1].Area = Rectangle2(0,0,0,0);
				}
				break;
			case PT_OK_CANCEL:
				{
					Position2 btnOffset((s32)(innerScale*size.Width*0.25f), 0);
					const Dimension2& dim1 = m_Font[2]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_OK));
					const Dimension2& dim2 = m_Font[2]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_CANCEL));
					Position2 cornerOffset1(dim1.Width>>1, dim1.Height>>1);
					Position2 cornerOffset2(dim2.Width>>1, dim2.Height>>1);
					m_Buttons[0].Area = Rectangle2(pos2-btnOffset-cornerOffset1, pos2-btnOffset+cornerOffset1);
					m_Buttons[1].Area = Rectangle2(pos2+btnOffset-cornerOffset2, pos2+btnOffset+cornerOffset2);
				}
				break;
			case PT_YES_NO:
				{
					Position2 btnOffset((s32)(innerScale*size.Width*0.25f), 0);
					const Dimension2& dim1 = m_Font[2]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_YES));
					const Dimension2& dim2 = m_Font[2]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_NO));
					Position2 cornerOffset1(dim1.Width>>1, dim1.Height>>1);
					Position2 cornerOffset2(dim2.Width>>1, dim2.Height>>1);
					m_Buttons[0].Area = Rectangle2(pos2-btnOffset-cornerOffset1, pos2-btnOffset+cornerOffset1);
					m_Buttons[1].Area = Rectangle2(pos2+btnOffset-cornerOffset2, pos2+btnOffset+cornerOffset2);
				}
				break;
			default:
				assert(0);
				break;
			}
			m_Buttons[0].IsSelected = false;
			m_Buttons[1].IsSelected = false;
		}
	}
}

void PopupPanel::Render()
{
	if(!m_IsVisible)
		return;

	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	if(m_Window)
	{
		IColor tint = IColor(150,255,0,0);

		const Dimension2& size = m_Window->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)(0.5f*originalSize.Width*innerScale) + innerOffset.X, 
			(s32)(0.5f*originalSize.Height*innerScale) + innerOffset.Y);

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_Window, sourceRect, mat, true, tint, true, m_UsePremultipliedAlpha);

		if(m_Font[2] && m_Font[1] && m_Font[0])
		{
			float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
			IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(255>>1), ratio);

			Position2 upOffset(0, (s32)(innerScale*size.Height*0.45f));
			Position2 pos2(pos - upOffset);

			IColor color = COLOR_WHITE;
			m_Font[0]->draw(m_Message, Rectangle2(pos2, D2Zero), color, true, false);
			pos2 = pos + upOffset;
			pos2.Y -= m_Font[2]->getSize() + (u32)((float)20 * innerScale);
			switch(m_Type)
			{
			case PT_OK:
				{
					m_Font[2]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_OK), Rectangle2(pos2, D2Zero), tint, true, true);
					((m_CurrentSelection==0)?m_Font[2]:m_Font[1])->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_OK), Rectangle2(pos2, D2Zero), color, true, true);
				}
				break;
			case PT_OK_CANCEL:
				{
					Position2 btnOffset((s32)(innerScale*size.Width*0.25f), 0);
					m_Font[2]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_OK+m_CurrentSelection), Rectangle2(pos2+((m_CurrentSelection==0)?-btnOffset:btnOffset), D2Zero), tint, true, true);
					((m_CurrentSelection==0)?m_Font[2]:m_Font[1])->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_OK), Rectangle2(pos2-btnOffset, D2Zero), color, true, true);
					((m_CurrentSelection==1)?m_Font[2]:m_Font[1])->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_CANCEL), Rectangle2(pos2+btnOffset, D2Zero), color, true, true);
				}
				break;
			case PT_YES_NO:
				{
					Position2 btnOffset((s32)(innerScale*size.Width*0.25f), 0);
					m_Font[2]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_YES+m_CurrentSelection), Rectangle2(pos2+((m_CurrentSelection==0)?-btnOffset:btnOffset), D2Zero), tint, true, true);
					((m_CurrentSelection==0)?m_Font[2]:m_Font[1])->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_YES), Rectangle2(pos2-btnOffset, D2Zero), color, true, true);
					((m_CurrentSelection==1)?m_Font[2]:m_Font[1])->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_BTN_NO), Rectangle2(pos2+btnOffset, D2Zero), color, true, true);
				}
				break;
			default:
				assert(0);
				break;
			}
		}
	}

	if(IsFading())
	{
// 		IColor fadeColor = BLACK;
// 		IColor transparent = TRANSPARENT;
// 		//fade in
// 		float ratio = m_Fader.GetRatio();
// 		IColor color = MathUtils::GetInterpolatedColor(fadeColor, transparent, ratio);
// 		fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);
	}
}

bool PopupPanel::OnEvent(const SEvent& event)
{
	if(!m_IsVisible)
		return false;

	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return true;

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
			for(int i = 0; i < 2; ++i)
			{
				m_Buttons[i].IsSelected = false;
				if(m_Buttons[i].Area.isPointInside(mousePos))
				{
					m_CurrentSelection = i;
					m_Buttons[m_CurrentSelection].IsSelected = true;
				}
			}
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_Buttons[0].IsSelected || m_Buttons[1].IsSelected)
			{
				switch(m_Type)
				{
				case PT_OK:
					if(m_Buttons[0].IsSelected)
					{
						m_Result = PRT_OK;
					}
					m_IsVisible = false;
					break;
				case PT_OK_CANCEL:
					if(m_Buttons[0].IsSelected)
					{
						m_Result = PRT_OK;
					}
					else if(m_Buttons[1].IsSelected)
					{
						m_Result = PRT_CANCEL;
					}
					m_IsVisible = false;
					break;
				case PT_YES_NO:
					if(m_Buttons[0].IsSelected)
					{
						m_Result = PRT_YES;
					}
					else if(m_Buttons[1].IsSelected)
					{
						m_Result = PRT_NO;
					}
					m_IsVisible = false;
					break;
				default:
					assert(0);
					break;
				}
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				switch(m_Type)
				{
				case PT_OK:
					m_Result = PRT_OK;
					m_IsVisible = false;
					break;
				case PT_OK_CANCEL:
					m_Result = PRT_OK;
					m_IsVisible = false;
					break;
				case PT_YES_NO:
					m_Result = PRT_NO;
					m_IsVisible = false;
					break;
				default:
					assert(0);
					break;
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				switch(m_Type)
				{
				case PT_OK:
					m_Result = PRT_OK;
					m_IsVisible = false;
					break;
				case PT_OK_CANCEL:
					m_Result = (m_CurrentSelection==0)? PRT_OK:PRT_CANCEL;
					m_IsVisible = false;
					break;
				case PT_YES_NO:
					m_Result = (m_CurrentSelection==0)? PRT_YES:PRT_NO;
					m_IsVisible = false;
					break;
				default:
					assert(0);
					break;
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentSelection > 0)
					--m_CurrentSelection;
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentSelection < 1 && m_Type != PT_OK)
					++m_CurrentSelection;
			}
		}
	}

	return true;
}

#undef PANEL_FADETIME

