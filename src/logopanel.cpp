﻿#include "panel.h"
#include "engine.h"
#include "utils.h"



#define PANEL_FADETIME 0.5f


// LogoPanel
LogoPanel::LogoPanel() : IPanel(), m_BG(NULL), m_Phase(0)
{
	for(int i = 0; i < 3; ++i)
		m_Logo[i] = NULL;
}

LogoPanel::~LogoPanel()
{
	UnloadPanel();
}

void LogoPanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_LOAD_IMAGE(driver, m_BG, Engine::GetInstance()->GetGamePath("system/Intro_BG.jpg"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Logo[0], Engine::GetInstance()->GetGamePath("system/Intro_001.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Logo[1], Engine::GetInstance()->GetGamePath("system/Intro_002.png"), USE_PREMULTIPLIED_ALPHA);
	SAFE_LOAD_IMAGE(driver, m_Logo[2], Engine::GetInstance()->GetGamePath("system/Intro_003.png"), USE_PREMULTIPLIED_ALPHA);

	IPanel::LoadPanel();
}

void LogoPanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_Logo[0]);
	SAFE_UNLOAD(m_Logo[1]);
	SAFE_UNLOAD(m_Logo[2]);
	SAFE_UNLOAD(m_BG);

	IPanel::UnloadPanel();
}

void LogoPanel::Update(float dt) 
{
	IPanel::Update(dt);

	if(m_Time > 6.0f && m_TransitToChildPanelIndex != 0)
	{
		if(m_Phase == 2)
		{
			m_TransitToChildPanelIndex = 0;
			StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
		}
		else
		{
			ResetPanel();
			++m_Phase;
		}
	}
}

void LogoPanel::Render()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	if(m_BG)
	{
		float ratio = (m_Time-1.0f)/2.0f;
		ratio *= core::abs_(ratio);
		IColor tint = MathUtils::GetInterpolatedColor((m_Phase%2 ==0)? COLOR_BLACK : COLOR_WHITE, (m_Phase%2 ==0)? COLOR_WHITE : COLOR_BLACK, ratio);

		Dimension2 size = m_BG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		//innerScale = 1.0f;
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_BG, sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	if(m_Phase > 0 && m_Logo[m_Phase-1])
	{
		float ratio = m_Time;
		ratio *= core::abs_(ratio);
		ratio = 1.0f-ratio;
		IColor tint = MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, ratio);
		Dimension2 size = m_Logo[m_Phase-1]->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		//innerScale = 1.0f;
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_Logo[m_Phase-1], sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	if(m_Logo[m_Phase])
	{
		float ratio = m_Time;
		ratio *= core::abs_(ratio);
		IColor tint = MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, ratio);
		Dimension2 size = m_Logo[m_Phase]->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos;
		if(m_Phase == 2)
			pos = Position2((s32)((f32)(originalSize.Width - (size.Width>>1) - 100)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;
		else
			pos = Position2((s32)((f32)(originalSize.Width>>1)*innerScale), (s32)((f32)(originalSize.Height>>1)*innerScale)) + innerOffset;

		//innerScale = 1.0f;
		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

 		draw2DImage(driver, m_Logo[m_Phase], sourceRect, mat, true, tint, true, USE_PREMULTIPLIED_ALPHA);
	}

	if(IsFading())
	{
		IColor fadeColor = COLOR_BLACK;
		IColor transparent = COLOR_TRANSPARENT;
		//fade in
		float ratio = m_Fader.GetRatio();
		IColor color = MathUtils::GetInterpolatedColor(fadeColor, transparent, ratio);
		fill2DRect(driver, Rectangle2(P2Zero, renderSize), color, color, color, color, USE_PREMULTIPLIED_ALPHA);
	}
}

bool LogoPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if(m_Time < 3.0f)
		return false;

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_Phase == 2)
			{
				m_TransitToChildPanelIndex = 0;
				StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			}
			else
			{
				ResetPanel();
				++m_Phase;
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
		if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1) || 
			event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
		{
			if(m_Phase == 2)
			{
				m_TransitToChildPanelIndex = 0;
				StartFade(Fader(PANEL_FADETIME, Fader::FT_FADEOUT));
			}
			else
			{
				ResetPanel();
				++m_Phase;
			}
		}
	}

	return false;
}

#undef PANEL_FADETIME

