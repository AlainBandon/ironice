#ifndef SCENE_H
#define SCENE_H

#include "utils.h"
#include "engine.h"
#include "sprite.h"
#ifdef IRON_ICE_EDITOR
#include "md5.h"
#endif


#define DELAY_BETWEEN_DIALOGS 1.0f
#define TEXTURE_SIZE_LIMIT 2048

class AudioStream;

// A structure containing informations to animate any sprite from the bank over time
struct SpriteDataScene
{
	SpriteDataScene(): m_Index(0), m_MirrorH(false), m_MirrorV(false), m_Loop(false), m_LoopAnim(false) {}
	SpriteDataScene(u32 spriteIndex): m_Index(spriteIndex), m_MirrorH(false), m_MirrorV(false), m_Loop(false), m_LoopAnim(false) {}
	~SpriteDataScene() {m_Keys.clear();}

	u32 m_Index;// index of a sprite inside the sprite bank
	TArray<KeyTime> m_Keys;// a list of keytime to "animate" the sprite over time
	bool m_MirrorH:1;// should we mirror horizontally ?
	bool m_MirrorV:1;// should we mirror vertically ?
	bool m_Loop:1;// should the keytimes loop ?
	bool m_LoopAnim:1;// should the frame of the sprite loop ?
};

// A class representing an instance of a sprite in the scene
class SpriteEntity
{
public:
	SpriteEntity(const SmartPointer<AnimatedSprite>& animatedSprite, const SpriteDataScene& data);
	~SpriteEntity();

	const SmartPointer<AnimatedSprite>& GetAnimatedSprite() const {return m_AnimatedSprite;}
	const SpriteDataScene& GetSpriteData() const {return m_Data;}

	const Dimension2& GetSize() const {return m_AnimatedSprite->GetSize();}

	const Position2& GetPosition() const {return m_Position;}
	void SetPosition(const Position2& position) {m_Position = position;}
	const core::vector2df& GetScale() const {return m_Scale;}
	void SetScale(const core::vector2df& scale) {m_Scale = scale;}
	float GetRotation() const {return m_Rotation;}
	void SetRotation(float rotation) {m_Rotation = rotation;}
	const IColor& GetTint() const {return m_Tint;}
	void SetTint(const IColor& color) {m_Tint = color;}

	float GetFramerate() const {return m_AnimatedSprite->GetFramerate();}// Get the framerate of the sprite

	void Update(float dt);// Update the sprite instance using the data
	void Render();// render the sprite instance on the screen using the current position
	void Render(const core::matrix4& matrix);// render the sprite instance on the screen using the matrix
	void RenderDebugInfo(bool recursive = false);// render debug info (bounding box) of the sprite instance using the current position

protected:
	SmartPointer<AnimatedSprite> m_AnimatedSprite; // a smart pointer on the sprite (we use a smart pointer here to handle load/unload of the sprite smartly between the scenes and the bank)
	SpriteDataScene m_Data;// the sprite data describing the animation of the sprite isntance

	// the current data resulting of an interpolation between the keys above in the data
	Position2 m_Position;// the current position on the screen
	core::vector2df m_Scale;// the current scale of the sprite
	float m_Rotation;// the current rotation of the sprite
	IColor m_Tint;// the current tint of the sprite

	u32 m_CurrentFrame;// the current frame index of the sprite instance
	float m_CurrentTime;// the current time of the sprite instance used to get the current frame given the framerate of the sprite
};

// A class representing a BackGround instance (since BG are animated too)
struct ImageScene
{
	ImageScene() : m_Image(NULL), m_Loop(false) {}
	ImageScene(Texture* image, const TArray<KeyTime>& keys) : m_Image(image), m_Keys(keys), m_Loop(false)
	{

	}
	~ImageScene()
	{
		if(m_Image)
			Engine::GetInstance()->GetDriver()->removeTexture(m_Image);
	}

	Texture* m_Image;// the texture used as a BG
	TArray<KeyTime> m_Keys;// a list of keytimes defining the animation of the BG (CAUTION : position and scale are used as source parameters for the bg texture)
	bool m_Loop;// should the keys loop ?
};

// A structure defining a sound instance
struct SoundDataScene
{
	SoundDataScene() {}
	SoundDataScene(io::path path, const SmartPointer<AudioStream>& sound) : m_Path(path), m_Sound(sound) {}
	~SoundDataScene() 
	{
		Engine::GetInstance()->GetAudioDevice()->CloseSound(m_Sound);
	}

	bool operator ==(const SoundDataScene& sd) 
	{return (m_Path == sd.m_Path && m_StreamData == sd.m_StreamData);}
	bool operator ==(const io::path& path) const {return (m_Path == path);}

	bool IsAValidPath() const {return (m_Path != "");}

	io::path m_Path;// the path of the sound
	SmartPointer<AudioStream> m_Sound;// a smart pointer on the audio stream (we use a smart pointer to smartly manage the load/unload of the sounds)

	// A structure containing all the parameters used for playing a sound
	struct StreamData
	{
		StreamData() : m_Pan(0.0f), m_Pitch(1.0f), m_Volume(0.5f), m_Repeat(false), m_StopAtDialogsEnd(true) {}
		StreamData(float pan, float pitch, float volume, bool repeat = false, bool stopAtDialogsEnd = true) :
			m_Pan(pan), m_Pitch(pitch), m_Volume(volume), m_Repeat(repeat), m_StopAtDialogsEnd(stopAtDialogsEnd) {}

		bool operator ==(const StreamData& ds) const 
		{
			return m_Pan == ds.m_Pan && 
				m_Pitch == ds.m_Pitch && 
				m_Volume == ds.m_Volume && 
				m_Repeat == ds.m_Repeat && 
				m_StopAtDialogsEnd == ds.m_StopAtDialogsEnd;
		}

		float m_Pan;// -1 (left) -> 0 (center) -> 1 (right)
		float m_Pitch;// 0.5f (pitch 2x lower) -> 1.0f (normal pitch) -> 2.0f (pitch 2x higher)
		float m_Volume;// 0.0f (mute) -> 1.0f (volume max)
		bool m_Repeat:8;// should we repeat the sound
		bool m_StopAtDialogsEnd:8;// should we stop the sound at the end of the dialog
		bool m_Unused1:8;
		bool m_Unused2:8;
	} m_StreamData;// the parameters of the stream instance
};

// A class defining a dialog in the scene. A dialog is composed of a BG, some text to display, some transitions, up to 3 sound instances, some sprite instances.
// It lasts for a fixed amount of time before transitionning to another dialog.
class DialogNode
{
public:
	DialogNode(u32 id = 0xffffffff);
	DialogNode(DialogNode* dlg);
	~DialogNode();

	// Dialog type :
	// DN_AUTO_VALID : automatically go to next dialog at the end of this dialog
	// DN_MANUAL_VALID : manually go to next dialog at the end of this dialog on validation button press
	// DN_MULTI_VALID : allow to manually select the next dialog through a choice list
	// DN_QTE_VALID : manage a QTE at the end of the dialog to determine the next dialog between the win and the fail
	enum DN_Type {DN_AUTO_VALID, DN_MANUAL_VALID, DN_MULTI_VALID, DN_QTE_VALID, DN_MAX};

	// transition type :
	// none or black fade 
	enum DN_TransitionType {DN_TRANSITION_NONE, DN_TRANSITION_FADE, DN_TRANSITION_MAX};

	// Parameters to define a fade in/out
	struct TransitionData
	{
		TransitionData() : m_Type(DN_TRANSITION_NONE), m_Duration(0.0f) {}
		~TransitionData() {}

		DN_TransitionType m_Type;
		float m_Duration;
	};

	u32 GetID() const {return m_ID;}
	void SetID(u32 id) {m_ID = id;}

	DN_Type GetNodeType() const {return m_NodeType;}
	void SetNodeType(DN_Type type) {m_NodeType = type;}

	void _AddParentNode(DialogNode* node) {if(node) m_ParentNodes.push_back(node);}// add a parent node (do not use directly)
	void _AddChildNode(DialogNode* node) {if(node) m_ChildNodes.push_back(node);}// add a child node (do not use directly)
	void _RemoveParentAtIndex(u32 index) {if(index < m_ParentNodes.size()) m_ParentNodes.erase(index);}// remove a parent node (do not use directly)
	void _RemoveChildAtIndex(u32 index) {if(index < m_ChildNodes.size()) m_ChildNodes.erase(index);}// remove a child node (do not use directly)
	s32 _GetParentNodeIndex(DialogNode* node) {return m_ParentNodes.linear_search(node);}// get a parent node (do not use directly)
	s32 _GetChildNodeIndex(DialogNode* node) {return m_ChildNodes.linear_search(node);}// get a child node (do not use directly)

	bool BranchToNode(DialogNode* childNode);// Branch a dialog node to this dialog
	bool RemoveChildAtIndex(u32 index);// Remove a child node link properly
	bool RemoveParentAtIndex(u32 index);// Remove a parent node link properly
	DialogNode* const GetChildAtIndex(u32 index) const {return (index < m_ChildNodes.size())?m_ChildNodes[index]:NULL;}// Get the child node at a given index
	u32 GetNbChildren() const {return m_ChildNodes.size();}// Get the number of children nodes
	const TArray<DialogNode*>& GetChildNodes() const {return m_ChildNodes;}// get the list of the child nodes

	void SetDialogName(const core::stringw& dialogName) {m_DialogName = dialogName;}
	const core::stringw& GetDialogName() const {return m_DialogName;}
	core::stringw* GetDialogNamePtr() {return &m_DialogName;}// Get a pointer on the dialog name (used in editor to edit the name)
	void SetDialogHeader(const core::stringw& dialogHeader, u32 language);
	const core::stringw& GetDialogHeader(u32 language) const {return (language < Engine::LS_MAX)? m_DialogHeader[language] : m_DialogHeader[Engine::LS_MAX];}
	void SetDialogText(const core::stringw& dialogText, u32 language);
	const core::stringw& GetDialogText(u32 language) const {return (language < Engine::LS_MAX)? m_DialogText[language] : m_DialogText[Engine::LS_MAX];}

	void SetUseTextBackGround(bool useBg) {m_UseTextBackGround = useBg;}
	bool GetUseTextBackGround() const {return m_UseTextBackGround;}

	void SetDialogSpeed(float speed) {m_DialogSpeed = speed;}
	float GetDialogSpeed() const {return m_DialogSpeed;}

	void SetDialogDuration(float duration) {m_DialogDuration = duration;}
	float GetDialogDuration() const {return m_DialogDuration;}

	void SetDialogPosition(const Vector2& position) {m_DialogPosition = position;}
	const Vector2& GetDialogPosition() const {return m_DialogPosition;}

	void SetDialogSize(const Vector2& size) {m_DialogSize = size;}
	const Vector2& GetDialogSize() const {return m_DialogSize;}

	const SmartPointer<ImageScene>& GetImageScene() const {return m_ImageScene;}
	void SetImageScene(const SmartPointer<ImageScene>& imageScene) {m_ImageScene = imageScene;}

	TArray< SmartPointer<AnimatedSprite> >& GetSprites() {return m_Sprites;}// Get the list of the sprites used by this dialog (not instances !)
	SmartPointer<AnimatedSprite> GetSpriteAtIndex(u32 index) const {return ((index<m_Sprites.size())?m_Sprites[index]:NULL);}// Get the sprite at index
	void _AddSprite(const SmartPointer<AnimatedSprite>& sprite) {m_Sprites.push_back(sprite);}// Add a new sprite to the dialog (do not use directly)
	bool _RemoveSpriteAtIndex(u32 index)// remove a sprite from the dialog (do not use directly)
	{
		if(index >= m_Sprites.size())
			return false;

		m_Sprites.erase(index);
		return true;
	}
	TArray<SpriteDataScene>& GetSpritesData() {return m_SpritesData;}// Get the list of the sprites data used by this dialog 
	SpriteDataScene* GetSpriteDataAtIndex(u32 index) {return ((index<m_SpritesData.size())?&(m_SpritesData[index]):NULL);}// Get the sprite data at index
	void _AddSpriteData(const SpriteDataScene& data) {m_SpritesData.push_back(data);}// Add a new sprite data to the dialog (do not use directly)
	bool _RemoveSpriteDataAtIndex(u32 index)// remove a sprite data from the dialog (do not use directly)
	{
		if(index >= m_SpritesData.size())
			return false;

		m_SpritesData.erase(index);
		return true;
	}

	const SmartPointer<SoundDataScene>& GetSoundBG1() const {return m_SoundBG1;}
	void SetSoundBG1( io::path path) {m_SoundBG1 = SmartPointer<SoundDataScene>(new SoundDataScene()); m_SoundBG1->m_Path = path;}
	void SetSoundBG1(const SmartPointer<SoundDataScene>& sd) {m_SoundBG1 = sd;}
	const SmartPointer<SoundDataScene>& GetSoundBG2() const {return m_SoundBG2;}
	void SetSoundBG2(io::path path) {m_SoundBG2 = SmartPointer<SoundDataScene>(new SoundDataScene()); m_SoundBG2->m_Path = path;}
	void SetSoundBG2(const SmartPointer<SoundDataScene>& sd) {m_SoundBG2 = sd;}
	const SmartPointer<SoundDataScene>& GetVoice() const {return m_Voice;}
	void SetVoice(io::path path) {m_Voice = SmartPointer<SoundDataScene>(new SoundDataScene()); m_Voice->m_Path = path;}
	void SetVoice(const SmartPointer<SoundDataScene>& sd) {m_Voice = sd;}

	const TransitionData& GetTransitionIn() const {return m_TransitionIn;}
	void SetTransitionIn(const TransitionData& trans) {m_TransitionIn = trans;}
	const TransitionData& GetTransitionOut() const {return m_TransitionOut;}
	void SetTransitionOut(const TransitionData& trans) {m_TransitionOut = trans;}

	void SetKeepPreviousSpritesAlive(bool state) {m_KeepPreviousSpritesAlive = state;}// NOT USED 
	bool GetKeepPreviousSpritesAlive() const {return m_KeepPreviousSpritesAlive;}// NOT USED 

	TArray<core::stringw>& GetConditionalTexts(u32 language) {return (language < Engine::LS_MAX)? m_ConditionsText[language] : m_ConditionsText[Engine::LS_MAX];}// Get the choice entries for the given language
	void AddConditionalText(const core::stringw& conditionText, u32 language);// add a new choice entry for the given language
	void RemoveConditionalTextAtIndex(u32 index) {for(u32 i = 0; i < Engine::LS_MAX; ++i) if(index < m_ConditionsText[i].size()) m_ConditionsText[i].erase(index);}// remove the given choice from the choice list for all languages

	TArray<InputData>& GetInputs() {return m_Inputs;}
	InputData* GetInputAtIndex(u32 index) {return ((index<m_Inputs.size())?&(m_Inputs[index]):NULL);}
	void RemoveInputAtIndex(u32 index) {if(index<m_Inputs.size()) m_Inputs.erase(index);}
	void AddInput(const InputData& input) {m_Inputs.push_back(input);}

	void ResetInputsCounter() {for(u32 i=0; i<m_Inputs.size(); ++i) m_Inputs[i].m_NbCurrentPress = 0;}

	void SetScreenShakeStrength(float screenShakeStrength) {m_ScreenShakeStrength = screenShakeStrength;}
	float GetScreenShakeStrength() const {return m_ScreenShakeStrength;}
	void SetScreenShakeFrequency(float screenShakeFrequency) {m_ScreenShakeFrequency = screenShakeFrequency;}
	float GetScreenShakeFrequency() const {return m_ScreenShakeFrequency;}

	bool GetIsLoaded() const {return m_IsLoaded;}
	void SetIsLoaded(bool state) {m_IsLoaded = state;}// Use only in toolmode to force the load state when created from scratch
	bool LoadFromFile(const char* name, bool useTextureSizeLimit = false);
	bool UnLoad();
#ifndef IRON_ICE_FINAL
	bool SaveToFile(const char* name);
	bool SaveToFileXML(const char* name);
#endif

	u32 GetLoadCount() const {return m_LoadCount;}

#ifdef IRON_ICE_EDITOR
	MD5 GetMD5();
#endif

	float GetDialogTotalDuration(bool skipOutro = false, bool evaluateInputTime = false) const;// Get the real dialog duration (w or w\ outro duration, and w or w\ the inputs (for QTE) duration)

protected:
	u32 m_ID;// the dialog ID (only used from scene loading)

	TArray<DialogNode*> m_ParentNodes;// list of parent dialog nodes
	TArray<DialogNode*> m_ChildNodes;// list of child dialog nodes

	core::stringw m_DialogName;// the name of the dialog (used in editor only)
	core::stringw m_DialogHeader[Engine::LS_MAX+1];// a dialog text header (in all languages) (used generally for the name of the character speaking)
	core::stringw m_DialogText[Engine::LS_MAX+1];// a dialog text content (in all languages)
	bool m_UseTextBackGround;// should we use the text background ?
	float m_DialogSpeed;// writing speed : letters written per second (will define the duration if dialog duration is set to 0)
	float m_DialogDuration;// the duration of the dialog (0 = driven by the letters speed)

	Vector2 m_DialogPosition;// the position of the dialog text area : in % of screen dim
	Vector2 m_DialogSize;// the size of the dialog text area : in % of screen dim

	DN_Type m_NodeType;// the type of dialog

	TArray<core::stringw> m_ConditionsText[Engine::LS_MAX+1];// must be the same size as the child nodes' array if multi valid mode

	SmartPointer<ImageScene> m_ImageScene;// the BG instance : only one instance for each dialog

	TArray<SpriteDataScene> m_SpritesData;// list of all the sprite data used by the dialog
	TArray< SmartPointer<AnimatedSprite> > m_Sprites;// list of all the sprites used by the dialog (use smart pointers to handle loading/unloading)

	SmartPointer<SoundDataScene> m_SoundBG1;// the sound instance of the sound BG1
	SmartPointer<SoundDataScene> m_SoundBG2;// the sound instance of the sound BG2
	SmartPointer<SoundDataScene> m_Voice;// the sound instance of the voice

	TransitionData m_TransitionIn;// the transition IN (fade in)
	TransitionData m_TransitionOut;// the transition OUT (fade out)

	bool m_KeepPreviousSpritesAlive;// should we keep the previous sprite instances alive ? NOT USED !

	TArray<InputData> m_Inputs;// a list of inputs (for non auto dialogs type)

	float m_ScreenShakeStrength;// Strength of the shake (0 = none)
	float m_ScreenShakeFrequency;// Frequency of the shake (slow --> fast)

	bool m_IsLoaded;// is the dialog loaded ?
	u32 m_LoadCount;// load count (for editor debug)
};

// A class representing a scene the game. The scene is defined by a list of dialogs linked together. 
// It will start with an initial dialog and end when any dialog has no child dialog. The scene will
// manage the load/unload of the dialogs depending the needs, will manage the dialogs with the user inputs,
// and will update and render the current dialog with all the resources used by itself.
class Scene
{
public:
	Scene();
	~Scene();

	DialogNode* GetCurrentDialog() const {return m_CurrentDialog;}
	void SetCurrentDialog(DialogNode* dlg) {m_CurrentDialog = dlg;}
	DialogNode* GetDialogAtIndex(u32 index) const {return ((index<m_Dialogs.size())?m_Dialogs[index]:NULL);}
	TArray<DialogNode*>& GetDialogs() {return m_Dialogs;}

	DialogNode* GetInitialDialog() const {return m_InitialDialog;}
	void SetInitialDialog(s32 index) {m_InitialDialog = (index>=0 && (u32)index<m_Dialogs.size())?m_Dialogs[index]:NULL;}

	void AddDialog(DialogNode* dialog) {m_Dialogs.push_back(dialog);}
	bool RemoveDialogAtIndex(u32 index)
	{
		if(index >= m_Dialogs.size())
			return false;

		if(m_InitialDialog == m_Dialogs[index])
			m_InitialDialog = NULL;
		SafeDelete(m_Dialogs[index]);
		m_Dialogs.erase(index);
		return true;
	}

	// the sprites bank
	TArray< SmartPointer<AnimatedSprite> >& GetSprites() {return m_SpritesBank;}
	SmartPointer<AnimatedSprite> GetSpriteAtIndex(u32 index) const {return ((index<m_SpritesBank.size())?m_SpritesBank[index]:NULL);}
	void AddSpriteToBank(const SmartPointer<AnimatedSprite>& sprite) {m_SpritesBank.push_back(sprite);}
	bool RemoveSpriteAtIndex(u32 index)
	{
		if(index >= m_SpritesBank.size())
			return false;

		m_SpritesBank.erase(index);
		return true;
	}

	// the sounds bank (editor only)
	TArray< io::path >& GetSounds() {return m_SoundsBank;}
	io::path GetSoundAtIndex(u32 index) const {return ((index<m_SoundsBank.size())?m_SoundsBank[index]:L"");}
	u32 AddSoundToBank(const io::path& path, bool checkMD5 = false);
	bool RemoveSound(const io::path& path);
	void RemoveSound(u32 index) {m_SoundsBank.erase(index);}

	// Usefull functions to easilly add a sprite from the bank to any dialog (editor only !)
	bool AddSpriteToDialog(DialogNode* dlg, s32 bankIndex);
	bool RemoveSpriteFromDialog(DialogNode* dlg, s32 index)
	{
		assert(!dlg || (dlg->GetSprites().size() == dlg->GetSpritesData().size()));
		return (dlg && dlg->_RemoveSpriteAtIndex(index) && dlg->_RemoveSpriteDataAtIndex(index));
	}

	// sprite instances management
	TArray< SmartPointer<SpriteEntity> >& GetSpritesAlive() {return m_SpritesAlive;}
	SmartPointer<SpriteEntity> GetSpriteAliveAtIndex(u32 index) const {return ((index<m_SpritesAlive.size())?m_SpritesAlive[index]:NULL);}
	void AddSpriteAlive(const SmartPointer<SpriteEntity>& sprite) {m_SpritesAlive.push_back(sprite);}
	void ClearSpritesAlive() {m_SpritesAlive.clear();}

	// sound instances management
	TArray< SmartPointer<AudioStream> >& GetSoundsAlive() {return m_SoundsAlive;}
	SmartPointer<AudioStream> GetSoundAliveAtIndex(u32 index) const {return ((index<m_SoundsAlive.size())?m_SoundsAlive[index]:NULL);}
	void AddSoundAlive(const SmartPointer<AudioStream>& sound) {m_SoundsAlive.push_back(sound);}
	void ClearSoundsAlive() {m_SoundsAlive.clear();}
	AudioDataSerializable GetSoundAliveNamesAtIndex(u32 index) const {return ((index<m_SoundsAliveData.size())?m_SoundsAliveData[index]:AudioDataSerializable());}


	
	void ClearAllSprites();// clear both sprites bank and instances

	const SmartPointer<ImageScene>& GetCurrentBackground() const {return m_CurrentBackground;}
	void SetCurrentBackground(const SmartPointer<ImageScene>& imageScene) {m_CurrentBackground = imageScene;}

	bool LoadSceneFromFile(const char* path);// load a scene from a .scn file
#ifndef IRON_ICE_FINAL
	bool Deprecated_SaveSceneToFile(const char* path);// DO NOT USE : deprecated save .scn file
	bool SaveSceneToFile(const char* path);// save the scene in a .scn file, and all the sounds, sprites, and dialogs used by it in subfolders
#endif

	bool InitDialog(DialogNode* dlg);// initialize the dialog
	bool LoadDialog(DialogNode* dlg);// load the dialog and initialize it (call LoadDialogFromIndex())

	bool LoadDialogFromIndex(u32 index, bool useTextureSizeLimit = false);// simply load the dialog

#ifndef IRON_ICE_FINAL
	bool SaveDialogFromIndex(const char* path, u32 index);// save a unique dialog
	bool SaveSpriteFromIndex(const char* path, u32 index);// save a unique sprite
#endif

	bool LoadSpriteFromIndex(u32 index);// load a sprite from the bank

	bool CheckSceneSpritesIntegrity(bool removeUnusedData = false);// various check for sprites and dialogs to verify the scene is consistent
	void CheckSceneIntegrity(); // use it before you save the scene : to remove obsolete data

	void FlushSpritesBank();// unload sprites used nowhere

	void FlushSoundsAlive();// remove sounds used nowhere

	// scene execution controls
	float GetCurrentDialogTime() const {return m_CurrentTime;}
	bool IsInIntro() const {return m_CurrentDialog && (m_CurrentTimeInternal < m_CurrentDialog->GetTransitionIn().m_Duration && m_CurrentDialog->GetTransitionIn().m_Duration > 0.0f);}
	bool IsInOutro() const {return m_CurrentDialog && (m_CurrentDialog->GetDialogTotalDuration() - m_CurrentTimeInternal < m_CurrentDialog->GetTransitionOut().m_Duration && m_CurrentDialog->GetTransitionOut().m_Duration > 0.0f);}
	float GetIntroRatio() const {return m_CurrentDialog?m_CurrentTimeInternal/m_CurrentDialog->GetTransitionIn().m_Duration:0.0f;}
	float GetOutroRatio() const {return m_CurrentDialog?core::max_(0.0f, m_CurrentDialog->GetDialogTotalDuration() - m_CurrentTimeInternal)/m_CurrentDialog->GetTransitionOut().m_Duration:0.0f;}
	void ResetScene()// reset everything
	{
		m_CurrentTime = m_CurrentTimeInternal = 0.0f;
		m_CurrentDialog = m_InitialDialog;
		m_CurrentChoice = 0;
		m_CurrentDlgValidated = false;
	}
	void Step(float dt, bool skipDialogValidation = false);// update the scene
	void StepInputs(bool skipDialogValidation = false);// update the inputs
	bool HasCurrentDialogEnded(bool skipOutro = false) const {return m_CurrentDialog?(m_CurrentTimeInternal>core::max_(m_CurrentDialog->GetDialogTotalDuration(skipOutro), 1.0f)):true;}// has the current dialog ended ?
	InputData& GetCurrentInputsPressed() {return m_CurrentInputsPressed;}// current input pressed by the user
	InputData& GetCurrentInputs() {return m_CurrentInputs;}// used for display : input to press
	float GetCurrentInputsRemainingTime() const {return m_CurrentInputsRemainingTime;}// remaining time to press the current input (QTE) : used for display

	u32 GetCurrentDialogChoice() const {return m_CurrentChoice;}
	void SetCurrentDialogChoice(u32 choice) { m_CurrentChoice = choice;}

	bool HasEnded() const {return m_HasEnded;}

	void Loop1stDialog(bool state) {m_Loop1stDialog = state;}

	void ForceLoadDialog()// force the current dialog to load if not loaded
	{
		if(m_CurrentDialog && !m_CurrentDialog->GetIsLoaded())
			LoadDialog(m_CurrentDialog);
	}

	void SetScenePath(const io::path& scenePath) {m_ScenePath = scenePath;}
	const io::path& GetScenePath() const {return m_ScenePath;}

	u32 GetNbSoundsBank() const {return m_NbSoundsBank;}

	void RestoreSounds();

protected:
	DialogNode* m_CurrentDialog;// the dialog currently played
	float m_CurrentTime;// the current time 
	float m_CurrentTimeInternal;// internal current time
	float m_Accumulator;// accumulator used for the timer (not used)
	InputData m_CurrentInputsPressed;// current input pressed by the user
	InputData m_CurrentInputs;// used for display : input to press
	float m_CurrentInputsRemainingTime;// remaining time to press the current input (QTE) : used for display
	u32 m_CurrentChoice;// current choice selected (for multi valid dialogs)
	bool m_CurrentDlgValidated;// is the current dialog validated (if true we will go to the next dialog)?  

	TArray<DialogNode*> m_Dialogs;// list of all the dialogs of the scene
	DialogNode* m_InitialDialog;// initial dialog for the scene
	TArray< SmartPointer<AnimatedSprite> > m_SpritesBank;// contains all the sprites used by the scene
	TArray< SmartPointer<SpriteEntity> > m_SpritesAlive;// contains all sprite instances currently instanciated by the dialog

	TArray< io::path > m_SoundsBank;// contains all the sounds pathes used by the scene (in editor only)
	TArray< SmartPointer<AudioStream> > m_SoundsAlive;// contains all the sound instances used by the current dialog (only 3)
	TArray< AudioDataSerializable > m_SoundsAliveData;// short name of the sounds alive files

	u32 m_NbSpritesBank;// number of sprites in the bank
	u32 m_NbSoundsBank;// number of sounds in the bank
	u32 m_NbDialogs;// number of dialogs in the scene

	bool m_HasEnded;// has the scene ended (no more child dialog)

	bool m_Loop1stDialog;// should we loop the first dialog ? (used for previews)

	io::path m_ScenePath;// the scene path

	SmartPointer<ImageScene> m_CurrentBackground;// the current background
};


// Abstract class used to describe a game content
class NodeDesc
{
public:
	u32 ID;// an ID
	core::stringw Name[Engine::LS_MAX];// a localized name
	Texture* PreviewTex;// a preview (texture)

	NodeDesc():ID(0xffffffff), PreviewTex(NULL) {}
	NodeDesc(u32 id, NodeDesc* parent = NULL):ID(id), PreviewTex(NULL) {}
	virtual ~NodeDesc() {SAFE_UNLOAD(PreviewTex);}
};

// A class used to describe a scene content
class SceneDesc: public NodeDesc
{
public:
	SceneDesc(u32 id): NodeDesc(id) {}
	virtual ~SceneDesc() {}
};

// A class used to describe a chapter content
class ChapterDesc: public NodeDesc
{
public:
	ChapterDesc(u32 id): NodeDesc(id) {}
	virtual ~ChapterDesc() {}

	TArray<SceneDesc> ScenesDesc;// contains a list of scene descriptions
};

// A class used to describe the game content
class GameDesc: public NodeDesc
{
public:
	GameDesc(): NodeDesc(), m_NbScenes(0) {}
	virtual ~GameDesc() {UnloadPreviews();}

	TArray<ChapterDesc> ChaptersDesc;// contains a list of chapters descriptions

	ChapterDesc* GetChapterFromId(u32 chId) {return (chId < ChaptersDesc.size())? &ChaptersDesc[chId] : NULL;}// Get a given chapter description 
	SceneDesc* GetSceneFromId(u32 chId, u32 scId)// Get a given scene description 
	{
		return (chId < ChaptersDesc.size())?
			((chId < ChaptersDesc.size())? &(ChaptersDesc[chId].ScenesDesc[scId]) : NULL)
			: NULL;
	}

	// get the path for the given chapter data
	static core::stringc GetChapterPath(u32 chId, const core::stringc& filename)
	{
		return  Engine::GetInstance()->GetGamePath(StringFormat("data/CH%02d/%s", chId+1, filename.c_str()).c_str());
	}

	// get the path for the given scene data
	static core::stringc GetScenePath(u32 chId, u32 scId, const core::stringc& filename = "scene.scn")
	{
		return  (chId == 0xffffffff) ? // chapter id 0xffffffff is used for extra scenes data (menu)
			Engine::GetInstance()->GetGamePath(StringFormat("system/menu_scn/%04d/%s",scId, filename.c_str()).c_str()) : 
			Engine::GetInstance()->GetGamePath(StringFormat("data/CH%02d/SC%02d/%s", chId+1, scId+1, filename.c_str()).c_str());
	}

	void LoadPreviews();// load all the description previews
	void UnloadPreviews();// unload all the description previews

	bool ReadFromFile(const char* path);// read the game description from a .scd file
#ifndef IRON_ICE_FINAL
	bool ReadFromFileTxt(const char* path, Engine::LanguageSettings language = Engine::LS_FRENCH);// read the game description from a .txt file
	bool WriteToFile(const char* path);// write the game description into a .scd file
	//bool WriteToFileXML(const char* path);// write the game description into a .xml file
#endif

	// statics to manage the game load/save
	static u32 m_CurrentChapter;// the current chapter id used
	static u32 m_CurrentScene;// the current scene id used
	static u32 m_CurrentDialog;// the current dialog id used
	static AudioDataSerializable m_CurrentSounds[3];// the current sounds played (short name)
	static bool m_RequestSceneStart;// request a scene start

	bool GoToNextScene();// go to the next scene/chapter given the game content (will read/write the statics above)

	u32 GetNbScenes() const {return m_NbScenes;}
	float GetProgressionRatio(u32 chapterIdx, u32 sceneIdx);// Get a float value between 0 and 1 indicating the game progression given a chapter id and a scene id

private:
	enum NodeDescType{NDT_UNDEFINED, NDT_SCENE, NDT_CHAPTER};// used to read the txt file
#ifndef IRON_ICE_FINAL
	bool ReadLineTxt(wchar_t *line, NodeDescType& outType, u32& outId, core::stringw& outName);// low level function used to read the txt file
#endif

	u32 m_NbScenes;// total number of scenes of the game (excluding extra content)
};

// A manager to manage the game description
class GameDescManager
{
public:
	~GameDescManager() {}

	static GameDesc* GetGameDesc()
	{
		if(!ms_GameDesc)
			ms_GameDesc = new GameDesc();
		return ms_GameDesc;
	}
	static void DeleteGameDesc() {SafeDelete(ms_GameDesc);}

protected:
	GameDescManager() {}

	static GameDesc* ms_GameDesc;// the game description
};

#ifdef IRON_ICE_EDITOR
// A class to manage the data edition (load/unload/save/display) in the editor.
// It is used to have the names of the dialogs without loading them, a preview of the sprites without loading them, 
// and provides a md5 checksum control to know when to save or not a resource in the temp directory before unloading it.
// This data must reflect the real data edited (same number of dialogs, and sprites)
class SceneEditorData
{
public :
	SceneEditorData();
	~SceneEditorData();

	bool LoadFromFile(char* path);// load from a .iie file
	bool SaveToFile(char* path);// save to a .iie file

	void RemoveDialog(u32 index);// remove a dialog from the editor data
	void AddDialog(DialogNode* dlg);// add a dialog to the editor data : new/import/duplicate
	void SwapDialogNext(u32 index);// swap a dialog with the next one 
	void ClearDialogs();// clear all the dialogs in the temp dir

	void RemoveSprite(u32 index);// remove a sprite from the editor data
	void AddSprite();// add a sprite to the editor data : new/import
	void ClearSprites();// clear all the sprites in the temp dir

	void ClearSounds();// clear all the sounds in the temp dir

	void PushInCache(Scene* scene);// push the entire content of a scene in the cache directory (editor temp dir)

	TArray<core::stringw> m_DialogListNames;// the list of all the dialogs' names
	TArray<MD5::MD5Raw> m_DialogsMD5;// the list of all the dialogs' md5
	TArray<Texture*> m_SpriteBankPreviews;// the list of all the sprites' texture preview
	TArray<MD5::MD5Raw> m_SpritesMD5;// the list of all the sprites' md5
};
#endif

#endif
