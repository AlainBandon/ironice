/** Example 014 Win32 Window

This example only runs under MS Windows and demonstrates that Irrlicht can
render inside a win32 window. MFC and .NET Windows.Forms windows are possible,
too.

In the begining, we create a windows window using the windows API. I'm not
going to explain this code, because it is windows specific. See the MSDN or a
windows book for details.
*/
#include <irrlicht.h>

#if defined(_IRR_WINDOWS_)
#include <windows.h>
#define sleep Sleep
#endif

#include "utils.h"
#include "engine.h"
#include "gameview.h"

using namespace irr;

#pragma comment(lib, "irrlicht.lib")

//! Define _NGE_DISABLE_CONSOLE_ON_VC_RELEASE to enable console Only on Debug build with VC.
/*! This shows console only when the engine is compiled on Debug build. Release build
wont have the console shown. Comment this line out to disable this feature which means
the console will always be shown. */
#define _NGE_DISABLE_CONSOLE_ON_VC_RELEASE
#if defined(_NGE_DISABLE_CONSOLE_ON_VC_RELEASE)
#   if defined(_IRR_WINDOWS_)
#      if defined(_DEBUG)
#         pragma comment(linker, "/subsystem:console")
#      else
#         pragma comment(linker, "/subsystem:windows /ENTRY:mainCRTStartup")
#      endif
#   endif
#endif

/*
   Now ask for the driver and create the Windows specific window.
*/
int main()
{
	Engine::ms_WindowSize = Dimension2(800, 600);
	Engine::ms_FullScreen = false;
	Engine* engine = Engine::GetInstance();// will initialize the engine

	//engine->DumpResolutions();

	Game* game = Game::GetInstance();// will initialize the game

	int lastFPS = -1;

	u32 then = engine->GetDevice()->getTimer()->getTime();

	while(engine->GetDevice()->run() && engine->GetDriver())
	{
		// check for device reset
		if(engine->ShouldResetDevice())
		{
			game->OnDevicePreReset();
			engine->ResetDevice();
			engine->DrawLoadScreen();
			game->OnDevicePostReset();
			continue;
		}

		const u32 now = engine->GetDevice()->getTimer()->getTime();
		f32 frameDeltaTime = (f32)(now - then) / 1000.f; // Time in seconds
		if(frameDeltaTime > 0.1f)
			frameDeltaTime = 0.1f;

		then = now;

		if(frameDeltaTime <= 0.0f)
			continue;

		engine->Update(frameDeltaTime);
		Dimension2 curRenderSize = engine->GetDriver()->getCurrentRenderTargetSize();
		curRenderSize.Width &= 0xfffffffe;
		curRenderSize.Height &= 0xfffffffe;
		if(engine->m_RenderSize != curRenderSize)
		{
			engine->m_RenderSize = curRenderSize;
			ProfileData::GetInstance()->m_ODS.m_WindowSize = engine->m_RenderSize;
			ProfileData::GetInstance()->WriteSave();

			engine->RecomputeSize();

			game->OnResize();
		}

		if (engine->GetDevice()->isWindowActive())
		{
			game->Update(frameDeltaTime);


			engine->GetDriver()->beginScene(true, true, video::SColor(0,100,100,100));

			game->Render();

			engine->GetGui()->drawAll();

			engine->GetDriver()->endScene();

			// display frames per second in window title
			int fps = engine->GetDriver()->getFPS();
			if (lastFPS != fps)
			{
#ifndef IRON_ICE_FINAL
				core::stringw str = WINDOW_TITLE_STRING;
				str += " - FPS:";
				str += fps;
#else
				core::stringw str = WINDOW_TITLE_STRING;
#endif

				engine->GetDevice()->setWindowCaption(str.c_str());
				lastFPS = fps;
			}
		}

#if defined(_DEBUG) && defined(_IRR_WINDOWS_) // some sleep to not uselessly burn my cpu during debug phase
		sleep(1);
#endif
		//device->sleep(1);
		//device->yield();
	}

	game->DeleteInstance();
	engine->DeleteInstance();

	return 0;
}

/*
That's it, Irrlicht now runs in your own windows window.
**/
