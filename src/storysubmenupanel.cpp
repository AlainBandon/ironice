﻿#include "panel.h"
#include "scene.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f

#define USE_CIRCULAR_MENU 0

enum StringDesc {SD_CONTINUE = Engine::SFTID_StorySubMenuPanel, SD_NEWGAME, SD_LOAD, SD_DELETESAVE, SD_CHAPTERS, SD_START, SD_EMPTY, SD_DELETESAVE_POPUP, SD_AUTOSAVE, SD_QUICKSAVE, SD_MAX};

// StorySubMenuPanel
StorySubMenuPanel::StorySubMenuPanel() : IPanel()
, m_ActiveBG(NULL)
, m_Selec(NULL)
, m_CurrentSelection(0)
, m_CurrentSubMenuActive(0)
, m_PreviousSubMenuActive(0)
, m_CurrentSaveSlotSelected(0)
, m_CurrentChapterSelected(0)
, m_CurrentChapterSlotSelected(0)
, m_CurrentSceneSelected(0)
, m_CurrentSelectionValidatedFader(0.2f)
, m_ScenePreviewNew(NULL)
, m_ScenePreviewContinue(NULL)
, m_SaveSlotTex(NULL)
, m_SaveSlotDelete(NULL)
, m_SaveSlotDeleteA(NULL)
, m_PopupPanel(NULL)
{
	for(int i = 0; i < 4; ++i)
		m_Font[i] = NULL;

	for(int i = 0; i < 2; ++i)
		m_Arrows[i] = NULL;

	for(int i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
		m_ScenePreviewSave[i] = NULL;

	bool useRT = true;
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
	if(useRT && driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		m_RenderTarget = driver->addRenderTargetTexture(Engine::GetInstance()->m_RenderSize, "RTT_Story", video::ECF_A8R8G8B8);
	}
	else
	{
		m_RenderTarget = NULL;
	}
}

StorySubMenuPanel::~StorySubMenuPanel()
{
	UnloadPanel();
}

void StorySubMenuPanel::LoadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	SAFE_LOAD_IMAGE(driver, m_ActiveBG, engine->GetGamePath("system/02_Active_BG.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_Selec, engine->GetGamePath("system/00_selection.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_SaveSlotTex, engine->GetGamePath("system/0_Save_under.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_SaveSlotDelete, engine->GetGamePath("system/0_Delete_inActive.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_SaveSlotDeleteA, engine->GetGamePath("system/0_Delete_Active.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_Arrows[0], engine->GetGamePath("system/02_Chapter_L_arrow.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_Arrows[1], engine->GetGamePath("system/02_Chapter_R_arrow.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[4] = {28, 40, 48, 60};
		for(int i = 0; i < 4; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA), size/6);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	ProfileData* profil = ProfileData::GetInstance();
	{
		SaveData& save = profil->GetSave(PROFILE_AUTOSAVE_SLOT);
		if(!m_ScenePreviewContinue && save.Used)
		{
			m_ScenePreviewContinue = new ScenePanel();
			m_ScenePreviewContinue->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(save.ChapterId, save.SceneId));
			m_ScenePreviewContinue->LoadPanel();
			if(save.DialogId > 0)
				m_ScenePreviewContinue->SetCurrentDialog(save.DialogId-1);
			m_ScenePreviewContinue->UsePremultipliedAlpha(true);
			m_ScenePreviewContinue->m_PlaySounds = false;
			m_ScenePreviewContinue->m_Loop1stDialog = true;
			m_ScenePreviewContinue->ForceLoadScene();
		}
	}

	if(!m_ScenePreviewNew)
	{
		m_ScenePreviewNew = new ScenePanel();
		m_ScenePreviewNew->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(0, 0));
		m_ScenePreviewNew->LoadPanel();
		m_ScenePreviewNew->UsePremultipliedAlpha(true);
		m_ScenePreviewNew->m_PlaySounds = false;
		m_ScenePreviewNew->m_Loop1stDialog = true;
		m_ScenePreviewNew->ForceLoadScene();
	}

	for(u32 i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
	{
		SaveData& save = profil->GetSave(i);
		if(save.Used)
		{
			m_ScenePreviewSave[i] = new ScenePanel();
			m_ScenePreviewSave[i]->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(save.ChapterId, save.SceneId));
			if(i == m_CurrentSaveSlotSelected)
			{
				m_ScenePreviewSave[i]->LoadPanel();
				if(save.DialogId > 0)
					m_ScenePreviewSave[i]->SetCurrentDialog(save.DialogId-1);
			}
			m_ScenePreviewSave[i]->UsePremultipliedAlpha(true);
			m_ScenePreviewSave[i]->m_PlaySounds = false;
			m_ScenePreviewSave[i]->m_Loop1stDialog = true;
			if(i == m_CurrentSaveSlotSelected)
				m_ScenePreviewSave[i]->ForceLoadScene();
		}
	}

	if(!m_PopupPanel)
	{
		m_PopupPanel = new PopupPanel(m_RenderTarget || USE_PREMULTIPLIED_ALPHA);
		m_PopupPanel->LoadPanel();
	}

	OnResize();

	//StartFade(PANEL_FADETIME);

	IPanel::LoadPanel();
}

void StorySubMenuPanel::UnloadPanel() 
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	SAFE_UNLOAD(m_ActiveBG);

	SAFE_UNLOAD(m_Selec);

	SAFE_UNLOAD(m_SaveSlotTex);

	SAFE_UNLOAD(m_SaveSlotDelete);

	SAFE_UNLOAD(m_SaveSlotDeleteA);

	SAFE_UNLOAD(m_Arrows[0]);
	SAFE_UNLOAD(m_Arrows[1]);

	for(int i = 0; i < 4; ++i)
		SAFE_UNLOAD(m_Font[i]);

	SafeDelete(m_ScenePreviewNew);
	SafeDelete(m_ScenePreviewContinue);

	for(int i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
		SafeDelete(m_ScenePreviewSave[i]);

	if(m_PopupPanel)
		m_PopupPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void StorySubMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	const u32 sizes[4] = {28, 40, 48, 60};
	if(m_Font[0])
	{
		for(int i = 0; i < 4; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			m_Font[i]->setSize(size, size/6);
		}
	}

	{
		Dimension2 size = (m_ActiveBG)? m_ActiveBG->getSize() : Dimension2(1161, 834);
		size.Width += 100;
		size.Height += 60;
		Position2 pos = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)size.Height*innerScale)) + innerOffset;
		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)size.Height*innerScale));
		m_SubMenusArea[SMAT_CENTER].Area = Rectangle2(pos-cornerOffset, pos);
		m_SubMenusArea[SMAT_CENTER].IsSelected = false;

		pos.X -= cornerOffset.X;
		m_SubMenusArea[SMAT_LEFT].Area = Rectangle2(innerOffset, pos);
		m_SubMenusArea[SMAT_LEFT].IsSelected = false;

		pos.X = innerOffset.X;
		cornerOffset = Position2((s32)((f32)originalSize.Width*innerScale), (s32)((f32)originalSize.Height*innerScale))+innerOffset;
		m_SubMenusArea[SMAT_BOTTOM].Area = Rectangle2(pos, cornerOffset);
		m_SubMenusArea[SMAT_BOTTOM].IsSelected = false;

		m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
	}

	// chapter submenu
#ifndef DEMO_VERSION
	{
		u32 nbChapters = GameDescManager::GetGameDesc()->ChaptersDesc.size();
#ifdef IRON_ICE_ENGINE
		nbChapters = (ProfileData::GetInstance()->m_PDS.m_ProgressionCH != 0xff)?
			ProfileData::GetInstance()->m_PDS.m_ProgressionCH + 1 : GameDescManager::GetGameDesc()->ChaptersDesc.size();
#endif

		const Dimension2& size = m_ActiveBG->getSize();

		const u32 margin = 30;
		const u32 slotWidth = (size.Width - (margin<<1))/4;
		const u32 slotHeight = (size.Height - (margin<<1))/6;
		const u32 innerMargin = 10;

		for(s32 i = -2; i <= 2; ++i)
		{
			s32 slotIndex = i;
			slotIndex += m_CurrentChapterSelected;
			bool useTexArea = true;
			if(USE_CIRCULAR_MENU)
			{
				slotIndex += nbChapters;
				slotIndex %= nbChapters;
			}
			else
			{
				if(slotIndex < 0 || (u32)slotIndex >= nbChapters)
					useTexArea = false;
			}

			Position2 cornerOffset;
// 			if(useTexArea)
// 			{
// 				Texture* tex = GameDescManager::GetGameDesc()->ChaptersDesc[slotIndex].PreviewTex;
// 				float previewScale = (float)(slotHeight - innerMargin)/tex->getSize().Height;
// 				cornerOffset = Position2((s32)(previewScale*innerScale*tex->getSize().Width*0.5f), (s32)(previewScale*innerScale*tex->getSize().Height*0.5f));
// 			}
// 			else
			{
				cornerOffset = Position2((s32)(innerScale*slotWidth*0.5f), (s32)(innerScale*slotHeight*0.5f));
			}
			
			Position2 rawPos = Position2((s32)(innerScale*(originalSize.Width-size.Width-100+margin +(slotWidth>>1))), 
				(s32)(innerScale*((size.Height>>1) + 60 + i*slotHeight))) + innerOffset;

			if(abs(i) >= 1)
				rawPos.Y += (i > 0)? (s32)(innerScale*(slotHeight>>1)) : -(s32)(innerScale*(slotHeight>>1));

			m_ChapterSlotArea[i+2].Area = Rectangle2(rawPos - cornerOffset, rawPos + cornerOffset);
			m_ChapterSlotArea[i+2].IsSelected = false;
		}
	}
#endif

	{
		
		Dimension2 size(originalSize.Width>>2, sizes[2]);
		Position2 pos = Position2((s32)((f32)100*innerScale), (s32)((f32)(originalSize.Height>>2)*innerScale)) + innerOffset;

		u32 dy = (u32)(((originalSize.Height>>1)/(f32)SBT_MAX)*innerScale);
		Position2 offset(0, 0);

		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)dy*innerScale));
		for(int i = 0; i < SBT_MAX; ++i, offset.Y += dy)
		{
			if(m_Font[3])
			{
				cornerOffset.X = m_Font[3]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), i+SD_CONTINUE)).Width;
			}
			m_ButtonsArea[i].Area = Rectangle2(pos + offset, pos + offset + cornerOffset);
			m_ButtonsArea[i].IsSelected = false;
		}

		m_ButtonsArea[m_CurrentSelection].IsSelected = true;
	}

	if(m_RenderTarget && renderSize != m_RenderTarget->getSize())
		m_RenderTarget = driver->addRenderTargetTexture(renderSize, "RTT_Story", video::ECF_A8R8G8B8);

	if(m_ScenePreviewNew)
	{
		m_ScenePreviewNew->OnResize();
	}

	if(m_ScenePreviewContinue)
	{
		m_ScenePreviewContinue->OnResize();
	}

	for(int i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
	{
		if(m_ScenePreviewSave[i])
		{
			m_ScenePreviewSave[i]->OnResize();
		}
	}

	if(m_PopupPanel)
	{
		m_PopupPanel->OnResize();
	}
}

void StorySubMenuPanel::Update(float dt) 
{
	IPanel::Update(dt);

	m_CurrentSelectionValidatedFader.Tick(dt);
	m_CurrentChapterSelectionFader.Tick(dt);

	switch(m_CurrentSelection)
	{
	case SBT_CONTINUE:
		if(m_ScenePreviewContinue)
			m_ScenePreviewContinue->Update(dt);
		break;
	case SBT_NEWGAME:
		if(m_ScenePreviewNew)
			m_ScenePreviewNew->Update(dt);
		break;
	case SBT_LOAD:
	case SBT_DELETESAVE:
		for(u32 i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
		{
			if(m_ScenePreviewSave[i])
			{
				if(m_CurrentSaveSlotSelected == i)
				{
					if(!m_ScenePreviewSave[i]->IsLoaded())
					{
						SaveData& save = ProfileData::GetInstance()->GetSave(i);
						if(save.Used)
						{
							m_ScenePreviewSave[i]->LoadPanel();
							if(save.DialogId > 0)
								m_ScenePreviewSave[i]->SetCurrentDialog(save.DialogId-1);
						}
					}
					else
						m_ScenePreviewSave[i]->Update(dt);
				}
				else if(m_ScenePreviewSave[i]->IsLoaded())
				{
					m_ScenePreviewSave[i]->UnloadPanel();
				}
			}
		}
		break;
	case SBT_CHAPTER:
		break;
	default:
		assert(0);
		break;
	}

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Update(dt);
		}
	}
}

// void StorySubMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
// {
// 	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
// 
// 	Engine* engine = Engine::GetInstance();
// 	const Dimension2& renderSize = engine->m_RenderSize;
// 	const Dimension2& originalSize = engine->m_OriginalSize;
// 	float innerScale = engine->m_InnerScale;
// 	const Position2& innerOffset = engine->m_InnerOffset;
// 
// 	IColor off = IsFading()? 
// 		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0), 4.0f*m_Fader.GetRatio()-3.0f) 
// 		: IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0);
// 	IColor on = COMPUTE_THEME_COLOR_2(0);
// 	float ratio = m_CurrentSelectionValidatedFader.GetRealRatio();
// 	IColor onToOff = MathUtils::GetInterpolatedColor(off, on, ratio);
// 	IColor offToOn = MathUtils::GetInterpolatedColor(on, off, ratio);
// 	Rectangle2 rect(innerOffset, Dimension2((u32)(innerScale*originalSize.Width), (u32)(innerScale*originalSize.Height)));
// 	Rectangle2 rects[4] = 
// 	{
// 		m_SubMenusArea[SMAT_LEFT].Area,
// 		Rectangle2(m_SubMenusArea[SMAT_BOTTOM].Area.UpperLeftCorner, Position2(m_SubMenusArea[SMAT_LEFT].Area.LowerRightCorner.X, m_SubMenusArea[SMAT_BOTTOM].Area.LowerRightCorner.Y)),
// 		Rectangle2(Position2(m_SubMenusArea[SMAT_LEFT].Area.LowerRightCorner.X, m_SubMenusArea[SMAT_BOTTOM].Area.UpperLeftCorner.Y), m_SubMenusArea[SMAT_BOTTOM].Area.LowerRightCorner),
// 		m_SubMenusArea[SMAT_CENTER].Area,
// 	};
// 	if(m_CurrentSubMenuActive == SMAT_LEFT)// 1000
// 	{
// 		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 0011 -> 1000
// 		{
// 			//fill2DRect(driver, rect, offToOn, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], offToOn, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011 -> 1111
// 			fill2DRect(driver, rects[1], on, on, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1100
// 			fill2DRect(driver, rects[2], on, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1000
// 			fill2DRect(driver, rects[3], offToOn, off, on, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011 -> 1010
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_CENTER) // 0100 -> 1000
// 		{
// 			//fill2DRect(driver, rect, offToOn, onToOff, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], offToOn, on, offToOn, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0101 -> 1111
// 			fill2DRect(driver, rects[1], offToOn, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0100 -> 1100
// 			fill2DRect(driver, rects[2], on, onToOff, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100 -> 1000
// 			fill2DRect(driver, rects[3], on, onToOff, on, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1010
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1000
// 		{
// 			//fill2DRect(driver, rect, offToOn, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
// 			fill2DRect(driver, rects[1], offToOn, offToOn, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1100
// 			fill2DRect(driver, rects[2], offToOn, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1000
// 			fill2DRect(driver, rects[3], offToOn, off, offToOn, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1010
// 		}
// 		else
// 		{
// 			//fill2DRect(driver, rect, on, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
// 			fill2DRect(driver, rects[1], on, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100
// 			fill2DRect(driver, rects[2], on, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1000
// 			fill2DRect(driver, rects[3], on, off, on, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1010
// 		}
// 	}
// 	else if(m_CurrentSubMenuActive == SMAT_CENTER)// 0100
// 	{
// 		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 0011 -> 0100
// 		{
// 			//fill2DRect(driver, rect, off, offToOn, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, offToOn, onToOff, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011 -> 0101
// 			fill2DRect(driver, rects[1], onToOff, on, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0100
// 			fill2DRect(driver, rects[2], on, on, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1100
// 			fill2DRect(driver, rects[3], offToOn, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011 -> 1111
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1000 -> 0100
// 		{
// 			//fill2DRect(driver, rect, onToOff, offToOn, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], onToOff, on, onToOff, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0101
// 			fill2DRect(driver, rects[1], onToOff, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100 -> 0100
// 			fill2DRect(driver, rects[2], on, offToOn, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1000 -> 1100
// 			fill2DRect(driver, rects[3], on, offToOn, on, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1010 -> 1111
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 0100
// 		{
// 			//fill2DRect(driver, rect, off, offToOn, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, offToOn, off, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0101
// 			fill2DRect(driver, rects[1], off, offToOn, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0100
// 			fill2DRect(driver, rects[2], offToOn, offToOn, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1100
// 			fill2DRect(driver, rects[3], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
// 		}
// 		else
// 		{
// 			//fill2DRect(driver, rect, off, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, on, off, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0101
// 			fill2DRect(driver, rects[1], off, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0100
// 			fill2DRect(driver, rects[2], on, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100
// 			fill2DRect(driver, rects[3], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
// 		}
// 	}
// 	else if(m_CurrentSubMenuActive == SMAT_BOTTOM)// 0011
// 	{
// 		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0100 -> 0011
// 		{
// 			//fill2DRect(driver, rect, off, onToOff, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, onToOff, offToOn, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0101 -> 0011
// 			fill2DRect(driver, rects[1], offToOn, on, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0100 -> 1111
// 			fill2DRect(driver, rects[2], on, on, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100 -> 1111
// 			fill2DRect(driver, rects[3], onToOff, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0011
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1000 -> 0011
// 		{
// 			//fill2DRect(driver, rect, onToOff, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], onToOff, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0011
// 			fill2DRect(driver, rects[1], on, on, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100 -> 1111
// 			fill2DRect(driver, rects[2], on, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1000 -> 1111
// 			fill2DRect(driver, rects[3], onToOff, off, on, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1010 -> 0011
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 0011
// 		{
// 			//fill2DRect(driver, rect, off, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0011
// 			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
// 			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
// 			fill2DRect(driver, rects[3], off, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0011
// 		}
// 		else
// 		{
// 			//fill2DRect(driver, rect, off, off, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, on, off, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011
// 			fill2DRect(driver, rects[1], off, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
// 			fill2DRect(driver, rects[2], on, on, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
// 			fill2DRect(driver, rects[3], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011
// 		}
// 	}
// 	else if(m_CurrentSubMenuActive == SMAT_POPUP)// 0000
// 	{
// 		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0100 -> 0000
// 		{
// 			//fill2DRect(driver, rect, off, onToOff, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, onToOff, off, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0101 -> 0000
// 			fill2DRect(driver, rects[1], off, onToOff, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0100 -> 0000
// 			fill2DRect(driver, rects[2], onToOff, onToOff, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100 -> 0000
// 			fill2DRect(driver, rects[3], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1000 -> 0000
// 		{
// 			//fill2DRect(driver, rect, onToOff, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
// 			fill2DRect(driver, rects[1], onToOff, onToOff, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1100 -> 0000
// 			fill2DRect(driver, rects[2], onToOff, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1000 -> 0000
// 			fill2DRect(driver, rects[3], onToOff, off, onToOff, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1010 -> 0000
// 		}
// 		else if(m_PreviousSubMenuActive == SMAT_BOTTOM) // 0011 -> 0000
// 		{
// 			//fill2DRect(driver, rect, off, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011 -> 0000
// 			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
// 			fill2DRect(driver, rects[2], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
// 			fill2DRect(driver, rects[3], off, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0011 -> 0000
// 		}
// 		else
// 		{
// 			//fill2DRect(driver, rect, off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 			fill2DRect(driver, rects[0], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 			fill2DRect(driver, rects[1], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 			fill2DRect(driver, rects[2], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 			fill2DRect(driver, rects[3], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 		}
// 	}
// 	else
// 	{
// 		assert(false);
// 		//fill2DRect(driver, rect, on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 		fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 		fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 		fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 		fill2DRect(driver, rects[3], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
// 	}
// }

void StorySubMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor off = IsFading()? 
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0), 4.0f*m_Fader.GetRatio()-3.0f) 
		: IColor(PANEL_MENU_FADER_ALPHA, 0, 0, 0);
	IColor on = COMPUTE_THEME_COLOR_FONT_GLOW(0);
	float ratio = m_CurrentSelectionValidatedFader.GetRealRatio();
	IColor onToOff = MathUtils::GetInterpolatedColor(off, on, ratio);
	IColor offToOn = MathUtils::GetInterpolatedColor(on, off, ratio);
	Rectangle2 rect(innerOffset, Dimension2((u32)(innerScale*originalSize.Width), (u32)(innerScale*originalSize.Height)));
	Rectangle2 rects[3] = 
	{
		m_SubMenusArea[SMAT_LEFT].Area,
		m_SubMenusArea[SMAT_CENTER].Area,
		m_SubMenusArea[SMAT_BOTTOM].Area
	};
	if(m_CurrentSubMenuActive == SMAT_LEFT)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 1011
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_CENTER) // 0111 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, onToOff, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1011
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, off, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1011
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, off, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_CENTER)
	{
		if(m_PreviousSubMenuActive == SMAT_BOTTOM)// 1111 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0111
		{
			fill2DRect(driver, rects[0], onToOff, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 0111
		{
			fill2DRect(driver, rects[0], off, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 0111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], off, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_BOTTOM)// 1111
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1010 -> 1111
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 1111
			fill2DRect(driver, rects[1], on, offToOn, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
		else if(m_PreviousSubMenuActive == SMAT_POPUP) // 0000 -> 1111
		{
			fill2DRect(driver, rects[0], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[1], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
			fill2DRect(driver, rects[2], offToOn, offToOn, offToOn, offToOn, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000 -> 1111
		}
		else
		{
			fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
			fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		}
	}
	else if(m_CurrentSubMenuActive == SMAT_POPUP)// 0000
	{
		if(m_PreviousSubMenuActive == SMAT_CENTER)// 0111 -> 0000
		{
			fill2DRect(driver, rects[0], off, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_LEFT) // 1011 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, off, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1011 -> 0000
		}
		else if(m_PreviousSubMenuActive == SMAT_BOTTOM) // 1111 -> 0000
		{
			fill2DRect(driver, rects[0], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
			fill2DRect(driver, rects[1], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
		}
		else
		{
			fill2DRect(driver, rects[0], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
			fill2DRect(driver, rects[1], off, off, off, off, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 0000
		}
		fill2DRect(driver, rects[2], onToOff, onToOff, onToOff, onToOff, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111 -> 0000
	}
	else
	{
		assert(false);
		fill2DRect(driver, rects[0], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[1], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
		fill2DRect(driver, rects[2], on, on, on, on, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));// 1111
	}
}

void StorySubMenuPanel::RenderNewGame(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	ScenePanel *scnPanel = (m_CurrentSelection==SBT_CONTINUE)? m_ScenePreviewContinue : m_ScenePreviewNew;
	if(scnPanel && m_ActiveBG)
	{
		ProfileData* profil = ProfileData::GetInstance();
		SaveData& save = profil->GetSave(PROFILE_AUTOSAVE_SLOT);

		const u32 margin = 30;

		IColor colorTex = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);
		IColor color = isCenterActive? COMPUTE_THEME_COLOR(255) : COMPUTE_THEME_COLOR(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);

		const Dimension2& size = m_ActiveBG->getSize();
		float scale2 = (float)size.Width*scale*0.9f/originalSize.Width;
		Position2 pos((s32)((((f32)originalSize.Width-0.95f*size.Width-100)*scale + offset.X)*innerScale), (s32)((((f32)60+0.05f*size.Height)*scale + offset.Y)*innerScale));
		if(isCenterActive)
		{
			float scale3 = scale2*innerScale;
			Position2 pos3 = pos + Position2((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f)) + innerOffset;

			if(m_Selec && isCenterActive)
			{
				float ratio = core::abs_(cosf(0.5f*core::PI*m_Time));
				IColor tint = m_ActiveValidationArea.IsSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(0), ratio);
				Rectangle2 sourceRect(Position2(0,0), m_Selec->getSize());
				float scaleX = (float)(s32)(scale3*originalSize.Width)/(m_Selec->getSize().Width - (18<<1));
				float scaleY = (float)(s32)(scale3*originalSize.Height)/(m_Selec->getSize().Height - (18<<1));

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos3.X,	(f32)pos3.Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				draw2DImage(driver, m_Selec, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
			}

			scnPanel->SetDimension(pos, scale2);
			scnPanel->Render();

// 			if(m_Font[3])
// 			{
// 				float ratio = fabs(cosf(1.0f*core::PI*m_Time));
// 				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(100), IColor(255, 0, 0, 0), ratio);
// 				m_Font[3]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_START), Rectangle2(pos3, D2Zero), tint, scale, true, true);
// 				m_Font[3]->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_START), Rectangle2(pos3, D2Zero), COMPUTE_THEME_COLOR(200), scale, true, true);
// 			}

			Position2 halfCorner((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f));
			m_ActiveValidationArea.Area = Rectangle2(pos3-halfCorner, pos3+halfCorner);
		}
		else
		{
			Texture* tex = GameDescManager::GetGameDesc()->ChaptersDesc[(m_CurrentSelection == SBT_CONTINUE)? save.ChapterId : 0].PreviewTex;
			Rectangle2 sourceRect(Position2(0,0), tex->getSize());
			scale2 = (float)size.Width*scale*innerScale*0.9f/tex->getSize().Width;
			pos += Position2((s32)(scale2*tex->getSize().Width*0.5f), (s32)(scale2*tex->getSize().Height*0.5f)) + innerOffset;

			core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X,	(f32)pos.Y, 0.0f)) *
				core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
				core::matrix4().setScale(core::vector3df((f32)scale2, (f32)scale2, 0.0f));

			draw2DImage(driver, tex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
		}

		// draw the description of the preview
		const core::stringw& strDate = save.GetDateTime();
		ChapterDesc* chDesc = GameDescManager::GetGameDesc()->GetChapterFromId((m_CurrentSelection==SBT_CONTINUE)?save.ChapterId:0);
		SceneDesc* scDesc = GameDescManager::GetGameDesc()->GetSceneFromId((m_CurrentSelection==SBT_CONTINUE)?save.ChapterId:0, (m_CurrentSelection==SBT_CONTINUE)?save.SceneId:0);
		const core::stringw& strChapter = chDesc->Name[engine->GetLanguage()];
		const core::stringw& strScene = scDesc->Name[engine->GetLanguage()];
		if(m_Font[1] && m_Font[3])
		{
// 			u32 longestStrSize = core::max_(m_Font[3]->getDimension(strChapter.c_str()).Width, 
// 			m_Font[1]->getDimension(strScene.c_str()).Width, 
// 			m_Font[1]->getDimension(strDate.c_str()).Width);

			Position2 pos = Position2((s32)(((((f32)originalSize.Width-(size.Width>>1)-100)*scale + offset.X)*innerScale)/* - (longestStrSize>>1)*/), 
				(s32)((((f32)((size.Height)+60)*scale + offset.Y)*innerScale)));
			pos += innerOffset;

			pos.Y -= (u32)((float)margin * innerScale * scale);

			// font 0 : 48pxl : Date
			pos.Y -= (u32)(scale * m_Font[1]->getSize());
			m_Font[1]->draw(strDate, Rectangle2(pos, D2Zero), color, scale, true, false);			
			// font 0 : 48pxl : Scene name
			pos.Y -= (u32)(scale * m_Font[1]->getSize()) + (u32)((float)10 * innerScale * scale);
			m_Font[1]->draw(strScene, Rectangle2(pos, D2Zero), color, scale, true, false);
			// font 2 : 60pxl : Chapter name
			pos.Y -= (u32)(scale * m_Font[3]->getSize()) + (u32)((float)20 * innerScale * scale);
			m_Font[3]->draw(strChapter, Rectangle2(pos, D2Zero), color, scale, true, false);
		}
	}
}

void StorySubMenuPanel::RenderSave(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	if(m_SaveSlotTex && m_ActiveBG)
	{
		IColor tint = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		Rectangle2 sourceRect(Position2(0,0), m_SaveSlotTex->getSize());

		const u32 margin = 30;
		const u32 slotWidth = (size.Width - (margin<<1))/4;
		const u32 slotHeight = (size.Height - (margin<<1))/3;

		ProfileData* profil = ProfileData::GetInstance();

		for(u32 i = 0; i < 3; ++i)
		{
			for(u32 j = 0; j < 4; ++j)
			{
				u32 slotIndex = 4*i + j;
				SaveData& save = profil->GetSave(slotIndex);

				u32 grey = (slotIndex == m_CurrentSaveSlotSelected)? 255:50;
				u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);

				Position2 rawPos(originalSize.Width-size.Width-100+margin + j*slotWidth+(slotWidth>>1), 
					60+margin + i*slotHeight+(slotHeight>>1));

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(((f32)rawPos.X*scale+offset.X)*innerScale + innerOffset.X, 
					((f32)rawPos.Y*scale+offset.Y)*innerScale + innerOffset.Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

				draw2DImage(driver, m_SaveSlotTex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

				if(save.Used)
				{
					// draw the preview
					if(m_SaveSlotTex)
					{
						const Dimension2& size = m_SaveSlotTex->getSize();
						float scale2 = (float)size.Width*scale*0.9f/originalSize.Width;
						Position2 pos((s32)((((f32)rawPos.X - 0.45f*size.Width)*scale + offset.X)*innerScale), (s32)((((f32)rawPos.Y - 0.45f*size.Height)*scale + offset.Y)*innerScale));
						if(isCenterActive && m_ScenePreviewSave[slotIndex] && slotIndex == m_CurrentSaveSlotSelected)// dynamic preview of the scene if available and slot selected
						{
							if(m_Selec && isCenterActive)
							{
								float scale3 = scale2*innerScale;
								Position2 pos3 = pos + Position2((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f)) + innerOffset;

								float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
								IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(100), ratio);
								Rectangle2 sourceRect(Position2(0,0), m_Selec->getSize());
								float scaleX = (float)(s32)(scale3*originalSize.Width)/(m_Selec->getSize().Width - (18<<1));
								float scaleY = (float)(s32)(scale3*originalSize.Height)/(m_Selec->getSize().Height - (18<<1));

								core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos3.X,	(f32)pos3.Y, 0.0f)) *
									core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
									core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

								draw2DImage(driver, m_Selec, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
							}

							m_ScenePreviewSave[slotIndex]->SetDimension(pos, scale2);
							m_ScenePreviewSave[slotIndex]->Render();
						}
						else// else static preview of the chapter
						{
							Texture* tex = GameDescManager::GetGameDesc()->ChaptersDesc[save.ChapterId].PreviewTex;
							Rectangle2 sourceRect(Position2(0,0), tex->getSize());
							scale2 = (float)size.Width*scale*innerScale*0.9f/tex->getSize().Width;
							pos += Position2((s32)(scale2*tex->getSize().Width*0.5f), (s32)(scale2*tex->getSize().Height*0.5f)) + innerOffset;

							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X,	(f32)pos.Y, 0.0f)) *
								core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df((f32)scale2, (f32)scale2, 0.0f));

							draw2DImage(driver, tex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
						}
					}
					if(m_Font[0])
					{
						float ratio = fabs(cosf(0.5f*core::PI*m_Time));
						u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 200:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
						IColor color = (slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_SELECTION(alpha) : COMPUTE_THEME_COLOR(alpha);
						IColor tint = MathUtils::GetInterpolatedColor((slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_GLOW(isCenterActive?alpha:alpha>>1) : COMPUTE_THEME_COLOR(isCenterActive?alpha:alpha>>1), COMPUTE_THEME_COLOR(isCenterActive?alpha>>1:alpha>>2), 1.0f-ratio);
						const core::stringw& str = save.GetStringCompact();
						if(slotIndex == m_CurrentSaveSlotSelected)
							m_Font[0]->drawGauss(str, Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), tint, scale, true, false);
						m_Font[0]->draw(str, Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), color, scale, true, false);
					}
				}
				else
				{
					float ratio = fabs(cosf(0.5f*core::PI*m_Time));
					u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 200:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
					IColor color = (slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_SELECTION(alpha) : COMPUTE_THEME_COLOR(alpha);
					IColor tint = MathUtils::GetInterpolatedColor((slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_GLOW(isCenterActive?alpha:alpha>>1) : COMPUTE_THEME_COLOR(isCenterActive?alpha:alpha>>1), COMPUTE_THEME_COLOR(isCenterActive?alpha>>1:alpha>>2), 1.0f-ratio);
					if(m_Font[0])
					{
						if(slotIndex == m_CurrentSaveSlotSelected)
							m_Font[0]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_EMPTY), Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), tint, scale, true, false);
						m_Font[0]->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_EMPTY), Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), color, scale, true, false);
					}
				}

				if(slotIndex == PROFILE_AUTOSAVE_SLOT || slotIndex == PROFILE_QUICKSAVE_SLOT)// auto + quick saves slot strings
				{
					const wchar_t* const s[2] = {Engine::GetLanguageString(engine->GetLanguage(), SD_AUTOSAVE), Engine::GetLanguageString(engine->GetLanguage(), SD_QUICKSAVE)};
					float ratio = fabs(cosf(0.5f*core::PI*m_Time));
					u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 200:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
					IColor color = COMPUTE_THEME_COLOR(alpha);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(isCenterActive?alpha:alpha>>1), COMPUTE_THEME_COLOR(isCenterActive?alpha>>1:alpha>>2), 1.0f-ratio);
					if(m_Font[0])
					{
						m_Font[0]->drawGauss(s[slotIndex], Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y - 20*innerScale*scale)), D2Zero), tint, scale, true, false);
						m_Font[0]->draw(s[slotIndex], Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y - 20*innerScale*scale)), D2Zero), color, scale, true, false);
					}
				}

				// delete buttons
				if(m_CurrentSelection == SBT_DELETESAVE && save.Used && slotIndex != PROFILE_AUTOSAVE_SLOT)
				{
					float ratio = (cosf(2.0f*core::PI*m_Time)+1.0f)*0.5f;
					IColor tint = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? MathUtils::GetInterpolatedColor(IColor(50, 255, 255, 255), COLOR_WHITE, ratio) : COLOR_WHITE) : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY, 255, 255, 255);

					Texture* tex = (isCenterActive && slotIndex == m_CurrentSaveSlotSelected)? m_SaveSlotDeleteA : m_SaveSlotDelete;
					const Dimension2& size = m_SaveSlotTex->getSize();
					Rectangle2 sourceRect(Position2(0,0), tex->getSize());
					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((((f32)rawPos.X+((size.Width-(sourceRect.getWidth()>>1))>>1))*scale+offset.X)*innerScale + innerOffset.X, 
						((f32)(rawPos.Y+((size.Height-(sourceRect.getHeight()>>1))>>1))*scale+offset.Y)*innerScale + innerOffset.Y, 0.0f)) *
						core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
						core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

					draw2DImage(driver, tex, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
				}

				if(slotIndex == m_CurrentSaveSlotSelected)
				{
					Position2 pos((s32)(((f32)rawPos.X*scale+offset.X)*innerScale + innerOffset.X), (s32)(((f32)rawPos.Y*scale+offset.Y)*innerScale + innerOffset.Y));
					Position2 halfCorner((s32)(innerScale*scale*m_SaveSlotTex->getSize().Width*0.5f), (s32)(innerScale*scale*m_SaveSlotTex->getSize().Height*0.5f));
					m_ActiveValidationArea.Area = Rectangle2(pos-halfCorner, pos+halfCorner);
				}
			}
		}
	}
}

void StorySubMenuPanel::RenderChapter(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
#ifndef DEMO_VERSION
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);
	if(m_ActiveBG)
	{
		const Dimension2& size = m_ActiveBG->getSize();

		const u32 margin = 30;
		const u32 slotWidth = (size.Width - (margin<<1))/4;
		const u32 slotHeight = (size.Height - (margin<<1))/6;
		const u32 innerMargin = 10;

		ProfileData* profil = ProfileData::GetInstance();

		float ratio = core::abs_(cosf(1.0f*core::PI*m_Time + core::PI*0.5f));

		u32 nbChapters = GameDescManager::GetGameDesc()->ChaptersDesc.size();
#ifdef IRON_ICE_ENGINE
		nbChapters = (ProfileData::GetInstance()->m_PDS.m_ProgressionCH != 0xff)?
			ProfileData::GetInstance()->m_PDS.m_ProgressionCH + 1 : GameDescManager::GetGameDesc()->ChaptersDesc.size();
#endif

		// Chapters part left
		for(s32 i = -3; i <= 3; ++i)
		{
			if(i == 3 && (!m_CurrentChapterSelectionFader.IsFading() || m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_RIGHT))
				continue;
			else if(i == -3 && (!m_CurrentChapterSelectionFader.IsFading() || m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_LEFT))
				continue;

			s32 slotIndex = i;
			slotIndex += m_CurrentChapterSelected;
			if(USE_CIRCULAR_MENU)
			{
				slotIndex += nbChapters;
				slotIndex %= nbChapters;
			}
			else
			{
				if(slotIndex < 0 || (u32)slotIndex >= nbChapters)
					continue;
			}

			Texture* tex = GameDescManager::GetGameDesc()->ChaptersDesc[slotIndex].PreviewTex;
			float previewScale = (float)(slotHeight - innerMargin)/tex->getSize().Height;
			Rectangle2 sourceRect(Position2(0,0), tex->getSize());

			bool isAtCenter = ((m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_RIGHT) && (i == 0 || i == -1) || 
				(m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_LEFT) && (i == 0 || i == 1));
			u32 deltaY = isAtCenter? slotHeight+(slotHeight>>1) : slotHeight;
			s32 offsetY = (u32)((f32)deltaY * m_CurrentChapterSelectionFader.GetRealRatio());
			if(m_CurrentChapterSelectionFader.IsFading() && m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_LEFT)
				offsetY = -offsetY;
			Position2 rawPos(originalSize.Width-size.Width-100+margin +(slotWidth>>1), 
				(size.Height>>1) + 60 + i*slotHeight + offsetY);

			if(abs(i) >= 1)
				rawPos.Y += (i > 0)? (slotHeight>>1) : -(s32)(slotHeight>>1);

			const u32 alpha = (i == 0)? 128 : 32 + (u32)(96.0f * core::max_(1.0f - (float)fabs((float)rawPos.Y-((size.Height>>1)+60))/(size.Height>>1), 0.0f));
			IColor color = isCenterActive? ((i == 0 && !m_CurrentChapterSelectionFader.IsFading())? COLOR_WHITE : IColor(alpha, 255, 255, 255)) : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY, 255, 255, 255);
			if(i != 0 && !m_CurrentChapterSelectionFader.IsFading() && m_ChapterSlotArea[i+2].IsSelected)
				color = COLOR_WHITE;//MathUtils::GetInterpolatedColor(WHITE, color, ratio);

			if(m_CurrentChapterSelectionFader.IsFading())
			{
				if(m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_RIGHT)
				{
					if(i==2)
					{
						float ratio = 1.0f-m_CurrentChapterSelectionFader.GetRealRatio();
						color = IColor((u32)(ratio*ratio*ratio*ratio*alpha), 255, 255, 255);
					}
					else if(i==-3)
					{
						float ratio = m_CurrentChapterSelectionFader.GetRealRatio();
						color = IColor((u32)(ratio*ratio*ratio*ratio*alpha), 255, 255, 255);
					}
				}
				else if(m_CurrentChapterSelectionFader.FadeSide == Fader::FS_FADE_LEFT)
				{
					if(i==-2)
					{
						float ratio = 1.0f-m_CurrentChapterSelectionFader.GetRealRatio();
						color = IColor((u32)(ratio*ratio*ratio*ratio*alpha), 255, 255, 255);
					}
					else if(i==3)
					{
						float ratio = m_CurrentChapterSelectionFader.GetRealRatio();
						color = IColor((u32)(ratio*ratio*ratio*ratio*alpha), 255, 255, 255);
					}
				}
			}

			core::vector3df vec(((f32)rawPos.X*scale+offset.X)*innerScale + innerOffset.X, 
				((f32)rawPos.Y*scale+offset.Y)*innerScale + innerOffset.Y, 0.0f);
			if(m_Selec && isCenterActive && i == 0 && !m_CurrentChapterSelectionFader.IsFading())
			{
				IColor tint = COMPUTE_THEME_COLOR_FONT_SELECTION(255);
				Rectangle2 sourceRect(Position2(0,0), m_Selec->getSize());
				float scaleX = previewScale*innerScale*scale*tex->getSize().Width/(m_Selec->getSize().Width - (18<<1));
				float scaleY = previewScale*innerScale*scale*tex->getSize().Height/(m_Selec->getSize().Height - (18<<1));

				core::matrix4 mat = core::matrix4().setTranslation(vec) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

				draw2DImage(driver, m_Selec, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
			}

			core::matrix4 mat = core::matrix4().setTranslation(vec) *
				core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
				core::matrix4().setScale(core::vector3df((f32)previewScale*innerScale*scale, (f32)previewScale*innerScale*scale, 0.0f));

			draw2DImage(driver, tex, sourceRect, mat, true, color, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
		}

		if(m_Arrows[0] && m_Arrows[1])
		{
			IColor tint = isCenterActive? MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(200), COMPUTE_THEME_COLOR_FONT_GLOW(64), ratio) : COMPUTE_THEME_COLOR(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);
// 			if(m_SceneSelectionArea[0].IsSelected)
// 				tint = COMPUTE_THEME_COLOR(255);//WHITE;

			{
				Position2 pos(originalSize.Width-size.Width-100+margin +(slotWidth>>1), 
					(size.Height>>1) + 60 - (s32)(3.1f*slotHeight));

				Dimension2 size = m_Arrows[0]->getSize();
				Rectangle2 sourceRect(Position2(0,0), size);

				core::vector3df vec(((f32)pos.X*scale+offset.X)*innerScale + innerOffset.X, 
					((f32)pos.Y*scale+offset.Y)*innerScale + innerOffset.Y, 0.0f);
				core::matrix4 mat = core::matrix4().setTranslation(vec) *
					core::matrix4().setRotationAxisRadians(90*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale*scale*0.75f, (f32)innerScale*scale*0.75f, 0.0f));

				draw2DImage(driver,m_Arrows[0], sourceRect, mat, true, ((s32)m_CurrentChapterSelected-2 > 0)? tint : COMPUTE_THEME_COLOR(20), true, USE_PREMULTIPLIED_ALPHA);

// 				Position2 halfCorner((s32)(innerScale*scale/**0.75f*/*size.Width*0.5f), (s32)(innerScale*scale/**0.75f*/*size.Height*0.5f));
// 				m_SceneSelectionArea[0].Area = Rectangle2(pos-halfCorner, pos+halfCorner);
			}
			if(m_CurrentChapterSelected <= 0)
			{
				//m_SceneSelectionArea[0].Area = Rectangle2(0,0,0,0);
			}

			{
				Position2 pos(originalSize.Width-size.Width-100+margin +(slotWidth>>1), 
					(size.Height>>1) + 60 + (s32)(3.1f*slotHeight));

				Dimension2 size = m_Arrows[1]->getSize();
				Rectangle2 sourceRect(Position2(0,0), size);

				core::vector3df vec(((f32)pos.X*scale+offset.X)*innerScale + innerOffset.X, 
					((f32)pos.Y*scale+offset.Y)*innerScale + innerOffset.Y, 0.0f);
				core::matrix4 mat = core::matrix4().setTranslation(vec) *
					core::matrix4().setRotationAxisRadians(90*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale*scale*0.75f, (f32)innerScale*scale*0.75f, 0.0f));

				draw2DImage(driver, m_Arrows[1], sourceRect, mat, true, ((s32)m_CurrentChapterSelected+2 < (s32)nbChapters-1)? tint : COMPUTE_THEME_COLOR(20), true, USE_PREMULTIPLIED_ALPHA);
				
// 				Position2 halfCorner((s32)(innerScale*scale/**0.75f*/*size.Width*0.5f), (s32)(innerScale*scale/**0.75f*/*size.Height*0.5f));
// 				m_SceneSelectionArea[1].Area = Rectangle2(pos-halfCorner, pos+halfCorner);
			}
			if((s32)m_CurrentChapterSelected >= (s32)nbChapters-1)
			{
				//m_SceneSelectionArea[1].Area = Rectangle2(0,0,0,0);
			}
		}

		// preview part
		{
			IColor colorTex = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);
			IColor color = isCenterActive? COMPUTE_THEME_COLOR(255) : COMPUTE_THEME_COLOR(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);

			const Dimension2& size = m_ActiveBG->getSize();
			float scale2 = (((float)size.Width - slotWidth - (margin<<1))*scale)/originalSize.Width;
			Position2 pos((s32)((((f32)originalSize.Width-size.Width-100+slotWidth+margin)*scale + offset.X)*innerScale), (s32)((((f32)60+0.05f*size.Height)*scale + offset.Y)*innerScale));
			Position2 posPreview = pos;
			pos += innerOffset;
// 			if(isCenterActive)
// 			{
// 				scnPanel->SetDimension(pos, scale2);
// 				scnPanel->Render();
// 			}
// 			else
			{
				Texture* tex = GameDescManager::GetGameDesc()->ChaptersDesc[m_CurrentChapterSelected].PreviewTex;
				Rectangle2 sourceRect(Position2(0,0), tex->getSize());
				scale2 = ((float)size.Width - slotWidth - (margin<<1))*scale*innerScale/tex->getSize().Width;
				pos += Position2((s32)(scale2*tex->getSize().Width*0.5f), (s32)(scale2*tex->getSize().Height*0.5f));

				if(m_Selec && isCenterActive)
				{
					float ratio = core::abs_(cosf(0.5f*core::PI*m_Time));
					IColor tint = m_ActiveValidationArea.IsSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(255), COMPUTE_THEME_COLOR(0), ratio);
					Rectangle2 sourceRect(Position2(0,0), m_Selec->getSize());
					float scaleX = ((float)size.Width - slotWidth - (margin<<1))*scale*innerScale/(m_Selec->getSize().Width - (18<<1));
					float scaleY = scale2*tex->getSize().Height/(m_Selec->getSize().Height - (18<<1));

					core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X,	(f32)pos.Y, 0.0f)) *
						core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
						core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));

					draw2DImage(driver, m_Selec, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
				}

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X,	(f32)pos.Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)scale2, (f32)scale2, 0.0f));

				draw2DImage(driver, tex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

// 				if(isCenterActive && m_Font[3])
// 				{
// 					float ratio = fabs(cosf(1.0f*core::PI*m_Time));
// 					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_2(100), IColor(255, 0, 0, 0), ratio);
// 					m_Font[3]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_START), Rectangle2(pos, D2Zero), tint, scale, true, true);
// 					m_Font[3]->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_START), Rectangle2(pos, D2Zero), IColor(200,200,255,255), scale, true, true);
// 				}

				Position2 halfCorner((s32)(scale2*tex->getSize().Width*0.5f), (s32)(scale2*tex->getSize().Height*0.5f));
				m_ActiveValidationArea.Area = Rectangle2(pos-halfCorner, pos+halfCorner);
			}

			// draw the description of the preview
			ChapterDesc* chDesc = GameDescManager::GetGameDesc()->GetChapterFromId(m_CurrentChapterSelected);
			SceneDesc* scDesc = GameDescManager::GetGameDesc()->GetSceneFromId(m_CurrentChapterSelected, m_CurrentSceneSelected);
			const core::stringw& strChapter = chDesc->Name[engine->GetLanguage()];
			const core::stringw& strScene = scDesc->Name[engine->GetLanguage()];
			if(m_Font[2] && m_Font[3])
			{
				pos.Y = (s32)((((f32)((size.Height)+60)*scale + offset.Y)*innerScale))+innerOffset.Y;

				pos.Y -= (u32)((float)(margin) * innerScale * scale);

				// font 1 : 40pxl : Scene name
				pos.Y -= (u32)(scale * m_Font[2]->getSize()) + (u32)((float)80 * innerScale * scale);
				m_Font[1]->draw(strScene, Rectangle2(pos, D2Zero), color, scale, true, false);
				if(m_Arrows[0] && m_Arrows[1])
				{
					//if(m_CurrentSceneSelected > 0)
					{
						IColor tint = isCenterActive? MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(200), COMPUTE_THEME_COLOR(64), ratio) : COMPUTE_THEME_COLOR(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);
						if(m_SceneSelectionArea[0].IsSelected)
							tint = COMPUTE_THEME_COLOR_FONT_SELECTION(255);//WHITE;
						Dimension2 size = m_Arrows[0]->getSize();
						Rectangle2 sourceRect(Position2(0,0), size);
						Position2 pos(posPreview.X + (s32)(((float)size.Width*0.5f)*scale*innerScale), pos.Y + (s32)((float)m_Font[2]->getSize()*0.75f));

						core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos.X), (f32)(pos.Y), 0.0f)) *
							core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
							core::matrix4().setScale(core::vector3df((f32)innerScale*scale/**0.75f*/, (f32)innerScale*scale/**0.75f*/, 0.0f));

						draw2DImage(driver,m_Arrows[0], sourceRect, mat, true, (m_CurrentSceneSelected > 0)? tint : COMPUTE_THEME_COLOR(20), true, USE_PREMULTIPLIED_ALPHA);

						Position2 halfCorner((s32)(innerScale*scale/**0.75f*/*size.Width*0.5f), (s32)(innerScale*scale/**0.75f*/*size.Height*0.5f));
						m_SceneSelectionArea[0].Area = Rectangle2(pos-halfCorner, pos+halfCorner);
					}
					/*else */if(m_CurrentSceneSelected <= 0)
					{
						m_SceneSelectionArea[0].Area = Rectangle2(0,0,0,0);
					}

					u32 nbScenes = chDesc->ScenesDesc.size();
#ifdef IRON_ICE_ENGINE
					if(m_CurrentChapterSelected == nbChapters - 1)
						nbScenes = (ProfileData::GetInstance()->m_PDS.m_ProgressionSC != 0xff)?
						ProfileData::GetInstance()->m_PDS.m_ProgressionSC + 1 : chDesc->ScenesDesc.size();
#endif
					//if((s32)m_CurrentSceneSelected < (s32)nbScenes-1)
					{
						IColor tint = isCenterActive? MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(200), COMPUTE_THEME_COLOR(64), ratio) : COMPUTE_THEME_COLOR(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY);
						if(m_SceneSelectionArea[1].IsSelected)
							tint = COMPUTE_THEME_COLOR_FONT_SELECTION(255);//WHITE;
						Dimension2 size = m_Arrows[1]->getSize();
						Rectangle2 sourceRect(Position2(0,0), size);
						Position2 pos2((s32)((((f32)originalSize.Width-100-(f32)size.Width*0.5f-margin)*scale + offset.X)*innerScale)+innerOffset.X, pos.Y + (s32)((float)m_Font[2]->getSize()*0.75f));

						core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
							core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
							core::matrix4().setScale(core::vector3df((f32)innerScale*scale/**0.75f*/, (f32)innerScale*scale/**0.75f*/, 0.0f));

						draw2DImage(driver, m_Arrows[1], sourceRect, mat, true, ((s32)m_CurrentSceneSelected < (s32)nbScenes-1)? tint : IColor(20,255,255,255), true, USE_PREMULTIPLIED_ALPHA);
						Position2 halfCorner((s32)(innerScale*scale/**0.75f*/*size.Width*0.5f), (s32)(innerScale*scale/**0.75f*/*size.Height*0.5f));
						m_SceneSelectionArea[1].Area = Rectangle2(pos2-halfCorner, pos2+halfCorner);
					}
					/*else */if((s32)m_CurrentSceneSelected >= (s32)nbScenes-1)
					{
						m_SceneSelectionArea[1].Area = Rectangle2(0,0,0,0);
					}
				}

				// font 2 : 48pxl : Chapter name
				pos.Y -= (u32)(scale * m_Font[3]->getSize()) + (u32)((float)80 * innerScale * scale);
				m_Font[2]->draw(strChapter, Rectangle2(pos, D2Zero), color, scale, true, false);
			}
		}
	}
#endif
}

void StorySubMenuPanel::_Render(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	// draw the left submenu
	if(m_Font[2] && m_Font[3])
	{
		s32 x = (u32)((originalSize.Width/(f32)SBT_MAX)*innerScale);

#ifdef DEMO_VERSION
		IColor notAvailableColor = IColor(200, 100, 100, 100);
#endif
		for(int i = 0; i < SBT_MAX; ++i)
		{
			core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), i+SD_CONTINUE));
			bool isSelected = m_ButtonsArea[i].IsSelected;
			u32 alpha = (m_CurrentSubMenuActive != SMAT_CENTER)? 255 : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
			IColor color = isSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : COMPUTE_THEME_COLOR((m_CurrentSubMenuActive != SMAT_CENTER)? alpha : 16);
#ifdef DEMO_VERSION
			if(i == SBT_CHAPTER)
				color = notAvailableColor;
#endif
			const Rectangle2& area = m_ButtonsArea[i].Area;
			Rectangle2 rect(Position2((s32)((float)(area.UpperLeftCorner.X-innerOffset.X)*scale+(float)offset.X*innerScale+innerOffset.X),
				(s32)((float)(area.UpperLeftCorner.Y-innerOffset.Y)*scale+(float)offset.Y*innerScale+innerOffset.Y)),
				Dimension2((u32)((float)area.getWidth()*scale), (u32)((float)area.getHeight()*scale)));
			//if(isSelected)
			{
				float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
#ifdef DEMO_VERSION
				if(i == SBT_CHAPTER)
					tint = notAvailableColor;
#endif
				(isSelected?m_Font[3]:m_Font[2])->drawGauss(str, rect, tint, scale, false, false);
			}
			(isSelected?m_Font[3]:m_Font[2])->draw(str, rect, color, scale, false, false);
		}
	}

	// center part
	bool isCenterActive = (m_CurrentSubMenuActive == SMAT_CENTER);

	// draw the active BG at the center
	if(m_ActiveBG)
	{
		IColor tint = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = m_ActiveBG->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(originalSize.Width-(size.Width>>1)-100)*innerScale),
			(s32)((f32)((size.Height>>1)+60)*innerScale));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X*scale+(f32)offset.X*innerScale + innerOffset.X, (f32)pos.Y*scale+(f32)offset.Y*innerScale + innerOffset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

		draw2DImage(driver, m_ActiveBG, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	}

	switch(m_CurrentSelection)
	{
	case SBT_CONTINUE:
	case SBT_NEWGAME:
		RenderNewGame(scale, offset);
		break;
	case SBT_LOAD:
	case SBT_DELETESAVE:
		RenderSave(scale, offset);
		break;
	case SBT_CHAPTER:
#ifndef DEMO_VERSION
		RenderChapter(scale, offset);
#endif
		break;
	default:
		assert(0);
		break;
	}

	RenderFader(scale, offset);

	if(m_CurrentSubMenuActive == SMAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Render();
		}
	}
}

void StorySubMenuPanel::PreRender()
{
	// render here only if we have a render target : we render in the render target
	if(m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		// set render target texture
		IColor color = COLOR_TRANSPARENT;
		color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(m_RenderTarget, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		_Render();

		// set back old render target
		// The buffer might have been distorted, so clear it
		color = COLOR_TRANSPARENT;
		if(USE_PREMULTIPLIED_ALPHA)
			color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(0, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

void StorySubMenuPanel::Render()
{
	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)),
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_Render(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();

		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)*Engine::GetInstance()->m_InnerScale), 0);

		IColor tint = COLOR_WHITE;

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((Engine::GetInstance()->m_RenderSize.Width>>1),
			(Engine::GetInstance()->m_RenderSize.Height>>1));

		if(m_CurrentSelectionValidatedFader.IsFading())
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// other to bottom fade
				ratio = 0.75f * ratio + m_CurrentSelectionValidatedFader.GetRealRatio()*0.25f;// 1 to 0.75
			else if(m_PreviousSubMenuActive == SMAT_BOTTOM)// bottom to other fade
				ratio = 0.75f * ratio + (1.0f - m_CurrentSelectionValidatedFader.GetRealRatio())*0.25f;// 0.75 to 1
		}
		else
		{
			if(m_CurrentSubMenuActive == SMAT_BOTTOM)// bottom
				ratio = 0.75f*ratio;// 0.75
		}

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

bool StorySubMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return false;

	if(m_PopupPanel)
	{
		if(m_PopupPanel->OnEvent(event))
		{
			if(!m_PopupPanel->IsVisible())// this means that the popup just closed
			{
				if(m_CurrentSelection == SBT_DELETESAVE)
				{
					if(m_PopupPanel->GetResult() == PopupPanel::PRT_YES)
					{
						// delete the current save
						ProfileData* profile = ProfileData::GetInstance();
						SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
						save.Used = false;

						profile->WriteSave();
					}
				}
				ChangeSubMenuActive(m_PreviousSubMenuActive);
			}

			return true;
		}
	}

	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Engine* engine = Engine::GetInstance();
			const Dimension2& renderSize = engine->m_RenderSize;
			const Dimension2& originalSize = engine->m_OriginalSize;
			float innerScale = engine->m_InnerScale;
			const Position2& innerOffset = engine->m_InnerOffset;

			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);

			// check which sub menu is active
			if(!m_CurrentSelectionValidatedFader.IsFading())
			{
				u32 oldSubMenuActive = m_CurrentSubMenuActive;
				for(int i = 0; i < SMAT_MAX; ++i)
				{
					m_SubMenusArea[i].IsSelected = false;
					if(m_SubMenusArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentSubMenuActive = i;
					}
				}
				m_SubMenusArea[m_CurrentSubMenuActive].IsSelected = true;
				if(oldSubMenuActive != m_CurrentSubMenuActive)
				{
					m_PreviousSubMenuActive = oldSubMenuActive;
					m_CurrentSelectionValidatedFader.StartFade();
				}
			}

			// Left menu
			u32 oldSelection = m_CurrentSelection;
			{
				for(int i = 0; i < SBT_MAX; ++i)
				{
					m_ButtonsArea[i].IsSelected = false;
					if(m_ButtonsArea[i].Area.isPointInside(mousePos))
					{
						m_CurrentSelection = i;
					}
				}
#ifdef DEMO_VERSION
				if(m_CurrentSelection == SBT_CHAPTER)
					m_CurrentSelection = oldSelection;
#endif
				m_ButtonsArea[m_CurrentSelection].IsSelected = true;
			}
			bool hasSelectionChanged = (oldSelection != m_CurrentSelection);

			// center area
			m_ActiveValidationArea.IsSelected = m_ActiveValidationArea.Area.isPointInside(mousePos);
			switch(m_CurrentSelection)
			{
			case SBT_CONTINUE:
				break;
			case SBT_NEWGAME:
				break;
			case SBT_LOAD:
			case SBT_DELETESAVE:
				if(m_ActiveBG)
				{
					const Dimension2& size = m_ActiveBG->getSize();

					u32 margin = 30;
					u32 slotWidth = (size.Width - (margin<<1))/4;
					u32 slotHeight = (size.Height - (margin<<1))/3;

					u32 oldSaveSlotSelection = m_CurrentSaveSlotSelected;
					{
						bool slotHovered = false;
						for(u32 i = 0; i < 3 && !slotHovered; ++i)
						{
							for(u32 j = 0; j < 4 && !slotHovered; ++j)
							{
								u32 slotIndex = 4*i + j;

								Position2 rawPos(originalSize.Width-size.Width-100+margin + j*slotWidth+(slotWidth>>1),
									60+margin + i*slotHeight+(slotHeight>>1));
								Position2 pos((s32)(((f32)rawPos.X - (slotWidth>>1))*innerScale),
									(s32)(((f32)rawPos.Y - (slotHeight>>1))*innerScale));

								Rectangle2 rect(pos+innerOffset, Dimension2((u32)((f32)slotWidth*innerScale), (u32)((f32)slotHeight*innerScale)));
								if(rect.isPointInside(mousePos))
								{
									m_CurrentSaveSlotSelected = slotIndex;
									slotHovered = true;
								}
							}
						}
					}

//					hasSelectionChanged |= (oldSaveSlotSelection != m_CurrentSaveSlotSelected);
				}
				break;
			case SBT_CHAPTER:
#ifndef DEMO_VERSION
				{
					s32 currentSelection = -1;
					for(int i = 0; i < 5; ++i)
					{
						m_ChapterSlotArea[i].IsSelected = false;
						if(m_ChapterSlotArea[i].Area.isPointInside(mousePos))
						{
							currentSelection = i;
						}
					}
					if(currentSelection >= 0)
						m_ChapterSlotArea[currentSelection].IsSelected = true;

					currentSelection = -1;
					for(int i = 0; i < 2; ++i)
					{
						m_SceneSelectionArea[i].IsSelected = false;
						if(m_SceneSelectionArea[i].Area.isPointInside(mousePos))
						{
							currentSelection = i;
						}
					}
					if(currentSelection >= 0)
						m_SceneSelectionArea[currentSelection].IsSelected = true;
				}
#endif
				break;
			default:
				assert(0);
				break;
			}
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			ProfileData* profile = ProfileData::GetInstance();
			{
				SaveData& save = profile->GetSave(PROFILE_AUTOSAVE_SLOT);
				if(!m_ScenePreviewContinue && save.Used)
				{
					m_ScenePreviewContinue = new ScenePanel();
					m_ScenePreviewContinue->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(save.ChapterId, save.SceneId));
					m_ScenePreviewContinue->LoadPanel();
					if(save.DialogId > 0)
						m_ScenePreviewContinue->SetCurrentDialog(save.DialogId-1);
					m_ScenePreviewContinue->UsePremultipliedAlpha(true);
					m_ScenePreviewContinue->m_PlaySounds = false;
					m_ScenePreviewContinue->m_Loop1stDialog = true;
					m_ScenePreviewContinue->ForceLoadScene();
				}
			}

			if(!m_ScenePreviewNew)
			{
				m_ScenePreviewNew = new ScenePanel();
				m_ScenePreviewNew->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(0, 0));
				m_ScenePreviewNew->LoadPanel();
				m_ScenePreviewNew->UsePremultipliedAlpha(true);
				m_ScenePreviewNew->m_PlaySounds = false;
				m_ScenePreviewNew->m_Loop1stDialog = true;
				m_ScenePreviewNew->ForceLoadScene();
			}

			for(u32 i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
			{
				SaveData& save = profile->GetSave(i);
				if(save.Used)
				{
					if(!m_ScenePreviewSave[i])
					{
						m_ScenePreviewSave[i] = new ScenePanel();
						m_ScenePreviewSave[i]->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(save.ChapterId, save.SceneId));
						if(i == m_CurrentSaveSlotSelected)
						{
							m_ScenePreviewSave[i]->LoadPanel();
							if(save.DialogId > 0)
								m_ScenePreviewSave[i]->SetCurrentDialog(save.DialogId-1);
						}
						m_ScenePreviewSave[i]->UsePremultipliedAlpha(true);
						m_ScenePreviewSave[i]->m_PlaySounds = false;
						m_ScenePreviewSave[i]->m_Loop1stDialog = true;
						if(i == m_CurrentSaveSlotSelected)
							m_ScenePreviewSave[i]->ForceLoadScene();
					}
				}
			}

			switch(m_CurrentSelection)
			{
			case SBT_CONTINUE:
				if(m_ActiveValidationArea.IsSelected)
				{
					// PLAY THE SCENE
					SaveData& save = profile->GetSave(PROFILE_AUTOSAVE_SLOT);
					if(save.Used)
					{
						GameDesc::m_CurrentChapter = save.ChapterId;
						GameDesc::m_CurrentScene = save.SceneId;
						GameDesc::m_CurrentDialog = save.DialogId;
						for(u32 i = 0; i < 3; ++i)
						{
							GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
						}
						GameDesc::m_RequestSceneStart = true;
					}
				}
				break;
			case SBT_NEWGAME:
				if(m_ActiveValidationArea.IsSelected)
				{
					// PLAY THE SCENE
					GameDesc::m_CurrentChapter = 0;
					GameDesc::m_CurrentScene = 0;
					GameDesc::m_CurrentDialog = 0;
					for(u32 i = 0; i < 3; ++i)
					{
						GameDesc::m_CurrentSounds[i] = AudioDataSerializable();
					}
					GameDesc::m_RequestSceneStart = true;
				}
				break;
			case SBT_LOAD:
				if(m_ActiveValidationArea.IsSelected)
				{
					// PLAY THE SCENE
					SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
					if(save.Used)
					{
						GameDesc::m_CurrentChapter = save.ChapterId;
						GameDesc::m_CurrentScene = save.SceneId;
						GameDesc::m_CurrentDialog = save.DialogId;
						for(u32 i = 0; i < 3; ++i)
						{
							GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
						}
						GameDesc::m_RequestSceneStart = true;
					}
				}
				break;
			case SBT_DELETESAVE:
				if(m_ActiveValidationArea.IsSelected)
				{
					SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
					if(save.Used && m_CurrentSaveSlotSelected != PROFILE_AUTOSAVE_SLOT)
					{
						ChangeSubMenuActive(SMAT_POPUP);
						if(m_PopupPanel)
						{
							m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_DELETESAVE_POPUP));
						}
					}
				}
				break;
			case SBT_CHAPTER:
#ifndef DEMO_VERSION
				if(m_ActiveValidationArea.IsSelected)
				{
					// PLAY THE SCENE
					GameDesc::m_CurrentChapter = m_CurrentChapterSelected;
					GameDesc::m_CurrentScene = m_CurrentSceneSelected;
					GameDesc::m_CurrentDialog = 0;
					GameDesc::m_RequestSceneStart = true;
				}

				{
					u32 nbChapters = GameDescManager::GetGameDesc()->ChaptersDesc.size();
#ifdef IRON_ICE_ENGINE
					nbChapters = (ProfileData::GetInstance()->m_PDS.m_ProgressionCH != 0xff)?
						ProfileData::GetInstance()->m_PDS.m_ProgressionCH + 1 : GameDescManager::GetGameDesc()->ChaptersDesc.size();
#endif

					if(!m_CurrentChapterSelectionFader.IsFading())
					{
						for(int i = -2; i <= 2; ++i)
						{
							if( i != 0 && m_ChapterSlotArea[i+2].IsSelected)
							{
								if(USE_CIRCULAR_MENU)
								{
									m_CurrentChapterSelected += i;
									m_CurrentChapterSelected += nbChapters;
									m_CurrentChapterSelected %= nbChapters;
								}
								else
								{
									if((s32)m_CurrentChapterSelected+i < 0 || (s32)m_CurrentChapterSelected+i >= (s32)nbChapters)
										continue;// empty slot : continue
									//m_CurrentChapterSelected += (i > 0)? 1 : -1;
									m_CurrentChapterSelected += i;
								}
								m_CurrentSceneSelected = 0;
								m_CurrentChapterSelectionFader = Fader(0.3f, Fader::FT_FADEIN, ((i > 0)? Fader::FS_FADE_RIGHT : Fader::FS_FADE_LEFT));
								m_CurrentChapterSelectionFader.StartFade();
							}
						}

						if( m_SceneSelectionArea[1].IsSelected )
						{
							ChapterDesc* chDesc = &GameDescManager::GetGameDesc()->ChaptersDesc[m_CurrentChapterSelected];
							u32 nbScenes = chDesc->ScenesDesc.size();
#ifdef IRON_ICE_ENGINE
							if(m_CurrentChapterSelected == nbChapters - 1)
								nbScenes = (ProfileData::GetInstance()->m_PDS.m_ProgressionSC != 0xff)?
								ProfileData::GetInstance()->m_PDS.m_ProgressionSC + 1 : chDesc->ScenesDesc.size();
#endif
							if(m_CurrentSceneSelected < nbScenes-1)
								++m_CurrentSceneSelected;
						}
						if( m_SceneSelectionArea[0].IsSelected )
						{
							if(m_CurrentSceneSelected > 0)
								--m_CurrentSceneSelected;
						}
					}
				}
#endif
				break;
			default:
				assert(0);
				break;
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		//if(m_SelectionFadeTimer == 0.0f)
		if(event.KeyInput.PressedDown)
		{
			u32 nbChapters = GameDescManager::GetGameDesc()->ChaptersDesc.size();
#ifdef IRON_ICE_ENGINE
			nbChapters = (ProfileData::GetInstance()->m_PDS.m_ProgressionCH != 0xff)?
				ProfileData::GetInstance()->m_PDS.m_ProgressionCH + 1 : GameDescManager::GetGameDesc()->ChaptersDesc.size();
#endif

			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					// reset center area selection
					{
						for(int i = 0; i < 5; ++i)
							m_ChapterSlotArea[i].IsSelected = false;
						for(int i = 0; i < 2; ++i)
							m_SceneSelectionArea[i].IsSelected = false;
						m_ActiveValidationArea.IsSelected = false;
					}

					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_BOTTOM);
				}
				return true;// MUST BYPASS THE MAINPANEL ESCAPE EVENT
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					ProfileData* profile = ProfileData::GetInstance();
					switch(m_CurrentSelection)
					{
					case SBT_CONTINUE:
						//if(m_ActiveValidationArea.IsSelected)
						{
							// PLAY THE SCENE
							SaveData& save = profile->GetSave(PROFILE_AUTOSAVE_SLOT);
							if(save.Used)
							{
								GameDesc::m_CurrentChapter = save.ChapterId;
								GameDesc::m_CurrentScene = save.SceneId;
								GameDesc::m_CurrentDialog = save.DialogId;
								for(u32 i = 0; i < 3; ++i)
								{
									GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
								}
								GameDesc::m_RequestSceneStart = true;
							}
						}
						break;
					case SBT_NEWGAME:
						//if(m_ActiveValidationArea.IsSelected)
						{
							// PLAY THE SCENE
							GameDesc::m_CurrentChapter = 0;
							GameDesc::m_CurrentScene = 0;
							GameDesc::m_CurrentDialog = 0;
							for(u32 i = 0; i < 3; ++i)
							{
								GameDesc::m_CurrentSounds[i] = AudioDataSerializable();
							}
							GameDesc::m_RequestSceneStart = true;
						}
						break;
					case SBT_LOAD:
						if(m_ActiveValidationArea.IsSelected)
						{
							// PLAY THE SCENE
							SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
							if(save.Used)
							{
								GameDesc::m_CurrentChapter = save.ChapterId;
								GameDesc::m_CurrentScene = save.SceneId;
								GameDesc::m_CurrentDialog = save.DialogId;
								for(u32 i = 0; i < 3; ++i)
								{
									GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
								}
								GameDesc::m_RequestSceneStart = true;
							}
						}
						break;
					case SBT_DELETESAVE:
						{
							ProfileData* profil = ProfileData::GetInstance();
							SaveData& save = profil->GetSave(m_CurrentSaveSlotSelected);
							if(save.Used && m_CurrentSaveSlotSelected != PROFILE_AUTOSAVE_SLOT)
							{
								ChangeSubMenuActive(SMAT_POPUP);
								if(m_PopupPanel)
								{
									m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_DELETESAVE_POPUP));
								}
							}
						}
						break;
					case SBT_CHAPTER:
#ifndef DEMO_VERSION
						{
							// PLAY THE SCENE
							GameDesc::m_CurrentChapter = m_CurrentChapterSelected;
							GameDesc::m_CurrentScene = m_CurrentSceneSelected;
							GameDesc::m_CurrentDialog = 0;
							for(u32 i = 0; i < 3; ++i)
							{
								GameDesc::m_CurrentSounds[i] = AudioDataSerializable();
							}
							GameDesc::m_RequestSceneStart = true;
						}
#endif
						break;
					default:
						assert(0);
						break;
					}
				}
				//m_TransitToChildPanelIndex = 0;
				//StartFade(PANEL_FADETIME);
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					--m_CurrentSelection;
					if(m_CurrentSelection >= SBT_MAX)
						m_CurrentSelection = 0;
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
				}
				else if(m_CurrentSubMenuActive == SMAT_BOTTOM)
				{
					ChangeSubMenuActive(SMAT_LEFT);
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_LOAD || m_CurrentSelection == SBT_DELETESAVE)
					{
						if(m_CurrentSaveSlotSelected >= 4)
							m_CurrentSaveSlotSelected -= 4;
					}
					else if(m_CurrentSelection == SBT_CHAPTER)
					{
#ifndef DEMO_VERSION
						if(!m_CurrentChapterSelectionFader.IsFading())
						{
							if(m_CurrentChapterSelected > 0 || USE_CIRCULAR_MENU)
							{
								if(USE_CIRCULAR_MENU)
								{
									m_CurrentChapterSelected += nbChapters;
									--m_CurrentChapterSelected;
									m_CurrentChapterSelected %= nbChapters;
								}
								else
									--m_CurrentChapterSelected;
								m_CurrentChapterSlotSelected = 0;
								m_CurrentSceneSelected = 0;
								m_CurrentChapterSelectionFader = Fader(0.3f, Fader::FT_FADEIN, Fader::FS_FADE_LEFT);
								m_CurrentChapterSelectionFader.StartFade();
							}
						}
#endif
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					for(int i = 0; i < SBT_MAX; ++i)
						m_ButtonsArea[i].IsSelected = false;
					++m_CurrentSelection;
#ifdef DEMO_VERSION
					if(m_CurrentSelection == SBT_CHAPTER)
						++m_CurrentSelection;
#endif
					if(m_CurrentSelection >= SBT_MAX)
					{
						m_CurrentSelection = SBT_MAX - 1;
#ifdef DEMO_VERSION
						if(m_CurrentSelection == SBT_CHAPTER)
							--m_CurrentSelection;
#endif
						ChangeSubMenuActive(SMAT_BOTTOM);
					}
					m_ButtonsArea[m_CurrentSelection].IsSelected = true;
				}
				else if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_LOAD || m_CurrentSelection == SBT_DELETESAVE)
					{
						if(m_CurrentSaveSlotSelected < PROFILE_SAVE_SLOTS-4)
							m_CurrentSaveSlotSelected += 4;
					}
					else if(m_CurrentSelection == SBT_CHAPTER)
					{
#ifndef DEMO_VERSION
						if(!m_CurrentChapterSelectionFader.IsFading())
						{
							if(m_CurrentChapterSelected < nbChapters-1 || USE_CIRCULAR_MENU)
							{
								if(USE_CIRCULAR_MENU)
								{
									++m_CurrentChapterSelected;
									m_CurrentChapterSelected %= nbChapters;
								}
								else
									++m_CurrentChapterSelected;
								m_CurrentChapterSlotSelected = 0;
								m_CurrentSceneSelected = 0;
								m_CurrentChapterSelectionFader = Fader(0.3f, Fader::FT_FADEIN, Fader::FS_FADE_RIGHT);
								m_CurrentChapterSelectionFader.StartFade();
							}
						}
#endif
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_LOAD || m_CurrentSelection == SBT_DELETESAVE)
					{
						if(m_CurrentSaveSlotSelected >= 1)
							m_CurrentSaveSlotSelected -= 1;
					}
					else if(m_CurrentSelection == SBT_CHAPTER)
					{
#ifndef DEMO_VERSION
						if(m_CurrentSceneSelected > 0)
							--m_CurrentSceneSelected;
#endif
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentSubMenuActive == SMAT_CENTER)
				{
					if(m_CurrentSelection == SBT_LOAD || m_CurrentSelection == SBT_DELETESAVE)
					{
						if(m_CurrentSaveSlotSelected < PROFILE_SAVE_SLOTS-1)
							m_CurrentSaveSlotSelected += 1;
					}
					else if(m_CurrentSelection == SBT_CHAPTER)
					{
						ChapterDesc* chDesc = &GameDescManager::GetGameDesc()->ChaptersDesc[m_CurrentChapterSelected];
						u32 nbScenes = chDesc->ScenesDesc.size();
#ifdef IRON_ICE_ENGINE
						if(m_CurrentChapterSelected == nbChapters - 1)
							nbScenes = (ProfileData::GetInstance()->m_PDS.m_ProgressionSC != 0xff)?
							ProfileData::GetInstance()->m_PDS.m_ProgressionSC + 1 : chDesc->ScenesDesc.size();
#endif
#ifndef DEMO_VERSION
						if(m_CurrentSceneSelected < nbScenes-1)
							++m_CurrentSceneSelected;
#endif
					}
				}
				else if(m_CurrentSubMenuActive == SMAT_LEFT)
				{
					ChangeSubMenuActive(SMAT_CENTER);
				}
			}
		}
	}

	return (m_CurrentSubMenuActive != SMAT_BOTTOM);// bypass the main panel events if we don't have the focus on the bottom element
}

#undef PANEL_FADETIME
