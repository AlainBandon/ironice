#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include "utils.h"
#include "sprite.h"

#include "panel.h"

class Game;

// Declare a structure to hold some context for the event receiver so that it
// has it available inside its OnEvent() method.
struct SAppContext
{
	IrrlichtDevice *device;
	s32				counter;
	Game*			game;

	gui::IGUIFileOpenDialog* openfiledlg;
};


/*
The Event Receiver is not only capable of getting keyboard and
mouse input events, but also events of the graphical user interface
(gui). There are events for almost everything: Button click,
Listbox selection change, events that say that a element was hovered
and so on. To be able to react to some of these events, we create
an event receiver.
We only react to gui events, and if it's such an event, we get the
id of the caller (the gui element which caused the event) and get
the pointer to the gui environment.
*/


class MyEventReceiver : public IEventReceiver
{
public:
	MyEventReceiver(const SAppContext& context) : Context(context), m_SaveFileNameDialog(NULL), m_SaveFileNameEditBox(NULL) { }

	virtual bool OnEvent(const SEvent& event);

private:
	SAppContext Context;

	gui::IGUIWindow* m_SaveFileNameDialog;
	gui::IGUIEditBox* m_SaveFileNameEditBox;
	io::path m_SaveName;
};

class Game
{
public:
	~Game();

	static Game* GetInstance();
	void DeleteInstance();

	void InitializeEventReceiver();

	void Update(float dt);
	void Render();

	void OnResize() {if(m_CurrentPanel) m_CurrentPanel->OnResize();}
	void OnDevicePreReset();
	void OnDevicePostReset();

	IPanel* GetCurrentPanel() const {return m_CurrentPanel;}
	void EnableMouse(bool state) {m_EnableMouse = state;}
	void DisableMouse(float duration) {m_TimerDisableMouse = duration;}
	bool IsMouseTemporarilyDisabled() const {return (m_TimerDisableMouse > 0.0f);}
	bool IsMouseDisabled() const {return (m_TimerDisableMouse > 0.0f) || !m_EnableMouse;}

protected:
	Game();
	void BuildPanels();

	static Game* ms_Instance;

	MyEventReceiver* m_EventReceiver;

	TArray<IPanel*> m_Panels;// list of all panels (must be unique)
	IPanel* m_CurrentPanel;

	float m_Accumulator;
	float m_TimerDisableMouse;
	bool m_EnableMouse;
	Position2 m_LastDisableMousePosition;
};



#endif //#ifdef GAMEVIEW_H
