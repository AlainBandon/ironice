#ifndef SPRITE_H
#define SPRITE_H

#include "utils.h"
#ifdef IRON_ICE_EDITOR
#include "md5.h"
#endif


/*
An AnimatedSprite is an array of FameSprite composed themselves of an array of LayerSprite.
Basically the AnimatedSprite object has a framerate defining the frequency of frame change. 
Each frame owns an array of layers representing an image. Each image (layer) of this frame
is supposed to be displayed at the same time in the order defined in the array.
*/

// Contains an image and with position, rotation and scale (through source size and dest size), 
// all relatively to the matrix transform of the sprite
class LayerSprite
{
public:
	LayerSprite();
	LayerSprite(LayerSprite* layer);
	~LayerSprite();

	const Position2& GetDestPosition() const {return m_DestPosition;}
	void SetDestPosition(const Position2& position) {m_DestPosition = position;}

	const Position2& GetSrcPosition() const {return m_SrcPosition;}
	void SetSrcPosition(const Position2& position) {m_SrcPosition = position;}

	const Dimension2& GetDestSize() const {return m_DestSize;}
	void SetDestSize(const Dimension2& size) {m_DestSize = size;}

	const Dimension2& GetSrcSize() const {return m_SrcSize;}
	void SetSrcSize(const Dimension2& size) {m_SrcSize = size;}

	float GetRotation() const {return m_Rotation;}
	void SetRotation(float rotation) {m_Rotation = rotation;}

	Texture* GetImage() const {return m_Image;}
	void SetImage(Texture* image);

	// deprecated rendering function 
	//void Render(const Position2& pos, float scale = 1.0f, const IColor& tint = COLOR_WHITE);

	// rendering function of the layer
	// param 1 : matrix transform of the sprite (defines the local base for the layer transform calculation)
	// param 2 : tint of the layer
	// param 3 : clipping rect (NULL if no clipping)
	void Render(const core::matrix4& matrix, const IColor& tint = COLOR_WHITE, bool mirrorH = false, bool mirrorV = false, core::rect<irr::s32>* clip = NULL);

	// deprecated debug rendering function
	//void RenderDebugInfo(const Position2& pos, float scale = 1.0f);

	// debug rendering function (display the bounding box)
	// param 1 : matrix transform of the sprite
	void RenderDebugInfo(const core::matrix4& matrix, bool mirrorH = false, bool mirrorV = false);

protected:
	Dimension2 m_SrcSize;// Dimension of the area taken from the texture
	Position2 m_SrcPosition;// Offset of the area taken from the texture
	Dimension2 m_DestSize;// Dimension of the area displayed on the screen (relative the transform of the sprite)
	Position2 m_DestPosition;// Position of the area displayed on the screen (relative the transform of the sprite)
	float m_Rotation;// Rotation of the texture (relative the transform of the sprite)
	Texture* m_Image;// Texture used by the layer (CAUTION : the layer doesn't own the texture (a single texture can be used by multiple layers))
};

// Contains an array of layers that are supposed to be all displayed when this frame is the current frame to display.
// The layers are displayed in the order of the array.
class FrameSprite
{
public:
	FrameSprite();
	FrameSprite(FrameSprite* frame);
	~FrameSprite();

	// add a new layer to the frame
	// param 1 : the layer to add
	bool AddLayer(LayerSprite* layer);// add a layer to the frame

	// get the array of the layers
	TArray<LayerSprite*>& GetLayers() {return m_Layers;}

	const Dimension2& GetSize() const {return m_Size;}
	void SetSize(const Dimension2& size) {m_Size = size;}

	// deprecated rendering function for the frame
	//void Render(const Position2& pos, float scale = 1.0f, const IColor& tint = COLOR_WHITE);

	// rendering function of the frame : display all the layer one by one in the array order
	// param 1 : matrix transform of the sprite
	// param 2 : tint of the layer
	// param 3 : clipping rect (NULL if no clipping)
	void Render(const core::matrix4& matrix, const IColor& tint = COLOR_WHITE, bool mirrorH = false, bool mirrorV = false, core::rect<irr::s32>* clip = NULL);

	// deprecated debug rendering function
	//void RenderDebugInfo(const Position2& pos, float scale = 1.0f, bool recursive = false);

	// debug rendering function : display the bounding box of the frame
	// param 1 : matrix transform of the sprite
	// param 2 : do we recursively display the bounding boxes of the layers
	void RenderDebugInfo(const core::matrix4& matrix, bool mirrorH = false, bool mirrorV = false, bool recursive = false);

	// function to check each layer owns an image
	bool HasAnyLayerImage() const 
	{
		for(u32 i=0; i<m_Layers.size(); ++i)
			if(m_Layers[i]->GetImage())
				return true;
		return false;
	}

protected:
	Dimension2 m_Size;// Dimension of the bounding box of the frame (not used for now)

	TArray<LayerSprite*> m_Layers;// Array containing all the layers of this frame

};

// Contains an array of frames displayed at the frequency defined in the framerate member.
// All texture used by the layers are owned by an array of texture in the sprite : the raw image bank.
class AnimatedSprite
{
public:
	AnimatedSprite();
	~AnimatedSprite();

	bool LoadSpriteFromFile(char* path);// load sprite from a .spr file
#ifndef IRON_ICE_FINAL
	bool SaveSpriteToFile(char* path);// save sprite to a .spr file
	bool SaveSpriteToFileXML(const char* path);// save sprite to a .xml file with png images
#endif

#ifdef IRON_ICE_EDITOR
	MD5 GetMD5();// Get the checksum md5 of the sprite (for editor only)
	bool Optimize();// Optmize the sprite by making unicity of textures inside the bank (TODO : more optimisations)
#endif

	bool AddImageToData(Texture* tex);// add an image to the image bank
	bool AddFrame(FrameSprite* frame);// add a new frame

	bool HasAnyLayerImage() const // Check every layer has an image
	{
		for(u32 i=0; i<m_Frames.size(); ++i)
			for(u32 j=0; j<m_Frames[i]->GetLayers().size(); ++j)
				if(m_Frames[i]->GetLayers()[j]->GetImage())
					return true;
		return false;
	}


	TArray<FrameSprite*>& GetFrames() {return m_Frames;}// Get the frames
	TArray<Texture*>& GetDataImages() {return m_RawDataImages;}// Get the image bank

	const Dimension2& GetSize() const {return m_Size;}
	void SetSize(const Dimension2& size) {m_Size = size;}

	float GetFramerate() const {return m_Framerate;}
	void SetFramerate(float framerate) {m_Framerate = framerate;}

	// Get the corresponding frame at a given time
	// param 1 : time in seconds
	// param 2 : do we loop the frames ?
	u32 GetFrameIndexFromTime(float time, bool loop = false) 
	{
		u32 frameNb = loop?(u32)(time/m_Framerate)%(u32)core::max_((s32)m_Frames.size(), 0):(u32)(time/m_Framerate);
		return core::clamp(frameNb, 0U, (u32)core::max_((s32)m_Frames.size()-1, 0));
	}

	// rendering function of the sprite : display all the layers of a given frame one by one in the array order
	// param 2 : the index of the frame wanted in the array
	// param 2 : matrix transform of the sprite
	// param 3 : tint of the layer
	// param 4 : clipping rect (NULL if no clipping)
	void RenderFrame(u32 frameIndex, const core::matrix4& matrix, const IColor& tint = COLOR_WHITE, bool mirrorH = false, bool mirrorV = false, core::rect<irr::s32>* clip = NULL);
	
	// debug rendering function : display the bounding box of the sprite
	// param 1 : index of the frame wanted in the array
	// param 2 : matrix transform of the sprite
	// param 3 : do we recursively display the bounding boxes of the frame and its layers
	void RenderDebugInfo(u32 frameIndex, const core::matrix4& matrix, bool mirrorH = false, bool mirrorV = false, bool recursive = false);
	
	// deprecated debug rendering function
	//void RenderDebugInfo(u32 frameIndex, const Position2& pos, float scale = 1.0f, bool recursive = false);

	// Get the sprite preview by rendering the 1st frame on a render target texture
	video::ITexture* RenderPreview();

protected:
	Dimension2 m_Size;
	TArray<FrameSprite*> m_Frames;// All the frames of the sprite

	float m_Framerate;// Framerate which is actually a delta time in seconds between each frames

	TArray<Texture*> m_RawDataImages;// The raw image bank : contains and owns the textures used by the layers
};


#endif 
