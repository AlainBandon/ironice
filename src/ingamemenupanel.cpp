﻿#include "panel.h"
#include "engine.h"
#include "utils.h"


#define PANEL_FADETIME 0.5f
 
enum StringDesc {SD_DISPLAY = Engine::SFTID_OptionsSubMenuPanel, SD_AUDIO, SD_KEYS, SD_LANGUAGE, SD_DRIVER, SD_RESOLUTION, SD_FULLSCREEN, SD_VSYNC, SD_GAMMARAMP,
	SD_SAVE_POPUP, SD_NO, SD_YES, SD_D3D, SD_OPENGL, SD_SOFTWARE, SD_VOL_GENERAL, SD_VOL_BG1, SD_VOL_BG2, SD_VOL_VOICE,
	SD_KEY_LEFT, SD_KEY_RIGHT, SD_KEY_UP, SD_KEY_DOWN,
	SD_KEY_VALID, SD_KEY_CANCEL, SD_KEY_QTE1, SD_KEY_QTE2, SD_KEY_QTE3, SD_KEY_QTE4, SD_KEY_PAUSE, SD_KEY_QUICK_MENU, 
	
	SD_RESUME = Engine::SFTID_InGameMenuPanel, SD_SAVE, SD_LOAD, SD_OPTIONS, SD_EXIT, SD_EMPTY, SD_SKIP, SD_QUICKSAVE, SD_QUICKLOAD, SD_AUTOSAVE,
	SD_MAX};

// InGameMenuPanel
InGameMenuPanel::InGameMenuPanel() : IPanel()
, m_MenuTex(NULL)
, m_QuickMenuTex(NULL)
, m_Selec(NULL)
, m_SaveSlotTex(NULL)
, m_CurrentMBSelection(0)
, m_CurrentMBSelectionValidated(false)
, m_CurrentSBSelection(0)
, m_CurrentSBSelectionValidated(false)
, m_CurrentLine(0)
, m_CurrentSaveSlotSelected(0)
, m_CurrentMenuActive(MAT_RIGHT)
, m_PreviousMenuActive(MAT_RIGHT)
, m_MATChangeFader(0.2f)
, m_MenuSelectionFader(0.3f)
, m_PopupPanel(NULL)
, m_IsProcessingKey(false)
, m_RequestQuit(false)
, m_RequestSkip(false)
, m_UseQuickMenu(false)
{
	for(int i = 0; i < 4; ++i)
		m_Font[i] = NULL;

	m_RenderTarget = NULL;
}

InGameMenuPanel::~InGameMenuPanel()
{
	UnloadPanel();
}

void InGameMenuPanel::LoadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	if(!m_RenderTarget)// ARGH... this must be done here cause we could have lose it during a device reset and do the load only now
	{
		bool useRT = true;
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
		if(useRT && driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
		{
			m_RenderTarget = driver->addRenderTargetTexture(Engine::GetInstance()->m_RenderSize, "RTT_InGameMenu", video::ECF_A8R8G8B8);
		}
		else
		{
			m_RenderTarget = NULL;
		}
	}

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	SAFE_LOAD_IMAGE(driver, m_MenuTex, engine->GetGamePath("system/03_Pause.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	SAFE_LOAD_IMAGE(driver, m_QuickMenuTex, engine->GetGamePath("system/04_Select_under.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_Selec, engine->GetGamePath("system/00_selection.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	SAFE_LOAD_IMAGE(driver, m_SaveSlotTex, engine->GetGamePath("system/0_Save_under.png"), (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

	if(!m_Font[0])
	{
		CGUITTFace* face = new CGUITTFace();
		face->load(engine->GetGamePath("system/MenuFont.ttf"));

		const u32 sizes[4] = {28, 32, 48, 60};
		for(int i = 0; i < 4; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			CGUIFreetypeFont *font = new CGUIFreetypeFont(driver);
			font->attach(face, size, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA), size/6);
			font->AntiAlias = true;
			font->Transparency = true;

			m_Font[i] = font;
		}
		face->drop();// now we attached it we can drop the reference
	}

	if(!m_PopupPanel)
	{
		m_PopupPanel = new PopupPanel(m_RenderTarget || USE_PREMULTIPLIED_ALPHA);
	}
	if(m_PopupPanel && !m_PopupPanel->IsLoaded())
	{
		m_PopupPanel->LoadPanel();
	}

	OnResize();

	//StartFade(PANEL_FADETIME);
	m_CurrentMenuActive = MAT_RIGHT;
	m_RequestQuit = false;
	m_RequestSkip = false;
	m_UseQuickMenu = false;

	IPanel::LoadPanel();
}

void InGameMenuPanel::UnloadPanel()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	m_RenderTarget = NULL;

	SAFE_UNLOAD(m_MenuTex);
	SAFE_UNLOAD(m_QuickMenuTex);

	SAFE_UNLOAD(m_Selec);

	SAFE_UNLOAD(m_SaveSlotTex);

	for(int i = 0; i < 4; ++i)
		SAFE_UNLOAD(m_Font[i]);

	if(m_PopupPanel)
		m_PopupPanel->UnloadPanel();

	IPanel::UnloadPanel();
}

void InGameMenuPanel::OnResize()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;
	const float fontScale = engine->m_CoreData.SubMenuFontScale;

	const u32 sizes[4] = {28, 32, 48, 60};
	if(m_Font[0])
	{
		for(int i = 0; i < 4; ++i)
		{
			u32 size = (u32)((f32)sizes[i] * innerScale * fontScale);
			if(i == 1)
				m_Font[i]->setSize(size, size/5);
			else
				m_Font[i]->setSize(size, size/6);
		}
	}

	{
		Dimension2 size = Dimension2(1161, 834);
		size.Width += 100 + 100;
		size.Height += 60;
		Position2 pos = Position2((s32)((f32)0*innerScale), (s32)((f32)0*innerScale)) + innerOffset;
		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)size.Height*innerScale));
		m_SubMenusArea[MAT_CENTER].Area = Rectangle2(pos, pos+cornerOffset);
		m_SubMenusArea[MAT_CENTER].IsSelected = false;

		pos.Y += cornerOffset.Y;
		m_SubMenusArea[MAT_BOTTOM].Area = Rectangle2(pos, Position2(pos.X+cornerOffset.X, innerOffset.Y+renderSize.Height));
		m_SubMenusArea[MAT_BOTTOM].IsSelected = false;

		pos.Y -= cornerOffset.Y;
		pos.X = cornerOffset.X;
		m_SubMenusArea[MAT_RIGHT].Area = Rectangle2(pos, Position2(innerOffset.X+renderSize.Width, innerOffset.Y+renderSize.Height));
		m_SubMenusArea[MAT_RIGHT].IsSelected = false;

		m_SubMenusArea[m_CurrentMenuActive].IsSelected = true;
	}

	{

		//const Dimension2& size = Dimension2(1161, 834);// dimension of the center part
		Dimension2 size(1161, sizes[3]);
		Position2 pos = Position2((s32)((f32)100*innerScale), (s32)((f32)(834+60)*innerScale)) + innerOffset;

		s32 dx = (u32)(((f32)1161/SBT_MAX)*innerScale);
		Position2 offset(0, 0);

		Position2 cornerOffset((s32)dx, (s32)((f32)size.Height*innerScale));
		for(int i = 0; i < SBT_MAX; ++i, offset.X += dx)
		{
			if(m_Font[3])
			{
				cornerOffset.X = m_Font[3]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_DISPLAY+i)).Width;
			}
			Position2 midOffset((dx-cornerOffset.X)>>1, 0);
			m_ButtonsArea[i].Area = Rectangle2(pos + midOffset + offset, pos + midOffset + offset + cornerOffset);
			m_ButtonsArea[i].IsSelected = false;
		}

		m_ButtonsArea[m_CurrentSBSelection].IsSelected = true;
	}

	{

		Dimension2 size(originalSize.Width>>2, sizes[3]);
		Position2 pos = Position2((s32)((f32)(1352 + 60)*innerScale), (s32)((f32)(133 + 60)*innerScale)) + innerOffset;

		u32 dy = (u32)(((originalSize.Height>>1)/(f32)MBT_MAX)*innerScale);
		Position2 offset(0, 0);

		Position2 cornerOffset((s32)((f32)size.Width*innerScale), (s32)((f32)dy*innerScale));
		for(int i = 0; i < MBT_MAX; ++i, offset.Y += dy)
		{
			if(m_Font[3])
			{
				cornerOffset.X = m_Font[3]->getDimension(Engine::GetLanguageString(engine->GetLanguage(), SD_RESUME+i)).Width;
			}
			m_ButtonsArea2[i].Area = Rectangle2(pos + offset, pos + offset + cornerOffset);
			m_ButtonsArea2[i].IsSelected = false;
		}

		m_ButtonsArea2[m_CurrentMBSelection].IsSelected = true;
	}

	if(m_RenderTarget && renderSize != m_RenderTarget->getSize())
		m_RenderTarget = driver->addRenderTargetTexture(renderSize, "RTT_InGameMenu", video::ECF_A8R8G8B8);

	if(m_PopupPanel)
	{
		m_PopupPanel->OnResize();
	}
}

void InGameMenuPanel::OnDevicePreReset()
{

}

void InGameMenuPanel::OnDevicePostReset()
{
	
}

void InGameMenuPanel::Update(float dt)
{
	IPanel::Update(dt);

	if(m_UseQuickMenu)
		UpdateQuickMenu(dt);
	else
		UpdateMenu(dt);
}

void InGameMenuPanel::UpdateMenu(float dt)
{
	m_MATChangeFader.Tick(dt);
	m_MenuSelectionFader.Tick(dt);

	if(m_CurrentMenuActive == MAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Update(dt);
		}
	}
}

void InGameMenuPanel::UpdateQuickMenu(float dt)
{
	
}

void InGameMenuPanel::RenderFader(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

// 	IColor tint = IsFading()?
// 		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, IColor(/*PANEL_MENU_FADER_ALPHA*/255, 255, 255, 255), 4.0f*m_Fader.GetRatio()-3.0f)
// 		: IColor(/*PANEL_MENU_FADER_ALPHA*/255, 255, 255, 255);
// 
// 	// draw the menu tex at the center
// 	if(m_MenuTex)
// 	{
// 		const Dimension2& size = m_MenuTex->getSize();
// 		Rectangle2 sourceRect(Position2(0,0), size);
// 		Position2 pos = Position2((s32)((f32)(size.Width>>1)*innerScale),
// 			(s32)((f32)(size.Height>>1)*innerScale));
// 
// 		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X*scale+(f32)offset.X*innerScale + innerOffset.X, (f32)pos.Y*scale+(f32)offset.Y*innerScale + innerOffset.Y, 0.0f)) *
// 			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
// 			core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));
// 
// 		draw2DImage(driver, m_MenuTex, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 	}
}

void InGameMenuPanel::RenderSave(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentMenuActive == MAT_CENTER);

	if(m_SaveSlotTex)
	{
		IColor tint = isCenterActive? COLOR_WHITE : IColor(PANEL_MENU_NOT_ACTIVE_TRANSPARENCY,255,255,255);

		const Dimension2& size = Dimension2(1161, 834);
		Rectangle2 sourceRect(Position2(0,0), m_SaveSlotTex->getSize());

		const u32 margin = 30;
		const u32 slotWidth = (size.Width - (margin<<1))/4;
		const u32 slotHeight = (size.Height - (margin<<1))/3;

		ProfileData* profil = ProfileData::GetInstance();

		for(u32 i = 0; i < 3; ++i)
		{
			for(u32 j = 0; j < 4; ++j)
			{
				u32 slotIndex = 4*i + j;
				SaveData& save = profil->GetSave(slotIndex);

				u32 grey = (slotIndex == m_CurrentSaveSlotSelected)? 255:50;
				u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);

				Position2 rawPos(100+margin + j*slotWidth+(slotWidth>>1), 
					60+margin + i*slotHeight+(slotHeight>>1));

				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df(((f32)rawPos.X*scale+offset.X)*innerScale + innerOffset.X, 
					((f32)rawPos.Y*scale+offset.Y)*innerScale + innerOffset.Y, 0.0f)) *
					core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
					core::matrix4().setScale(core::vector3df((f32)innerScale*scale, (f32)innerScale*scale, 0.0f));

				draw2DImage(driver, m_SaveSlotTex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

				if(save.Used)
				{
					// draw the preview
					if(m_SaveSlotTex)
					{
						const Dimension2& size = m_SaveSlotTex->getSize();
						float scale2 = (float)size.Width*scale*0.9f/originalSize.Width;
						Position2 pos((s32)((((f32)rawPos.X - 0.45f*size.Width)*scale + offset.X)*innerScale), (s32)((((f32)rawPos.Y - 0.45f*size.Height)*scale + offset.Y)*innerScale));
// 						if(isCenterActive && m_ScenePreviewSave[slotIndex] && slotIndex == m_CurrentSaveSlotSelected)// dynamic preview of the scene if available and slot selected
// 						{
// 							if(m_Selec && isCenterActive)
// 							{
// 								float scale3 = scale2*innerScale;
// 								Position2 pos3 = pos + Position2((s32)(scale3*originalSize.Width*0.5f), (s32)(scale3*originalSize.Height*0.5f)) + innerOffset;
// 
// 								float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
// 								IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR(255), IColor(100, 255, 255, 255), ratio);
// 								Rectangle2 sourceRect(Position2(0,0), m_Selec->getSize());
// 								float scaleX = (float)(s32)(scale3*originalSize.Width)/(m_Selec->getSize().Width - (18<<1));
// 								float scaleY = (float)(s32)(scale3*originalSize.Height)/(m_Selec->getSize().Height - (18<<1));
// 
// 								core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos3.X,	(f32)pos3.Y, 0.0f)) *
// 									core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
// 									core::matrix4().setScale(core::vector3df((f32)scaleX, (f32)scaleY, 0.0f));
// 
// 								draw2DImage(driver, m_Selec, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
// 							}
// 
// 							m_ScenePreviewSave[slotIndex]->SetDimension(pos, scale2);
// 							m_ScenePreviewSave[slotIndex]->Render();
// 						}
// 						else// else static preview of the chapter
						{
							Texture* tex = GameDescManager::GetGameDesc()->ChaptersDesc[save.ChapterId].PreviewTex;
							Rectangle2 sourceRect(Position2(0,0), tex->getSize());
							scale2 = (float)size.Width*scale*innerScale*0.9f/tex->getSize().Width;
							pos += Position2((s32)(scale2*tex->getSize().Width*0.5f), (s32)(scale2*tex->getSize().Height*0.5f)) + innerOffset;

							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X,	(f32)pos.Y, 0.0f)) *
								core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df((f32)scale2, (f32)scale2, 0.0f));

							draw2DImage(driver, tex, sourceRect, mat, true, colorTex, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
						}
					}
					if(m_Font[0])
					{
						float ratio = fabs(cosf(0.5f*core::PI*m_Time));
						u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 200:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
						IColor color = (slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_SELECTION(alpha) : COMPUTE_THEME_COLOR(alpha);
						IColor tint = MathUtils::GetInterpolatedColor((slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_GLOW(isCenterActive?alpha:alpha>>1) : COMPUTE_THEME_COLOR(isCenterActive?alpha:alpha>>1), COMPUTE_THEME_COLOR(isCenterActive?alpha>>1:alpha>>2), 1.0f-ratio);
						const core::stringw& str = save.GetStringCompact();
						if(slotIndex == m_CurrentSaveSlotSelected)
							m_Font[0]->drawGauss(str, Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), tint, scale, true, false);
						m_Font[0]->draw(str, Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), color, scale, true, false);
					}
				}
				else
				{
					float ratio = fabs(cosf(0.5f*core::PI*m_Time));
					u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 200:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
					IColor color = (slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_SELECTION(alpha) : COMPUTE_THEME_COLOR(alpha);
					IColor tint = MathUtils::GetInterpolatedColor((slotIndex == m_CurrentSaveSlotSelected)? COMPUTE_THEME_COLOR_FONT_GLOW(isCenterActive?alpha:alpha>>1) : COMPUTE_THEME_COLOR(isCenterActive?alpha:alpha>>1), COMPUTE_THEME_COLOR(isCenterActive?alpha>>1:alpha>>2), 1.0f-ratio);
					if(m_Font[0])
					{
						if(slotIndex == m_CurrentSaveSlotSelected)
							m_Font[0]->drawGauss(Engine::GetLanguageString(engine->GetLanguage(), SD_EMPTY), Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), tint, scale, true, false);
						m_Font[0]->draw(Engine::GetLanguageString(engine->GetLanguage(), SD_EMPTY), Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y+30*innerScale*scale)), D2Zero), color, scale, true, false);
					}
				}

				if(slotIndex == PROFILE_AUTOSAVE_SLOT || slotIndex == PROFILE_QUICKSAVE_SLOT)// auto + quick saves slot strings
				{
					const wchar_t* const s[2] = {Engine::GetLanguageString(engine->GetLanguage(), SD_AUTOSAVE), 
						Engine::GetLanguageString(engine->GetLanguage(), SD_QUICKSAVE)};
					float ratio = fabs(cosf(0.5f*core::PI*m_Time));
					u32 alpha = isCenterActive? ((slotIndex == m_CurrentSaveSlotSelected)? 200:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
					IColor color = COMPUTE_THEME_COLOR(alpha);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(isCenterActive?alpha:alpha>>1), COMPUTE_THEME_COLOR(isCenterActive?alpha>>1:alpha>>2), 1.0f-ratio);
					if(m_Font[0])
					{
						m_Font[0]->drawGauss(s[slotIndex], Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y - 20*innerScale*scale)), D2Zero), tint, scale, true, false);
						m_Font[0]->draw(s[slotIndex], Rectangle2(Position2((u32)mat.getTranslation().X, (u32)(mat.getTranslation().Y - 20*innerScale*scale)), D2Zero), color, scale, true, false);
					}
				}

				if(slotIndex == m_CurrentSaveSlotSelected)
				{
					Position2 pos((s32)(((f32)rawPos.X*scale+offset.X)*innerScale + innerOffset.X), (s32)(((f32)rawPos.Y*scale+offset.Y)*innerScale + innerOffset.Y));
					Position2 halfCorner((s32)(innerScale*scale*m_SaveSlotTex->getSize().Width*0.5f), (s32)(innerScale*scale*m_SaveSlotTex->getSize().Height*0.5f));
					m_ActiveValidationArea.Area = Rectangle2(pos-halfCorner, pos+halfCorner);
				}
			}
		}
	}
}

void InGameMenuPanel::RenderDisplay(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentMenuActive == MAT_CENTER);

	{
		ProfileData* profil = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = Dimension2(1161, 834);

		if(m_Font[2] && m_Font[3])
		{
			Position2 pos = Position2((s32)(((f32)(100)*scale + offset.X)*innerScale),
				(s32)(((f32)(60)*scale + offset.Y)*innerScale));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			ProfileData* profile = ProfileData::GetInstance();

			u32 dy = (u32)(scale * m_Font[3]->getSize()) + (u32)((float)30 * innerScale * scale);
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			core::stringw str1[4] =
			{
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_DRIVER)),
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_RESOLUTION)),
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_FULLSCREEN)),
				StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_VSYNC))
			};

			core::stringw str2[4] =
			{
				StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), SD_D3D+profile->m_ODS.m_DriverType)),
				StringFormat(L"%d x %d", profile->m_ODS.m_WindowSize.Width, profile->m_ODS.m_WindowSize.Height),
				StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), SD_NO+profile->m_ODS.m_FullScreen)),
				StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), SD_NO+profile->m_ODS.m_VSync))
			};

			for(u32 i = 0; i < 4; ++i)
			{
				bool isSelected = (m_CurrentLine==i);

				u32 alpha = (m_CurrentMenuActive == MAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor color = COMPUTE_THEME_COLOR(alpha);

				float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);

				if(isSelected)
				{
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str1[i], Rectangle2(pos, D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[3]:m_Font[2])->draw(str1[i], Rectangle2(pos, D2Zero), color, scale, false, false);

				if(isSelected)
				{
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str2[i], Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[3]:m_Font[2])->draw(str2[i], Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), color, scale, false, false);

				m_LinesArea[i].Area = Rectangle2(pos, Dimension2(dx, dy));
				pos.Y += dy;
			}

			// the gamma ramp
			{
				bool isSelected = (m_CurrentLine==4);

				u32 alpha = (m_CurrentMenuActive == MAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				core::stringw str = StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_GAMMARAMP));

				if(isSelected)
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					//float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str, Rectangle2(pos, D2Zero), tint, scale, false, false);
				}

				(isSelected?m_Font[3]:m_Font[2])->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);

				m_Slider.SetValue(0.01f*profile->m_ODS.m_GammaRamp);
				m_Slider.Render(Rectangle2(Position2(pos.X + (dx>>1), pos.Y + (dy>>2)), Dimension2(dx>>1, dy>>1)), colorTex);

				m_LinesArea[4].Area = Rectangle2(Position2(pos.X, pos.Y + (dy>>2)), Dimension2(dx, dy>>1));
				pos.Y += dy;
			}

			for(u32 i = 5; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}

void InGameMenuPanel::RenderAudio(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentMenuActive == MAT_CENTER);

	{
		ProfileData* profile = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = Dimension2(1161, 834);

		if(m_Font[2] && m_Font[3])
		{
			Position2 pos = Position2((s32)(((f32)(100)*scale + offset.X)*innerScale),
				(s32)(((f32)(60)*scale + offset.Y)*innerScale));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			ProfileData* profil = ProfileData::GetInstance();

			u32 dy = (u32)(scale * m_Font[3]->getSize()) + (u32)((float)30 * innerScale * scale);
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			core::stringw str[4] =
			{
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_GENERAL)),
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_BG1)),
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_BG2)),
				StringFormat(L"%ls :", Engine::GetLanguageString(engine->GetLanguage(), SD_VOL_VOICE))
			};

			float values[4] =
			{
				0.01f*profile->m_ODS.m_VolumeGeneral,
				0.01f*profile->m_ODS.m_VolumeBG1,
				0.01f*profile->m_ODS.m_VolumeBG2,
				0.01f*profile->m_ODS.m_VolumeVoice
			};

			for(u32 i = 0; i < 4; ++i)
			{
				bool isSelected = (m_CurrentLine==i);

				u32 alpha = (m_CurrentMenuActive == MAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				if(isSelected)
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					//float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str[i], Rectangle2(pos, D2Zero), tint, scale, false, false);
				}

				(isSelected?m_Font[3]:m_Font[2])->draw(str[i], Rectangle2(pos, D2Zero), color, scale, false, false);

				m_Slider.SetValue(values[i]);
				m_Slider.Render(Rectangle2(Position2(pos.X + (dx>>1), pos.Y + (dy>>2)), Dimension2(dx>>1, dy>>1)), colorTex);

				m_LinesArea[i].Area = Rectangle2(Position2(pos.X, pos.Y + (dy>>2)), Dimension2(dx, dy>>1));
				pos.Y += dy;
			}

			for(u32 i = 4; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}

void InGameMenuPanel::RenderKeys(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentMenuActive == MAT_CENTER);

	{
		ProfileData* profile = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = Dimension2(1161, 834);

		if(m_Font[2] && m_Font[3])
		{
			Position2 pos = Position2((s32)(((f32)(100)*scale + offset.X)*innerScale),
				(s32)(((f32)(60)*scale + offset.Y)*innerScale));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			u32 dy = (u32)(scale * m_Font[3]->getSize())/* + (u32)((float)10 * innerScale * scale)*/;
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			ProfileData::KeyMappingSerializable& keymap = profile->m_KMS;
			for(u32 i = 0; i < 12; ++i)
			{
				bool isSelected = (m_CurrentLine==i);

				u32 alpha = (m_CurrentMenuActive == MAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor colorTex = IColor(alpha, 255, 255, 255);
				IColor color = COMPUTE_THEME_COLOR(alpha);

				IColor tint(COMPUTE_THEME_COLOR(255));
				if(isSelected)
				{
					float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
					tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
				}

				core::stringw str = Engine::GetLanguageString(engine->GetLanguage(), SD_KEY_LEFT+i);
				if(isSelected)
				{
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str, Rectangle2(pos, D2Zero), tint, scale, false, false);
				}

				(isSelected?m_Font[3]:m_Font[2])->draw(str, Rectangle2(pos, D2Zero), color, scale, false, false);

				if(!m_IsProcessingKey || m_CurrentLine!=i)
				{
					EKEY_CODE keycode = keymap.GetKeyCode1((ProfileData::KeyMappingSerializable::IIKeyType)i);
					core::stringw strCode = GetStringFromKeyCode(keycode);

					if(strCode != L"")
					{
						Engine::KeyElementType type = (strCode.size() > 1)? Engine::KET_EMPTY_KEY2 : Engine::KET_EMPTY_KEY1;
						// special keys
						if(keycode == KEY_LBUTTON)
							type = Engine::KET_LMOUSE;
						else if(keycode == KEY_RBUTTON)
							type = Engine::KET_RMOUSE;
						else if(keycode == KEY_LEFT)
							type = Engine::KET_LEFT_KEY;
						else if(keycode == KEY_RIGHT)
							type = Engine::KET_RIGHT_KEY;
						else if(keycode == KEY_UP)
							type = Engine::KET_UP_KEY;
						else if(keycode == KEY_DOWN)
							type = Engine::KET_DOWN_KEY;

						Position2 pos2(pos.X + (dx>>2) + (dx>>1), pos.Y + (dy>>1));

						if(engine->GetKeyElement(type))
						{
							Dimension2 size = engine->GetKeyElement(type)->getSize();
							Rectangle2 sourceRect(Position2(0,0), size);

							float menuScale = (float)dy/size.Height;
							if(type == Engine::KET_EMPTY_KEY2)
								menuScale *= 2.0f;
							else if(type == Engine::KET_RMOUSE || type == Engine::KET_LMOUSE)
								menuScale *= 1.0f;
							else
								menuScale *= 1.5f;

							if(isSelected)
								menuScale *= 1.2f;

							core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)(pos2.X), (f32)(pos2.Y), 0.0f)) *
								core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
								core::matrix4().setScale(core::vector3df(menuScale, menuScale, 0.0f));

							draw2DImage(driver, engine->GetKeyElement(type), sourceRect, mat, true, colorTex, true, true);
						}

						if(type == Engine::KET_EMPTY_KEY2 || type == Engine::KET_EMPTY_KEY1)
						{
							if(isSelected)
							{
								(isSelected?m_Font[3]:m_Font[2])->drawGauss(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), tint, scale, true, true);
							}
							(isSelected?m_Font[3]:m_Font[2])->draw(strCode, Rectangle2(Position2(pos2.X, pos2.Y), D2Zero), colorTex, scale, true, true);
						}
					}
				}

				m_LinesArea[i].Area = Rectangle2(pos, Dimension2(dx, dy));
				pos.Y += dy;
			}
		}
	}
}

void InGameMenuPanel::RenderLanguage(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	bool isCenterActive = (m_CurrentMenuActive == MAT_CENTER);

	{
		ProfileData* profil = ProfileData::GetInstance();

		const u32 margin = 60;

		const Dimension2& size = Dimension2(1161, 834);

		if(m_Font[2] && m_Font[3])
		{
			Position2 pos = Position2((s32)(((f32)(100)*scale + offset.X)*innerScale),
				(s32)(((f32)(60)*scale + offset.Y)*innerScale));
			pos += innerOffset;

			s32 marginOffset = (s32)((float)margin * innerScale * scale);
			pos += Position2(marginOffset, marginOffset);

			ProfileData* profile = ProfileData::GetInstance();

			u32 dy = (u32)(scale * m_Font[3]->getSize()) + (u32)((float)10 * innerScale * scale);
			u32 dx = (u32)(scale * innerScale * (size.Width - (margin<<1)));

			core::stringw str1 = StringFormat(L"%ls : ", Engine::GetLanguageString(engine->GetLanguage(), SD_LANGUAGE));
			core::stringw str2 = StringFormat(L"%ls", Engine::GetLanguageString(engine->GetLanguage(), Engine::SFTID_LanguageMenuPanel));

			{
				bool isSelected = (m_CurrentLine==0);

				u32 alpha = (m_CurrentMenuActive == MAT_CENTER)? (isSelected?255:200) : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor color = COMPUTE_THEME_COLOR(alpha);

				float ratio = core::abs_(cosf(0.75f*core::PI*m_Time));
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);

				if(isSelected)
				{
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str1, Rectangle2(pos, D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[3]:m_Font[2])->draw(str1, Rectangle2(pos, D2Zero), color, scale, false, false);

				if(isSelected)
				{
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str2, Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), tint, scale, false, false);
				}
				(isSelected?m_Font[3]:m_Font[2])->draw(str2, Rectangle2(Position2(pos.X + (dx>>1), pos.Y), D2Zero), color, scale, false, false);

				m_LinesArea[0].Area = Rectangle2(pos, Dimension2(dx, dy));
				pos.Y += dy;
			}

			for(u32 i = 1; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}

void InGameMenuPanel::_RenderMenu(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	RenderFader(scale, offset);

	// draw the bottom submenu
	if(m_CurrentMBSelection == MBT_OPTIONS)
	{
		if(m_Font[2] && m_Font[3])
		{
			//s32 x = (u32)((originalSize.Width/(f32)SBT_MAX)*innerScale);

			for(int i = 0; i < SBT_MAX; ++i)
			{
				core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_DISPLAY+i));
				bool isSelected = m_ButtonsArea[i].IsSelected;
				u32 alpha = (m_CurrentMenuActive == MAT_BOTTOM)? 255 : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
				IColor color = isSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : COMPUTE_THEME_COLOR((m_CurrentMenuActive == MAT_BOTTOM)? alpha : 16);
				const Rectangle2& area = m_ButtonsArea[i].Area;
				Rectangle2 rect(Position2((s32)((float)(area.UpperLeftCorner.X-innerOffset.X)*scale+(float)offset.X*innerScale+innerOffset.X),
					(s32)((float)(area.UpperLeftCorner.Y-innerOffset.Y)*scale+(float)offset.Y*innerScale+innerOffset.Y)),
					Dimension2((u32)((float)area.getWidth()*scale), (u32)((float)area.getHeight()*scale)));
				//if(isSelected)
				{
					float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
					IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
					(isSelected?m_Font[3]:m_Font[2])->drawGauss(str, rect, tint, scale, true, true);
				}
				(isSelected?m_Font[3]:m_Font[2])->draw(str, rect, color, scale, true, true);
				//fill2DRect(driver, rect, color, color, color, color);
			}
		}
	}

	// draw the right menu
// 	if(m_Font[2] && m_Font[3])
// 	{
// 		//s32 x = (u32)((originalSize.Width/(f32)MBT_MAX)*innerScale);
// 
// 		for(int i = 0; i < MBT_MAX; ++i)
// 		{
// 			core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_RESUME+i));
// 			bool isSelected = m_ButtonsArea2[i].IsSelected;
// 			u32 alpha = (m_CurrentMenuActive == MAT_RIGHT)? 255 : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
// 			IColor color = isSelected? COMPUTE_THEME_COLOR(255) : IColor((m_CurrentMenuActive == MAT_RIGHT)? alpha : 16, 255, 255, 255);
// 			const Rectangle2& area = m_ButtonsArea2[i].Area;
// 			Rectangle2 rect(Position2((s32)((float)(area.UpperLeftCorner.X-innerOffset.X)*scale+(float)offset.X*innerScale+innerOffset.X),
// 				(s32)((float)(area.UpperLeftCorner.Y-innerOffset.Y)*scale+(float)offset.Y*innerScale+innerOffset.Y)),
// 				Dimension2((u32)((float)area.getWidth()*scale), (u32)((float)area.getHeight()*scale)));
// 			//if(isSelected)
// 			{
// 				float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i+core::PI)-3.0f, 0.0f);
// 				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_2(alpha), IColor(alpha>>1, 255, 255, 255), 1.0f-ratio);
// 				(isSelected?m_Font[3]:m_Font[2])->drawGauss(str, rect, tint, scale, false, false);
// 			}
// 			(isSelected?m_Font[3]:m_Font[2])->draw(str, rect, color, scale, false, false);
// 		}
// 	}

	// center part
	bool isCenterActive = (m_CurrentMenuActive == MAT_CENTER);

	if(m_CurrentMBSelection == MBT_OPTIONS)
	{
		switch(m_CurrentSBSelection)
		{
		case SBT_DISPLAY:
			RenderDisplay(scale, offset);
			break;
		case SBT_AUDIO:
			RenderAudio(scale, offset);
			break;
		case SBT_KEYS:
			RenderKeys(scale, offset);
			break;
		case SBT_LANGUAGE:
			RenderLanguage(scale, offset);
			break;
		default:
			assert(0);
			break;
		}
	}
	else if(m_CurrentMBSelection == MBT_SAVE || m_CurrentMBSelection == MBT_LOAD)
	{
		RenderSave(scale, offset);
	}

	if(m_CurrentMenuActive == MAT_POPUP)
	{
		if(m_PopupPanel)
		{
			m_PopupPanel->Render();
		}
	}
}

void InGameMenuPanel::_RenderQuickMenu(float scale/* = 1.0f*/, const Position2& offset/* = P2Zero*/)
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	RenderFader(scale, offset);

	IColor tint = IsFading()?
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, 4.0f*m_Fader.GetRatio()-3.0f)
		: COLOR_WHITE;

	if(m_QuickMenuTex)
	{
		u32 dy = 780;
		const Dimension2& size = m_QuickMenuTex->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(size.Width>>1)*innerScale),
			(s32)((f32)dy*innerScale));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X + innerOffset.X, (f32)pos.Y + innerOffset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_QuickMenuTex, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));

		pos.X = 0;
		pos.Y -= (s32)((f32)(size.Height>>1)*innerScale);
		if(m_Font[1])
		{
			u32 dx = (u32)(((f32)size.Width/5)*innerScale);
			pos.X += dx>>1;
			Dimension2 dim(dx, (u32)((f32)size.Height*innerScale));
			u32 indices[4] = {SD_RESUME, SD_SKIP, SD_QUICKSAVE, SD_QUICKLOAD};
			for(u32 i = 0; i < 4; ++i)
			{
				core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), indices[i]));
				bool isSelected = (m_CurrentLine==i);
				u32 alpha = 255;
				IColor color = COMPUTE_THEME_COLOR(isSelected?alpha:(alpha>>1));
				Rectangle2 rect(pos+innerOffset, dim);
				m_Font[1]->drawGauss(str, rect, color, scale, true, true);
				if(isSelected)
					m_Font[1]->draw(str, rect, color, scale, true, true);

				m_LinesArea[i].Area = rect;
				//fill2DRect(driver, rect, color, color, color, color);
				pos.X += dx;
			}
			for(u32 i = 4; i < 12; ++i)
				m_LinesArea[i].Area = Rectangle2(0, 0, 0, 0);
		}
	}
}


void InGameMenuPanel::PreRender()
{
	// render here only if we have a render target : we render in the render target
	if(m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		// set render target texture
		IColor color = COLOR_TRANSPARENT;
		color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(m_RenderTarget, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		if(m_UseQuickMenu)
			_RenderQuickMenu();
		else
			_RenderMenu();

		// set back old render target
		// The buffer might have been distorted, so clear it
		color = COLOR_TRANSPARENT;
		if(USE_PREMULTIPLIED_ALPHA)
			color.color = PremultiplyAlpha(color.color);
		driver->setRenderTarget(0, true, true, color);
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}

void InGameMenuPanel::Render()
{
	if(m_UseQuickMenu)
		RenderQuickMenu();
	else
		RenderMenu();

}

void InGameMenuPanel::RenderMenu()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor tint = IsFading()?
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, 4.0f*m_Fader.GetRatio()-3.0f)
		: COLOR_WHITE;

	// draw the menu tex at the center
	if(m_MenuTex)
	{
		const Dimension2& size = m_MenuTex->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos = Position2((s32)((f32)(size.Width>>1)*innerScale),
			(s32)((f32)(size.Height>>1)*innerScale));

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X + innerOffset.X, (f32)pos.Y + innerOffset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df((f32)innerScale, (f32)innerScale, 0.0f));

		draw2DImage(driver, m_MenuTex, sourceRect, mat, true, tint, true, (m_RenderTarget || USE_PREMULTIPLIED_ALPHA));
	}

	// draw the right menu
	if(m_Font[2] && m_Font[3])
	{
		//s32 x = (u32)((originalSize.Width/(f32)MBT_MAX)*innerScale);

		for(int i = 0; i < MBT_MAX; ++i)
		{
			core::stringw str(Engine::GetLanguageString(engine->GetLanguage(), SD_RESUME+i));
			bool isSelected = m_ButtonsArea2[i].IsSelected;
			u32 alpha = (m_CurrentMenuActive == MAT_RIGHT)? 255 : PANEL_MENU_NOT_ACTIVE_TRANSPARENCY;
			IColor color = isSelected? COMPUTE_THEME_COLOR_FONT_SELECTION(255) : COMPUTE_THEME_COLOR((m_CurrentMenuActive == MAT_RIGHT)? alpha : 16);
			const Rectangle2& area = m_ButtonsArea2[i].Area;
			Rectangle2 rect(Position2((s32)((float)(area.UpperLeftCorner.X-innerOffset.X)+innerOffset.X),
				(s32)((float)(area.UpperLeftCorner.Y-innerOffset.Y)+innerOffset.Y)),
				Dimension2((u32)((float)area.getWidth()), (u32)((float)area.getHeight())));
			//if(isSelected)
			{
				float ratio = isSelected? core::abs_(cosf(0.75f*core::PI*m_Time)) : core::max_(4.0f*cosf(0.6f*core::PI*m_Time - 0.3f*i)-3.0f, 0.0f);
				IColor tint = MathUtils::GetInterpolatedColor(COMPUTE_THEME_COLOR_FONT_GLOW(alpha), COMPUTE_THEME_COLOR(alpha>>1), 1.0f-ratio);
				(isSelected?m_Font[3]:m_Font[2])->drawGauss(str, rect, tint, 1.0f, false, false);
			}
			(isSelected?m_Font[3]:m_Font[2])->draw(str, rect, color, 1.0f, false, false);
		}
	}

	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)),
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_RenderMenu(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();

		// Dirty Hardcoding Party !!!! YEAHHHHHHHH !
		float magicRatio = 0.75f;//(float)502/1920;
		float dx = 0.0f;//innerScale*(1351-(1920>>1)+(502>>1));
		float dy = 0.0f;//innerScale*(1025-(1080>>1));
		//dy -= 0.5f*magicRatio*534;

		float d = (m_CurrentMenuActive == MAT_RIGHT)? (1.0f-m_MATChangeFader.GetRealRatio()) : 
			(m_PreviousMenuActive == MAT_RIGHT)? m_MATChangeFader.GetRealRatio() : 0.0f;
		Position2 offset((s32)(d*dx), (s32)(d*dy));

		IColor tint = MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, m_MenuSelectionFader.GetRatio());

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((renderSize.Width>>1), (renderSize.Height>>1));

		if(m_MATChangeFader.IsFading())
		{
			if(m_CurrentMenuActive == MAT_RIGHT)// other to right fade
				ratio = magicRatio * ratio + m_MATChangeFader.GetRealRatio()*(1.0f-magicRatio);
			else if(m_PreviousMenuActive == MAT_RIGHT)// right to other fade
				ratio = magicRatio * ratio + (1.0f - m_MATChangeFader.GetRealRatio())*(1.0f-magicRatio);
		}
		else
		{
			if(m_CurrentMenuActive == MAT_RIGHT)// right
				ratio = magicRatio*ratio;
		}

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

void InGameMenuPanel::RenderQuickMenu()
{
	video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

	Engine* engine = Engine::GetInstance();
	const Dimension2& renderSize = engine->m_RenderSize;
	const Dimension2& originalSize = engine->m_OriginalSize;
	float innerScale = engine->m_InnerScale;
	const Position2& innerOffset = engine->m_InnerOffset;

	IColor tint = IsFading()?
		MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, 4.0f*m_Fader.GetRatio()-3.0f)
		: COLOR_WHITE;

	
	if(!m_RenderTarget) // render here only if we don't have a render target : render on the screen directly
	{
		float ratio = m_Fader.GetRatio();
		float side = (m_Fader.FadeSide == Fader::FS_FADE_RIGHT)? 1.0f : -1.0f;
		Position2 offset((s32)((side+1.0f)*(1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Width>>1)),
			(s32)((1.0f-ratio)*(float)(Engine::GetInstance()->m_OriginalSize.Height>>1)));
		_RenderQuickMenu(ratio, offset);
	}
	else // else we render the render target result with some effects
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();

		float ratio = m_Fader.GetRatio();

		// Dirty Hardcoding Party !!!! YEAHHHHHHHH !
		float magicRatio = 1.0f;
		float dx = 0.0f;
		float dy = 0.0f;

		Position2 offset((s32)(dx), (s32)(dy));

		IColor tint = MathUtils::GetInterpolatedColor(COLOR_TRANSPARENT, COLOR_WHITE, m_MenuSelectionFader.GetRatio());

		Dimension2 size = m_RenderTarget->getSize();
		Rectangle2 sourceRect(Position2(0,0), size);
		Position2 pos((renderSize.Width>>1), (renderSize.Height>>1));

		ratio = magicRatio*ratio;

		core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X+offset.X, (f32)pos.Y+offset.Y, 0.0f)) *
			core::matrix4().setRotationAxisRadians(0*core::DEGTORAD, core::vector3df(0.0f, 0.0f, 1.0f)) *
			core::matrix4().setScale(core::vector3df(ratio, ratio, 0.0f));

		draw2DImage(driver, m_RenderTarget, sourceRect, mat, true, tint, true, true);
	}
}

bool InGameMenuPanel::OnEvent(const SEvent& event)
{
	if(IPanel::OnEvent(event))
		return true;

	if(IsFading())
		return true;

	if(m_UseQuickMenu)
		return OnEventQuickMenu(event);
	else
		return OnEventMenu(event);

	return false;
}

bool InGameMenuPanel::OnEventMenu(const SEvent& event)
{
	bool keepMenuOpen = true;

	ProfileData* profile = ProfileData::GetInstance();

	if(m_PopupPanel)
	{
		if(m_PopupPanel->OnEvent(event))
		{
			if(!m_PopupPanel->IsVisible())// this means that the popup just closed
			{
				{
					if(m_PopupPanel->GetResult() == PopupPanel::PRT_YES)
					{
						// save the changes
						profile->WriteSave();

						// apply the changes for the ones that need to reset the device
						if(profile->m_ODS.NeedsResetDevice(m_ODS))
							Engine::GetInstance()->RequestResetDevice();
					}
					profile->ReadSave();
					//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
				}
				ChangeMenuActive(m_PreviousMenuActive);
			}

			return true;
		}
	}
	if(m_IsProcessingKey)
	{
		ProcessKey(event);
		return true;
	}
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Engine* engine = Engine::GetInstance();
			const Dimension2& renderSize = engine->m_RenderSize;
			const Dimension2& originalSize = engine->m_OriginalSize;
			float innerScale = engine->m_InnerScale;
			const Position2& innerOffset = engine->m_InnerOffset;

			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);

			if(m_CurrentMBSelection != MBT_RESUME && m_CurrentMBSelection != MBT_EXIT)
			{
				// check which sub menu is active
				if(!m_MATChangeFader.IsFading())
				{
					u32 oldSubMenuActive = m_CurrentMenuActive;
					for(int i = 0; i < MAT_MAX; ++i)
					{
						m_SubMenusArea[i].IsSelected = false;
						if(m_SubMenusArea[i].Area.isPointInside(mousePos))
						{
							m_CurrentMenuActive = i;
						}
					}
					m_SubMenusArea[m_CurrentMenuActive].IsSelected = true;
					if(oldSubMenuActive != m_CurrentMenuActive)
					{
						m_PreviousMenuActive = oldSubMenuActive;
						m_MATChangeFader.StartFade();
					}
				}

				if(m_CurrentMBSelection == MBT_OPTIONS)
				{
					// bottom menu
					{
						for(int i = 0; i < SBT_MAX; ++i)
						{
							m_ButtonsArea[i].IsSelected = false;
							if(m_ButtonsArea[i].Area.isPointInside(mousePos))
							{
								if(m_CurrentSBSelection != i)
								{
									m_CurrentSBSelection = i;

									// reset center area selection
									for(u32 i = 0; i < 12; ++i)
										m_LinesArea[i].IsSelected = false;
									m_CurrentLine = 0;
								}
							}
						}
						m_ButtonsArea[m_CurrentSBSelection].IsSelected = true;
					}
				}
				else
				{
					m_ActiveValidationArea.IsSelected = m_ActiveValidationArea.Area.isPointInside(mousePos);

					const Dimension2& size = Dimension2(1161, 834);

					u32 margin = 30;
					u32 slotWidth = (size.Width - (margin<<1))/4;
					u32 slotHeight = (size.Height - (margin<<1))/3;

					u32 oldSaveSlotSelection = m_CurrentSaveSlotSelected;
					{
						bool slotHovered = false;
						for(u32 i = 0; i < 3 && !slotHovered; ++i)
						{
							for(u32 j = 0; j < 4 && !slotHovered; ++j)
							{
								u32 slotIndex = 4*i + j;

								Position2 rawPos(100+margin + j*slotWidth+(slotWidth>>1), 
									60+margin + i*slotHeight+(slotHeight>>1));
								Position2 pos((s32)(((f32)rawPos.X - (slotWidth>>1))*innerScale), 
									(s32)(((f32)rawPos.Y - (slotHeight>>1))*innerScale));

								Rectangle2 rect(pos+innerOffset, Dimension2((u32)((f32)slotWidth*innerScale), (u32)((f32)slotHeight*innerScale)));
								if(rect.isPointInside(mousePos))
								{
									m_CurrentSaveSlotSelected = slotIndex;
									slotHovered = true;
								}
							}
						}
					}
				}
			}

			// right menu
			{
				for(int i = 0; i < MBT_MAX; ++i)
				{
					m_ButtonsArea2[i].IsSelected = false;
					if(m_ButtonsArea2[i].Area.isPointInside(mousePos))
					{
						if(m_CurrentMBSelection != i)
						{
							m_MenuSelectionFader.StartFade();
							m_CurrentMBSelection = i;
						}
					}
				}
				m_ButtonsArea2[m_CurrentMBSelection].IsSelected = true;
			}

			// center part
			if(m_CurrentMBSelection == MBT_OPTIONS)
			{
				if( !((m_CurrentSBSelection == SBT_AUDIO || (m_CurrentSBSelection == SBT_DISPLAY && m_CurrentLine == 4)) &&
					event.MouseInput.isLeftPressed()) )// do not change the selection in the audio menu if left mouse is pressed
				{
					for(int i = 0; i < 12; ++i)
					{
						m_LinesArea[i].IsSelected = false;
						if(m_LinesArea[i].Area.isPointInside(mousePos))
						{
							m_CurrentLine = i;
							m_LinesArea[m_CurrentLine].IsSelected = true;
						}
					}
				}

				if(m_CurrentSBSelection == SBT_AUDIO)
				{
					if(event.MouseInput.isLeftPressed())
					{
						float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
							(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
						if(value >= 0.0f && value <= 1.0f)
						{
							u8* valuesPtr[4] =
							{
								&profile->m_ODS.m_VolumeGeneral,
								&profile->m_ODS.m_VolumeBG1,
								&profile->m_ODS.m_VolumeBG2,
								&profile->m_ODS.m_VolumeVoice
							};
							*(valuesPtr[m_CurrentLine]) = (u8)(value*100);
						}
					}
				}
				else if(m_CurrentSBSelection == SBT_DISPLAY && m_CurrentLine == 4)
				{
					if(event.MouseInput.isLeftPressed())
					{
						float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
							(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
						if(value >= 0.0f && value <= 1.0f)
						{
							profile->m_ODS.m_GammaRamp = (u8)(value*100);
							//engine->SetGamma(profile->m_ODS.m_GammaRamp);
						}
					}
				}
			}

		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_CurrentMenuActive == MAT_RIGHT)
			{
				m_ODS = profile->m_ODS;
				m_KMS = profile->m_KMS;
				profile->ReadSave();
				if(m_ODS != profile->m_ODS || m_KMS != profile->m_KMS)// if sth changed : ask for apply them
				{
					ChangeMenuActive(MAT_POPUP);
					if(m_PopupPanel)
					{
						m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_SAVE_POPUP));
					}

					{
						ProfileData::OptionDataSerializable temp = profile->m_ODS;
						profile->m_ODS = m_ODS;// reapply the changes done for the display
						m_ODS = temp;// we store the old profile options
					}

					{
						ProfileData::KeyMappingSerializable temp = profile->m_KMS;
						profile->m_KMS = m_KMS;// reapply the changes done for the keys
						m_KMS = temp;// we store the old profile options
					}

					return true;
				}
				else if(m_CurrentMBSelection == MBT_RESUME)// nothing change in the options and requesting resume : let's resume the game
				{
					keepMenuOpen = false;
				}
				else if(m_CurrentMBSelection == MBT_EXIT)
				{
					m_RequestQuit = true;
					keepMenuOpen = false;
				}
			}
			else if(m_CurrentMenuActive == MAT_CENTER)
			{
				if(m_CurrentMBSelection == MBT_OPTIONS)
				{
					if(m_LinesArea[m_CurrentLine].IsSelected)
					{
						switch(m_CurrentSBSelection)
						{
						case SBT_DISPLAY:
							{
								switch(m_CurrentLine)
								{
								case 0:// driver
									{
										u32 value = (u32)profile->m_ODS.m_DriverType;
										++value %= ProfileData::OptionDataSerializable::DT_MAX;
										profile->m_ODS.m_DriverType = (ProfileData::OptionDataSerializable::DriverType)value;
									}
									break;
								case 1:// resolution
									profile->m_ODS.m_WindowSize = Engine::GetInstance()->GetNextResolution(profile->m_ODS.m_WindowSize);
									break;
								case 2:// fullscreen
									profile->m_ODS.m_FullScreen = !profile->m_ODS.m_FullScreen;
									break;
								case 3:// v-sync
									profile->m_ODS.m_VSync = !profile->m_ODS.m_VSync;
									break;
								case 4:// gamma ramp
									{
										Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
										float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
											(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
										if(value >= 0.0f && value <= 1.0f)
										{
											profile->m_ODS.m_GammaRamp = (u8)(value*100);
											//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
										}
									}
									break;
								default:
									assert(0);
									break;
								}
							}
							break;
						case SBT_AUDIO:
							{
								Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);
								if(m_CurrentLine <= 4)
								{
									float value = (float)(mousePos.X - m_LinesArea[m_CurrentLine].Area.getCenter().X)/
										(float)(m_LinesArea[m_CurrentLine].Area.LowerRightCorner.X - m_LinesArea[m_CurrentLine].Area.getCenter().X);
									if(value >= 0.0f && value <= 1.0f)
									{
										u8* valuesPtr[4] =
										{
											&profile->m_ODS.m_VolumeGeneral,
											&profile->m_ODS.m_VolumeBG1,
											&profile->m_ODS.m_VolumeBG2,
											&profile->m_ODS.m_VolumeVoice
										};
										*(valuesPtr[m_CurrentLine]) = (u8)(value*100);
									}
								}
							}
							break;
						case SBT_KEYS:
							{
								m_IsProcessingKey = true;
							}
							break;
						case SBT_LANGUAGE:
							{
								s32 ls = (s32)Engine::GetInstance()->GetLanguage();
								s32 oldLs = ls;
								do
								{
									++ls;
									if(ls >= Engine::LS_MAX)
										ls = 0;
								}
								while(!Engine::GetInstance()->m_CoreData.LangsAllowed[ls] && ls != oldLs);
								Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)ls);
								OnResize();// because of the left menu entries dimensions
							}
							break;
						default:
							assert(0);
							break;
						}
					}
				}
				else if(m_CurrentMBSelection == MBT_SAVE || m_CurrentMBSelection == MBT_LOAD)
				{
// 					for(int i = 0; i < PROFILE_SAVE_SLOTS ; ++i)
// 					{
// 						SaveData& save = profile->GetSave(i);
// 						if(save.Used)
// 						{
// 							if(!m_ScenePreviewSave[i])
// 							{
// 								m_ScenePreviewSave[i] = new ScenePanel();
// 								m_ScenePreviewSave[i]->SetScenePath(GameDescManager::GetGameDesc()->GetScenePath(save.ChapterId, save.SceneId));
// 								m_ScenePreviewSave[i]->LoadPanel();
// 								if(save.DialogId > 0)
// 									m_ScenePreviewSave[i]->SetCurrentDialog(save.DialogId-1);
// 								m_ScenePreviewSave[i]->UsePremultipliedAlpha(true);
// 								m_ScenePreviewSave[i]->m_PlaySounds = false;
// 								m_ScenePreviewSave[i]->m_Loop1stDialog = true;
// 							}
// 						}
// 					}

					if(m_CurrentMBSelection == MBT_LOAD)
					{
						if(m_ActiveValidationArea.IsSelected)
						{
							// PLAY THE SCENE
							SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
							if(save.Used)
							{
								GameDesc::m_CurrentChapter = save.ChapterId;
								GameDesc::m_CurrentScene = save.SceneId;
								GameDesc::m_CurrentDialog = save.DialogId;
								for(u32 i = 0; i < 3; ++i)
								{
									GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
								}
								GameDesc::m_RequestSceneStart = true;
							}
						}
					}
					else // SAVE
					{
						if(m_ActiveValidationArea.IsSelected)
						{
							SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
							//if(!save.Used)
							{
								save.Save(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene, GameDesc::m_CurrentDialog, GameDesc::m_CurrentSounds);
								profile->WriteSave();
							}
						}
					}
				}
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		//if(m_SelectionFadeTimer == 0.0f)
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				if(m_CurrentMenuActive == MAT_CENTER)
				{
					// reset center area selection
					for(u32 i = 0; i < 12; ++i)
						m_LinesArea[i].IsSelected = false;

					if(m_CurrentMBSelection == MBT_OPTIONS)
						ChangeMenuActive(MAT_BOTTOM);
					else
						ChangeMenuActive(MAT_RIGHT);
				}
				else
				{
					// Check for change and ask for save in case of need
					m_ODS = profile->m_ODS;
					m_KMS = profile->m_KMS;
					profile->ReadSave();
					if(m_ODS != profile->m_ODS || m_KMS != profile->m_KMS)// if sth changed : ask for apply them
					{
						ChangeMenuActive(MAT_POPUP);
						if(m_PopupPanel)
						{
							m_PopupPanel->Display(PopupPanel::PT_YES_NO, Engine::GetLanguageString(Engine::GetInstance()->GetLanguage(), SD_SAVE_POPUP));
						}

						{
							ProfileData::OptionDataSerializable temp = profile->m_ODS;
							profile->m_ODS = m_ODS;// reapply the changes done for the display
							m_ODS = temp;// we store the old profile options
						}

						{
							ProfileData::KeyMappingSerializable temp = profile->m_KMS;
							profile->m_KMS = m_KMS;// reapply the changes done for the keys
							m_KMS = temp;// we store the old profile options
						}

						return true;
					}

					if(m_CurrentMenuActive == MAT_RIGHT)
						keepMenuOpen = false;
					else
						ChangeMenuActive(MAT_RIGHT);
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				if(m_CurrentMenuActive == MAT_RIGHT)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
						ChangeMenuActive(MAT_BOTTOM);
					else if(m_CurrentMBSelection == MBT_RESUME)
						keepMenuOpen = false;
					else if(m_CurrentMBSelection == MBT_EXIT)
					{
						m_RequestQuit = true;
						keepMenuOpen = false;
					}
					else// save + load
						ChangeMenuActive(MAT_CENTER);
				}
				else if(m_CurrentMenuActive == MAT_BOTTOM)
				{
					ChangeMenuActive(MAT_CENTER);
				}
				else if(m_CurrentMenuActive == MAT_CENTER)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						if(m_CurrentSBSelection == SBT_KEYS)
						{
							m_IsProcessingKey = true;
						}
					}
					else if(m_CurrentMBSelection == MBT_LOAD)
					{
						if(m_ActiveValidationArea.IsSelected)
						{
							// PLAY THE SCENE
							SaveData& save = profile->GetSave(m_CurrentSaveSlotSelected);
							if(save.Used)
							{
								GameDesc::m_CurrentChapter = save.ChapterId;
								GameDesc::m_CurrentScene = save.SceneId;
								GameDesc::m_CurrentDialog = save.DialogId;
								for(u32 i = 0; i < 3; ++i)
								{
									GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
								}
								GameDesc::m_RequestSceneStart = true;
							}
						}
					}
				}
				//m_TransitToChildPanelIndex = 0;
				//StartFade(PANEL_FADETIME);
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_UP))
			{
				if(m_CurrentMenuActive == MAT_RIGHT)
				{
					for(int i = 0; i < MBT_MAX; ++i)
						m_ButtonsArea2[i].IsSelected = false;
					--m_CurrentMBSelection;
					if(m_CurrentMBSelection >= MBT_MAX)
						m_CurrentMBSelection = 0;
					else
						m_MenuSelectionFader.StartFade();
					m_ButtonsArea2[m_CurrentMBSelection].IsSelected = true;
				}
				else if(m_CurrentMenuActive == MAT_BOTTOM)
				{
					ChangeMenuActive(MAT_CENTER);
				}
				else if(m_CurrentMenuActive == MAT_CENTER)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						switch(m_CurrentSBSelection)
						{
						case SBT_DISPLAY:
						case SBT_AUDIO:
						case SBT_KEYS:
						case SBT_LANGUAGE:
							if(m_CurrentLine > 0)
								--m_CurrentLine;
							break;
						default:
							assert(0);
							break;
						}
					}
					else if(m_CurrentMBSelection == MBT_LOAD || m_CurrentMBSelection == MBT_SAVE)
					{
						if(m_CurrentSaveSlotSelected >= 4)
							m_CurrentSaveSlotSelected -= 4;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
			{
				if(m_CurrentMenuActive == MAT_RIGHT)
				{
					for(int i = 0; i < MBT_MAX; ++i)
						m_ButtonsArea2[i].IsSelected = false;
					++m_CurrentMBSelection;
					if(m_CurrentMBSelection >= MBT_MAX)
						m_CurrentMBSelection = MBT_MAX - 1;
					else
						m_MenuSelectionFader.StartFade();
					m_ButtonsArea2[m_CurrentMBSelection].IsSelected = true;
				}
				else if(m_CurrentMenuActive == MAT_CENTER)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						switch(m_CurrentSBSelection)
						{
						case SBT_DISPLAY:
							if(m_CurrentLine < 4)
								++m_CurrentLine;
							break;
						case SBT_AUDIO:
							if(m_CurrentLine < 3)
								++m_CurrentLine;
							break;
						case SBT_KEYS:
							if(m_CurrentLine < 11)
								++m_CurrentLine;
							break;
						case SBT_LANGUAGE:
							if(m_CurrentLine < 0)
								++m_CurrentLine;
							break;
						default:
							assert(0);
							break;
						}
					}
					else if(m_CurrentMBSelection == MBT_LOAD || m_CurrentMBSelection == MBT_SAVE)
					{
						if(m_CurrentSaveSlotSelected < PROFILE_SAVE_SLOTS-4)
							m_CurrentSaveSlotSelected += 4;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentMenuActive == MAT_BOTTOM)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						for(int i = 0; i < SBT_MAX; ++i)
							m_ButtonsArea[i].IsSelected = false;
						--m_CurrentSBSelection;
						if(m_CurrentSBSelection >= SBT_MAX)
							m_CurrentSBSelection = 0;
						m_ButtonsArea[m_CurrentSBSelection].IsSelected = true;
						// reset center area selection
						for(u32 i = 0; i < 12; ++i)
							m_LinesArea[i].IsSelected = false;
						m_CurrentLine = 0;
					}
				}
				else if(m_CurrentMenuActive == MAT_RIGHT)
				{
					ChangeMenuActive((m_CurrentMBSelection == MBT_OPTIONS)? MAT_BOTTOM : MAT_CENTER);
				}
				else if(m_CurrentMenuActive == MAT_CENTER)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						switch(m_CurrentSBSelection)
						{
						case SBT_DISPLAY:
							{
								switch(m_CurrentLine)
								{
								case 0:// driver
									{
										u32 value = (u32)profile->m_ODS.m_DriverType + ProfileData::OptionDataSerializable::DT_MAX;
										--value %= ProfileData::OptionDataSerializable::DT_MAX;
										profile->m_ODS.m_DriverType = (ProfileData::OptionDataSerializable::DriverType)value;
									}
									break;
								case 1:// resolution
									profile->m_ODS.m_WindowSize = Engine::GetInstance()->GetPreviousResolution(profile->m_ODS.m_WindowSize);
									break;
								case 2:// fullscreen
									profile->m_ODS.m_FullScreen = !profile->m_ODS.m_FullScreen;
									break;
								case 3:// v-sync
									profile->m_ODS.m_VSync = !profile->m_ODS.m_VSync;
									break;
								case 4:// gamma ramp
									if(profile->m_ODS.m_GammaRamp > 0)
									{
										profile->m_ODS.m_GammaRamp -= 1;
										//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
									}
									break;
								default:
									assert(0);
									break;
								}
							}
							break;
						case SBT_AUDIO:
							{
								if(m_CurrentLine <= 4)
								{
									u8* valuesPtr[4] =
									{
										&profile->m_ODS.m_VolumeGeneral,
										&profile->m_ODS.m_VolumeBG1,
										&profile->m_ODS.m_VolumeBG2,
										&profile->m_ODS.m_VolumeVoice
									};
									if(*(valuesPtr[m_CurrentLine]) > 0)
										*(valuesPtr[m_CurrentLine]) -= 1;
								}
							}
							break;
						case SBT_KEYS:
							{

							}
							break;
						case SBT_LANGUAGE:
							{
								s32 ls = (s32)Engine::GetInstance()->GetLanguage();
								s32 oldLs = ls;
								do
								{
									--ls;
									if(ls < 0)
										ls = Engine::LS_MAX - 1;
								}
								while(!Engine::GetInstance()->m_CoreData.LangsAllowed[ls] && ls != oldLs);
								Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)ls);
								OnResize();// because of the left menu entries dimensions
							}
							break;
						default:
							assert(0);
							break;
						}
					}
					else if(m_CurrentMBSelection == MBT_LOAD || m_CurrentMBSelection == MBT_SAVE)
					{
						if(m_CurrentSaveSlotSelected >= 1)
							m_CurrentSaveSlotSelected -= 1;
					}
				}
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentMenuActive == MAT_BOTTOM)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						for(int i = 0; i < SBT_MAX; ++i)
							m_ButtonsArea[i].IsSelected = false;
						++m_CurrentSBSelection;
						if(m_CurrentSBSelection >= SBT_MAX)
						{
							m_CurrentSBSelection = SBT_MAX - 1;
							ChangeMenuActive(MAT_RIGHT);
						}
						m_ButtonsArea[m_CurrentSBSelection].IsSelected = true;
						// reset center area selection
						for(u32 i = 0; i < 12; ++i)
							m_LinesArea[i].IsSelected = false;
						m_CurrentLine = 0;
					}
				}
				else if(m_CurrentMenuActive == MAT_CENTER)
				{
					if(m_CurrentMBSelection == MBT_OPTIONS)
					{
						switch(m_CurrentSBSelection)
						{
						case SBT_DISPLAY:
							{
								switch(m_CurrentLine)
								{
								case 0:// driver
									{
										u32 value = (u32)profile->m_ODS.m_DriverType;
										++value %= ProfileData::OptionDataSerializable::DT_MAX;
										profile->m_ODS.m_DriverType = (ProfileData::OptionDataSerializable::DriverType)value;
									}
									break;
								case 1:// resolution
									profile->m_ODS.m_WindowSize = Engine::GetInstance()->GetNextResolution(profile->m_ODS.m_WindowSize);
									break;
								case 2:// fullscreen
									profile->m_ODS.m_FullScreen = !profile->m_ODS.m_FullScreen;
									break;
								case 3:// v-sync
									profile->m_ODS.m_VSync = !profile->m_ODS.m_VSync;
									break;
								case 4:// gamma ramp
									if(profile->m_ODS.m_GammaRamp < 100)
									{
										profile->m_ODS.m_GammaRamp += 1;
										//Engine::GetInstance()->SetGamma(profile->m_ODS.m_GammaRamp);
									}
									break;
								default:
									assert(0);
									break;
								}
							}
							break;
						case SBT_AUDIO:
							{
								if(m_CurrentLine <= 4)
								{
									u8* valuesPtr[4] =
									{
										&profile->m_ODS.m_VolumeGeneral,
										&profile->m_ODS.m_VolumeBG1,
										&profile->m_ODS.m_VolumeBG2,
										&profile->m_ODS.m_VolumeVoice
									};
									if(*(valuesPtr[m_CurrentLine]) < 100)
										*(valuesPtr[m_CurrentLine]) += 1;
								}
							}
							break;
						case SBT_KEYS:
							{

							}
							break;
						case SBT_LANGUAGE:
							{
								s32 ls = (s32)Engine::GetInstance()->GetLanguage();
								s32 oldLs = ls;
								do
								{
									++ls;
									if(ls >= Engine::LS_MAX)
										ls = 0;
								}
								while(!Engine::GetInstance()->m_CoreData.LangsAllowed[ls] && ls != oldLs);
								Engine::GetInstance()->SetLanguage((Engine::LanguageSettings)ls);
								OnResize();// because of the left menu entries dimensions
							}
							break;
						default:
							assert(0);
							break;
						}
					}
					else if(m_CurrentMBSelection == MBT_LOAD || m_CurrentMBSelection == MBT_SAVE)
					{
						if(m_CurrentSaveSlotSelected < PROFILE_SAVE_SLOTS-1)
							m_CurrentSaveSlotSelected += 1;
					}
				}
			}
		}
	}

	return keepMenuOpen;
}

bool InGameMenuPanel::OnEventQuickMenu(const SEvent& event)
{
	bool keepMenuOpen = true;

	ProfileData* profile = ProfileData::GetInstance();
	
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
		{
			Engine* engine = Engine::GetInstance();
			const Dimension2& renderSize = engine->m_RenderSize;
			const Dimension2& originalSize = engine->m_OriginalSize;
			float innerScale = engine->m_InnerScale;
			const Position2& innerOffset = engine->m_InnerOffset;

			Position2 mousePos(event.MouseInput.X, event.MouseInput.Y);

			for(int i = 0; i < 12; ++i)
			{
				m_LinesArea[i].IsSelected = false;
				if(m_LinesArea[i].Area.isPointInside(mousePos))
				{
					m_CurrentLine = i;
					m_LinesArea[m_CurrentLine].IsSelected = true;
				}
			}

		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
		{
			if(m_LinesArea[m_CurrentLine].IsSelected)
			{
				// validate the current entry of the quick menu
				switch(m_CurrentLine)
				{
				case 0: // resume
					break;
				case 1:// skip
					m_RequestSkip = true;
					break;
				case 2:// quick save
					{
						SaveData& save = profile->GetSave(PROFILE_QUICKSAVE_SLOT);
						//if(!save.Used)
						{
							save.Save(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene, GameDesc::m_CurrentDialog, GameDesc::m_CurrentSounds);
							profile->WriteSave();
						}
					}
					break;
				case 3:// quick load
					{
						// PLAY THE SCENE
						SaveData& save = profile->GetSave(PROFILE_QUICKSAVE_SLOT);
						if(save.Used)
						{
							GameDesc::m_CurrentChapter = save.ChapterId;
							GameDesc::m_CurrentScene = save.SceneId;
							GameDesc::m_CurrentDialog = save.DialogId;
							for(u32 i = 0; i < 3; ++i)
							{
								GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
							}
							GameDesc::m_RequestSceneStart = true;
						}
					}
					break;
				default:
					assert(false);
					break;
				}

				keepMenuOpen = false;
			}
		}
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		//if(m_SelectionFadeTimer == 0.0f)
		if(event.KeyInput.PressedDown)
		{
			ProfileData::KeyMappingSerializable& keymap = ProfileData::GetInstance()->m_KMS;
			if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE2))
			{
				// exit quick menu
				keepMenuOpen = false;
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_QTE1))
			{
				// validate the current entry of the quick menu
				switch(m_CurrentLine)
				{
				case 0: // resume
					break;
				case 1:// skip
					m_RequestSkip = true;
					break;
				case 2:// quick save
					{
						SaveData& save = profile->GetSave(PROFILE_QUICKSAVE_SLOT);
						//if(!save.Used)
						{
							save.Save(GameDesc::m_CurrentChapter, GameDesc::m_CurrentScene, GameDesc::m_CurrentDialog, GameDesc::m_CurrentSounds);
							profile->WriteSave();
						}
					}
					break;
				case 3:// quick load
					{
						// PLAY THE SCENE
						SaveData& save = profile->GetSave(PROFILE_QUICKSAVE_SLOT);
						if(save.Used)
						{
							GameDesc::m_CurrentChapter = save.ChapterId;
							GameDesc::m_CurrentScene = save.SceneId;
							GameDesc::m_CurrentDialog = save.DialogId;
							for(u32 i = 0; i < 3; ++i)
							{
								GameDesc::m_CurrentSounds[i] = save.CurrentSounds[i];
							}
							GameDesc::m_RequestSceneStart = true;
						}
					}
					break;
				default:
					assert(false);
					break;
				}

				keepMenuOpen = false;
			}
// 			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_UP))
// 			{
// 				
// 			}
// 			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_DOWN))
// 			{
// 				
// 			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_LEFT))
			{
				if(m_CurrentLine > 0)
					--m_CurrentLine;
			}
			else if(event.KeyInput.Key == keymap.GetKeyCode2(ProfileData::KeyMappingSerializable::II_KEY_RIGHT))
			{
				if(m_CurrentLine < 3)
					++m_CurrentLine;
			}
		}
	}

	return keepMenuOpen;
}

bool InGameMenuPanel::ProcessKey(const SEvent& event)
{
	EKEY_CODE code = (EKEY_CODE)0x0;
	if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
			code = KEY_LBUTTON;
		else if(event.MouseInput.Event == EMIE_RMOUSE_PRESSED_DOWN)
			code = KEY_RBUTTON;
		else if(event.MouseInput.Event == EMIE_MMOUSE_PRESSED_DOWN)
			code = KEY_MBUTTON;
	}
	else if (event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.PressedDown)
		{
			code = event.KeyInput.Key;
		}
	}

	core::stringw str = GetStringFromKeyCode(code);
	if(str != L"")
	{
		m_IsProcessingKey = !ProfileData::GetInstance()->m_KMS.MapKey1((ProfileData::KeyMappingSerializable::IIKeyType)m_CurrentLine, code);

		return true;
	}

	return false;
}


#undef PANEL_FADETIME
