﻿#include "spriteview.h"
#include "engine.h"
#include "utils.h"


// Define some values that we'll use to identify individual GUI controls.
#define GUI_ID_CONTEXT_MENU 101
#define GUI_ID_SAVE_SPRITE_FILE_NAME_DLG 102


// SpriteInterface
SpriteInterface::SpriteInterface() : 
	m_CurrentFrame(NULL),
	m_CurrentLayer(NULL)
{
	m_Sprite = new AnimatedSprite();
	m_Sprite->SetSize(Dimension2(256,256));
}

SpriteInterface::~SpriteInterface()
{
}

bool SpriteInterface::AddImageToBank(Texture* tex)
{
	return m_Sprite->AddImageToData(tex);
}

bool SpriteInterface::RemoveBankImageAtIndex(u32 index, bool& isAnyLayerRemovedOut)
{
	TArray<Texture*>& bank = m_Sprite->GetDataImages();
	if(index < bank.size())
	{
		Texture* tex = bank[index];
		bank.erase(index);
		isAnyLayerRemovedOut = _CheckRemoveImage(tex);
		return true;
	}

	isAnyLayerRemovedOut = false;
	return false;
}

bool SpriteInterface::ClearBankImages()
{
	m_Sprite = SmartPointer<AnimatedSprite>(new AnimatedSprite());
	m_CurrentFrame = NULL;
	m_CurrentLayer = NULL;
	return true;
}

bool SpriteInterface::_CheckRemoveImage(Texture* tex)// check if the texture exists in the frames and remove layer containing this texture
{
	bool ret = false;
	TArray<FrameSprite*>& frames = m_Sprite->GetFrames();
	for(u32 i=0; i<frames.size(); ++i)
	{
		FrameSprite* frame = frames[i];
		if(frame)
		{
			TArray<LayerSprite*>& layers = frame->GetLayers();
			for(u32 j=0; j<layers.size(); ++j)
			{
				LayerSprite* layer = layers[j];
				if(layer && layer->GetImage() == tex)
				{
					SafeDelete(layer);
					layers.erase(j);
					ret = true;
				}
			}
		}
	}

	return ret;
}

bool SpriteInterface::AddNewFrame()
{
	FrameSprite* frame = new FrameSprite();
	frame->SetSize(m_Sprite->GetSize());
	return m_Sprite->AddFrame(frame);
}

bool SpriteInterface::InsertNewFrameAtIndex(u32 index, s32 indexToCopy /*= -1*/)
{
	bool ret = false;
	TArray<FrameSprite*>& frames = m_Sprite->GetFrames();
	if(index < frames.size())
	{
		FrameSprite* ref = (indexToCopy>=0 && (u32)indexToCopy<frames.size())?frames[indexToCopy]:NULL;
		frames.insert(new FrameSprite(ref), index);
		ret = true;
	}
	return ret;
}

bool SpriteInterface::RemoveFrameAtIndex(u32 index)
{
	bool ret = false;
	TArray<FrameSprite*>& frames = m_Sprite->GetFrames();
	if(index < frames.size())
	{
		SafeDelete(frames[index]);
		frames.erase(index);
		ret = true;
	}
	return ret;
}

bool SpriteInterface::ClearFrames()
{
	TArray<FrameSprite*>& frames = m_Sprite->GetFrames();
	for(u32 i=0; i<frames.size(); ++i)
	{
		SafeDelete(frames[i]);
	}
	frames.clear();
	return true;
}

bool SpriteInterface::AddNewLayer()
{
	if(m_CurrentFrame)
	{
		return m_CurrentFrame->AddLayer(new LayerSprite());
	}
	return false;
}

bool SpriteInterface::InsertNewLayerAtIndex(u32 index, s32 indexToCopy /*= -1*/)
{
	if(m_CurrentFrame)
	{
		bool ret = false;
		TArray<LayerSprite*>& layers = m_CurrentFrame->GetLayers();
		if(index < layers.size())
		{
			LayerSprite* ref = (indexToCopy>=0 && (u32)indexToCopy<layers.size())?layers[indexToCopy]:NULL;
			layers.insert(new LayerSprite(ref), index);
			ret = true;
		}
		return ret;
	}
	return false;
}

bool SpriteInterface::RemoveLayerAtIndex(u32 index)
{
	if(m_CurrentFrame)
	{
		bool ret = false;
		TArray<LayerSprite*>& layers = m_CurrentFrame->GetLayers();
		if(index < layers.size())
		{
			SafeDelete(layers[index]);
			layers.erase(index);
			ret = true;
		}
		return ret;
	}
	return false;
}

bool SpriteInterface::ClearLayers()
{
	if(m_CurrentFrame)
	{
		TArray<LayerSprite*>& layers = m_CurrentFrame->GetLayers();
		for(u32 i=0; i<layers.size(); ++i)
		{
			SafeDelete(layers[i]);
		}
		layers.clear();
		return true;
	}
	return false;
}

bool SpriteInterface::SwapLayers(u32 index1, u32 index2)
{
	if(m_CurrentFrame)
	{
		TArray<LayerSprite*>& layers = m_CurrentFrame->GetLayers();
		if(index1 < layers.size() && index2 < layers.size() && index1 != index2)
		{
			LayerSprite* temp = layers[index1];
			layers[index1] = layers[index2];
			layers[index2] = temp;
			return true;
		}
	}
	return false;
}


bool SpriteInterface::LoadSprite(const io::path &name)
{
	m_Sprite = SmartPointer<AnimatedSprite>(new AnimatedSprite());
	bool ret = m_Sprite->LoadSpriteFromFile((char*)name.c_str());
	m_CurrentFrame = m_Sprite->GetFrames().empty()?NULL:m_Sprite->GetFrames()[0];
	m_CurrentLayer = NULL;
	
	return ret;
}

bool SpriteInterface::SaveSprite(const io::path &name)
{
	if(m_Sprite.IsValid())
	{
		return m_Sprite->SaveSpriteToFile((char*)name.c_str());
	}
	return false;
}

void SpriteInterface::EditSprite(const SmartPointer<AnimatedSprite>& sprite)
{
	if(sprite.IsValid())
	{
		m_Sprite = SmartPointer<AnimatedSprite>(sprite);
	}
	else
	{
		m_Sprite = SmartPointer<AnimatedSprite>(new AnimatedSprite());
	}

	m_CurrentFrame = NULL;
	if(!m_Sprite->GetFrames().empty())
		m_CurrentFrame = m_Sprite->GetFrames()[0];
	m_CurrentLayer = NULL;
}

static void ComputeSpriteBank(gui::IGUIListBox* ImgList, TArray<Texture*>& textures, const Dimension2& dim, core::stringc bankName)
{
	Engine* engine = Engine::GetInstance();
	gui::IGUIEnvironment* gui = engine->GetGui();
	video::IVideoDriver* driver = engine->GetDriver();
	// browse the archives for maps
	if ( ImgList )
	{
		ImgList->clear();

		gui::IGUISpriteBank *bank = gui->getSpriteBank(bankName);
		if ( !bank )
			bank = gui->addEmptySpriteBank(bankName);

		gui::SGUISprite sprite;
		gui::SGUISpriteFrame frame;
		Rectangle2 r;

		bank->getSprites().clear();
		bank->getPositions().clear ();
		ImgList->setSpriteBank ( bank );

		u32 g = 0;
		core::stringw s;

		for ( u32 i=0; i< textures.size(); ++i)
		{
			Texture *tex = ResizeTexture(textures[i], dim);

			bank->setTexture ( g, tex );

			r.LowerRightCorner.X = dim.Width;
			r.LowerRightCorner.Y = dim.Height;
			ImgList->setItemHeight ( r.LowerRightCorner.Y + 4 );
			frame.rectNumber = bank->getPositions().size();
			frame.textureNumber = g;

			bank->getPositions().push_back(r);

			sprite.Frames.set_used ( 0 );
			sprite.Frames.push_back(frame);
			sprite.frameTime = 0;
			bank->getSprites().push_back(sprite);

			ImgList->addItem ( s.c_str (), g );
			g += 1;
		}

		ImgList->setSelected ( -1 );
		gui::IGUIScrollBar * bar = (gui::IGUIScrollBar*)ImgList->getElementFromId( 0 );
		if ( bar )
			bar->setPos ( 0 );

	}
}



// SpriteBankListBox
SpriteBankListBox::SpriteBankListBox(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position) 
	: gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), m_Gui(env), m_ContextualMenu(NULL), m_OpenFileDlg(NULL), m_CurrentlyHovered(false), m_SpriteInterface(NULL), m_EditorWindow(NULL), m_LayerMenu(NULL), m_FrameScrollMenu(NULL)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	m_Title = m_Gui->addStaticText(titletext, Rectangle2(Position2(0, 0), Dimension2(this->getRelativePosition().getWidth(), VIEW_TITLES_HEIGHT)), true, true, this);
	m_ListBox = m_Gui->addListBox(Rectangle2(Position2(0, VIEW_TITLES_HEIGHT), Dimension2(this->getRelativePosition().getWidth(), this->getRelativePosition().getHeight()-VIEW_TITLES_HEIGHT)), this);
	m_ListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
}

SpriteBankListBox::~SpriteBankListBox()
{

}

bool SpriteBankListBox::AddTextureToBank(const io::path &name)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	Texture* tex = NULL;
	if(tex = driver->getTexture(name))
	{
		m_SpriteInterface->AddImageToBank(tex);
		ComputeSpriteBank(m_ListBox, m_SpriteInterface->GetSprite()->GetDataImages(), Dimension2(VIEW_SPRITE_BANK_SIZE,VIEW_SPRITE_BANK_SIZE), "TextureBank");
		return true;
	}
	return false;
}

bool SpriteBankListBox::OnEvent(const SEvent& event)
{
// 	SEvent evt = event;
// 	printf("SpriteBankListBox\n");
	if (m_CurrentlyHovered && event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
		{
			bool isAnyImage = (m_ListBox->getItemCount() != 0);
			s32 index = m_ListBox->getSelected();
			Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
			gui::IGUIContextMenu* menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
			menu->addItem(L"Ajouter", 666, true);
			menu->addItem(L"Supprimer", 666, isAnyImage && index >= 0);
			menu->addItem(L"Supprimer tout", 666, isAnyImage);
			// 		menu->addSeparator();
			// 		menu->addItem(L"Annuler");

			Engine* engine = Engine::GetInstance();
			video::IVideoDriver* driver = engine->GetDriver();

			Dimension2 dim = driver->getScreenSize();
			r = menu->getRelativePosition();
			r.constrainTo(Rectangle2(Position2(0, 0), dim));
			menu->setRelativePosition(r);

			menu->setEventParent(this);
		}
		else if(event.MouseInput.Event == EMIE_LMOUSE_DOUBLE_CLICK)
		{
			LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
			if(layer)
			{
				s32 index = m_ListBox->getSelected();
				if(index >= 0 && (u32)index < m_SpriteInterface->GetSprite()->GetDataImages().size())
				{
					bool isFirstImageSet = m_SpriteInterface->GetSprite()->HasAnyLayerImage();
					bool isFirstImageSetForThisFrame = m_SpriteInterface->GetCurrentFrame()->HasAnyLayerImage();
					Texture* tex = m_SpriteInterface->GetSprite()->GetDataImages()[index];
					layer->SetImage(tex);
					if(!isFirstImageSet)
						m_SpriteInterface->GetSprite()->SetSize(tex->getSize());
					if(!isFirstImageSetForThisFrame)
						m_SpriteInterface->GetCurrentFrame()->SetSize(tex->getSize());
					m_LayerMenu->OnLayersChanged();
					m_EditorWindow->OnSpriteChange();
				}
			}
		}
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}

		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// Ajouter
					m_OpenFileDlg = m_Gui->addFileOpenDialog(L"Choisissez une image ou un dossier d'images à ajouter");
					break;
				case 1:// supprimer
					{
						s32 index = m_ListBox->getSelected();
						if(index >= 0 && (u32)index < m_ListBox->getItemCount())
						{
							bool isAnyLayerRemoved = false;
							m_SpriteInterface->RemoveBankImageAtIndex(index, isAnyLayerRemoved);
							if(isAnyLayerRemoved)
							{
								// TODO : popup to tell the user that layers containing the image have been removed 

								m_LayerMenu->OnFrameChanged();
							}
							m_ListBox->removeItem(index);
							ComputeSpriteBank(m_ListBox, m_SpriteInterface->GetSprite()->GetDataImages(), Dimension2(VIEW_SPRITE_BANK_SIZE,VIEW_SPRITE_BANK_SIZE), "TextureBank");
							if((u32)index < m_ListBox->getItemCount())
								m_ListBox->setSelected(index);
							else
								m_ListBox->setSelected(-1);
						}
					}
					break;
				case 2:// supprimer tout
					m_SpriteInterface->ClearBankImages();
					m_ListBox->clear();
					ComputeSpriteBank(m_ListBox, m_SpriteInterface->GetSprite()->GetDataImages(), Dimension2(VIEW_SPRITE_BANK_SIZE,VIEW_SPRITE_BANK_SIZE), "TextureBank");
					m_LayerMenu->OnSpriteChange();
					m_EditorWindow->OnSpriteChange();
					m_FrameScrollMenu->OnSpriteChange();
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void SpriteBankListBox::OnSpriteChange()
{
	m_ListBox->clear();
	ComputeSpriteBank(m_ListBox, m_SpriteInterface->GetSprite()->GetDataImages(), Dimension2(VIEW_SPRITE_BANK_SIZE,VIEW_SPRITE_BANK_SIZE), "TextureBank");
	m_ListBox->setSelected(-1);
}


// FrameListBox
FrameScrollMenu::FrameScrollMenu(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position) 
	: gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), 
	m_Gui(env), 
	m_ContextualMenu(NULL), 
	m_CurrentlyHovered(false), 
	m_SpriteInterface(NULL),
	m_EditorWindow(NULL),
	m_IsPlaying(false)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	m_Title = m_Gui->addStaticText(titletext, Rectangle2(Position2(0, 0), Dimension2(64, this->getRelativePosition().getHeight())), true, true, this);
	m_Title->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_ScrollBar = m_Gui->addScrollBar(true, Rectangle2(Position2(64, 0), Dimension2(this->getRelativePosition().getWidth()-64-32-32, this->getRelativePosition().getHeight())), this);
	m_ScrollBar->setSmallStep(1);
	m_ScrollBar->setLargeStep(1);
	m_ScrollBar->setMin(0);
	m_ScrollBar->setMax(0);
	m_ScrollBar->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
	m_Frames = m_Gui->addStaticText(L"0/0", Rectangle2(Position2(this->getRelativePosition().getWidth()-32-32, 0), Dimension2(32, this->getRelativePosition().getHeight())), true, true, this);
	m_Frames->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	m_ButtonPlay = m_Gui->addButton(Rectangle2(Position2(this->getRelativePosition().getWidth()-32, 0), Dimension2(32, this->getRelativePosition().getHeight())), this, -1, L"Jouer");
	m_ButtonPlay->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
}

FrameScrollMenu::~FrameScrollMenu()
{

}

bool FrameScrollMenu::OnEvent(const SEvent& event)
{
// 	SEvent evt = event;
// 	printf("FrameScrollMenu\n");
	if (m_CurrentlyHovered && event.EventType == EET_MOUSE_INPUT_EVENT && event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP )
	{
		Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
		gui::IGUIContextMenu* menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
		menu->addItem(L"Ajouter", 666, true);
		menu->addItem(L"Insérer", 666, (m_SpriteInterface->GetSprite()->GetFrames().size() > 0));
		menu->addItem(L"Dupliquer", 666, (m_SpriteInterface->GetSprite()->GetFrames().size() > 0));
		menu->addItem(L"Supprimer", 666, (m_SpriteInterface->GetSprite()->GetFrames().size() > 0));
		menu->addItem(L"Supprimer tout", 666, (m_SpriteInterface->GetSprite()->GetFrames().size() > 0));
// 		menu->addSeparator();
// 		menu->addItem(L"Annuler");

		Engine* engine = Engine::GetInstance();
		video::IVideoDriver* driver = engine->GetDriver();

		Dimension2 dim = driver->getScreenSize();
		r = menu->getRelativePosition();
		r.constrainTo(Rectangle2(Position2(0, 0), dim));
		menu->setRelativePosition(r);

		menu->setEventParent(this);
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_SCROLL_BAR_CHANGED && event.GUIEvent.Caller == m_ScrollBar)
		{
			u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
			s32 index = m_ScrollBar->getPos();
			char text[32];
			sprintf(text, "%u/%u", index+1, size);
			m_Frames->setText(core::stringw(text).c_str());
			m_SpriteInterface->SetCurrentFrame(index);
			m_SpriteInterface->SetCurrentLayer(-1);
			m_LayerMenu->OnFrameChanged();
		}
		else if(event.GUIEvent.EventType == gui::EGET_BUTTON_CLICKED && event.GUIEvent.Caller == m_ButtonPlay)
		{
			m_IsPlaying = m_ScrollBar->isEnabled();
			m_ScrollBar->setEnabled(!m_IsPlaying);
			if(!m_IsPlaying)
			{
				m_ButtonPlay->setText(L"Jouer");
			}
			else
			{
				m_ButtonPlay->setText(L"Stop");
			}
			m_EditorWindow->SetDisplayDebugInfo(!m_IsPlaying);
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}

		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// Ajouter
					{
						m_SpriteInterface->AddNewFrame();
						u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
						m_ScrollBar->setMax(size-1);
						m_ScrollBar->setMin(0);
						m_ScrollBar->setPos(size-1);
						s32 index = m_ScrollBar->getPos();
						char text[32];
						sprintf(text, "%u/%u", index+1, size);
						m_Frames->setText(core::stringw(text).c_str());
						m_SpriteInterface->SetCurrentFrame(index);
						m_SpriteInterface->SetCurrentLayer(-1);
						m_LayerMenu->OnFrameChanged();
						m_EditorWindow->OnSpriteChange();
					}
					break;
				case 1:// Insérer
					{
						s32 index = m_ScrollBar->getPos();
						if(index >= 0 && (u32)index < m_SpriteInterface->GetSprite()->GetFrames().size())
						{
							m_SpriteInterface->InsertNewFrameAtIndex(index);
							u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
							m_ScrollBar->setMax(size-1);
							m_ScrollBar->setMin(0);
							index = m_ScrollBar->getPos();
							char text[32];
							sprintf(text, "%u/%u", index+1, size);
							m_Frames->setText(core::stringw(text).c_str());
							m_SpriteInterface->SetCurrentFrame(index);
							m_SpriteInterface->SetCurrentLayer(-1);
							m_LayerMenu->OnFrameChanged();
							m_EditorWindow->OnSpriteChange();
						}
					}
					break;
				case 2:// Dupliquer
					{
						s32 index = m_ScrollBar->getPos();
						if(index >= 0 && (u32)index < m_SpriteInterface->GetSprite()->GetFrames().size())
						{
							m_SpriteInterface->InsertNewFrameAtIndex(index, index);
							u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
							m_ScrollBar->setMax(size-1);
							m_ScrollBar->setMin(0);
							index = m_ScrollBar->getPos();
							char text[32];
							sprintf(text, "%u/%u", index+1, size);
							m_Frames->setText(core::stringw(text).c_str());
							m_SpriteInterface->SetCurrentFrame(index);
							m_SpriteInterface->SetCurrentLayer(-1);
							m_LayerMenu->OnFrameChanged();
							m_EditorWindow->OnSpriteChange();
						}
					}
					break;
				case 3:// supprimer
					{
						s32 index = m_ScrollBar->getPos();
						if(index >= 0 && (u32)index < m_SpriteInterface->GetSprite()->GetFrames().size())
						{
							m_SpriteInterface->RemoveFrameAtIndex(index);
							u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
							m_ScrollBar->setMax(size-1);
							m_ScrollBar->setMin(0);
							index = m_ScrollBar->getPos();
							char text[32];
							sprintf(text, "%u/%u", index+1, size);
							m_Frames->setText(core::stringw(text).c_str());
							m_SpriteInterface->SetCurrentFrame(index);
							m_SpriteInterface->SetCurrentLayer(-1);
							m_LayerMenu->OnFrameChanged();
							m_EditorWindow->OnSpriteChange();
						}
					}
					break;
				case 4:// supprimer tout
					m_SpriteInterface->ClearFrames();
					m_ScrollBar->setMax(0);
					m_ScrollBar->setMin(0);
					m_Frames->setText(L"0/0");
					m_SpriteInterface->SetCurrentFrame(-1);
					m_SpriteInterface->SetCurrentLayer(-1);
					m_LayerMenu->OnFrameChanged();
					m_EditorWindow->OnSpriteChange();
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void FrameScrollMenu::GoNextFrame()
{
	if(m_IsPlaying)
	{
		u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
		s32 index = m_ScrollBar->getPos();
		++index;
		if((u32)index >= size)
			index = 0;
		m_ScrollBar->setPos(index);
		char text[32];
		sprintf(text, "%u/%u", index+1, size);
		m_Frames->setText(core::stringw(text).c_str());
		m_SpriteInterface->SetCurrentFrame(index);
		m_SpriteInterface->SetCurrentLayer(-1);
		m_LayerMenu->OnFrameChanged();
		m_EditorWindow->OnSpriteChange();
	}
}

void FrameScrollMenu::OnSpriteChange()
{
	u32 size = m_SpriteInterface->GetSprite()->GetFrames().size();
	s32 index = (size == 0)?-1:0;
	m_ScrollBar->setPos(index);
	m_ScrollBar->setMax(size-1);
	m_ScrollBar->setMin(0);
	char text[32];
	sprintf(text, "%u/%u", index+1, size);
	m_Frames->setText(core::stringw(text).c_str());
}


// LayerMenu
LayerMenu::LayerMenu(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position) 
	: gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), m_Gui(env), m_ContextualMenu(NULL), m_CurrentlyHovered(false), m_SpriteInterface(NULL), m_EditorWindow(NULL)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	m_Title = m_Gui->addStaticText(titletext, Rectangle2(Position2(0, 0), Dimension2(this->getRelativePosition().getWidth(), VIEW_TITLES_HEIGHT)), true, true, this);
	m_ListBox = m_Gui->addListBox(Rectangle2(Position2(0, VIEW_TITLES_HEIGHT), Dimension2(this->getRelativePosition().getWidth(), this->getRelativePosition().getHeight()-VIEW_TITLES_HEIGHT)), this);
	m_ListBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);
}

LayerMenu::~LayerMenu()
{

}

bool LayerMenu::OnEvent(const SEvent& event)
{
// 	SEvent evt = event;
// 	printf("SpriteBankListBox\n");
	if (m_CurrentlyHovered && event.EventType == EET_MOUSE_INPUT_EVENT && event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP )
	{
		s32 index = m_ListBox->getSelected();
		bool isCurrentFrame = (m_SpriteInterface->GetCurrentFrame() != NULL);
		u32 size = isCurrentFrame?m_SpriteInterface->GetCurrentFrame()->GetLayers().size():0;
		Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
		gui::IGUIContextMenu* menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
		menu->addItem(L"Ajouter", 666, isCurrentFrame);
		menu->addItem(L"Insérer", 666, (isCurrentFrame && size > 0 && index >= 0));
		menu->addItem(L"Dupliquer", 666, (isCurrentFrame && size > 0 && index >= 0));
		menu->addItem(L"Supprimer", 666, (isCurrentFrame && size > 0 && index >= 0));
		menu->addItem(L"Supprimer tout", 666, (isCurrentFrame && size > 0 && index >= 0));
		menu->addSeparator();
		menu->addItem(L"Monter", 666, (isCurrentFrame && size > 1 && index > 0));
		menu->addItem(L"Descendre", 666, (isCurrentFrame && size > 1 && index >= 0 && (u32)index < size-1));

		Engine* engine = Engine::GetInstance();
		video::IVideoDriver* driver = engine->GetDriver();

		Dimension2 dim = driver->getScreenSize();
		r = menu->getRelativePosition();
		r.constrainTo(Rectangle2(Position2(0, 0), dim));
		menu->setRelativePosition(r);

		menu->setEventParent(this);
	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}
		else if(event.GUIEvent.EventType == gui::EGET_LISTBOX_CHANGED || event.GUIEvent.EventType == gui::EGET_LISTBOX_SELECTED_AGAIN)
		{
			m_SpriteInterface->SetCurrentLayer(m_ListBox->getSelected());
			m_EditorWindow->OnSpriteChange();
		}
		
		if(m_SpriteInterface->GetCurrentFrame())
		{
			s32 id = event.GUIEvent.Caller->getID();
			switch(id)
			{
			case GUI_ID_CONTEXT_MENU: // context menu
				if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
				{
					s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
					switch(s)
					{
					case 0:// Ajouter
						{
							if(m_SpriteInterface->AddNewLayer())
							{
								OnLayersChanged();
								m_ListBox->setSelected(m_ListBox->getItemCount()-1);
							}
							s32 index = m_ListBox->getSelected();
							m_SpriteInterface->SetCurrentLayer(index);
							m_EditorWindow->OnSpriteChange();
						}
						break;
					case 1:// Insérer
						{
							s32 index = m_ListBox->getSelected();
							if(index >= 0 && (u32)index < m_SpriteInterface->GetCurrentFrame()->GetLayers().size())
							{
								if(m_SpriteInterface->InsertNewLayerAtIndex(index))
								{
									OnLayersChanged();
									m_ListBox->setSelected(index);
								}
								m_SpriteInterface->SetCurrentLayer(index);
								m_EditorWindow->OnSpriteChange();
							}
						}
						break;
					case 2:// Dupliquer
						{
							s32 index = m_ListBox->getSelected();
							if(index >= 0 && (u32)index < m_SpriteInterface->GetCurrentFrame()->GetLayers().size())
							{
								if(m_SpriteInterface->InsertNewLayerAtIndex(index, index))
								{
									OnLayersChanged();
									m_ListBox->setSelected(index);
								}
								m_SpriteInterface->SetCurrentLayer(index);
								m_EditorWindow->OnSpriteChange();
							}
						}
						break;
					case 3:// supprimer
						{
							s32 index = m_ListBox->getSelected();
							if(index >= 0 && (u32)index < m_SpriteInterface->GetCurrentFrame()->GetLayers().size())
							{
								m_SpriteInterface->RemoveLayerAtIndex(index);
								OnLayersChanged();
								if((u32)index >= m_ListBox->getItemCount())
									index = m_ListBox->getItemCount()-1;
								m_ListBox->setSelected(index);
								m_SpriteInterface->SetCurrentLayer(index);
								m_EditorWindow->OnSpriteChange();
							}
						}
						break;
					case 4:// supprimer tout
						if(m_SpriteInterface->ClearLayers())
						{
							OnLayersChanged();
						}
						m_SpriteInterface->SetCurrentLayer(-1);
						m_EditorWindow->OnSpriteChange();
						break;
					case 5:// Separator
						break;
					case 6:// Monter
						{
							s32 index = m_ListBox->getSelected();
							if(index > 0 && m_SpriteInterface->SwapLayers(index, index-1))
							{
								OnLayersChanged();
								m_SpriteInterface->SetCurrentLayer(index-1);
								m_ListBox->setSelected(index-1);
							}
							m_EditorWindow->OnSpriteChange();
						}
						break;
					case 7:// descendre
						{
							s32 index = m_ListBox->getSelected();
							if(index >= 0 && m_SpriteInterface->SwapLayers(index, index+1))
							{
								OnLayersChanged();
								m_SpriteInterface->SetCurrentLayer(index+1);
								m_ListBox->setSelected(index+1);
							}
							m_EditorWindow->OnSpriteChange();
						}
						break;
					default:
						break;
					}
				}
				break;
			default:
				break;
			}
		}
	}

	return false;
}

void LayerMenu::OnLayersChanged()
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	s32 lastIndex = m_ListBox->getSelected();
	TArray<Texture*> textures;
	if(m_SpriteInterface->GetCurrentFrame())
	{
		const TArray<LayerSprite*>& layers = m_SpriteInterface->GetCurrentFrame()->GetLayers();
		for(u32 i=0; i<layers.size(); ++i)
		{
			LayerSprite* layer = layers[i];
			if(layer)
				textures.push_back(layer->GetImage());
		}
		//if(!textures.empty()) // must recompute an empty list from empty texture list
		{
			ComputeSpriteBank(m_ListBox, textures, Dimension2(VIEW_LAYER_PREVIEW_SIZE,VIEW_LAYER_PREVIEW_SIZE), "LayerPreviewBank");
		}
	}

	if(lastIndex < 0 || (u32)lastIndex >= m_ListBox->getItemCount())
		lastIndex = -1;
	m_ListBox->setSelected(lastIndex);
}

void LayerMenu::OnFrameChanged()
{
	s32 lastIndex = m_ListBox->getSelected();
	m_ListBox->clear();
	FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
	if(frame)
	{
		const TArray<LayerSprite*>& layers = frame->GetLayers();
		for(u32 i=0; i<layers.size(); ++i)
		{
			LayerSprite* layer = layers[i];
			m_ListBox->addItem(L"nouveau layer");
		}
		OnLayersChanged();
	}
}

void LayerMenu::OnSpriteChange()
{
	m_ListBox->setSelected(-1);
	OnFrameChanged();
}


// EditorWindow
EditorWindow::EditorWindow(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position) 
	: gui::IGUIElement(gui::EGUIET_LIST_BOX, env, parent, id, position), 
	m_Gui(env), 
	m_ContextualMenu(NULL), 
	m_LoadFileDlg(NULL),
	m_SaveFileDlg(NULL), 
	m_CurrentlyHovered(false),
	m_LayerCurrentlyDisplaced(false),
	m_SpriteInterface(NULL),
	m_DisplayOtherLayers(false),
	m_DisplayDebugInfo(true),
	m_CtrlIsPressed(false),
	m_ShiftIsPressed(false),
	m_RenderZoom(1.0f)
{
	Engine* engine = Engine::GetInstance();
	IrrlichtDevice* device = engine->GetDevice();
	video::IVideoDriver* driver = engine->GetDriver();

	gui::IGUIStaticText* staticText = NULL;

	// Toolbar line 1
	m_MouseCoord = m_Gui->addStaticText(L"Position : ", Rectangle2(Position2(0, 0), Dimension2(128, VIEW_EDITOR_TOOLBARS_SIZE)), true, true, this);
	m_MouseCoord->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);

	staticText = m_Gui->addStaticText(L"Zoom", Rectangle2(Position2(128, 0), Dimension2(64, VIEW_EDITOR_TOOLBARS_SIZE)), true, true, this);
	staticText->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderZoomSpinBox = m_Gui->addSpinBox(L"Zoom", Rectangle2(Position2(128+64, 0), Dimension2(64, VIEW_EDITOR_TOOLBARS_SIZE)), true, this);
	m_RenderZoomSpinBox->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_RenderZoomSpinBox->setDecimalPlaces(4);
	m_RenderZoomSpinBox->setRange(0.0001f, 10.0f);
	m_RenderZoomSpinBox->setStepSize(0.1f);
	m_RenderZoomSpinBox->setValue(m_RenderZoom);
	

	m_ViewerBackground = m_Gui->addImage(Rectangle2(Position2(0, VIEW_EDITOR_TOOLBARS_SIZE), Dimension2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, this->getRelativePosition().getHeight() - VIEW_EDITOR_TOOLBARS_SIZE)), this);
	m_ViewerBackground->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());
	m_lastSize = dim;

	if (driver->queryFeature(video::EVDF_RENDER_TO_TARGET))
	{
		m_RenderTarget = driver->addRenderTargetTexture(dim, "RTT_EditorWindow", video::ECF_A8R8G8B8);
		m_Viewer = m_Gui->addImage(m_RenderTarget, Position2(0, 0), true, m_ViewerBackground);
	}
	else
	{
		m_RenderTarget = NULL;
		m_Viewer = m_Gui->addImage(Rectangle2(Position2(0, 0), dim), m_ViewerBackground);
	}
	m_Viewer->setAlignment(gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_LOWERRIGHT);

	u32 posY = VIEW_EDITOR_TOOLBARS_SIZE;
	
	staticText = m_Gui->addStaticText(L"Sprite", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteWidthSpinBox = m_Gui->addSpinBox(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_SpriteWidthSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteWidthSpinBox->setDecimalPlaces(0);
	m_SpriteWidthSpinBox->setValue(256);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteHeightSpinBox = m_Gui->addSpinBox(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_SpriteHeightSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteHeightSpinBox->setDecimalPlaces(0);
	m_SpriteHeightSpinBox->setValue(256);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Framerate", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteFramerateSpinBox = m_Gui->addSpinBox(L"Framerate", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_SpriteFramerateSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_SpriteFramerateSpinBox->setDecimalPlaces(0);
	m_SpriteFramerateSpinBox->setValue(12);
	posY += 48;

	staticText = m_Gui->addStaticText(L"Frame", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_FrameWidthSpinBox = m_Gui->addSpinBox(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_FrameWidthSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_FrameWidthSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_FrameHeightSpinBox = m_Gui->addSpinBox(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_FrameHeightSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_FrameHeightSpinBox->setDecimalPlaces(0);
	posY += 48;

	staticText = m_Gui->addStaticText(L"Layer", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;

	staticText = m_Gui->addStaticText(L"Rotation", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerRotationSpinBox = m_Gui->addSpinBox(L"Rotation", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerRotationSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerRotationSpinBox->setDecimalPlaces(0);
	posY += 16;
	
	staticText = m_Gui->addStaticText(L"Source", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position X", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcPosXSpinBox = m_Gui->addSpinBox(L"Position X", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerSrcPosXSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcPosXSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position Y", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcPosYSpinBox = m_Gui->addSpinBox(L"Position Y", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerSrcPosYSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcPosYSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcWidthSpinBox = m_Gui->addSpinBox(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerSrcWidthSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcWidthSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcHeightSpinBox = m_Gui->addSpinBox(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerSrcHeightSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerSrcHeightSpinBox->setDecimalPlaces(0);
	posY += 16;

	staticText = m_Gui->addStaticText(L"Destination", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position X", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestPosXSpinBox = m_Gui->addSpinBox(L"Position X", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerDestPosXSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestPosXSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Position Y", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestPosYSpinBox = m_Gui->addSpinBox(L"Position Y", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerDestPosYSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestPosYSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestWidthSpinBox = m_Gui->addSpinBox(L"Largeur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerDestWidthSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestWidthSpinBox->setDecimalPlaces(0);
	posY += 16;
	staticText = m_Gui->addStaticText(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, true, this);
	staticText->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestHeightSpinBox = m_Gui->addSpinBox(L"Hauteur", Rectangle2(Position2(this->getRelativePosition().getWidth() - VIEW_EDITOR_WINDOW_CONTROL/2, posY), Dimension2(VIEW_EDITOR_WINDOW_CONTROL/2, 16)), true, this);
	m_LayerDestHeightSpinBox->setAlignment(gui::EGUIA_LOWERRIGHT, gui::EGUIA_LOWERRIGHT, gui::EGUIA_UPPERLEFT, gui::EGUIA_UPPERLEFT);
	m_LayerDestHeightSpinBox->setDecimalPlaces(0);
	posY += 48;
}

EditorWindow::~EditorWindow()
{
	/*{
		FILE* output = fopen("test.bin", "wb");
		Image* img = TextureToImage(m_RenderTarget);
		PreProcessAlphaPixels((unsigned char*)(img->lock()), 256*256*4);
		RLECompressFile((unsigned char*)(img->lock()), 256*256*4, output);
		fclose(output);
		img->drop();
	}

	{
		FILE* input = fopen("test.bin", "rb");
		int inputlen = (int)_filelength(_fileno(input));
		unsigned char* fileguts = (unsigned char*)malloc(inputlen);
		fread(fileguts, inputlen, 1, input);
		fclose(input);
		Image* img = TextureToImage(m_RenderTarget);
		RLEDecompressFile(fileguts, inputlen, (unsigned char*)(img->lock()));
		Engine::GetInstance()->GetDriver()->writeImageToFile(img, "test.bmp");
		img->drop();
		free(fileguts);
	}*/
}

void EditorWindow::OnSpriteChange()
{
	m_SpriteWidthSpinBox->setEnabled(false);
	m_SpriteHeightSpinBox->setEnabled(false);
	m_SpriteFramerateSpinBox->setEnabled(false);
	m_FrameWidthSpinBox->setEnabled(false);
	m_FrameHeightSpinBox->setEnabled(false);
	m_LayerRotationSpinBox->setEnabled(false);
	m_LayerSrcPosXSpinBox->setEnabled(false);
	m_LayerSrcPosYSpinBox->setEnabled(false);
	m_LayerSrcWidthSpinBox->setEnabled(false);
	m_LayerSrcHeightSpinBox->setEnabled(false);
	m_LayerDestPosXSpinBox->setEnabled(false);
	m_LayerDestPosYSpinBox->setEnabled(false);
	m_LayerDestWidthSpinBox->setEnabled(false);
	m_LayerDestHeightSpinBox->setEnabled(false);

	if(m_SpriteInterface)
	{
		SmartPointer<AnimatedSprite>& sprite = m_SpriteInterface->GetSprite();
		if(sprite.IsValid())
		{
			Dimension2 size = sprite->GetSize();
			m_SpriteWidthSpinBox->setValue((f32)size.Width);
			m_SpriteHeightSpinBox->setValue((f32)size.Height);
			m_SpriteFramerateSpinBox->setValue((f32)1.0f/sprite->GetFramerate());
			m_SpriteWidthSpinBox->setEnabled(true);
			m_SpriteHeightSpinBox->setEnabled(true);
			m_SpriteFramerateSpinBox->setEnabled(true);

			FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
			if(frame)
			{
				Dimension2 size = frame->GetSize();
				m_FrameWidthSpinBox->setValue((f32)size.Width);
				m_FrameHeightSpinBox->setValue((f32)size.Height);
				m_FrameWidthSpinBox->setEnabled(true);
				m_FrameHeightSpinBox->setEnabled(true);
			}

			LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
			if(layer)
			{
				Position2 srcPos = layer->GetSrcPosition();
				m_LayerRotationSpinBox->setValue(layer->GetRotation());
				m_LayerSrcPosXSpinBox->setValue((f32)srcPos.X);
				m_LayerSrcPosYSpinBox->setValue((f32)srcPos.Y);
				m_LayerRotationSpinBox->setEnabled(true);
				m_LayerSrcPosXSpinBox->setEnabled(true);
				m_LayerSrcPosYSpinBox->setEnabled(true);

				Dimension2 srcSize = layer->GetSrcSize();
				m_LayerSrcWidthSpinBox->setValue((f32)srcSize.Width);
				m_LayerSrcHeightSpinBox->setValue((f32)srcSize.Height);
				m_LayerSrcWidthSpinBox->setEnabled(true);
				m_LayerSrcHeightSpinBox->setEnabled(true);

				Position2 destPos = layer->GetDestPosition();
				m_LayerDestPosXSpinBox->setValue((f32)destPos.X);
				m_LayerDestPosYSpinBox->setValue((f32)destPos.Y);
				m_LayerDestPosXSpinBox->setEnabled(true);
				m_LayerDestPosYSpinBox->setEnabled(true);

				Dimension2 destSize = layer->GetDestSize();
				m_LayerDestWidthSpinBox->setValue((f32)destSize.Width);
				m_LayerDestHeightSpinBox->setValue((f32)destSize.Height);
				m_LayerDestWidthSpinBox->setEnabled(true);
				m_LayerDestHeightSpinBox->setEnabled(true);

			}
		}
	}
}

bool EditorWindow::OnEvent(const SEvent& event)
{
	// 	SEvent evt = event;
	// 	printf("SpriteBankListBox\n");
	if(event.EventType == EET_KEY_INPUT_EVENT)
	{
		if(event.KeyInput.Key == KEY_LSHIFT)
		{
			if(event.KeyInput.PressedDown && !m_ShiftIsPressed)
				m_LayerCurrentlyDisplaced = false;
			m_ShiftIsPressed = event.KeyInput.PressedDown;
		}
		else if(event.KeyInput.Key == KEY_LCONTROL)
		{
			if(event.KeyInput.PressedDown && !m_CtrlIsPressed)
				m_LayerCurrentlyDisplaced = false;
			m_CtrlIsPressed = event.KeyInput.PressedDown;
		}
	}
	else if (event.EventType == EET_MOUSE_INPUT_EVENT)
	{
		if(event.MouseInput.Event == EMIE_LMOUSE_LEFT_UP)
		{
			m_LayerCurrentlyDisplaced = false;
		}
		else if(m_CurrentlyHovered)
		{
			if(event.MouseInput.Event == EMIE_RMOUSE_LEFT_UP)
			{
				Rectangle2 r(event.MouseInput.X, event.MouseInput.Y, 0, 0);
				gui::IGUIContextMenu* menu = m_Gui->addContextMenu(r, 0, GUI_ID_CONTEXT_MENU);
				if(m_DisplayOtherLayers)
					menu->addItem(L"Masquer les autres layers", 666, true);
				else
					menu->addItem(L"Afficher les autres layers", 666, true);
				menu->addSeparator();
				menu->addItem(L"Sauvegarder", 666, true);
				menu->addItem(L"Charger", 666, true);
				// 		menu->addItem(L"Supprimer tout", 666, true);
				// 		menu->addSeparator();
				// 		menu->addItem(L"Annuler");

				Engine* engine = Engine::GetInstance();
				video::IVideoDriver* driver = engine->GetDriver();

				Dimension2 dim = driver->getScreenSize();
				r = menu->getRelativePosition();
				r.constrainTo(Rectangle2(Position2(0, 0), dim));
				menu->setRelativePosition(r);

				menu->setEventParent(this);
			}
			else if(event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN)
			{
				m_LayerCurrentlyDisplaced = true;
				m_LayerCurrentCursorPos = Position2(event.MouseInput.X, event.MouseInput.Y);
				if(!m_ShiftIsPressed && !m_CtrlIsPressed)
				{
					LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
					if(layer)
						m_LayerCurrentCursorPos = Position2((s32)((1.0f/m_RenderZoom) * (m_LayerCurrentCursorPos.X - m_ViewerBackground->getAbsolutePosition().getCenter().X)),
							(s32)((1.0f/m_RenderZoom) * (m_LayerCurrentCursorPos.Y - m_ViewerBackground->getAbsolutePosition().getCenter().Y)))
							- layer->GetDestPosition();
				}
			}
			else if(event.MouseInput.Event == EMIE_MOUSE_MOVED)
			{
				Position2 localPos((s32)((1.0f/m_RenderZoom) * (event.MouseInput.X - m_ViewerBackground->getAbsolutePosition().getCenter().X)), 
					(s32)((1.0f/m_RenderZoom) * (event.MouseInput.Y - m_ViewerBackground->getAbsolutePosition().getCenter().Y)));
				core::stringw str = StringFormat(L"Position : %d:%d", localPos.X, localPos.Y);
					
				m_MouseCoord->setText(str.c_str());
				if(m_LayerCurrentlyDisplaced)
				{
					Position2 newPos = Position2(event.MouseInput.X, event.MouseInput.Y);

					LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
					if(layer)
					{
						if(m_ShiftIsPressed)
						{
							Dimension2 dim = layer->GetDestSize();
							s32 sizeX = (s32)dim.Width;
							s32 sizeY = (s32)dim.Height;
							sizeX += newPos.X - m_LayerCurrentCursorPos.X;
							sizeY -= newPos.Y - m_LayerCurrentCursorPos.Y;
							sizeX = core::max_(sizeX, 0);
							sizeY = core::max_(sizeY, 0);
							layer->SetDestSize(Dimension2((u32)sizeX, (u32)sizeY));
							m_LayerCurrentCursorPos = newPos;
						}
						else if(m_CtrlIsPressed)
						{
							float rot = layer->GetRotation();
							rot += newPos.Y - m_LayerCurrentCursorPos.Y;
							layer->SetRotation(rot);
							m_LayerCurrentCursorPos = newPos;
						}
						else
						{
// 							Position2 pos = layer->GetDestPosition();
// 							pos.X += (s32)((1.0f/m_RenderZoom) * (newPos.X - m_LayerCurrentCursorPos.X));
// 							pos.Y += (s32)((1.0f/m_RenderZoom) * (newPos.Y - m_LayerCurrentCursorPos.Y));
							Position2 pos(localPos.X - m_LayerCurrentCursorPos.X, localPos.Y - m_LayerCurrentCursorPos.Y);
							layer->SetDestPosition(pos);
						}
						OnSpriteChange();
					}
				}
			}
		}
	}
// 	else if(event)
// 	{
// 		m_ViewerBackground->getRelativePosition().getSize();
// 	}
	else if (event.EventType == EET_GUI_EVENT)
	{
		if(event.GUIEvent.EventType == gui::EGET_ELEMENT_HOVERED)
		{
			m_CurrentlyHovered = true;
		}
		else if(event.GUIEvent.EventType == gui::EGET_ELEMENT_LEFT)
		{
			m_CurrentlyHovered = false;
		}
		else if(event.GUIEvent.EventType == gui::EGET_SPINBOX_CHANGED)
		{
			if(event.GUIEvent.Caller == m_RenderZoomSpinBox)
			{
				float zoom = m_RenderZoomSpinBox->getValue();
				m_RenderZoom = core::clamp(zoom, 0.0001f, 10.0f);
			}
			else
			{
				SmartPointer<AnimatedSprite> sprite = m_SpriteInterface->GetSprite();
				FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
				LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
				if(sprite.IsValid())
				{
					if(event.GUIEvent.Caller == m_SpriteWidthSpinBox)
					{
						Dimension2 size = sprite->GetSize();
						size.Width = (u32)m_SpriteWidthSpinBox->getValue();
						sprite->SetSize(size);
					}
					else if(event.GUIEvent.Caller == m_SpriteHeightSpinBox)
					{
						Dimension2 size = sprite->GetSize();
						size.Height = (u32)m_SpriteHeightSpinBox->getValue();
						sprite->SetSize(size);
					}
					else if(event.GUIEvent.Caller == m_SpriteFramerateSpinBox)
					{
						f32 value = m_SpriteFramerateSpinBox->getValue();
						if(value < 1.0f)
							value = 1.0f;
						f32 framerate = 1.0f/(u32)value;
						sprite->SetFramerate(framerate);
					}
					else if(event.GUIEvent.Caller == m_FrameWidthSpinBox)
					{
						if(frame)
						{
							Dimension2 size = frame->GetSize();
							size.Width = (u32)m_FrameWidthSpinBox->getValue();
							frame->SetSize(size);
						}
					}
					else if(event.GUIEvent.Caller == m_FrameHeightSpinBox)
					{
						if(frame)
						{
							Dimension2 size = frame->GetSize();
							size.Height = (u32)m_FrameHeightSpinBox->getValue();
							frame->SetSize(size);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerRotationSpinBox)
					{
						if(layer)
						{
							layer->SetRotation(m_LayerRotationSpinBox->getValue());
						}
					}
					else if(event.GUIEvent.Caller == m_LayerSrcPosXSpinBox)
					{
						if(layer)
						{
							Position2 pos = layer->GetSrcPosition();
							pos.X = (u32)m_LayerSrcPosXSpinBox->getValue();
							layer->SetSrcPosition(pos);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerSrcPosYSpinBox)
					{
						if(layer)
						{
							Position2 pos = layer->GetSrcPosition();
							pos.Y = (u32)m_LayerSrcPosYSpinBox->getValue();
							layer->SetSrcPosition(pos);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerSrcWidthSpinBox)
					{
						if(layer)
						{
							Dimension2 size = layer->GetSrcSize();
							size.Width = (u32)m_LayerSrcWidthSpinBox->getValue();
							layer->SetSrcSize(size);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerSrcHeightSpinBox)
					{
						if(layer)
						{
							Dimension2 size = layer->GetSrcSize();
							size.Height = (u32)m_LayerSrcHeightSpinBox->getValue();
							layer->SetSrcSize(size);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerDestPosXSpinBox)
					{
						if(layer)
						{
							Position2 pos = layer->GetDestPosition();
							pos.X = (u32)m_LayerDestPosXSpinBox->getValue();
							layer->SetDestPosition(pos);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerDestPosYSpinBox)
					{
						if(layer)
						{
							Position2 pos = layer->GetDestPosition();
							pos.Y = (u32)m_LayerDestPosYSpinBox->getValue();
							layer->SetDestPosition(pos);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerDestWidthSpinBox)
					{
						if(layer)
						{
							Dimension2 size = layer->GetDestSize();
							size.Width = (u32)m_LayerDestWidthSpinBox->getValue();
							layer->SetDestSize(size);
						}
					}
					else if(event.GUIEvent.Caller == m_LayerDestHeightSpinBox)
					{
						if(layer)
						{
							Dimension2 size = layer->GetDestSize();
							size.Height = (u32)m_LayerDestHeightSpinBox->getValue();
							layer->SetDestSize(size);
						}
					}
				}
			}
		}

		s32 id = event.GUIEvent.Caller->getID();
		switch(id)
		{
		case GUI_ID_CONTEXT_MENU: // context menu
			if (event.GUIEvent.EventType == gui::EGET_MENU_ITEM_SELECTED)
			{
				s32 s = ((gui::IGUIContextMenu*)event.GUIEvent.Caller)->getSelectedItem();
				switch(s)
				{
				case 0:// display other layers
					m_DisplayOtherLayers = !m_DisplayOtherLayers;
					break;
				case 2:// Save
					m_SaveFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un Dossier où enregistrer");
					break;
				case 3:// Load
					m_LoadFileDlg = m_Gui->addFileOpenDialog(L"Choisissez un fichier à charger");
					break;
				default:
					break;
				}
			}
			break;
		default:
			break;
		}
	}

	return false;
}

void EditorWindow::draw()
{
	gui::IGUIElement::draw();

// 	FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
// 	if(frame)
// 	{
// 		LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
// 		if(layer)
// 		{
// 			Position2 pos(m_Viewer->getAbsolutePosition().getCenter());
// 			pos.X -= frame->GetSize().Width>>1;
// 			pos.Y -= frame->GetSize().Height>>1;
// 			layer->Render(pos);
// 		}
// 	}
}

void EditorWindow::UpdateRenderTarget(float dt)
{
	
	if (m_RenderTarget)
	{
		video::IVideoDriver* driver = Engine::GetInstance()->GetDriver();
		// draw scene into render target

		// handle viewer size change
		Dimension2 dim(m_ViewerBackground->getRelativePosition().getSize());
		if(dim != m_lastSize)
		{
// 			if(m_RenderTarget)
// 				driver->removeTexture(m_RenderTarget);
			m_RenderTarget = driver->addRenderTargetTexture(dim, "RTT_EditorWindow", video::ECF_A8R8G8B8);
			m_Viewer->setImage(m_RenderTarget);
			
			m_lastSize = dim;
		}

		// set render target texture
		driver->setRenderTarget(m_RenderTarget, true, true, COLOR_TRANSPARENT);

		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity

		if(m_SpriteInterface)
		{
			SmartPointer<AnimatedSprite> sprite = m_SpriteInterface->GetSprite();

			if(sprite.IsValid())
			{
				Position2 center = m_ViewerBackground->getRelativePosition().getCenter();
				Dimension2 dim = sprite->GetSize();
				//Position2 pos(center.X - (dim.Width>>1), center.Y - (dim.Height>>1));
				Position2 pos(center);
				core::matrix4 mat = core::matrix4().setTranslation(core::vector3df((f32)pos.X, (f32)pos.Y, 0.0f)) *
					core::matrix4().setScale(core::vector3df(m_RenderZoom, m_RenderZoom, 0.0f));
				if(m_DisplayDebugInfo)
					sprite->RenderDebugInfo(0, mat);
			
				LayerSprite* layer = m_SpriteInterface->GetCurrentLayer();
				if(layer && !m_DisplayOtherLayers)
				{
					FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
					if(frame)
					{
						if(m_DisplayDebugInfo)
							frame->RenderDebugInfo(mat);
					}

					layer->Render(mat);
					if(m_DisplayDebugInfo)
						layer->RenderDebugInfo(mat);
				}
				else
				{
					FrameSprite* frame = m_SpriteInterface->GetCurrentFrame();
					if(frame)
					{
						if(m_DisplayDebugInfo)
							frame->RenderDebugInfo(mat, false, false, true);
						frame->Render(mat);
					}
				}
			}
		}

		// set back old render target
		// The buffer might have been distorted, so clear it
		driver->setRenderTarget(0, true, true, IColor(0,200,200,200));
		driver->setTransform(video::ETS_VIEW, core::IdentityMatrix); //<- set view matrix to identity
	}
}


#undef GUI_ID_CONTEXT_MENU
#undef GUI_ID_SAVE_SPRITE_FILE_NAME_DLG
