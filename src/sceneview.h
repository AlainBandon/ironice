#ifndef SCENEVIEW_H
#define SCENEVIEW_H

#include "utils.h"
#include "sprite.h"
#include "scene.h"

//#include "driverChoice.h"

class SceneEditorWindow;
class DialogListBox;
class AnimationBankListBox;


class SceneInterface
{
public:
	SceneInterface();
	~SceneInterface();

	enum ResourcePathType {RPT_Backgrounds, RPT_Sounds, RPT_Sprites, RPT_Current, RPT_MAX};

	void AddSpriteToBank(const SmartPointer<AnimatedSprite>& sprite);
	bool RemoveBankSpriteAtIndex(u32 index);
	bool ClearBankSprites();

	void AddNewDialog(DialogNode* dialog);
	bool RemoveDialogAtIndex(u32 index);
	bool ClearDialogs();
	bool LoadDialog(const io::path &name);
	bool SaveDialog(const io::path &name);
	bool XMLExportDialog(const io::path &name);

	core::stringw GetNextDialogName();

	bool NewScene();
	bool LoadScene(const io::path &name, bool preloadAll = false);
	bool SaveScene(const io::path &name);

	u32 GetDialogVMemoryUsage(DialogNode* dlg, bool evaluateSprites = true);
	bool ExportSceneForVita(const io::path &name);

	bool ExportSceneDialogsTexts(const io::path &name);
	bool ImportSceneDialogsTexts(const io::path &name);

	void LoadResourcePath(ResourcePathType type);
	void SaveResourcePath(ResourcePathType type, io::path path = "");

	bool UpdateLoading();

	Scene* GetScene() {return m_Scene;}

	SmartPointer<AnimatedSprite> GetCurrentSprite() const {return m_CurrentSprite;}
	void SetCurrentSprite(s32 index) 
	{
		m_CurrentSprite = (index >= 0 && (u32)index < m_Scene->GetSprites().size())? m_Scene->GetSprites()[index]:NULL; 
	}

	DialogNode* GetCurrentDialog() const {return m_CurrentDialog;}
	void SetCurrentDialog(s32 index) 
	{
		m_CurrentDialog = (index >= 0 && (u32)index < m_Scene->GetDialogs().size())? m_Scene->GetDialogs()[index]:NULL; 
	}

	const io::path& GetSceneNameForQuickSave() const {return m_SceneNameForQuickSave;}
	void SetSceneNameForQuickSave(const io::path& sceneName) {m_SceneNameForQuickSave = sceneName;}

	SceneEditorData* m_Data;

	bool m_IsSetForVitaExport;
	bool m_IsSetForTextsExport;
	bool m_IsSetForTextsImport;

protected:
	Scene* m_Scene;

	SmartPointer<AnimatedSprite> m_CurrentSprite;

	DialogNode* m_CurrentDialog;
	io::path m_SceneNameForQuickSave;

	io::path m_ResourcePath[RPT_MAX];
};

class SceneEditorWindow : public gui::IGUIElement
{
public:
	SceneEditorWindow(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~SceneEditorWindow();

	enum FileLoadType {FLT_NONE, FLT_TEXTURE, FLT_SOUND_BG1, FLT_SOUND_BG2, FLT_SOUND_VOICE};

	void SetSceneInterface(SceneInterface* scnInterface) {m_SceneInterface = scnInterface;}

	void SetDialogList(DialogListBox* dialogList) {m_DialogList = dialogList;}
	void SetAnimationBankList(AnimationBankListBox* animationBankList) {m_AnimationBankList = animationBankList;}

	virtual void draw();

	void UpdateRenderTarget(float dt);

	void UpdateBgBank();
	void UpdateSpriteListBank(s32 select = -1);

	void OnSceneChange();
	void OnDialogChange();
	void OnSpriteSelectionChanged();

	void OnSound1Changed();
	void OnSound2Changed();
	void OnVoiceChanged();

	enum SceneTabType{SCT_Dialog, SCT_Background, SCT_Sprites, SCT_Tansitions, SCT_Sounds, SCT_MAX};
	void SetSceneTab(SceneTabType tabType) { m_TabControl->setActiveTab((s32)tabType);}

	void SetSoundBG1(const io::path& path);
	void SetSoundBG2(const io::path& path);
	void SetSoundVoice(const io::path& path);

	gui::IGUIImage* m_Viewer;
	gui::IGUIImage* m_ViewerBackground;
	gui::IGUIContextMenu* m_ContextualMenu;
	gui::IGUIFileOpenDialog* m_LoadFileDlg;
	gui::IGUIFileOpenDialog* m_SaveFileDlg;
	gui::IGUIWindow* m_PopupDialog;

	gui::IGUIFileOpenDialog* m_OpenFileDlg;

	FileLoadType m_FileLoadType;

	video::ITexture* m_RenderTarget;

	bool m_CurrentlyHovered;
	bool m_LayerCurrentlyDisplaced;
	Position2 m_LayerCurrentCursorPos;
	bool m_CtrlIsPressed;
	bool m_ShiftIsPressed;

protected:
	virtual bool OnEvent(const SEvent& event);

	void ChangeRenderTargetSize();

	SceneTabType m_CurrentTab;

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	// create tab control and tabs
	gui::IGUITabControl* m_TabControl;

	gui::IGUIScrollBar* m_ViewerWScrollBar;
	gui::IGUIScrollBar* m_ViewerHScrollBar;

	gui::IGUIScrollBar* m_TimeControlScrollBar;
	gui::IGUIStaticText* m_TimeText;
	gui::IGUIButton* m_ButtonPlay;

	gui::IGUISpinBox* m_RenderWidthSpinBox;
	gui::IGUISpinBox* m_RenderHeightSpinBox;
	gui::IGUIComboBox* m_LanguageTypeComboBox;


	Position2 m_ViewerPosition;

	gui::IGUIComboBox* m_DialogTypeComboBox;
	gui::IGUIEditBox* m_DialogHeaderBox;
	gui::IGUIEditBox* m_DialogTextBox;
	gui::IGUICheckBox* m_DialogTextBGCheckBox;
	gui::IGUISpinBox* m_DialogSpeedSpinBox;
	gui::IGUISpinBox* m_DialogDurationSpinBox;
	gui::IGUISpinBox* m_DialogPosXSpinBox;
	gui::IGUISpinBox* m_DialogPosYSpinBox;
	gui::IGUISpinBox* m_DialogSizeXSpinBox;
	gui::IGUISpinBox* m_DialogSizeYSpinBox;
	gui::IGUIListBox* m_DialogChildListBox;
	gui::IGUIEditBox* m_DialogChoiceTextBox;
	gui::IGUIListBox* m_DialogInputsListBox;
	gui::IGUISpinBox* m_DialogInputDurationSpinBox;
	gui::IGUISpinBox* m_DialogInputNbPressSpinBox;
	gui::IGUICheckBox* m_DialogInputsCheckBox[InputData::PI_MAX];

	gui::IGUIListBox* m_DialogBackgroundListBox;
	gui::IGUIListBox* m_DialogBackgroundKeysListBox;
	gui::IGUISpinBox* m_DialogBGKeyTimeSpinBox;
	gui::IGUISpinBox* m_DialogBGKeyPosXSpinBox;
	gui::IGUISpinBox* m_DialogBGKeyPosYSpinBox;
	gui::IGUISpinBox* m_DialogBGKeyScaleSpinBox;
	gui::IGUISpinBox* m_DialogBGKeyRotationSpinBox;
	gui::IGUIStaticText* m_DialogBGKeyColorTextBox;
	gui::IGUISpinBox* m_DialogBGKeyColorASpinBox;
	gui::IGUISpinBox* m_DialogBGKeyColorRSpinBox;
	gui::IGUISpinBox* m_DialogBGKeyColorGSpinBox;
	gui::IGUISpinBox* m_DialogBGKeyColorBSpinBox;
	gui::IGUICheckBox* m_DialogBGKeysLoopCheckBox;
	gui::IGUISpinBox* m_DialogScreenShakeStrengthSpinBox;
	gui::IGUISpinBox* m_DialogScreenShakeFrequencySpinBox;

	gui::IGUIListBox* m_SpriteListBox;
	gui::IGUIListBox* m_SpriteKeysListBox;
	gui::IGUISpinBox* m_SpriteKeyTimeSpinBox;
	gui::IGUISpinBox* m_SpriteKeyPosXSpinBox;
	gui::IGUISpinBox* m_SpriteKeyPosYSpinBox;
	gui::IGUISpinBox* m_SpriteKeyScaleSpinBox;
	gui::IGUISpinBox* m_SpriteKeyRotationSpinBox;
	gui::IGUIStaticText* m_SpriteKeyColorTextBox;
	gui::IGUISpinBox* m_SpriteKeyColorASpinBox;
	gui::IGUISpinBox* m_SpriteKeyColorRSpinBox;
	gui::IGUISpinBox* m_SpriteKeyColorGSpinBox;
	gui::IGUISpinBox* m_SpriteKeyColorBSpinBox;
	gui::IGUICheckBox* m_SpriteMirrorHCheckBox;
	gui::IGUICheckBox* m_SpriteMirrorVCheckBox;
	gui::IGUICheckBox* m_SpriteKeysLoopCheckBox;
	gui::IGUICheckBox* m_SpriteAnimLoopCheckBox;

	gui::IGUIStaticText* m_AudioBGTextBox1;
	gui::IGUIStaticText* m_AudioBGTextBox2;
	gui::IGUIStaticText* m_AudioVoiceTextBox;
	gui::IGUIButton* m_AudioBGBrowseButton1;
	gui::IGUIButton* m_AudioBGBrowseButton2;
	gui::IGUIButton* m_AudioVoiceBrowseButton;
	gui::IGUIButton* m_AudioBGPlayButton1;
	gui::IGUIButton* m_AudioBGPlayButton2;
	gui::IGUIButton* m_AudioVoicePlayButton;
	gui::IGUISpinBox* m_AudioBG1VolumeSpinBox;
	gui::IGUISpinBox* m_AudioBG2VolumeSpinBox;
	gui::IGUISpinBox* m_AudioVoiceVolumeSpinBox;
	gui::IGUISpinBox* m_AudioBG1PitchSpinBox;
	gui::IGUISpinBox* m_AudioBG2PitchSpinBox;
	gui::IGUISpinBox* m_AudioVoicePitchSpinBox;
	gui::IGUISpinBox* m_AudioBG1PanSpinBox;
	gui::IGUISpinBox* m_AudioBG2PanSpinBox;
	gui::IGUISpinBox* m_AudioVoicePanSpinBox;
	gui::IGUICheckBox* m_AudioBG1RepeatCheckBox;
	gui::IGUICheckBox* m_AudioBG2RepeatCheckBox;
	gui::IGUICheckBox* m_AudioVoiceRepeatCheckBox;
	gui::IGUICheckBox* m_AudioBG1StopCheckBox;
	gui::IGUICheckBox* m_AudioBG2StopCheckBox;
	gui::IGUICheckBox* m_AudioVoiceStopCheckBox;

	gui::IGUIComboBox* m_TransitionInTypeComboBox;
	gui::IGUISpinBox* m_TransitionInDurationSpinBox;
	gui::IGUIComboBox* m_TransitionOutTypeComboBox;
	gui::IGUISpinBox* m_TransitionOutDurationSpinBox;

	SceneInterface* m_SceneInterface;
	DialogListBox* m_DialogList;
	AnimationBankListBox* m_AnimationBankList;

	Dimension2 m_lastSize;

	float m_CurrentDlgTime;
	bool m_IsPlaying;
	bool m_IsPlayingSoundPreview;

	TArray<Rectangle2> m_DialogChoiceAreas;
};

class DialogListBox : public gui::IGUIElement
{
public:
	DialogListBox(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~DialogListBox();

	enum DialogFileType {DFT_NONE, DFT_IMPORT, DFT_EXPORT, DFT_XML_EXPORT};

	void SetSceneInterface(SceneInterface* scnInterface) {m_SceneInterface = scnInterface;}

	bool AddDialog(const io::path &name);

	//void SetLayerMenu(LayerMenu* layerMenu) {m_LayerMenu = layerMenu;}
	void SetSceneEditorWindow(SceneEditorWindow* sceneEditorWindow) {m_SceneEditorWindow = sceneEditorWindow;}
	void SetAnimationBankList(AnimationBankListBox* animationBankList) {m_AnimationBankList = animationBankList;}

	void OnSceneChange();

	gui::IGUIListBox* m_ListBox;
	gui::IGUIContextMenu* m_ContextualMenu;
	gui::IGUIFileOpenDialog* m_OpenFileDlg;

	bool m_CurrentlyHovered;

	DialogFileType m_DialogFileType;

protected:
	virtual bool OnEvent(const SEvent& event);

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	SceneInterface* m_SceneInterface;
	SceneEditorWindow* m_SceneEditorWindow;
	AnimationBankListBox* m_AnimationBankList;
};

class AnimationBankListBox : public gui::IGUIElement
{
public:
	AnimationBankListBox(gui::IGUIEnvironment* env, gui::IGUIElement* parent, s32 id, wchar_t* titletext, Rectangle2 position);
	~AnimationBankListBox();

	enum SpriteLoadType {SLT_NONE, SLT_SPRITE, SLT_TEXTURE, SLT_BATCH_TEXTURE};

	void UpdateSpriteBank();

	void SetSceneInterface(SceneInterface* scnInterface) {m_SceneInterface = scnInterface;}

	void SetSceneEditorWindow(SceneEditorWindow* sceneEditorWindow) {m_SceneEditorWindow = sceneEditorWindow;}
	void SetDialogList(DialogListBox* dialogList) {m_DialogList = dialogList;}

	bool AddSpriteToBank(const io::path &name);

	bool ExportSpriteXML(const io::path &name);

	void OnSceneChange();

	gui::IGUIListBox* m_ListBox;
	gui::IGUIContextMenu* m_ContextualMenu;
	gui::IGUIFileOpenDialog* m_OpenFileDlg;
	gui::IGUIFileOpenDialog* m_SaveFileDlg;

	SpriteLoadType m_SpriteLoadType;

	bool m_CurrentlyHovered;
	SmartPointer<AnimatedSprite> m_SpriteToEdit;

protected:
	virtual bool OnEvent(const SEvent& event);

	gui::IGUIEnvironment* m_Gui;
	gui::IGUIStaticText* m_Title;

	SceneInterface* m_SceneInterface;
	SceneEditorWindow* m_SceneEditorWindow;
	DialogListBox* m_DialogList;
};


class NameSelectWindow : public gui::IGUIElement
{
public:
	NameSelectWindow(gui::IGUIEnvironment* environment, gui::IGUIElement* parent, s32 id, core::rect<s32> rectangle, gui::IGUIListBox* inListBox, SceneInterface* scnInterface = NULL);
	~NameSelectWindow() {}

	const wchar_t*  GetTitle() const {return m_EditBox->getText();}
	void SetTitle(const wchar_t*  title) {m_EditBox->setText(title);}

	virtual bool OnEvent(const SEvent& event);


protected:
	gui::IGUIEnvironment* m_Gui;
	gui::IGUIWindow* m_Window;
	gui::IGUIEditBox* m_EditBox;

	gui::IGUIButton* m_ValidButton;

	gui::IGUIListBox* m_InListBox;

	SceneInterface* m_ScnInterface;
};

class ListSelectWindow : public gui::IGUIElement
{
public:
	ListSelectWindow(gui::IGUIEnvironment* environment, gui::IGUIElement* parent, s32 id, core::rect<s32> rectangle, const TArray<const wchar_t*>& list, SceneInterface* scnInterface = NULL);
	~ListSelectWindow() {}

	s32 GetSelectedIndex() const {return m_ListBox->getSelected();}

	virtual bool OnEvent(const SEvent& event);


protected:
	gui::IGUIEnvironment* m_Gui;
	gui::IGUIWindow* m_Window;
	gui::IGUIListBox* m_ListBox;

	gui::IGUIButton* m_ValidButton;

	SceneInterface* m_ScnInterface;
};


#endif //#ifdef SCENEVIEW_H
