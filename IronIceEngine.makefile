# Compiler flags...
CPP_COMPILER = g++
C_COMPILER = gcc

# Include paths...
Debug_Include_Path=-I"./dependancies/irrlicht/include" -I"./dependancies/audiere/include" -I"./dependancies/vld/include" -I"./dependancies/ft235/include" 
Release_Include_Path=-I"./dependancies/irrlicht/include" -I"./dependancies/audiere/include" -I"./dependancies/ft235/include" 

# Library paths...
Debug_Library_Path=-L"./dependancies/irrlicht/lib/debug" -L"./dependancies/audiere/lib" -L"./dependancies/ft235/lib" 
Release_Library_Path=-L"./dependancies/irrlicht/lib/release" -L"./dependancies/audiere/lib" -L"./dependancies/ft235/lib" 

# Additional libraries...
Debug_Libraries=-Wl,--start-group -lopengl32 -laudiere -lvld -lfreetype  -Wl,--end-group
Release_Libraries=-Wl,--start-group -lopengl32 -laudiere -lfreetype  -Wl,--end-group

# Preprocessor definitions...
Debug_Preprocessor_Definitions=-D GCC_BUILD -D _DEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D IRON_ICE_ENGINE 
Release_Preprocessor_Definitions=-D GCC_BUILD -D NDEBUG -D _CONSOLE -D _CRT_SECURE_NO_DEPRECATE -D IRON_ICE_ENGINE 

# Implictly linked object files...
Debug_Implicitly_Linked_Objects=
Release_Implicitly_Linked_Objects=

# Compiler flags...
Debug_Compiler_Flags=-O0 -g 
Release_Compiler_Flags=-O2 

# Builds all configurations for this project...
.PHONY: build_all_configurations
build_all_configurations: Debug Release 

# Builds the Debug configuration...
.PHONY: Debug
Debug: create_folders ./gccDebug/src/CGUITTFont.o ./gccDebug/src/scene.o ./gccDebug/src/sprite.o ./gccDebug/src/gameview.o ./gccDebug/src/bonussubmenupanel.o ./gccDebug/src/exitsubmenupanel.o ./gccDebug/src/gamepanel.o ./gccDebug/src/ingamemenupanel.o ./gccDebug/src/languagepanel.o ./gccDebug/src/logopanel.o ./gccDebug/src/mainmenupanel.o ./gccDebug/src/optionssubmenupanel.o ./gccDebug/src/popuppanel.o ./gccDebug/src/scenepanel.o ./gccDebug/src/startmenupanel.o ./gccDebug/src/statssubmenupanel.o ./gccDebug/src/storysubmenupanel.o ./gccDebug/src/audio.o ./gccDebug/src/engine.o ./gccDebug/src/glossary.o ./gccDebug/src/main_game.o ./gccDebug/src/utils.o 
	g++ ./gccDebug/src/CGUITTFont.o ./gccDebug/src/scene.o ./gccDebug/src/sprite.o ./gccDebug/src/gameview.o ./gccDebug/src/bonussubmenupanel.o ./gccDebug/src/exitsubmenupanel.o ./gccDebug/src/gamepanel.o ./gccDebug/src/ingamemenupanel.o ./gccDebug/src/languagepanel.o ./gccDebug/src/logopanel.o ./gccDebug/src/mainmenupanel.o ./gccDebug/src/optionssubmenupanel.o ./gccDebug/src/popuppanel.o ./gccDebug/src/scenepanel.o ./gccDebug/src/startmenupanel.o ./gccDebug/src/statssubmenupanel.o ./gccDebug/src/storysubmenupanel.o ./gccDebug/src/audio.o ./gccDebug/src/engine.o ./gccDebug/src/glossary.o ./gccDebug/src/main_game.o ./gccDebug/src/utils.o  $(Debug_Library_Path) $(Debug_Libraries) -Wl,-rpath,./ -o ./gccDebug/IronIceEngine.exe

# Compiles file src/CGUITTFont.cpp for the Debug configuration...
-include ./gccDebug/src/CGUITTFont.d
./gccDebug/src/CGUITTFont.o: src/CGUITTFont.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/CGUITTFont.cpp $(Debug_Include_Path) -o ./gccDebug/src/CGUITTFont.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/CGUITTFont.cpp $(Debug_Include_Path) > ./gccDebug/src/CGUITTFont.d

# Compiles file src/scene.cpp for the Debug configuration...
-include ./gccDebug/src/scene.d
./gccDebug/src/scene.o: src/scene.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/scene.cpp $(Debug_Include_Path) -o ./gccDebug/src/scene.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/scene.cpp $(Debug_Include_Path) > ./gccDebug/src/scene.d

# Compiles file src/sprite.cpp for the Debug configuration...
-include ./gccDebug/src/sprite.d
./gccDebug/src/sprite.o: src/sprite.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/sprite.cpp $(Debug_Include_Path) -o ./gccDebug/src/sprite.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/sprite.cpp $(Debug_Include_Path) > ./gccDebug/src/sprite.d

# Compiles file src/gameview.cpp for the Debug configuration...
-include ./gccDebug/src/gameview.d
./gccDebug/src/gameview.o: src/gameview.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/gameview.cpp $(Debug_Include_Path) -o ./gccDebug/src/gameview.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/gameview.cpp $(Debug_Include_Path) > ./gccDebug/src/gameview.d

# Compiles file src/bonussubmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/bonussubmenupanel.d
./gccDebug/src/bonussubmenupanel.o: src/bonussubmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/bonussubmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/bonussubmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/bonussubmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/bonussubmenupanel.d

# Compiles file src/exitsubmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/exitsubmenupanel.d
./gccDebug/src/exitsubmenupanel.o: src/exitsubmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/exitsubmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/exitsubmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/exitsubmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/exitsubmenupanel.d

# Compiles file src/gamepanel.cpp for the Debug configuration...
-include ./gccDebug/src/gamepanel.d
./gccDebug/src/gamepanel.o: src/gamepanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/gamepanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/gamepanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/gamepanel.cpp $(Debug_Include_Path) > ./gccDebug/src/gamepanel.d

# Compiles file src/ingamemenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/ingamemenupanel.d
./gccDebug/src/ingamemenupanel.o: src/ingamemenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/ingamemenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/ingamemenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/ingamemenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/ingamemenupanel.d

# Compiles file src/languagepanel.cpp for the Debug configuration...
-include ./gccDebug/src/languagepanel.d
./gccDebug/src/languagepanel.o: src/languagepanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/languagepanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/languagepanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/languagepanel.cpp $(Debug_Include_Path) > ./gccDebug/src/languagepanel.d

# Compiles file src/logopanel.cpp for the Debug configuration...
-include ./gccDebug/src/logopanel.d
./gccDebug/src/logopanel.o: src/logopanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/logopanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/logopanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/logopanel.cpp $(Debug_Include_Path) > ./gccDebug/src/logopanel.d

# Compiles file src/mainmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/mainmenupanel.d
./gccDebug/src/mainmenupanel.o: src/mainmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/mainmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/mainmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/mainmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/mainmenupanel.d

# Compiles file src/optionssubmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/optionssubmenupanel.d
./gccDebug/src/optionssubmenupanel.o: src/optionssubmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/optionssubmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/optionssubmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/optionssubmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/optionssubmenupanel.d

# Compiles file src/popuppanel.cpp for the Debug configuration...
-include ./gccDebug/src/popuppanel.d
./gccDebug/src/popuppanel.o: src/popuppanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/popuppanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/popuppanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/popuppanel.cpp $(Debug_Include_Path) > ./gccDebug/src/popuppanel.d

# Compiles file src/scenepanel.cpp for the Debug configuration...
-include ./gccDebug/src/scenepanel.d
./gccDebug/src/scenepanel.o: src/scenepanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/scenepanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/scenepanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/scenepanel.cpp $(Debug_Include_Path) > ./gccDebug/src/scenepanel.d

# Compiles file src/startmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/startmenupanel.d
./gccDebug/src/startmenupanel.o: src/startmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/startmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/startmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/startmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/startmenupanel.d

# Compiles file src/statssubmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/statssubmenupanel.d
./gccDebug/src/statssubmenupanel.o: src/statssubmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/statssubmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/statssubmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/statssubmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/statssubmenupanel.d

# Compiles file src/storysubmenupanel.cpp for the Debug configuration...
-include ./gccDebug/src/storysubmenupanel.d
./gccDebug/src/storysubmenupanel.o: src/storysubmenupanel.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/storysubmenupanel.cpp $(Debug_Include_Path) -o ./gccDebug/src/storysubmenupanel.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/storysubmenupanel.cpp $(Debug_Include_Path) > ./gccDebug/src/storysubmenupanel.d

# Compiles file src/audio.cpp for the Debug configuration...
-include ./gccDebug/src/audio.d
./gccDebug/src/audio.o: src/audio.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/audio.cpp $(Debug_Include_Path) -o ./gccDebug/src/audio.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/audio.cpp $(Debug_Include_Path) > ./gccDebug/src/audio.d

# Compiles file src/engine.cpp for the Debug configuration...
-include ./gccDebug/src/engine.d
./gccDebug/src/engine.o: src/engine.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/engine.cpp $(Debug_Include_Path) -o ./gccDebug/src/engine.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/engine.cpp $(Debug_Include_Path) > ./gccDebug/src/engine.d

# Compiles file src/glossary.cpp for the Debug configuration...
-include ./gccDebug/src/glossary.d
./gccDebug/src/glossary.o: src/glossary.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/glossary.cpp $(Debug_Include_Path) -o ./gccDebug/src/glossary.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/glossary.cpp $(Debug_Include_Path) > ./gccDebug/src/glossary.d

# Compiles file src/main_game.cpp for the Debug configuration...
-include ./gccDebug/src/main_game.d
./gccDebug/src/main_game.o: src/main_game.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/main_game.cpp $(Debug_Include_Path) -o ./gccDebug/src/main_game.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/main_game.cpp $(Debug_Include_Path) > ./gccDebug/src/main_game.d

# Compiles file src/utils.cpp for the Debug configuration...
-include ./gccDebug/src/utils.d
./gccDebug/src/utils.o: src/utils.cpp
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -c src/utils.cpp $(Debug_Include_Path) -o ./gccDebug/src/utils.o
	$(CPP_COMPILER) $(Debug_Preprocessor_Definitions) $(Debug_Compiler_Flags) -MM src/utils.cpp $(Debug_Include_Path) > ./gccDebug/src/utils.d

# Builds the Release configuration...
.PHONY: Release
Release: create_folders ./gccRelease/src/CGUITTFont.o ./gccRelease/src/scene.o ./gccRelease/src/sprite.o ./gccRelease/src/gameview.o ./gccRelease/src/bonussubmenupanel.o ./gccRelease/src/exitsubmenupanel.o ./gccRelease/src/gamepanel.o ./gccRelease/src/ingamemenupanel.o ./gccRelease/src/languagepanel.o ./gccRelease/src/logopanel.o ./gccRelease/src/mainmenupanel.o ./gccRelease/src/optionssubmenupanel.o ./gccRelease/src/popuppanel.o ./gccRelease/src/scenepanel.o ./gccRelease/src/startmenupanel.o ./gccRelease/src/statssubmenupanel.o ./gccRelease/src/storysubmenupanel.o ./gccRelease/src/audio.o ./gccRelease/src/engine.o ./gccRelease/src/glossary.o ./gccRelease/src/main_game.o ./gccRelease/src/utils.o 
	g++ ./gccRelease/src/CGUITTFont.o ./gccRelease/src/scene.o ./gccRelease/src/sprite.o ./gccRelease/src/gameview.o ./gccRelease/src/bonussubmenupanel.o ./gccRelease/src/exitsubmenupanel.o ./gccRelease/src/gamepanel.o ./gccRelease/src/ingamemenupanel.o ./gccRelease/src/languagepanel.o ./gccRelease/src/logopanel.o ./gccRelease/src/mainmenupanel.o ./gccRelease/src/optionssubmenupanel.o ./gccRelease/src/popuppanel.o ./gccRelease/src/scenepanel.o ./gccRelease/src/startmenupanel.o ./gccRelease/src/statssubmenupanel.o ./gccRelease/src/storysubmenupanel.o ./gccRelease/src/audio.o ./gccRelease/src/engine.o ./gccRelease/src/glossary.o ./gccRelease/src/main_game.o ./gccRelease/src/utils.o  $(Release_Library_Path) $(Release_Libraries) -Wl,-rpath,./ -o ./gccRelease/IronIceEngine.exe

# Compiles file src/CGUITTFont.cpp for the Release configuration...
-include ./gccRelease/src/CGUITTFont.d
./gccRelease/src/CGUITTFont.o: src/CGUITTFont.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/CGUITTFont.cpp $(Release_Include_Path) -o ./gccRelease/src/CGUITTFont.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/CGUITTFont.cpp $(Release_Include_Path) > ./gccRelease/src/CGUITTFont.d

# Compiles file src/scene.cpp for the Release configuration...
-include ./gccRelease/src/scene.d
./gccRelease/src/scene.o: src/scene.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/scene.cpp $(Release_Include_Path) -o ./gccRelease/src/scene.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/scene.cpp $(Release_Include_Path) > ./gccRelease/src/scene.d

# Compiles file src/sprite.cpp for the Release configuration...
-include ./gccRelease/src/sprite.d
./gccRelease/src/sprite.o: src/sprite.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/sprite.cpp $(Release_Include_Path) -o ./gccRelease/src/sprite.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/sprite.cpp $(Release_Include_Path) > ./gccRelease/src/sprite.d

# Compiles file src/gameview.cpp for the Release configuration...
-include ./gccRelease/src/gameview.d
./gccRelease/src/gameview.o: src/gameview.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/gameview.cpp $(Release_Include_Path) -o ./gccRelease/src/gameview.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/gameview.cpp $(Release_Include_Path) > ./gccRelease/src/gameview.d

# Compiles file src/bonussubmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/bonussubmenupanel.d
./gccRelease/src/bonussubmenupanel.o: src/bonussubmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/bonussubmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/bonussubmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/bonussubmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/bonussubmenupanel.d

# Compiles file src/exitsubmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/exitsubmenupanel.d
./gccRelease/src/exitsubmenupanel.o: src/exitsubmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/exitsubmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/exitsubmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/exitsubmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/exitsubmenupanel.d

# Compiles file src/gamepanel.cpp for the Release configuration...
-include ./gccRelease/src/gamepanel.d
./gccRelease/src/gamepanel.o: src/gamepanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/gamepanel.cpp $(Release_Include_Path) -o ./gccRelease/src/gamepanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/gamepanel.cpp $(Release_Include_Path) > ./gccRelease/src/gamepanel.d

# Compiles file src/ingamemenupanel.cpp for the Release configuration...
-include ./gccRelease/src/ingamemenupanel.d
./gccRelease/src/ingamemenupanel.o: src/ingamemenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/ingamemenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/ingamemenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/ingamemenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/ingamemenupanel.d

# Compiles file src/languagepanel.cpp for the Release configuration...
-include ./gccRelease/src/languagepanel.d
./gccRelease/src/languagepanel.o: src/languagepanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/languagepanel.cpp $(Release_Include_Path) -o ./gccRelease/src/languagepanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/languagepanel.cpp $(Release_Include_Path) > ./gccRelease/src/languagepanel.d

# Compiles file src/logopanel.cpp for the Release configuration...
-include ./gccRelease/src/logopanel.d
./gccRelease/src/logopanel.o: src/logopanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/logopanel.cpp $(Release_Include_Path) -o ./gccRelease/src/logopanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/logopanel.cpp $(Release_Include_Path) > ./gccRelease/src/logopanel.d

# Compiles file src/mainmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/mainmenupanel.d
./gccRelease/src/mainmenupanel.o: src/mainmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/mainmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/mainmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/mainmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/mainmenupanel.d

# Compiles file src/optionssubmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/optionssubmenupanel.d
./gccRelease/src/optionssubmenupanel.o: src/optionssubmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/optionssubmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/optionssubmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/optionssubmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/optionssubmenupanel.d

# Compiles file src/popuppanel.cpp for the Release configuration...
-include ./gccRelease/src/popuppanel.d
./gccRelease/src/popuppanel.o: src/popuppanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/popuppanel.cpp $(Release_Include_Path) -o ./gccRelease/src/popuppanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/popuppanel.cpp $(Release_Include_Path) > ./gccRelease/src/popuppanel.d

# Compiles file src/scenepanel.cpp for the Release configuration...
-include ./gccRelease/src/scenepanel.d
./gccRelease/src/scenepanel.o: src/scenepanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/scenepanel.cpp $(Release_Include_Path) -o ./gccRelease/src/scenepanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/scenepanel.cpp $(Release_Include_Path) > ./gccRelease/src/scenepanel.d

# Compiles file src/startmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/startmenupanel.d
./gccRelease/src/startmenupanel.o: src/startmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/startmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/startmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/startmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/startmenupanel.d

# Compiles file src/statssubmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/statssubmenupanel.d
./gccRelease/src/statssubmenupanel.o: src/statssubmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/statssubmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/statssubmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/statssubmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/statssubmenupanel.d

# Compiles file src/storysubmenupanel.cpp for the Release configuration...
-include ./gccRelease/src/storysubmenupanel.d
./gccRelease/src/storysubmenupanel.o: src/storysubmenupanel.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/storysubmenupanel.cpp $(Release_Include_Path) -o ./gccRelease/src/storysubmenupanel.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/storysubmenupanel.cpp $(Release_Include_Path) > ./gccRelease/src/storysubmenupanel.d

# Compiles file src/audio.cpp for the Release configuration...
-include ./gccRelease/src/audio.d
./gccRelease/src/audio.o: src/audio.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/audio.cpp $(Release_Include_Path) -o ./gccRelease/src/audio.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/audio.cpp $(Release_Include_Path) > ./gccRelease/src/audio.d

# Compiles file src/engine.cpp for the Release configuration...
-include ./gccRelease/src/engine.d
./gccRelease/src/engine.o: src/engine.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/engine.cpp $(Release_Include_Path) -o ./gccRelease/src/engine.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/engine.cpp $(Release_Include_Path) > ./gccRelease/src/engine.d

# Compiles file src/glossary.cpp for the Release configuration...
-include ./gccRelease/src/glossary.d
./gccRelease/src/glossary.o: src/glossary.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/glossary.cpp $(Release_Include_Path) -o ./gccRelease/src/glossary.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/glossary.cpp $(Release_Include_Path) > ./gccRelease/src/glossary.d

# Compiles file src/main_game.cpp for the Release configuration...
-include ./gccRelease/src/main_game.d
./gccRelease/src/main_game.o: src/main_game.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/main_game.cpp $(Release_Include_Path) -o ./gccRelease/src/main_game.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/main_game.cpp $(Release_Include_Path) > ./gccRelease/src/main_game.d

# Compiles file src/utils.cpp for the Release configuration...
-include ./gccRelease/src/utils.d
./gccRelease/src/utils.o: src/utils.cpp
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -c src/utils.cpp $(Release_Include_Path) -o ./gccRelease/src/utils.o
	$(CPP_COMPILER) $(Release_Preprocessor_Definitions) $(Release_Compiler_Flags) -MM src/utils.cpp $(Release_Include_Path) > ./gccRelease/src/utils.d

# Creates the intermediate and output folders for each configuration...
.PHONY: create_folders
create_folders:
	mkdir -p ./gccDebug
	mkdir -p ./gccRelease

# Cleans intermediate and output files (objects, libraries, executables)...
.PHONY: clean
clean:
	rm -f ./gccDebug/*.o
	rm -f ./gccDebug/*.d
	rm -f ./gccDebug/*.a
	rm -f ./gccDebug/*.so
	rm -f ./gccDebug/*.dll
	rm -f ./gccDebug/*.exe
	rm -f ./gccRelease/*.o
	rm -f ./gccRelease/*.d
	rm -f ./gccRelease/*.a
	rm -f ./gccRelease/*.so
	rm -f ./gccRelease/*.dll
	rm -f ./gccRelease/*.exe

